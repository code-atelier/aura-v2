﻿using Aura.IO;
using Aura.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Aura
{
    public class SpriteManager : ICollection<Sprite>, ISpriteSource, IFrameReference, IDisposable
    {
        public IFileSource FileSource { get; set; }

        private List<Sprite> Sprites { get; } = new List<Sprite>();

        private ImageCache ImageCache { get; } = new ImageCache();

        public double FrameTime { get; private set; }

        public int Count => Sprites.Count;

        public bool IsReadOnly => false;

        public bool LazyLoad { get; set; } = true;

        public DispatcherTimer DispatcherTimer { get; } = new DispatcherTimer(DispatcherPriority.Render);

        public SpriteManager()
        {
            FileSource = new VirtualFileSystem();
            DispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 16);
            DispatcherTimer.Tick += DispatcherTimer_Tick;
            lastTick = DateTime.Now;
            DispatcherTimer.Start();
        }

        DateTime lastTick;
        private void DispatcherTimer_Tick(object? sender, EventArgs e)
        {
            var now = DateTime.Now;
            var delta = now - lastTick;
            FrameTime += delta.TotalMilliseconds;
            lastTick = now;
        }

        public void Add(Sprite item)
        {
            if (Contains(item.Id))
            {
                Remove(item.Id);
                ImageCache.Remove(item.Id);
            }
            Sprites.Add(item);
            ImageCache.Add(item.Id, LazyLoad ? null : LoadImageForSprite(item));
        }

        public void Clear()
        {
            Sprites.Clear();
            ImageCache.Clear();
            FrameTime = 0;
        }

        public bool Contains(Guid item)
        {
            return Sprites.Any(x => x.Id == item);
        }

        public bool Contains(Sprite item)
        {
            return Sprites.Contains(item);
        }

        public void CopyTo(Sprite[] array, int arrayIndex)
        {
            Sprites.CopyTo(array, arrayIndex);
        }

        public int GetCurrentFrameIndex(int framePerSecond, int frameCount, LoopKind loopKind, double startingFrameTime = 0)
        {
            if (frameCount <= 0) return -1;
            if (frameCount == 1) return 0;

            var totalFrameTime = FrameTime - Math.Min(FrameTime, Math.Max(0, startingFrameTime));
            double frameMillis = 1000d / Math.Max(1, framePerSecond);
            double totalFrameMillis = frameMillis * frameCount;
            if (loopKind == LoopKind.None)
            {
                if (totalFrameTime >= totalFrameMillis)
                {
                    return frameCount - 1;
                }
                return (int)Math.Floor(totalFrameTime / frameMillis);
            }
            if (loopKind == LoopKind.ForwardThenBackward)
                totalFrameMillis += frameMillis * (frameCount - 2);
            double currentLoop = totalFrameTime % totalFrameMillis;
            while (currentLoop < 0) currentLoop += totalFrameMillis;
            var frIndex = (int)Math.Floor(currentLoop / frameMillis);
            return frIndex < frameCount ? frIndex : frameCount - (frIndex - frameCount) - 2;
        }

        public IEnumerator<Sprite> GetEnumerator()
        {
            return Sprites.GetEnumerator();
        }

        public Sprite? GetSprite(Guid spriteId)
        {
            return Sprites.FirstOrDefault(x => x.Id == spriteId);
        }

        public ImageSource? GetSpriteAsImageSource(Guid spriteId)
        {
            if (!Contains(spriteId)) return null;
            var img = ImageCache[spriteId];
            if (img == null)
            {
                var sprite = GetSprite(spriteId);
                if (sprite == null) return null;
                img = ImageCache[spriteId] = LoadImageForSprite(sprite);
            }
            return img;
        }

        public bool Remove(Sprite item)
        {
            ImageCache.Remove(item.Id);
            return Sprites.Remove(item);
        }

        public bool Remove(Guid item)
        {
            ImageCache.Remove(item);
            Sprites.RemoveAll(x => x.Id == item);
            return true;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private ImageSource? LoadImageForSprite(Sprite sprite)
        {
            var buffer = sprite.BinaryData != null && sprite.BinaryData.Length > 0 ? sprite.BinaryData : FileSource.GetFileAsBuffer(sprite.Path);
            if (buffer == null || buffer.Length == 0) return null;
            var ms = new MemoryStream(buffer);
            var bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = ms;
            bmp.EndInit();
            return bmp;
        }

        public void Dispose()
        {
            DispatcherTimer.Stop();
        }
    }
}
