﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public static class InternalTags
    {
        /// <summary>
        /// Emotion tags always have values between 0..1
        /// </summary>
        public static class Emotion
        {
            public const string Happy = "emo:happy";
            public const string Sad = "emo:sad";

            public const string Angry = "emo:angry";
            public const string Scared = "emo:scared";

            public const string Anticipating = "emo:anticipating";
            public const string Surprised = "emo:surprised";

            public const string Trusting = "emo:trusting";
            public const string Disgusted = "emo:disgusted";
        }

        /// <summary>
        /// Composite Emotion tags always have values between 0..1 and it is calculated by averaging two emotions
        /// </summary>
        public static class CompositeEmotion
        {
            public const string Excited = "emo:excited"; // anticipating & happy
            public const string Alerted = "emo:alerted"; // anger & anticipating
            public const string Hatred = "emo:hatred"; // disgust & anger
            public const string Pity = "emo:pity"; // sad & disgust
            public const string Shocked = "emo:shocked"; // surprise & sad
            public const string Panicked = "emo:panicked"; // fear & surprise
            public const string Obedient = "emo:obedient"; // trust & fear
            public const string Comfort = "emo:comfort"; // trust & joy
        }

        /// <summary>
        /// Urges tags are using activation function. Ultimately, it should be between 0..1
        /// </summary>
        public static class Urges
        {
            public const string Lust = "urge:lust"; // -asexual +horny
            public const string Hunger = "urge:hunger"; // -full +hungry
            public const string Social = "urge:social"; // -passive +active
            public const string Fun = "urge:fun"; //
            public const string Hygiene = "urge:hygiene"; // 
            public const string Sleep = "urge:sleep"; // 
        }

        /// <summary>
        /// Region tags does not have value and should never be used in tag pool
        /// </summary>
        public static class Region
        {
            public const string Hair = "hair";
            public const string Head = "head";
            public const string Forehead = "head:forehead";
            public const string HeadTop = "head:top";
            public const string Cheek = "head:cheek";
            public const string Mouth = "head:mouth";
            public const string Nose = "head:nose";
            public const string Eyes = "head:eye";
            public const string EyeRight = "head:eye:right";
            public const string EyeLeft = "head:eye:left";
            public const string Ears = "head:ear";
            public const string EarRight = "head:ear:right";
            public const string EarLeft = "head:ear:left";
            public const string Chin = "head:chin";
            public const string Neck = "neck";
            public const string Shoulders = "shoulder";
            public const string ShoulderRight = "shoulder:right";
            public const string ShoulderLeft = "shoulder:left";
            public const string Arms = "arms";
            public const string ArmRight = "arm:right";
            public const string ArmRightUpper = "arm:right:upper";
            public const string ArmRightLower = "arm:right:lower";
            public const string ArmLeft = "arm:left";
            public const string ArmLeftUpper = "arm:left:upper";
            public const string ArmLeftLower = "arm:left:lower";
            public const string Hands = "hand";
            public const string HandRight = "hand:right";
            public const string HandLeft = "hand:left";
            public const string Chest = "chest";
            public const string ChestCollar = "chest:collar";
            public const string ChestBreasts = "chest:breast";
            public const string ChestBreastRight = "chest:breast:right";
            public const string ChestBreastLeft = "chest:breast:left";
            public const string ChestNipples = "chest:breast:nipple";
            public const string ChestNippleRight = "chest:breast:right:nipple";
            public const string ChestNippleLeft = "chest:breast:left:nipple";
            public const string Stomach = "stomach";
            public const string Navel = "stomach:navel";
            public const string Waist = "waist";
            public const string Hip = "hip";
            public const string Butt = "butt";
            public const string Crotch = "crotch";
            public const string Vagina = "crotch:vagina";
            public const string Penis = "crotch:penis";
            public const string Thighs = "thigh";
            public const string ThighRight = "thigh:right";
            public const string ThighLeft = "thigh:left";
            public const string Ankles = "ankle";
            public const string AnkleRight = "ankle:right";
            public const string AnkleLeft = "ankle:left";
            public const string Feet = "foot";
            public const string FootRight = "foot:right";
            public const string FootLeft = "foot:left";
        }

        /// <summary>
        /// Anchor tags does not have value and should never be used in tag pool
        /// </summary>
        public static class Anchor
        {
            public const string Position = "anchor:position";

            public const string BaloonLeft = "anchor:baloon:left";
            public const string BaloonLeftTop = "anchor:baloon:left:top";
            public const string BaloonLeftBottom = "anchor:baloon:left:bottom";
            public const string BaloonCenter = "anchor:baloon:center";
            public const string BaloonCenterTop = "anchor:baloon:center:top";
            public const string BaloonCenterBottom = "anchor:baloon:center:bottom";
            public const string BaloonRight = "anchor:baloon:right";
            public const string BaloonRightTop = "anchor:baloon:right:top";
            public const string BaloonRightBottom = "anchor:baloon:right:bottom";

            public const string CommandOrigin = "anchor:command:origin";
            public const string CommandLeft = "anchor:command:left";
            public const string CommandRight = "anchor:command:right";
            public const string CommandTray = "anchor:command:tray";
        }
    }
}
