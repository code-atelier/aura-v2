﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public interface IFileSource
    {
        byte[]? GetFileAsBuffer(string path);

        Stream? GetFileStream(string path);
    }
}
