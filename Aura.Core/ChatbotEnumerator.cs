﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public class ChatbotEnumerator : IAutoConfigItemSource
    {
        public static Dictionary<string, object?> Items { get; } = new Dictionary<string, object?>()
        {
            { "<< Auto >>", null }
        };

        public object? this[string key] { get => Items[key]; set => Items[key] = value; }

        public IEnumerator<KeyValuePair<string, object?>> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }
    }
}
