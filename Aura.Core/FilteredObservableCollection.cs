﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura
{
    public class FilteredObservableCollection<T> : ICollection<T>, IEnumerable<T>, IList<T>, IList, INotifyCollectionChanged, INotifyPropertyChanged
    {
        private ObservableCollection<T> UnderlyingCollection { get; } = new ObservableCollection<T>();

        public FilteredObservableCollection(ObservableCollection<T> underlyingCollection)
        {
            UnderlyingCollection = underlyingCollection;
            (UnderlyingCollection as INotifyCollectionChanged).CollectionChanged += FilteredObservableCollection_CollectionChanged;
            (UnderlyingCollection as INotifyPropertyChanged).PropertyChanged += FilteredObservableCollection_PropertyChanged;
        }
        public FilteredObservableCollection()
        {
            (UnderlyingCollection as INotifyCollectionChanged).CollectionChanged += FilteredObservableCollection_CollectionChanged;
            (UnderlyingCollection as INotifyPropertyChanged).PropertyChanged += FilteredObservableCollection_PropertyChanged;
        }

        private void FilteredObservableCollection_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        private void FilteredObservableCollection_CollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            InvokeCollectionChanged(e);
        }

        bool ignoreCollectionChanged = false;
        private void InvokeCollectionChanged(NotifyCollectionChangedEventArgs? e = null)
        {
            if (ignoreCollectionChanged) return;
            if (e == null)
            {
                e = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            }
            CollectionChanged?.Invoke(this, e);
        }

        Func<T, bool>? _filter = null;
        public Func<T, bool>? Filter
        {
            get => _filter;
            set
            {
                _filter = value;
                InvokeCollectionChanged();
            }
        }

        public void OrderBy<TKey>(Func<T, TKey> selector)
        {
            var col = UnderlyingCollection.OrderBy(selector);
            ignoreCollectionChanged = true;
            try
            {
                UnderlyingCollection.Clear();
                foreach (var item in col)
                {
                    UnderlyingCollection.Add(item);
                }
            }
            finally
            {
                ignoreCollectionChanged = false;
                InvokeCollectionChanged();
            }
        }

        private List<T> FilteredCollection { get => Filter != null ? UnderlyingCollection.Where(Filter).ToList() : UnderlyingCollection.ToList(); }

        public object? this[int index]
        {
            get => FilteredCollection[index];
            set
            {
                if (Filter != null)
                {
                    throw new Exception("Cannot change item to filtered collection");
                }
                if (value is T tvalue)
                {
                    UnderlyingCollection[index] = tvalue;
                }
                else
                {
                    throw new Exception("Cannot add this item to the collection");
                }
            }
        }

        T IList<T>.this[int index] { 
            get => FilteredCollection[index]; 
            set
            {
                if (Filter != null)
                {
                    throw new Exception("Cannot change item to filtered collection");
                }
                UnderlyingCollection[index] = value;
            }
        }

        public bool IsFixedSize => (UnderlyingCollection as IList).IsFixedSize;

        public bool IsReadOnly => (UnderlyingCollection as IList).IsReadOnly;

        public int Count => FilteredCollection.Count;

        public bool IsSynchronized => (UnderlyingCollection as IList).IsSynchronized;

        public object SyncRoot => (UnderlyingCollection as IList).SyncRoot;

        public int Add(object? value)
        {
            if (value is T tvalue)
            {
                UnderlyingCollection.Add(tvalue);
                return UnderlyingCollection.Count - 1;
            }
            else
            {
                return -1;
            }
        }

        public void Add(T item)
        {
            UnderlyingCollection.Add(item);
        }

        public void Clear()
        {
            UnderlyingCollection.Clear();
        }

        public bool Contains(object? value)
        {
            if (value is T tvalue)
            {
                return FilteredCollection.Contains(tvalue);
            }
            return false;
        }

        public bool Contains(T item)
        {
            return FilteredCollection.Contains(item);
        }

        public void CopyTo(Array array, int index)
        {
            FilteredCollection.ToArray().CopyTo(array, index);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            FilteredCollection.CopyTo(array, arrayIndex);
        }

        public IEnumerator GetEnumerator()
        {
            return FilteredCollection.GetEnumerator();
        }

        public int IndexOf(object? value)
        {
            if (value is T tvalue)
            {
                return FilteredCollection.IndexOf(tvalue);
            }
            return -1;
        }

        public int IndexOf(T item)
        {
            return FilteredCollection.IndexOf(item);
        }

        public void Insert(int index, object? value)
        {
            if (Filter != null)
            {
                throw new Exception("Cannot change item to filtered collection");
            }
            if (value is T item)
            {
                UnderlyingCollection.Insert(index, item);
            }
        }

        public void Insert(int index, T item)
        {
            if (Filter != null)
            {
                throw new Exception("Cannot change item to filtered collection");
            }
            UnderlyingCollection.Insert(index, item);
        }

        public void Remove(object? value)
        {
            if (Filter != null)
            {
                throw new Exception("Cannot change item to filtered collection");
            }
            if (value is T item)
            {
                UnderlyingCollection.Remove(item);
            }
        }

        public bool Remove(T item)
        {
            if (Filter != null)
            {
                throw new Exception("Cannot change item to filtered collection");
            }
            return UnderlyingCollection.Remove(item);
        }

        public void RemoveAt(int index)
        {
            if (Filter != null)
            {
                throw new Exception("Cannot change item to filtered collection");
            }
            UnderlyingCollection.RemoveAt(index);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return FilteredCollection.GetEnumerator();
        }

        public event NotifyCollectionChangedEventHandler? CollectionChanged;

        public event PropertyChangedEventHandler? PropertyChanged;

    }
}
