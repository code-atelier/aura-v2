﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace Aura
{
    public class IPCServer
    {
        AuraLogger Logger { get; }
        public bool Started { get; private set; }
        bool WantStop { get; set; }
        public string PipeName { get; }
        public IServiceMessageBus ServiceMessageBus { get; }
        CancellationTokenSource? CancellationTokenSource { get; set; }

        public IPCServer(string pipeName, IServiceMessageBus serviceMessageBus)
        {
            PipeName = pipeName;
            ServiceMessageBus = serviceMessageBus;
            Logger = new AuraLogger("logs\\ipc_service.log");
            Logger.Information($"NamedPipeServerStream '{pipeName}' created.");
        }

        public void Stop()
        {
            if (!Started) return;
            WantStop = true;
            if (CancellationTokenSource != null)
            {
                CancellationTokenSource.Cancel();
            }
        }

        public async void Start()
        {
            if (Started) return;
            try
            {
                await Task.Run(() =>
                {
                    CancellationTokenSource = new CancellationTokenSource();
                    WantStop = false;
                    Started = true;
                    Run();
                });
            }
            catch { }
            finally
            {
            }
        }

        private async void Run()
        {
            try
            {
                while (!WantStop)
                {
                    if (CancellationTokenSource != null)
                        await ReceiveConnection(CancellationTokenSource.Token);
                    else
                        await Task.Delay(1000);
                }
            }
            finally
            {
                Started = false;
            }
        }

        private async Task ReceiveConnection(CancellationToken cancellationToken)
        {
            try
            {
                using (var serverStream = new NamedPipeServerStream(PipeName, PipeDirection.In))
                {
                    Logger.Information($"[{PipeName}] waiting for connection...");
                    await serverStream.WaitForConnectionAsync(cancellationToken);
                    Logger.Information($"[{PipeName}] client connected.");
                    using (StreamReader sr = new StreamReader(serverStream, Encoding.UTF8, true, -1, true))
                    {
                        var base64 = await sr.ReadLineAsync(cancellationToken);
                        if (base64 == null) return;
                        var buffer = Convert.FromBase64String(base64);
                        // this should be an IPC Message
                        var line = Encoding.UTF8.GetString(buffer);
                        try
                        {
                            var ipcMessage = JsonSerializer.Deserialize<IPCMessage>(line);
                            if (ipcMessage != null)
                            {
                                if (ipcMessage.Type == "uri")
                                {
                                    var uriRequest = JsonSerializer.Deserialize<UriRequest>(ipcMessage.Message);
                                    if (uriRequest != null)
                                    {
                                        Logger.Information($"[{PipeName}] uris received: " + string.Join("\r\n", uriRequest.Uris));
                                        var msg = new Message<UriRequest>(InternalServices.IPC, InternalEvents.UriRequest, uriRequest, "URIRequest");
                                        await ServiceMessageBus.SendMessageAsync(msg);
                                    }
                                    else
                                    {
                                        throw new Exception("Received invalid Uri message");
                                    }
                                }
                                else
                                {
                                    Logger.Information($"[{PipeName}] message received: " + ipcMessage.Message);
                                    var msg = new Message<IPCMessage>(InternalServices.IPC, InternalEvents.IPCReceived, ipcMessage, "IPCMessage");
                                    await ServiceMessageBus.SendMessageAsync(msg);
                                }
                                Logger.Information($"[{PipeName}] message sent.");
                            }
                            else
                            {
                                throw new Exception("Received invalid IPC message");
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex.Message);
                            using (StreamWriter sw = new StreamWriter(serverStream, Encoding.UTF8, -1, true))
                            {
                                sw.WriteLine("ERROR");
                            }
                        }
                    }
                }
            }
            catch { }
        }
    }

    public static class IPCClient
    {
        public static bool SendMessageToPipe(string pipeName, string message)
        {
            try
            {
                using (var client = new NamedPipeClientStream(".", pipeName, PipeDirection.Out))
                {
                    client.Connect();
                    using (var sw = new StreamWriter(client, Encoding.UTF8, -1, true))
                    {
                        var base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(message));
                        sw.WriteLine(base64);
                    }
                    client.Close();
                }
                return true;
            }
            catch { return false; }
        }

        public static bool SendMessageToPipe(string pipeName, IPCMessage message)
        {
            var json = JsonSerializer.Serialize(message);
            return SendMessageToPipe(pipeName, json);
        }
    }

    public class IPCMessage
    {

        [JsonPropertyName("message")]
        public string Message { get; set; } = string.Empty;

        [JsonPropertyName("type")]
        public string Type { get; set; } = string.Empty;

        public string ToJSON()
        {
            return JsonSerializer.Serialize(this);
        }

        public T? DeserializeMessage<T>()
        {
            return JsonSerializer.Deserialize<T>(Message);
        }
    }
}
