﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.IO
{
    public class VirtualFileSystem : IDictionary<string, MemoryStream>, IFileSource
    {
        private Dictionary<string, MemoryStream> _fs = new Dictionary<string, MemoryStream>();

        private string SanitizePath(string key)
        {
            return key.Trim().Replace("\\", "/");
        }

        public MemoryStream this[string path]
        {
            get
            {
                if (!ContainsKey(path))
                    throw new Exception("File not found in virtual file system: " + path);
                var ms = _fs[SanitizePath(path)];
                ms.Seek(0, SeekOrigin.Begin);
                return ms;
            }

            set
            {
                Add(path, value);
            }
        }

        public ICollection<string> Keys => _fs.Keys;

        public ICollection<MemoryStream> Values => _fs.Values;

        public int Count => _fs.Count;

        public bool IsReadOnly => false;

        public void Add(string path, MemoryStream value)
        {
            path = SanitizePath(path);
            _fs[path] = value;
        }

        public void Add(string path, Stream stream, long position = 0, SeekOrigin seekOrigin = SeekOrigin.Begin, long length = -1)
        {
            stream.Seek(position, seekOrigin);
            var buf = new byte[length < 0 ? stream.Length - stream.Position : length];
            stream.Read(buf, 0, buf.Length);
            var ms = new MemoryStream(buf);
            Add(path, ms);
        }

        public void Add(KeyValuePair<string, MemoryStream> item)
        {
            Add(item.Key, item.Value);
        }

        public void WriteAllText(string path, string content)
        {
            var buf = Encoding.UTF8.GetBytes(content);
            var ms = new MemoryStream(buf);
            ms.Seek(0, SeekOrigin.Begin);
            Add(path, ms);
        }

        public string ReadAllText(string path)
        {
            var ms = this[path];
            ms.Seek(0, SeekOrigin.Begin);
            var buf = new byte[ms.Length];
            ms.Read(buf);
            return Encoding.UTF8.GetString(buf);
        }

        public void Clear()
        {
            _fs.Clear();
        }

        public bool Contains(KeyValuePair<string, MemoryStream> item)
        {
            return ContainsKey(item.Key);
        }

        public bool Contains(string path)
        {
            return ContainsKey(path);
        }

        public bool ContainsKey(string path)
        {
            return _fs.Keys.Any(x => x.ToLower() == path.ToLower().Trim());
        }

        public void CopyTo(KeyValuePair<string, MemoryStream>[] array, int arrayIndex)
        {
            throw new NotSupportedException();
        }

        public IEnumerator<KeyValuePair<string, MemoryStream>> GetEnumerator()
        {
            return _fs.ToList().GetEnumerator();
        }

        public bool Remove(string path)
        {
            MemoryStream? ms = this[path];
            try
            {
                ms.Dispose();
            }
            catch { }
            return _fs.Remove(path);
        }

        public bool Remove(KeyValuePair<string, MemoryStream> item)
        {
            return Remove(item.Key);
        }

        public bool TryGetValue(string path, [MaybeNullWhen(false)] out MemoryStream value)
        {
            return _fs.TryGetValue(path, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void SaveAsArchive(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            using (var fo = File.Create(path))
            {
                using (ZipArchive archive = new ZipArchive(fo, ZipArchiveMode.Create))
                {
                    foreach (var file in this)
                    {
                        var entry = archive.CreateEntry(file.Key, CompressionLevel.Optimal);
                        using (var strm = entry.Open())
                        {
                            var source = file.Value;
                            source.Seek(0, SeekOrigin.Begin);
                            source.CopyTo(strm);
                        }
                    }
                }
            }
        }

        public void LoadFromArchive(string path)
        {
            using (var fi = File.OpenRead(path))
            {
                using (ZipArchive archive = new ZipArchive(fi, ZipArchiveMode.Read))
                {
                    this.Clear();
                    foreach (var entry in archive.Entries)
                    {
                        using (var ts = entry.Open())
                        {
                            var ms = new MemoryStream();
                            ts.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            Add(entry.Name, ms);
                        }
                    }
                }
            }
        }

        public byte[]? GetFileAsBuffer(string path)
        {
            path = SanitizePath(path);
            if (ContainsKey(path))
            {
                var ms = this[path];
                var buf = new byte[ms.Length];
                ms.Seek(0, SeekOrigin.Begin);
                ms.Read(buf, 0, buf.Length);
                return buf;
            }
            return null;
        }

        public Stream? GetFileStream(string path)
        {
            var buf = GetFileAsBuffer(path);
            if (buf != null)
            {
                return new MemoryStream(buf);
            }
            return null;
        }
    }
}
