﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public interface IFrameReference
    {
        double FrameTime { get; }

        int GetCurrentFrameIndex(int framePerSecond, int frameCount, LoopKind loopKind, double startingFrameTime = 0);
    }
}
