﻿namespace Aura
{
    public interface IObjectWithReference
    {
        /// <summary>
        /// The id of the object.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// The reference of the object.
        /// </summary>
        string? Reference { get; }
    }
}
