﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    /// <summary>
    /// An interface that describes a component that have an icon.
    /// </summary>
    public interface IComponentWithIcon
    {
        /// <summary>
        /// The URI of the icon image.
        /// </summary>
        string IconUri { get; }
    }
}
