﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public static class InternalServices
    {
        public const string APIService = "aura.api";
        public const string AvatarService = "aura.avatar";
        public const string ConfigurationService = "aura.config";
        public const string ScriptingService = "aura.script";
        public const string TagService = "aura.tag";
        public const string MainUI = "aura.ui";
        public const string ToastNotification = "windows.toastnotification";
        public const string IPC = "windows.ipc";
    }
    public static class AuraPaths
    {
        public const string Assemblies = "services\\{assembly}\\bin";
        public const string Services = "services";
        public const string Compendium = "data\\compendium";
    }

    public static class InternalEvents
    {
        public const string PersonaConfigurationUpdated = "persona:configupdated";
        public const string PersonaTagsUpdated = "persona:tagsupdated";
        public const string FileDrop = "filedrop";
        public const string ExecuteFileDrop = "execfiledrop";
        public const string ShowToastNotificationRequest = "toastnotificationrequest";
        public const string StartMenuOpened = "startmenuopened";

        public const string ToastShowInExplorerRequest = "toast:showinexplorerrequest";
        public const string ToastOpenFileRequest = "toast:openfilerequest";
        public const string ToastActivation = "toast:activation";

        public const string IPCReceived = "ipc:receivemessage";

        public const string UriRequest = "uri:request";
    }
}
