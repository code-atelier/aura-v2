﻿namespace Aura
{
    /// <summary>
    /// Provides information about an extension.
    /// </summary>
    public struct ExtensionInfo
    {
        /// <summary>
        /// Gets the id of this extension.
        /// </summary>
        public string Id;

        /// <summary>
        /// Gets the name of this extension.
        /// </summary>
        public string Name;

        /// <summary>
        /// Gets the icon of this extension.
        /// </summary>
        public string? Icon;

        /// <summary>
        /// Gets a description of this extension.
        /// </summary>
        public string Description;

        /// <summary>
        /// Gets the author of this extension.
        /// </summary>
        public string Author;

        /// <summary>
        /// Gets the current version of this extension.
        /// </summary>
        public Version Version;

        public override string ToString()
        {
            return Id + " (v" + Version + ")";
        }
    }
}
