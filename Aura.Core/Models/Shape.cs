﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Shape
    {
        [JsonPropertyName("type")]
        public ShapeType Type { get; set; }

        [JsonPropertyName("x")]
        public double X { get; set; }

        [JsonPropertyName("y")]
        public double Y { get; set; }

        [JsonPropertyName("width")]
        public double Width { get; set; }

        [JsonPropertyName("height")]
        public double Height { get; set; }

        public override string ToString()
        {
            return Type == ShapeType.Point ? $"{Type} ({X}, {Y})" : $"{Type} ({X}, {Y}, {Width}, {Height})";
        }
    }
}
