﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class MediaFile
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("reference")]
        public string Reference { get; set; } = string.Empty;

        [JsonPropertyName("extension")]
        public string Extension { get; set; } = string.Empty;

        [JsonIgnore]
        public byte[]? BinaryData { get; set; }

        [JsonIgnore]
        public string FileName { get => Id.ToString("n") + Extension; }

        public override string ToString()
        {
            return BuildScriptReference();
        }

        public virtual string BuildScriptReference(string? reference = null)
        {
            return reference ?? Reference;
        }
    }
}
