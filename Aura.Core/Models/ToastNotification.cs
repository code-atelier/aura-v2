﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class ToastNotification
    {
        public string Title { get; set; } = string.Empty;

        public string? Group { get; set; }

        public string? Tag { get; set; }

        public Dictionary<string, string> Arguments { get; set; } = new Dictionary<string, string>();

        public List<ToastNotificationContent> Contents { get; set; } = new List<ToastNotificationContent>();

        public DateTimeOffset? ExpirationTime { get; set; }

        public ToastNotificationScenario Scenario { get; set; }

        public ToastNotificationDuration Duration { get; set; }
    }

    public class ToastNotificationContent
    {
        public string? Text { get; set; }

        public Uri? ImageUri { get; set; }

        public Uri? ActivationUri { get; set; }

        public ToastNotificationButtonActivation ButtonActivation { get; set; }

        public Dictionary<string, string> Arguments { get; set; } = new Dictionary<string, string>();

        public ToastNotificationContentKind Kind { get; set; } = ToastNotificationContentKind.Text;
    }

    public enum ToastNotificationContentKind
    {
        Text,
        InlineImage,
        Button,
        HeroImage,
    }

    public enum ToastNotificationScenario
    {
        //
        // Summary:
        //     The normal Toast behavior. The Toast appears for a short duration, and then automatically
        //     dismisses into Action Center.
        Default,
        //
        // Summary:
        //     Causes the Toast to stay on-screen and expanded until the user takes action.
        //     Also causes a looping alarm sound to be selected by default.
        Alarm,
        //
        // Summary:
        //     Causes the Toast to stay on-screen and expanded until the user takes action.
        Reminder,
        //
        // Summary:
        //     Causes the Toast to stay on-screen and expanded until the user takes action (on
        //     Mobile this expands to full screen). Also causes a looping incoming call sound
        //     to be selected by default.
        IncomingCall
    }

    public enum ToastNotificationDuration
    {
        //
        // Summary:
        //     Default value. Toast appears for a short while and then goes into Action Center.
        Short,
        //
        // Summary:
        //     Toast stays on-screen for longer, and then goes into Action Center.
        Long
    }

    public enum ToastNotificationButtonActivation
    {
        /// <summary>
        /// Calls back to application
        /// </summary>
        BackgroundActivation,

        /// <summary>
        /// Opens a Uri. ActionUri is required.
        /// </summary>
        ProtocolActivation,

        /// <summary>
        /// Act as a snooze button
        /// </summary>
        Snooze,

        /// <summary>
        /// Act as a dismiss button
        /// </summary>
        Dismiss,
    }
}
