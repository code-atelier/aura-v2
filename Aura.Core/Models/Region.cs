﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Region
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Gets region reference
        /// </summary>
        [JsonPropertyName("reference")]
        public string? Reference { get; set; }

        /// <summary>
        /// Get region definitions
        /// </summary>
        [JsonPropertyName("shapes")]
        public List<Shape> Shapes { get; set; } = new List<Shape>();

        /// <summary>
        /// Gets the tags and its weights.
        /// </summary>
        [JsonPropertyName("tagCondition")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public TagCondition TagCondition { get; set; } = new TagCondition();

        public override string ToString()
        {
            return (Reference + "    [ID: " + Id.ToString("d") + "]").Trim();
        }
    }
}
