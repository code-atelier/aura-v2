﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Set: IObjectWithReference
    {
        /// <summary>
        /// Set Id
        /// </summary>
        [JsonPropertyName("id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Set reference
        /// </summary>
        [JsonPropertyName("ref")]
        public string? Reference { get; set; }

        /// <summary>
        /// Set category
        /// </summary>
        [JsonPropertyName("category")]
        public string? Category { get; set; }

        /// <summary>
        /// Set with higher priority will prevail when having the same total weight.
        /// </summary>
        [JsonPropertyName("priority")]
        public int Priority { get; set; }

        /// <summary>
        /// Set name
        /// </summary>
        [JsonIgnore]
        public string Name { get => !string.IsNullOrWhiteSpace(Category) ? Category + "#" + Reference : Reference ?? ""; }

        /// <summary>
        /// Gets the layer configuration for this set. If the value is null, it should not override anything. If the value is empty guid, it should remove the layer image.
        /// Value is guid reference to material.
        /// </summary>
        [JsonPropertyName("layers")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public Dictionary<string, Guid?> Layers { get; set; } = new Dictionary<string, Guid?>();

        /// <summary>
        /// Gets the tags and its weights.
        /// </summary>
        [JsonPropertyName("tagCondition")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public TagCondition TagCondition { get; set; } = new TagCondition();

        public override string ToString()
        {
            return Name;
        }
    }
}
