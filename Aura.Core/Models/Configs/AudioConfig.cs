﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models.Configs
{
    public class AudioConfig
    {
        [JsonPropertyName("voiceVolume")]
        [AutoConfigMember("Voice Volume",
            Description = "The volume of avatar's voice lines",
            Kind = AutoConfigPropertyKind.Slider,
            NumericMaxValue = 100,
            NumericMinValue = 0,
            SliderSteps = 1
            )]
        public double VoiceVolume { get; set; } = 100;
    }
}
