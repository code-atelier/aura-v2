﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models.Configs
{
    [AuraConfiguration("aura.json")]
    [AutoConfig("Aura")]
    public class AuraConfig
    {
        [JsonPropertyName("autoStart")]
        public Dictionary<string, bool> AutoStartService { get; set; } = new Dictionary<string, bool>();

        [JsonPropertyName("defaultServiceAutoStart")]
        [AutoConfigMember("Service auto start behavior",
            Description = "Allow service to auto start when not explicitly disabled",
            Kind = AutoConfigPropertyKind.Toggle,
            Icon = "res/img/window-system.png",
            Index = 99
            )]
        public bool DefaultServiceAutoStart { get; set; } = true;

        [JsonPropertyName("avatar")]
        [AutoConfigMember("Avatar",
            Description = "Avatar position and control configuration",
            Icon = "res/img/profile.png",
            Index = 0,
            Kind = AutoConfigPropertyKind.Page)]
        public AvatarConfig Avatar { get; set; } = new AvatarConfig();

        [JsonPropertyName("audio")]
        [AutoConfigMember("Audio",
            Description = "Audio related configuration",
            Icon = "res/img/headphone.png",
            Index = 4,
            Kind = AutoConfigPropertyKind.Page)]
        public AudioConfig Audio { get; set; } = new AudioConfig();

        [JsonPropertyName("defaultChatbotService")]
        [AutoConfigMember("Default chatbot service",
            Description = "The default chatbot service to use when chatting in console",
            Kind = AutoConfigPropertyKind.DropDown,
            Icon = "res/img/bubbles-alt.png",
            Index = 1,
            ItemSource = typeof(ChatbotEnumerator)
            )]
        public string? DefaultChatbotService { get; set; }

        [JsonIgnore]
        [AutoConfigMember("Start on windows logon",
            Description = "Starts Aura when you logged on to Windows",
            Kind = AutoConfigPropertyKind.Toggle,
            Icon = "res/img/lightning.png",
            Index = 2
            )]
        public bool AutoLogon
        {
            get
            {
                try
                {
                    RegistryKey? reg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                    if (reg != null)
                    {
                        return reg.GetValue("Aurav2") != null;
                    }
                }
                catch
                {
                }
                return false;
            }
            set
            {
                try
                {
                    RegistryKey? reg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                    if (value)
                    {
                        if (reg != null)
                            reg.SetValue("Aurav2", "\"" + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Aura.exe" + "\" --quick");
                    }
                    else
                    {
                        if (reg != null)
                            reg.DeleteValue("Aurav2");
                    }
                }
                catch { }
            }
        }

        [JsonPropertyName("showInTaskbar")]
        [AutoConfigMember("Show in taskbar",
            Description = "Show Aura's main window in your taskbar",
            Kind = AutoConfigPropertyKind.Toggle,
            Icon = "res/img/window.png",
            Index = 3
            )]
        public bool ShowInTaskbar { get; set; } = true;

        [JsonPropertyName("startMenuShortcuts")]
        public List<ShortcutRecord> Shortcuts { get; set; } = new List<ShortcutRecord>();

        [AutoConfigMember("Working directory",
            Index = 101,
            Icon = "res/img/folder.png",
            MethodLabel = "Open",
            Description = "Open Aura's working directory"
        )]
        public void GoToRootDir()
        {
            Util.OpenFile(Environment.CurrentDirectory);
        }
    }

    public class ShortcutRecord : IComparable<ShortcutRecord>
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("path")]
        public string Path { get; set; } = string.Empty;

        [JsonPropertyName("lastAccess")]
        public DateTime? LastAccess { get; set; }

        [JsonIgnore]
        public bool IsDirectory { get => Directory.Exists(Path); }

        [JsonIgnore]
        public bool IsFile { get => File.Exists(Path); }

        [JsonIgnore]
        public bool Exists { get => IsDirectory || IsFile; }

        public ShortcutRecord()
        {

        }

        public ShortcutRecord(string path)
        {
            Path = path;
            Name = System.IO.Path.GetFileNameWithoutExtension(path);
        }

        public int CompareTo(ShortcutRecord? other)
        {
            if (other == null) return -1;
            // same access or both null
            if (IsDirectory && !other.IsDirectory) return -1;
            if (!IsDirectory && other.IsDirectory) return 1;
            // both are the same kind
            return Name.CompareTo(other.Name);
        }

        public override bool Equals(object? obj)
        {
            if (obj is ShortcutRecord sr)
            {
                return sr.Path.ToLower() == Path.ToLower();
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Path.ToLower().GetHashCode();
        }
    }
}
