﻿using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models.Configs
{
    public class AvatarConfig
    {
        [JsonPropertyName("personaId")]
        [AutoConfigMember("Persona",
            Description = "Persona to use",
            Kind = AutoConfigPropertyKind.CustomWithLabel,
            Icon = "res/img/profile.png",
            ConfigurationControl = typeof(PersonaCompendiumLinkLabel)
            )]
        public string? PersonaId { get; set; }

        [JsonPropertyName("position")]
        public AuraAvatarPosition? Position { get; set; }

        [JsonPropertyName("topMost")]
        [AutoConfigMember("Top most",
            Description = "Aura stays on top of other windows",
            Icon = "res/img/aura-topmost.png",
            Kind = AutoConfigPropertyKind.Toggle
            )]
        public bool TopMost { get; set; } = true;

        [JsonPropertyName("lockPosition")]
        [AutoConfigMember("Lock position",
            Description = "Lock Aura in position so it cannot be moved",
            Icon = "res/img/aura-lockpos.png",
            Kind = AutoConfigPropertyKind.Toggle
            )]
        public bool LockPosition { get; set; } = false;

        [JsonPropertyName("snapToScreen")]
        [AutoConfigMember("Snap to screen",
            Description = "Snap Aura inside screen boundary",
            Kind = AutoConfigPropertyKind.Toggle
            )]
        public bool SnapToScreen { get; set; } = true;

        [JsonPropertyName("scale")]
        [AutoConfigMember("Scale",
            Description = "Avatar scale in percentage",
            Kind = AutoConfigPropertyKind.Slider,
            NumericMaxValue = 200,
            NumericMinValue = 25,
            SliderSteps = 5
            )]
        public double Scale { get; set; } = 100;
    }

    public class AuraAvatarPosition
    {
        [JsonPropertyName("x")]
        public int X { get; set; }

        [JsonPropertyName("y")]
        public int Y { get; set; }
    }
}
