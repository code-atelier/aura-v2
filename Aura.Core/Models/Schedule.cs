﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class ScheduleEntry
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("dateTimeRange")]
        public ScheduleDateTimeRange DateTimeRange { get; set; } = new ScheduleDateTimeRange();

        [JsonPropertyName("tags")]
        public List<WeightedTag> Tags { get; set; } = new List<WeightedTag>();

        public override string ToString()
        {
            return Name;
        }
    }

    public class ScheduleDateTimeRange
    {
        [JsonPropertyName("kind")]
        public ScheduleDateRangeKind Kind { get; set; }

        [JsonPropertyName("startDate")]
        public DateTime? StartDate { get; set; }

        [JsonPropertyName("endDate")]
        public DateTime? EndDate { get; set; }

        [JsonPropertyName("useTimeRange")]
        public bool UseTimeRange { get; set; }

        [JsonPropertyName("startTime")]
        public TimeSpan? StartTime { get; set; }

        [JsonPropertyName("endTime")]
        public TimeSpan? EndTime { get; set; }

        [JsonPropertyName("dayOfWeek")]
        public List<DayOfWeek> DayOfWeeks { get; set; } = new List<DayOfWeek>();

        public bool Contains(DateTime dateTime)
        {
            bool dateValid = false;
            if (Kind == ScheduleDateRangeKind.AnyDate || (StartDate == null && EndDate == null))
            {
                dateValid = true;
            }
            else if (StartDate == null && EndDate != null)
            {
                dateValid = dateTime < EndDate.Value.Date.AddDays(1);
            }
            else if (StartDate != null && EndDate == null)
            {
                dateValid = StartDate.Value.Date <= dateTime;
            }
            else if (StartDate != null && EndDate != null)
            {
                dateValid = StartDate.Value.Date <= dateTime && dateTime < EndDate.Value.Date.AddDays(1);
            }
            if (!dateValid) return false;

            bool timeValid = false;
            var ctime = dateTime.TimeOfDay;
            if (!UseTimeRange || (StartTime == null && EndTime == null))
            {
                timeValid = true;
            }
            else if (StartTime == null && EndTime != null)
            {
                timeValid = ctime >= StartTime;
            }
            else if (StartTime != null && EndTime == null)
            {
                timeValid = ctime <= EndTime;
            }
            else if (StartTime != null && EndTime != null)
            {
                var sTime = StartTime.Value;
                var eTime = EndTime.Value;
                if (sTime > eTime)
                {
                    timeValid = sTime <= ctime || ctime <= eTime;
                }
                else
                {
                    timeValid = sTime <= ctime && ctime <= eTime;
                }
            }
            if (!timeValid) return false;

            return DayOfWeeks.Contains(dateTime.DayOfWeek);
        }

        [JsonIgnore]
        public bool ActiveNow { get => Contains(DateTime.Now); }
    }

    public enum ScheduleDateRangeKind
    {
        AnyDate,
        DateOfYear,
        FixedDate
    }
}
