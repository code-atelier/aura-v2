﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Layer
    {
        /// <summary>
        /// Layer id
        /// </summary>
        [JsonPropertyName("id")]
        public string Id { get; set; } = string.Empty;

        /// <summary>
        /// Default material for this layer.
        /// </summary>
        [JsonPropertyName("default")]
        public Guid? Default { get; set; }

        /// <summary>
        /// Available materials in this layer.
        /// </summary>
        [JsonPropertyName("materials")]
        public List<LayerMaterial> Materials { get; set; } = new List<LayerMaterial>();

        /// <summary>
        /// The Z-Index of this layer.
        /// </summary>
        [JsonPropertyName("zIndex")]
        public int ZIndex { get; set; }

        [JsonPropertyName("fadeIn")]
        public bool FadeIn { get; set; } = true;

        [JsonPropertyName("transitionFadeIn")]
        public bool TransitionFadeIn { get; set; } = true;

        [JsonPropertyName("fadeOut")]
        public bool FadeOut { get; set; }

        public override string ToString()
        {
            return Id + " [ZIndex: " + ZIndex + "]";
        }
    }
}
