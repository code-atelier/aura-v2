﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    /// <summary>
    /// Used to send schema uri request
    /// </summary>
    public class UriRequest
    {
        public List<Uri> Uris { get; set; } = new List<Uri>();
    }
}
