﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Psyche
    {
        /// <summary>
        /// Emotion decay window in seconds. Default is 300.
        /// </summary>
        [JsonPropertyName("emotionDecayWindow")]
        public double EmotionDecayWindow { get; set; } = 300;
    }
}
