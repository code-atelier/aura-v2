﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class FileDropEventBody
    {
        public IReadOnlyList<string> Files { get; }

        public FileDropEventBody(IEnumerable<string> files) {             
            Files = files.ToList();
        }
    }

    public class FileDropExecuteEventBody: FileDropEventBody
    {
        public FileDropHandler Handler { get; }

        public FileDropExecuteEventBody(IEnumerable<string> files, FileDropHandler handler): base(files)
        {
            Handler = handler;
        }
    }

    public class FileDropHandler
    {
        public string? Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string? Icon { get; set; }

        public string? Action { get; set; }
    }
}
