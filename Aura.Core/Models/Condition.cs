﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Condition
    {
        public string Tag { get; set; } = string.Empty;

        public string Operator { get; set; } = "=";

        public double Value { get; set; }

        public static string[] Operators { get; } = new string[]
        {
            "=",
            "!=",
            "<",
            "<=",
            ">",
            ">="
        };
    }
}
