﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class WeightedTag
    {
        string tag = string.Empty;

        [JsonPropertyName("tag")]
        public string Tag
        {
            get => tag; set
            {
                tag = value?.Trim().ToLower() ?? "";
            }
        }

        [JsonPropertyName("weight")]
        public double Weight { get; set; } = 0;

        public static double operator *(WeightedTag a, string tag)
        {
            if (string.IsNullOrWhiteSpace(tag)) return 0;
            if (string.IsNullOrWhiteSpace(a.Tag)) return 0;
            var tga = a.Tag.Trim().ToLower();
            var tgb = tag.Trim().ToLower();
            return tga == tgb ? a.Weight : 0;
        }
        public static double operator *(WeightedTag a, IEnumerable<string> tag)
        {
            var tga = a.Tag.Trim().ToLower();
            if (tag.Any(x => x != null && x.Trim().ToLower() == tga)) return a.Weight;
            return 0;
        }
        public static double operator *(WeightedTag a, IDictionary<string, double> tag)
        {
            var tga = a.Tag.Trim().ToLower();
            foreach (var kv in tag)
            {
                if (kv.Key?.Trim().ToLower() == tga) return kv.Value * a.Weight;
            }
            return 0;
        }

        public override string ToString()
        {
            return Tag.Trim().ToLower() + " (" + Math.Round(Weight, 2).ToString() + ")";
        }

        public MemoryTag ToMemoryTag(string? sourceId, DateTime? expireAt)
        {
            return new MemoryTag(Tag, sourceId, expireAt, Weight);
        }
    }

    public class MemoryTag: WeightedTag
    {
        public DateTime? ExpiresAt { get; }

        public string? SourceId { get; }

        public MemoryTag(string tag, double weight = 1)
        {
            Tag = tag.Trim().ToLower();
            Weight = weight;
        }

        public MemoryTag(string tag, string? sourceId, DateTime? expiresAt = null, double weight = 1)
        {
            Tag = tag.Trim().ToLower();
            SourceId = sourceId;
            ExpiresAt = expiresAt;
            Weight = weight;
        }

        public MemoryTag(string tag, DateTime? expiresAt, double weight = 1)
        {
            Tag = tag.Trim().ToLower();
            ExpiresAt = expiresAt;
            Weight = weight;
        }
        public MemoryTag(MemoryTagDto dto)
        {
            Tag = dto.Tag;
            SourceId = dto.SourceId;
            ExpiresAt = dto.ExpiresAt;
            Weight = dto.Weight;
        }

        public MemoryTagDto ToDto()
        {
            return new MemoryTagDto()
            {
                Tag = Tag,
                SourceId = SourceId,
                ExpiresAt = ExpiresAt,
                Weight = Weight
            };
        }
    }

    public class MemoryTagDto
    {
        public string Tag { get; set; } = string.Empty;

        public DateTime? ExpiresAt { get; set; }

        public string? SourceId { get; set; }

        public double Weight { get; set; }
    }

    public static class WeightedTagExtension
    {
        public static double CalculateWeight(this IEnumerable<WeightedTag> weightedTags, IEnumerable<string> tags)
        {
            return weightedTags.Sum(tag => tag * tags);
        }
        public static double CalculateWeight(this IEnumerable<WeightedTag> weightedTags, IDictionary<string, double> tags)
        {
            return weightedTags.Sum(tag => tag * tags);
        }
    }

    public class WeightedTagCollection : IDictionary<string, double>
    {
        protected Dictionary<string, double> TagValues { get; } = new Dictionary<string, double>();

        public double this[string key]
        {
            get
            {
                key = key?.Trim().ToLower() ?? "";
                if (!TagValues.ContainsKey(key)) return 0;
                return TagValues[key];
            }
            set
            {
                Add(key, value);
            }
        }

        public ICollection<string> Keys => TagValues.Keys.ToList();

        public ICollection<double> Values => TagValues.Values.ToList();

        public int Count => TagValues.Count;

        public bool IsReadOnly => false;

        public void Add(string key, double value)
        {
            key = key?.Trim().ToLower() ?? "";
            TagValues[key] = value;
            HasUpdate = true;
            InvokeTagChanged();
        }

        public void Add(KeyValuePair<string, double> item)
        {
            Add(item.Key, item.Value);
        }

        public void Add(WeightedTag weightedTag)
        {
            Add(weightedTag.Tag, weightedTag.Weight);
        }

        public void Clear()
        {
            TagValues.Clear();
            HasUpdate = true;
            InvokeTagChanged();
        }

        public bool Contains(KeyValuePair<string, double> item)
        {
            return ContainsKey(item.Key);
        }

        public bool ContainsKey(string key)
        {
            return TagValues.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<string, double>[] array, int arrayIndex)
        {
            TagValues.ToArray().CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<string, double>> GetEnumerator()
        {
            return TagValues.GetEnumerator();
        }

        public bool Remove(string key)
        {
            try
            {
                return TagValues.Remove(key);
            }
            finally
            {
                HasUpdate = true;
                InvokeTagChanged();
            }
        }

        public bool Remove(KeyValuePair<string, double> item)
        {
            return Remove(item.Key);
        }

        public bool TryGetValue(string key, [MaybeNullWhen(false)] out double value)
        {
            return TagValues.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        bool HasUpdate { get; set; } = false;

        bool IgnoreTagChange { get; set; } = false;

        public void BeginUpdate()
        {
            HasUpdate = false;
            IgnoreTagChange = true;
        }

        public void EndUpdate()
        {
            IgnoreTagChange = false;
            if (HasUpdate)
            {
                InvokeTagChanged();
                HasUpdate = false;
            }
        }

        protected void InvokeTagChanged()
        {
            if (!IgnoreTagChange)
            {
                TagChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler? TagChanged;
    }

    public class TagCondition
    {
        [JsonPropertyName("bias")]
        public double Bias { get; set; } = 1;

        [JsonPropertyName("tags")]
        public List<WeightedTag> Tags { get; set; } = new List<WeightedTag>();

        public double CalculateWeight(IDictionary<string, double> tags)
        {
            return Tags.Sum(tag => tag * tags) + Bias;
        }

        public TagCondition Clone()
        {
            var res = new TagCondition();
            res.Bias = Bias;
            foreach(var tag in Tags)
            {
                res.Tags.Add(new WeightedTag() { Tag = tag.Tag, Weight = tag.Weight });
            }
            return res;
        }
    }

    public class MemoryTagCollection : ICollection<MemoryTag>
    {
        private List<MemoryTag> InternalList { get; } = new List<MemoryTag>();

        public int Count => InternalList.Count;

        public bool IsReadOnly => false;

        public void Add(MemoryTag item)
        {
            InternalList.Add(item);
            RemoveExpiredTags();
        }

        public void AddRange(IEnumerable<MemoryTag> items)
        {
            InternalList.AddRange(items);
            RemoveExpiredTags();
        }

        public void Clear()
        {
            InternalList.Clear();
        }

        public bool Contains(MemoryTag item)
        {
            return InternalList.Contains(item);
        }

        public void CopyTo(MemoryTag[] array, int arrayIndex)
        {
            InternalList.CopyTo(array, arrayIndex);
        }

        public IEnumerator<MemoryTag> GetEnumerator()
        {
            return InternalList.GetEnumerator();
        }

        public bool Remove(MemoryTag item)
        {
            return InternalList.Remove(item);
        }

        public int RemoveAll(Predicate<MemoryTag> predicate)
        {
            return InternalList.RemoveAll(predicate);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int RemoveExpiredTags()
        {
            try
            {
                return InternalList.RemoveAll(x => x.ExpiresAt != null && x.ExpiresAt <= DateTime.Now);
            }
            catch { }
            return 0;
        }

        public int RemoveTagsBySource(string sourceId)
        {
            return InternalList.RemoveAll(x => x.SourceId == sourceId);
        }
    }
}
