﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class LayerMaterial
    {
        /// <summary>
        /// Material
        /// </summary>
        [JsonPropertyName("material")]
        public Guid Material { get; set; }

        /// <summary>
        /// Left offset
        /// </summary>
        [JsonPropertyName("x")]
        public int X { get; set; }

        /// <summary>
        /// Top offset
        /// </summary>
        [JsonPropertyName("y")]
        public int Y { get; set; }
    }
}
