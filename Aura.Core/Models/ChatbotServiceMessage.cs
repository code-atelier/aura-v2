﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public abstract class ChatbotServiceMessage
    {
        public abstract bool IsHuman { get; }

        public string? MessageId { get; set; }

        public string? AuthorName { get; set; }
        public string? AuthorId { get; set; }

        public DateTime Timestamp { get; set; } = DateTime.Now;

        public string Message { get; set; } = string.Empty;

        public bool IsSuccessful { get; set; } = true;
    }

    public class ChatbotServiceUserMessage: ChatbotServiceMessage
    {
        public override bool IsHuman { get; } = true;

        public int Retries { get; set; } = 1;

        public int RetryDelaySecond { get; set; } = 2;
    }

    public class ChatbotServiceBotMessage: ChatbotServiceMessage
    {
        public override bool IsHuman { get; } = false;

        public string? LastUserMessageId { get; set; }

        public string? UserMessage { get; set; }
    }
}
