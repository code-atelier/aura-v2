﻿using System.IO;
using System.Text.Json.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Aura.Models
{
    public class Avatar
    {
        /// <summary>
        /// Get the layers of this avatar.
        /// </summary>
        [JsonPropertyName("layers")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public List<Layer> Layers { get; set; } = new List<Layer>();

        /// <summary>
        /// Gets the sets of this avatar.
        /// </summary>
        [JsonPropertyName("sets")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public ObjectWithReferenceCollection<Set> Sets { get; set; } = new ObjectWithReferenceCollection<Set>();

        /// <summary>
        /// Gets the materials of this avatar.
        /// </summary>
        [JsonPropertyName("materials")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public ObjectWithReferenceCollection<Material> Materials { get; set; } = new ObjectWithReferenceCollection<Material>();

        /// <summary>
        /// Gets the sets of this avatar.
        /// </summary>
        [JsonPropertyName("sprites")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public ObjectWithReferenceCollection<Sprite> Sprites { get; set; } = new ObjectWithReferenceCollection<Sprite>();

        [JsonPropertyName("regions")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public List<Region> Regions { get; set; } = new List<Region>();

        [JsonPropertyName("display")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public DisplayConfiguration Display { get; set; } = new DisplayConfiguration();

        public Dictionary<string, List<Set>> GetCategorizedSets()
        {
            var res = new Dictionary<string, List<Set>>();
            foreach (var set in Sets)
            {
                var cat = set.Category?.Trim().ToLower();
                if (string.IsNullOrWhiteSpace(cat))
                {
                    if (!string.IsNullOrWhiteSpace(set.Reference))
                    {
                        res["#nocat_" + Guid.NewGuid().ToString("n")] = new List<Set>() { set };
                    }
                }
                else if (!res.ContainsKey(cat))
                {
                    res[cat] = new List<Set> { set };
                }
                else
                {
                    res[cat].Add(set);
                }
            }
            foreach(var r in res)
            {
                r.Value.Sort((a,b) => a.Priority.CompareTo(b.Priority));
            }
            return res;
        }

    }
}
