﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models.Memory
{
    public interface IFact
    {
        /// <summary>
        /// The subject of the fact
        /// </summary>
        string Subject { get; } 
    }

    public interface INumericFact: IFact
    {
        /// <summary>
        /// The value of this fact (0..1)
        /// </summary>
        double Value { get; set; }
    }
}
