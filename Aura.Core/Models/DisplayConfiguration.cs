﻿using System.Text.Json.Serialization;

namespace Aura.Models
{
    public class DisplayConfiguration
    {

        /// <summary>
        /// Drawable width of virtual canvas.
        /// </summary>
        [JsonPropertyName("canvasWidth")]
        public int CanvasWidth { get; set; }

        /// <summary>
        /// Drawable height of virtual canvas.
        /// </summary>
        [JsonPropertyName("canvasHeight")]
        public int CanvasHeight { get; set; }
    }
}
