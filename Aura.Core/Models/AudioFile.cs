﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class AudioFile: MediaFile
    {
        [JsonPropertyName("category")]
        public AudioCategory Category { get; set; }

        public override string BuildScriptReference(string? reference = null)
        {
            return (Category == AudioCategory.Voice ? "voi:" : Category == AudioCategory.SoundEffect ? "sfx:" : "mus:") + (reference ?? Reference);
        }
    }

    public enum AudioCategory: int
    {
        Voice = 0,
        SoundEffect = 1,
        Music = 2,
    }
}
