﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.Models
{
    public class Persona
    {
        [JsonPropertyName("id")]
        public string Id { get; set; } = Guid.NewGuid().ToString("n");

        [JsonPropertyName("avatar")]
        public Avatar Avatar { get; set; } = new Avatar();

        [JsonPropertyName("givenName")]
        public string GivenName { get; set; } = string.Empty;

        [JsonPropertyName("familyName")]
        public string FamilyName { get; set; } = string.Empty;

        [JsonPropertyName("author")]
        public string Author { get; set; } = string.Empty;

        [JsonPropertyName("tags")]
        public List<string> Tags { get; set; } = new List<string>();

        [JsonPropertyName("birthday")]
        public DateTime? Birthday { get; set; }

        [JsonPropertyName("nsfw")]
        public bool NSFW { get; set; } = false;

        [JsonPropertyName("randomSeed")]
        public int RandomSeed { get; set; } = new Random().Next(int.MaxValue);

        [JsonPropertyName("schedule")]
        public List<ScheduleEntry> Schedule { get; set; } = new List<ScheduleEntry>();

        [JsonPropertyName("events")]
        public List<PersonaEvent> Events { get; set; } = new List<PersonaEvent>();

        [JsonPropertyName("audios")]
        public List<AudioFile> AudioFiles { get; set; } = new List<AudioFile>();

        [JsonPropertyName("targetVersion")]
        public string? TargetVersion { get; set; }

        [JsonPropertyName("makerVersion")]
        public string? MakerVersion { get; set; }

        [JsonPropertyName("personaVersion")]
        public string? PersonaVersion { get; set; } = "1.0.0";

        public void Save(string path)
        {
            string tempDir = Path.Combine(Path.GetTempPath(), "p" + Guid.NewGuid().ToString("n"));
            string archiveDir = Path.Combine(tempDir, "arc");
            string imageDir = Path.Combine(archiveDir, "images");
            string audioDir = Path.Combine(archiveDir, "audios");

            string zipPath = Path.Combine(tempDir, "persona.zip");
            try
            {
                if (Directory.Exists(tempDir))
                {
                    Directory.Delete(tempDir, true);
                }
                Directory.CreateDirectory(tempDir);
                Directory.CreateDirectory(archiveDir);
                Directory.CreateDirectory(imageDir);
                Directory.CreateDirectory(audioDir);

                // save all sprites
                foreach (var sprite in Avatar.Sprites)
                {
                    if (sprite.BinaryData != null)
                    {
                        sprite.Path = "images/" + sprite.Id.ToString("n");
                        File.WriteAllBytes(Path.Combine(imageDir, sprite.Id.ToString("n")), sprite.BinaryData);
                    }
                }

                // save all audio
                foreach(var audio in AudioFiles)
                {
                    if (audio.BinaryData != null)
                    {
                        var fname = audio.FileName;
                        File.WriteAllBytes(Path.Combine(audioDir, fname), audio.BinaryData);
                    }
                }

                var json = JsonSerializer.Serialize(this);
                File.WriteAllText(Path.Combine(archiveDir, "persona.json"), json);

                ZipFile.CreateFromDirectory(archiveDir, zipPath);

                // copy zip
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                File.Move(zipPath, path);
            }
            finally
            {
                try
                {
                    File.Delete(zipPath);
                }
                catch { }
                try
                {
                    Directory.Delete(tempDir, true);
                }
                catch { }
            }
        }

        public static Persona Load(string path, bool loadBinaryData = true)
        {
            Persona? persona = null;
            using (var zip = ZipFile.OpenRead(path))
            {
                // load persona manifest
                var manifest = zip.Entries.FirstOrDefault(x => x.FullName == "persona.json");
                if (manifest == null) throw new Exception("Persona manifest is not found!");
                
                using (var strm = manifest.Open())
                {
                    var ms = new MemoryStream();
                    strm.CopyTo(ms);
                    var json = Encoding.UTF8.GetString(ms.ToArray());
                    var opt = new JsonSerializerOptions();
                    persona = JsonSerializer.Deserialize<Persona>(json, opt);
                }

                if (persona == null) throw new Exception("Invalid persona manifest!");

                foreach (var spr in persona.Avatar.Sprites)
                {
                    var sprStrm = zip.Entries.FirstOrDefault(x => x.FullName == spr.Path);
                    if (sprStrm == null) throw new Exception("Sprite data cannot be found: " + spr.Path);

                    if (loadBinaryData)
                    {
                        using (var s = sprStrm.Open())
                        {
                            var ms = new MemoryStream();
                            s.CopyTo(ms);
                            spr.BinaryData = ms.ToArray();
                        }
                    }
                }
                foreach (var audio in persona.AudioFiles)
                {
                    var sprStrm = zip.Entries.FirstOrDefault(x => x.FullName == "audios/" + audio.FileName);
                    if (sprStrm == null) throw new Exception("Audio data cannot be found: " + audio.FileName);

                    if (loadBinaryData)
                    {
                        using (var s = sprStrm.Open())
                        {
                            var ms = new MemoryStream();
                            s.CopyTo(ms);
                            audio.BinaryData = ms.ToArray();
                        }
                    }
                }
            }

            return persona;
        }
    }
}
