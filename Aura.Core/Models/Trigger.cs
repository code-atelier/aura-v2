﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Trigger
    {
        [JsonPropertyName("actionId")]
        public string? ActionId { get; set; }

        [JsonPropertyName("parameters")]
        public Dictionary<string, string> Parameters { get; set; } = new Dictionary<string, string>();
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class TriggerActionInfoAttribute : Attribute
    {
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;
    }

    public static class TriggerAction
    {
        [TriggerActionInfo(Name = "Tag Changed", Description = "When active tag has changed")]
        public const string TagChanged = "tag:changed";

        [TriggerActionInfo(Name = "File Dropped", Description = "A file dropped to Aura")]
        public const string FileDropped = "file:dropped";

        [TriggerActionInfo(Name = "Region Clicked", Description = "{region:region?} is clicked by the user")]
        public const string RegionClicked = "region:click";

        [TriggerActionInfo(Name = "Time Changed", Description = "Every {secondVal:int=1} second(s) elapsed")]
        public const string TimeChanged = "time:second";
    }

}
