﻿using System.Text.Json.Serialization;

namespace Aura.Models
{
    public class Sprite: IObjectWithReference
    {
        /// <summary>
        /// Sprite Id
        /// </summary>
        [JsonPropertyName("id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Sprite reference
        /// </summary>
        [JsonPropertyName("ref")]
        public string? Reference { get; set; }

        /// <summary>
        /// Path to image file
        /// </summary>
        [JsonPropertyName("path")]
        public string Path { get; set; } = string.Empty;

        /// <summary>
        /// Image width
        /// </summary>
        [JsonPropertyName("width")]
        public int Width { get; set; }

        /// <summary>
        /// Image height
        /// </summary>
        [JsonPropertyName("height")]
        public int Height { get; set; }

        [JsonIgnore]
        public byte[] BinaryData { get; set; } = new byte[0];

        public override string ToString()
        {
            return (Reference + "    [ID: " + Id.ToString("d") + "]").Trim();
        }
    }
}
