﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum ShapeType
    {
        Rectangle = 0,
        Ellipse = 1,
        Point = 2,
    }
}
