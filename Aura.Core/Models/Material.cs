﻿using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Windows.Media.Imaging;

namespace Aura.Models
{
    public class Material : IObjectWithReference
    {
        /// <summary>
        /// Material Id
        /// </summary>
        [JsonPropertyName("id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Material reference
        /// </summary>
        [JsonPropertyName("ref")]
        public string? Reference { get; set; }

        /// <summary>
        /// Material width
        /// </summary>
        [JsonPropertyName("width")]
        public int Width { get; set; }

        /// <summary>
        /// Material height
        /// </summary>
        [JsonPropertyName("height")]
        public int Height { get; set; }

        /// <summary>
        /// Guid references to sprite
        /// </summary>
        [JsonPropertyName("frames")]
        public List<Guid> Frames { get; set; } = new List<Guid>();

        /// <summary>
        /// Frames per second
        /// </summary>
        [JsonPropertyName("fps")]
        public int FramesPerSecond { get; set; } = 6;

        /// <summary>
        /// Animation loop kind.
        /// </summary>
        [JsonPropertyName("loopKind")]
        public LoopKind LoopKind { get; set; } = LoopKind.None;

        [JsonIgnore]
        public double StartingFrameTime { get; set; }

        public override string ToString()
        {
            return (Reference + "    [ID: " + Id.ToString("d") + "]").Trim();
        }

        public void Save(string filePath, Avatar avatar)
        {
            string tempDir = Path.Combine(Path.GetTempPath(), "p" + Guid.NewGuid().ToString("n"));
            string archiveDir = Path.Combine(tempDir, "arc");
            string imageDir = Path.Combine(archiveDir, "images");
            string zipPath = Path.Combine(tempDir, "material.zip");

            try
            {
                if (Directory.Exists(tempDir))
                {
                    Directory.Delete(tempDir, true);
                }
                Directory.CreateDirectory(tempDir);
                Directory.CreateDirectory(archiveDir);
                Directory.CreateDirectory(imageDir);

                var sprMap = new Dictionary<string, string?>();

                // save sprites related to this material
                foreach (var sprite in avatar.Sprites.Where(x => Frames.Contains(x.Id)))
                {
                    if (sprite.BinaryData != null)
                    {
                        sprite.Path = "images/" + sprite.Id.ToString("n");
                        File.WriteAllBytes(Path.Combine(imageDir, sprite.Id.ToString("n")), sprite.BinaryData);
                        sprMap[sprite.Id.ToString("n")] = sprite.Reference;
                    }
                }

                var json = JsonSerializer.Serialize(this);
                File.WriteAllText(Path.Combine(archiveDir, "material.json"), json); 
                
                json = JsonSerializer.Serialize(sprMap);
                File.WriteAllText(Path.Combine(archiveDir, "spritemap.json"), json);

                ZipFile.CreateFromDirectory(archiveDir, zipPath);

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                File.Move(zipPath, filePath);
            }
            finally
            {
                try
                {
                    File.Delete(zipPath);
                }
                catch { }
                try
                {
                    Directory.Delete(tempDir, true);
                }
                catch { }
            }
        }

        public static Material Load(string path, Avatar avatar, bool loadBinaryData = true)
        {
            Material? material = null;
            using (var zip = ZipFile.OpenRead(path))
            {
                // load persona manifest
                var manifest = zip.Entries.FirstOrDefault(x => x.FullName == "material.json");
                if (manifest == null) throw new Exception("Material manifest is not found!");
                var spriteMap = zip.Entries.FirstOrDefault(x => x.FullName == "spritemap.json");
                if (spriteMap == null) throw new Exception("Sprite map is not found!");

                using (var strm = manifest.Open())
                {
                    var ms = new MemoryStream();
                    strm.CopyTo(ms);
                    var json = Encoding.UTF8.GetString(ms.ToArray());
                    material = JsonSerializer.Deserialize<Material>(json);
                }
                Dictionary<string, string?> sprMap;
                using (var strm = spriteMap.Open())
                {
                    var ms = new MemoryStream();
                    strm.CopyTo(ms);
                    var json = Encoding.UTF8.GetString(ms.ToArray());
                    sprMap = JsonSerializer.Deserialize<Dictionary<string, string?>>(json) ?? new Dictionary<string, string?>();
                }

                if (material == null) throw new Exception("Invalid material manifest!");

                if (avatar.Materials.Any(x => x.Reference == material.Reference || x.Id == material.Id))
                {
                    throw new Exception("Material with the same id or reference already existed: " + material.Reference + " {" + material.Id.ToString("n") + "}");
                }

                foreach (var entry in zip.Entries.Where(x => x.FullName.StartsWith("images/")))
                {
                    var id = entry.FullName.Substring(7);
                    Guid sprId;
                    if (Guid.TryParse(id, out sprId))
                    {
                        if (avatar.Sprites.Any(x => x.Id == sprId)) {
                            throw new Exception($"Sprite with id {{{id}}} is already exists!");
                        }
                        if (!sprMap.ContainsKey(id) || string.IsNullOrWhiteSpace(sprMap[id]) || avatar.Sprites.Any(x => x.Reference == sprMap[id]))
                        {
                            throw new Exception($"Sprite with id {{{id}}} does not have a reference!");
                        }
                        var spr = new Sprite()
                        {
                            Id = sprId,
                            Reference = sprMap[id],
                        };
                        using (var s = entry.Open())
                        {
                            var ms = new MemoryStream();
                            s.CopyTo(ms);
                            spr.BinaryData = ms.ToArray();
                            ms.Seek(0, SeekOrigin.Begin);

                            var bmp = new BitmapImage();
                            bmp.BeginInit();
                            bmp.StreamSource = ms;
                            bmp.EndInit();
                            spr.Width = bmp.PixelWidth;
                            spr.Height = bmp.PixelHeight;
                        }
                        avatar.Sprites.Add(spr);
                    }
                    else
                    {
                        throw new Exception("Invalid sprite: " + id);
                    }
                }

                avatar.Materials.Add(material);
            }

            return material;
        }
    }
}
