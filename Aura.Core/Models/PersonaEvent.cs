﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Aura.Models
{
    public class PersonaEvent
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("active")]
        public bool Active { get; set; } = true;

        [JsonPropertyName("script")]
        public string Script { get; set; } = string.Empty;

        public override string ToString()
        {
            return Name + " (" + (Active ? "ON" : "OFF") + ")";
        }
    }
}
