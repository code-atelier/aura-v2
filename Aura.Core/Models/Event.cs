﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Event
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("triggers")]
        public List<Trigger> Triggers { get; set; } = new List<Trigger>();

        [JsonPropertyName("conditions")]
        public List<Condition> Conditions { get; set; } = new List<Condition>();

        [JsonPropertyName("actions")]
        public List<Action> Actions { get; set; } = new List<Action>();
    }
}
