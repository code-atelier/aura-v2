﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    /// <summary>
    /// Contains static information about current Aura.
    /// </summary>
    public static class AuraInfo
    {
        public static Version Version { get; } = new Version(2, 2, 39);
    }
}
