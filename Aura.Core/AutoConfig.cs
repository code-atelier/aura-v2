﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class AutoConfigAttribute : Attribute
    {
        public string Name { get; set; }

        public double DefaultWidth { get; set; } = 800;

        public double DefaultHeight { get; set; } = 600;

        /// <summary>
        /// Absolute uri to icon
        /// </summary>
        public string? Icon { get; set; }

        public AutoConfigAttribute(string name)
        {
            Name = name;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class AutoConfigMemberAttribute : Attribute
    {
        public string Name { get; set; }

        public string? MethodLabel { get; set; }

        public string Description { get; set; } = string.Empty;

        public int Index { get; set; } = 0;

        public string? Group { get; set; }
        public int GroupIndex { get; set; } = 0;

        /// <summary>
        /// Absolute uri to icon
        /// </summary>
        public string? Icon { get; set; }

        public AutoConfigPropertyKind Kind { get; set; } = AutoConfigPropertyKind.Textbox;

        public double NumericMinValue { get; set; } = double.MinValue;
        public double NumericMaxValue { get; set; } = double.MaxValue;
        public double SliderSteps { get; set; } = 5;

        /// <summary>
        /// Items source for DropDown or Radio, must implement <see cref="IAutoConfigItemSource"/>
        /// </summary>
        public Type? ItemSource { get; set; }

        /// <summary>
        /// A user control that able to edit the configuration value. Must implement <see cref="IAutoConfigUserControl"/>
        /// </summary>
        public Type? ConfigurationControl { get; set; }

        /// <summary>
        /// A class that implements <see cref="IAutoConfigValidator"/>
        /// </summary>
        public Type? Validator { get; set; }

        /// <summary>
        /// Update the whole page when value changed.
        /// </summary>
        public bool UpdatePage { get; set; } = false;

        public bool ReadOnly { get; set; } = false;

        public AutoConfigMemberAttribute(string name)
        {
            Name = name;
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class AutoConfigCheckboxItemAttribute : Attribute
    {

    }

    public enum AutoConfigPropertyKind
    {
        /// <summary>
        /// Shows a textbox to edit texts.
        /// </summary>
        Textbox,

        /// <summary>
        /// Shows a textbox to edit integers.
        /// </summary>
        IntegerTextbox,

        /// <summary>
        /// Shows a textbox to edit decimals.
        /// </summary>
        DecimalTextbox,

        /// <summary>
        /// Shows a slider to edit numeric value.
        /// </summary>
        Slider,

        /// <summary>
        /// Shows a single checkbox as on/off.
        /// </summary>
        Toggle,

        /// <summary>
        /// Shows multiple checkboxes. Class properties must have <see cref="AutoConfigCheckboxItemAttribute"/> attribute.
        /// </summary>
        Checkboxes,

        /// <summary>
        /// Shows a list of radio buttons.
        /// </summary>
        Radio,

        /// <summary>
        /// Shows a drop down list.
        /// </summary>
        DropDown,

        /// <summary>
        /// Use custom control to edit value.
        /// </summary>
        Custom,

        /// <summary>
        /// Use custom control to edit value with additional label to show current value.
        /// </summary>
        CustomWithLabel,

        /// <summary>
        /// A label that is read only.
        /// </summary>
        ReadOnlyLabel,

        /// <summary>
        /// Shows a new page when clicked.
        /// </summary>
        Page,
    }

    public interface IAutoConfigItemSource : IEnumerable<KeyValuePair<string, object?>>
    {
        object? this[string key] { get; set; }
    }

    public interface IAutoConfigUserControl
    {
        void LoadConfigurationValue(object? value);

        object? GetConfigurationValue();

        event EventHandler? ValueChanged;
    }

    public interface IAutoConfigUserControlWithLabel : IAutoConfigUserControl
    {
        string GetLabel();
    }

    public interface IAutoConfigValidator
    {
        bool Validate(object? value, object? configurationObject);

        object? Format(object? value);
    }
}
