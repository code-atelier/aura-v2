﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class AuraConfigurationAttribute : Attribute
    {
        public string FileName { get; }

        public AuraConfigurationAttribute(string fileName)
        {
            FileName = fileName;
        }
    }
}
