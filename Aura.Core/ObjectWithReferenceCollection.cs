﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura
{
    public class ObjectWithReferenceCollection<T> : ICollection<T> where T : IObjectWithReference
    {
        Dictionary<Guid, T> objectLibrary { get; } = new Dictionary<Guid, T>();

        [JsonIgnore]
        public int Count => objectLibrary.Count;

        [JsonIgnore]
        public bool IsReadOnly => false;

        public void Add(T item)
        {
            if (!string.IsNullOrWhiteSpace(item.Reference))
            {
                if (objectLibrary.Values.Any(x => x.Reference == item.Reference))
                {
                    throw new Exception("Reference already exists: " + item.Reference);
                }
            }
            objectLibrary.Add(item.Id, item);
        }

        public void Clear()
        {
            objectLibrary.Clear();
        }

        public bool Contains(T item)
        {
            return objectLibrary.Values.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            objectLibrary.Values.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return objectLibrary.Values.GetEnumerator();
        }

        public bool Remove(T item)
        {
            return objectLibrary.Remove(item.Id);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public T? this[string reference]
        {
            get
            {
                return objectLibrary.Values.FirstOrDefault(x => x.Reference == reference);
            }
        }

        public T? this[Guid id]
        {
            get
            {
                return objectLibrary.ContainsKey(id) ? objectLibrary[id] : default;
            }
        }
    }
}
