﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Aura
{
    public class AuraLogger
    {
        public const string DefaultLogPath = "logs";
        public string LogTimestampFormat { get; set; } = "yyyy-MM-dd HH:mm:ss.fff";

        private string GetNewDefaultLogPath()
        {
            return Path.Combine(DefaultLogPath, DateTime.Now.ToString("yyyyMMdd") + ".aura.log");
        }

        /// <summary>
        /// Gets the current log file path.
        /// </summary>
        public string CurrentLogPath { get; private set; } = string.Empty;

        public AuraLogger() { 
            StartNew();
        }

        public AuraLogger(string path)
        {
            StartNew(path);
        }

        public void StartNew(string? logPath = null)
        {
            if (!Directory.Exists(DefaultLogPath))
            {
                Directory.CreateDirectory(DefaultLogPath);
            }
            logPath = logPath ?? GetNewDefaultLogPath();
            CurrentLogPath = logPath;

            Raw("---===== Aura ver. " + AuraInfo.Version + " =====---");
        }

        public void Raw(string raw)
        {
            try
            {
                File.AppendAllText(CurrentLogPath, raw + Environment.NewLine);
            }
            catch { }
        }

        /// <summary>
        /// Logs an exception.
        /// </summary>
        public void Error(Exception ex)
        {
            Error("Unhandled exception", ex);
        }

        /// <summary>
        /// Logs an exception with custom message.
        /// </summary>
        public void Error(string message, Exception ex)
        {
            var log = "[" + DateTime.Now.ToString(LogTimestampFormat) + "] Error: " + message;
            log += Environment.NewLine + $"{ex.GetType().Name}: " + ex.Message;
            log += Environment.NewLine + "Stack Trace:";
            log += Environment.NewLine + ex.StackTrace;

            var stack = 0;
            var inner = ex.InnerException;
            while (inner != null)
            {
                log += Environment.NewLine + Environment.NewLine + $"Inner#{stack++} >> {inner.GetType().Name}: " + inner.Message;
                log += Environment.NewLine + "Stack Trace:";
                log += Environment.NewLine + inner.StackTrace;

                inner = inner.InnerException;
            }

            Raw(log);
        }

        /// <summary>
        /// Logs a custom error message.
        /// </summary>
        public void Error(string message)
        {
            var log = "[" + DateTime.Now.ToString(LogTimestampFormat) + "] Error: " + message;
            Raw(log);
        }

        /// <summary>
        /// Logs a custom warning message.
        /// </summary>
        public void Warning(string message)
        {
            var log = "[" + DateTime.Now.ToString(LogTimestampFormat) + "] Warning: " + message;
            Raw(log);
        }

        /// <summary>
        /// Logs a custom message.
        /// </summary>
        public void Information(string message)
        {
            var log = "[" + DateTime.Now.ToString(LogTimestampFormat) + "] Info: " + message;
            Raw(log);
        }

        /// <summary>
        /// Logs a custom warning message.
        /// </summary>
        public void Debug(string message)
        {
            var log = "[" + DateTime.Now.ToString(LogTimestampFormat) + "] Debug: " + message;
            Raw(log);
        }
    }
}
