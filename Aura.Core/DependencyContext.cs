﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    /// <summary>
    /// Describes how a dependency should be injected.
    /// </summary>
    public class DependencyContext
    {
        /// <summary>
        /// Gets the identifier type.
        /// </summary>
        public Type SourceType { get; }

        /// <summary>
        /// Gets the instance type.
        /// </summary>
        public Type TargetType { get; }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        public object? Instance { get; }

        /// <summary>
        /// Gets the type of the dependency.
        /// </summary>
        public DependencyContextType Type { get; }

        public DependencyContext(Type sourceType, Type targetType)
        {
            SourceType = sourceType;
            TargetType = targetType;
            Type = DependencyContextType.Transient;
        }

        public DependencyContext(Type sourceType, Type targetType, object instance)
        {
            SourceType = sourceType;
            TargetType = targetType;
            Instance = instance;
            Type = DependencyContextType.Singleton;
        }
    }

    public enum DependencyContextType
    {
        Singleton,
        Transient
    }
}
