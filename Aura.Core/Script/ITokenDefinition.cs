﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public interface ITokenDefinition
    {
        IEnumerable<TokenMatch> Matches(string inputString);

        int Priority { get; set; }
    }

    public interface ITokenDefinition<TTokenType>: ITokenDefinition where TTokenType : Enum
    {
        new IEnumerable<TokenMatch<TTokenType>> Matches(string inputString);
    }
}
