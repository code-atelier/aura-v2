﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class Parser<TToken> where TToken : Token
    {
        protected TokenStream<TToken> Stream { get; set; } = new TokenStream<TToken>(new List<TToken>());

        protected Grammar Grammar { get; set; } = new Grammar();

        protected ParseTreeRoot Root { get; set; } = new ParseTreeRoot();

        public ParseResult Parse(TokenStream<TToken> stream, Grammar grammar)
        {
            var result = new ParseResult();
            try
            {
                if (!stream.HasToken)
                {
                    result.AddGlobalError("No token found.");
                    return result;
                }
                if (!grammar.RuleSet.ContainsKey("S"))
                {
                    result.AddGlobalError("Grammar does not have an entry point.");
                    return result;
                }
                var startRule = grammar.RuleSet["S"];
                Grammar = grammar;
                Stream = stream;
                Root = new ParseTreeRoot(Grammar);

                RunParser(result);
            }
            catch (Exception ex)
            {
                result.AddParserError(ex.Message);
            }
            return result;
        }

        public ParserExpression GetCompiled()
        {
            if (Root.Paths.Count == 1)
            {
                var exp = Root.Paths[0].Expression;
                exp.Simplify();
                return exp;
            }
            throw new Exception("Parser is not in a completed state");
        }

        protected void RunParser(ParseResult result)
        {
            while (Stream.HasToken)
            {
                var strm = Stream.Consume();
                var res = Root.Consume(strm);
                if (!res.Success)
                {
                    if (res.Error != null)
                    {
                        if (res.Token != null)
                            result.AddError(res.Error, res.Token);
                        else
                            result.AddGlobalError(res.Error);
                    }
                    return;
                }
            }
            Root.Consume(new EndOfFileToken());
        }
    }

    public class ParseTreeRoot : ParseTreeNode
    {
        public static ParseTreeRoot None { get; } = new ParseTreeRoot();

        public Grammar Grammar { get; }

        public ParseTreeRoot()
        {
            Grammar = new Grammar();
            Root = this;
        }

        public ParseTreeRoot(Grammar grammar)        {
            Grammar = grammar;
            Root = this;
            Token = new GrammarToken()
            {
                Kind = GrammarTokenKind.NonTerminal,
                Value = "S"
            };
            Reset();
        }

        public void Reset()
        {
            _paths.Clear();
        }
    }

    public class ParseTreePath
    {
        protected ParseTreeRoot Root { get; }

        public ParseTreeNode? Parent { get; internal set; }

        public GrammarRule Rule { get; }

        public GrammarToken ReferenceToken { get; }

        protected Stack<ParseTreeNode> _nodes { get; } = new Stack<ParseTreeNode>();

        protected List<ParserExpression> _yields { get; } = new List<ParserExpression>();

        public ParseTreePath(GrammarToken token, GrammarRule rule, ParseTreeRoot root)
        {
            ReferenceToken = token;
            Root = root;
            Rule = rule;
            _nodes.Push(ParseTreeNode.EndOfPath);
            for (var i = rule.Tokens.Count - 1; i >= 0; i--)
            {
                var t = rule.Tokens[i];
                var newNode = new ParseTreeNode(t, Root);
                newNode.Parent = this;
                _nodes.Push(newNode);
            }
        }

        public ParserExpression Expression
        {
            get
            {
                var res = new ParserExpression();
                res.ExpressionType = ReferenceToken.Value.TrimStart('@');
                res.Parameters.AddRange(_yields);
                return res;
            }
        }

        public ParseConsumeResult Consume(Token token)
        {
            if (_nodes.Count == 0)
            {
                return ParseConsumeResult.InvalidTokenError(token);
            }
            var top = _nodes.Peek();
            var res = top.Consume(token);
            if (res.Kind == ParseConsumeResultKind.Traverse)
            {
                _nodes.Pop();
                if (_nodes.Count > 0)
                    return Consume(token);
                // yield
                return ParseConsumeResult.YieldThenTraverse(Expression);
            }
            if (res.Kind == ParseConsumeResultKind.Yield)
            {
                _nodes.Pop();
                if (res.Result != null)
                    _yields.Add(res.Result);
                else
                    return ParseConsumeResult.ParserError($"Yield result does not have expression: " + CursorLocation());
                // consume
                return ParseConsumeResult.Consumed();
            }
            if (res.Kind == ParseConsumeResultKind.YieldThenTraverse)
            {
                _nodes.Pop();
                if (res.Result != null)
                    _yields.Add(res.Result);
                else
                    return ParseConsumeResult.ParserError($"Yield result does not have expression: " + CursorLocation());
                return Consume(token);
            }
            return res;
        }

        public override string ToString()
        {
            return string.Join(" -> ", _nodes.Where(x => x.Token.Kind != GrammarTokenKind.Return).Select(x => x.Token.Value));
        }

        public string CursorLocation()
        {
            var prefix = "";
            if (Parent != null)
            {
                prefix = Parent.CursorLocation(false) + " ==> ";
            }
            var cIndex = Rule.Tokens.Count - (_nodes.Count - 1);
            var li = new List<string>();
            for (var i = 0; i < Rule.Tokens.Count; i++)
            {
                if (cIndex == i)
                    li.Add("[" + Rule.Tokens[i].Value.TrimStart('@') + "]");
                else
                    li.Add(Rule.Tokens[i].Value.TrimStart('@'));
            }
            return prefix + string.Join(" ", li);
        }
    }

    public class ParseTreeNode
    {
        protected ParseTreeRoot Root { get; set; }

        public ParseTreePath? Parent { get; internal set; }

        public GrammarToken Token { get; protected set; }

        protected List<ParseTreePath> _paths { get; } = new List<ParseTreePath>();
        public IReadOnlyList<ParseTreePath> Paths { get => _paths; }

        public bool IsTerminal { get => Token.Kind == GrammarTokenKind.Terminal; }

        public bool IsEndOfPath { get; private set; }

        internal ParseTreeNode()
        {
            Root = ParseTreeRoot.None;
            Token = new GrammarToken();
        }

        public ParseTreeNode(GrammarToken token, ParseTreeRoot root)
        {
            Root = root;
            Token = token;
        }

        public static ParseTreeNode EndOfPath { get; } = new ParseTreeNode() { IsEndOfPath = true };

        public bool Expand()
        {
            if (IsTerminal || _paths.Count > 0) return false;

            var identifier = Token.Value.TrimStart('@');
            if (Root.Grammar.RuleSet.ContainsKey(identifier))
            {
                var rs = Root.Grammar.RuleSet[identifier];
                foreach (var rule in rs)
                {
                    var newPath = new ParseTreePath(Token, rule, Root);
                    newPath.Parent = this;
                    _paths.Add(newPath);
                }
                return true;
            }
            throw new Exception($"Rule '{identifier}' is not found");
        }

        public ParseConsumeResult Consume(Token token)
        {
            if (IsEndOfPath)
            {
                return ParseConsumeResult.Traverse();
            }
            if (IsTerminal)
            {
                // if this is a terminal there will be no path
                if (token.GetTokenType() == Token.Value)
                {
                    return ParseConsumeResult.Yield(Token.Value, token);
                }
                return ParseConsumeResult.ExpectingTokenError(token, Token.Value);
            }

            // Expand non-terminal node
            Expand();

            // feed token to all paths
            var results = new Dictionary<ParseConsumeResultKind, List<ParseConsumeResult>>()
            {
                { ParseConsumeResultKind.Consumed, new List<ParseConsumeResult>() },
                { ParseConsumeResultKind.YieldThenTraverse, new List<ParseConsumeResult>() },
                { ParseConsumeResultKind.Error, new List<ParseConsumeResult>() },
            };
            var shouldRemove = new List<ParseTreePath>();
            var ambiguous = new List<ParseTreePath>();
            foreach (var path in _paths)
            {
                var res = path.Consume(token);
                results[res.Kind].Add(res);
                if (res.Kind == ParseConsumeResultKind.Error || res.Kind == ParseConsumeResultKind.YieldThenTraverse)
                    shouldRemove.Add(path);
                if (res.Kind == ParseConsumeResultKind.Consumed || res.Kind == ParseConsumeResultKind.YieldThenTraverse)
                    ambiguous.Add(path);
            }

            if (results[ParseConsumeResultKind.Consumed].Count == 0)
            {
                var totalYield = results[ParseConsumeResultKind.YieldThenTraverse].Count;
                if (totalYield > 1)
                {
                    // should only be one yield, otherwise it is ambiguous grammar
                    return ParseConsumeResult.ParserError("Ambiguous grammar. Multiple yields in: " + Token.Value);
                }

                if (results[ParseConsumeResultKind.YieldThenTraverse].Count > 0)
                {
                    // return the only yield
                    return results[ParseConsumeResultKind.YieldThenTraverse].First();
                }
                else if (results[ParseConsumeResultKind.Error].Count > 0)
                {
                    // just return the first error
                    return results[ParseConsumeResultKind.Error].First();
                }
                else
                {
                    // no consume, yield, or error???
                    return ParseConsumeResult.ParserError("Parser is in an ambiguous state");
                }
            }
            else if (results[ParseConsumeResultKind.YieldThenTraverse].Count > 0)
            {
                // decide which path should be removed

            }

            // remove paths with error or yield
            _paths.RemoveAll(shouldRemove.Contains);

            return ParseConsumeResult.Consumed();
        }

        public override string ToString()
        {
            return IsEndOfPath ? "<End>" : Token.Value;
        }

        public string CursorLocation(bool printSelf = true)
        {
            string prefix;
            if (Parent != null)
            {
                prefix = Parent.CursorLocation();
                if (!printSelf)
                    return prefix;
            }
            else
            {
                return "{" + ToString() + "}";
            }
            return prefix + " >>> {" + ToString() + "}";
        }
    }

    public class ParseConsumeResult
    {
        public bool Success { get => Kind != ParseConsumeResultKind.Error; }

        public ParseConsumeResultKind Kind { get; private set; }

        public ParserExpression? Result { get; private set; }

        public string? Error { get; private set; }

        public Token? Token { get; private set; }

        public static ParseConsumeResult Traverse()
        {
            return new ParseConsumeResult()
            {
                Kind = ParseConsumeResultKind.Traverse
            };
        }

        public static ParseConsumeResult Consumed()
        {
            return new ParseConsumeResult()
            {
                Kind = ParseConsumeResultKind.Consumed
            };
        }

        public static ParseConsumeResult Yield(string expressionType, Token token)
        {
            return Yield(new ParserExpression()
            {
                ExpressionType = expressionType,
                Token = token
            });
        }

        public static ParseConsumeResult Yield(ParserExpression expression)
        {
            return new ParseConsumeResult()
            {
                Result = expression,
                Kind = ParseConsumeResultKind.Yield
            };
        }

        public static ParseConsumeResult YieldThenTraverse(string expressionType, Token token)
        {
            return YieldThenTraverse(new ParserExpression()
            {
                ExpressionType = expressionType,
                Token = token
            });
        }

        public static ParseConsumeResult YieldThenTraverse(ParserExpression expression)
        {
            return new ParseConsumeResult()
            {
                Result = expression,
                Kind = ParseConsumeResultKind.YieldThenTraverse
            };
        }

        public static ParseConsumeResult InvalidTokenError(Token token)
        {
            return new ParseConsumeResult()
            {
                Kind = ParseConsumeResultKind.Error,
                Token = token,
                Error = $"Invalid token '{token.GetTokenType()}'",
            };
        }

        public static ParseConsumeResult ExpectingTokenError(Token token, params string[] expected)
        {
            return new ParseConsumeResult()
            {
                Kind = ParseConsumeResultKind.Error,
                Token = token,
                Error = $"Expecting {string.Join(", ", expected.Select(x => "'" + x + "'"))} but found '{token.GetTokenType()}'"
            };
        }

        internal static ParseConsumeResult ParserError(string message, Token? token = null)
        {
            return new ParseConsumeResult()
            {
                Kind = ParseConsumeResultKind.Error,
                Token = token,
                Error = message
            };
        }
    }

    public enum ParseConsumeResultKind
    {
        Consumed,
        Yield,
        YieldThenTraverse,
        Traverse,
        Error,
    }
}