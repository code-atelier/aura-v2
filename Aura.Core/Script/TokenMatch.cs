﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class TokenMatch
    {
        public bool IsValid { get; set; }

        public int Length { get; set; }

        public string? Value { get; set; }

        public int Priority { get; set; }

        public Token ToToken(int index, int currentLine, int currentColumn)
        {
            return new Token()
            {
                Column = currentColumn,
                Line = currentLine,
                Index = index,
                Length = Length,
                Value = Value,
            };
        }
    }

    public class TokenMatch<TTokenType> : TokenMatch where TTokenType : Enum
    {
        public TTokenType? Type { get; set; }

        public new Token<TTokenType> ToToken(int index, int currentLine, int currentColumn)
        {
            return new Token<TTokenType>()
            {
                Column = currentColumn,
                Line = currentLine,
                Length = Length,
                Index = index,
                Value = Value,
                Type = Type
            };
        }
    }
}
