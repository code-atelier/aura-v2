﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class ParserExpressionConverter<TTarget>
    {
        protected Dictionary<string, ParserExpressionConverterLogic<TTarget>> Logics { get; } = new Dictionary<string, ParserExpressionConverterLogic<TTarget>>();

        protected List<string> Ignored { get; } = new List<string>();

        public TTarget? Convert(ParserExpression expression)
        {
            if (Ignored.Contains(expression.ExpressionType)) return default;
            if (!Logics.ContainsKey(expression.ExpressionType))
                throw new Exception($"No conversion exists for '{expression.ExpressionType}'");
            var conv = Logics[expression.ExpressionType];
            return conv.Convert(expression);
        }

        public T? Convert<T>(ParserExpression expression) where T : TTarget
        {
            var cvt = Convert(expression);
            if (cvt is T t) return t;
            return default;
        }

        public void Ignore(string identifier)
        {
            if (!Ignored.Contains(identifier))
            {
                Ignored.Add(identifier);
            }
        }
        public void Ignore(IEnumerable<string> identifiers)
        {
            foreach (var ident in identifiers)
            {
                Ignore(ident);
            }
        }

        public IParserExpressionConverterLogicBuilder<TTarget> Expression(string identifier)
        {
            if (Logics.ContainsKey(identifier)) return Logics[identifier];
            var logic = new ParserExpressionConverterLogic<TTarget>(identifier);
            Logics.Add(identifier, logic);
            return logic;
        }

        private Func<string, string, TTarget>? LiteralConstructorFunction { get; set; }
        public void LiteralConstructor(Func<string, string, TTarget> constructorFunction)
        {
            LiteralConstructorFunction = constructorFunction;
        }

        public void LiteralExpression(string identifier)
        {
            if (Logics.ContainsKey(identifier)) throw new Exception($"Conversion for '{identifier}' already existed");
            var logic = new ParserExpressionConverterLogic<TTarget>(identifier);

            logic.Conversion(x => LiteralConstructorFunction != null ?
                LiteralConstructorFunction.Invoke(
                    x.Token?.Value ?? throw new Exception($"'{x.ExpressionType}' is not a literal compatible type"),
                    x.ExpressionType
                )
                : throw new Exception("Literal constructor function is not defined")
            );
            Logics.Add(identifier, logic);
        }
        public void LiteralExpression(IEnumerable<string> identifiers)
        {
            foreach(var ident in identifiers)
            {
                LiteralExpression(ident);
            }
        }
    }

    public interface IParserExpressionConverterLogicBuilder<TTarget>
    {
        void Conversion(Func<ParserExpression, TTarget> conversionFunction);
    }

    public interface ILiteralExpression
    {
        object? Value { set; }
    }

    public class ParserExpressionConverterLogic<TTarget> : IParserExpressionConverterLogicBuilder<TTarget>
    {
        public string Identifier { get; }

        protected Func<ParserExpression, TTarget>? ConversionFunction { get; set; }

        public ParserExpressionConverterLogic(string expressionIdentifier)
        {
            Identifier = expressionIdentifier;
        }

        public TTarget Convert(ParserExpression expression)
        {
            if (ConversionFunction == null) throw new Exception($"Converter function for '{Identifier}' is not set");
            return ConversionFunction(expression);
        }

        public void Conversion(Func<ParserExpression, TTarget> conversionFunction)
        {
            ConversionFunction = conversionFunction;
        }
    }
}
