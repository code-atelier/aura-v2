﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class GrammarLexer
    {
        Lexer<GrammarTokenType> Lexer { get; } = new Lexer<GrammarTokenType>();

        public GrammarLexer()
        {
            InitLexer();
        }

        private void InitLexer()
        {
            Lexer.TokenDefinitions.Add(TokenDefinition.Create(GrammarTokenType.Identifier, "[a-zA-Z_][a-zA-Z0-9_]*"));
            Lexer.TokenDefinitions.Add(TokenDefinition.Create(GrammarTokenType.Variable, "@[a-zA-Z_][a-zA-Z0-9_]*"));

            Lexer.TokenDefinitions.Add(TokenDefinition.Create(GrammarTokenType.RuleDescriptor, "->", 1));
            Lexer.TokenDefinitions.Add(TokenDefinition.Create(GrammarTokenType.AlternateRule, @"\|", 1));
            Lexer.TokenDefinitions.Add(TokenDefinition.Create(GrammarTokenType.Quantitizer, @"(\*|\+|\?)", 1));
            Lexer.TokenDefinitions.Add(TokenDefinition.Create(GrammarTokenType.NewLine, @"(\r\n|\n|\r)", 1));

            Lexer.TokenDefinitions.Add(TokenDefinition.Create(GrammarTokenType.Comment, @"^\/\/[^\r\n]*", 2));
        }

        public IEnumerable<Token<GrammarTokenType>> Tokenize(string text)
        {
            return Lexer.Tokenize(text).Where(x => !(!x.IsValid && string.IsNullOrWhiteSpace(x.Value)));
        }
    }

    public enum GrammarTokenType
    {
        Invalid,

        Identifier,
        Variable,

        RuleDescriptor,
        AlternateRule,
        Quantitizer,
        NewLine,

        Comment,
    }
}
