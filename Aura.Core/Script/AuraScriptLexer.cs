﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class AuraScriptLexer : Lexer<AuraScriptTokenType>
    {
        public AuraScriptLexer() 
        {
            InitLexer();
        }

        private void InitLexer()
        {
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.TriggerClause, "on"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.ConditionClause, "when"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.BeginActions, "do"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.End, "end"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.If, "if"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Then, "then"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Else, "else"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.ElseIf, @"else\s+if"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.For, "for"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Set, "set"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Call, "call"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Run, "run"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Play, "play"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.After, "after"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Random, "random"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.In, @"(in|not\s+in)"));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Comment, @"(\/\/[^\r\n]*|\/\*(?:(?!\*\/).)*?\*\/)"));

            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.TimeInterval, @"[0-9]+(\\.[0-9]+)?(f|s|m|h|d|M|y)( +and +([0-9]+(\\.[0-9]+)?(ms|s|m|h|d|M|y)))*", 1));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.AudioIdentifier, "(voi|sfx|mus):[a-zA-Z_][a-zA-Z_0-9]*", 1));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.EventIdentifier, "e(:|!)[a-zA-Z_][a-zA-Z_0-9]*", 1));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.TagIdentifier, "t(:|!)[a-zA-Z_][a-zA-Z_0-9]*", 1));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.RegionIdentifier, "r(:|!)[a-zA-Z_][a-zA-Z_0-9]*", 1));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.StringLiteral, @"""(\\""|\\r|\\n|\\t|\\\\|\\\/|\\b|\\f|\\u[0-9a-fA-F]{4}|[^""\r\n\\])*""", 1));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.NumberLiteral, @"-?[0-9]+(\\.[0-9]+)?", 1));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.BooleanLiteral, @"true|false", 1));

            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.BooleanOperator, @"(==|!=|\>=|\<=|\|\||&&|\>|\<)", 2));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.ArithmeticOperator, @"(\+|-|\*|\/)", 2));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Terminal, ";", 2));
            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Braces, "\\(|\\)", 2));

            TokenDefinitions.Add(TokenDefinition.Create(AuraScriptTokenType.Assign, "=", 3));
        }

        public override IEnumerable<Token<AuraScriptTokenType>> Tokenize(string text)
        {
            return base.Tokenize(text).Where(x => !(!x.IsValid && string.IsNullOrWhiteSpace(x.Value)));
        }
    }

    public enum AuraScriptTokenType
    {
        Invalid,

        TriggerClause,
        ConditionClause,
        BeginActions,
        End,
        If,
        Then,
        Else,
        ElseIf,
        For,
        Set,
        Run,
        Call,
        Play,
        After,
        Random,
        In,

        TimeInterval,
        EventIdentifier,
        TagIdentifier,
        RegionIdentifier,
        AudioIdentifier,
        StringLiteral,
        NumberLiteral,
        BooleanLiteral,

        BooleanOperator,
        ArithmeticOperator,
        Terminal,
        Comment,

        Assign,

        Braces,
    }
}
