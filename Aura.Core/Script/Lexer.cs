﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class Lexer
    {
        public List<TokenDefinition> TokenDefinitions { get; } = new List<TokenDefinition>();

        public virtual IEnumerable<Token> Tokenize(string text)
        {
            int startIndex = 0;
            int line = 1;
            int column = 0;
            string invalidToken = "";
            while (startIndex < text.Length)
            {
                var match = FindMatch(text, startIndex);
                if (match != null)
                {
                    if (invalidToken != "")
                    {
                        yield return new Token()
                        {
                            IsValid = false,
                            Value = invalidToken,
                            Length = invalidToken.Length,
                            Index = startIndex,
                            Line = line,
                            Column = column,
                        };
                        UpdateLineAndColumn(invalidToken, ref line, ref column);
                        invalidToken = "";
                    }
                    yield return match.ToToken(startIndex, line, column);
                    UpdateLineAndColumn(match.Value, ref line, ref column);
                    startIndex += match.Length;
                }
                else
                {
                    invalidToken += text.Substring(startIndex, 1);
                    startIndex++;
                }
            }
            if (!string.IsNullOrWhiteSpace(invalidToken))
            {
                yield return new Token()
                {
                    IsValid = false,
                    Value = invalidToken,
                    Length = invalidToken.Length,
                    Index = startIndex,
                    Line = line,
                    Column = column,
                };
            }
        }

        private void UpdateLineAndColumn(string? value, ref int line, ref int column)
        {
            if (value == null) return;
            var newLines = value.Count(x => x == '\n');
            line += newLines;
            if (newLines > 0)
            {
                var newLineLastIndex = value.LastIndexOf('\n');
                column = value.Length - newLineLastIndex - 1;
            }
            else
            {
                column += value.Length;
            }
        }

        private TokenMatch? FindMatch(string text, int startIndex)
        {
            var processedText = text[startIndex..];
            var matches = new List<TokenMatch>();
            foreach (var tokenDefinition in TokenDefinitions)
            {
                var match = tokenDefinition.Matches(processedText).OrderBy(x => x.Priority);
                matches.AddRange(match);
            }
            return matches.OrderBy(x => x.Priority).FirstOrDefault();
        }
    }

    public class Lexer<TTokenType> where TTokenType : Enum
    {
        public List<TokenDefinition<TTokenType>> TokenDefinitions { get; } = new List<TokenDefinition<TTokenType>>();

        public virtual IEnumerable<Token<TTokenType>> Tokenize(string text)
        {
            int startIndex = 0;
            int line = 1;
            int column = 0;
            string invalidToken = "";
            while (startIndex < text.Length)
            {
                var match = FindMatch(text, startIndex);
                if (match != null)
                {
                    if (invalidToken != "")
                    {
                        yield return new Token<TTokenType>()
                        {
                            IsValid = false,
                            Value = invalidToken,
                            Length = invalidToken.Length,
                            Index = startIndex,
                            Line = line,
                            Column = column,
                        };
                        UpdateLineAndColumn(invalidToken, ref line, ref column);
                        invalidToken = "";
                    }
                    yield return match.ToToken(startIndex, line, column);
                    UpdateLineAndColumn(match.Value, ref line, ref column);
                    startIndex += match.Length;
                }
                else
                {
                    invalidToken += text.Substring(startIndex, 1);
                    startIndex++;
                }
            }
            if (!string.IsNullOrWhiteSpace(invalidToken))
            {
                yield return new Token<TTokenType>()
                {
                    IsValid = false,
                    Value = invalidToken,
                    Length = invalidToken.Length,
                    Index = startIndex,
                    Line = line,
                    Column = column,
                };
            }
        }

        private void UpdateLineAndColumn(string? value, ref int line, ref int column)
        {
            if (value == null) return;
            var newLines = value.Count(x => x == '\n');
            line += newLines;
            if (newLines > 0)
            {
                var newLineLastIndex = value.LastIndexOf('\n');
                column = value.Length - newLineLastIndex - 1;
            }
            else
            {
                column += value.Length;
            }
        }

        private TokenMatch<TTokenType>? FindMatch(string text, int startIndex)
        {
            var processedText = text[startIndex..];
            var matches = new List<TokenMatch<TTokenType>>();
            foreach (var tokenDefinition in TokenDefinitions)
            {
                var match = tokenDefinition.Matches(processedText).OrderBy(x => x.Priority);
                matches.AddRange(match);
            }
            return matches.OrderBy(x => x.Priority).FirstOrDefault();
        }
    }
}
