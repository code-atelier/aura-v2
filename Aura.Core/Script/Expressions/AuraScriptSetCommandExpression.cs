﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptSetCommandExpression : IAuraScriptExpression
    {
        public AuraScriptLiteralExpression? LeftSide { get; set; }

        public IAuraScriptExpression? RightSide { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            if (LeftSide != null && RightSide is AuraScriptNumberWithDurationExpression durationNotation && durationNotation.Number != null)
            {
                var tagName = (LeftSide.Value?.ToString() ?? "t:").Substring(2);
                if (tagName == "") return false;
                if (durationNotation.Number.Eval(context) is double tagValue)
                {
                    TimeSpan? duration = null;
                    if (durationNotation.Duration != null && durationNotation.Duration.Eval(context) is TimeSpan ts)
                    {
                        duration = ts;
                    }

                    if (duration != null)
                        context.SetTag.Add(new MemoryTag(tagName, DateTime.Now + duration, tagValue));
                    else
                        context.SetTag.Add(new MemoryTag(tagName, tagValue));
                }
            }
            return false;
        }

        public override string ToString()
        {
            return (LeftSide?.ToString() ?? "#ref") + " = " + (RightSide?.ToString() ?? "null");
        }
    }
}
