﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public interface IAuraScriptExpression
    {
        object? Eval(AuraScriptContext context, params object?[] parameters);
    }

    public interface IAuraScriptExpression<T> : IAuraScriptExpression
    {
        new T Eval(AuraScriptContext context, params object?[] parameters);
    }
}
