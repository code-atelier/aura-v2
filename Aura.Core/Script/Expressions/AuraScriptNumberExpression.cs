﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptNumberExpression : IAuraScriptExpression
    {
        public IAuraScriptExpression? Value { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            return Value?.Eval(context, parameters);
        }

        public override string ToString()
        {
            return Value?.ToString() ?? "null";
        }
    }
}
