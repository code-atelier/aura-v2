﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptDurationExpression : IAuraScriptExpression
    {
        public AuraScriptLiteralExpression? TimeInterval { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            var str = TimeInterval?.Value?.ToString() ?? "";
            if (string.IsNullOrWhiteSpace(str)) return null;
            var spl = str.Split(' ', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            var res = new TimeSpan();
            foreach (var s in spl)
            {
                try
                {
                    var notation = s.Substring(s.Length - 1);
                    var num = s.Substring(0, s.Length - 1);
                    if (int.TryParse(num, out int val))
                    {
                        if (notation == "f") res = res.Add(new TimeSpan(0, 0, 0, 0, val));
                        if (notation == "s") res = res.Add(new TimeSpan(0, 0, val));
                        if (notation == "m") res = res.Add(new TimeSpan(0, val, 0));
                        if (notation == "h") res = res.Add(new TimeSpan(val, 0, 0));
                        if (notation == "d") res = res.Add(new TimeSpan(val, 0, 0, 0));
                    }
                }
                catch { }
            }
            return res;
        }

        public override string ToString()
        {
            return "for " + (TimeInterval?.ToString() ?? "-");
        }
    }
}
