﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptNumberWithDurationExpression : IAuraScriptExpression
    {
        public AuraScriptNumberExpression? Number { get; set; }

        public AuraScriptDurationExpression? Duration { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            return null;
        }

        public override string ToString()
        {
            return (Number?.ToString() ?? "null") + (Duration != null ? " for " + Duration.ToString() : "");
        }
    }
}
