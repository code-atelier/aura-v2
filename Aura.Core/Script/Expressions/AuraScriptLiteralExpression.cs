﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptLiteralExpression : IAuraScriptExpression
    {
        public object? Value { get; set; }

        public AuraScriptLiteralType Type { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            if (Type == AuraScriptLiteralType.Tag && Value is string tagName)
            {
                tagName = tagName.Substring(2);
                context.ActiveTags.ContainsKey(tagName);
                return context.ActiveTags[tagName];
            }
            if (Type == AuraScriptLiteralType.String)
            {
                var str = Value?.ToString();
                if (str != null && str.Length >= 2)
                {
                    return str.Substring(1, str.Length - 2)
                        .Replace("\\r", "\r")
                        .Replace("\\n", "\n")
                        .Replace("\\t", "\t")
                        .Replace("\\b", "\b");
                }
            }
            return Value;
        }

        public override string? ToString()
        {
            return Value?.ToString();
        }
    }
}
