﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptActionClauseExpression : IAuraScriptExpression
    {
        public IAuraScriptExpression? Expression { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            return Expression?.Eval(context, parameters);
        }

        public override string ToString()
        {
            return Expression?.ToString() ?? "<<No Operation>>";
        }
    }
}
