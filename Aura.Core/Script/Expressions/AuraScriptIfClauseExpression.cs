﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptIfClauseExpression : IAuraScriptExpression
    {
        public IAuraScriptExpression<bool>? Condition { get; set; }

        public List<IAuraScriptExpression> Actions { get; set; } = new List<IAuraScriptExpression>();

        public AuraScriptIfNextExpression? Next { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            var conditionCheck = Condition?.Eval(context, parameters) ?? false;
            if (conditionCheck)
            {
                foreach(var act in Actions)
                {
                    act.Eval(context, parameters);
                }
            }
            else if (Next != null)
            {
                return Next.Eval(context, parameters);
            }
            return null;
        }

        public override string ToString()
        {
            return "if " + (Condition?.ToString() ?? "false") + " then action* " + (Next?.ToString() ?? "");
        }
    }
}
