﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptTriggerClauseExpression : IAuraScriptExpression<bool>
    {
        public string EventIdentifier { get; }

        public bool NotIn { get; set; } = false;

        public List<AuraScriptValueExpression> EventParameters { get; } = new List<AuraScriptValueExpression>();

        public AuraScriptTriggerClauseExpression(string eventIdentifier)
        {
            EventIdentifier = eventIdentifier;
        }

        public bool Eval(AuraScriptContext context, params object?[] parameters)
        {
            bool res;
            if (EventIdentifier == "e:file_dropped" && EventParameters.Count > 0)
            {
                res = EventParameters.Any(x => context.TriggeringEntity.Contains(x.Eval(context)?.ToString() ?? ":null:"));
            }
            else
            {
                res = EventParameters.Any(x => context.TriggeringEntity == x.Eval(context)?.ToString());
            }
            return NotIn ? !res : res;
        }

        object? IAuraScriptExpression.Eval(AuraScriptContext context, params object?[] parameters)
        {
            return Eval(context, parameters);
        }

        public override string? ToString()
        {
            return EventIdentifier + (NotIn ? " not in " : "") + "(" + string.Join(", ", EventParameters.Select(x => x.ToString())) + ")";
        }
    }
}
