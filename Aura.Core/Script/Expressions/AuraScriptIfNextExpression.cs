﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptIfNextExpression : IAuraScriptExpression
    {
        public IAuraScriptExpression<bool>? Condition { get; set; }

        public List<IAuraScriptExpression> Actions { get; set; } = new List<IAuraScriptExpression>();

        public AuraScriptIfNextExpression? Next { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            var conditionCheck = Condition == null ? true : Condition.Eval(context, parameters);
            if (conditionCheck)
            {
                foreach (var act in Actions)
                {
                    act.Eval(context, parameters);
                }
            }
            else if (Next != null)
            {
                return Next.Eval(context, parameters);
            }
            return null;
        }

        public override string ToString()
        {
            string ret;
            if (Actions.Count == 0 && Condition == null)
            {
                ret = "end";
                return ret;
            }
            else if (Condition != null)
            {
                ret = "else if " + Condition.ToString();
            }
            else
            {
                ret = "else";
            }
            ret += " then actions* " + (Next?.ToString() ?? "end");
            return ret;
        }
    }
}
