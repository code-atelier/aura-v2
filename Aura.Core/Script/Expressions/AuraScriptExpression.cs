﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptExpression : IAuraScriptExpression
    {
        public List<AuraScriptTriggerClauseExpression> TriggerClauses { get; } = new List<AuraScriptTriggerClauseExpression>();
        public List<AuraScriptConditionClauseExpression> ConditionClauses { get; } = new List<AuraScriptConditionClauseExpression>();
        public List<AuraScriptActionClauseExpression> ActionClauses { get; } = new List<AuraScriptActionClauseExpression>();

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            var triggered = false;
            foreach (var triggerClause in TriggerClauses)
            {
                if (triggerClause.EventIdentifier == context.Trigger)
                {
                    if (triggerClause.EventParameters.Count == 0 || triggerClause.Eval(context, parameters))
                    {
                        triggered = true;
                        break;
                    }
                }
            }
            if (!triggered)
            {
                return false;
            }

            // check conditions
            if (ConditionClauses.Count > 0)
            {
                foreach (var conditionClause in ConditionClauses)
                {
                    if (!conditionClause.Eval(context))
                        return false;
                }
            }

            foreach (var action in ActionClauses)
            {
                action.Eval(context);
            }
            return true;
        }

        public override string ToString()
        {
            return "Aura Script";
        }
    }

    public enum AuraScriptLiteralType
    {
        String,
        Number,
        Boolean,

        Region,
        Tag,
        Event,

        TimeInterval,
    }

}
