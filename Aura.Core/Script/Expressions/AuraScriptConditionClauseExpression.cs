﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptConditionClauseExpression : IAuraScriptExpression<bool>
    {
        public AuraScriptBooleanOperationExpression? BooleanOperation { get; set; }

        public AuraScriptConditionClauseExpression()
        {
        }

        public bool Eval(AuraScriptContext context, params object?[] parameters)
        {
            return BooleanOperation?.Eval(context, parameters) ?? false;
        }

        object? IAuraScriptExpression.Eval(AuraScriptContext context, params object?[] parameters)
        {
            return Eval(context, parameters);
        }

        public override string ToString()
        {
            return "when(" + BooleanOperation?.ToString() + ")";
        }
    }
}
