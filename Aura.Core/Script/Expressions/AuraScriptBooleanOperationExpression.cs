﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptBooleanOperationExpression : IAuraScriptExpression<bool>
    {
        public IAuraScriptExpression? LeftHand { get; set; }

        public IAuraScriptExpression? RightHand { get; set; }

        public string? Operator { get; set; }

        public bool Eval(AuraScriptContext context, params object?[] parameters)
        {
            if (LeftHand != null && RightHand != null && Operator != null)
            {
                var left = LeftHand.Eval(context, parameters);
                var right = RightHand.Eval(context, parameters);
                if (left is double numL && right is double numR)
                {
                    if (Operator == "<") return numL < numR;
                    if (Operator == ">") return numL > numR;
                    if (Operator == "<=") return numL <= numR;
                    if (Operator == ">=") return numL >= numR;
                    if (Operator == "!=") return numL != numR;
                    if (Operator == "==") return numL == numR;
                }
                switch (Operator)
                {
                    case "!=":
                        return left?.ToString() != right?.ToString();
                    case "==":
                        return left?.ToString() == right?.ToString();
                    default:
                        return false;
                }
            }
            return false;
        }

        object? IAuraScriptExpression.Eval(AuraScriptContext context, params object?[] parameters)
        {
            return Eval(context, parameters);
        }

        public override string ToString()
        {
            return (LeftHand?.ToString() ?? "null") + " " + Operator + " " + (RightHand?.ToString() ?? "null");
        }
    }
}
