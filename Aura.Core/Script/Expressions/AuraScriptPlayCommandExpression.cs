﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script.Expressions
{
    public class AuraScriptPlayCommandExpression : IAuraScriptExpression
    {
        public bool Random { get; set; }

        public List<string> MediaIdentifiers { get; set; } = new List<string>();

        public TimeSpan? Delay { get; set; }

        public object? Eval(AuraScriptContext context, params object?[] parameters)
        {
            if (MediaIdentifiers.Count == 0) return null;
            string id = MediaIdentifiers[0];
            if (Random)
            {
                var rg = new Random();
                id = MediaIdentifiers[rg.Next(MediaIdentifiers.Count)];
            }
            var audio = context.Persona.AudioFiles.Where(x => x.ToString() == id).FirstOrDefault();
            if (audio != null)
            {
                SoundManager.Play(audio);
            }
            return null;
        }

        public override string ToString()
        {
            return "play " + (Random ? "random " : "") + "audio*" + (Delay != null ? " after " + Delay.ToString() : "");
        }
    }
}
