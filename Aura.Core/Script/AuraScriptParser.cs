﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;
using Aura.Script.Expressions;

namespace Aura.Script
{
    public partial class AuraScriptParser
    {
        public const string AuraScriptGrammar =
@"S -> @TRIGGER_CLAUSE+ @CONDITION_CLAUSE* BeginActions @ACTION_CLAUSE*
TRIGGER_CLAUSE -> TriggerClause EventIdentifier In? @VALUE* Terminal
CONDITION_CLAUSE -> ConditionClause @BOOLEAN_OPERATION Terminal
ACTION_CLAUSE -> @SET_COMMAND 
    | @PLAY_COMMAND
    | @IF

SET_COMMAND -> TagIdentifier Assign @NUMBER_WITH_DURATION Terminal
PLAY_COMMAND -> Play AudioIdentifier @DELAY? Terminal
    | Play Random AudioIdentifier+ @DELAY? Terminal

NUMBER_WITH_DURATION -> @NUMBER @DURATION?

IF -> If @BOOLEAN_OPERATION Then @ACTION_CLAUSE* @IF_NEXT
IF_NEXT -> End
    | Else @ACTION_CLAUSE* End
    | ElseIf @BOOLEAN_OPERATION Then @ACTION_CLAUSE* @IF_NEXT

DURATION -> For TimeInterval
DELAY -> After TimeInterval

VALUE -> StringLiteral 
        | NumberLiteral
        | BooleanLiteral
        | RegionIdentifier
        | TagIdentifier
        | EventIdentifier
NUMBER -> NumberLiteral
BOOLEAN -> BooleanLiteral
BOOLEAN_OPERATION -> @VALUE BooleanOperator @VALUE";

        public AuraScriptLexer Lexer { get; } = new AuraScriptLexer();

        private Grammar Grammar { get; } = new Grammar();

        private ParserExpressionConverter<IAuraScriptExpression> ExpressionConverter { get; } = new ParserExpressionConverter<IAuraScriptExpression>();

        public AuraScriptParser()
        {
            var res = Grammar.LoadGrammarDefinition(AuraScriptGrammar);
            Initialize();
        }

        private void Initialize()
        {
            ExpressionConverter.LiteralConstructor((x, t) =>
            {
                var ret = new AuraScriptLiteralExpression();
                ret.Value =
                    t == "NumberLiteral" ? double.Parse(x)
                    : t == "BooleanLiteral" ? x == "true"
                    : x;
                ret.Type =
                    t == "EventIdentifier" ? AuraScriptLiteralType.Event
                    : t == "TagIdentifier" ? AuraScriptLiteralType.Tag
                    : t == "RegionIdentifier" ? AuraScriptLiteralType.Region
                    : t == "NumberLiteral" ? AuraScriptLiteralType.Number
                    : t == "BooleanLiteral" ? AuraScriptLiteralType.Boolean
                    : t == "TimeInterval" ? AuraScriptLiteralType.TimeInterval
                    : AuraScriptLiteralType.String;
                return ret;
            });
            ExpressionConverter.LiteralExpression(new string[] {
                "EventIdentifier",
                "RegionIdentifier",
                "TagIdentifier",
                "NumberLiteral",
                "BooleanLiteral",
                "StringLiteral",
                "TimeInterval"
            });
            ExpressionConverter.Ignore(new string[]
            {
                "BeginActions",
                "For",
            });

            BuildConverters();
        }

        public IAuraScriptExpression? Parse(string script)
        {
            var tokens = Lexer.Tokenize(script);
            var invalid = tokens.Where(x => !x.IsValid).ToList();
            var tokenStream = TokenStream.Create(tokens.Where(x => x.Type != AuraScriptTokenType.Comment));

            var parser = new Parser<Token<AuraScriptTokenType>>();
            var result = parser.Parse(tokenStream, Grammar);
            foreach(var err in invalid)
            {
                result.AddInvalidTokenError(err.Value?.Trim() ?? "", err);
            }
            result.SortEntries();
            if (result.IsSuccess)
            {
                var compiled = parser.GetCompiled();
                return ExpressionConverter.Convert(compiled);
            }
            return null;
        }

        public ParseResult? Check(string script)
        {
            var tokens = Lexer.Tokenize(script);
            var invalid = tokens.Where(x => !x.IsValid).ToList();
            var tokenStream = TokenStream.Create(tokens.Where(x => x.Type != AuraScriptTokenType.Comment));

            var parser = new Parser<Token<AuraScriptTokenType>>();
            var result = parser.Parse(tokenStream, Grammar);
            foreach (var err in invalid)
            {
                result.AddInvalidTokenError(err.Value?.Trim() ?? "", err);
            }
            result.SortEntries();
            return result;
        }
    }
}
