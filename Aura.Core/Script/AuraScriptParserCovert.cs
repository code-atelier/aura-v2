﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using Aura.Script.Expressions;

namespace Aura.Script
{
    public partial class AuraScriptParser
    {
        private void BuildConverters()
        {
            ConvertMainClauses();
            ConvertActions();
            ConvertValues();
            ConvertOperations();
        }

        private void ConvertMainClauses()
        {
            ExpressionConverter.Expression("S").Conversion((x) =>
            {
                var ret = new AuraScriptExpression();
                foreach (var param in x.Parameters)
                {
                    var cvt = ExpressionConverter.Convert(param);
                    if (cvt is AuraScriptTriggerClauseExpression t)
                    {
                        ret.TriggerClauses.Add(t);
                    }
                    if (cvt is AuraScriptConditionClauseExpression c)
                    {
                        ret.ConditionClauses.Add(c);
                    }
                    if (cvt is AuraScriptActionClauseExpression a)
                    {
                        ret.ActionClauses.Add(a);
                    }
                }
                return ret;
            });
            ExpressionConverter.Expression("TRIGGER_CLAUSE").Conversion((x) =>
            {
                var ret = new AuraScriptTriggerClauseExpression(x.Parameters[1].Token?.Value ?? throw new Exception("Trigger clause expects 2nd parameter to be an event identifier"));
                int stIndex = 2;
                if (x.Parameters.Count > 2 && x.Parameters[2].ExpressionType == "In")
                {
                    stIndex = 3;
                    if (x.Parameters[2].Token?.Value == "not in")
                        ret.NotIn = true;
                }
                for (var i = stIndex; i < x.Parameters.Count - 1; i++)
                {
                    ret.EventParameters.Add(ExpressionConverter.Convert<AuraScriptValueExpression>(x.Parameters[i]) ?? throw new Exception("Trigger clause expects following parameters to be values"));
                }
                return ret;
            });
            ExpressionConverter.Expression("CONDITION_CLAUSE").Conversion((x) =>
            {
                var ret = new AuraScriptConditionClauseExpression();
                ret.BooleanOperation = ExpressionConverter.Convert<AuraScriptBooleanOperationExpression>(x.Parameters[1]) ?? throw new Exception("Trigger clause expects 2nd parameter to be boolean operation");
                return ret;
            });
            ExpressionConverter.Expression("ACTION_CLAUSE").Conversion((x) =>
            {
                var ret = new AuraScriptActionClauseExpression();
                ret.Expression = ExpressionConverter.Convert(x.Parameters[0]);
                return ret;
            });

        }

        private void ConvertValues()
        {
            ExpressionConverter.Expression("NUMBER_WITH_DURATION").Conversion(x =>
            {
                var ret = new AuraScriptNumberWithDurationExpression();
                ret.Number = ExpressionConverter.Convert<AuraScriptNumberExpression>(x.Parameters[0])
                ?? throw new Exception($"{ret.GetType().Name} expects number as 1st parameter");
                if (x.Parameters.Count > 1)
                    ret.Duration = ExpressionConverter.Convert<AuraScriptDurationExpression>(x.Parameters[1])
                    ?? throw new Exception($"{ret.GetType().Name} expects time interval as 2nd parameter");
                return ret;
            });
            ExpressionConverter.Expression("NUMBER").Conversion(x =>
            {
                var ret = new AuraScriptNumberExpression();
                ret.Value = ExpressionConverter.Convert(x.Parameters[0])
                    ?? throw new Exception($"{ret.GetType().Name} expects expression as 1st parameter");
                return ret;
            });
            ExpressionConverter.Expression("DURATION").Conversion(x =>
            {
                var ret = new AuraScriptDurationExpression();
                ret.TimeInterval = ExpressionConverter.Convert<AuraScriptLiteralExpression>(x.Parameters[1])
                    ?? throw new Exception($"{ret.GetType().Name} expects time interval as 2nd parameter");
                return ret;
            });

            ExpressionConverter.Expression("VALUE").Conversion((x) =>
            {
                var ret = new AuraScriptValueExpression();
                ret.Value = ExpressionConverter.Convert(x.Parameters.First());
                return ret;
            });
        }

        private void ConvertActions()
        {
            ExpressionConverter.Expression("SET_COMMAND").Conversion(x =>
            {
                var ret = new AuraScriptSetCommandExpression();
                ret.LeftSide = ExpressionConverter.Convert<AuraScriptLiteralExpression>(x.Parameters[0]) ?? throw new Exception($"Left hand side of assignment must be an identifier");
                ret.RightSide = ExpressionConverter.Convert(x.Parameters[2]) ?? throw new Exception($"Right hand side of assignment must be an expression");
                return ret;
            });

            ExpressionConverter.Expression("PLAY_COMMAND").Conversion((x) =>
            {
                var ret = new AuraScriptPlayCommandExpression();
                var stIndex = 1;
                if (x.Parameters[1].ExpressionType == "Random")
                {
                    ret.Random = true;
                    stIndex++;
                }
                for (var i = stIndex; i < x.Parameters.Count - 1; i++)
                {
                    var par = x.Parameters[i];
                    if (par.ExpressionType == "AudioIdentifier" && !string.IsNullOrWhiteSpace(par.Token?.Value))
                    {
                        ret.MediaIdentifiers.Add(par.Token.Value);
                    }
                    else break;
                }
                return ret;
            });

            ExpressionConverter.Expression("IF").Conversion((x) =>
            {
                var ret = new AuraScriptIfClauseExpression();
                if (x.Parameters.Count > 1)
                {
                    ret.Condition = ExpressionConverter.Convert<IAuraScriptExpression<bool>>(x.Parameters[1]) ?? throw new Exception();
                    for (var i = 3; i < x.Parameters.Count - 1; i++)
                    {
                        var cvt = ExpressionConverter.Convert(x.Parameters[i]);
                        if (cvt != null)
                        {
                            ret.Actions.Add(cvt);
                        }
                    }
                    var last = x.Parameters.Last();
                    ret.Next = ExpressionConverter.Convert<AuraScriptIfNextExpression>(last) ?? throw new Exception();
                }

                return ret;
            });

            ExpressionConverter.Expression("IF_NEXT").Conversion((x) =>
            {
                var ret = new AuraScriptIfNextExpression();
                if (x.Parameters.Count > 0)
                {
                    var first = x.Parameters[0].ExpressionType;
                    if (first == "End") return ret;
                    if (first == "ElseIf")
                    {
                        ret.Condition = ExpressionConverter.Convert<IAuraScriptExpression<bool>>(x.Parameters[1]) ?? throw new Exception();
                        for (var i = 3; i < x.Parameters.Count - 1; i++)
                        {
                            var cvt = ExpressionConverter.Convert(x.Parameters[i]);
                            if (cvt != null)
                            {
                                ret.Actions.Add(cvt);
                            }
                        }
                        var last = x.Parameters.Last();
                        ret.Next = ExpressionConverter.Convert<AuraScriptIfNextExpression>(last) ?? throw new Exception();
                    }
                    if (first == "Else")
                    {
                        for(var i = 1; i < x.Parameters.Count - 1; i++)
                        {
                            var cvt = ExpressionConverter.Convert(x.Parameters[i]);
                            if (cvt != null)
                            {
                                ret.Actions.Add(cvt);
                            }
                        }
                    }
                }

                return ret;
            });
        }

        private void ConvertOperations()
        {
            ExpressionConverter.Expression("BOOLEAN_OPERATION").Conversion((x) =>
            {
                var ret = new AuraScriptBooleanOperationExpression();
                ret.LeftHand = ExpressionConverter.Convert(x.Parameters[0]);
                ret.Operator = x.Parameters[1].Token?.Value ?? "==";
                ret.RightHand = ExpressionConverter.Convert(x.Parameters[2]);
                return ret;
            });
        }
    }
}
