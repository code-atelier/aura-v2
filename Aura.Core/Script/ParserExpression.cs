﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class ParserExpression
    {
        public string ExpressionType { get; set; } = string.Empty;

        public List<ParserExpression> Parameters { get; set; } = new List<ParserExpression>();

        public Token? Token { get; set; }

        public void Simplify()
        {
            // if token, ignore
            if (Token != null) return;

            foreach (var par in Parameters)
            {
                par.Simplify();
            }
            var newParams = new List<ParserExpression>();
            foreach (var par in Parameters)
            {
                if (par.ExpressionType.EndsWith("*") || par.ExpressionType.EndsWith("?") || par.ExpressionType.EndsWith("+"))
                {
                    newParams.AddRange(par.Parameters);
                }
                else
                {
                    newParams.Add(par);
                }
            }
            Parameters = newParams;
        }
    }
}
