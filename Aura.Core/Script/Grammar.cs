﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Aura.Script
{
    public class Grammar
    {
        public GrammarRuleSet RuleSet { get; } = new GrammarRuleSet();

        public ParseResult LoadGrammarDefinition<TTokenType>(string text) where TTokenType : Enum
        {
            return LoadGrammarDefinition(text, typeof(TTokenType));
        }

        public ParseResult LoadGrammarDefinition(string text, Type? tokenTypeEnum = null)
        {
            var res = new ParseResult();
            RuleSet.Clear();
            var lexer = new GrammarLexer();
            var tokens = lexer.Tokenize(text).ToList();
            foreach (var error in tokens.Where(x => !x.IsValid))
            {
                res.AddError($"Unexpected '{error.Type}'", error);
            }
            tokens.RemoveAll(x => !x.IsValid || x.Type == GrammarTokenType.Comment);
            tokens.Add(new Token<GrammarTokenType>()
            {
                IsValid = true,
                Type = GrammarTokenType.NewLine,
                Column = -1,
                Line = -1,
                Length = 0,
                Index = -1,
            });

            var tokenStream = TokenStream.Create(tokens);
            var section = 0;
            GrammarRules? currentRule = null;
            while (tokenStream.HasToken)
            {
                var currentToken = tokenStream.Consume();
                var nextToken = tokenStream.TryPeek();
                var nextNonNewLineToken = tokenStream.Next(x => x.Type != GrammarTokenType.NewLine);

                // section 0 : Rule identifier
                if (section == 0)
                {
                    if (currentToken.Type == GrammarTokenType.NewLine) continue; // ignore empty line
                    if (currentToken.Type != GrammarTokenType.Identifier) // expecting a rule name, but found none
                    {
                        res.AddError($"Expecting '{GrammarTokenType.Identifier}', found '{currentToken.Type}' instead", currentToken);
                        goto skip;
                    }
                    else if (string.IsNullOrWhiteSpace(currentToken.Value))
                    {
                        res.AddError($"'{GrammarTokenType.Identifier}' cannot be an empty string", currentToken);
                        goto skip;
                    }
                    else
                    {
                        var alreadyExisted = RuleSet.ContainsKey(currentToken.Value);
                        currentRule = RuleSet.Rule(currentToken.Value, new GrammarToken()
                        {
                            Column = currentToken.Column,
                            Line = currentToken.Line,
                            Length = currentToken.Length,
                            Value = currentToken.Value,
                            Kind = GrammarTokenKind.Terminal
                        });
                        if (alreadyExisted) currentRule.Alternate(); // this must be an alternate rule of existing rule
                        section = 1;
                        continue;
                    }
                }

                // section 1 : Assigner
                if (section == 1)
                {
                    if (currentToken.Type != GrammarTokenType.RuleDescriptor) // expecting a -> but found none
                    {
                        res.AddError($"Expecting '{GrammarTokenType.RuleDescriptor}', but found '{currentToken.Type}' instead", currentToken);
                        goto skip;
                    }
                    else
                    {
                        section = 2;
                        continue;
                    }
                }

                // section 2 : Descriptor
                if (section == 2)
                {
                    if (currentRule == null)
                    {
                        goto skip;
                    }
                    // should only accept identifier, quantitizer, alternate, or newline followed by alternate
                    else if (currentToken.Type == GrammarTokenType.Identifier || currentToken.Type == GrammarTokenType.AlternateRule || currentToken.Type == GrammarTokenType.Quantitizer || currentToken.Type == GrammarTokenType.Variable)
                    {
                        currentRule.Add(currentToken);
                    }
                    else if (currentToken.Type == GrammarTokenType.NewLine && nextNonNewLineToken?.Type == GrammarTokenType.AlternateRule)
                    {
                        // skip newlines until before alternate rule and continue
                        while (tokenStream.TryPeek() is Token<GrammarTokenType> nToken && nToken.Type == GrammarTokenType.NewLine)
                        {
                            tokenStream.Consume();
                        }
                        continue;
                    }
                    else if (currentToken.Type == GrammarTokenType.NewLine)
                    {
                        // otherwise, the rule is completed
                        section = 0;
                        currentRule = null;
                        currentToken = null;
                        continue;
                    }
                    else
                    {
                        res.AddError($"Unexpected '{currentToken.Type}' was found", currentToken);
                        goto skip;
                    }
                }
                continue;

                skip:
                    while (tokenStream.TryPeek() is Token<GrammarTokenType> nToken && nToken.Type != GrammarTokenType.NewLine)
                    {
                        tokenStream.Consume();
                    }
                    if (tokenStream.HasToken)
                        tokenStream.Consume(); // consume the new line
                    currentRule = null;
                    section = 0; // reset section
            }

            if (!res.IsSuccess)
            {
                RuleSet.Clear();
                return res;
            }

            CheckRules(res, tokenTypeEnum);
            if (!res.IsSuccess)
            {
                RuleSet.Clear();
            }
            res.SortEntries();
            return res;
        }

        private void CheckRules(ParseResult res, Type? tokenTypeEnum)
        {
            // error: it must have S (Start)
            if (!RuleSet.ContainsKey("S"))
            {
                res.AddGlobalError("Entry point rule is missing. It is denoted by a rule named 'S'");
            }

            // error: all pointers must have a rule
            var pointers = RuleSet.SelectMany(x => x.Value.SelectMany(y => y.Tokens.Where(z => z.Kind == GrammarTokenKind.NonTerminal).Select(z => (z.Value ?? "@").Substring(1)))).Distinct().ToList();
            var pointerTokens = RuleSet.SelectMany(x => x.Value.SelectMany(y => y.Tokens.Where(z => z.Kind == GrammarTokenKind.NonTerminal)));
            foreach (var pointer in pointerTokens)
            {
                if (!RuleSet.ContainsKey((pointer.Value ?? "@").Substring(1)))
                {
                    res.AddError($"Rule '{pointer.Value?.TrimStart('@')}' is not found.", pointer);
                }
            }

            // error: rule cannot be ambiguous
            // 1 - cannot self reference on first component
            foreach(var kv in RuleSet)
            {
                var ruleRef = "@" + kv.Key;
                foreach(var rule in kv.Value)
                {
                    if (rule.Tokens.Count > 0 && 
                        rule.Tokens.First().Kind == GrammarTokenKind.NonTerminal &&
                        rule.Tokens.First().Value == ruleRef)
                    {
                        res.AddError($"Rule '{kv.Key + " -> " + rule.ToString()}' is ambiguous. A rule cannot self reference on the first component.", rule.Tokens.First());
                    }
                }
            }

            // warning: rule is not used
            foreach (var key in RuleSet.Keys)
            {
                if (key == "S")
                {
                    continue;
                }
                if (!pointers.Contains(key))
                {
                    var rules = RuleSet[key];
                    res.AddWarning($"Rule '{key}' was never used.", new Token()
                    {
                        Column = rules.IdentifierToken?.Column ?? -1,
                        Length = rules.IdentifierToken?.Length ?? -1,
                        Line = rules.IdentifierToken?.Line ?? -1,
                        Value = rules.Identifier,
                    });
                }
            }

            // warning: terminal x tokentype matcher check
            if (tokenTypeEnum != null && tokenTypeEnum.IsEnum)
            {
                var identifiers = RuleSet.SelectMany(x => x.Value.SelectMany(y => y.Tokens.Where(z => z.Kind == GrammarTokenKind.Terminal))).ToList();
                var enumValues = Enum.GetValues(tokenTypeEnum);
                var enumStrings = new List<string>();
                foreach (var val in enumValues)
                {
                    if (val != null && val.ToString() is string str)
                        enumStrings.Add(str);
                }
                foreach (var identifier in identifiers)
                {
                    if (identifier.Value != null && !enumStrings.Contains(identifier.Value))
                    {
                        res.AddWarning($"'{identifier.Value}' is not a recognized token type in '{tokenTypeEnum.Name}'", identifier);
                    }
                }
            }
        }

        public ParseResult CheckRules(Type? tokenTypeEnum = null)
        {
            var result = new ParseResult();
            CheckRules(result, tokenTypeEnum);
            return result;
        }

        public ParseResult CheckRules<TTokenType>() where TTokenType : Enum
        {
            var result = new ParseResult();
            CheckRules(result, typeof(TTokenType));
            return result;
        }

        public string BakeRuleset()
        {
            // optimize
            var clone = RuleSet.Clone();

            // eliminate unused rules
            var pointers = clone.SelectMany(x => x.Value.SelectMany(y => y.Tokens.Where(z => z.Kind == GrammarTokenKind.NonTerminal).Select(z => (z.Value ?? "@").Substring(1)))).Distinct().ToList();
            var shouldRemove = new List<string>();
            foreach (var key in clone.Keys)
            {
                if (key == "S")
                {
                    continue;
                }
                if (!pointers.Contains(key))
                {
                    shouldRemove.Add(key);
                }
            }

            // remove rules that can be removed
            foreach (var key in shouldRemove)
            {
                clone.Remove(key);
            }

            return clone.ToString();
        }
    }
}
