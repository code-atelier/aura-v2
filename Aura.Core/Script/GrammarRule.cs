﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class GrammarRule
    {
        public List<GrammarToken> Tokens { get; } = new List<GrammarToken>();

        public override string ToString()
        {
            return string.Join(" ", Tokens.Select(x => x.Value));
        }

        public GrammarRule Clone()
        {
            var res = new GrammarRule();
            res.Tokens.AddRange(Tokens.Select(x => x.Clone()));
            return res;
        }
    }
}
