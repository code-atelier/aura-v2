﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class AuraScriptContext
    {
        public Persona Persona { get; set; }

        public WeightedTagCollection ActiveTags { get; set; } = new WeightedTagCollection();

        public string Trigger { get; set; } = string.Empty;

        public string TriggeringEntity { get; set; } = string.Empty;

        public MemoryTagCollection SetTag { get; set; } = new MemoryTagCollection();

        public AuraScriptContext(Persona persona)
        {
            Persona = persona;
        }
    }
}
