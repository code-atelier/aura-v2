﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class GrammarToken
    {
        public GrammarTokenKind Kind { get; set; } = GrammarTokenKind.Return;

        public string Value { get; set; } = string.Empty;

        public int Line { get; set; }

        public int Column { get; set; }

        public int Length { get; set; }

        public int Index { get; set; }

        public static GrammarToken From(Token token, GrammarTokenKind kind)
        {
            return new GrammarToken()
            {
                Value = token.Value ?? throw new ArgumentNullException(nameof(token.Value)),
                Kind = kind,
                Line = token.Line,
                Index = token.Index,
                Column = token.Column,
                Length = token.Length
            };
        }
        public GrammarToken Clone()
        {
            var res = new GrammarToken()
            {
                Kind = Kind,
                Value = Value,
                Line = Line,
                Index = Index,
                Column = Column,
                Length = Length,
            };
            return res;
        }
    }

    public enum GrammarTokenKind
    {
        Terminal,
        NonTerminal,
        Return
    }
}
