﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class Token
    {
        public bool IsValid { get; set; } = true;

        public int Line { get; set; }

        public int Column { get; set; }

        public int Index { get; set; }

        public int Length { get; set; }

        public string? Value { get; set; }

        public virtual string GetTokenType()
        {
            return string.Empty;
        }

        public virtual bool IsEndOfFile()
        {
            return false;
        }
    }

    public class EndOfFileToken : Token
    {
        public override bool IsEndOfFile()
        {
            return true;
        }
    }

    public class Token<TTokenType> : Token where TTokenType : Enum
    {
        public TTokenType? Type { get; set; }

        public override string GetTokenType()
        {
            return Type?.ToString() ?? "";
        }
    }
}
