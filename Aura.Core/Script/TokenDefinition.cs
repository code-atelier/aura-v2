﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class TokenDefinition: ITokenDefinition
    {
        public Regex Regex { get; protected set; }

        public int Priority { get; set; }

        public TokenDefinition(string pattern, int priority = 0)
        {
            Regex = new Regex("^" + pattern, RegexOptions.Compiled | RegexOptions.Singleline);
            Priority = priority;
        }

        public TokenDefinition(Regex regex, int priority = 0)
        {
            Regex = regex;
            Priority = priority;
        }

        public IEnumerable<TokenMatch> Matches(string input)
        {
            var matches = Regex.Matches(input);
            foreach (var match in matches.OfType<Match>()) {
                if (match.Success)
                {
                    yield return new TokenMatch()
                    {
                        IsValid = true,
                        Length = match.Length,
                        Value = match.Value,
                        Priority = Priority,
                    };
                }
            }
        }

        public static TokenDefinition Create(string pattern, int priority = 0)
        {
            return new TokenDefinition(pattern, priority);
        }

        public static TokenDefinition<T> Create<T>(T tokenType, string pattern, int priority = 0) where T: Enum
        {
            return new TokenDefinition<T>(tokenType, pattern, priority);
        }
    }

    public class TokenDefinition<TTokenType>: TokenDefinition, ITokenDefinition<TTokenType> where TTokenType : Enum
    {
        public TTokenType TokenType { get; protected set; }

        public TokenDefinition(TTokenType tokenType, string pattern, int priority = 0) : base(pattern, priority)
        {
            TokenType = tokenType;
        }

        public TokenDefinition(TTokenType tokenType, Regex regex, int priority = 0) : base(regex, priority)
        {
            TokenType = tokenType;
        }

        public new IEnumerable<TokenMatch<TTokenType>> Matches(string input)
        {
            var tokenMatches = base.Matches(input);
            foreach (var tokenMatch in tokenMatches)
            {
                yield return new TokenMatch<TTokenType>()
                {
                    IsValid = tokenMatch.IsValid,
                    Value = tokenMatch.Value,
                    Length = tokenMatch.Length,
                    Priority = Priority,
                    Type = TokenType
                };
            }
        }
    }
}