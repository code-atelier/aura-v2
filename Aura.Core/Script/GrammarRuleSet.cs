﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class GrammarRuleSet : Dictionary<string, GrammarRules>
    {
        public GrammarRules Rule(string identifier, GrammarToken? grammarToken = null)
        {
            if (identifier.StartsWith("@"))
            {
                identifier = identifier.Substring(1);
            }
            GrammarRules rules;
            if (ContainsKey(identifier))
            {
                rules = this[identifier];
            }
            else
            {
                rules = new GrammarRules(this)
                {
                    Identifier = identifier,
                    IdentifierToken = grammarToken,
                };
                this[identifier] = rules;
            }
            return rules;
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine, Values.Select(x => x.ToString()));
        }

        public GrammarRuleSet Clone()
        {
            var res = new GrammarRuleSet();
            foreach(var kv in this)
            {
                res[kv.Key] = kv.Value.Clone();
            }
            return res;
        }
    }

    public class GrammarRules : IEnumerable<GrammarRule>
    {
        public string Identifier { get; set; } = string.Empty;

        public GrammarToken? IdentifierToken { get; set; }

        public List<GrammarRule> Rules { get; } = new List<GrammarRule>();

        public GrammarRuleSet? RuleSet { get; }

        private GrammarRule currentRule;

        private GrammarToken? lastToken;

        public GrammarRules()
        {
            currentRule = new GrammarRule();
            Rules.Add(currentRule);
        }

        public GrammarRules(GrammarRuleSet ruleSet)
        {
            currentRule = new GrammarRule();
            Rules.Add(currentRule);
            RuleSet = ruleSet;
        }

        public void Alternate()
        {
            lastToken = null;
            currentRule = new GrammarRule();
            Rules.Add(currentRule);
        }

        public void Add(GrammarToken token)
        {
            lastToken = token;
            currentRule.Tokens.Add(token);
        }

        public void Add(Token<GrammarTokenType> token)
        {
            if (token.Type == GrammarTokenType.Identifier || token.Type == GrammarTokenType.Variable)
            {
                lastToken = GrammarToken.From(token, token.Type == GrammarTokenType.Identifier ? GrammarTokenKind.Terminal : GrammarTokenKind.NonTerminal);
                currentRule.Tokens.Add(lastToken);
                return;
            }

            if (token.Type == GrammarTokenType.AlternateRule)
            {
                Alternate();
                return;
            }

            if (token.Type == GrammarTokenType.Quantitizer)
            {
                if (RuleSet == null) throw new Exception("Cannot use quantitizer on rule without ruleset");
                if (lastToken == null) throw new Exception("Cannot quantitize symbol token");
                currentRule.Tokens.Remove(lastToken);
                // zero or one
                if (token.Value == "?")
                {
                    // create new rule
                    var ruleName = lastToken.Value.Trim('@') + "?";
                    if (!RuleSet.ContainsKey(ruleName))
                    {
                        var rule = RuleSet.Rule(ruleName);
                        // A? -> A
                        rule.Add(new GrammarToken()
                        {
                            Kind = lastToken.Kind,
                            Value = lastToken.Value,
                        });
                        // A? ->
                        rule.Alternate();
                    }
                    Add(new GrammarToken()
                    {
                        Kind = GrammarTokenKind.NonTerminal,
                        Value = "@" + ruleName,
                    });
                }
                // zero or many
                if (token.Value == "*")
                {
                    // create new rule
                    var ruleName = lastToken.Value.Trim('@') + "*";
                    if (!RuleSet.ContainsKey(ruleName))
                    {
                        var rule = RuleSet.Rule(ruleName);
                        // A* -> A A*
                        rule.Add(new GrammarToken()
                        {
                            Kind = lastToken.Kind,
                            Value = lastToken.Value,
                        });
                        rule.Add(new GrammarToken()
                        {
                            Kind = GrammarTokenKind.NonTerminal,
                            Value = "@" + ruleName,
                        });
                        // A* ->
                        rule.Alternate();
                    }
                    Add(new GrammarToken()
                    {
                        Kind = GrammarTokenKind.NonTerminal,
                        Value = "@" + ruleName,
                    });
                }
                // one or many
                if (token.Value == "+")
                {
                    // create new rule
                    var ruleName = lastToken.Value.Trim('@') + "+";
                    var ruleNameNext = ruleName + "+";
                    if (!RuleSet.ContainsKey(ruleName))
                    {
                        var rule = RuleSet.Rule(ruleName);
                        var ruleNext = RuleSet.Rule(ruleNameNext);
                        // A+ -> A A++
                        rule.Add(new GrammarToken()
                        {
                            Kind = lastToken.Kind,
                            Value = lastToken.Value,
                        });
                        rule.Add(new GrammarToken()
                        {
                            Kind = GrammarTokenKind.NonTerminal,
                            Value = "@" + ruleNameNext,
                        });
                        // A++ -> A A++
                        ruleNext.Add(new GrammarToken()
                        {
                            Kind = lastToken.Kind,
                            Value = lastToken.Value,
                        });
                        ruleNext.Add(new GrammarToken()
                        {
                            Kind = GrammarTokenKind.NonTerminal,
                            Value = "@" + ruleNameNext,
                        });
                        // A++ ->
                        ruleNext.Alternate();
                    }
                    Add(new GrammarToken()
                    {
                        Kind = GrammarTokenKind.NonTerminal,
                        Value = "@" + ruleName,
                    });
                }
            }

            lastToken = null;
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine, Rules.Select(x => Identifier + " -> " + x.ToString()));
        }

        public IEnumerator<GrammarRule> GetEnumerator()
        {
            return Rules.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public GrammarRules Clone()
        {
            var res = new GrammarRules()
            {
                Identifier = Identifier,
                IdentifierToken = IdentifierToken,
            };
            res.Rules.Clear();
            res.Rules.AddRange(Rules.Select(x => x.Clone()));
            res.currentRule = res.Rules.Last();
            return res;
        }
    }
}
