﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public static class TokenStream
    {
        public static TokenStream<T> Create<T>(IEnumerable<T> tokens)
        {
            return new TokenStream<T>(tokens);
        }
    }

    public class TokenStream<TToken>
    {
        List<TToken> tokens;

        public TokenStream(IEnumerable<TToken> tokens) {
            this.tokens = new List<TToken>();
            this.tokens.AddRange(tokens);
        }

        public TToken Peek()
        {
            return this.tokens[0];
        }

        public TToken? TryPeek()
        {
            return HasToken ? this.tokens[0] : default;
        }

        public IEnumerable<TToken> Peek(int count)
        {
            if (count < 1) return new List<TToken>();
            var res = new List<TToken>();
            for(var i = 0; i < count; i++)
            {
                res.Add(this.tokens[i]);
            }
            return res;
        }

        public TToken? Next(Func<TToken, bool> predicate)
        {
            foreach(var token in this.tokens)
            {
                if (predicate.Invoke(token)) return token;
            }
            return default;
        }

        public bool HasToken { get => this.tokens.Count > 0; }

        public TToken Consume()
        {
            var token = this.tokens[0];
            tokens.RemoveAt(0);
            return token;
        }
    }
}
