﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Script
{
    public class ParseResult
    {
        public List<ParseResultEntry> Entries { get; } = new List<ParseResultEntry>();

        public bool IsSuccess { get => Errors == 0; }

        public int Errors { get => Entries.Count(x => x.Type == ParseResultEntryType.Error); }

        public void AddEntry(ParseResultEntry entry)
        {
            Entries.Add(entry);
        }

        public void AddMessage(string message, GrammarToken token)
        {
            Entries.Add(new ParseResultEntry()
            {
                Column = token.Column,
                Message = message,
                Line = token.Line,
                Index = token.Index,
                Length = token.Length,
                Type = ParseResultEntryType.Message,
            });
        }

        public void AddMessage(string message, Token token)
        {
            Entries.Add(new ParseResultEntry()
            {
                Column = token.Column,
                Message = message,
                Line = token.Line,
                Index = token.Index,
                Length = token.Length,
                Type = ParseResultEntryType.Message,
            });
        }

        public void AddWarning(string message, GrammarToken token)
        {
            Entries.Add(new ParseResultEntry()
            {
                Column = token.Column,
                Message = message,
                Line = token.Line,
                Index = token.Index,
                Length = token.Length,
                Type = ParseResultEntryType.Warning,
            });
        }

        public void AddWarning(string message, Token token)
        {
            Entries.Add(new ParseResultEntry()
            {
                Column = token.Column,
                Message = message,
                Line = token.Line,
                Index = token.Index,
                Length = token.Length,
                Type = ParseResultEntryType.Warning,
            });
        }

        public void AddError(string message, GrammarToken token)
        {
            Entries.Add(new ParseResultEntry()
            {
                Column = token.Column,
                Message = message,
                Line = token.Line,
                Index = token.Index,
                Length = token.Length,
                Type = ParseResultEntryType.Error,
            });
        }

        public void AddError(string message, Token token)
        {
            Entries.Add(new ParseResultEntry()
            {
                Column = token.Column,
                Message = message,
                Line = token.Line,
                Length = token.Length,
                Index = token?.Index,
                Type = ParseResultEntryType.Error,
            });
        }

        public void AddGlobalMessage(string message)
        {
            Entries.Add(new ParseResultEntry()
            {
                Message = message,
                Type = ParseResultEntryType.Message,
            });
        }

        public void AddGlobalWarning(string message)
        {
            Entries.Add(new ParseResultEntry()
            {
                Message = message,
                Type = ParseResultEntryType.Warning,
            });
        }

        public void AddGlobalError(string message)
        {
            Entries.Add(new ParseResultEntry()
            {
                Message = message,
                Type = ParseResultEntryType.Error,
            });
        }

        public void AddParserError(string message)
        {
            Entries.Add(new ParseResultEntry()
            {
                Message = "A parser error has occured: " + message,
                Type = ParseResultEntryType.Error,
            });
        }

        public void SortEntries()
        {
            Entries.Sort((a, b) => a.Type.CompareTo(b.Type) is int cType && cType != 0 ? cType * -1 : (a.Line ?? -1).CompareTo(b.Line ?? -1) is int line && line != 0 ? line : (a.Column ?? -1).CompareTo(b.Column ?? -1));
        }

        public void AddExpectButFoundError(string expecting, string found, Token? token = null)
        {
            Entries.Add(new ParseResultEntry()
            {
                Column = token?.Column,
                Message = $"Expecting '{expecting}', but found '{found}' instead",
                Line = token?.Line,
                Length = token?.Length,
                Index = token?.Index,
                Type = ParseResultEntryType.Error,
            });
        }
        public void AddInvalidTokenError(string found, Token? token = null)
        {
            Entries.Add(new ParseResultEntry()
            {
                Column = token?.Column,
                Message = $"Invalid token '{found}'",
                Line = token?.Line,
                Length = token?.Length,
                Index = token?.Index,
                Type = ParseResultEntryType.Error,
            });
        }
    }

    public class ParseResultEntry
    {
        public int? Line { get; set; }

        public int? Column { get; set; }

        public int? Length { get; set; }

        public int? Index { get; set; }

        public ParseResultEntryType Type { get; set; }

        public string Message { get; set; } = string.Empty;

        public override string ToString()
        {
            var suffix = "";
            if (Line != null && Column != null)
            {
                suffix = $" at line {Line} column {Column}";
            }
            return Message + suffix;
        }
    }

    public enum ParseResultEntryType
    {
        Message,
        Warning,
        Error,
    }
}
