﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura
{
    public interface ITool
    {
        ExtensionInfo Info { get; }

        ToolKind Kind { get; }

        string Icon { get; }

        void Activate();

        void Show();

        void Close();
    }

    public enum ToolKind
    {
        Tool = 0,
        Widget = 1,
    }

    public abstract class ToolBase : ITool
    {
        public abstract ExtensionInfo Info { get; }

        public ToolKind Kind { get; } = ToolKind.Tool;

        protected Window? Window { get; private set; }
        public abstract string Icon { get; }

        public void Activate()
        {
            if (Window != null)
            {
                Window.Activate();
            }
        }

        public void Close()
        {
            Window?.Close();
            Window = null;
        }

        public void Show()
        {
            if (Window != null)
            {
                try
                {
                    if (Window.WindowState == WindowState.Minimized)
                    {
                        Window.WindowState = WindowState.Normal;
                    }
                    Window.Activate();
                }
                catch { }
            }
            else
            {
                Window = CreateToolWindow();
                Window.Closed += Window_Closed;
                Window.Show();
            }
        }

        private void Window_Closed(object? sender, EventArgs e)
        {
            if (sender is Window window && window == Window)
            {
                Window = null;
            }
        }

        protected abstract Window CreateToolWindow();
    }

    public abstract class WidgetBase : ITool
    {
        public abstract ExtensionInfo Info { get; }

        public ToolKind Kind { get; } = ToolKind.Widget;

        protected List<Window> Windows { get; } = new List<Window>();
        public abstract string Icon { get; }

        public void Activate()
        {
            foreach (var win in Windows)
            {
                try
                {
                    win.Activate();
                }
                catch { }
            }
        }

        public void Close()
        {
            foreach(var win in Windows)
            {
                try
                {
                    win.Close();
                }
                catch { }
            }
            Windows.Clear();
        }

        public void Show()
        {
            var window = CreateWidgetWindow();
            window.Closed += Window_Closed;
            window.Show();
        }

        private void Window_Closed(object? sender, EventArgs e)
        {
            if (sender is Window window && Windows.Contains(window))
            {
                Windows.Remove(window);
            }
        }

        protected abstract Window CreateWidgetWindow();
    }
}
