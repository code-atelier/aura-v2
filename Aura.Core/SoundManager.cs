﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Aura
{
    public static class SoundManager
    {
        public static Action<AudioFile>? PlayAudio { get; set; }

        public static async void Play(AudioFile audioFile)
        {
            await Task.Run(() =>
            {
                PlayAudio?.Invoke(audioFile);
            });
        }
    }
}
