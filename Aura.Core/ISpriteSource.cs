﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Aura
{
    public interface ISpriteSource
    {
        Sprite? GetSprite(Guid spriteId);

        ImageSource? GetSpriteAsImageSource(Guid spriteId);
    }
}
