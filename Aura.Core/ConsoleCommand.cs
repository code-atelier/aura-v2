﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aura
{
    public class ConsoleCommand
    {
        public string RawCommand { get; set; } = string.Empty;

        public string Command { get; set; } = string.Empty;

        public List<ConsoleCommandPart> Parts { get; } = new List<ConsoleCommandPart>();

        public static ConsoleCommand Parse(string command)
        {
            var res = new ConsoleCommand();
            res.RawCommand = command;
            var spl = command.Split(' ', 2, StringSplitOptions.None);
            res.Command = spl[0].ToLower().Trim();

            ConsoleCommandPart? currentParam = null;

            if (spl.Length > 1)
            {
                var qChars = new Queue<char>();
                foreach (var c in spl[1].Trim())
                {
                    qChars.Enqueue(c);
                }

                /*
                 0 - initial
                 1 - after - or /
                 3 - after = or :
                 4 - after = or : then quote
                 5 - after = or :, in quote, after escape
                 11 - in quote
                 12 - after escape
                 */
                int state = 0;
                char currentChar;
                string buffer = string.Empty;
                while (qChars.TryDequeue(out currentChar))
                {
                    // initial state
                    if (state == 0)
                    {
                        if (currentChar == '"')
                        {
                            state = 11;
                        }
                        else if (currentChar == '-' || currentChar == '/')
                        {
                            state = 1;
                        }
                        else if (currentChar == ' ')
                        {
                            if (!string.IsNullOrWhiteSpace(buffer))
                            {
                                res.Parts.Add(new ConsoleCommandPart() { Value = buffer });
                                buffer = string.Empty;
                            }
                        }
                        else
                        {
                            buffer += currentChar;
                        }
                        continue;
                    }

                    // expect param name
                    if (state == 1)
                    {
                        if (currentChar == '=' || currentChar == ':')
                        {
                            currentParam = new ConsoleCommandPart()
                            {
                                ParameterName = buffer,
                            };
                            buffer = string.Empty;
                            state = 3;
                        }
                        else if (currentChar == ' ')
                        {
                            if (!string.IsNullOrWhiteSpace(buffer))
                            {
                                res.Parts.Add(new ConsoleCommandPart() { ParameterName = buffer });
                            }
                            buffer = string.Empty;
                            state = 0;
                        }
                        else
                        {
                            buffer += currentChar;
                        }
                        continue;
                    }

                    // expect param value
                    if (state == 3)
                    {
                        if (buffer == "" && currentChar == '"')
                        {
                            state = 4;
                        }
                        else if (currentChar == ' ')
                        {
                            if (currentParam != null)
                            {
                                currentParam.Value = buffer;
                                res.Parts.Add(currentParam);
                                buffer = string.Empty;
                            }
                            state = 0;
                        }
                        else
                        {
                            buffer += currentChar;
                        }
                        continue;
                    }

                    // param value - in quote
                    if (state == 4)
                    {
                        if (currentChar == '\\')
                        {
                            state = 5;
                        }
                        else if (currentChar == '"')
                        {
                            if (currentParam != null)
                            {
                                currentParam.Value = buffer;
                                res.Parts.Add(currentParam);
                                buffer = string.Empty;
                            }
                            state = 0;
                        }
                        else
                        {
                            buffer += currentChar;
                        }
                        continue;
                    }

                    // param value - in quote after escape
                    if (state == 5)
                    {
                        if (currentChar == 'n')
                        {
                            buffer += '\n';
                        }
                        if (currentChar == 'r')
                        {
                            buffer += '\r';
                        }
                        if (currentChar == 't')
                        {
                            buffer += '\t';
                        }
                        else
                        {
                            buffer += currentChar;
                        }
                        state = 4;
                        continue;
                    }

                    // free text
                    if (state == 11)
                    {
                        if (currentChar == '\\')
                        {
                            state = 12;
                        }
                        else if (currentChar == '"')
                        {
                            res.Parts.Add(new ConsoleCommandPart()
                            {
                                Value = buffer,
                            });
                            buffer = string.Empty;
                            state = 0;
                        }
                        else
                        {
                            buffer += currentChar;
                        }
                        continue;
                    }

                    // free text after escape
                    if (state == 12)
                    {
                        if (currentChar == 'n')
                        {
                            buffer += '\n';
                        }
                        if (currentChar == 'r')
                        {
                            buffer += '\r';
                        }
                        if (currentChar == 't')
                        {
                            buffer += '\t';
                        }
                        else
                        {
                            buffer += currentChar;
                        }
                        state = 11;
                        continue;
                    }
                }

                if (!string.IsNullOrWhiteSpace(buffer))
                {
                    if (state == 0)
                    {
                        res.Parts.Add(new ConsoleCommandPart() { Value = buffer });
                    }
                    if (state == 1)
                    {
                        res.Parts.Add(new ConsoleCommandPart() { ParameterName = buffer });
                    }
                    if ((state == 3 || state == 4 || state == 5) && currentParam != null)
                    {
                        currentParam.Value = buffer;
                        res.Parts.Add(currentParam);
                    }
                    if (state == 11 || state == 12)
                    {
                        res.Parts.Add(new ConsoleCommandPart()
                        {
                            Value = buffer,
                        });
                    }
                }
                else
                {
                    if (state == 3 && currentParam != null)
                    {
                        res.Parts.Add(currentParam);
                    }
                }
            }

            return res;
        }

        public string? Switch(string switchName)
        {
            var part = Parts.FirstOrDefault(x => x.ParameterName?.ToLower() == switchName.ToLower());
            if (part != null)
            {
                return part.Value ?? "";
            }
            return null;
        }

        public string? Value(int index)
        {
            return Parts.Where(x => x.ParameterName == null).ElementAtOrDefault(index)?.Value;
        }
    }

    public class ConsoleCommandPart
    {
        public string? ParameterName { get; set; }

        public string Value { get; set; } = string.Empty;
    }
}
