﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.WPF
{
    public interface IFileDropMenu: ICommandWindow
    {
        ExtensionInfo Info { get; }

        void LoadFiles(IEnumerable<string> files);

        void LoadHandlers(IEnumerable<FileDropHandler> handlers);

        void Close();

        event FileDropExecuteRequestEventHandler? OnFileDropExecute;
    }

    public delegate void FileDropExecuteRequestEventHandler(object sender, FileDropHandler handler, IEnumerable<string> files);
}
