﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.WPF
{
    public interface ICommandWindow
    {
        void Show(string anchor, double anchorX, double anchorY);
    }
}
