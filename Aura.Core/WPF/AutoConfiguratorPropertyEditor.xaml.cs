﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for AutoConfiguratorPropertyEditor.xaml
    /// </summary>
    public partial class AutoConfiguratorPropertyEditor : UserControl
    {
        public string Title { get => lbTitle.Content?.ToString() ?? ""; set => lbTitle.Content = value; }
        public string Description
        {
            get => lbDescription.Content?.ToString() ?? "";
            set
            {
                lbDescription.Content = value;
                lbDescription.Visibility = string.IsNullOrWhiteSpace(value) ? Visibility.Collapsed : Visibility.Visible;
            }
        }
        string? iconPath = null;
        public string? IconPath
        {
            get => iconPath;
            set
            {
                try
                {
                    if (value != null)
                    {
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.UriSource = new Uri(value);
                        bmp.EndInit();
                        imgIcon.Source = bmp;
                        iconPath = value;
                    }
                    else
                    {
                        imgIcon.Source = null;
                        iconPath = value;
                    }
                }
                catch { }
            }
        }

        public object? ConfigurationObject { get; private set; }
        public PropertyInfo? PropertyInfo { get; private set; }
        public MethodInfo? MethodInfo { get; private set; }
        public AutoConfigMemberAttribute? MemberAttribute { get; private set; }

        public IAutoConfigUserControl? CustomControl { get; private set; }

        public void Reload()
        {
            MemberInfo? memberInfo = PropertyInfo != null ? PropertyInfo : MethodInfo;
            if (memberInfo != null && MemberAttribute != null)
            {
                Setup(ConfigurationObject, memberInfo, MemberAttribute);
            }
        }

        public void Setup(object? configurationObject, MemberInfo memberInfo, AutoConfigMemberAttribute attribute)
        {
            Title = attribute.Name;
            Description = attribute.Description;
            ConfigurationObject = configurationObject;
            MemberAttribute = attribute;
            PropertyInfo = memberInfo as PropertyInfo;
            MethodInfo = memberInfo as MethodInfo;

            if (configurationObject != null && attribute.Icon != null)
            {
                var cfgType = configurationObject.GetType();
                IconPath = Util.CreateResourceUri(cfgType.Assembly, attribute.Icon);
            }
            else
            {
                IconPath = attribute.Icon;
            }

            clickable = attribute.Kind == AutoConfigPropertyKind.Page;
            if (!clickable)
            {
                grRight.Children.Clear();
                grBottom.Children.Clear();
                FrameworkElement? element = null;
                if (PropertyInfo != null)
                {
                    switch (attribute.Kind)
                    {
                        case AutoConfigPropertyKind.DropDown:
                            element = BuildDropDown();
                            break;
                        case AutoConfigPropertyKind.Toggle:
                            element = BuildToggle();
                            break;
                        case AutoConfigPropertyKind.Slider:
                            element = BuildSlider(attribute);
                            break;
                        case AutoConfigPropertyKind.IntegerTextbox:
                            element = BuildIntegerTextBox(attribute);
                            break;
                        case AutoConfigPropertyKind.Textbox:
                            element = BuildTextBox(attribute);
                            break;
                        case AutoConfigPropertyKind.ReadOnlyLabel:
                            element = BuildReadOnlyLabel(attribute);
                            break;
                        case AutoConfigPropertyKind.Custom:
                        case AutoConfigPropertyKind.CustomWithLabel:
                            element = BuildCustom(attribute.Kind == AutoConfigPropertyKind.CustomWithLabel);
                            break;
                        case AutoConfigPropertyKind.Page:
                        default:
                            break;
                    }
                }
                else if (MethodInfo != null)
                {
                    var label = attribute.MethodLabel ?? MethodInfo.Name;
                    element = BuildMethodLink(label);
                }
                if (element != null)
                {
                    element.HorizontalAlignment = HorizontalAlignment.Right;
                    element.VerticalAlignment = VerticalAlignment.Center;
                    grRight.Children.Add(element);
                }
            }
        }

        private FrameworkElement? BuildSlider(AutoConfigMemberAttribute attribute)
        {
            var sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;

            var strval = GetValue()?.ToString() ?? "100";
            var value = 0.0;
            if (!double.TryParse(strval, out value))
            {
                value = 100;
            }

            var sl = new Slider();
            sl.Minimum = attribute.NumericMinValue;
            sl.Maximum = attribute.NumericMaxValue;
            sl.TickFrequency = attribute.SliderSteps;
            sl.IsSnapToTickEnabled = true;
            sl.Width = 150;
            sl.Value = value;
            sl.ValueChanged += Sl_ValueChanged;

            var lb = new Label();
            lb.Margin = new Thickness(0, 0, 8, 0);
            lb.Content = Math.Round(value, 2);
            sl.Tag = lb;
            sp.Children.Add(lb);
            sp.Children.Add(sl);

            return sp;
        }

        private void Sl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sender is Slider sl && sl.Tag is Label lb)
            {
                double value = sl.Value;
                lb.Content = Math.Round(value, 2);
                SetValue(value);
            }
        }

        private FrameworkElement? BuildReadOnlyLabel(AutoConfigMemberAttribute attribute)
        {
            var tb = new TextBlock();
            tb.Text = GetValue()?.ToString();
            tb.Width = double.NaN;
            tb.TextAlignment = TextAlignment.Right;
            return tb;
        }

        private TextBlock BuildMethodLink(string content)
        {
            var tb = new TextBlock();
            var hp = new Hyperlink();
            hp.Inlines.Add(content);
            hp.Click += Hyperlink_Click;
            hp.Tag = ConfigurationObject;
            tb.Inlines.Add(hp);
            tb.IsEnabled = MemberAttribute?.ReadOnly != true;
            return tb;
        }

        private IAutoConfigItemSource? GetItemSource()
        {
            try
            {
                if (MemberAttribute?.ItemSource != null)
                {
                    return Activator.CreateInstance(MemberAttribute.ItemSource) as IAutoConfigItemSource;
                }
            }
            catch
            { }
            return null;
        }

        private ComboBox BuildDropDown()
        {
            var comboBox = new ComboBox();
            comboBox.Width = double.NaN;
            comboBox.MinWidth = 100;
            comboBox.IsEnabled = MemberAttribute?.ReadOnly != true;
            var itemSource = GetItemSource();
            if (itemSource != null)
            {
                var currentValue = PropertyInfo?.GetValue(ConfigurationObject);
                ComboBoxItem? selection = null;
                foreach (var item in itemSource)
                {
                    var cbi = new ComboBoxItem()
                    {
                        Content = item.Key,
                        Tag = item.Value,
                    };
                    if (item.Value?.Equals(currentValue) == true || (item.Value == null && currentValue == null))
                    {
                        selection = cbi;
                    }
                    comboBox.Items.Add(cbi);
                }
                if (selection != null)
                {
                    comboBox.SelectedItem = selection;
                }
            }
            comboBox.SelectionChanged += ComboBox_SelectionChanged;
            return comboBox;
        }

        private Slider BuildToggle()
        {
            //                 < Slider HorizontalAlignment = "Right" VerticalAlignment = "Center" Width = "30" Minimum = "0" Maximum = "1"
            //            TickFrequency = "1" LargeChange = "1" SmallChange = "1" ></ Slider >
            var slider = new Slider();
            slider.HorizontalAlignment = HorizontalAlignment.Right;
            slider.VerticalAlignment = VerticalAlignment.Center;
            slider.Width = 30;
            slider.Minimum = 0;
            slider.TickFrequency =
            slider.LargeChange =
            slider.SmallChange =
            slider.Maximum = 1;
            slider.Value = 0;
            slider.Background = new SolidColorBrush(Colors.Crimson);
            slider.IsEnabled = MemberAttribute?.ReadOnly != true;

            var currentValue = PropertyInfo?.GetValue(ConfigurationObject);
            if (currentValue is bool b)
            {
                slider.Value = b ? 1 : 0;
                if (b)
                    slider.Background = new SolidColorBrush(Colors.LimeGreen);
            }
            slider.ValueChanged += Slider_ValueChanged;

            return slider;
        }

        private TextBox BuildIntegerTextBox(AutoConfigMemberAttribute attribute)
        {
            var integerTextBox = new TextBox();
            integerTextBox.HorizontalAlignment = HorizontalAlignment.Right;
            integerTextBox.VerticalAlignment = VerticalAlignment.Center;
            integerTextBox.Width = double.NaN;
            integerTextBox.MinWidth = 50;
            integerTextBox.HorizontalContentAlignment = HorizontalAlignment.Right;
            integerTextBox.Padding = new Thickness(4, 2, 4, 2);
            integerTextBox.Tag = attribute;
            integerTextBox.IsEnabled = MemberAttribute?.ReadOnly != true;
            try
            {
                integerTextBox.Text = PropertyInfo?.GetValue(ConfigurationObject)?.ToString();
            }
            catch { }
            integerTextBox.LostKeyboardFocus += IntegerTextBox_LostKeyboardFocus;
            return integerTextBox;
        }

        private TextBox BuildTextBox(AutoConfigMemberAttribute attribute)
        {
            var textBox = new TextBox();
            textBox.HorizontalAlignment = HorizontalAlignment.Right;
            textBox.VerticalAlignment = VerticalAlignment.Center;
            textBox.Width = double.NaN;
            textBox.MinWidth = 100;
            textBox.HorizontalContentAlignment = HorizontalAlignment.Right;
            textBox.Padding = new Thickness(4, 2, 4, 2);
            textBox.Tag = attribute;
            textBox.IsEnabled = MemberAttribute?.ReadOnly != true;
            try
            {
                textBox.Text = PropertyInfo?.GetValue(ConfigurationObject)?.ToString();
            }
            catch { }
            textBox.LostKeyboardFocus += TextBox_LostKeyboardFocus;
            return textBox;
        }

        private FrameworkElement? BuildCustom(bool withLabel)
        {
            if (MemberAttribute?.ConfigurationControl != null)
            {
                try
                {
                    CustomControl = Activator.CreateInstance(MemberAttribute.ConfigurationControl) as IAutoConfigUserControl;

                    if (CustomControl != null && CustomControl is FrameworkElement ctl)
                    {
                        var currentValue = PropertyInfo?.GetValue(ConfigurationObject);
                        CustomControl.LoadConfigurationValue(currentValue);
                        grBottom.Children.Add(ctl);
                        CustomControl.ValueChanged += CustomControl_ValueChanged;
                    }
                }
                catch { }
            }
            if (withLabel && CustomControl is IAutoConfigUserControlWithLabel wl)
            {
                var label = new Label();
                label.Content = wl.GetLabel();
                label.HorizontalAlignment = HorizontalAlignment.Right;
                label.VerticalAlignment = VerticalAlignment.Center;
                return label;
            }
            return null;
        }

        private bool IsValid(object? value)
        {
            if (MemberAttribute?.Validator != null)
            {
                try
                {
                    var instance = Activator.CreateInstance(MemberAttribute.Validator) as IAutoConfigValidator;
                    if (instance != null)
                    {
                        value = instance.Format(value);
                        if (!instance.Validate(value, ConfigurationObject))
                            return false;
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private bool SetValue(object? value)
        {
            try
            {
                if (MemberAttribute?.Validator != null)
                {
                    try
                    {
                        var instance = Activator.CreateInstance(MemberAttribute.Validator) as IAutoConfigValidator;
                        if (instance != null)
                        {
                            value = instance.Format(value);
                        }
                    }
                    catch
                    {
                    }
                }
                if (PropertyInfo != null)
                {
                    PropertyInfo.SetValue(ConfigurationObject, value);
                    ValueChanged?.Invoke(this, EventArgs.Empty);
                    return true;
                }
            }
            catch
            {

            }
            return false;
        }

        private object? GetValue()
        {
            return PropertyInfo?.GetValue(ConfigurationObject);
        }
        
        private void TextBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (sender is TextBox textBox && textBox.Tag is AutoConfigMemberAttribute)
            {
                if (IsValid(textBox.Text))
                {
                    SetValue(textBox.Text);
                }
                textBox.Text = GetValue()?.ToString() ?? "";
            }
        }

        private void IntegerTextBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (sender is TextBox textBox && textBox.Tag is AutoConfigMemberAttribute attribute)
            {
                int value;
                if (int.TryParse(textBox.Text, out value))
                {
                    value = (int)Math.Min(attribute.NumericMaxValue, Math.Max(attribute.NumericMinValue, value));

                    if (IsValid(textBox.Text))
                    {
                        SetValue(textBox.Text);
                    }
                    textBox.Text = GetValue()?.ToString() ?? "";
                }
                else
                {
                    textBox.Text = GetValue()?.ToString() ?? "";
                }
            }
        }

        private void CustomControl_ValueChanged(object? sender, EventArgs e)
        {
            if (sender is IAutoConfigUserControl control)
            {
                if (control is IAutoConfigUserControlWithLabel wl && grRight.Children.Count > 0 && grRight.Children[0] is Label label)
                {
                    label.Content = wl.GetLabel();
                }
                try
                {
                    var value = control.GetConfigurationValue();
                    SetValue(value);
                }
                catch { }
            }
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sender is Slider slider)
            {
                slider.Background = slider.Value < 1 ? new SolidColorBrush(Colors.Crimson) : new SolidColorBrush(Colors.LimeGreen);
                if (PropertyInfo?.PropertyType == typeof(bool))
                {
                    var bValue = slider.Value > 0.5;
                    SetValue(bValue);
                }
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ComboBox cb)
            {
                try
                {
                    if (cb.SelectedItem is ComboBoxItem cbi)
                    {
                        SetValue(cbi.Tag);
                    }
                    else
                    {
                        SetValue(null);
                    }
                }
                catch { }
            }
        }

        public AutoConfiguratorPropertyEditor()
        {
            InitializeComponent();
        }

        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            brdMain.Background = new SolidColorBrush(Color.FromArgb(0x44, 0xdd, 0xdd, 0xdd));
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            brdMain.Background = new SolidColorBrush(Color.FromArgb(0x88, 0xff, 0xff, 0xff));
        }

        private void grBottom_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Height = grBottom.ActualHeight + 70;
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Hyperlink btn && btn.Tag != null && MethodInfo != null)
            {
                try
                {
                    MethodInfo.Invoke(btn.Tag, null);
                    ValueChanged?.Invoke(this, EventArgs.Empty);
                }
                catch { }
            }
        }

        bool clickable = false;
        private void UserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!clickable) { return; }
            Click?.Invoke(this, e);
            e.Handled = true;
        }

        public event MouseEventHandler? Click;

        public event EventHandler? ValueChanged;
    }
}
