﻿using Aura.Models;
using System.Windows.Controls;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for AvatarLayerMaterial.xaml
    /// </summary>
    public partial class AvatarLayerMaterial : UserControl, IDisposable
    {
        public Avatar Avatar { get; }
        public SpriteManager SpriteManager { get; }
        public Guid? CurrentSpriteId { get; private set; }
        public Material? CurrentMaterial { get; private set; }

        public AvatarLayerMaterial(Avatar avatar, SpriteManager spriteManager)
        {
            InitializeComponent();
            Avatar = avatar;
            SpriteManager = spriteManager;
            SpriteManager.DispatcherTimer.Tick += DispatcherTimer_Tick;
        }

        private void DispatcherTimer_Tick(object? sender, EventArgs e)
        {
            Update();
        }

        public void SetMaterial(Guid? materialId)
        {
            CurrentSpriteId = null;
            if (materialId == null)
            {
                CurrentMaterial = null;
            }
            else
            {
                CurrentMaterial = Avatar.Materials[materialId.Value];
                if (CurrentMaterial != null)
                {
                    CurrentMaterial.StartingFrameTime = SpriteManager.FrameTime;
                }
            }
            Update();
        }

        private void Update()
        {
            if (CurrentMaterial == null)
            {
                CurrentSpriteId = null;
                Dispatcher.Invoke(() =>
                {
                    imgSprite.Source = null;
                });
                return;
            }
            var material = CurrentMaterial;
            if (Width != material.Width || Height != material.Height)
            {
                Dispatcher.Invoke(() =>
                {
                    Width = material.Width;
                    Height = material.Height;
                });
            }

            var currentFrameIndex = SpriteManager.GetCurrentFrameIndex(material.FramesPerSecond, material.Frames.Count, material.LoopKind, CurrentMaterial.StartingFrameTime);

            if (material.Frames.Count > 0 && currentFrameIndex < material.Frames.Count && currentFrameIndex >= 0)
            {
                var spriteId = material.Frames[currentFrameIndex];
                if (CurrentSpriteId != spriteId)
                {
                    CurrentSpriteId = spriteId;
                    Dispatcher.Invoke(() =>
                    {
                        imgSprite.Source = SpriteManager.GetSpriteAsImageSource(spriteId);
                    });
                }
            }
            else
            {
                CurrentSpriteId = null;
                Dispatcher.Invoke(() =>
                {
                    imgSprite.Source = null;
                });
            }
        }

        public void Dispose()
        {
            SpriteManager.DispatcherTimer.Tick -= DispatcherTimer_Tick;
        }
    }
}
