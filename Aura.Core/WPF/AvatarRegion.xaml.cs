﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for AvatarRegion.xaml
    /// </summary>
    public partial class AvatarRegion : UserControl
    {
        public Avatar? Avatar { get; set; }

        public IReadOnlyDictionary<string, double>? ActiveTags { get; private set; }

        public AvatarRegion()
        {
            InitializeComponent();
        }

        public void LoadAvatar(Avatar? avatar)
        {
            Avatar = avatar;
            if (avatar == null)
            {
                Width = 0;
                Height = 0;
            }
            else
            {
                Width = avatar.Display.CanvasWidth;
                Height = avatar.Display.CanvasHeight;
            }
            LoadRegions();
        }

        bool _showRegion = false;
        public bool ShowRegion
        {
            get => _showRegion; set
            {
                if (_showRegion == value) return;
                _showRegion = value;
                UpdateOpacity();
            }
        }

        public void Clear()
        {
            canvasMain.Children.Clear();
        }

        private void UpdateOpacity()
        {
            canvasMain.Opacity = _showRegion ? 1 : 0.01;
        }

        public void LoadRegions(WeightedTagCollection? activeTags = null, Func<Region, bool>? filter = null)
        {
            Clear();
            if (Avatar != null)
            {
                foreach (var region in Avatar.Regions)
                {
                    if (filter != null && !filter(region))
                    {
                        continue;
                    }
                    LoadRegionShapes(region);
                }
            }
            UpdateRegions(activeTags);
            UpdateOpacity();
        }

        public void UpdateRegions(WeightedTagCollection? activeTags = null)
        {
            if (Avatar == null) return;
            ActiveTags = activeTags?.AsReadOnly() ?? ActiveTags;
            var tags = activeTags ?? new WeightedTagCollection();
            foreach (var shp in canvasMain.Children.OfType<System.Windows.Shapes.Shape>())
            {
                if (shp.Tag is Region region && tags != null)
                {
                    var w = region.TagCondition.CalculateWeight(tags);
                    shp.Visibility = w >= 1 ? Visibility.Visible : Visibility.Collapsed;
                }
                else
                {
                    shp.Visibility = Visibility.Visible;
                }
            }
        }

        private void LoadRegionShapes(Region region)
        {
            if (Avatar == null) return;
            foreach (var shape in region.Shapes)
            {
                System.Windows.Shapes.Shape? shp = null;
                if (shape.Type == ShapeType.Rectangle)
                {
                    shp = new Rectangle();
                }
                else if (shape.Type == ShapeType.Ellipse)
                {
                    shp = new Ellipse();
                }

                if (shp != null)
                {
                    shp.Width = shape.Width;
                    shp.Height = shape.Height;
                    shp.Fill = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));
                    shp.Margin = new Thickness(shape.X, shape.Y, 0, 0);
                    shp.VerticalAlignment = VerticalAlignment.Top;
                    shp.HorizontalAlignment = HorizontalAlignment.Left;
                    shp.Tag = region;
                    shp.MouseLeftButtonDown += RegionMouseButtonDown;
                    shp.MouseEnter += Shp_MouseEnter;
                    shp.MouseLeave += Shp_MouseLeave;
                    shp.MouseMove += Shp_MouseMove;
                    canvasMain.Children.Add(shp);
                }
            }
        }

        private void Shp_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender is System.Windows.Shapes.Shape shp)
            {
                RegionMouseMove?.Invoke(shp.Tag, e);
            }
        }

        private void Shp_MouseLeave(object sender, MouseEventArgs e)
        {
            if (sender is System.Windows.Shapes.Shape shp)
            {
                RegionMouseLeave?.Invoke(shp.Tag, e);
            }
        }

        private void Shp_MouseEnter(object sender, MouseEventArgs e)
        {
            if (sender is System.Windows.Shapes.Shape shp)
            {
                RegionMouseEnter?.Invoke(shp.Tag, e);
            }
        }

        private void RegionMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is System.Windows.Shapes.Shape shape && shape.Tag is Region region)
            {
                RegionClicked?.Invoke(sender, region);
                e.Handled = true;
            }
        }

        public event EventHandler<Region>? RegionClicked;
        public event MouseEventHandler? RegionMouseEnter;
        public event MouseEventHandler? RegionMouseLeave;
        public event MouseEventHandler? RegionMouseMove;
    }
}
