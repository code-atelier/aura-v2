﻿using Aura.IO;
using Aura.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Bitmap = System.Drawing.Bitmap;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for AvatarCanvas.xaml
    /// </summary>
    public partial class AvatarCanvas : UserControl
    {
        public Avatar? Avatar { get; private set; }
        public List<Layer> Layers { get => layerContainer.Children.OfType<AvatarLayer>().Select(x => x.Layer).ToList(); }
        public WeightedTagCollection ActiveTags { get; } = new WeightedTagCollection();
        public Dictionary<string, Guid?> MaterialOverride { get; } = new Dictionary<string, Guid?>();
        public List<Guid> ForceSet { get; } = new List<Guid>();
        public SpriteManager SpriteManager { get; private set; } = new SpriteManager();

        public bool IsRegionVisible { get => avaRegion.ShowRegion; set => avaRegion.ShowRegion = value; }

        public AvatarCanvas()
        {
            InitializeComponent();
            ActiveTags.TagChanged += ActiveTags_TagChanged;
        }

        private void ActiveTags_TagChanged(object? sender, EventArgs e)
        {
            UpdateLayers();
            avaRegion.UpdateRegions(ActiveTags);
        }

        public void LoadAvatar(Avatar? avatar, SpriteManager fileSource)
        {
            SpriteManager = fileSource;
            Avatar = avatar;

            if (avatar != null)
            {
                layerContainer.Width = Width = avatar.Display.CanvasWidth;
                layerContainer.Height = Height = avatar.Display.CanvasHeight;
            }
            else
            {
                layerContainer.Width = Width = 0;
                layerContainer.Height = Height = 0;
            }

            ReloadLayers();
            avaRegion.LoadAvatar(avatar);
            ReloadRegions(ActiveTags);
        }

        public void ReloadRegions(WeightedTagCollection? activeTags = null, Func<Region, bool>? filter = null)
        {
            avaRegion.LoadRegions(activeTags, filter);
        }

        public void ReloadLayers()
        {
            layerContainer.Children.Clear();
            MaterialOverride.Clear();
            ForceSet.Clear();

            if (Avatar != null)
            {
                var layers = Avatar.Layers.ToList();
                layers.Sort((a, b) => a.ZIndex.CompareTo(b.ZIndex));
                foreach (var layer in layers)
                {
                    var avaLayer = new AvatarLayer(layer, Avatar, SpriteManager);
                    layerContainer.Children.Add(avaLayer);
                    MaterialOverride.Add(layer.Id, null);
                }
            }
            UpdateLayers();
        }

        public void UpdateLayers()
        {
            if (Avatar == null) return;

            var categorizedSets = Avatar.GetCategorizedSets();
            var maxSets = new List<Tuple<Set, double>>();

            foreach (var sets in categorizedSets)
            {
                Set? maxSet = null;
                var setlist = sets.Value;
                double maxValue = 1; // must reach 1 to be active
                for (var i = 0; i < setlist.Count; i++)
                {
                    var ss = setlist[i];
                    var w = ForceSet.Contains(ss.Id) ? double.MaxValue : ss.TagCondition.CalculateWeight(ActiveTags);
                    if (w >= maxValue)
                    {
                        maxValue = w;
                        maxSet = ss;
                    }
                }
                if (maxSet != null)
                {
                    maxSets.Add(new Tuple<Set, double>(maxSet, maxValue));
                }
            }
            maxSets.Sort((a, b) => a.Item2.CompareTo(b.Item2));
            var activeSets = maxSets.Select(x => x.Item1).ToList();

            foreach (var layer in layerContainer.Children.OfType<AvatarLayer>())
            {
                var matOvr = MaterialOverride.ContainsKey(layer.Layer.Id) ? MaterialOverride[layer.Layer.Id] : null;
                if (matOvr != null)
                {
                    if (layer.ActiveMaterial != matOvr)
                    {
                        layer.SetActiveMaterial(matOvr);
                    }
                }
                else
                {
                    var material = layer.Layer.Default;
                    bool isDefault = true;
                    foreach (var set in activeSets)
                    {
                        if (set.Layers.ContainsKey(layer.Layer.Id))
                        {
                            material = set.Layers[layer.Layer.Id] ?? material;
                            isDefault = false;
                        }
                    }
                    if (!isDefault || layer.ActiveMaterial != material)
                    {
                        layer.SetActiveMaterial(material);
                    }
                }
            }
        }

        public event EventHandler<Region>? RegionClicked;

        public event MouseEventHandler? RegionMouseEnter;
        public event MouseEventHandler? RegionMouseLeave;
        public event MouseEventHandler? RegionMouseMove;

        private void avaRegion_RegionClicked(object sender, Region e)
        {
            RegionClicked?.Invoke(sender, e);
        }

        private void avaRegion_RegionMouseEnter(object sender, MouseEventArgs e)
        {
            RegionMouseEnter?.Invoke(sender, e);
        }

        private void avaRegion_RegionMouseLeave(object sender, MouseEventArgs e)
        {
            RegionMouseLeave?.Invoke(sender, e);
        }

        private void avaRegion_RegionMouseMove(object sender, MouseEventArgs e)
        {
            RegionMouseMove?.Invoke(sender, e);
        }

        public bool WriteAvatar(Stream targetStream)
        {
            try
            {
                if (Avatar == null) { return false; }
                using (Bitmap bmp = new Bitmap(Avatar.Display.CanvasWidth, Avatar.Display.CanvasHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                {
                    using (var g = System.Drawing.Graphics.FromImage(bmp))
                    {
                        g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                        g.FillRectangle(new System.Drawing.SolidBrush(System.Drawing.Color.Transparent), new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height));
                        g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                        foreach (var layer in layerContainer.Children.OfType<AvatarLayer>())
                        {
                            var activeMat = layer.ActiveMaterial;
                            var layerMat = layer.Layer.Materials.FirstOrDefault(x => x.Material == activeMat);
                            var mat = Avatar.Materials.FirstOrDefault(x => x.Id == activeMat);
                            if (mat != null && layerMat != null)
                            {
                                var destRect = new System.Drawing.Rectangle(layerMat.X, layerMat.Y, mat.Width, mat.Height);
                                var frame = SpriteManager.GetCurrentFrameIndex(mat.FramesPerSecond, mat.Frames.Count, mat.LoopKind, mat.StartingFrameTime);
                                if (mat.Frames.Count > 0 && frame < mat.Frames.Count && frame >= 0)
                                {
                                    var spriteId = mat.Frames[frame];
                                    var sprite = SpriteManager.GetSprite(spriteId);
                                    if (sprite != null && sprite.BinaryData != null && sprite.BinaryData.Length > 0)
                                    {
                                        using (var ms = new MemoryStream(sprite.BinaryData))
                                        {
                                            ms.Seek(0, SeekOrigin.Begin);
                                            using (var draw = new Bitmap(ms))
                                            {
                                                g.DrawImage(draw, destRect);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    bmp.Save(targetStream, System.Drawing.Imaging.ImageFormat.Png);
                    return true;
                }
            }
            catch { }
            return false;
        }
    }
}
