﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.WPF.Windows
{
    /// <summary>
    /// Interaction logic for Prompt.xaml
    /// </summary>
    public partial class Prompt : Window
    {
        public Prompt()
        {
            InitializeComponent();
        }

        public static string? ShowDialog(string label, string title, string initialValue = "")
        {
            var f = new Prompt();
            f.txInputField.Text = initialValue;
            f.Title = title;
            f.lbInputLabel.Content = label;
            if (f.ShowDialog() == true)
            {
                return f.txInputField.Text;
            }
            return null;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            txInputField.Focus();
            txInputField.SelectAll();
        }
    }
}
