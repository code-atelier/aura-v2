﻿using Aura.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.WPF.Windows
{
    /// <summary>
    /// Interaction logic for PersonaCompendium.xaml
    /// </summary>
    public partial class PersonaCompendium : Window
    {
        public PersonaCompendium()
        {
            InitializeComponent();
        }

        public string? WorkingDirectory { get; private set; }
        
        public string? SelectedPersonaPath { get; private set; }

        private SpriteManager Sprites { get; } = new SpriteManager();

        public void SetWorkingDirectory(string directory, bool includeSubdir = false)
        {
            WorkingDirectory = includeSubdir ? null : directory;
            btnLoadFile.Visibility = !includeSubdir ? Visibility.Visible : Visibility.Collapsed;
            var files = Directory.EnumerateFiles(directory, "*.a2p", !includeSubdir ? SearchOption.TopDirectoryOnly : SearchOption.AllDirectories).ToList();
            lbPersonas.Items.Clear();
            foreach (var file in files)
            {
                try
                {
                    var p = Persona.Load(file, false);
                    var lbi = new ListBoxItem()
                    {
                        Content = p.GivenName + " " + p.FamilyName + " {" + p.Id + "}",
                        Tag = file,
                    };
                    lbPersonas.Items.Add(lbi);
                }
                catch { }
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void CommandSelect_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lbPersonas.SelectedItem is ListBoxItem lbi && lbi.Tag is string;
        }

        private void CommandSelect_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (lbPersonas.SelectedItem is ListBoxItem lbi && lbi.Tag is string file && File.Exists(file))
            {
                SelectedPersonaPath = file;
                DialogResult = true;
                Close();
            }
        }

        private void btnLoadFile_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(WorkingDirectory)) return;
            var ofd = new OpenFileDialog();
            ofd.Filter = "Aura2 Persona File|*.a2p";
            if (ofd.ShowDialog() == true)
            {
                try
                {
                    var p = Persona.Load(ofd.FileName, false);
                    var destPath = System.IO.Path.Combine(WorkingDirectory, p.Id + ".a2p");
                    if (File.Exists(destPath) && MessageBox.Show("Persona with the same id already exists, overwrite?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                        return;
                    File.Copy(ofd.FileName, destPath, true);
                    SetWorkingDirectory(WorkingDirectory);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Failed to read persona: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void lbPersonas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbPersonas.SelectedItem is ListBoxItem lbi && lbi.Tag is string file && File.Exists(file))
            {
                try
                {
                    var p = Persona.Load(file, true);
                    Sprites.Clear();
                    foreach (var sprite in p.Avatar.Sprites)
                    {
                        Sprites.Add(sprite);
                    }
                    avatarCanvas.LoadAvatar(p.Avatar, Sprites);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to load persona: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
