﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Aura.WPF
{
    public class ScrollableCommandButtonItem : INotifyPropertyChanged
    {
        string _label = string.Empty;
        public string Label
        {
            get => _label;
            set
            {
                if (_label != value)
                {
                    _label = value;
                    NotifyPropertyChanged(nameof(Label));
                }
            }
        }

        public string? Tooltip { get; set; }

        public ImageSource? Image { get; set; }

        public object? Tag { get; set; }

        public event EventHandler? OpenRequested;
        public event EventHandler? OpenFolderRequested;
        public event DeletingEventHandler? DeleteRequested;
        public event EventHandler? RenameRequested;

        public event PropertyChangedEventHandler? PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        internal void RequestOpen()
        {
            OpenRequested?.Invoke(this, EventArgs.Empty);
        }
        internal void RequestOpenFolder()
        {
            OpenFolderRequested?.Invoke(this, EventArgs.Empty);
        }
        internal void RequestRename()
        {
            RenameRequested?.Invoke(this, EventArgs.Empty);
        }
        internal bool RequestDelete()
        {
            return DeleteRequested?.Invoke(this, EventArgs.Empty) ?? false;
        }
    }

    public delegate bool DeletingEventHandler(object sender, EventArgs e);
}
