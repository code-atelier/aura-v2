﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for FileDropMenu.xaml
    /// </summary>
    public partial class FileDropMenu : Window, IFileDropMenu
    {
        public ExtensionInfo Info { get; } = new ExtensionInfo
        {
            Author = "CodeAtelier",
            Description = "Default file drop menu",
            Id = "coatl.aura.component.filedropmenu",
            Name = "File Drop Menu",
            Version = new Version(1, 0, 0)
        };

        public FileDropMenu()
        {
            InitializeComponent();
        }

        private void BeginClose()
        {
            var da = new DoubleAnimation()
            {
                From = 1,
                To = 0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 200))
            };
            var sb = new Storyboard();
            sb.Children.Add(da);
            Storyboard.SetTargetProperty(da, new PropertyPath(OpacityProperty));
            sb.Completed += Sb_Completed;
            sb.Begin(this);
        }

        private void Sb_Completed(object? sender, EventArgs e)
        {
            Close();
        }

        public List<string> Files { get; } = new List<string>();


        public event FileDropExecuteRequestEventHandler? OnFileDropExecute;

        public void LoadFiles(IEnumerable<string> files)
        {
            Files.Clear();
            Files.AddRange(files);
            lbInfo.Content = files.Count() == 1 ? "What to do with this file?" : $"What to do with these {files.Count()} files?";
        }

        public void LoadHandlers(IEnumerable<FileDropHandler> handlers)
        {
            foreach (var handler in handlers.OrderBy(x => x.Name))
            {
                var scbi = new ScrollableCommandButtonItem()
                {
                    Label = handler.Name,
                    Tag = handler,
                };
                if (!string.IsNullOrWhiteSpace(handler.Icon))
                {
                    var bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.UriSource = new Uri(handler.Icon);
                    bmp.EndInit();
                    scbi.Image = bmp;
                    scbi.OpenRequested += Scbi_Click;
                }
                scbActions.Items.Add(scbi);
            }
        }

        private async void Scbi_Click(object? sender, EventArgs e)
        {
            if (sender is ScrollableCommandButtonItem scrollableCommandButtonItem && scrollableCommandButtonItem.Tag is FileDropHandler fdh)
            {
                BeginClose();
                await Task.Run(() =>
                {
                    OnFileDropExecute?.Invoke(this, fdh, Files);
                });
            }
        }

        public void Show(string anchor, double anchorX, double anchorY)
        {
            const int margin = 10;
            if (anchor == InternalTags.Anchor.CommandRight)
            {
                Left = anchorX + margin;
                Top = anchorY - Height / 2;
            }
            else
            {
                // assume left
                Left = anchorX - Width - margin;
                Top = anchorY - Height / 2;
            }

            Opacity = 0;
            Show();
            var da = new DoubleAnimation()
            {
                From = 0,
                To = 1,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 200))
            };
            var sb = new Storyboard();
            sb.Children.Add(da);
            Storyboard.SetTargetProperty(da, new PropertyPath(OpacityProperty));
            sb.Begin(this);
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            BeginClose();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            BeginClose();
        }
    }
}
