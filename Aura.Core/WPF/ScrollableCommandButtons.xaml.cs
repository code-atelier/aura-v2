﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for ScrollableCommandButtons.xaml
    /// </summary>
    public partial class ScrollableCommandButtons : UserControl
    {
        public Visibility ContextMenuVisibility { 
            get => (Resources["ctmButton"] as ContextMenu)?.Visibility ?? Visibility.Collapsed; 
            set
            {
                if (Resources["ctmButton"] is ContextMenu cm)
                {
                    cm.Visibility = value;
                }
            }
        }

        const int ButtonHeight = 75;
        Color _pagerColor = Color.FromRgb(0x66, 0x77, 0x99);
        public Color PagerColor
        {
            get => _pagerColor;
            set
            {
                _pagerColor = value;
                UpdatePagerColor();
            }
        }

        public FilteredObservableCollection<ScrollableCommandButtonItem> Items { get; } = new FilteredObservableCollection<ScrollableCommandButtonItem>();

        public ScrollableCommandButtons()
        {
            InitializeComponent();
            Items.CollectionChanged += Items_CollectionChanged;
            itmContainer.ItemsSource = Items;
        }

        private void Items_CollectionChanged(object? sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdatePager();
        }

        private void UpdatePagerColor()
        {
            foreach (var ellipse in spPagerTop.Children.OfType<Ellipse>())
            {
                ellipse.Fill = new SolidColorBrush(_pagerColor);
            }
            foreach (var ellipse in spPagerBot.Children.OfType<Ellipse>())
            {
                ellipse.Fill = new SolidColorBrush(_pagerColor);
            }
        }

        private void UpdatePager()
        {
            var topCount = Math.Round(svScroller.VerticalOffset / ButtonHeight);
            var botCount = Math.Max(0, Math.Round((itmContainer.ActualHeight - svScroller.ViewportHeight - svScroller.VerticalOffset) / ButtonHeight));
            while (spPagerTop.Children.Count < topCount)
            {
                spPagerTop.Children.Add(CreatePagerDot(true));
            }
            while (spPagerTop.Children.Count > topCount)
            {
                spPagerTop.Children.RemoveAt(0);
            }
            while (spPagerBot.Children.Count < botCount)
            {
                spPagerBot.Children.Add(CreatePagerDot(false));
            }
            while (spPagerBot.Children.Count > botCount)
            {
                spPagerBot.Children.RemoveAt(0);
            }
        }

        private Ellipse CreatePagerDot(bool top)
        {
            var ell = new Ellipse();
            ell.Height = 4;
            ell.Width = 4;
            ell.Fill = new SolidColorBrush(_pagerColor);
            ell.Margin = new Thickness(0, top ? 0 : 2, 0, top ? 2 : 0);
            return ell;
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            double fixScroll;
            if (e.VerticalChange > 0)
            {
                fixScroll = Math.Ceiling(svScroller.VerticalOffset / ButtonHeight) * ButtonHeight;
            }
            else
            {
                fixScroll = Math.Floor(svScroller.VerticalOffset / ButtonHeight) * ButtonHeight;
            }
            svScroller.ScrollToVerticalOffset(fixScroll);
            UpdatePager();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn && btn.Tag is ScrollableCommandButtonItem scrollableCommandButtonItem)
            {
                scrollableCommandButtonItem.RequestOpen();
            }
        }

        private void mi_Open(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem mi && mi.Tag is ScrollableCommandButtonItem scrollableCommandButtonItem)
            {
                scrollableCommandButtonItem.RequestOpen();
            }
        }

        private void Button_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (sender is Button b && b.ContextMenu != null)
            {
                foreach(var mi in b.ContextMenu.Items.OfType<MenuItem>())
                {
                    mi.Tag = b.Tag;
                }
            }
        }

        private void mi_OpenFolder(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem mi && mi.Tag is ScrollableCommandButtonItem scrollableCommandButtonItem)
            {
                scrollableCommandButtonItem.RequestOpenFolder();
            }
        }

        private void mi_Rename(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem mi && mi.Tag is ScrollableCommandButtonItem scrollableCommandButtonItem)
            {
                scrollableCommandButtonItem.RequestRename();
            }
        }

        private void mi_Delete(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem mi && mi.Tag is ScrollableCommandButtonItem scrollableCommandButtonItem)
            {
                if (scrollableCommandButtonItem.RequestDelete())
                {
                    Items.Remove(scrollableCommandButtonItem);
                }
            }
        }
    }
}
