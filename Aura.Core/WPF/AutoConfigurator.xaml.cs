﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for AutoConfigurator.xaml
    /// </summary>
    public partial class AutoConfigurator : Window
    {
        List<AutoConfiguratorPage> Pages { get; } = new List<AutoConfiguratorPage>();

        public AutoConfigurator()
        {
            InitializeComponent();
        }

        public void LoadConfiguration(object configuration)
        {
            var cfgType = configuration.GetType();
            var acClass = cfgType.GetCustomAttribute<AutoConfigAttribute>();
            if (acClass == null)
            {
                throw new Exception($"{cfgType.Name} does not have AutoConfig attribute");
            }
            Title = acClass.Name + " Configuration";
            Width = acClass.DefaultWidth;
            Height = acClass.DefaultHeight;

            if (acClass.Icon!= null)
            {
                try
                {
                    var bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.UriSource = new Uri(Util.CreateResourceUri(cfgType.Assembly, acClass.Icon));
                    bmp.EndInit();
                    Icon = bmp;
                }
                catch { }
            }

            LoadPage(configuration, acClass.Name);
        }

        private void LoadPage(object configurationObject, string title)
        {
            var page = new AutoConfiguratorPage();
            page.LoadConfigurationObject(configurationObject, title);
            page.Navigate += Page_Navigate;

            Pages.Add(page);

            svPageScroller.Content = page;
            svPageScroller.ScrollToVerticalOffset(0);
            RefreshNavigator();
        }

        private void Page_Navigate(object sender, AutoConfiguratorPageNavigateEventArgs args)
        {
            LoadPage(args.ConfigurationObject, args.Title);
        }

        private void RefreshNavigator()
        {
            var navs = Pages.ToList();
            var idx = 0;
            spNavigator.Children.Clear();
            foreach (var nav in navs)
            {
                if (idx > 0)
                {
                    // separator
                    var lb = new Label();
                    lb.Padding = new Thickness(0);
                    lb.VerticalAlignment = VerticalAlignment.Top;
                    lb.FontSize = 24;
                    lb.FontWeight = FontWeights.Bold;
                    lb.Margin = new Thickness(10, 0, 10, 0);
                    lb.Content = "»";
                    spNavigator.Children.Add(lb);
                }
                if (idx < navs.Count - 1)
                {
                    // <Button Padding="0" VerticalAlignment="Top" Background="Transparent" FontSize="24" Style="{StaticResource ResourceKey=ButtonCornerRadius}">System</Button>
                    var lb = new Button();
                    lb.Background = new SolidColorBrush(Colors.Transparent);
                    lb.Padding = new Thickness(0);
                    lb.VerticalAlignment = VerticalAlignment.Top;
                    lb.FontSize = 24;
                    lb.Content = nav.PageTitle;
                    lb.Tag = nav;
                    lb.Style = Resources["ButtonCornerRadius"] as Style;
                    lb.Click += Lb_Click;
                    spNavigator.Children.Add(lb);
                }
                else
                {
                    // <Label Padding="0" VerticalAlignment="Top" FontSize="24">Avatar</Label>
                    var lb = new Label();
                    lb.Padding = new Thickness(0);
                    lb.VerticalAlignment = VerticalAlignment.Top;
                    lb.FontSize = 24;
                    lb.Content = nav.PageTitle;
                    lb.Tag = nav;
                    spNavigator.Children.Add(lb);
                }
                idx++;
            }
        }

        private void Lb_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn && btn.Tag is AutoConfiguratorPage page)
            {
                var idx = Pages.IndexOf(page);
                if (idx >= 0)
                {
                    while (Pages.Count > idx + 1)
                    {
                        Pages.RemoveAt(Pages.Count - 1);
                    }
                }
                page.ReloadConfigurationObject();
                svPageScroller.Content = page;
                svPageScroller.ScrollToVerticalOffset(0);
                RefreshNavigator();
            }
        }
    }
}
