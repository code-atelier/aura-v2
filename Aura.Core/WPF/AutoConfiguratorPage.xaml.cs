﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for AutoConfiguratorPage.xaml
    /// </summary>
    public partial class AutoConfiguratorPage : UserControl
    {
        public object? ConfigurationObject { get; private set; }

        public string? PageTitle { get; private set; }

        public AutoConfiguratorPage()
        {
            InitializeComponent();
        }

        public void LoadConfigurationObject(object configurationObject, string title)
        {
            ConfigurationObject = configurationObject;
            PageTitle = title;
            var oType = configurationObject.GetType();
            var oProps = oType.GetProperties().Where(x => x.CanRead).ToList();
            var oMethods = oType.GetMethods().Where(x => x.IsPublic && !x.IsConstructor && x.GetParameters().Length == 0).ToList();

            List<Tuple<MemberInfo, AutoConfigMemberAttribute>> members = new List<Tuple<MemberInfo, AutoConfigMemberAttribute>>();

            foreach (var prop in oProps)
            {
                var aprop = prop.GetCustomAttribute<AutoConfigMemberAttribute>();
                if (aprop != null &&
                    (aprop.Kind == AutoConfigPropertyKind.ReadOnlyLabel || prop.CanWrite)
                )
                {
                    members.Add(new Tuple<MemberInfo, AutoConfigMemberAttribute>(prop, aprop));
                }
            }
            foreach (var method in oMethods)
            {
                var amethod = method.GetCustomAttribute<AutoConfigMemberAttribute>();
                if (amethod != null)
                {
                    members.Add(new Tuple<MemberInfo, AutoConfigMemberAttribute>(method, amethod));
                }
            }

            var ordered = members.OrderBy(x => x.Item2.GroupIndex).GroupBy(x => x.Item2.Group).ToList();

            spPropertyContainer.Children.Clear();
            foreach (var group in ordered)
            {
                var groupItems = group.OrderBy(x => x.Item2.Index).ToList();
                if (!string.IsNullOrWhiteSpace(group.Key))
                {
                    var label = new Label();
                    label.FontWeight = FontWeights.Bold;
                    label.Content = group.Key;
                    spPropertyContainer.Children.Add(label);
                }
                foreach (var prop in groupItems)
                {
                    var pc = new AutoConfiguratorPropertyEditor();
                    pc.Setup(configurationObject, prop.Item1, prop.Item2);
                    pc.Margin = new Thickness(0, 0, 0, 8);
                    pc.Tag = prop;
                    if (prop.Item2.Kind == AutoConfigPropertyKind.Page)
                    {
                        pc.Click += Pc_Click;
                    }
                    else
                    {
                        pc.ValueChanged += Pc_ValueChanged;
                    }
                    spPropertyContainer.Children.Add(pc);
                }
            }
        }

        private void Pc_ValueChanged(object? sender, EventArgs e)
        {
            if (sender is AutoConfiguratorPropertyEditor editor && editor.MemberAttribute?.UpdatePage == true)
            {
                foreach (var propEditor in spPropertyContainer.Children.OfType<AutoConfiguratorPropertyEditor>())
                {
                    propEditor.Reload();
                }
            }
        }

        public void ReloadConfigurationObject()
        {
            if (ConfigurationObject != null && PageTitle != null)
            {
                LoadConfigurationObject(ConfigurationObject, PageTitle);
            }
        }

        private void Pc_Click(object sender, MouseEventArgs e)
        {
            if (sender is AutoConfiguratorPropertyEditor acpe
                && acpe.MemberAttribute?.Kind == AutoConfigPropertyKind.Page
                && acpe.ConfigurationObject != null
                && acpe.PropertyInfo != null)
            {
                var newConfigObject = acpe.PropertyInfo.GetValue(acpe.ConfigurationObject);
                if (newConfigObject != null)
                {
                    Navigate?.Invoke(this, new AutoConfiguratorPageNavigateEventArgs(newConfigObject, acpe.MemberAttribute.Name));
                }
            }
        }

        public event AutoConfiguratorPageNavigateEvent? Navigate;
    }

    public delegate void AutoConfiguratorPageNavigateEvent(object sender, AutoConfiguratorPageNavigateEventArgs args);

    public class AutoConfiguratorPageNavigateEventArgs : EventArgs
    {
        public object ConfigurationObject { get; }

        public string Title { get; }

        public AutoConfiguratorPageNavigateEventArgs(object configObject, string title)
        {
            ConfigurationObject = configObject;
            Title = title;
        }
    }
}
