﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for AvatarLayer.xaml
    /// </summary>
    public partial class AvatarLayer : UserControl
    {
        public Layer Layer { get; set; }

        public Avatar Avatar { get; set; }

        public Guid? ActiveMaterial { get; set; }

        private AvatarLayerMaterial LayerMaterial { get; set; }

        private AvatarLayerMaterial OldLayerMaterial { get; set; }

        private SpriteManager Sprites { get; set; }

        public AvatarLayer(Layer layer, Avatar avatar, SpriteManager spriteManager)
        {
            InitializeComponent();
            Layer = layer;
            Avatar = avatar;
            Sprites = spriteManager;

            OldLayerMaterial = new AvatarLayerMaterial(avatar, spriteManager);
            LayerMaterial = new AvatarLayerMaterial(avatar, spriteManager);
            grMain.Children.Add(OldLayerMaterial);
            grMain.Children.Add(LayerMaterial);
        }

        public void SetActiveMaterial(Guid? materialId)
        {
            var noMargin = new Thickness(0);
            ActiveMaterial = materialId;
            if (materialId == null || materialId == Guid.Empty)
            {
                SwitchMaterial(null, noMargin);
                return;
            }
            var lmat = Layer.Materials.FirstOrDefault(x => x.Material == materialId);
            if (lmat != null)
            {
                SwitchMaterial(lmat.Material, new Thickness(lmat.X, lmat.Y, 0, 0));
            }
            else
            {
                SwitchMaterial(null, noMargin);
            }
        }

        private void SwitchMaterial(Guid? materialId, Thickness margin)
        {
            TargetMargin = margin;
            TargetMaterialId = materialId;
            FadeOut();
        }

        private Thickness TargetMargin { get; set; }

        private Guid? TargetMaterialId { get; set; }

        private bool FadingOut { get; set; }

        private void FadeOut()
        {
            if (FadingOut) { return; }
            FadingOut = true;
            if (!Layer.FadeOut || LayerMaterial.CurrentMaterial == null || (Layer.FadeIn && TargetMaterialId != null && TargetMaterialId != Guid.Empty))
            {
                FadeIn();
                return;
            }
            // do fade out
            var dAnim = new DoubleAnimation
            {
                From = 1.0,
                To = 0.0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300))
            };
            var sb = new Storyboard();
            sb.Children.Add(dAnim);
            Storyboard.SetTargetProperty(dAnim, new PropertyPath(OpacityProperty));
            sb.Completed += Sb_Completed;
            sb.Begin(LayerMaterial);
        }

        private void Sb_Completed(object? sender, EventArgs e)
        {
            FadeIn();
        }

        private void FadeIn()
        {
            Storyboard? sbOldFadeOut = null;
            FadingOut = false;
            if (TargetMaterialId != null && Layer.FadeIn && Layer.TransitionFadeIn)
            {
                grMain.Children.Remove(OldLayerMaterial);
                OldLayerMaterial = LayerMaterial;
                OldLayerMaterial.Opacity = 1.0;
                LayerMaterial = new AvatarLayerMaterial(Avatar, Sprites);
                LayerMaterial.Opacity = 0.0;
                grMain.Children.Add(LayerMaterial);

                var dOldAnim = new DoubleAnimation
                {
                    From = 1.0,
                    To = 0.0,
                    Duration = new TimeSpan(0, 0, 0, 0, 200),
                    BeginTime = new TimeSpan(0, 0, 0, 0, 100)
                };
                sbOldFadeOut = new Storyboard();
                sbOldFadeOut.Children.Add(dOldAnim);
                Storyboard.SetTargetProperty(dOldAnim, new PropertyPath(OpacityProperty));
                sbOldFadeOut.Begin(OldLayerMaterial);
            }
            LayerMaterial.Margin = TargetMargin;
            LayerMaterial.SetMaterial(TargetMaterialId);
            if (TargetMaterialId == null || !Layer.FadeIn)
            {
                LayerMaterial.Opacity = 1;
                return;
            }
            // do fade in
            var dAnim = new DoubleAnimation
            {
                From = 0.0,
                To = 1.0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300))
            };
            var sb = new Storyboard();
            sb.Children.Add(dAnim);
            Storyboard.SetTargetProperty(dAnim, new PropertyPath(OpacityProperty));
            sb.Completed += Sb_Completed1;
            sb.Begin(LayerMaterial);
        }

        private void Sb_Completed1(object? sender, EventArgs e)
        {
            OldLayerMaterial.SetMaterial(null);
            OldLayerMaterial.Margin = new Thickness(0, 0, 0, 0);
        }
    }
}
