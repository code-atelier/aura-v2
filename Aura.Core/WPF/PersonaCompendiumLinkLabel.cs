﻿using Aura.Models;
using Aura.Models.Configs;
using Aura.WPF.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Aura.WPF
{
    public class PersonaCompendiumLinkLabel : TextBlock, IAutoConfigUserControlWithLabel
    {
        public Dictionary<string, object?> Items { get; } = new Dictionary<string, object?>();

        public PersonaCompendiumLinkLabel()
        {
            Margin = new Thickness(50, 0, 0, 0);

            var hp = new Hyperlink();
            hp.Inlines.Add("Open Compendium");
            hp.Click += Hp_Click;
            Inlines.Add(hp);

            ReloadItems();
        }

        private void ReloadItems()
        {
            Items.Clear();
            // fill available persona
            var files = Directory.EnumerateFiles(AuraPaths.Compendium, "*.a2p").ToList();
            foreach (var file in files)
            {
                try
                {
                    var p = Persona.Load(file, false);
                    Items.Add(p.GivenName + " " + p.FamilyName, p.Id + ".a2p");
                }
                catch { }
            }
        }

        public event EventHandler? ValueChanged;

        private void Hp_Click(object sender, RoutedEventArgs e)
        {
            var compend = new PersonaCompendium();
            compend.SetWorkingDirectory(AuraPaths.Compendium);
            if (compend.ShowDialog() == true && File.Exists(compend.SelectedPersonaPath))
            {
                try
                {
                    ReloadItems();
                    var p = Persona.Load(compend.SelectedPersonaPath, true);
                    Tag = Path.GetFileName(compend.SelectedPersonaPath);
                    ValueChanged?.Invoke(this, new EventArgs());
                }
                catch
                {
                }
            }
        }

        public void LoadConfigurationValue(object? value)
        {
            Tag = value;
        }

        public string GetLabel()
        {
            var item = Items.Where(x => x.Value?.Equals(Tag) == true).FirstOrDefault();

            return string.IsNullOrEmpty(item.Key) ? "none" : item.Key;
        }

        public object? GetConfigurationValue()
        {
            return Tag;
        }
    }
}
