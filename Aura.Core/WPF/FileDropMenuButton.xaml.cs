﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for FileDropMenuButton.xaml
    /// </summary>
    public partial class FileDropMenuButton : UserControl
    {
        public string Title { get => lbTitle.Content?.ToString() ?? ""; set => lbTitle.Content = value; }
        public void SetIcon(string uri)
        {
            try
            {
                var bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.UriSource = new Uri(uri, UriKind.Absolute);
                bmp.EndInit();
                imgIcon.Source = bmp;
            }
            catch
            {
            }
        }

        public Color BackgroundIdle { get; set; } = Color.FromArgb(220, 190, 210, 230);
        public Color BorderIdle { get; set; } = Color.FromArgb(220, 95, 105, 115);
        public Color BackgroundHover { get; set; } = Colors.LightSkyBlue;
        public Color BorderHover { get; set; } = Colors.DeepSkyBlue;

        public FileDropMenuButton()
        {
            InitializeComponent();
            UpdateColor();
        }

        private void brdBackground_MouseEnter(object sender, MouseEventArgs e)
        {
            brdBackground.Background = new SolidColorBrush(BackgroundHover);
            brdBackground.BorderBrush = new SolidColorBrush(BorderHover);
        }

        private void brdBackground_MouseLeave(object sender, MouseEventArgs e)
        {
            brdBackground.Background = new SolidColorBrush(BackgroundIdle);
            brdBackground.BorderBrush = new SolidColorBrush(BorderIdle);
        }

        private void brdBackground_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                e.Handled = true;
                Clicked?.Invoke(this, EventArgs.Empty);
            }
        }

        public void UpdateColor()
        {
            brdBackground.Background = new SolidColorBrush(BackgroundIdle);
            brdBackground.BorderBrush = new SolidColorBrush(BorderIdle);
        }

        public event EventHandler? Clicked;
    }
}
