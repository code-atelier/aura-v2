﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public static class ServiceExtension
    {
        /// <summary>
        /// Creates a new message with this service as the sender.
        /// </summary>
        /// <param name="eventId">The id of the event.</param>
        /// <returns>New instance of <see cref="Message"/></returns>
        public static Message CreateMessage(this IService service, string eventId)
        {
            return new Message(service.Info.Id, eventId);
        }

        /// <summary>
        /// Creates a new message with this service as the sender.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventId">The id of the event.</param>
        /// <param name="body">The body of the message.</param>
        /// <returns>New instance of <see cref="Message"/></returns>
        public static Message<T> CreateMessage<T>(this IService service, string eventId, T? body = default)
        {
            return new Message<T>(service.Info.Id, eventId, body);
        }
    }
}
