﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public abstract class ServiceBase : IService
    {
        public abstract ExtensionInfo Info { get; }

        public abstract ServiceUserAction UserActions { get; }

        public ServiceState State { get; protected set; }

        public abstract Task Initialize();

        public async Task Shutdown()
        {
            if (State == ServiceState.Running || State == ServiceState.Busy)
            {
                try
                {
                    await OnShutdown();
                    State = ServiceState.Stopping;
                }
                catch
                {
                    State = ServiceState.Error;
                }
            }
        }

        public async Task Start()
        {
            if (State != ServiceState.Stopped) return;
            State = ServiceState.Starting;
            try
            {
                await OnStart();
                State = ServiceState.Running;
                _ = Task.Run(DoServe);
            }
            catch
            {
                State = ServiceState.Stopped;
            }
        }

        private async Task DoServe()
        {
            try
            {
                while (State != ServiceState.Stopping && State != ServiceState.Error && State != ServiceState.Stopped)
                {
                    try
                    {
                        await Serve();
                    }
                    finally
                    {
                        if (State != ServiceState.Stopping)
                        {
                            State = ServiceState.Running;
                        }
                    }
                }
            }
            catch
            {
                State = ServiceState.Error;
            }
            finally
            {
                State = ServiceState.Stopped;
            }
        }

        /// <summary>
        /// Main logic of the service. This method will be called multiple times until the service stopped. This method is always asynchronously called.
        /// </summary>
        public abstract Task Serve();

        public virtual async Task OnStart()
        {
            await Task.Run(() => { });
        }

        public virtual async Task OnShutdown()
        {
            await Task.Run(() => { });
        }
    }
}
