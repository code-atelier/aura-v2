﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    /// <summary>
    /// An enum of the priorities of a <see cref="IMessageReceiver"/>
    /// </summary>
    public enum MessageReceiverPriority
    {
        /// <summary>
        /// <see cref="Critical"/> will run first until it completes its tasks before any other priority.
        /// </summary>
        Critical = 0,

        /// <summary>
        /// <see cref="High"/> will run after all <see cref="Critical"/> had ran to completion.
        /// </summary>
        High = 1,

        /// <summary>
        /// <see cref="Medium"/> will run after all <see cref="Critical"/> had ran to completion and all <see cref="High"/> had started.
        /// </summary>
        Medium = 2,

        /// <summary>
        /// <see cref="Low"/> will run after other priorities had ran to completion.
        /// </summary>
        Low = 3,
    }
}
