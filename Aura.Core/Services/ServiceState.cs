﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public enum ServiceState
    {
        Stopped,
        Starting,
        Running,
        Busy,
        Stopping,
        Error,
    }
}
