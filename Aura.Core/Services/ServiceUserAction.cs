﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public enum ServiceUserAction
    {
        None = 0,

        Start = 1,
        Stop = 2,
        Restart = 4,

        Configure = 8,
        ViewLog = 16,

        All = 255,
    }
}
