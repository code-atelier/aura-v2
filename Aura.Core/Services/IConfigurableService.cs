﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    /// <summary>
    /// Describes a service with configuration window.
    /// </summary>
    public interface IConfigurableService
    {
        /// <summary>
        /// Opens the configuration window of this service.
        /// </summary>
        Task OpenConfigurationWindow();
    }
}
