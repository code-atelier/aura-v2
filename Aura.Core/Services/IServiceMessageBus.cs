﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    /// <summary>
    /// Describes how a ServiceMessageBus should behave.
    /// </summary>
    public interface IServiceMessageBus
    {
        /// <summary>
        /// Subscribes to the message bus.
        /// </summary>
        /// <param name="receiver">The receiver of the messages.</param>
        /// <param name="eventId">The event id of receivable messages. If null, accept all messages regardless of the event.</param>
        /// <param name="senderId">The sender id of receivable messages. If null, accept all messages regardless of the sender.</param>
        void Subscribe(IMessageReceiver receiver, string? eventId = null, string? senderId = null);

        /// <summary>
        /// Unsubscribe from the message bus.
        /// </summary>
        /// <param name="receiver">The receiver of the messages.</param>
        /// <param name="eventId">The event id of subscription.</param>
        /// <param name="senderId">The sender id of subscription.</param>
        void Unsubscribe(IMessageReceiver receiver, string? eventId = null, string? senderId = null);

        /// <summary>
        /// Unsubscribe from all events in the message bus.
        /// </summary>
        /// <param name="receiver">The receiver of the messages.</param>
        void UnsubscribeAll(IMessageReceiver receiver);

        /// <summary>
        /// Send a message asynchronously and receives the replies.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>Message replies.</returns>
        Task<IEnumerable<Message>> SendMessageAsync(Message message, CancellationToken cancellationToken, bool expectNoReturn = false);

        /// <summary>
        /// Send a message asynchronously and receives the replies.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <returns>Message replies.</returns>
        Task<IEnumerable<Message>> SendMessageAsync(Message message, bool expectNoReturn = false);

        /// <summary>
        /// Send a message synchronously and receives the replies.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <returns>Message replies.</returns>
        IEnumerable<Message> SendMessage(Message message);

        /// <summary>
        /// Send a message asynchronously and receives the replies.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>Message replies.</returns>
        Task<IEnumerable<Message>> SendMessageToAsync(Message message, string recipientId, CancellationToken cancellationToken);

        /// <summary>
        /// Send a message asynchronously and receives the replies.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <returns>Message replies.</returns>
        Task<IEnumerable<Message>> SendMessageToAsync(Message message, string recipientId);

        /// <summary>
        /// Send a message synchronously and receives the replies.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <returns>Message replies.</returns>
        IEnumerable<Message> SendMessageTo(Message message, string recipientId);
    }
}
