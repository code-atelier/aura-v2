﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    /// <summary>
    /// Provides a way for services to communicate with other services or components.
    /// </summary>
    public class ServiceMessageBus : IServiceMessageBus
    {
        private Dictionary<string, List<IMessageReceiver>> Subscriptions { get; } = new Dictionary<string, List<IMessageReceiver>>();

        public IEnumerable<Message> SendMessage(Message message)
        {
            var cts = new CancellationTokenSource();
            var task = SendMessageAsync(message, cts.Token);
            task.Wait();
            return task.Result;
        }

        public async Task<IEnumerable<Message>> SendMessageAsync(Message message, CancellationToken cancellationToken, bool expectNoReturn = false)
        {
            var keys = new[]
            {
                SubscriptionKey(message.EventId, message.SenderId),
                SubscriptionKey(message.EventId, null),
                SubscriptionKey(null, message.SenderId),
                SubscriptionKey(null, null),
            };
            var receivers = Subscriptions.Where(x => keys.Contains(x.Key)).SelectMany(x => x.Value).OrderBy(x => x.Priority);

            var tasks = new List<Task<Message?>>();
            var executionBatches = new[] { MessageReceiverPriority.Critical, MessageReceiverPriority.Medium, MessageReceiverPriority.Low };
            MessageReceiverPriority? previousPriority = null;
            foreach (var batch in executionBatches)
            {
                var batchReceivers = receivers.Where(x => x.Priority <= batch && (previousPriority == null || x.Priority > previousPriority));
                foreach (var receiver in batchReceivers)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }
                    if (!receiver.CanReceiveMessage)
                    {
                        tasks.Add(Task.Run<Message?>(() =>
                        {
                            var reply = message.CreateReply(receiver.Id);
                            reply.IsSuccess = false;
                            reply.ErrorMessage = "Receiver is not ready";
                            return reply;
                        }));
                        continue;
                    }
                    var task = receiver.ReceiveMessageAsync(message, cancellationToken);
                    tasks.Add(task);
                }
                if (!expectNoReturn)
                    await Task.WhenAll(tasks);
                previousPriority = batch;
            }
            if (expectNoReturn) return new List<Message>();
            return tasks.Where(x => x.IsCompletedSuccessfully).Select(x => x.Result).OfType<Message>();
        }

        public async Task<IEnumerable<Message>> SendMessageAsync(Message message, bool expectNoReturn = false)
        {
            var cts = new CancellationTokenSource();
            return await SendMessageAsync(message, cts.Token, expectNoReturn);
        }

        private string SubscriptionKey(string? eventId, string? senderId)
        {
            if (string.IsNullOrWhiteSpace(eventId))
            {
                eventId = "*";
            }
            else
            {
                eventId = eventId.Trim().ToLower();
            }
            if (string.IsNullOrWhiteSpace(senderId))
            {
                senderId = "*";
            }
            else
            {
                senderId = senderId.Trim().ToLower();
            }
            return senderId + "::" + eventId;
        }

        public void Subscribe(IMessageReceiver receiver, string? eventId = null, string? senderId = null)
        {
            var key = SubscriptionKey(eventId, senderId);
            if (!Subscriptions.ContainsKey(key))
            {
                Subscriptions[key] = new List<IMessageReceiver>();
            }
            if (!Subscriptions[key].Contains(receiver))
            {
                Subscriptions[key].Add(receiver);
            }
        }

        public void Unsubscribe(IMessageReceiver receiver, string? eventId = null, string? senderId = null)
        {
            var key = SubscriptionKey(eventId, senderId);
            if (Subscriptions.ContainsKey(key))
            {
                Subscriptions[key].Remove(receiver);
            }
        }

        public void UnsubscribeAll(IMessageReceiver receiver)
        {
            foreach (var subs in Subscriptions.Values)
            {
                subs.Remove(receiver);
            }
        }

        public async Task<IEnumerable<Message>> SendMessageToAsync(Message message, string recipientId, CancellationToken cancellationToken)
        {
            var tasks = new List<Task<Message?>>();
            var receivers = Subscriptions.Where(x => x.Value.Any(w => w.Id == recipientId)).Select(x => x.Value.First(w => w.Id == recipientId)).Distinct().OrderBy(x => x.Priority).ToList();
            foreach (var receiver in receivers)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                var task = receiver.ReceiveMessageAsync(message, cancellationToken);
                tasks.Add(task);
            }
            await Task.WhenAll(tasks);
            return tasks.Where(x => x.IsCompletedSuccessfully).Select(x => x.Result).OfType<Message>();
        }

        public async Task<IEnumerable<Message>> SendMessageToAsync(Message message, string recipientId)
        {
            var cts = new CancellationTokenSource();
            return await SendMessageToAsync(message, recipientId, cts.Token);
        }

        public IEnumerable<Message> SendMessageTo(Message message, string recipientId)
        {
            var cts = new CancellationTokenSource();
            var task = SendMessageToAsync(message, recipientId, cts.Token);
            task.Wait();
            return task.Result;
        }
    }
}
