﻿using System.Windows;

namespace Aura.Services
{
    /// <summary>
    /// Describes how a service should behave.
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// Gets the information of this service.
        /// </summary>
        ExtensionInfo Info { get; }

        /// <summary>
        /// Gets the available user actions.
        /// </summary>
        ServiceUserAction UserActions { get; }

        /// <summary>
        /// Initializes this service. Only called once when the service is created, this will block splash screen.
        /// </summary>
        Task Initialize();

        /// <summary>
        /// Starts this service, this won't block splash screen.
        /// </summary>
        Task Start();

        /// <summary>
        /// Shuts down this service.
        /// </summary>
        Task Shutdown();

        /// <summary>
        /// Gets the current state of the service.
        /// </summary>
        ServiceState State { get; }
    }

    public interface IServiceWithConfiguration
    {
        Window CreateConfigurationWindow();
    }

    public interface IServiceWithLog
    {
        ServiceLog Log { get; }
    }
}