﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Media.Animation;

namespace Aura.Services
{
    /// <summary>
    /// A message that passed through the <see cref="ServiceMessageBus"/> by services and components.
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Gets the id of this message.
        /// </summary>
        public Guid Id { get; } = Guid.NewGuid();
        /// <summary>
        /// Gets the sender id of this message.
        /// </summary>
        public string SenderId { get; }
        /// <summary>
        /// Gets the event id of this message.
        /// </summary>
        public string EventId { get; }
        /// <summary>
        /// Gets the header of this message.
        /// </summary>
        public Dictionary<string, string> Headers { get; } = new Dictionary<string, string>();

        public bool IsSuccess { get; set; } = true;

        public string? ErrorMessage { get; set; }

        public Message(string senderId, string eventId)
        {
            SenderId = senderId.Trim().ToLower();
            EventId = eventId.Trim().ToLower();
        }

        /// <summary>
        /// Gets the body of this message.
        /// </summary>
        /// <typeparam name="T">The type of the body of this message.</typeparam>
        /// <returns>The body of the message as <typeparamref name="T"/></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public T? GetBody<T>()
        {
            if (this is Message<T> mt)
            {
                return mt.Body;
            }
            if (this is IMessageWithBody mwb && mwb.GetBody() is T body)
            {
                return body;
            }
            throw new InvalidOperationException($"This message does not have a body with type '{typeof(T).FullName}'");
        }

        /// <summary>
        /// Gets the body of this message.
        /// </summary>
        /// <typeparam name="T">The type of the body of this message.</typeparam>
        /// <returns>The body of the message as <typeparamref name="T"/></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public T? GetJsonBody<T>()
        {
            if (TryGetBodyAsString() is string json)
            {
                return JsonSerializer.Deserialize<T>(json);
            }
            if (TryGetBodyAsRaw() is byte[] rawJson)
            {
                var rjson = Encoding.UTF8.GetString(rawJson);
                return JsonSerializer.Deserialize<T>(rjson);
            }
            throw new InvalidOperationException($"This message does not have a body with type 'string' or 'byte[]'");
        }

        public byte[]? TryGetBodyAsRaw()
        {
            try
            {
                return GetBody<byte[]>();
            }
            catch
            {
                return null;
            }
        }

        public string? TryGetBodyAsString()
        {
            try
            {
                return GetBody<string>();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Checks whether this message has a body of type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">The type of the body of this message.</typeparam>
        /// <returns><see cref="true"/> if this message has a body of type <typeparamref name="T"/>, otherwise <see cref="false"/>.</returns>
        public bool HasBody<T>()
        {
            if (this is Message<T> || (this is IMessageWithBody mwb && mwb.GetBody() is T))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a reply for this message.
        /// </summary>
        /// <param name="senderId">The id of the sender.</param>
        /// <returns>New instance of <see cref="Message"/> as a reply to this message.</returns>
        public Message CreateReply(string senderId)
        {
            var msg = new Message(senderId, "re:" + EventId);
            msg.Headers["ReplyOf"] = Id.ToString("n");
            return msg;
        }

        /// <summary>
        /// Creates a reply for this message with a body.
        /// </summary>
        /// <typeparam name="T">The type of the body.</typeparam>
        /// <param name="senderId">The id of the sender.</param>
        /// <param name="body">The body of the message.</param>
        /// <returns>New instance of <see cref="Message{T}"/> as a reply to this message.</returns>
        public Message<T> CreateReply<T>(string senderId, T? body = default)
        {
            var msg = new Message<T>(senderId, "re:" + EventId, body);
            msg.Headers["ReplyOf"] = Id.ToString("n");
            return msg;
        }
    }

    /// <summary>
    /// A message that passed through the <see cref="ServiceMessageBus"/> by services and components.
    /// </summary>
    public class Message<T> : Message, IMessageWithBody
    {
        /// <summary>
        /// Gets the body of the message.
        /// </summary>
        public T? Body { get; set; }

        public string Type { get; set; } = "raw/binary";

        public Message(string senderId, string eventId, T? body = default, string? type = null) : base(senderId, eventId)
        {
            Body = body;
            if (type != null) Type = type;
        }

        public object? GetBody()
        {
            return Body;
        }
    }

    public interface IMessageWithBody
    {
        object? GetBody();

        string Type { get; }
    }

    public static class MessageToChainExtension
    {
        public static MessageFunctionChain When(this Message message)
        {
            return new MessageFunctionChain(message);
        }
    }

    public class MessageFunctionChain
    {
        public Message Message { get; }

        public Message? Reply { get; private set; }

        public bool Handled { get; private set; }

        public MessageFunctionChain(Message message)
        {
            Message = message;
        }

        public MessageFunctionChain FileDropPreview(Func<MessageFunctionChainArgument<FileDropEventBody>, Message?> callback)
        {
            if (Handled) return this;
            if (Message.EventId == InternalEvents.FileDrop && Message.GetBody<FileDropEventBody>() is FileDropEventBody body)
            {
                var arg = CreateArg(body);
                Reply = callback.Invoke(arg);
                Handled = Handled || arg.Handled;
            }
            return this;
        }

        public MessageFunctionChain FileDropExecute(Func<MessageFunctionChainArgument<FileDropExecuteEventBody>, Message?> callback)
        {
            if (Handled) return this;
            if (Message.EventId == InternalEvents.ExecuteFileDrop && Message.GetBody<FileDropExecuteEventBody>() is FileDropExecuteEventBody body)
            {
                var arg = CreateArg(body);
                Reply = callback.Invoke(arg);
                Handled = Handled || arg.Handled;
            }
            return this;
        }

        private static MessageFunctionChainArgument<T> CreateArg<T>(T body)
        {
            return new MessageFunctionChainArgument<T>(body);
        }
    }

    public class MessageFunctionChainArgument<T> { 

        public bool Handled { get; set; } = false;

        public T Body { get; }

        public MessageFunctionChainArgument(T body)
        {
            Body = body;
        }
    }
}
