﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public abstract class PassiveServiceBase : IService
    {
        public abstract ExtensionInfo Info { get; }

        public abstract ServiceUserAction UserActions { get; }

        public ServiceState State { get; protected set; }

        public virtual async Task Initialize()
        {
            await Task.Run(() => { });
        }

        public async Task Shutdown()
        {
            if (State == ServiceState.Running || State == ServiceState.Busy)
            {
                try
                {
                    await OnShutdown();
                    State = ServiceState.Stopped;
                }
                catch
                {
                    State = ServiceState.Error;
                }
            }
        }

        public async Task Start()
        {
            if (State != ServiceState.Stopped) return;
            await Task.Run(async () =>
            {
                State = ServiceState.Starting;
                try
                {
                    await OnStart();
                    State = ServiceState.Running;
                }
                catch
                {
                    State = ServiceState.Stopped;
                    throw;
                }
            });
        }

        public virtual async Task OnStart()
        {
            await Task.Run(() => { });
        }

        public virtual async Task OnShutdown()
        {
            await Task.Run(() => { });
        }
    }
}
