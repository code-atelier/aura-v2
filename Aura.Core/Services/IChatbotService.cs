﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public interface IChatbotService
    {
        Task<ChatbotServiceBotMessage?> GetGreeting();

        Task<bool> LoadChatHistory();

        Queue<ChatbotServiceMessage> ChatHistory { get; }

        Task<IEnumerable<ChatbotServiceBotMessage>?> SendMessage(ChatbotServiceUserMessage message);
    }

    public interface IRetryableChatbotService
    {
        Task<ChatbotServiceBotMessage> GetNewResponseFor(ChatbotServiceBotMessage message);
    }
}
