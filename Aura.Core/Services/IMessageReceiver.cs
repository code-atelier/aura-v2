﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    /// <summary>
    /// Describes how a message receiver should behave.
    /// </summary>
    public interface IMessageReceiver
    {
        /// <summary>
        /// Gets the priority of this receiver.
        /// </summary>
        MessageReceiverPriority Priority { get; }

        /// <summary>
        /// Gets the id of this receiver.
        /// </summary>
        string Id { get; }

        bool CanReceiveMessage { get; }

        /// <summary>
        /// Receives a message from bus.
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <returns>Reply <see cref="Message"/> or null if no reply.</returns>
        Task<Message?> ReceiveMessageAsync(Message message, CancellationToken cancellationToken);
    }
}
