﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace Aura.Services
{
    public class ServiceLog
    {
        public string Path { get; }

        public IService Service { get; }

        public ServiceLog(IService service, string fileName)
        {
            Service = service;
            Path = System.IO.Path.Combine(AuraLogger.DefaultLogPath, service.Info.Id, fileName);
            var dir = System.IO.Path.GetDirectoryName(Path);
            if (dir != null && !Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }

        public void Begin(string? message = null)
        {
            message = message ?? $"Logging for '{Service.Info.Id}' started.";
            Write(ServiceLogLevel.Info, message);
        }

        public void End(string? message = null)
        {
            message = message ?? $"Logging for '{Service.Info.Id}' ended.";
            Write(ServiceLogLevel.Info, message);
        }

        public void Write(ServiceLogLevel level, string message)
        {
            try
            {
                var line = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " - " + level.ToString() + ": " + message.ReplaceLineEndings(" ");
                File.AppendAllText(Path, line + Environment.NewLine);
            }
            catch { }
        }

        public void Debug(string message)
        {
            Write(ServiceLogLevel.Debug, message);
        }

        public void Info(string message)
        {
            Write(ServiceLogLevel.Info, message);
        }

        public void Information(string message)
        {
            Write(ServiceLogLevel.Info, message);
        }

        public void Warning(string message)
        {
            Write(ServiceLogLevel.Warning, message);
        }

        public void Error(string message)
        {
            Write(ServiceLogLevel.Error, message);
        }

        public void Error(Exception exception)
        {
            try
            {
                var line = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " - " + ServiceLogLevel.Error.ToString() + ": Exception '" + exception.GetType().Name + "' was thrown: " + exception.Message.ReplaceLineEndings(" ");
                if (!string.IsNullOrWhiteSpace(exception.StackTrace))
                {
                    line += Environment.NewLine + "Stack Trace:";
                    line += Environment.NewLine + exception.StackTrace;
                }
                if (exception.InnerException != null)
                {
                    line += Environment.NewLine + InnerException(exception.InnerException);
                }
                File.AppendAllText(Path, line + Environment.NewLine);
            }
            catch { }
        }

        private string InnerException(Exception exception, int level = 0)
        {
            var entry = "Inner Exception '" + exception.GetType().Name + "' #" + level + ": " + exception.Message;
            entry += Environment.NewLine + "Stack Trace:";
            entry += Environment.NewLine + exception.StackTrace;
            if (exception.InnerException != null)
            {
                entry += Environment.NewLine + InnerException(exception.InnerException, level + 1);
            }
            return entry;
        }

        public List<ServiceLogLine> GetLines(ServiceLogLevel? minimumLevel = null, DateTime? from = null, DateTime? to = null)
        {
            var res = new List<ServiceLogLine>();
            try
            {
                var lines = File.ReadAllLines(Path);
                foreach (var line in lines)
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;
                    try
                    {
                        var spl1 = line.Split(": ", 2, StringSplitOptions.TrimEntries);
                        if (spl1.Length == 2)
                        {
                            var spl2 = spl1[0].Split(" - ", 2, StringSplitOptions.TrimEntries);
                            if (spl2.Length == 2)
                            {
                                var entry = new ServiceLogLine()
                                {
                                    Level = Enum.Parse<ServiceLogLevel>(spl2[1]),
                                    Message = spl1[1],
                                    Timestamp = DateTime.Parse(spl2[0])
                                };
                                if (
                                    (minimumLevel == null || minimumLevel.Value <= entry.Level)
                                    && (from == null || from.Value <= entry.Timestamp)
                                    && (to == null || to.Value >= entry.Timestamp)
                                )
                                {
                                    res.Add(entry);
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
            catch { }
            return res;
        }
    }

    public class ServiceLogLine
    {
        public ServiceLogLevel Level { get; set; }

        public DateTime Timestamp { get; set; }

        public string Message { get; set; } = string.Empty;

        public string TimestampString { get => Timestamp.ToString("yyyy-MM-dd HH:mm:ss.fff"); }

        public override string ToString()
        {
            return Timestamp.ToString("yyyy-MM-dd HH:mm:ss.fff") + " - " + Level.ToString() + ": " + Message.ReplaceLineEndings(" ");
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            return obj is ServiceLogLine sll && sll.GetHashCode() == GetHashCode();
        }
    }

    public enum ServiceLogLevel
    {
        Debug = 0,
        Info = 1,
        Warning = 2,
        Error = 3,
    }
}
