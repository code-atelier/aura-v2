﻿using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.IO;

namespace Aura.API
{
    public class APIMessage
    {
        /// <summary>
        /// Get the headers of this message.
        /// </summary>
        public Dictionary<string, string> Headers { get; } = new Dictionary<string, string>();

        /// <summary>
        /// Gets the protocol version used by this message.
        /// </summary>
        public Version ProtocolVersion { get; private set; } = Version;

        /// <summary>
        /// Gets or sets the content of this message.
        /// </summary>
        public APIMessageContent[] Contents { get; set; } = new APIMessageContent[0];

        /// <summary>
        /// Gets the head bytes of API Messages.
        /// </summary>
        public static byte[] Head { get; } = Encoding.UTF8.GetBytes("AuraAPI");

        /// <summary>
        /// Gets the content EOF bytes of API Messages.
        /// </summary>
        public static byte[] ContentEOF { get; } = Encoding.UTF8.GetBytes("<EOF>");

        /// <summary>
        /// Gets the current version of API messaging protocol.
        /// </summary>
        public static Version Version { get; } = new Version(1, 0);

        /// <summary>
        /// Process a client and get an API message.
        /// </summary>
        /// <returns>API Message</returns>
        public static async Task<APIMessage> FromClient(TcpClient client)
        {
            return await Task.Run(() =>
            {
                Stream stream = client.GetStream();
                return ReadMessage(stream);
            });
        }

        private static bool CompareBytes(byte[] a, byte[] b)
        {
            if (a == null || b == null) return false;
            if (a.Length != b.Length) return false;
            for (var i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i]) return false;
            }
            return true;
        }

        public static APIMessage ReadMessage(Stream stream)
        {
            var msg = new APIMessage();

            // read head
            var data = new byte[Head.Length];
            var read = stream.Read(data, 0, data.Length);
            if (read != data.Length) throw new InvalidDataException("Invalid Head");
            if (!CompareBytes(data, Head)) throw new InvalidDataException("Invalid Head");

            // read 2 bytes of version
            try
            {
                var major = stream.ReadByte();
                var minor = stream.ReadByte();
                msg.ProtocolVersion = new Version(major, minor);
            }
            catch { }
            if (msg.ProtocolVersion != Version)
            {
                throw new InvalidDataException("Protocol Version Mismatched");
            }

            // read header length
            data = new byte[4];
            read = stream.Read(data, 0, data.Length);
            if (read != data.Length) throw new InvalidDataException("Invalid Header Length");
            if (!BitConverter.IsLittleEndian) Array.Reverse(data);
            var headerLength = BitConverter.ToInt32(data, 0);
            if (headerLength < 0) throw new InvalidDataException("Invalid Header Length");

            // read headers (JSON)
            data = new byte[headerLength];
            read = stream.Read(data, 0, data.Length);
            if (read != data.Length) throw new InvalidDataException("Invalid Header");
            try
            {
                var headerJson = Encoding.UTF8.GetString(data);
                var headers = JsonSerializer.Deserialize<Dictionary<string, string>>(headerJson);
                if (headers == null) throw new InvalidDataException("Invalid Header Format");
                foreach (var header in headers)
                {
                    msg.Headers[header.Key.ToLower()] = header.Value;
                }
            }
            catch (InvalidDataException)
            {
                throw;
            }
            catch
            {
                throw new InvalidDataException("Corrupted Header");
            }

            var contentLengthStr = msg.Headers.Keys.Any(x => x.ToLower() == "content-length") ? msg.Headers[msg.Headers.Keys.First(x => x.ToLower() == "content-length")] : "0";
            var contentLengths = contentLengthStr.Split(';', StringSplitOptions.TrimEntries);
            var contentTypeStr = msg.Headers.Keys.Any(x => x.ToLower() == "content-type") ? msg.Headers[msg.Headers.Keys.First(x => x.ToLower() == "content-type")] : "raw/binary";
            var contentTypes = contentLengthStr.Split(';', StringSplitOptions.TrimEntries);
            var contentProvidersStr = msg.Headers.Keys.Any(x => x.ToLower() == "content-provider") ? msg.Headers[msg.Headers.Keys.First(x => x.ToLower() == "content-provider")] : "";
            var contentProviders = contentLengthStr.Split(';', StringSplitOptions.TrimEntries);

            if (contentLengths.Length != contentTypes.Length || contentTypes.Length != contentProviders.Length)
            {
                throw new InvalidDataException("Content Descriptors Have Different Count");
            }
            var contents = new List<APIMessageContent>();
            for(var i = 0; i < contentLengths.Length; i++)
            {
                int contentLength;
                if (int.TryParse(contentLengths[i], out contentLength))
                {
                    if (contentLength < 0) throw new InvalidDataException("Invalid Content Length");
                    if (contentLength > 0)
                    {
                        // read content
                        data = new byte[contentLength];
                        stream.Read(data);
                        contents.Add(new APIMessageContent(contentProviders[i], contentTypes[i], data));
                    }
                    else
                    {
                        contents.Add(new APIMessageContent(contentProviders[i], contentTypes[i], new byte[0]));
                    }
                }
            }
            msg.Contents = contents.ToArray();

            // read EOF
            data = new byte[ContentEOF.Length];
            read = stream.Read(data, 0, data.Length);
            if (read != data.Length) throw new InvalidDataException("EOF Not Found");
            if (!CompareBytes(data, ContentEOF)) throw new InvalidDataException("EOF Expected");

            return msg;
        }

        public void WriteMessage(Stream stream)
        {
            // write head
            stream.Write(Head);
            // write ver
            stream.WriteByte((byte)ProtocolVersion.Major);
            stream.WriteByte((byte)ProtocolVersion.Minor);

            // prepare header
            var headers = new Dictionary<string, string>();
            foreach (var header in Headers)
            {
                headers[header.Key.ToLower()] = header.Value;
            }
            headers["content-length"] = string.Join(";", Contents.Select(x => x.Content.Length.ToString()));
            headers["content-type"] = string.Join(";", Contents.Select(x => x.ContentType));
            headers["content-provider"] = string.Join(";", Contents.Select(x => x.Provider));

            var headerJson = JsonSerializer.Serialize(headers);
            var headerData = Encoding.UTF8.GetBytes(headerJson);

            // write header length
            var data = BitConverter.GetBytes(headerData.Length);
            if (!BitConverter.IsLittleEndian) Array.Reverse(data);
            stream.Write(data);

            // write header
            stream.Write(headerData);

            // write content
            foreach(var content in Contents)
            {
                stream.Write(content.Content);
            }

            // write eof
            stream.Write(ContentEOF);
        }

        public async Task ToClient(TcpClient client)
        {
            await Task.Run(() =>
            {
                Stream stream = client.GetStream();
                WriteMessage(stream);
            });
        }
    }

    public class APIMessageContent
    {
        public string Provider { get; set; } = "";
        public string ContentType { get; set; } = "raw/binary";

        public byte[] Content { get; set; } = new byte[0];

        public APIMessageContent()
        {

        }

        public APIMessageContent(string provider, string contentType, byte[] content)
        {
            ContentType = contentType;
            Content = content;
            Provider = provider;
        }
    }
}
