﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public interface IConsoleExtension
    {
        ExtensionInfo Info { get; }

        /// <summary>
        /// Case-insensitive command id
        /// </summary>
        string Command { get; }

        Task<string> RunCommand(ConsoleCommand command);
    }
    public interface IConsoleExtensionWithHelp: IConsoleExtension
    {
        public string Help { get; }
    }
}
