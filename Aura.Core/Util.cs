﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices.ActiveDirectory;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Aura
{
    public static class Util
    {
        public static string? SanitizeId(string? id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            id = id.Trim().ToLower();
            string pattern = @"[^a-z0-9\.\:\/\\\-_]";
            Regex regex = new Regex(pattern);
            id = regex.Replace(id, "");
            return id;
        }

        public static string? SanitizeStrictId(string? id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            id = id.Trim();
            string pattern = @"[^a-zA-Z0-9\._]";
            Regex regex = new Regex(pattern);
            id = regex.Replace(id, "");
            return id;
        }

        public static string? SanitizeFileName(string filename)
        {
            return filename.Trim()
                .Replace("\\", "")
                .Replace(":", "")
                .Replace("*", "")
                .Replace("?", "")
                .Replace("\"", "")
                .Replace("<", "")
                .Replace(">", "")
                .Replace("|", "")
                .Replace("/", "");
        }

        public static double CalculateDecayedWeight(DateTime recordTime, double weight, double slidingWindowSeconds, Func<double, double>? decayFunction = null)
        {
            return CalculateDecayedWeight(recordTime, weight, DateTime.Now.AddSeconds(-slidingWindowSeconds), decayFunction);
        }

        public static double CalculateDecayedWeight(DateTime recordTime, double weight, DateTime landmarkTime, Func<double, double>? decayFunction = null)
        {
            var now = DateTime.Now;
            if (recordTime <= landmarkTime) return 0;
            if (recordTime >= now) return weight;

            var tNow = (now - landmarkTime).TotalSeconds;
            var wNow = decayFunction != null ? decayFunction(tNow) : DecayFunction(tNow);
            var tArrival = (recordTime - landmarkTime).TotalSeconds;
            var wArrival = decayFunction != null ? decayFunction(tArrival) : DecayFunction(tArrival);
            return wArrival / wNow * weight;
        }

        public static double DecayFunction(double value)
        {
            return value;
        }

        public static string CreateResourceUri(Assembly assembly, string path)
        {
            var assemblyName = assembly.GetName();
            var packName = assemblyName.Name ?? assemblyName.FullName;
            return CreateResourceUri(packName, path);
        }

        public static string CreateResourceUri(string assembly, string path)
        {
            path = path.TrimStart('/');
            if (assembly.StartsWith("/"))
                return assembly + ";component/" + path;
            return "pack://application:,,,/" + assembly + ";component/" + path;
        }

        public static string CreateResourceUri(string path)
        {
            var assembly = Assembly.GetCallingAssembly();
            return CreateResourceUri(assembly, path);
        }
        public static void ShowInExplorer(string path)
        {
            if (File.Exists(path) || Directory.Exists(path))
            {
                path = Path.GetFullPath(path);
                try
                {
                    Process.Start("explorer.exe", string.Format("/select,\"{0}\"", path));
                }
                catch { }
            }
        }

        public static void OpenFile(string path, bool useShellExecute = true)
        {
            var validSchemes = new string[] { "http", "https" };
            if (Uri.TryCreate(path, UriKind.Absolute, out Uri? uri) && validSchemes.Contains(uri.Scheme.ToLower()))
            {
                try
                {
                    Process.Start(
                        new ProcessStartInfo()
                        {
                            UseShellExecute = useShellExecute,
                            FileName = path,
                        }
                    );
                }
                catch { }
            }
            else if (File.Exists(path) || Directory.Exists(path))
            {
                path = Path.GetFullPath(path);
                try
                {
                    Process.Start(
                        new ProcessStartInfo()
                        {
                            WorkingDirectory = Path.GetDirectoryName(path),
                            UseShellExecute = useShellExecute,
                            FileName = path,
                        }
                    );
                }
                catch { }
            }
        }

        public static string GetTempDirectory(string? path = null)
        {
            path = path ?? Guid.NewGuid().ToString("n");
            var dir = Path.Combine(Path.GetTempPath(), path);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return dir;
        }

        public static Rect ScaledDestinationRectangle(Rect sourceRectange, Rect destinationRectange)
        {
            var w = sourceRectange.Width;
            var h = sourceRectange.Height;

            var dw = destinationRectange.Width;
            var dh = destinationRectange.Height;

            var scale = Math.Min(dw / w, dh / h);

            var sw = scale * w;
            var sh = scale * h;
            return new Rect(destinationRectange.X + (destinationRectange.Width - sw) / 2, destinationRectange.Y + (destinationRectange.Height - sh) / 2, sw, sh);
        }

        public static bool ArrayEqual<T>(T[] a, T[] b)
        {
            if (a == null && b == null) return true;
            if (a == null || b == null) return false;
            if (a.Length != b.Length) return false;
            for (var i = 0; i < a.Length; i++)
            {
                if (a[i] == null && b[i] == null) continue;
                if (a[i] == null || b[i] == null) return false;
                if (a[i]?.Equals(b[i]) != true)
                    return false;
            }
            return true;
        }

        public static byte[] PrepByteArray(byte[] data, bool isLittleEndian)
        {
            if (BitConverter.IsLittleEndian != isLittleEndian)
            {
                var buf = new byte[data.Length];
                Array.Copy(data, buf, buf.Length);
                Array.Reverse(buf);
                return buf;
            }
            return data;
        }

        public static async Task RunHyperThreads<T>(int threadCount, IEnumerable<T> objects, Action<T> callback)
        {
            await Task.Run(() =>
            {
                double objCount = objects.Count();
                if (objCount == 0) return;
                int takeObj = (int)Math.Ceiling(objCount / threadCount);
                int index = 0;
                List<Task> tasks = new List<Task>();
                while (tasks.Count < takeObj && index < objCount)
                {
                    T p = objects.ElementAt(index);
                    tasks.Add(Task.Run(() =>
                    {
                        try
                        {
                            callback.Invoke(p);
                        }
                        catch { }
                    }
                    ));
                    index++;
                    Task.WaitAny(tasks.ToArray());
                    tasks.RemoveAll(x => x.Status == TaskStatus.Faulted || x.Status == TaskStatus.RanToCompletion || x.Status == TaskStatus.Canceled);
                }
                Task.WaitAll(tasks.ToArray());
            });
        }

        public static string FormatByteSize(int? length)
        {
            if (length == null) return "-";
            string[] labels = new string[]
            {
                "B", "KiB", "MiB", "GiB", "TiB", "PiB"
            };
            double rem = length.Value;
            int idx = 0;
            while (rem > 1000 && idx < labels.Length - 1)
            {
                rem /= 1024.0;
                idx++;
            }

            return Math.Round(rem, 1) + " " + labels[idx];
        }
    }

    public static class Extensions
    {

    }
}
