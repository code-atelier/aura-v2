﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Aura
{
    public class ImageCache : IDictionary<Guid, ImageSource?>
    {
        private Dictionary<Guid, ImageSource> Cache { get; } = new Dictionary<Guid, ImageSource>();

        public ICollection<Guid> Keys => Cache.Keys;

        public ICollection<ImageSource?> Values => Cache.Values.Cast<ImageSource?>().ToList();

        public int Count => Cache.Count;

        public bool IsReadOnly => false;

        public ImageSource? this[Guid key]
        {
            get => Cache.ContainsKey(key) ? Cache[key] : null; set
            {
                Add(key, value);
            }
        }

        public void Add(Guid key, ImageSource? value)
        {
            if (Cache.ContainsKey(key))
            {
                Remove(key);
            }
            if (value == null)
            {
                Cache.Remove(key);
            }
            else
            {
                Cache[key] = value;
            }
        }

        public bool ContainsKey(Guid key)
        {
            return Cache.ContainsKey(key);
        }

        public bool Remove(Guid key)
        {
            if (Cache.ContainsKey(key))
            {
                Dispose(Cache[key]);
            }
            return Cache.Remove(key);
        }

        public bool TryGetValue(Guid key, [MaybeNullWhen(false)] out ImageSource? value)
        {
            return Cache.TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<Guid, ImageSource?> item)
        {
            Add(item.Key, item.Value);
        }

        public void Clear()
        {
            foreach (var img in Cache.Values)
            {
                Dispose(img);
            }
            Cache.Clear();
        }

        public bool Contains(KeyValuePair<Guid, ImageSource?> item)
        {
            return Cache.ContainsKey(item.Key);
        }

        public void CopyTo(KeyValuePair<Guid, ImageSource?>[] array, int arrayIndex)
        {
            Cache.ToArray().CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<Guid, ImageSource?> item)
        {
            return Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<Guid, ImageSource?>> GetEnumerator()
        {
            return Cache.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void Dispose(ImageSource imageSource)
        {
            try
            {
                if (imageSource is BitmapImage bmp && bmp.StreamSource is Stream strm)
                {
                    strm.Dispose();
                }
            }
            catch { }
        }
    }
}
