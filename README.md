# Aura
A Windows desktop virtual assistant. It provides quick access to various functions and it can be extended! (.Net knowledge required)

## Requirements
To compile this project, you need:
- Windows 10 or later with latest update
- .Net 7
- Microsoft Visual Studio 2022

To run the application, you need:
- Windows 10 or later with latest update
- .Net 7 Runtime
- At least 4 GB of RAM (vanilla Aura uses up to 300 MB of RAM, usually way less) or more
- AMD Ryzen or Intel Core processor released after 2018
- Dedicated GPU is recommended

## Support
If you have an issue, post it in https://gitlab.com/code-atelier/aura-v2.

## Roadmap
- Adding more extensions (TBA)
- Live2D support (Still having issue of adding Live2D to WPF with transparency)

## Authors
- Zecchan Silverlake: head of development, also the developer.
- Alice Silverlake: ~~mascot~~, ideas, debugging and technical consultant.

## Credits
- ARSoft for DNS Server
- RestSharp for REST Client

## License
Apache License v2.0

## Project status
Under development, beta release soon.

## Creating an Extension
(draft)
