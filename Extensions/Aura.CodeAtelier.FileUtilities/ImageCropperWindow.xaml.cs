﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MessageBox = System.Windows.MessageBox;
using Path = System.IO.Path;

namespace Aura.CodeAtelier
{
    /// <summary>
    /// Interaction logic for ImageCropperWindow.xaml
    /// </summary>
    public partial class ImageCropperWindow : Window
    {
        public const int ImageTileWidth = 100;
        public const int ImageTileHeight = 100;

        private List<IDisposable> DisposeList { get; } = new List<IDisposable>();

        public IReadOnlyList<ImageCropperSource> Sources { get; private set; } = new List<ImageCropperSource>();

        private IEnumerable<IGrouping<string, ImageCropperSource>>? CurrentGroups { get; set; }

        public ImageCropperWindow()
        {
            InitializeComponent();
            txGroupPrefixRegex.Text = previousPrefixRegex;
        }

        public async void LoadSources(IEnumerable<string> imageFiles)
        {
            DisposeUnusedMemory();
            var li = new List<ImageCropperSource>();
            int processed = 0;
            Dispatcher.Invoke(() =>
            {
                pbProgress.Minimum = 0;
                pbProgress.Maximum = imageFiles.Count();
            });

            await Util.RunHyperThreads(8, imageFiles, (file) =>
            {
                Dispatcher.Invoke(() =>
                {
                    processed++;
                    lbProgress.Content = $"Loading image {processed} of {imageFiles.Count()}...";
                    pbProgress.Value = processed;
                });
                var item = new ImageCropperSource();
                item.LoadImage(file);
                if (item != null)
                {
                    DisposeList.Add(item);
                    li.Add(item);
                }
            });
            Sources = li;
            Dispatcher.Invoke(() =>
            {
                RefreshThumbnails();
                lbProgress.Content = "Start the process to begin";
                pbProgress.Value = 0;
            });
        }

        private void RefreshThumbnails()
        {
            btnStart.IsEnabled = false;
            spContainer.Children.Clear();

            try
            {
                if (rbGroupNone.IsChecked == true)
                {
                    RefreshThumbnailGroupNone();
                }
                else if (rbGroupPrefix.IsChecked == true)
                {
                    RefreshThumbnailGroupPrefix();
                }
                else if (rbGroupResolution.IsChecked == true)
                {
                    RefreshThumbnailGroupResolution();
                }
            }
            finally
            {
                btnStart.IsEnabled = true;
            }
        }

        private void RefreshThumbnailGroupResolution()
        {
            CurrentGroups = Sources.GroupBy(x => x.Resolution);
            foreach (var group in CurrentGroups)
            {
                var expGroup = MakeGroup("Resolution: " + group.Key, group);
                spContainer.Children.Add(expGroup);
            }
        }

        private void RefreshThumbnailGroupPrefix()
        {
            // regex
            var pattern = "^\\s*" + txGroupPrefixRegex.Text;
            var regex = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            foreach (var source in Sources)
            {
                var match = regex.Match(source.FileNameWithoutExtension);
                if (match.Success && match.Groups.Count > 1)
                {
                    source.CustomGroup = match.Groups[1].Value + "@" + source.Resolution;
                }
                else
                {
                    source.CustomGroup = "<no match>@" + source.Resolution;
                }
            }
            CurrentGroups = Sources.GroupBy(x => x.CustomGroup);
            foreach (var group in CurrentGroups)
            {
                var expGroup = MakeGroup("Prefix: '" + group.Key + "'", group);
                spContainer.Children.Add(expGroup);
            }
        }

        private void RefreshThumbnailGroupNone()
        {
            CurrentGroups = null;
            var wrapPanel = new WrapPanel()
            {
                Orientation = System.Windows.Controls.Orientation.Horizontal
            };

            foreach (var source in Sources)
            {
                var img = MakeImage(source);
                wrapPanel.Children.Add(img);
            }

            spContainer.Children.Add(wrapPanel);
        }

        private Expander MakeGroup(string name, IEnumerable<ImageCropperSource> sources)
        {
            var expander = new Expander()
            {
                Header = name,
                IsExpanded = true,
            };

            var wrapPanel = new WrapPanel()
            {
                Orientation = System.Windows.Controls.Orientation.Horizontal
            };
            expander.Content = wrapPanel;

            foreach (var source in sources)
            {
                var img = MakeImage(source);
                wrapPanel.Children.Add(img);
            }

            return expander;
        }

        private Grid MakeImage(ImageCropperSource source)
        {
            var grid = new Grid()
            {
                Margin = new Thickness(4),
                Height = ImageTileWidth,
                Width = ImageTileHeight,
                ToolTip = source.FileName,
            };
            var img = new System.Windows.Controls.Image()
            {
                Stretch = Stretch.Uniform,
            };
            grid.Children.Add(img);
            source.ImageControl = grid;

            try
            {
                using (var ms = new MemoryStream())
                {
                    if (source.Thumbnail != null)
                    {
                        source.Thumbnail.Save(ms, ImageFormat.Png);
                        ms.Seek(0, SeekOrigin.Begin);
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.StreamSource = ms;
                        bmp.CacheOption = BitmapCacheOption.OnLoad;
                        bmp.EndInit();
                        img.Source = bmp;
                    }
                }
            }
            catch { }

            return grid;
        }

        private void rbGroup_Checked(object sender, RoutedEventArgs e)
        {
            if (txGroupPrefixRegex != null && rbGroupPrefix != null)
            {
                txGroupPrefixRegex.IsEnabled = rbGroupPrefix.IsChecked == true;
                RefreshThumbnails();
            }
        }

        private void rbOutput_Checked(object sender, RoutedEventArgs e)
        {
            if (btnOutputFolder != null && txOutputFolder != null && rbOutputFolder != null && txOutputNewFile != null && rbOutputNewFile != null)
            {
                btnOutputFolder.IsEnabled = txOutputFolder.IsEnabled = rbOutputFolder.IsChecked == true;
                txOutputNewFile.IsEnabled = rbOutputNewFile.IsChecked == true;
            }
        }

        string previousPrefixRegex = "([^_]+_)";
        private void txGroupPrefixRegex_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (txGroupPrefixRegex.Text == previousPrefixRegex)
            {
                return;
            }
            previousPrefixRegex = txGroupPrefixRegex.Text;
            RefreshThumbnails();
        }

        private async void btnStart_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                gbProcessor.IsEnabled = false;
            });
            await StartProcess();
        }

        private async Task StartProcess()
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    lbProgress.Content = "Starting...";
                    pbProgress.Value = 0;
                    pbProgress.Maximum = Math.Max(1, Sources.Count);
                });
                if (CurrentGroups != null)
                {
                    var cnt = CurrentGroups.Count();
                    var cur = 0;
                    foreach (var group in CurrentGroups.ToList())
                    {
                        cur++;
                        await ProcessBatch(group, cur, cnt);
                    }
                }
                else
                {
                    var cnt = Sources.Count();
                    var cur = 0;
                    foreach (var source in Sources.ToList())
                    {
                        cur++;
                        await ProcessBatch(new ImageCropperSource[] { source }, cur, cnt);
                    }
                }
                MessageBox.Show("Cropping finished!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    lbProgress.Content = "Start the process to begin";
                    pbProgress.Value = 0;
                    gbProcessor.IsEnabled = true;
                });
            }
        }

        private async Task ProcessBatch(IEnumerable<ImageCropperSource> sources, int taskNumber, int taskCount)
        {
            // calculate
            Dispatcher.Invoke(() =>
            {
                lbProgress.Content = $"[{taskNumber}/{taskCount}] Calculating crop bounds...";
                pbProgress.Value = 0;
                pbProgress.Maximum = sources.Count() * 2;
            });
            System.Drawing.Rectangle sourceRect = await CalculateRectangle(sources);

            // do crop
            await Util.RunHyperThreads(8, sources, (source) =>
            {
                try
                {
                    string? outputDir = null;
                    bool asNewFile = false;
                    string newFileName = "";
                    string imageFormat = "";
                    Dispatcher.Invoke(() =>
                    {
                        if (source.ImageControl is Grid image)
                        {
                            image.Background = new SolidColorBrush(Colors.LightSkyBlue);
                            image.BringIntoView();
                        }
                        pbProgress.Value += 1;
                        lbProgress.Content = $"[{taskNumber}/{taskCount}] Processing {pbProgress.Value - sources.Count()} of {pbProgress.Maximum - sources.Count()}";
                        outputDir = rbOutputFolder.IsChecked == true ? txOutputFolder.Text : Path.GetDirectoryName(source.Path);
                        asNewFile = rbOutputNewFile.IsChecked == true;
                        newFileName = txOutputNewFile.Text;
                        imageFormat = cbImageFormat.Text.ToLower();
                    });

                    // crop and save
                    outputDir = outputDir?.Replace("{dir}", Path.GetDirectoryName(source.Path));
                    if (outputDir == null) throw new Exception("Cannot find output directory");
                    if (!Directory.Exists(outputDir))
                    {
                        Directory.CreateDirectory(outputDir);
                    }
                    var outputFile = Path.GetFileNameWithoutExtension(source.Path);
                    if (asNewFile)
                    {
                        outputFile = newFileName.Replace("{name}", outputFile);
                    }
                    if (string.IsNullOrWhiteSpace(outputFile)) throw new Exception("Invalid output file name");
                    outputFile += "." + imageFormat;
                    var outputPath = Path.Combine(outputDir, outputFile);

                    var task = ProcessCrop(source, sourceRect, outputPath);
                    task.Wait();

                    Dispatcher.Invoke(() =>
                    {
                        if (source.ImageControl is Grid image)
                        {
                            image.Background = new SolidColorBrush(Colors.LightGreen);
                        }
                    });
                }
                catch
                {
                    Dispatcher.Invoke(() =>
                    {
                        if (source.ImageControl is Grid image)
                        {
                            image.Background = new SolidColorBrush(Colors.Crimson);
                        }
                    });
                    throw;
                }
            });
        }
        private async Task<System.Drawing.Rectangle> GetBoundsFast(ImageCropperSource source)
        {
            int top = 0, left = 0, right = source.ImageSize.Width, bottom = source.ImageSize.Height;

            using (var sbmp = (Bitmap)Bitmap.FromFile(source.Path))
            {
                using (var bmp = new QuickLockBits(sbmp, ImageLockMode.ReadOnly))
                {
                    var originPixel = bmp.PixelAt(0, 0);
                    var scanResult = await bmp.ScanRowsForColorAsync(16, 0, bmp.Height - 1, originPixel, true);
                    if (scanResult >= 0) top = scanResult;

                    scanResult = await bmp.ScanRowsForColorAsync(16,bmp.Height - 1, 0, originPixel, true);
                    if (scanResult >= 0) bottom = scanResult;

                    scanResult = await bmp.ScanColumnsForColorAsync(16, 0, bmp.Width - 1, originPixel, true);
                    if (scanResult >= 0) left = scanResult;

                    scanResult = await bmp.ScanColumnsForColorAsync(16, bmp.Width - 1, 0, originPixel, true);
                    if (scanResult >= 0) right = scanResult;
                }
            }

            return new System.Drawing.Rectangle(left, top, right - left + 1, bottom - top + 1);
        }

        private async Task<System.Drawing.Rectangle> CalculateRectangle(IEnumerable<ImageCropperSource> sources)
        {
            List<System.Drawing.Rectangle> rects = new List<System.Drawing.Rectangle>();
            await Util.RunHyperThreads(8, sources, (source) =>
            {
                Dispatcher.Invoke(() =>
                {
                    pbProgress.Value++;
                });
                try
                {
                    var task = GetBoundsFast(source);
                    task.Wait();
                    rects.Add(task.Result);
                }
                catch { }
            });
            if (rects.Count == 0)
            {
                throw new Exception("Failed to get bounds");
            }
            var x = rects.Min(x => x.X);
            var y = rects.Min(x => x.Y);
            var r = rects.Max(x => x.Right);
            var b = rects.Max(x => x.Bottom);
            var res = new System.Drawing.Rectangle(x, y, r - x, b - y);
            return res;
        }

        private async Task ProcessCrop(ImageCropperSource source, System.Drawing.Rectangle sourceRect, string outputPath)
        {
            await Task.Run(() =>
            {
                var bmp = new Bitmap(sourceRect.Width, sourceRect.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                using (var sbmp = Bitmap.FromFile(source.Path))
                {
                    using (var g = Graphics.FromImage(bmp))
                    {
                        var sourceR = new System.Drawing.Rectangle(sourceRect.X, sourceRect.Y, sourceRect.Width, sourceRect.Height);
                        var destR = new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height);
                        g.DrawImage(sbmp, destR, sourceR, GraphicsUnit.Pixel);
                    }
                }

                if (File.Exists(outputPath))
                {
                    File.Delete(outputPath);
                }
                ImageFormat imageFormat = ImageFormat.Png;
                Dispatcher.Invoke(() =>
                {
                    imageFormat =
                    cbImageFormat.SelectedIndex == 1 ? ImageFormat.Jpeg :
                    cbImageFormat.SelectedIndex == 2 ? ImageFormat.Bmp :
                    ImageFormat.Png;
                });
                bmp.Save(outputPath, imageFormat);
            });
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            DisposeUnusedMemory();
        }

        private void DisposeUnusedMemory()
        {
            foreach (var disposable in DisposeList)
            {
                try
                {
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                catch { }
            }
        }
    }

    public class ImageCropperSource : IDisposable
    {
        public Bitmap? Thumbnail { get; private set; }

        public System.Drawing.Size ImageSize { get; set; } = new System.Drawing.Size(0, 0);

        public string Resolution { get => $"{ImageSize.Width}x{ImageSize.Height}"; }

        public string Path { get; private set; } = string.Empty;

        public string FileName { get => System.IO.Path.GetFileName(Path); }

        public string FileNameWithoutExtension { get => System.IO.Path.GetFileNameWithoutExtension(Path); }

        public string CustomGroup { get; set; } = string.Empty;

        public Grid? ImageControl { get; set; }

        public ImageCropperSource()
        {
        }

        public void LoadImage(string path)
        {
            Path = path;
            try
            {
                byte[] thumbnailBuffer;
                using (var fi = File.OpenRead(path))
                {
                    fi.Seek(0, SeekOrigin.Begin);
                    using (var bmp = System.Drawing.Bitmap.FromStream(fi))
                    {
                        ImageSize = new System.Drawing.Size(bmp.Width, bmp.Height);

                        var scale = Math.Min(ImageCropperWindow.ImageTileWidth / (double)bmp.Width, ImageCropperWindow.ImageTileHeight / (double)bmp.Height);
                        var sw = (int)Math.Round(bmp.Width * scale);
                        var sh = (int)Math.Round(bmp.Height * scale);
                        var thumb = bmp.GetThumbnailImage(sw, sh, null, nint.Zero);
                        using (var ms = new MemoryStream())
                        {
                            thumb.Save(ms, ImageFormat.Png);
                            ms.Seek(0, SeekOrigin.Begin);
                            thumbnailBuffer = new byte[ms.Length];
                            ms.Read(thumbnailBuffer);
                        }
                    }
                }
                if (ServiceManager.MainDispatcher != null)
                {
                    ServiceManager.MainDispatcher.Invoke(() =>
                    {
                        var localBuffer = new byte[thumbnailBuffer.Length];
                        Array.Copy(thumbnailBuffer, localBuffer, thumbnailBuffer.Length);
                        using (var ms = new MemoryStream(localBuffer))
                        {
                            Thumbnail = (Bitmap)Bitmap.FromStream(ms);
                        }
                        ImageLoaded?.Invoke(this, EventArgs.Empty);
                    });
                }
            }
            catch { }
        }

        public void Dispose()
        {
            try
            {
                Thumbnail?.Dispose();
            }
            catch { }
        }

        public event EventHandler? ImageLoaded;
    }
}
