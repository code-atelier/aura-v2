﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Color = System.Windows.Media.Color;
using Cursors = System.Windows.Input.Cursors;
using Image = System.Windows.Controls.Image;
using MessageBox = System.Windows.MessageBox;

namespace Aura.CodeAtelier
{
    /// <summary>
    /// Interaction logic for ImageDiffWindow.xaml
    /// </summary>
    public partial class ImageDiffWindow : Window
    {
        const string ResultDir = "diffres";
        const int ThumbnailSize = 90;
        const int ThumbnailThread = 20;
        static readonly SolidColorBrush SelectedAsBase = new SolidColorBrush(Color.FromArgb(0xff, 30, 200, 60));
        static readonly SolidColorBrush ProcessingBackground = new SolidColorBrush(Color.FromArgb(0xff, 200, 60, 60));
        static readonly SolidColorBrush SelectedBackground = new SolidColorBrush(Color.FromArgb(0x66, 0x55, 0xaa, 0xff));
        static readonly SolidColorBrush SelectionBackground = new SolidColorBrush(Color.FromArgb(0x33, 0x55, 0xaa, 0xff));

        public static RoutedCommand GroupToRed = new RoutedCommand();
        public static RoutedCommand GroupToGreen = new RoutedCommand();
        public static RoutedCommand GroupToBlue = new RoutedCommand();
        public static RoutedCommand GroupToYellow = new RoutedCommand();
        public static RoutedCommand GroupToNone = new RoutedCommand();
        public static RoutedCommand SetAsBaseImage = new RoutedCommand();
        public static RoutedCommand RunDiffCommand = new RoutedCommand();

        public ImageDiffWindow()
        {
            InitializeComponent();

            PrepareShortcut();
        }

        public async void LoadFiles(IEnumerable<string> files)
        {
            files = files.OrderBy(x => Path.GetFileName(x)).ToList();
            foreach (var file in files)
            {
                var img = new Image();
                img.Stretch = Stretch.Uniform;
                img.Tag = file;
                img.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                img.VerticalAlignment = VerticalAlignment.Stretch;
                img.ToolTip = Path.GetFileName(file);

                var brd = new Border();
                brd.Width = ThumbnailSize;
                brd.Height = ThumbnailSize;
                img.Margin = new Thickness(4, 4, 4, 4);
                brd.Cursor = Cursors.Hand;
                brd.Child = img;
                brd.Tag = file;
                brd.MouseLeftButtonDown += Brd_MouseLeftButtonDown;

                LoadedFiles.Add(brd);
                wpUngrouped.Children.Add(brd);
            }
            svSelector.ScrollToTop();

            await Task.Run(() =>
            {
                for (var i = 0; i < files.Count(); i += ThumbnailThread)
                {
                    var tasks = new List<Task>();
                    for (var x = i; x < files.Count() && x - i < ThumbnailThread; x++)
                    {
                        tasks.Add(LoadThumbnail(files.ElementAt(x)));
                    }
                    if (tasks.Count > 0)
                        Task.WaitAll(tasks.ToArray());
                }
            });
        }
        public async Task LoadThumbnail(string path)
        {
            await Task.Run(() =>
            {
                try
                {
                    using (var img = System.Drawing.Image.FromFile(path))
                    {
                        var imW = img.Width;
                        var imH = img.Height;
                        var scl = Math.Min((double)ThumbnailSize / imW, (double)ThumbnailSize / imH);
                        imW = (int)Math.Round(imW * scl);
                        imH = (int)Math.Round(imH * scl);
                        using (var thumb = img.GetThumbnailImage(imW, imH, () => false, IntPtr.Zero))
                        {
                            using (var ms = new MemoryStream())
                            {
                                thumb.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                                ms.Seek(0, SeekOrigin.Begin);
                                var buf = new byte[ms.Length];
                                ms.Read(buf);

                                Dispatcher.Invoke(() =>
                                {
                                    var localBuffer = new byte[buf.Length];
                                    Array.Copy(buf, localBuffer, buf.Length);
                                    BitmapImage bmp = new BitmapImage();
                                    bmp.BeginInit();
                                    bmp.StreamSource = new MemoryStream(localBuffer);
                                    bmp.EndInit();

                                    foreach (var brd in LoadedFiles)
                                    {
                                        if (brd.Tag is string p && p == path && brd.Child is Image img)
                                        {
                                            img.Source = bmp;
                                            break;
                                        }
                                    }
                                });
                            }
                        }

                    }
                }
                catch { }
            });
        }

        private List<Border> LoadedFiles { get; } = new List<Border>();
        private List<Border> Selection { get; } = new List<Border>();
        private Border? Selected { get; set; }
        private Dictionary<string, Border?> SelectedBaseImages { get; } = new Dictionary<string, Border?>()
        {
            { "u" , null },
            { "r" , null },
            { "g" , null },
            { "b" , null },
            { "y" , null },
        };

        private void RefreshSelection()
        {
            foreach (var brd in LoadedFiles)
            {
                brd.Background = Selection.Contains(brd) ? (Selected == brd ? SelectedBackground : SelectionBackground) : null;
            }
            btnSetBase.IsEnabled = Selected != null;
        }

        private void Brd_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var sftDown = Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);
            if (sender is Border sbrd && sbrd.Tag is string p && File.Exists(p))
            {
                var container = (WrapPanel)sbrd.Parent;
                var lst = container.Children.OfType<Border>().ToList();
                if (Selected != null && !lst.Contains(Selected))
                {
                    Selected = null;
                }
                Selection.Clear();
                if (sftDown)
                {
                    if (Selected != null)
                    {
                        // many select
                        var ix = lst.IndexOf(Selected);
                        var cix = lst.IndexOf(sbrd);
                        for (var i = Math.Min(ix, cix); i <= Math.Max(ix, cix); i++)
                        {
                            if (!Selection.Contains(lst[i]))
                            {
                                Selection.Add(lst[i]);
                            }
                        }
                    }
                    else
                    {
                        Selected = sbrd;
                    }
                }
                else
                {
                    Selected = sbrd;
                }
                if (!Selection.Contains(sbrd))
                {
                    Selection.Add(sbrd);
                }
                RefreshSelection();
                LoadImage(p);
            }
        }

        private async void LoadImage(string path)
        {
            try
            {
                await Task.Run(() =>
                {
                    var buffer = File.ReadAllBytes(path);
                    Dispatcher.Invoke(() =>
                    {
                        var localBuffer = new byte[buffer.Length];
                        Array.Copy(buffer, localBuffer, buffer.Length);
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.StreamSource = new MemoryStream(localBuffer);
                        bmp.EndInit();

                        imgBase.Source = bmp;
                    });
                });
            }
            catch { }
        }

        private class DiffTask
        {
            public string Group { get; set; } = string.Empty;
            public string BaseImage { get; set; } = string.Empty;

            public Dictionary<Border, string> Files { get; set; } = new Dictionary<Border, string>();
        }

        private async void StartProcess(List<DiffTask> groups, bool outputByGroup)
        {
            string? targetDir = null;
            if (groups.Count == 0) { return; }
            await Task.Run(() =>
            {
                var tasks = new List<Task>();
                foreach (var grp in groups)
                {
                    targetDir = Path.Combine(Path.GetDirectoryName(grp.BaseImage) ?? "", ResultDir);
                    tasks.Add(RunDiff(outputByGroup ? grp.Group : string.Empty, grp.BaseImage, grp.Files));
                }
                Task.WaitAll(tasks.ToArray());
            });

            Dispatcher.Invoke(() =>
            {
                spControl.IsEnabled = true;
                cxOutputByGroup.IsEnabled = true;
                MessageBox.Show("Diff completed!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
                try
                {
                    if (Directory.Exists(targetDir))
                    {
                        Util.OpenFile(targetDir);
                    }
                }
                catch { }
            });
        }

        private async Task RunDiff(string group, string baseImage, Dictionary<Border, string> others)
        {
            try
            {
                var buffer = File.ReadAllBytes(baseImage);
                var targetDir = Path.Combine(Path.GetDirectoryName(baseImage) ?? "", ResultDir, group);
                if (!Directory.Exists(targetDir))
                    Directory.CreateDirectory(targetDir);
                var targetBaseFile = Path.Combine(targetDir, Path.GetFileNameWithoutExtension(baseImage) + "_base.png");
                using (System.Drawing.Image bmpBase = System.Drawing.Image.FromStream(new MemoryStream(buffer)))
                {
                    bmpBase.Save(targetBaseFile, System.Drawing.Imaging.ImageFormat.Png);
                    foreach (var other in others)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            var container = (WrapPanel)other.Key.Parent;
                            foreach (var brd in container.Children.OfType<Border>())
                            {
                                brd.Background = null;
                            }
                            other.Key.Background = ProcessingBackground;
                            other.Key.BringIntoView();
                        });

                        await Task.Run(async () =>
                        {
                            await LoadResultImage((Bitmap)bmpBase, other.Value, group);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task LoadResultImage(System.Drawing.Bitmap baseImage, string path, string group)
        {
            var targetDir = Path.Combine(System.IO.Path.GetDirectoryName(path) ?? "", ResultDir, group);
            await Task.Run(() =>
            {
                if (!Directory.Exists(targetDir))
                    Directory.CreateDirectory(targetDir);
                var targetFile = System.IO.Path.Combine(targetDir, Path.GetFileName(path));
                var buffer = File.ReadAllBytes(path);
                using (var ms = new MemoryStream(buffer))
                {
                    using (System.Drawing.Bitmap bmpTarget = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms))
                    {

                        if (bmpTarget.Width != baseImage.Width || bmpTarget.Height != baseImage.Height || bmpTarget.PixelFormat != baseImage.PixelFormat)
                            return;

                        using (var nBmp = new System.Drawing.Bitmap(bmpTarget.Width, bmpTarget.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                        {
                            using (var qBase = new QuickLockBits(baseImage, System.Drawing.Imaging.ImageLockMode.ReadOnly))
                            {
                                using (var qTarget = new QuickLockBits(bmpTarget, System.Drawing.Imaging.ImageLockMode.ReadOnly))
                                {
                                    for (var x = 0; x < nBmp.Width; x++)
                                    {
                                        for (var y = 0; y < nBmp.Height; y++)
                                        {
                                            var pxBase = qBase.PixelAt(x, y);
                                            var pxTarget = qTarget.PixelAt(x, y);
                                            if (pxBase != pxTarget)
                                            {
                                                nBmp.SetPixel(x, y, pxTarget);
                                            }
                                            else
                                            {
                                                nBmp.SetPixel(x, y, System.Drawing.Color.Transparent);
                                            }
                                        }
                                    }
                                }
                            }
                            using (var mst = new MemoryStream())
                            {
                                using (var bst = new MemoryStream())
                                {
                                    nBmp.Save(mst, System.Drawing.Imaging.ImageFormat.Png);
                                    mst.Seek(0, SeekOrigin.Begin);
                                    var msBuffer = mst.ToArray();
                                    File.WriteAllBytes(targetFile, msBuffer);

                                    baseImage.Save(bst, System.Drawing.Imaging.ImageFormat.Png);
                                    bst.Seek(0, SeekOrigin.Begin);
                                    var bsBuffer = bst.ToArray();

                                    Dispatcher.Invoke(() =>
                                    {
                                        var localBuffer = new byte[msBuffer.Length];
                                        Array.Copy(msBuffer, localBuffer, msBuffer.Length);
                                        var bmp = new BitmapImage();
                                        bmp.BeginInit();
                                        bmp.StreamSource = new MemoryStream(localBuffer);
                                        bmp.EndInit();

                                        imgResult.Source = bmp;

                                        localBuffer = new byte[bsBuffer.Length];
                                        Array.Copy(bsBuffer, localBuffer, bsBuffer.Length);
                                        bmp = new BitmapImage();
                                        bmp.BeginInit();
                                        bmp.StreamSource = new MemoryStream(localBuffer);
                                        bmp.EndInit();

                                        imgBase.Source = bmp;
                                    });
                                }
                            }
                        }
                    }
                }
            });
        }

        private void GroupSelectionTo(WrapPanel wpContainer)
        {
            if (Selection.Count > 0)
            {
                var lst = wpContainer.Children.OfType<Border>().ToList();
                var notGroupedToRed = Selection.Where(x => !lst.Contains(x)).ToList();
                foreach (var b in notGroupedToRed)
                {
                    lst.Add(b);
                    b.BorderBrush = null;
                    b.BorderThickness = new Thickness(0);
                    string? key = null;
                    if (SelectedBaseImages.Values.Contains(b))
                    {
                        foreach (var sbi in SelectedBaseImages)
                        {
                            if (sbi.Value == b)
                            {
                                key = sbi.Key;
                                break;
                            }
                        }
                    }
                    if (key != null)
                    {
                        SelectedBaseImages[key] = null;
                    }
                }
                wpContainer.Children.Clear();
                lst = lst.OrderBy(x => Path.GetFileName(x.Tag?.ToString())).ToList();
                foreach (var b in lst)
                {
                    if (b.Parent is WrapPanel wp) wp.Children.Remove(b);
                    wpContainer.Children.Add(b);
                    b.Background = null;
                }
                Selection.Clear();
                Selected = null;
                btnSetBase.IsEnabled = false;
            }
        }

        private void PrepareShortcut()
        {
            GroupToRed.InputGestures.Add(new KeyGesture(Key.D1, ModifierKeys.Control));
            GroupToGreen.InputGestures.Add(new KeyGesture(Key.D2, ModifierKeys.Control));
            GroupToBlue.InputGestures.Add(new KeyGesture(Key.D3, ModifierKeys.Control));
            GroupToYellow.InputGestures.Add(new KeyGesture(Key.D4, ModifierKeys.Control));
            GroupToNone.InputGestures.Add(new KeyGesture(Key.D5, ModifierKeys.Control));
            GroupToNone.InputGestures.Add(new KeyGesture(Key.A, ModifierKeys.Control));
            SetAsBaseImage.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            RunDiffCommand.InputGestures.Add(new KeyGesture(Key.R, ModifierKeys.Control));
        }

        private void CommandBinding_CanGroupToExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Selection.Count > 0;
        }

        private void CommandBinding_GroupToRedExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            GroupSelectionTo(wpRed);
        }

        private void CommandBinding_GroupToGreenExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            GroupSelectionTo(wpGreen);
        }

        private void CommandBinding_GroupToBlueExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            GroupSelectionTo(wpBlue);
        }

        private void CommandBinding_GroupToYellowExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            GroupSelectionTo(wpYellow);
        }

        private void CommandBinding_GroupToNoneExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            GroupSelectionTo(wpUngrouped);
        }

        private void CommandBinding_CanSetAsBaseImageExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Selected != null;
        }

        private void CommandBinding_SetAsBaseImageExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (Selected != null)
            {
                var container = (WrapPanel)Selected.Parent;
                foreach (var b in container.Children.OfType<Border>())
                {
                    b.BorderThickness = new Thickness(0);
                }
                Selected.BorderBrush = SelectedAsBase;
                Selected.BorderThickness = new Thickness(2);
                if (container.Tag is string key)
                    SelectedBaseImages[key] = Selected;
            }
        }

        private void CommandBinding_CanSaveExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            List<WrapPanel> groups = new List<WrapPanel>();
            if (wpUngrouped.Children.Count > 0) groups.Add(wpUngrouped);
            if (wpRed.Children.Count > 0) groups.Add(wpRed);
            if (wpGreen.Children.Count > 0) groups.Add(wpGreen);
            if (wpBlue.Children.Count > 0) groups.Add(wpBlue);
            if (wpYellow.Children.Count > 0) groups.Add(wpYellow);
            if (groups.Count == 0)
            {
                e.CanExecute = false;
                return;
            }

            e.CanExecute = true;
        }

        private void CommandBinding_SaveExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            List<WrapPanel> groups = new List<WrapPanel>();
            if (wpUngrouped.Children.Count > 0) groups.Add(wpUngrouped);
            if (wpRed.Children.Count > 0) groups.Add(wpRed);
            if (wpGreen.Children.Count > 0) groups.Add(wpGreen);
            if (wpBlue.Children.Count > 0) groups.Add(wpBlue);
            if (wpYellow.Children.Count > 0) groups.Add(wpYellow);
            if (groups.Count == 0)
            {
                MessageBox.Show("No image to process", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            foreach (var group in groups)
            {
                if (group.Tag is string key)
                {
                    if (SelectedBaseImages[key] == null)
                    {
                        var groupName = key == "r" ? "red" : key == "g" ? "green" : key == "b" ? "blue" : key == "y" ? "orange" : "ungrouped";
                        MessageBox.Show($"Please select base image for {groupName}!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            foreach (var b in LoadedFiles)
            {
                b.Background = null;
            }
            svSelector.IsEnabled = false;
            btnStart.IsEnabled = false;
            spControl.IsEnabled = false;
            cxOutputByGroup.IsEnabled = false;
            btnStart.Content = "Running...";

            var diffTasks = new List<DiffTask>();
            foreach (var group in groups)
            {
                if (group.Tag is string key)
                {
                    var sbi = SelectedBaseImages[key];
                    if (sbi != null && sbi.Tag is string imgBase && File.Exists(imgBase))
                    {
                        var files = new Dictionary<Border, string>();
                        foreach (var brd in group.Children.OfType<Border>())
                        {
                            if (brd != sbi && brd.Tag is string p && File.Exists(p))
                            {
                                files.Add(brd, p);
                            }
                        }
                        diffTasks.Add(new DiffTask()
                        {
                            BaseImage = imgBase,
                            Files = files,
                            Group = key
                        });
                    }
                }
            }
            var bOutputByGroup = cxOutputByGroup.IsChecked == true;
            StartProcess(diffTasks, bOutputByGroup);
        }
    }
}

