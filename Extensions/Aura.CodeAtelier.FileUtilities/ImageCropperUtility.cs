﻿using Aura.Models;
using System.IO;
using Message = Aura.Services.Message;

namespace Aura.CodeAtelier
{
    public partial class ImageUtilityService
    {
        private static string[] CropperImageExtensions { get; } = new string[]
        {
            ".png",
            ".jpg",
            ".jpeg",
            ".bmp",
            ".tga"
        };

        private List<FileDropHandler> GetCropperActionsFor(FileDropEventBody body)
        {
            var res = new List<FileDropHandler>();
            if (body.Files.All(x => CropperImageExtensions.Contains(Path.GetExtension(x).ToLower())))
            {
                var replyBody = new FileDropHandler()
                {
                    Action = "autocrop",
                    Id = Id,
                    Name = "Auto Cropper",
                    Icon = Util.CreateResourceUri("res/img/file-picture-crop.png"),
                };
                res.Add(replyBody);
            }
            return res;
        }

        private async Task<Message?> HandleCropperAction(Message message, FileDropExecuteEventBody body)
        {
            if (body.Handler.Action == "autocrop" && ServiceManager.MainDispatcher != null)
            {
                await Task.Run(() =>
                {
                    ServiceManager.MainDispatcher.Invoke(() =>
                    {
                        var w = new ImageCropperWindow();
                        w.LoadSources(body.Files);
                        w.Show();
                        w.Activate();
                    });
                });
                return message.CreateReply(Id);
            }
            return null;
        }
    }
}
