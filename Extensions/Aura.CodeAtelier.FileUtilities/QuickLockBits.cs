﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public class QuickLockBits : IDisposable
    {
        public Bitmap Bitmap { get; }

        public int Width { get; }

        public int Height { get; }

        public PixelFormat PixelFormat { get; }

        public ImageLockMode LockMode { get; }

        public BitmapData BitmapData { get; }

        public byte[] RawData { get; }

        public QuickLockBits(Bitmap bitmap, ImageLockMode imageLockMode = ImageLockMode.ReadWrite)
        {
            LockMode = imageLockMode;
            Bitmap = bitmap;
            Width = bitmap.Width;
            Height = bitmap.Height;
            PixelFormat = bitmap.PixelFormat;
            var rectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData = bitmap.LockBits(rectangle, imageLockMode, bitmap.PixelFormat);
            var dataLength = Math.Abs(BitmapData.Stride) * rectangle.Height;
            if ((LockMode & ImageLockMode.ReadOnly) > 0)
            {
                RawData = new byte[dataLength];
                Marshal.Copy(BitmapData.Scan0, RawData, 0, dataLength);
            }
            else
            {
                RawData = new byte[dataLength];
            }
        }

        public Color PixelAt(int x, int y)
        {
            return ColorHelper.BytesToColor(ColorBytesAt(x, y), PixelFormat);
        }

        public byte[] ColorBytesAt(int x, int y)
        {
            var bytesPerRow = BitmapData.Stride;
            var bytePerColor = ColorHelper.GetBytesPerPixel(PixelFormat);
            var startAddress = y * bytesPerRow + x * bytePerColor;
            var source = RawData[startAddress..(startAddress + bytePerColor)].ToArray();
            Array.Reverse(source);
            return source;
        }

        private List<int> MakeIndexes(int start, int end)
        {
            var indexes = new List<int>();
            if (start <= end)
            {
                for (var i = start; i <= end; i++) indexes.Add(i);
            }
            else
            {
                for (var i = start; i >= end; i--) indexes.Add(i);
            }
            return indexes;
        }

        /// <summary>
        /// Scan image rows for a color.
        /// </summary>
        /// <param name="except">When true, find any color except the specified color.</param>
        /// <returns>First row (with respect to row direction) where the color found</returns>
        public int ScanRowsForColor(int startRow, int endRow, Color color, bool except = false)
        {
            foreach(var row in MakeIndexes(startRow, endRow))
            {
                if (ScanRowForColor(row, color, except)) return row;
            }
            return -1;
        }

        public async Task<int> ScanRowsForColorAsync(int threads, int startRow, int endRow, Color color, bool except = false)
        {
            return await Task.Run(() =>
            {
                var results = new Dictionary<int, bool?>();
                var tasks = new List<Task>();
                var indexes = MakeIndexes(startRow, endRow);
                foreach (var row in indexes)
                {
                    results[row] = null;
                    var task = Task.Run(() => {
                        var val = ScanRowForColor(row, color, except);
                        results[row] = val;
                    });
                    tasks.Add(task);
                    while (tasks.Count >= threads)
                    {
                        Task.WaitAny(tasks.ToArray());
                        tasks.RemoveAll(t => t.IsCompleted);
                        if (indexes.Any(x => results.ContainsKey(x) && results[x] == true))
                        {
                            return indexes.First(x => results[x] == true);
                        }
                        Task.Delay(100);
                    }
                }
                Task.WaitAll(tasks.ToArray());
                return indexes.Any(x => results.ContainsKey(x) && results[x] == true) ? indexes.First(x => results[x] == true) : -1;
            });
        }

        /// <summary>
        /// Scan image columns for a color.
        /// </summary>
        /// <param name="except">When true, find any color except the specified color.</param>
        /// <returns>First column (with respect to row direction) where the color found</returns>
        public int ScanColumnsForColor(int startColumn, int endColumn, Color color, bool except = false)
        {
            foreach (var row in MakeIndexes(startColumn, endColumn))
            {
                if (ScanColumnForColor(row, color, except)) return row;
            }
            return -1;
        }
        public async Task<int> ScanColumnsForColorAsync(int threads, int startColumn, int endColumn, Color color, bool except = false)
        {
            return await Task.Run(() =>
            {
                var results = new Dictionary<int, bool?>();
                var tasks = new List<Task>();
                var indexes = MakeIndexes(startColumn, endColumn);
                foreach (var row in indexes)
                {
                    results[row] = null;
                    var task = Task.Run(() => {
                        var val = ScanColumnForColor(row, color, except);
                        results[row] = val;
                    });
                    tasks.Add(task);
                    while (tasks.Count >= threads)
                    {
                        Task.WaitAny(tasks.ToArray());
                        tasks.RemoveAll(t => t.IsCompleted);
                        if (indexes.Any(x => results.ContainsKey(x) && results[x] == true))
                        {
                            return indexes.First(x => results[x] == true);
                        }
                        Task.Delay(100);
                    }
                }
                Task.WaitAll(tasks.ToArray());
                return indexes.Any(x => results.ContainsKey(x) && results[x] == true) ? indexes.First(x => results[x] == true) : -1;
            });
        }

        public bool ScanRowForColor(int row, Color color, bool except = false)
        {
            var colorBytes = ColorHelper.ColorToBytes(color, PixelFormat);

            for (var x = 0; x < Width; x++)
            {
                var pixelBytes = ColorBytesAt(x, row);
                var sameColor = ColorHelper.BytesEqual(colorBytes, pixelBytes);
                if ((!sameColor && except) || (sameColor && !except)) return true;
            }
            return false;
        }
        public bool ScanColumnForColor(int column, Color color, bool except = false)
        {
            var colorBytes = ColorHelper.ColorToBytes(color, PixelFormat);

            for (var y = 0; y < Height; y++)
            {
                var pixelBytes = ColorBytesAt(column, y);
                var sameColor = ColorHelper.BytesEqual(colorBytes, pixelBytes);
                if ((!sameColor && except) || (sameColor && !except)) return true;
            }
            return false;
        }

        public void Dispose()
        {
            if ((LockMode & ImageLockMode.WriteOnly) > 0)
                Marshal.Copy(RawData, 0, BitmapData.Scan0, RawData.Length);
            Bitmap.UnlockBits(BitmapData);
        }
    }

    public static class ColorHelper
    {
        private static Dictionary<PixelFormat, int> BytesPerPixel { get; } = new Dictionary<PixelFormat, int>()
        {
            { PixelFormat.Format32bppArgb, 4 },
            { PixelFormat.Format24bppRgb, 3 },
            { PixelFormat.DontCare, 4 }
        };

        public static int GetBytesPerPixel(PixelFormat pixelFormat)
        {
            return BytesPerPixel[pixelFormat];
        }

        public static Color BytesToColor(byte[] bytes, PixelFormat pixelFormat)
        {
            const byte opaqueAlpha = 255;
            var source = new byte[bytes.Length];
            Array.Copy(bytes, source, bytes.Length);

            byte[] res;
            switch (pixelFormat)
            {
                case PixelFormat.DontCare:
                case PixelFormat.Format32bppArgb:
                    res = new[] { source[0], source[1], source[2], source[3] };
                    break;
                case PixelFormat.Format24bppRgb:
                    res = new[] { opaqueAlpha, source[0], source[1], source[2] };
                    break;
                default:
                    throw new NotImplementedException();
            }
            return Color.FromArgb(res[0], res[1], res[2], res[3]);
        }
        public static byte[] ColorToBytes(Color color, PixelFormat pixelFormat)
        {
            switch (pixelFormat)
            {
                case PixelFormat.DontCare:
                case PixelFormat.Format32bppArgb:
                    return new byte[4] { color.A, color.R, color.G, color.B };
                case PixelFormat.Format24bppRgb:
                    return new byte[3] { color.R, color.G, color.B };
                default:
                    throw new NotImplementedException();
            }
        }
        public static bool BytesEqual(byte[] a, byte[] b)
        {
            if (a == b) return true;
            if (a == null || b == null) return false;
            if (a.Length != b.Length) return false;
            for (var i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i]) return false;
            }
            return true;
        }
    }
}
