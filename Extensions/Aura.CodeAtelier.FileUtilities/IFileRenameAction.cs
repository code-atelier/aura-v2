﻿using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public interface IFileRenameAction
    {
        string Rename(string name, RenameParameterStorage renameParameterStorage);

        string Id { get; }

        string Name { get; }

        string Description { get; }

        object? GetParameter();
    }

    public interface IFileRenameAction<TFileRenameParameter> : IFileRenameAction where TFileRenameParameter : class, new()
    {
        TFileRenameParameter Parameter { get; }
    }

    public class FileRenameActionDTO
    {
        public string Id { get; set; } = string.Empty;

        public object? Parameter { get; set; }

        public static FileRenameActionDTO FromInterface(IFileRenameAction fileRename)
        {
            return new FileRenameActionDTO
            {
                Id = fileRename.Id,
                Parameter = fileRename.GetParameter(),
            };
        }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class FileRenameParameterAttribute: Attribute
    {
        public string Name { get; }

        public FileRenameParameterAttribute(string name)
        {
            Name = name;
        }
    }

    public class FileRenameParameterValidatorAttribute : Attribute
    {
        public Type ValidatorType { get; }

        public FileRenameParameterValidatorAttribute(Type validatorType)
        {
            ValidatorType = validatorType;
        }

        public FileRenameParameterValidationResult Validate(object? value)
        {
            try
            {
                var instance = Activator.CreateInstance(ValidatorType);
                if (instance is IFileRenameParameterValidator validator)
                {
                    return validator.Validate(value);
                }
            }
            catch { }
            return FileRenameParameterValidationResult.Fail($"Validator '{ValidatorType.Name}' failed to validate");
        }
    }

    public interface IFileRenameParameterValidator
    {
        FileRenameParameterValidationResult Validate(object? value);
    }

    public class FileRenameParameterValidationResult
    {
        public bool IsSuccess { get; set; }

        public string? Error { get; set; }

        public static FileRenameParameterValidationResult Success()
        {
            return new FileRenameParameterValidationResult()
            {
                IsSuccess = true
            };
        }
        public static FileRenameParameterValidationResult Fail(string message)
        {
            return new FileRenameParameterValidationResult()
            {
                IsSuccess = false,
                Error = message,
            };
        }
    }
}
