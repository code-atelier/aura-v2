﻿using Aura.CodeAtelier.Models;
using Aura.Models;
using Aura.Services;
using Message = Aura.Services.Message;

namespace Aura.CodeAtelier
{
    public class FileRenameService : PassiveServiceBase, IMessageReceiver
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo
        {
            Author = "CodeAtelier",
            Description = "Coatl File Rename Service",
            Id = "coatl.file.rename",
            Name = "File Rename Service",
            Version = new Version(1, 0, 7),
            Icon = Util.CreateResourceUri("res/img/file-renamer.png"),
        };

        public string Id { get => Info.Id; }

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.Start | ServiceUserAction.Stop;

        private IServiceMessageBus MessageBus { get; set; }

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public bool CanReceiveMessage { get => State == ServiceState.Running; }

        public Dictionary<string, IFileRenameAction> RenameActions { get; } = new Dictionary<string, IFileRenameAction>();

        public FileRenameService(IServiceMessageBus messageBus)
        {
            MessageBus = messageBus;
        }

        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                MessageBus.Subscribe(this, InternalEvents.FileDrop);
                MessageBus.Subscribe(this, InternalEvents.ExecuteFileDrop);

                RegisterAction(new NewFileNameAction());
                RegisterAction(new ReplaceFileNameAction());
                RegisterAction(new RemoveFileNameAction());
                RegisterAction(new TrimFileNameAction());
                RegisterAction(new PrefixAction());
                RegisterAction(new SuffixAction());
                RegisterAction(new CaptureVariableAction());
            });
        }

        public void RegisterAction(IFileRenameAction action)
        {
            if (!RenameActions.ContainsKey(action.Id))
            {
                RenameActions[action.Id] = action;
            }
        }

        public async Task<Message?> ReceiveMessageAsync(Message message, CancellationToken cancellationToken)      
        {
            return await Task.Run(() =>
            {
                if (message.EventId == InternalEvents.FileDrop && message.GetBody<FileDropEventBody>() is FileDropEventBody body)
                {
                    if (body.Files.Count > 0)
                    {
                        var b = new FileDropHandler()
                        {
                            Id = Info.Id,
                            Name = "File Renamer",
                            Icon = Util.CreateResourceUri("res/img/file-renamer.png"),
                            Action = "batch-rename"
                        };

                        return message.CreateReply(Info.Id, b);
                    }
                }

                if (message.EventId == InternalEvents.ExecuteFileDrop && message.GetBody<FileDropEventBody>() is FileDropEventBody body2)
                {
                    if (body2.Files.Count > 0)
                    {
                        ServiceManager.MainDispatcher?.Invoke(() =>
                        {
                            var w = new FileRenamer();
                            w.LoadActions(RenameActions);
                            w.LoadFiles(body2.Files);
                            w.Show();
                        });
                    }
                }
                return null;
            });
        }
    }
}