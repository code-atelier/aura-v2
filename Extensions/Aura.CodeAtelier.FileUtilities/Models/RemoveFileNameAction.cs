﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class RemoveFileNameAction : IFileRenameAction<SingleStringOrRegexParameter>
    {
        public SingleStringOrRegexParameter Parameter { get; } = new SingleStringOrRegexParameter();

        public string Name { get; } = "Remove";

        public string Id { get; } = "remove";

        public string Description { get; } = "Remove a part of current filename. Can use regex.";

        public object? GetParameter()
        {
            return Parameter;
        }
        public string Rename(string name, RenameParameterStorage renameParameterStorage)
        {
            var res = name;
            if (Parameter.UseRegex)
            {
                try
                {
                    var regex = new Regex(Parameter.Pattern);
                    res = regex.Replace(res, "");
                }
                catch { }
            }
            else
            {
                res = res.Replace(Parameter.Pattern, string.Empty);
            }
            return renameParameterStorage.ParseVariables(res);
        }
        public override string ToString()
        {
            return Name + " {" + Parameter.Pattern + "}";
        }
    }
    public class SingleStringOrRegexParameter
    {
        [FileRenameParameter("Pattern")]
        [FileRenameParameterValidator(typeof(NotEmptyValidator))]
        public string Pattern { get; set; } = "";

        [FileRenameParameter("Use Regex")]
        public bool UseRegex { get; set; } = false;
    }
}
