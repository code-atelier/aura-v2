﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class FileNameValidator : IFileRenameParameterValidator
    {
        public FileRenameParameterValidationResult Validate(object? value)
        {
            string invalidChara = "/\\?*:|\"<>";
            if (value is string fileName)
            {
                if (!fileName.Any(x => invalidChara.Contains(x)))
                {
                    return FileRenameParameterValidationResult.Success();
                }
            }
            return FileRenameParameterValidationResult.Fail("{field} contains invalid characters");
        }
    }

    public class NotEmptyValidator : IFileRenameParameterValidator
    {
        public FileRenameParameterValidationResult Validate(object? value)
        {
            if (value is string fileName)
            {
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    return FileRenameParameterValidationResult.Success();
                }
            }
            return FileRenameParameterValidationResult.Fail("{field} cannot be empty");
        }
    }
}
