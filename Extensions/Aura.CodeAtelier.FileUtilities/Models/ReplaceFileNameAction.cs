﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class ReplaceFileNameAction : IFileRenameAction<SingleStringOrRegexToStringParameter>
    {
        public SingleStringOrRegexToStringParameter Parameter { get; } = new SingleStringOrRegexToStringParameter();

        public string Name { get; } = "Replace";

        public string Id { get; } = "replace";

        public string Description { get; } = "Replace a part of current filename with another string. Can use regex.";

        public object? GetParameter()
        {
            return Parameter;
        }
        public string Rename(string name, RenameParameterStorage renameParameterStorage)
        {
            var res = name;
            if (Parameter.UseRegex)
            {
                try
                {
                    var regex = new Regex(Parameter.Pattern);
                    res = regex.Replace(res, Parameter.ReplaceWith);
                }
                catch { }
            }
            else
            {
                res = res.Replace(Parameter.Pattern, Parameter.ReplaceWith);
            }
            return renameParameterStorage.ParseVariables(res);
        }
        public override string ToString()
        {
            return Name + " {" + Parameter.ReplaceWith + "}";
        }
    }

    public class SingleStringOrRegexToStringParameter
    {
        [FileRenameParameter("Pattern")]
        [FileRenameParameterValidator(typeof(NotEmptyValidator))]
        public string Pattern { get; set; } = "";

        [FileRenameParameter("Use Regex")]
        public bool UseRegex { get; set; } = false;

        [FileRenameParameter("Replace With")]
        [FileRenameParameterValidator(typeof(NotEmptyValidator))]
        public string ReplaceWith { get; set; } = "";
    }
}
