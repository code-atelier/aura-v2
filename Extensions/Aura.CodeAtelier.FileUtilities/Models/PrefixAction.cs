﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Aura.CodeAtelier.Models
{
    public class PrefixAction : IFileRenameAction<SingleTextParameter>
    {
        public SingleTextParameter Parameter { get; } = new SingleTextParameter();

        public string Name { get; } = "Prefix";

        public string Id { get; } = "prefix";

        public string Description { get; } = "Prefixes the file name with a text. (Does not affect file extension)";

        public object? GetParameter()
        {
            return Parameter;
        }
        public string Rename(string name, RenameParameterStorage renameParameterStorage)
        {
            return renameParameterStorage.ParseVariables(Parameter.Text + name);
        }
        public override string ToString()
        {
            return Name + " {" + Parameter.Text + "}";
        }
    }
    public class SuffixAction : IFileRenameAction<SingleTextParameter>
    {
        public SingleTextParameter Parameter { get; } = new SingleTextParameter();

        public string Name { get; } = "Suffix";

        public string Id { get; } = "suffix";

        public string Description { get; } = "Suffixes the file name with a text. (Does not affect file extension)";

        public object? GetParameter()
        {
            return Parameter;
        }
        public string Rename(string name, RenameParameterStorage renameParameterStorage)
        {
            var fname = Path.GetFileNameWithoutExtension(name) ?? "";
            var ext = Path.GetExtension(name) ?? "";
            return renameParameterStorage.ParseVariables(fname + Parameter.Text + ext);
        }
        public override string ToString()
        {
            return Name + " {" + Parameter.Text + "}";
        }
    }

    public class SingleTextParameter
    {
        [FileRenameParameter("Text")]
        [FileRenameParameterValidator(typeof(NotEmptyValidator))]
        public string Text { get; set; } = "";
    }
}
