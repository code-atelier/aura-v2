﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class CaptureVariableAction : IFileRenameAction<SingleRegexAndNameParameter>
    {
        public SingleRegexAndNameParameter Parameter { get; } = new SingleRegexAndNameParameter();

        public string Name { get; } = "Capture Variable";

        public string Description { get; } = "Capture a part of the filename and store it to variable storage.";

        public string Id { get; } = "capturevar";

        public object? GetParameter()
        {
            return Parameter;
        }

        public string Rename(string name, RenameParameterStorage renameParameterStorage)
        {
            var res = name;
            var regex = new Regex(Parameter.Pattern);
            var match = regex.Match(res);
            if (match.Success && match.Groups.Count > 1)
            {
                renameParameterStorage.Variables[Parameter.VariableName] = match.Groups[1].Value;
            }
            return name;
        }

        public override string ToString()
        {
            return Name + " {" + Parameter.VariableName + "}";
        }
    }

    public class SingleRegexAndNameParameter
    {
        [FileRenameParameter("Pattern")]
        [FileRenameParameterValidator(typeof(NotEmptyValidator))]
        public string Pattern { get; set; } = "";

        [FileRenameParameter("Variable Name")]
        [FileRenameParameterValidator(typeof(NotEmptyValidator))]
        public string VariableName { get; set; } = "";
    }
}
