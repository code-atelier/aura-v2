﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class RenameParameterStorage
    {
        public Dictionary<string, string> Variables { get; } = new Dictionary<string, string>();

        public string ParseVariables(string text)
        {
            foreach(var variable in Variables)
            {
                var pattern = "\\{\\s*" + variable.Key + "(?>\\s*:([^\\}]+))?\\s*\\}";
                try
                {
                    var regex = new Regex(pattern);
                    var matches = regex.Matches(text);
                    foreach(Match match in matches) 
                    {
                        var matchedText = match.Groups[0].Value;
                        var value = variable.Value;
                        if (match.Groups.Count > 1)
                        {
                            var format = match.Groups[1].Value?.Trim();
                            if (!string.IsNullOrWhiteSpace(format))
                            {
                                if (format.All(x => x == '#'))
                                {
                                    value = value.PadLeft(format.Length, '0');
                                }
                                if (format == "A")
                                {
                                    value = value.ToUpper();
                                }
                                if (format == "a")
                                {
                                    value = value.ToLower();
                                }
                            }
                        }
                        text = text.Replace(matchedText, value);
                    }
                }
                catch { }
            }
            return text;
        }
    }
}
