﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class TrimFileNameAction : IFileRenameAction<SingleStringParameter>
    {
        public SingleStringParameter Parameter { get; } = new SingleStringParameter();

        public string Name { get; } = "Trim";

        public string Description { get; } = "Removes all leading and trailing characters from the file name. (Does not affect file extension)";

        public string Id { get; } = "trim";

        public object? GetParameter()
        {
            return Parameter;
        }

        public string Rename(string name, RenameParameterStorage renameParameterStorage)
        {
            var fname = Path.GetFileNameWithoutExtension(name) ?? "";
            var ext = Path.GetExtension(name);
            var charset = Parameter.Characters;
            charset = charset
                .Replace("\\t", "\t")
                .Replace("\\n", "\n")
                .Replace("\\r", "\r");
            fname = fname.Trim(charset.ToArray());
            return fname + ext;
        }

        public override string ToString()
        {
            return Name + " {" + Parameter.Characters + "}";
        }
    }
    public class SingleStringParameter
    {
        [FileRenameParameter("Characters")]
        [FileRenameParameterValidator(typeof(NotEmptyValidator))]
        public string Characters { get; set; } = " \\t\\n\\r";
    }
}
