﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class NewFileNameAction : IFileRenameAction<NewFileNameParameter>
    {
        public NewFileNameParameter Parameter { get; } = new NewFileNameParameter();

        public string Name { get; } = "New Name";

        public string Id { get; } = "newname";

        public string Description { get; } = "Change file name to a new name. You can use variables and format to manipulate the name.";

        public object? GetParameter()
        {
            return Parameter;
        }
        public string Rename(string name, RenameParameterStorage renameParameterStorage)
        {
            return renameParameterStorage.ParseVariables(Parameter.FileName);
        }
        public override string ToString()
        {
            return Name + " {" + Parameter.FileName + "}";
        }
    }

    public class NewFileNameParameter
    {
        [FileRenameParameter("File Name")]
        [FileRenameParameterValidator(typeof(NotEmptyValidator))]
        public string FileName { get; set; } = "New File Name";
    }
}
