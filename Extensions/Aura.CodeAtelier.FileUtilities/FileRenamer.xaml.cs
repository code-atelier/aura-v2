﻿using Aura.CodeAtelier.Models;
using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using CheckBox = System.Windows.Controls.CheckBox;
using MessageBox = System.Windows.MessageBox;
using Path = System.IO.Path;
using TextBox = System.Windows.Controls.TextBox;

namespace Aura.CodeAtelier
{
    /// <summary>
    /// Interaction logic for FileRenamer.xaml
    /// </summary>
    public partial class FileRenamer : Window
    {
        Dictionary<string, IFileRenameAction> Actions { get; } = new Dictionary<string, IFileRenameAction>();

        List<string> Files { get; } = new List<string>();

        ListBoxItem? EditedLBI { get; set; }

        public FileRenamer()
        {
            InitializeComponent();
            btnAddStep.IsEnabled = false;
        }

        public void LoadActions(Dictionary<string, IFileRenameAction> actions)
        {
            Actions.Clear();
            foreach (var action in actions)
            {
                Actions[action.Key] = action.Value;
            }
            cbActions.Items.Clear();
            foreach (var action in Actions)
            {
                var cbi = new ComboBoxItem();
                cbi.Content = action.Value.Name;
                cbi.Tag = action.Value;
                cbActions.Items.Add(cbi);
            }
            tbActionDesc.Text = "";
        }

        public void LoadFiles(IEnumerable<string> files)
        {
            Files.Clear();
            Files.AddRange(files.Where(File.Exists).Select(System.IO.Path.GetFullPath));
            Files.Sort();
            RefreshGrid();
        }

        private void cbActions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            spEditor?.Children.Clear();
            EditedLBI = null;
            if (tbActionDesc != null) tbActionDesc.Text = "";
            if (btnAddStep != null)
            {
                btnAddStep.IsEnabled = cbActions.SelectedIndex >= 0;
                btnAddStep.Content = "Add Step";
            }
            if (cbActions?.SelectedItem is ComboBoxItem cbi && cbi.Tag is IFileRenameAction fra && tbActionDesc != null)
            {
                tbActionDesc.Text = fra.Description;
                BuildParameterEditors(fra);
            }
        }

        private void BuildParameterEditors(IFileRenameAction fra, object? paramvalue = null)
        {
            spEditor?.Children.Clear();
            var param = paramvalue ?? fra.GetParameter();
            if (param != null)
            {
                var pType = param.GetType();
                var props = pType.GetProperties().Where(x => x.CanWrite && x.CanRead);
                foreach (var prop in props)
                {
                    var attr = prop.GetCustomAttribute<FileRenameParameterAttribute>();
                    if (attr != null)
                    {
                        BuildEditor(new ParameterBinding(prop, attr), paramvalue ?? param);
                    }
                }
            }
        }

        private class ParameterBinding
        {
            public PropertyInfo PropertyInfo { get; }

            public FileRenameParameterAttribute Attribute { get; }

            public ParameterBinding(PropertyInfo propertyInfo, FileRenameParameterAttribute attribute)
            {
                PropertyInfo = propertyInfo;
                Attribute = attribute;
            }
        }

        private void BuildLabel(ParameterBinding binding)
        {
            var textBlock = new TextBlock();
            textBlock.Margin = new Thickness(0, 4, 0, 0);
            textBlock.Text = binding.Attribute.Name;
            spEditor.Children.Add(textBlock);
        }

        private void BuildEditor(ParameterBinding binding, object? paramObject = null)
        {
            if (binding.PropertyInfo.PropertyType == typeof(bool))
            {
                var tx = new CheckBox();
                tx.Content = binding.Attribute.Name;
                tx.Margin = new Thickness(0, 2, 0, 0);
                tx.Tag = binding;
                try
                {
                    if (paramObject != null && binding.PropertyInfo.GetValue(paramObject) is bool b)
                    {
                        tx.IsChecked = b;
                    }
                }
                catch { }
                spEditor.Children.Add(tx);
            }

            if (binding.PropertyInfo.PropertyType == typeof(string))
            {
                BuildLabel(binding);
                var tx = new TextBox();
                tx.Tag = binding;
                try
                {
                    if (paramObject != null && binding.PropertyInfo.GetValue(paramObject) is string val)
                    {
                        tx.Text = val;
                    }
                }
                catch { }
                spEditor.Children.Add(tx);
            }
        }

        private void btnAddStep_Click(object sender, RoutedEventArgs e)
        {
            if (cbActions?.SelectedItem is ComboBoxItem cbi && cbi.Tag is IFileRenameAction fra)
            {
                var action = Activator.CreateInstance(fra.GetType()) as IFileRenameAction;
                if (action == null)
                {
                    MessageBox.Show("Cannot instantiate a new instance for action: " + fra.GetType().Name, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                foreach (var control in spEditor.Children.OfType<FrameworkElement>().Where(x => x.Tag is ParameterBinding))
                {
                    object? value = null;
                    if (control is TextBox tx) value = tx.Text;
                    if (control is CheckBox cb) value = cb.IsChecked;

                    var binding = control.Tag as ParameterBinding;
                    if (binding != null)
                    {
                        try
                        {
                            var validators = binding.PropertyInfo.GetCustomAttributes<FileRenameParameterValidatorAttribute>();
                            foreach (var validator in validators)
                            {
                                var res = validator.Validate(value);
                                if (!res.IsSuccess)
                                {
                                    var msg = res.Error ?? "Validation failed for {field}";
                                    msg = msg.Replace("{field}", binding.Attribute.Name);
                                    MessageBox.Show(msg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                    return;
                                }
                            }

                            binding.PropertyInfo.SetValue(action.GetParameter(), value);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }
                }
                var lbi = EditedLBI ?? new ListBoxItem();
                lbi.Content = action.ToString();
                lbi.Tag = action;
                lbi.MouseDoubleClick += Lbi_MouseDoubleClick;
                if (!lbActionList.Items.Contains(lbi))
                    lbActionList.Items.Add(lbi);
                cbActions.SelectedIndex = -1;
                RefreshGrid();
            }
        }

        private void Lbi_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender is ListBoxItem listBoxItem && listBoxItem.Tag is IFileRenameAction)
            {
                EditStep(listBoxItem);
            }
        }

        private void EditStep(ListBoxItem listBoxItem)
        {
            if (listBoxItem.Tag is IFileRenameAction fra)
            {
                var cbi = cbActions.Items.OfType<ComboBoxItem>().Where(x => x.Tag is IFileRenameAction frx && frx.GetType() == fra.GetType()).FirstOrDefault();
                if (cbi != null)
                {
                    var param = fra.GetParameter();
                    cbActions.SelectedItem = cbi;
                    BuildParameterEditors(fra, param);
                    EditedLBI = listBoxItem;
                    btnAddStep.Content = "Save Step";
                }
            }
        }

        class DataItem
        {
            public string? Name { get; set; }
            public string? Renamed { get; set; }
            public string? Directory { get; set; }

            public DataItemStatus Status { get; set; } = DataItemStatus.OK;

            public SolidColorBrush Foreground
            {
                get
                {
                    switch (Status)
                    {
                        case DataItemStatus.Duplicate:
                        case DataItemStatus.Invalid:
                            return new SolidColorBrush(Colors.Crimson);
                        case DataItemStatus.Skip:
                            return new SolidColorBrush(Colors.Gray);
                        default:
                        case DataItemStatus.OK:
                            return new SolidColorBrush(Colors.Black);
                    }
                }
            }

        }

        enum DataItemStatus
        {
            Skip,
            Duplicate,
            Invalid,
            OK
        }

        List<DataItem> RenameItems { get; } = new List<DataItem>();
        private void RefreshGrid()
        {
            RenameItems.Clear();
            dgvFiles.Items.Clear();
            int index = 1;
            foreach (var f in Files)
            {
                var dir = Path.GetDirectoryName(f);
                var name = Path.GetFileName(f);
                var data = new DataItem()
                {
                    Name = name,
                    Renamed = DoRename(name, index++),
                    Directory = dir
                };
                data.Status = data.Name.ToLower() == data.Renamed?.ToLower() ? DataItemStatus.Skip : DataItemStatus.OK;
                RenameItems.Add(data);
            }

            foreach (var item in RenameItems)
            {
                var invalidChars = "/\\?*:|\"<>";
                if (RenameItems.Any(x => x != item && x.Renamed?.ToLower() == item.Renamed?.ToLower()))
                {
                    item.Status = DataItemStatus.Duplicate;
                }
                else if (item.Renamed?.Any(x => invalidChars.Contains(x)) != false)
                {
                    item.Status = DataItemStatus.Invalid;
                }
            }

            foreach (var item in RenameItems)
            {
                dgvFiles.Items.Add(item);
            }

            btnStart.IsEnabled = !RenameItems.Any(x => x.Status == DataItemStatus.Duplicate || x.Status == DataItemStatus.Invalid) && !RenameItems.All(x => x.Status == DataItemStatus.Skip);
        }

        private string DoRename(string name, int index)
        {
            var storage = new RenameParameterStorage();
            storage.Variables["index"] = index.ToString();

            storage.Variables["origin"] = name;
            storage.Variables["name"] = Path.GetFileName(name) ?? "";
            storage.Variables["ext"] = Path.GetExtension(name) ?? "";

            storage.Variables["prev"] = name;
            storage.Variables["prev.name"] = Path.GetFileName(name) ?? "";
            storage.Variables["prev.ext"] = Path.GetExtension(name) ?? "";

            foreach (var lbi in lbActionList.Items.OfType<ListBoxItem>())
            {
                if (lbi.Tag is IFileRenameAction fra)
                {
                    name = fra.Rename(name, storage);
                    storage.Variables["prev"] = name;
                    storage.Variables["prev.name"] = Path.GetFileName(name) ?? "";
                    storage.Variables["prev.ext"] = Path.GetExtension(name) ?? "";
                }
            }
            return name;
        }

        private async void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Start renaming?\r\nIt might take a moment to rename the files.", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                return;

            Dispatcher.Invoke(() =>
            {
                btnLoadSteps.IsEnabled =
                gbAddStep.IsEnabled =
                gbSteps.IsEnabled =
                btnStart.IsEnabled = false;
            });

            try
            {
                await Task.Run(() =>
                {
                    var undoList = new Dictionary<string, string>();
                    var current = 0;
                    try
                    {
                        foreach (var item in RenameItems)
                        {
                            current++;
                            if (item.Status == DataItemStatus.Skip) continue;
                            Dispatcher.Invoke(() =>
                            {
                                tbProgress.Text = $"({current}/{RenameItems.Count}) {item.Renamed}...";
                            });
                            if (Directory.Exists(item.Directory) && !string.IsNullOrWhiteSpace(item.Name) && !string.IsNullOrWhiteSpace(item.Renamed))
                            {
                                var sourceFile = Path.Combine(item.Directory, item.Name);
                                if (File.Exists(sourceFile))
                                {
                                    var destFile = Path.Combine(item.Directory, item.Renamed);
                                    File.Move(sourceFile, destFile);
                                    undoList.Add(sourceFile, destFile);
                                }
                            }
                        }
                        Dispatcher.Invoke(() =>
                        {
                            tbProgress.Text += $" OK";
                        });
                    }
                    catch (Exception ex)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            tbProgress.Text += $" ERROR";
                        });
                        if (MessageBox.Show("Error: " + ex.Message + "\r\nDo you want to undo previous operations?", "Error", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes)
                        {
                            foreach (var undo in undoList)
                            {
                                try
                                {
                                    File.Move(undo.Value, undo.Key);
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            Dispatcher.Invoke(() =>
                            {
                                Close();
                            });
                        }
                        return;
                    }

                    MessageBox.Show("Rename completed!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    Dispatcher.Invoke(() =>
                    {
                        Close();
                    });
                });
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    btnLoadSteps.IsEnabled =
                    gbAddStep.IsEnabled =
                    gbSteps.IsEnabled =
                    btnStart.IsEnabled = true;
                });
            }
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            var msg = new List<string>()
            {
                "Here are the available internal variables:",
                "{index} - the index of the file in the table",
                "{origin} - the original name of the file",
                "{name} - the name part of the original file name",
                "{ext} - the extension part of the original file name",
                "{prev} - the name of the file after the previous step",
                "{prev.name} - the name part of the file name after the previous step",
                "{prev.ext} - the extension part of the file name after the previous step",
            };
            MessageBox.Show(string.Join("\r\n", msg), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            msg = new List<string>()
            {
                "Here are the formats:",
                "{varname:###} - pad the left of this variable with zeroes up to the count of the # characters",
                "{varname:A} - covert the text to uppercase",
                "{varname:a} - covert the text to lowercase",
            };
            MessageBox.Show(string.Join("\r\n", msg), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnSaveSteps_Click(object sender, RoutedEventArgs e)
        {
            List<FileRenameActionDTO> fras = new List<FileRenameActionDTO>();
            foreach (var lbi in lbActionList.Items.OfType<ListBoxItem>())
            {
                if (lbi.Tag is IFileRenameAction fra)
                {
                    fras.Add(FileRenameActionDTO.FromInterface(fra));
                }
            }
            if (fras.Count == 0) return;

            try
            {
                var dir = Path.GetFullPath("data\\filerenamer\\steps");
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                var sfd = new SaveFileDialog();
                sfd.Filter = "Steps Definition|*.steps";
                sfd.InitialDirectory = dir;
                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var json = JsonSerializer.Serialize(fras);
                    File.WriteAllText(sfd.FileName, json);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnLoadSteps_Click(object sender, RoutedEventArgs e)
        {
            ImportSteps(true);
        }

        private void ImportSteps(bool clear)
        {
            try
            {
                var dir = Path.GetFullPath("data\\filerenamer\\steps");
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                var ofd = new OpenFileDialog();
                ofd.Filter = "Steps Definition|*.steps";
                ofd.InitialDirectory = dir;
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var json = File.ReadAllText(ofd.FileName);
                    var dtos = JsonSerializer.Deserialize<List<FileRenameActionDTO>>(json);
                    if (dtos == null || dtos.Count == 0)
                    {
                        MessageBox.Show("Invalid step definition file!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (clear)
                        lbActionList.Items.Clear();
                    foreach (var dto in dtos)
                    {
                        if (dto != null && Actions.ContainsKey(dto.Id))
                        {
                            var act = Actions[dto.Id];
                            var action = Activator.CreateInstance(act.GetType()) as IFileRenameAction;
                            if (action != null)
                            {
                                // set params
                                var paramObj = dto.Parameter;
                                var actParam = action.GetParameter();
                                if (paramObj != null && actParam != null && paramObj is JsonElement ele)
                                {
                                    var parType = actParam.GetType();
                                    var props = parType.GetProperties().Where(x => x.CanRead && x.CanWrite && x.GetCustomAttribute<FileRenameParameterAttribute>() != null);
                                    foreach (var prop in props)
                                    {
                                        try
                                        {
                                            var eleProp = ele.GetProperty(prop.Name);
                                            if (prop.PropertyType == typeof(bool))
                                                prop.SetValue(actParam, eleProp.GetBoolean());
                                            if (prop.PropertyType == typeof(string))
                                                prop.SetValue(actParam, eleProp.GetString());
                                        }
                                        catch { }
                                    }
                                }

                                var lbi = new ListBoxItem();
                                lbi.Content = action.ToString();
                                lbi.Tag = action;
                                lbi.MouseDoubleClick += Lbi_MouseDoubleClick;
                                lbActionList.Items.Add(lbi);
                            }
                            else
                            {
                                throw new Exception($"Failed to create instance of action {act.GetType().Name}");
                            }
                        }
                        else
                        {
                            throw new Exception($"Failed to find action with id '{dto?.Id ?? "null"}'");
                        }
                    }
                    cbActions.SelectedIndex = -1;
                    RefreshGrid();
                }
            }
            catch (Exception ex)
            {
                lbActionList.Items.Clear();
                cbActions.SelectedIndex = -1;
                RefreshGrid();
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void lbActionList_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DeleteStep();
            }
        }

        private void DeleteStep()
        {
            if (lbActionList.SelectedItem is ListBoxItem listBoxItem && listBoxItem.Tag is IFileRenameAction)
            {
                if (MessageBox.Show("Delete this step?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                {
                    return;
                }

                lbActionList.Items.Remove(listBoxItem);
                RefreshGrid();
            }
        }

        private void btnActionDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteStep();
        }

        private void btnActionEdit_Click(object sender, RoutedEventArgs e)
        {
            if (lbActionList.SelectedItem is ListBoxItem listBoxItem && listBoxItem.Tag is IFileRenameAction)
            {
                EditStep(listBoxItem);
            }
        }

        private void lbActionList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbActionList != null && btnActionDelete != null && btnActionEdit != null && btnActionUp != null && btnActionDown != null)
            {
                btnActionEdit.IsEnabled = btnActionDelete.IsEnabled = lbActionList.SelectedItem is ListBoxItem lbi && lbi.Tag is IFileRenameAction;
                var idx = -1;
                if (lbActionList.SelectedItem is ListBoxItem lbx)
                    idx = lbActionList.Items.IndexOf(lbx);
                btnActionUp.IsEnabled = idx > 0 && btnActionEdit.IsEnabled;
                btnActionDown.IsEnabled = idx < lbActionList.Items.Count - 1 && btnActionEdit.IsEnabled;
            }
        }

        private void btnActionUp_Click(object sender, RoutedEventArgs e)
        {
            if (lbActionList.SelectedItem is ListBoxItem listBoxItem && listBoxItem.Tag is IFileRenameAction)
            {
                var idx = lbActionList.Items.IndexOf(listBoxItem);
                if (idx < 1) return;
                lbActionList.Items.Remove(listBoxItem);
                lbActionList.Items.Insert(idx - 1, listBoxItem);
                lbActionList.SelectedItem = listBoxItem;
                RefreshGrid();
            }
        }

        private void btnActionDown_Click(object sender, RoutedEventArgs e)
        {
            if (lbActionList.SelectedItem is ListBoxItem listBoxItem && listBoxItem.Tag is IFileRenameAction)
            {
                var idx = lbActionList.Items.IndexOf(listBoxItem);
                if (idx >= lbActionList.Items.Count - 1) return;
                lbActionList.Items.Remove(listBoxItem);
                lbActionList.Items.Insert(idx + 1, listBoxItem);
                lbActionList.SelectedItem = listBoxItem;
                RefreshGrid();
            }
        }

        private void btnImportSteps_Click(object sender, RoutedEventArgs e)
        {
            ImportSteps(false);
        }
    }
}
