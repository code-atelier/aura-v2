﻿using Aura.Models;
using System.IO;
using System.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;
using Message = Aura.Services.Message;

namespace Aura.CodeAtelier
{
    public partial class ImageUtilityService
    {
        private static string[] DiffImageExtensions { get; } = new string[]
        {
            ".png",
            ".jpg",
            ".jpeg",
            ".bmp",
            ".tga"
        };

        private List<FileDropHandler> GetDiffActionsFor(FileDropEventBody body)
        {
            var res = new List<FileDropHandler>();
            if (body.Files.Count > 1 && body.Files.All(x => DiffImageExtensions.Contains(Path.GetExtension(x).ToLower())))
            {
                var replyBody = new FileDropHandler()
                {
                    Action = "imgdiff",
                    Id = Id,
                    Name = "Image Diff",
                    Icon = Util.CreateResourceUri("res/img/file-picture-diff.png"),
                };
                res.Add(replyBody);
            }
            return res;
        }

        private async Task<Message?> HandleDiffAction(Message message, FileDropExecuteEventBody body)
        {
            if (body.Handler.Action == "imgdiff" && ServiceManager.MainDispatcher != null && body.Files.Count > 1)
            {
                await Task.Run(() =>
                {
                    ServiceManager.MainDispatcher.Invoke(() =>
                    {
                        var w = new ImageDiffWindow();
                        w.LoadFiles(body.Files);
                        w.Show();
                        w.Activate();
                    });
                });
                return message.CreateReply(Id);
            }
            return null;
        }
    }
}
