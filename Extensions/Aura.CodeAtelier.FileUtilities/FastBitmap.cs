﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public class FastBitmap : IDisposable
    {
        public Bitmap Bitmap { get; }

        public BitmapData BitmapData { get; }

        public PixelFormat PixelFormat { get; }

        public byte[] RawData { get; }

        public bool CanRead { get; }

        public bool CanWrite { get; }

        public int Height { get; }

        public int Width { get; }

        public ColorPixelsCollection Rows
        {
            get
            {
                return new ColorPixelsCollection(this, ReadDirection.Row);
            }
        }

        public ColorPixelsCollection Columns
        {
            get
            {
                return new ColorPixelsCollection(this, ReadDirection.Column);
            }
        }

        public FastBitmap(Bitmap bitmap, ImageLockMode imageLockMode = ImageLockMode.ReadWrite)
        {
            Bitmap = bitmap;
            var rectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            Width = rectangle.Width;
            Height = rectangle.Height;
            BitmapData = bitmap.LockBits(rectangle, imageLockMode, bitmap.PixelFormat);
            CanRead = imageLockMode != ImageLockMode.WriteOnly;
            CanWrite = imageLockMode != ImageLockMode.ReadOnly;
            PixelFormat = bitmap.PixelFormat;
            var dataLength = Math.Abs(BitmapData.Stride) * rectangle.Height;
            if (CanRead)
            {
                RawData = new byte[dataLength];
                Marshal.Copy(BitmapData.Scan0, RawData, 0, dataLength);
            }
            else
            {
                RawData = new byte[dataLength];
            }
        }

        public FastBitmap(Bitmap bitmap, Rectangle rectangle, ImageLockMode imageLockMode = ImageLockMode.ReadWrite)
        {
            Bitmap = bitmap;
            Width = rectangle.Width;
            Height = rectangle.Height;
            BitmapData = bitmap.LockBits(rectangle, imageLockMode, bitmap.PixelFormat);
            CanRead = imageLockMode != ImageLockMode.WriteOnly;
            CanWrite = imageLockMode != ImageLockMode.ReadOnly;
            PixelFormat = bitmap.PixelFormat;
            var dataLength = Math.Abs(BitmapData.Stride) * rectangle.Height;
            if (CanRead)
            {
                RawData = new byte[dataLength];
                Marshal.Copy(BitmapData.Scan0, RawData, 0, dataLength);
            }
            else
            {
                RawData = new byte[dataLength];
            }
        }

        public FastBitmap(Bitmap bitmap, Rectangle rectangle, PixelFormat pixelFormat, ImageLockMode imageLockMode = ImageLockMode.ReadWrite)
        {
            Bitmap = bitmap;
            Width = rectangle.Width;
            Height = rectangle.Height;
            BitmapData = bitmap.LockBits(rectangle, imageLockMode, pixelFormat);
            CanRead = imageLockMode != ImageLockMode.WriteOnly;
            CanWrite = imageLockMode != ImageLockMode.ReadOnly;
            PixelFormat = pixelFormat;
            var dataLength = Math.Abs(BitmapData.Stride) * rectangle.Height;
            if (CanRead)
            {
                RawData = new byte[dataLength];
                Marshal.Copy(BitmapData.Scan0, RawData, 0, dataLength);
            }
            else
            {
                RawData = new byte[dataLength];
            }
        }

        public ColorPixel GetPixel(int x, int y)
        {
            return new ColorPixel(this, x, y);
        }

        public void Dispose()
        {
            if (CanWrite)
                Marshal.Copy(RawData, 0, BitmapData.Scan0, RawData.Length);
            Bitmap.UnlockBits(BitmapData);
        }
    }

    public class ColorPixel
    {
        public byte Alpha { get => GetComponent(ColorComponent.Alpha); set => SetComponent(ColorComponent.Alpha, value); }

        public byte Red { get => GetComponent(ColorComponent.Red); set => SetComponent(ColorComponent.Red, value); }

        public byte Green { get => GetComponent(ColorComponent.Green); set => SetComponent(ColorComponent.Green, value); }

        public byte Blue { get => GetComponent(ColorComponent.Blue); set => SetComponent(ColorComponent.Blue, value); }

        public byte Palette { get => GetComponent(ColorComponent.Palette); set => SetComponent(ColorComponent.Palette, value); }

        public Color Color
        {
            get
            {
                return Color.FromArgb(Alpha, Red, Green, Blue);
            }
            set
            {
                Alpha = value.A;
                Red = value.R;
                Green = value.G;
                Blue = value.B;
            }
        }

        public FastBitmap Bitmap { get; }

        public int X { get; }

        public int Y { get; }

        public ColorPixel(FastBitmap bitmap, int x, int y)
        {
            Bitmap = bitmap;
            X = x;
            Y = y;
        }

        public byte GetComponent(ColorComponent color)
        {
            if (!Bitmap.CanRead) throw new NotSupportedException("Cannot read from this bitmap");
            var address = GetAddressInRaw(color);
            return Bitmap.RawData[address];
        }

        private int GetAddressInRaw(ColorComponent colorComponent)
        {
            int bytePerPixel;
            switch (Bitmap.PixelFormat)
            {
                default:
                case PixelFormat.Format32bppArgb:
                    bytePerPixel = 4;
                    break;
            }
            var offset = GetOffset(colorComponent);
            return bytePerPixel * Bitmap.Width * Y + bytePerPixel * X + offset;
        }

        private int GetOffset(ColorComponent colorComponent)
        {
            switch (Bitmap.PixelFormat)
            {
                default:
                case PixelFormat.Format32bppArgb:
                    return colorComponent == ColorComponent.Blue ? 0 : colorComponent == ColorComponent.Green ? 1 : colorComponent == ColorComponent.Red ? 2 : colorComponent == ColorComponent.Alpha ? 3 : throw new NotSupportedException();
            }
        }

        public void SetComponent(ColorComponent color, byte value)
        {
            if (!Bitmap.CanWrite) throw new NotSupportedException("Cannot write to this bitmap");
            var address = GetAddressInRaw(color);
            Bitmap.RawData[address] = value;
        }

        public override bool Equals(object? obj)
        {
            if (obj is ColorPixel cp)
            {
                return Color.Equals(cp.Color);
            }
            else if (obj is Color col)
            {
                return Color.Equals(col);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Color.GetHashCode();
        }
    }

    public class ColorPixels : IEnumerable<ColorPixel>
    {
        FastBitmap bitmap;
        ReadDirection direction;
        int index;

        public ColorPixels(FastBitmap bitmap, ReadDirection direction, int index)
        {
            this.bitmap = bitmap;
            this.direction = direction;
            this.index = index;
        }

        public IEnumerator<ColorPixel> GetEnumerator()
        {
            var list = new List<ColorPixel>();
            if (direction == ReadDirection.Column)
            {
                for (var y = 0; y < bitmap.Height; y++)
                {
                    list.Add(new ColorPixel(bitmap, index, y));
                }
            }
            else
            {
                for (var x = 0; x < bitmap.Width; x++)
                {
                    list.Add(new ColorPixel(bitmap, x, index));
                }
            }
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class ColorPixelsCollection : IEnumerable<ColorPixels>
    {
        FastBitmap bitmap;
        ReadDirection direction;

        public ColorPixelsCollection(FastBitmap bitmap, ReadDirection direction)
        {
            this.bitmap = bitmap;
            this.direction = direction;
        }

        public ColorPixels this[int index]
        {
            get
            {
                return new ColorPixels(bitmap, direction, index);
            }
        }

        public IEnumerator<ColorPixels> GetEnumerator()
        {
            var list = new List<ColorPixels>();
            for (var i = 0; i < (direction == ReadDirection.Row ? bitmap.Height : bitmap.Width); i++) { 
                list.Add(new ColorPixels(bitmap, direction, i));
            }
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public enum ColorComponent
    {
        Alpha,
        Red,
        Green,
        Blue,

        Palette,
    }

    public enum ReadDirection
    {
        Row,
        Column
    }
}
