﻿using Aura.CodeAtelier.GameData.ArchiveHandlers;
using Aura.CodeAtelier.GameData.Handlers;
using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.GameData
{
    public class GameArchiveService : PassiveServiceBase, IMessageReceiver
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo
        {
            Author = "CodeAtelier",
            Description = "Allows you to extract files from game archive.",
            Id = "coatl.game.archive",
            Name = "Game Archive Service",
            Version = new Version(1, 0, 0),
            Icon = Util.CreateResourceUri("res/img/arc.png"),
        };

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public string Id { get => Info.Id; }

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.Start | ServiceUserAction.Stop;

        public bool CanReceiveMessage { get => State == ServiceState.Running; }

        List<IGameDataHandler> _archiveHandlers { get; } = new List<IGameDataHandler>();
        public IReadOnlyList<IGameDataHandler> ArchiveHandlers { get => _archiveHandlers.ToList(); }
        private IServiceMessageBus MessageBus { get; set; }

        public GameArchiveService(IServiceMessageBus messageBus)
        {
            MessageBus = messageBus;
        }
        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                _archiveHandlers.Add(new AsarArchiveHandler());

                MessageBus.Subscribe(this, InternalEvents.FileDrop);
                MessageBus.Subscribe(this, InternalEvents.ExecuteFileDrop);
            });
        }

        public async Task<Services.Message?> ReceiveMessageAsync(Services.Message message, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                return message
                .When()
                .FileDropPreview((arg) =>
                {
                    var body = arg.Body;
                    var handlers = new List<FileDropHandler>();
                    foreach (var handler in ArchiveHandlers)
                    {
                        var col = handler.CheckFileDrop(body);
                        if (col != null && col.Any())
                        {
                            handlers.AddRange(col.Select(x => new FileDropHandler()
                            {
                                Id = Info.Id,
                                Action = x.Action,
                                Icon = x.Icon,
                                Name = x.Name
                            }));
                        }
                    }
                    if (handlers.Any())
                    {
                        arg.Handled = true;
                        return message.CreateReply(Info.Id, handlers);
                    }
                    return null;
                })
                .FileDropExecute((arg) =>
                {
                    var body = arg.Body;
                    if (body.Files.Count > 0)
                    {
                        var handler = ArchiveHandlers.FirstOrDefault(x => x.CanExecuteAction(body.Handler.Action ?? "", body.Files));
                        if (handler != null)
                        {
                            foreach (var file in body.Files)
                            {
                                var res = handler.ExecuteAction(body.Handler.Action ?? "", file, body.Files);
                                if (res != null)
                                {
                                    ToastNotif(handler.Name, res.Message, res.OutputFilePath);
                                }
                            }
                            arg.Handled = true;
                        }
                    }
                    return null;
                })
                .Reply;
            });
        }
        private async void ToastNotif(string title, string body, string? openPath = null)
        {
            var toast = new ToastNotification();
            toast.Title = title;
            toast.Contents.Add(new ToastNotificationContent()
            {
                Kind = ToastNotificationContentKind.Text,
                Text = body,
            });
            if (openPath != null)
            {
                toast.Contents.Add(new ToastNotificationContent()
                {
                    Text = "Show in Explorer",
                    Kind = ToastNotificationContentKind.Button,
                    Arguments = new Dictionary<string, string> {
                                    { "event", Directory.Exists(openPath) ? InternalEvents.ToastOpenFileRequest : InternalEvents.ToastShowInExplorerRequest },
                                    { "body", openPath }
                                }
                });
            }
            toast.Contents.Add(new ToastNotificationContent()
            {
                Text = "Dismiss",
                Kind = ToastNotificationContentKind.Button,
                ButtonActivation = ToastNotificationButtonActivation.Dismiss,
            });
            var tNotif = new Message<ToastNotification>(Id, InternalEvents.ShowToastNotificationRequest, toast);
            await MessageBus.SendMessageAsync(tNotif);
        }
    }
}
