﻿using Aura.CodeAtelier.GameData.Handlers;
using Aura.CodeAtelier.Models;
using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.GameData
{
    public class GameImageService : PassiveServiceBase, IMessageReceiver
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo
        {
            Author = "CodeAtelier",
            Description = "Allows you to rebuild images from game files.",
            Id = "coatl.game.image",
            Name = "Game Image Service",
            Version = new Version(1, 0, 10),
            Icon = Util.CreateResourceUri("res/img/file-picture.png"),
        };

        public string Id { get => Info.Id; }

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.Start | ServiceUserAction.Stop;

        private IServiceMessageBus MessageBus { get; set; }

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public bool CanReceiveMessage { get => State == ServiceState.Running; }

        List<IGameDataHandler> _imageHandlers { get; } = new List<IGameDataHandler>();
        public IReadOnlyList<IGameDataHandler> ImageHandlers { get => _imageHandlers.ToList(); }

        public GameImageService(IServiceMessageBus messageBus)
        {
            MessageBus = messageBus;
        }

        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                _imageHandlers.Add(new LSFImageHandler());
                _imageHandlers.Add(new WonderFoolFgImageHandler());
                _imageHandlers.Add(new RPGMakerToBakinHandler());
                _imageHandlers.Add(new AutoCompositePngBuilder());

                MessageBus.Subscribe(this, InternalEvents.FileDrop);
                MessageBus.Subscribe(this, InternalEvents.ExecuteFileDrop);
            });
        }

        public async Task<Services.Message?> ReceiveMessageAsync(Services.Message message, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                return message
                .When()
                .FileDropPreview((arg) =>
                {
                    var body = arg.Body;
                    var handlers = new List<FileDropHandler>();
                    foreach(var handler in ImageHandlers)
                    {
                        var col = handler.CheckFileDrop(body);
                        if (col != null && col.Any())
                        {
                            handlers.AddRange(col.Select(x => new FileDropHandler()
                            {
                                Id = Info.Id,
                                Action = x.Action,
                                Icon = x.Icon,
                                Name = x.Name
                            }));
                        }
                    }
                    if (handlers.Any())
                    {
                        arg.Handled = true;
                        return message.CreateReply(Info.Id, handlers);
                    }
                    return null;
                })
                .FileDropExecute((arg) =>
                {
                    var body = arg.Body;
                    if (body.Files.Count > 0)
                    {
                        var handler = ImageHandlers.FirstOrDefault(x => x.CanExecuteAction(body.Handler.Action ?? "", body.Files));
                        if (handler != null)
                        {
                            handler.Begin(body.Files);
                            foreach (var file in body.Files)
                            {
                                var res = handler.ExecuteAction(body.Handler.Action ?? "", file, body.Files);
                                if (res != null)
                                {
                                    ToastNotif(handler.Name, res.Message, res.OutputFilePath);
                                }
                            }
                            handler.End();
                            arg.Handled = true;
                        }
                    }
                    return null;
                })
                .Reply;
            });
        }

        private async void ToastNotif(string title, string body, string? openPath = null)
        {
            var toast = new ToastNotification();
            toast.Title = title;
            toast.Contents.Add(new ToastNotificationContent()
            {
                Kind = ToastNotificationContentKind.Text,
                Text = body,
            });
            if (openPath != null)
            {
                toast.Contents.Add(new ToastNotificationContent()
                {
                    Text = "Show in Explorer",
                    Kind = ToastNotificationContentKind.Button,
                    Arguments = new Dictionary<string, string> {
                                    { "event", Directory.Exists(openPath) ? InternalEvents.ToastOpenFileRequest : InternalEvents.ToastShowInExplorerRequest },
                                    { "body", openPath }
                                }
                });
            }
            toast.Contents.Add(new ToastNotificationContent()
            {
                Text = "Dismiss",
                Kind = ToastNotificationContentKind.Button,
                ButtonActivation = ToastNotificationButtonActivation.Dismiss,
            });
            var tNotif = new Message<ToastNotification>(Id, InternalEvents.ShowToastNotificationRequest, toast);
            await MessageBus.SendMessageAsync(tNotif);
        }
    }
}
