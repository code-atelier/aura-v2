﻿using Aura.Models;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Aura.CodeAtelier.GameData.ArchiveHandlers
{
    public class AsarArchiveHandler : IGameDataHandler
    {
        const string handlerActionId = "asar-archive-extract";
        public string Name { get; } = "Asar Arc Extract";

        public void Begin(IEnumerable<string> files)
        {
        }

        public bool CanExecuteAction(string? handlerAction, IEnumerable<string> files)
        {
            return handlerAction == handlerActionId && files.All(x => Path.GetExtension(x)?.ToLower() == ".asar");
        }

        public IEnumerable<GameDataHandlerInfo>? CheckFileDrop(FileDropEventBody body)
        {
            if (body.Files.All(x => Path.GetExtension(x)?.ToLower() == ".asar"))
            {
                return new List<GameDataHandlerInfo>()
                {
                    new GameDataHandlerInfo()
                    {
                        Action = handlerActionId,
                        Icon = Util.CreateResourceUri("res/img/arc_asar.png"),
                        Name = Name,
                    }
                };
            }
            return null;
        }

        public void End()
        {
        }

        public GameDataHandlerResult? ExecuteAction(string handlerAction, string file, IEnumerable<string> files)
        {
            if (handlerAction != handlerActionId) return null;
            var fname = Path.GetFileName(file);
            try
            {
                var dir = Path.GetDirectoryName(file);
                if (!Directory.Exists(dir))
                {
                    return null;
                }
                using (var fi = File.OpenRead(file))
                {
                    var buf_int = new byte[4];
                    fi.Read(buf_int);
                    var vint_a = BitConverter.ToInt32(buf_int);
                    fi.Read(buf_int);
                    var vint_b = BitConverter.ToInt32(buf_int);
                    fi.Read(buf_int);
                    var vint_c = BitConverter.ToInt32(buf_int);
                    fi.Read(buf_int);
                    var json_len = BitConverter.ToInt32(buf_int);

                    var buf_json = new byte[json_len];
                    fi.Read(buf_json);
                    var str = Encoding.UTF8.GetString(buf_json);
                    var tf = JsonSerializer.Deserialize<TyranoFile>(str);
                    if (tf == null)
                    {
                        throw new Exception("Failed to parse Tyrano JSON");
                    }
                    long ptr0 = vint_c + 12;

                    var odir = Path.Combine(dir, "extract");
                    TyranoExtract.Extract(ptr0, fi, tf, odir);

                    return GameDataHandlerResult.Create($"Extract completed '{fname}'", odir);
                }
            }
            catch (Exception ex)
            {
                return GameDataHandlerResult.Create($"Failed to extract '{fname}'\r\n{ex.Message}");
            }
        }
    }
    public class TyranoFile
    {
        [JsonPropertyName("files")]
        public Dictionary<string, TyranoFile>? Files { get; set; }

        [JsonPropertyName("size")]
        public long? Size { get; set; }

        [JsonPropertyName("offset")]
        public string? Offset { get; set; }

        [JsonIgnore]
        public bool IsFolder { get => Files != null; }
    }

    public static class TyranoExtract
    {
        public static void Extract(long ptr0, Stream stream, TyranoFile file, string path)
        {
            ExtractRecursive(ptr0, stream, null, file, path);
        }

        public static void ExtractRecursive(long ptr0, Stream stream, string? key, TyranoFile file, string path)
        {
            long offset;
            if (file.IsFolder && file.Files != null)
            {
                foreach (var kv in file.Files)
                {
                    var npath = Path.Combine(path, kv.Key);
                    if (kv.Value.IsFolder)
                    {
                        if (!Directory.Exists(npath))
                            Directory.CreateDirectory(npath);
                        ExtractRecursive(ptr0, stream, kv.Key, kv.Value, npath);
                    }
                    else
                        ExtractRecursive(ptr0, stream, kv.Key, kv.Value, path);
                }
            }
            else if (!string.IsNullOrWhiteSpace(key) && file.Offset != null && long.TryParse(file.Offset, out offset) && file.Size != null)
            {
                stream.Position = ptr0 + offset;
                var buf = new byte[file.Size.Value];
                stream.Read(buf);

                var fname = Path.Combine(path, key);
                File.WriteAllBytes(fname, buf);
            }
        }

        public static string ToHex(long value)
        {
            var ar = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(ar);
            return BitConverter.ToString(ar).Replace("-", "");
        }
        public static string ToHex(int value)
        {
            var ar = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(ar);
            return BitConverter.ToString(ar).Replace("-", "");
        }
    }
}
