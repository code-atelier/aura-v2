﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace Aura.CodeAtelier.GameData
{
    public interface IGameDataHandler
    {
        string Name { get; }

        IEnumerable<GameDataHandlerInfo>? CheckFileDrop(FileDropEventBody body);

        bool CanExecuteAction(string? handlerAction, IEnumerable<string> files);

        GameDataHandlerResult? ExecuteAction(string handlerAction, string file, IEnumerable<string> files);

        void Begin(IEnumerable<string> files);

        void End();
    }

    public class GameDataHandlerInfo
    {
        public string Name { get; set; } = string.Empty;

        public string? Icon { get; set; }

        public string? Action { get; set; }
    }

    public class GameDataHandlerResult
    {
        public string Message { get; set; } = string.Empty;

        public string? OutputFilePath { get; set; }

        public static GameDataHandlerResult Create(string message, string? outputFilePath = null)
        {
            return new GameDataHandlerResult
            {
                Message = message,
                OutputFilePath = outputFilePath
            };
        }
    }
}
