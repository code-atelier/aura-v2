﻿using Aura.Models;
using MetadataExtractor;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using Directory = System.IO.Directory;

namespace Aura.CodeAtelier.GameData.Handlers
{
    public class WonderFoolFgImageHandler : IGameDataHandler
    {
        const string handlerActionId = "wffg-build";
        public string Name { get; } = "WonderFool FG";

        public void Begin(IEnumerable<string> files)
        {
        }

        public bool CanExecuteAction(string? handlerAction, IEnumerable<string> files)
        {
            return handlerAction == handlerActionId && files.All(x => Path.GetExtension(x)?.ToLower() == ".png");
        }

        public IEnumerable<GameDataHandlerInfo>? CheckFileDrop(FileDropEventBody body)
        {
            if (body.Files.All(x => Path.GetExtension(x)?.ToLower() == ".png"))
            {
                return new List<GameDataHandlerInfo>()
                {
                    new GameDataHandlerInfo()
                    {
                        Action = handlerActionId,
                        Icon = Util.CreateResourceUri("res/img/wffg.png"),
                        Name = Name,
                    }
                };
            }
            return null;
        }

        public void End()
        {
        }

        public GameDataHandlerResult? ExecuteAction(string handlerAction, string file, IEnumerable<string> files)
        {
            if (handlerAction != handlerActionId) return null;
            int canvasWidth = 0;
            int canvasHeight = 0;
            int minX = int.MaxValue;
            int minY = int.MaxValue;
            foreach(var f in files)
            {
                try
                {
                    var rect = WonderFoolFgImageBuilder.GetImageRect(f);
                    if (rect != null)
                    {
                        canvasWidth = Math.Max(canvasWidth, rect[0] + rect[2]);
                        canvasHeight = Math.Max(canvasHeight, rect[1] + rect[3]);
                        minX = Math.Min(minX, rect[0]);
                        minY = Math.Min(minY, rect[1]);
                    }
                }
                catch { }
            }


            var fname = Path.GetFileName(file);
            try
            {
                var outPath = WonderFoolFgImageBuilder.BuildImage(file, canvasWidth, canvasHeight, minX, minY);
                var odir = Path.GetDirectoryName(outPath);
                if (outPath != null)
                {
                    return GameDataHandlerResult.Create($"Build completed '{fname}'", odir);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return GameDataHandlerResult.Create($"Failed to build '{fname}'\r\n{ex.Message}");
            }
        }
    }

    public class WonderFoolFgImageBuilder
    {
        public static int[]? GetImageRect(string file)
        {

            var metadata = ImageMetadataReader.ReadMetadata(file);
            Dictionary<string, string?> tags = new Dictionary<string, string?>();
            foreach (var directory in metadata)
            {
                foreach (var tag in directory.Tags)
                {
                    tags.Add(tag.Name, tag.Description);
                }
            }
            string comment;
            if (tags.ContainsKey("Textual Data"))
            {
                var textData = tags["Textual Data"];
                if (textData?.StartsWith("comment: ") == true)
                {
                    comment = textData.Substring(9);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
            if (comment.StartsWith("pos,"))
            {
                var pos = comment.Substring(4);
                var spl = pos.Split(',');

                return spl.Select(int.Parse).ToArray();
            }
            return null;
        }

        public static string? BuildImage(string file, int canvasWidth, int canvasHeight, int minX, int minY)
        {
            var inputDir = Path.GetDirectoryName(file);
            var outputDir = Path.Combine(inputDir ?? "", "output");
            var outputFile = Path.Combine(outputDir, Path.GetFileName(file));
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }
            var rect = GetImageRect(file);
            if (rect != null)
            {
                var x = rect[0];
                var y = rect[1];

                using (var bmp = new Bitmap(canvasWidth - minX, canvasHeight - minY, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                using (var g = Graphics.FromImage(bmp))
                {
                    using (var im = File.OpenRead(file))
                    {
                        using (var img = Bitmap.FromStream(im))
                            g.DrawImage(img, x - minX, y - minY);

                        bmp.Save(Path.Combine(outputDir, outputFile));
                    }
                }

                return outputFile;
            }
            return null;
        }
    }
}
