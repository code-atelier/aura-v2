﻿using Aura.Models;
using System.IO;
using System.Text;

namespace Aura.CodeAtelier.GameData.Handlers
{
    public partial class AutoCompositePngBuilder : IGameDataHandler
    {
        const string handlerActionId = "autocomp-build";
        public string Name { get; } = "Auto Comp. PNG";

        protected string WorkMode { get; set; } = "none";

        public void Begin(IEnumerable<string> files)
        {
            if (files.All(x => Path.GetExtension(x)?.ToLower() == ".png"))
            {
                WorkMode = "png-pure";
                PureGetImagesInfo(files);
            }
            if (files.All(x => Path.GetExtension(x)?.ToLower() == ".txt"))
            {
                WorkMode = "text-lamia";
            }
        }

        public void End()
        {
        }

        public bool CanExecuteAction(string? handlerAction, IEnumerable<string> files)
        {
            return handlerAction == handlerActionId &&
            (
                files.All(x => Path.GetExtension(x)?.ToLower() == ".png")
                || files.All(x => Path.GetExtension(x)?.ToLower() == ".txt")
            );
        }

        public IEnumerable<GameDataHandlerInfo>? CheckFileDrop(FileDropEventBody body)
        {
            if (
                body.Files.All(x => Path.GetExtension(x)?.ToLower() == ".png")
                || body.Files.All(x => Path.GetExtension(x)?.ToLower() == ".txt")
                )
            {
                return new List<GameDataHandlerInfo>()
                {
                    new GameDataHandlerInfo()
                    {
                        Action = handlerActionId,
                        Icon = Util.CreateResourceUri("res/img/autocomp.png"),
                        Name = Name,
                    }
                };
            }
            return null;
        }

        public GameDataHandlerResult? ExecuteAction(string handlerAction, string file, IEnumerable<string> files)
        {
            if (handlerAction != handlerActionId) return null;
            var dir = Path.GetDirectoryName(file);
            var fname = Path.GetFileName(file);
            var fnameNoExt = Path.GetFileNameWithoutExtension(fname);

            if (WorkMode == "png-pure")
            {
                try
                {
                    var imgInfo = PureImages.FirstOrDefault(x => x.ImagePath == file);
                    if (imgInfo != null)
                    {
                        var outPath = PureBuildImage(imgInfo);
                        var odir = Path.GetDirectoryName(outPath);
                        if (outPath != null)
                        {
                            return GameDataHandlerResult.Create($"Build completed '{fname}'", odir);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return GameDataHandlerResult.Create($"Failed to build '{fname}'\r\n{ex.Message}");
                }
            }
            if (WorkMode == "text-lamia")
            {
                try
                {
                    // UTF-16
                    using (var sr = new StringReader(File.ReadAllText(file, Encoding.Unicode)))
                    {
                        var firstLine = sr.ReadLine();

                        if (firstLine?.StartsWith("#layer_type\tname\tleft\ttop\t") == true)
                        {
                            // Lamia's composite image
                            var odir = HandleLamia(dir, fnameNoExt, sr);
                            return GameDataHandlerResult.Create($"Build completed '{fname}'", odir);
                        }
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    return GameDataHandlerResult.Create($"Failed to build '{fname}'\r\n{ex.Message}");
                }
            }

            return null;
        }
    }
}
