﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.GameData.Handlers
{
    public class RPGMakerToBakinHandler : IGameDataHandler
    {
        const string handlerActionId = "rpgm2bakin-convert";
        public string Name { get; } = "RPG Maker to Bakin";

        public void Begin(IEnumerable<string> files)
        {
        }

        public bool CanExecuteAction(string? handlerAction, IEnumerable<string> files)
        {
            return handlerAction == handlerActionId && files.All(x => Path.GetExtension(x)?.ToLower() == ".png");
        }

        public IEnumerable<GameDataHandlerInfo>? CheckFileDrop(FileDropEventBody body)
        {
            if (body.Files.All(x => Path.GetExtension(x)?.ToLower() == ".png"))
            {
                return new List<GameDataHandlerInfo>()
                {
                    new GameDataHandlerInfo()
                    {
                        Action = handlerActionId,
                        Icon = Util.CreateResourceUri("res/img/rmmzbakin.png"),
                        Name = Name,
                    }
                };
            }
            return null;
        }

        public void End()
        {
        }

        public GameDataHandlerResult? ExecuteAction(string handlerAction, string file, IEnumerable<string> files)
        {
            const string OutputDir = "bakin-converted";
            if (handlerAction != handlerActionId) return null;

            string fname = Path.GetFileName(file);
            try
            {
                var odir = Path.Combine(Path.GetDirectoryName(file) ?? "", OutputDir);
                if (!Directory.Exists(odir))
                {
                    Directory.CreateDirectory(odir);
                }
                HandleFile(file, odir);
                return GameDataHandlerResult.Create($"Conversion completed '{fname}'", odir);
            }
            catch (Exception ex)
            {
                return GameDataHandlerResult.Create($"Failed to convert '{fname}'\r\n{ex.Message}");
            }
        }

        private void HandleFile(string file, string outputPath)
        {
            var fname = Path.GetFileNameWithoutExtension(file);
            var ofile = Path.Combine(outputPath, fname);
            const string ext = ".png";
            using (var bmp = Bitmap.FromFile(file))
            {
                int colWidth, rowHeight, cellWidth, cellHeight, colCount, rowCount;

                if (fname.StartsWith("!$"))
                {
                    if (bmp.Width / 3.0 % 1 > 0)
                    {
                        throw new Exception("Image width must be divisible by 3");
                    }
                    if (bmp.Height / 4.0 % 1 > 0)
                    {
                        throw new Exception("Image height must be divisible by 4");
                    }
                    colWidth = bmp.Width;
                    rowHeight = bmp.Height;
                    cellWidth = bmp.Width / 3;
                    cellHeight = bmp.Height / 4;
                    colCount = 1;
                    rowCount = 1;
                }
                else
                {
                    if (bmp.Width / 12.0 % 1 > 0)
                    {
                        throw new Exception("Image width must be divisible by 12");
                    }
                    if (bmp.Height / 8.0 % 1 > 0)
                    {
                        throw new Exception("Image height must be divisible by 8");
                    }
                    colWidth = bmp.Width / 4;
                    rowHeight = bmp.Height / 2;
                    cellWidth = bmp.Width / 12;
                    cellHeight = bmp.Height / 8;
                    colCount = 4;
                    rowCount = 2;
                }

                for (var col = 0; col < colCount; col++)
                {
                    for (var row = 0; row < rowCount; row++)
                    {
                        // draw walking
                        using (var obmp = new Bitmap(colWidth, rowHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                        {
                            using (var g = Graphics.FromImage(obmp))
                            {
                                // draw top half
                                g.DrawImage(
                                    bmp,
                                    new Rectangle(0, 0, colWidth, cellHeight * 2), // dest
                                    new Rectangle(colWidth * col, row * rowHeight, colWidth, cellHeight * 2), // source
                                    GraphicsUnit.Pixel);
                                // draw src bottom to next
                                g.DrawImage(
                                    bmp,
                                    new Rectangle(0, cellHeight * 2, colWidth, cellHeight), // dest
                                    new Rectangle(colWidth * col, row * rowHeight + cellHeight * 3, colWidth, cellHeight), // source
                                    GraphicsUnit.Pixel);
                                // draw src second-to-bottom to next
                                g.DrawImage(
                                    bmp,
                                    new Rectangle(0, cellHeight * 3, colWidth, cellHeight), // dest
                                    new Rectangle(colWidth * col, row * rowHeight + cellHeight * 2, colWidth, cellHeight), // source
                                    GraphicsUnit.Pixel);
                                g.Flush();
                            }

                            // detect any image
                            try
                            {
                                using (var fb = new FastBitmap(obmp, System.Drawing.Imaging.ImageLockMode.ReadOnly))
                                {
                                    bool skip = true;
                                    var tlPix = fb.Rows[0].ElementAt(0);
                                    foreach (var r in fb.Rows)
                                    {
                                        foreach (var c in r)
                                        {
                                            if (!c.Equals(tlPix))
                                            {
                                                skip = false;
                                                break;
                                            }
                                        }
                                        if (!skip)
                                        {
                                            break;
                                        }
                                    }
                                    if (skip)
                                    {
                                        continue;
                                    }
                                }
                            }
                            catch { }

                            var ofname = ofile + "_" + (row * 4 + col + 1) + ext;
                            obmp.Save(ofname, System.Drawing.Imaging.ImageFormat.Png);
                        }

                        // draw stand
                        using (var obmp = new Bitmap(cellWidth, rowHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                        {
                            using (var g = Graphics.FromImage(obmp))
                            {
                                // draw top half
                                g.DrawImage(
                                    bmp,
                                    new Rectangle(0, 0, cellWidth, cellHeight * 2), // dest
                                    new Rectangle(colWidth * col + cellWidth, row * rowHeight, cellWidth, cellHeight * 2), // source
                                    GraphicsUnit.Pixel);
                                // draw src bottom to next
                                g.DrawImage(
                                    bmp,
                                    new Rectangle(0, cellHeight * 2, cellWidth, cellHeight), // dest
                                    new Rectangle(colWidth * col + cellWidth, row * rowHeight + cellHeight * 3, cellWidth, cellHeight), // source
                                    GraphicsUnit.Pixel);
                                // draw src second-to-bottom to next
                                g.DrawImage(
                                    bmp,
                                    new Rectangle(0, cellHeight * 3, cellWidth, cellHeight), // dest
                                    new Rectangle(colWidth * col + cellWidth, row * rowHeight + cellHeight * 2, cellWidth, cellHeight), // source
                                    GraphicsUnit.Pixel);
                                g.Flush();
                            }

                            // detect any image
                            try
                            {
                                using (var fb = new FastBitmap(obmp, System.Drawing.Imaging.ImageLockMode.ReadOnly))
                                {
                                    bool skip = true;
                                    var tlPix = fb.Rows[0].ElementAt(0);
                                    foreach (var r in fb.Rows)
                                    {
                                        foreach (var c in r)
                                        {
                                            if (!c.Equals(tlPix))
                                            {
                                                skip = false;
                                                break;
                                            }
                                        }
                                        if (!skip)
                                        {
                                            break;
                                        }
                                    }
                                    if (skip)
                                    {
                                        continue;
                                    }
                                }
                            }
                            catch { }

                            var ofname = ofile + "_" + (row * 4 + col + 1) + "_stand" + ext;
                            obmp.Save(ofname, System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                }

            }
        }
    }
}
