﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.GameData.Handlers
{
    public partial class AutoCompositePngBuilder
    {
        private string? HandleLamia(string? dir, string fname, StringReader reader)
        {
            // second line - has info for width and height of final image
            var secondLine = reader.ReadLine();
            if (secondLine == null)
            {
                throw new Exception("Invalid second line for Lamia composite image");
            }
            var vals = secondLine.Split('\t', StringSplitOptions.TrimEntries);
            int imgWidth = int.Parse(vals[4]);
            int imgHeight = int.Parse(vals[5]);

            var imgLayers = new List<LamiaCompositeImageLayer>();
            while (reader.ReadLine() is string line)
            {
                var spl = line.Split('\t', StringSplitOptions.TrimEntries);
                var entry = new LamiaCompositeImageLayer()
                {
                    LayerType = int.Parse(spl[0]),
                    Name = spl[1],
                    Left = int.Parse(spl[2]),
                    Top = int.Parse(spl[3]),
                    Width = int.Parse(spl[4]),
                    Height = int.Parse(spl[5]),
                    LayerId = spl[9]
                };
                if (entry.LayerType == 0)
                    imgLayers.Add(entry);
            }

            var outputDir = Path.Combine((dir ?? ""), "output");
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }
            foreach (var entry in imgLayers)
            {
                var srcImage = Path.Combine((dir ?? ""), fname + "_" + entry.LayerId) + ".png";
                var outputFile = fname + "_" + entry.Name + ".png";
                if (File.Exists(srcImage))
                {
                    try
                    {
                        using (var bmp = new Bitmap(imgWidth, imgHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                        using (var g = Graphics.FromImage(bmp))
                        {
                            using (var im = File.OpenRead(srcImage))
                            {
                                using (var img = Bitmap.FromStream(im))
                                    g.DrawImage(img, entry.Left, entry.Top, entry.Width, entry.Height);

                                bmp.Save(Path.Combine(outputDir, outputFile), System.Drawing.Imaging.ImageFormat.Png);
                            }
                        }
                    }
                    catch { }
                }
            }

            return outputDir;
        }

        class LamiaCompositeImageLayer
        {
            public int LayerType { get; set; }

            public string Name { get; set; } = "";

            public int Left { get; set; }

            public int Top { get; set; }

            public int Width { get; set; }

            public int Height { get; set; }

            public string LayerId { get; set; } = "";
        }
    }
}
