﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;

namespace Aura.CodeAtelier.GameData.Handlers
{
    public class LSFImageHandler : IGameDataHandler
    {
        public string Name { get; } = "LSF Image Builder";

        public void Begin(IEnumerable<string> files)
        {
        }

        public bool CanExecuteAction(string? handlerAction, IEnumerable<string> files)
        {
            return handlerAction == "lsf-build" && files.All(x => Path.GetExtension(x)?.ToLower() == ".lsf");
        }

        public IEnumerable<GameDataHandlerInfo>? CheckFileDrop(FileDropEventBody body)
        {
            if (body.Files.All(x => Path.GetExtension(x)?.ToLower() == ".lsf"))
            {
                var b = new GameDataHandlerInfo()
                {
                    Name = Name,
                    Icon = Util.CreateResourceUri("res/img/file-picture.png"),
                    Action = "lsf-build"
                };
                return new List<GameDataHandlerInfo>() { b };
            }
            return null;
        }

        public void End()
        {
        }

        public GameDataHandlerResult? ExecuteAction(string handlerAction, string file, IEnumerable<string> files)
        {
            if (handlerAction != "lsf-build") return null;

            var fname = Path.GetFileName(file) ?? "";
            var fullPath = Path.GetFullPath(file);
            var dir = Path.GetDirectoryName(fullPath);
            if (dir != null)
            {
                try
                {
                    var odir = Path.Combine(dir, Path.GetFileNameWithoutExtension(file));
                    if (!Directory.Exists(odir))
                        Directory.CreateDirectory(odir);
                    LSFImageBuilder.BuildLSFImage(fullPath, odir);
                    return GameDataHandlerResult.Create($"Build completed '{fname}'", odir);
                }
                catch (Exception ex)
                {
                    return GameDataHandlerResult.Create($"Failed to build '{fname}'\r\n{ex.Message}");
                }
            }
            return null;
        }
    }

    public static class LSFImageBuilder
    {
        static readonly byte[] MagicBytes = new byte[] { 0x4c, 0x53, 0x46 };
        public static void BuildLSFImage(string lsfFile, string outputDir)
        {
            var lsfDir = Path.GetDirectoryName(lsfFile) ?? "";
            var dic = new List<LSFImageStruct>();
            using (var fi = File.OpenRead(lsfFile))
            {
                var mb = new byte[3];
                fi.Read(mb);
                if (!Util.ArrayEqual(mb, MagicBytes)) { throw new Exception("Not a valid LSF file"); }

                var skip = new byte[4];
                fi.Read(skip);

                var iCount = new byte[4];
                fi.Read(iCount);
                iCount = Util.PrepByteArray(iCount, false);
                var entryCount = BitConverter.ToInt32(iCount, 0);

                for (var i = 0; i < entryCount; i++)
                {
                    skip = new byte[17];
                    fi.Read(skip);

                    var bufFileName = new byte[128];
                    fi.Read(bufFileName);
                    var fname = Encoding.ASCII.GetString(bufFileName).Trim('\0');

                    var bufInt = new byte[4];
                    fi.Read(bufInt);
                    bufInt = Util.PrepByteArray(bufInt, true);
                    var x = BitConverter.ToInt32(bufInt, 0);

                    bufInt = new byte[4];
                    fi.Read(bufInt);
                    bufInt = Util.PrepByteArray(bufInt, true);
                    var y = BitConverter.ToInt16(bufInt, 0);

                    var imFile = Path.Combine(lsfDir, fname + ".png");
                    using (var im = File.OpenRead(imFile))
                    {
                        var bmp = Bitmap.FromStream(im);
                        dic.Add(new LSFImageStruct()
                        {
                            Image = bmp,
                            FileName = fname,
                            X = x,
                            Y = y,
                        });
                    }

                    skip = new byte[11];
                    fi.Read(skip);
                }
            }
            int maxW = 0;
            int maxH = 0;
            foreach(var entry in dic)
            {
                if (entry.Image != null)
                {
                    maxW = Math.Max(entry.Image.Width + entry.X, maxW);
                    maxH = Math.Max(entry.Image.Height + entry.Y, maxH);
                }
            }

            foreach(var entry in dic)
            {
                if (entry.Image != null)
                {
                    using (var bmp = new Bitmap(maxW, maxH, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                    using (var g = Graphics.FromImage(bmp))
                    {
                        g.DrawImage(entry.Image, entry.X, entry.Y);

                        bmp.Save(Path.Combine(outputDir, entry.FileName + ".png"));
                    }
                }
            }
        }
    }

    public class LSFImageStruct
    {
        public string FileName { get; set; } = string.Empty;

        public Image? Image { get; set; } = null;

        public int X { get; set; }
        public int Y { get; set; }
    }
}
