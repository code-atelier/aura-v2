﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Directory = System.IO.Directory;
using MetadataExtractor;
using MetadataExtractor.Formats.Png;
using System;
using System.Collections.Generic;
using System.IO;

namespace Aura.CodeAtelier.GameData.Handlers
{
    public partial class AutoCompositePngBuilder
    {
        List<CompositeImageInfo> PureImages { get; } = new List<CompositeImageInfo>();
        Rectangle PureCanvasRect { get; set; }

        protected void PureGetImagesInfo(IEnumerable<string> files)
        {
            var right = int.MinValue;
            var bottom = int.MinValue;
            var left = int.MaxValue;
            var top = int.MaxValue;
            PureImages.Clear();
            foreach (var file in files)
            {
                var res = PureGetImageInfo(file);
                if (res != null)
                {
                    PureImages.Add(res);
                    right = Math.Max(right, res.CanvasRect.Right);
                    bottom = Math.Max(bottom, res.CanvasRect.Bottom);
                    left = Math.Min(left, res.CanvasRect.Left);
                    top = Math.Min(top, res.CanvasRect.Top);
                }
            }

            var x = 0;
            var y = 0;
            var width = right - left;
            var height = bottom - top;
            PureCanvasRect = new Rectangle(x, y, width, height);

            foreach(var pi in PureImages)
            {
                var adjustedX = -left;
                var adjustedY = -top;
                pi.DestinationRect = new Rectangle(pi.ImageRect.Left + adjustedX, pi.ImageRect.Top + adjustedY, pi.ImageRect.Width, pi.ImageRect.Height);
            }
        }

        protected CompositeImageInfo? PureGetImageInfo(string file)
        {
            var metadata = ImageMetadataReader.ReadMetadata(file);
            int imgWidth = -1, imgHeight = -1;

            try
            {
                if (metadata.FirstOrDefault(x => x.Name == "PNG-IHDR") is PngDirectory dHeader)
                {
                    imgWidth = int.Parse(dHeader.Tags.First(x => x.Type == PngDirectory.TagImageWidth).Description ?? "0");
                    imgHeight = int.Parse(dHeader.Tags.First(x => x.Type == PngDirectory.TagImageHeight).Description ?? "0");
                }
            }
            catch
            {
                try
                {
                    // else
                    using (var img = Bitmap.FromFile(file))
                    {
                        imgWidth = img.Width;
                        imgHeight = img.Height;
                    }
                }
                catch
                {
                }
            }
            if (imgWidth < 0 || imgHeight < 0)
            {
                return null;
            }

            // check PNG-oFFs
            if (metadata.FirstOrDefault(x => x.Name == "PNG-oFFs") is PngDirectory dOffset)
            {
                try
                {
                    var xOffset = int.Parse(dOffset.Tags.First(x => x.Type == PngDirectory.TagXOffset).Description ?? "0");
                    var yOffset = int.Parse(dOffset.Tags.First(x => x.Type == PngDirectory.TagYOffset).Description ?? "0");
                    var imgRect = new Rectangle(xOffset, yOffset, imgWidth, imgHeight);
                    var canvRect = new Rectangle(Math.Min(0, xOffset), Math.Min(0, yOffset), xOffset + imgWidth, yOffset + imgHeight);
                    return new CompositeImageInfo(canvRect, imgRect)
                    {
                        ImagePath = file
                    };
                }
                catch
                {
                }
            }
            else
            {
                var xOffset = 0;
                var yOffset = 0;
                var imgRect = new Rectangle(xOffset, yOffset, xOffset + imgWidth, yOffset + imgHeight);
                var canvRect = new Rectangle(Math.Min(0, xOffset), Math.Min(0, yOffset), xOffset + imgWidth, yOffset + imgHeight);
                return new CompositeImageInfo(canvRect, imgRect)
                {
                    ImagePath = file
                };
            }

            return null;
        }

        public string? PureBuildImage(CompositeImageInfo compositeImageInfo)
        {
            var inputDir = Path.GetDirectoryName(compositeImageInfo.ImagePath);
            var outputDir = Path.Combine(inputDir ?? "", "output");
            var outputFile = Path.Combine(outputDir, Path.GetFileName(compositeImageInfo.ImagePath));
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }
            var drect = compositeImageInfo.DestinationRect;

            using (var bmp = new Bitmap(PureCanvasRect.Width, PureCanvasRect.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
            using (var g = Graphics.FromImage(bmp))
            {
                using (var im = File.OpenRead(compositeImageInfo.ImagePath))
                {
                    using (var img = Bitmap.FromStream(im))
                        g.DrawImage(img, drect.Left, drect.Top, drect.Width, drect.Height);

                    bmp.Save(Path.Combine(outputDir, outputFile), System.Drawing.Imaging.ImageFormat.Png);
                }
            }

            return outputFile;
        }
    }

    public class CompositeImageInfo
    {
        public Rectangle CanvasRect { get; }

        public Rectangle ImageRect { get; }

        public Rectangle DestinationRect { get; set; }

        public string ImagePath { get; set; } = string.Empty;

        public CompositeImageInfo(Rectangle canvasRect, Rectangle imageRect)
        {
            CanvasRect = canvasRect;
            ImageRect = imageRect;
        }
    }
}
