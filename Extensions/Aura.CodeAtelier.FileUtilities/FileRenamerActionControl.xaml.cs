﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UserControl = System.Windows.Controls.UserControl;

namespace Aura.CodeAtelier
{
    /// <summary>
    /// Interaction logic for FileRenamerActionControl.xaml
    /// </summary>
    public partial class FileRenamerActionControl : UserControl
    {
        public FileRenamerActionControl()
        {
            InitializeComponent();
        }
    }
}
