﻿using Aura.Models;
using Aura.Services;
using Message = Aura.Services.Message;

namespace Aura.CodeAtelier
{
    public partial class ImageUtilityService : PassiveServiceBase, IMessageReceiver
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo
        {
            Author = "CodeAtelier",
            Description = "Allow manipulation of images by drag and dropping image files.",
            Id = "coatl.file.imageutilities",
            Name = "Image Utility Service",
            Version = new Version(1, 0, 0),
            Icon = Util.CreateResourceUri("res/img/file-picture.png"),
        };

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.Start | ServiceUserAction.Stop;

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public string Id { get => Info.Id; }

        private IServiceMessageBus MessageBus { get; set; }
        public bool CanReceiveMessage { get => State == ServiceState.Running; }

        public ImageUtilityService(IServiceMessageBus messageBus)
        {
            MessageBus = messageBus;
        }

        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                MessageBus.Subscribe(this, InternalEvents.FileDrop);
                MessageBus.Subscribe(this, InternalEvents.ExecuteFileDrop);
            });
        }

        public async Task<Message?> ReceiveMessageAsync(Message message, CancellationToken cancellationToken)
        {
            if (message.EventId == InternalEvents.FileDrop && message.GetBody<FileDropEventBody>() is FileDropEventBody body)
            {
                var actions = GetActionsFor(body);
                if (actions.Count == 0) return null;
                return message.CreateReply(Id, actions);
            }
            if (message.EventId == InternalEvents.ExecuteFileDrop && message.GetBody<FileDropExecuteEventBody>() is FileDropExecuteEventBody executeBody)
            {
                return await HandleAction(message, executeBody);
            }
            return null;
        }

        private List<FileDropHandler> GetActionsFor(FileDropEventBody body)
        {
            var res = new List<FileDropHandler>();
            res.AddRange(GetConverterActionsFor(body));
            res.AddRange(GetCropperActionsFor(body));
            res.AddRange(GetDiffActionsFor(body));
            return res;
        }

        private async Task<Message?> HandleAction(Message message, FileDropExecuteEventBody body)
        {
            return await HandleConverterAction(message, body)
                ?? await HandleCropperAction(message, body)
                ?? await HandleDiffAction(message, body);
        }
    }
}
