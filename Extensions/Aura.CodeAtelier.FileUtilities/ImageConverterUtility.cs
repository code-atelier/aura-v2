﻿using Aura.Models;
using Aura.Services;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using Message = Aura.Services.Message;

namespace Aura.CodeAtelier
{
    public partial class ImageUtilityService
    {
        List<string[]> ConverterImageExtensions = new List<string[]>()
        {
            new [] { "ico" },
            new [] { "png" },
            new [] { "bmp" },
            new [] { "gif" },
            new [] { "jpg", "jpeg", "jfif" },
            new [] { "tiff" },
            new [] { "exif" },
            new [] { "wmf" },
            new [] { "emf" },
        };

        Dictionary<string, ImageFormat> ImageFormats = new Dictionary<string, ImageFormat>()
        {
            { "ico", ImageFormat.Icon },
            { "png", ImageFormat.Png },
            { "bmp", ImageFormat.Bmp },
            { "gif", ImageFormat.Gif },
            { "jpg", ImageFormat.Jpeg },
            { "jpeg", ImageFormat.Jpeg },
            { "tiff", ImageFormat.Tiff },
            { "exif", ImageFormat.Exif },
            { "wmf", ImageFormat.Wmf },
            { "emf", ImageFormat.Emf },
        };

        private List<FileDropHandler> GetConverterActionsFor(FileDropEventBody body)
        {
            var res = new List<FileDropHandler>();
            var exts = body.Files.Select(x => Path.GetExtension(x)?.ToLower().TrimStart('.') ?? "").Distinct().ToList();
            if (!exts.All(x => ConverterImageExtensions.Any(ex => ex.Contains(x))))
                return res;
            var availableConvertFormats = ConverterImageExtensions.Where(x => !x.Any(w => exts.Contains(w))).Select(x => x.First());
            foreach (var format in availableConvertFormats)
            {
                res.Add(new FileDropHandler()
                {
                    Id = Info.Id,
                    Name = "Convert to " + format.ToUpper(),
                    Icon = Util.CreateResourceUri("res/img/file-picture-" + format.ToLower() + ".png"),
                    Action = "image-convert:" + format,
                });
            }
            return res;
        }

        private async Task<Message?> HandleConverterAction(Message message, FileDropExecuteEventBody body)
        {
            if (body.Handler.Action?.StartsWith("image-convert:") != true) return null;
            var convertTo = body.Handler.Action.Substring(14);
            if (body.Files.Count > 0 && ImageFormats.ContainsKey(convertTo))
            {
                List<string> success = new List<string>();
                List<string> failed = new List<string>();
                foreach (var file in body.Files)
                {
                    var dname = Path.Combine(Path.GetDirectoryName(file) ?? "", Path.GetFileNameWithoutExtension(file) + "." + convertTo);
                    try
                    {
                        using (var ms = new MemoryStream())
                        {
                            using (var fi = File.OpenRead(file))
                            {
                                fi.CopyTo(ms);
                            }
                            ms.Seek(0, SeekOrigin.Begin);
                            var bmp = Bitmap.FromStream(ms);
                            bmp.Save(dname, ImageFormats[convertTo]);
                        }
                        success.Add(dname);
                    }
                    catch
                    {
                        failed.Add(file);
                    }
                }
                if (success.Count + failed.Count > 0)
                {
                    // make toast notif
                    var toast = new ToastNotification();
                    toast.Title = "Image Coversion Completed";
                    toast.Contents.Add(new ToastNotificationContent()
                    {
                        Kind = ToastNotificationContentKind.Text,
                        Text = $"{success.Count} file{(success.Count > 0 ? "s" : "")} successfully converted."
                    });
                    if (failed.Count > 0)
                    {
                        toast.Contents.Add(new ToastNotificationContent()
                        {
                            Kind = ToastNotificationContentKind.Text,
                            Text = $"Failed to convert {failed.Count} file{(failed.Count > 0 ? "s" : "")}."
                        });
                    }
                    if (success.Count > 0)
                    {
                        try
                        {
                            if (success.Count > 1)
                            {
                                Bitmap bmp = new Bitmap(400, 100, PixelFormat.Format32bppArgb);
                                using (var g = Graphics.FromImage(bmp))
                                {
                                    double spacing = 300d / Math.Min(10, success.Count - 1);
                                    for (var i = 0; i < success.Count && i < 11; i++)
                                    {
                                        using (var fi = File.OpenRead(success[i]))
                                        {
                                            var pic = Bitmap.FromStream(fi);
                                            var srcRect = new Rect(0, 0, pic.Width, pic.Height);
                                            var dRect = Util.ScaledDestinationRectangle(srcRect, new Rect(spacing * i, 0, 100, 100));
                                            g.DrawImage(pic, (int)dRect.X, (int)dRect.Y, (int)dRect.Width, (int)dRect.Height);
                                        }
                                    }
                                }
                                var tempdir = Path.Combine(Path.GetTempPath(), "Aura", "notification");
                                if (!Directory.Exists(tempdir))
                                {
                                    Directory.CreateDirectory(tempdir);
                                }
                                var file = Path.Combine(tempdir, Guid.NewGuid().ToString("n") + ".png");
                                bmp.Save(file, ImageFormat.Png);
                                toast.Contents.Add(new ToastNotificationContent()
                                {
                                    Kind = ToastNotificationContentKind.InlineImage,
                                    ImageUri = new Uri(file),
                                });
                            }
                            else
                            {
                                toast.Contents.Add(new ToastNotificationContent()
                                {
                                    Kind = ToastNotificationContentKind.InlineImage,
                                    ImageUri = new Uri(success.First()),
                                });
                            }
                        }
                        catch { }
                        toast.Arguments = new Dictionary<string, string>()
                            {
                                { "event", InternalEvents.ToastOpenFileRequest },
                                { "body", success.First() }
                            };
                        toast.Contents.Add(new ToastNotificationContent()
                        {
                            Text = "Show in Explorer",
                            Kind = ToastNotificationContentKind.Button,
                            Arguments = new Dictionary<string, string> {
                                    { "event", InternalEvents.ToastShowInExplorerRequest },
                                    { "body", success.First() }
                                }
                        });
                        toast.Contents.Add(new ToastNotificationContent()
                        {
                            Text = "Dismiss",
                            Kind = ToastNotificationContentKind.Button,
                            ButtonActivation = ToastNotificationButtonActivation.Dismiss,
                        });
                    }
                    var tNotif = new Message<ToastNotification>(Id, InternalEvents.ShowToastNotificationRequest, toast);
                    await MessageBus.SendMessageAsync(tNotif);
                }
            }
            return message.CreateReply(Id);
        }
    }
}
