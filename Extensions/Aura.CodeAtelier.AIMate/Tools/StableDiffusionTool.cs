﻿using Aura.CodeAtelier.AIMate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.CodeAtelier.Tools
{
    public class StableDiffusionTool : ToolBase
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Stable Diffusion",
            Description = "Automatic111's Stable Diffusion WebUI tool",
            Author = "Code Atelier",
            Id = "coatl.aimate.automatic111.stablediffusion",
            Version = new Version(1, 0, 0),
        };

        StableDiffusionService Service { get; }

        public StableDiffusionTool(StableDiffusionService service)
        {
            Service = service;
        }

        public override string Icon { get; } = Util.CreateResourceUri("res/icon.png");

        protected override Window CreateToolWindow()
        {
            return new StableDiffusionWindow(Service);
        }
    }
}
