﻿using Aura.CodeAtelier.AIMate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura.CodeAtelier.Tools
{
    /// <summary>
    /// Interaction logic for StableDiffusionWindow.xaml
    /// </summary>
    public partial class StableDiffusionWindow : Window
    {
        StableDiffusionService Service { get; }

        DispatcherTimer TimerProgress { get; }

        public StableDiffusionWindow(StableDiffusionService service)
        {
            Service = service;
            InitializeComponent();

            TimerProgress = new DispatcherTimer(DispatcherPriority.Render);
            TimerProgress.Interval = new TimeSpan(0, 0, 1);
            TimerProgress.Tick += TimerProgress_Tick;
            TimerProgress.Start();
        }

        string? previousImageData = null;
        volatile bool IsRunning = false;
        private async void TimerProgress_Tick(object? sender, EventArgs e)
        {
            try
            {
                var progress = await Service.GetProgressAsync();
                Dispatcher.Invoke(() =>
                {
                    sbiServerState.Content = Service.State.ToString();
                    sbiServerState.Foreground = new SolidColorBrush(
                        Service.State == Services.ServiceState.Running ? Colors.LimeGreen :
                        Service.State == Services.ServiceState.Error ? Colors.Crimson :
                        Service.State == Services.ServiceState.Stopped ? Colors.DarkGray :
                        Colors.DarkOrange
                    );
                });

                Dispatcher.Invoke(() =>
                {
                    if (progress != null && !string.IsNullOrWhiteSpace(progress.State.Job))
                    {
                        if (progress.CurrentImage != null && progress.CurrentImage != previousImageData)
                        {
                            var imgData = progress.GetImageData();
                            if (imgData != null)
                            {
                                using (var ms = new MemoryStream(imgData))
                                {
                                    var bmp = new BitmapImage();
                                    bmp.BeginInit();
                                    bmp.StreamSource = ms;
                                    bmp.CacheOption = BitmapCacheOption.OnLoad;
                                    bmp.EndInit();
                                    imgProgressPreview.Source = bmp;
                                    previousImageData = progress.CurrentImage;
                                }
                            }
                        }
                        var dt = DateTime.Today.AddSeconds(progress.EtaRelative);
                        var delta = dt - DateTime.Today;
                        spProgress.Inlines.Clear();
                        spProgress.Inlines.Add($"{Math.Round(progress.Progress * 100, 2)}% ({Math.Floor(delta.TotalHours).ToString().PadLeft(2, '0')}:{delta.Minutes.ToString().PadLeft(2, '0')}:{delta.Seconds.ToString().PadLeft(2, '0')}.{Math.Round(delta.Milliseconds / 10d)})");
                        pbProgress.Value = Math.Max(0, Math.Min(100, progress.Progress * 100));
                        spJob.Inlines.Clear();
                        spJob.Inlines.Add($"{progress.State.JobNo + 1} of {progress.State.JobCount} (Sampling {progress.State.SamplingStep + 1} of {progress.State.SamplingSteps})");

                        var jobPerc = 1 / Math.Max(1, (double)progress.State.JobCount);
                        var samplingPerc = (progress.State.SamplingStep + 1) / Math.Max(1, (double)progress.State.SamplingSteps);
                        var fullJobPerc = progress.State.JobNo * jobPerc + samplingPerc * jobPerc;
                        pbJob.Value = Math.Max(0, Math.Min(100, fullJobPerc * 100));
                        IsRunning = true;
                        Dispatcher.Invoke(() =>
                        {
                            sbiAPIState.Content = "Online";
                            sbiAPIState.Foreground = new SolidColorBrush(Colors.Green);
                        });
                    }
                    else
                    {
                        previousImageData = null;
                        imgProgressPreview.Source = null;
                        spProgress.Inlines.Clear();
                        pbProgress.Value = 0;
                        spJob.Inlines.Clear();
                        pbJob.Value = 0;
                        IsRunning = false;
                        Dispatcher.Invoke(() =>
                        {
                            sbiAPIState.Content = "Offline";
                            sbiAPIState.Foreground = new SolidColorBrush(Colors.DarkGray);
                        });
                    }
                });
            }
            catch
            {
                Dispatcher.Invoke(() =>
                {
                    sbiAPIState.Content = "Error";
                    sbiAPIState.Foreground = new SolidColorBrush(Colors.Crimson);
                });
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            TimerProgress.Stop();
        }

        private void miPromptCompendium_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
