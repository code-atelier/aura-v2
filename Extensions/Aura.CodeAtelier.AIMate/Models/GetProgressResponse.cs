﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class GetProgressResponse
    {
        [JsonPropertyName("progress")]
        public double Progress { get; set; }

        [JsonPropertyName("eta_relative")]
        public double EtaRelative { get; set; }

        [JsonPropertyName("state")]
        public GetProgressResponseState State { get; set; } = new GetProgressResponseState();

        [JsonPropertyName("current_image")]
        public string? CurrentImage { get; set; }

        [JsonPropertyName("textinfo")]
        public string? TextInfo { get; set; }

        public byte[]? GetImageData()
        {
            try
            {
                if (CurrentImage != null)
                {
                    return Convert.FromBase64String(CurrentImage);
                }
            }
            catch { }
            return null;
        }
    }

    public class GetProgressResponseState
    {
        [JsonPropertyName("skipped")]
        public bool Skipped { get; set; }

        [JsonPropertyName("interrupted")]
        public bool Interrupted { get; set; }

        [JsonPropertyName("job")]
        public string Job { get; set; } = string.Empty;

        [JsonPropertyName("job_count")]
        public int JobCount { get; set; }

        [JsonPropertyName("job_timestamp")]
        public string JobTimestamp { get; set; } = string.Empty;

        [JsonPropertyName("job_no")]
        public int JobNo { get; set; }

        [JsonPropertyName("sampling_step")]
        public int SamplingStep { get; set; }

        [JsonPropertyName("sampling_steps")]
        public int SamplingSteps { get; set; }
    }
}
