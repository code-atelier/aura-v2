﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class GetLoraResponse: List<Lora>
    {
    }

    public class Lora
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("alias")]
        public string Alias { get; set; } = string.Empty;

        [JsonPropertyName("path")]
        public string Path { get; set; } = string.Empty;

        [JsonPropertyName("metadata")]
        public LoraMetadata Metadata { get; set; } = new LoraMetadata();
    }

    public class LoraMetadata
    {
        [JsonPropertyName("ss_sd_model_name")]
        public string SDModelName { get; set; } = string.Empty;

        [JsonPropertyName("ss_resolution")]
        public string Resolution { get; set; } = string.Empty;

        [JsonPropertyName("ss_clip_skip")]
        public string ClipSkip { get; set; } = string.Empty;

        [JsonPropertyName("ss_num_train_images")]
        public string TrainImagesCount { get; set; } = string.Empty;

        [JsonPropertyName("ss_batch_size_per_device")]
        public string BatchSizePerDevice { get; set; } = string.Empty;

        [JsonPropertyName("ss_tag_frequency")]
        public Dictionary<string, LoraTagFrequency> TagFrequency { get; set; } = new Dictionary<string, LoraTagFrequency>();
    }

    public class LoraTagFrequency: Dictionary<string, int>
    {

    }
}
