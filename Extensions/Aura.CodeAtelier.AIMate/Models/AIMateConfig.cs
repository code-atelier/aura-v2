﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    [AuraConfiguration("stablediffusion.json")]
    [AutoConfig("Stable Diffusion")]
    public class AIMateConfig
    {
        [JsonPropertyName("stableDiffuWebuiPath")]
        [AutoConfigMember("Startup file path",
            Description = "Stable diffusion webui startup batch file path (webui-user.bat)",
            Index = 1,
            Kind = AutoConfigPropertyKind.Textbox,
            Icon = "res/terminal.png"
            )]
        public string? StableDiffusionWebUIBatchFilePath { get; set; }

        [JsonPropertyName("stableDiffuWebApi")]
        [AutoConfigMember("Web api uri",
            Description = "Base uri of the stable diffusion webui",
            Index = 0,
            Kind = AutoConfigPropertyKind.Textbox,
            Icon = "res/globe.png"
            )]
        public string StableDiffusionWebAPI { get; set; } = "http://127.0.0.1:7860/";
    }
}
