﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class TxtToImageRequest
    {
        [JsonPropertyName("prompt")]
        public string PositivePrompt { get; set; } = string.Empty;

        [JsonPropertyName("negative_prompt")]
        public string NegativePrompt { get; set; } = string.Empty;

        [JsonPropertyName("styles")]
        public List<string> Styles { get; set; } = new List<string>();

        [JsonPropertyName("seed")]
        public int Seed { get; set; } = -1;

        [JsonPropertyName("subseed")]
        public int Subseed { get; set; } = -1;

        [JsonPropertyName("subseed_strength")]
        public double SubseedStrength { get; set; } = 0;

        [JsonPropertyName("seed_resize_from_h")]
        public int SeedResizeFromH { get; set; } = -1;

        [JsonPropertyName("seed_resize_from_w")]
        public int SeedResizeFromW { get; set; } = -1;

        [JsonPropertyName("sampler_name")]
        public string SamplerName { get; set; } = string.Empty;

        [JsonPropertyName("batch_size")]
        public int BatchSize { get; set; } = 1;

        [JsonPropertyName("n_iter")]
        public int BatchCount { get; set; } = 1;

        [JsonPropertyName("steps")]
        public int Steps { get; set; } = 20;

        [JsonPropertyName("cfg_scale")]
        public double CfgScale { get; set; } = 7;

        [JsonPropertyName("width")]
        public int Width { get; set; } = 512;

        [JsonPropertyName("height")]
        public int Height { get; set; } = 512;

        [JsonPropertyName("restore_faces")]
        public bool RestoreFaces { get; set; } = true;

        [JsonPropertyName("tiling")]
        public bool Tiling { get; set; } = true;

        [JsonPropertyName("do_not_save_samples")]
        public bool DoNotSaveSamples { get; set; } = false;

        [JsonPropertyName("do_not_save_grid")]
        public bool DoNotSaveGrid { get; set; } = false;

        [JsonPropertyName("eta")]
        public double ETA { get; set; } = 0;

        [JsonPropertyName("denoising_strength")]
        public double DenoisingStrength { get; set; } = 0.7;


        /*
         * 
  "s_min_uncond": 0,
  "s_churn": 0,
  "s_tmax": 0,
  "s_tmin": 0,
  "s_noise": 0,
  "override_settings": {},
  "override_settings_restore_afterwards": true,
  "refiner_checkpoint": "string",
  "refiner_switch_at": 0,
  "disable_extra_networks": false,
  "comments": {},
  "enable_hr": false,
  "firstphase_width": 0,
  "firstphase_height": 0,
  "hr_scale": 2,
  "hr_upscaler": "string",
  "hr_second_pass_steps": 0,
  "hr_resize_x": 0,
  "hr_resize_y": 0,
  "hr_checkpoint_name": "string",
  "hr_sampler_name": "string",
  "hr_prompt": "",
  "hr_negative_prompt": "",
  "sampler_index": "Euler",
  "script_name": "string",
  "script_args": [],
  "send_images": true,
  "save_images": false,
  "alwayson_scripts": {}
        */
    }
}
