﻿using Aura.CodeAtelier.Models;
using Aura.Services;
using Aura.Services.Passive;
using Aura.WPF;
using RestSharp;
using System.Configuration;
using System.Text.Json;
using System.Windows;

namespace Aura.CodeAtelier.AIMate
{
    public class StableDiffusionService : PassiveServiceBase, IServiceWithConfiguration
    {
        public const string AIMateDataStoragePath = "data\\ai.mate\\";
        public const string PromptStorage = "prompts";

        public AIMateConfig Configuration { get; }

        public ConfigurationService ConfigurationService { get; }

        public List<string> Tags { get; } = new List<string>();

        public IReadOnlyList<Lora> Loras { get; private set; } = new List<Lora>();

        public IReadOnlyList<StableDiffusionModel> Models { get; private set; } = new List<StableDiffusionModel>();

        public StableDiffusionService(ConfigurationService configurationService)
        {
            ConfigurationService = configurationService;
            Configuration = ConfigurationService.LoadConfig<AIMateConfig>();
        }

        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = "coatl.aimate",
            Name = "Stable Diffusion",
            Author = "CodeAtelier",
            Description = "Automatic111's Stable Diffusion API Service.",
            Version = new Version(1, 0, 0),
            Icon = Util.CreateResourceUri("res/icon.png"),
        };

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.All;

        private Window? ConfigWindow { get; set; }

        public Window CreateConfigurationWindow()
        {
            var ac = new AutoConfigurator();
            ac.LoadConfiguration(Configuration);
            ac.Closed += Ac_Closed;
            return ac;
        }

        private void Ac_Closed(object? sender, EventArgs e)
        {
            try
            {
                if (sender is Window w && w == ConfigWindow)
                {
                    ConfigWindow = null;
                }
                ConfigurationService.SaveConfig<AIMateConfig>();
            }
            catch { }
        }

        public RestClient? Client { get; private set; }

        public override async Task OnStart()
        {
            await Task.Run(async () =>
            {
                if (string.IsNullOrWhiteSpace(Configuration.StableDiffusionWebAPI))
                {
                    throw new Exception("Configuration is invalid!");
                }
                Client = new RestClient(Configuration.StableDiffusionWebAPI);
                await RefreshResources();
            });
        }

        public async Task RefreshResources()
        {
            Tags.Clear();

            await ReloadStableDiffusionModels();
            await ReloadLoras();
        }

        public async Task ReloadLoras()
        {
            var loras = new List<Lora>();
            Loras = loras;
            if (Client == null) return;
            try
            {
                var res = await Client.ExecuteAsync(new RestRequest("sdapi/v1/refresh-loras", Method.Post));
                if (res.IsSuccessStatusCode)
                {
                    res = await Client.ExecuteAsync(new RestRequest("sdapi/v1/loras", Method.Get));
                    if (res.IsSuccessStatusCode && res.Content != null)
                    {
                        var resp = JsonSerializer.Deserialize<GetLoraResponse>(res.Content);
                        if (resp != null)
                        {
                            loras.AddRange(resp);
                            foreach (var r in resp)
                            {
                                if (r.Metadata?.TagFrequency != null)
                                {
                                    foreach (var tf in r.Metadata.TagFrequency)
                                    {
                                        foreach (var tg in tf.Value)
                                        {
                                            if (!Tags.Contains(tg.Key))
                                            {
                                                Tags.Add(tg.Key);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        public async Task ReloadStableDiffusionModels()
        {
            var coll = new List<StableDiffusionModel>();
            Models = coll;
            if (Client == null) return;
            try
            {
                var res = await Client.ExecuteAsync(new RestRequest("sdapi/v1/refresh-checkpoints", Method.Post));
                if (res.IsSuccessStatusCode)
                {
                    res = await Client.ExecuteAsync(new RestRequest("sdapi/v1/sd-models", Method.Get));
                    if (res.IsSuccessStatusCode && res.Content != null)
                    {
                        var resp = JsonSerializer.Deserialize<GetStableDiffusionModelResponse>(res.Content);
                        if (resp != null)
                        {
                            coll.AddRange(resp);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        public async Task<GetProgressResponse?> GetProgressAsync()
        {
            if (Client == null || State != ServiceState.Running) return null;
            try
            {
                var res = await Client.ExecuteAsync(new RestRequest("sdapi/v1/progress?skip_current_image=false"));
                if (res.IsSuccessStatusCode && res.Content != null)
                {
                    return JsonSerializer.Deserialize<GetProgressResponse>(res.Content);
                }
            }
            catch
            {
            }
            return null;
        }

        public async Task<GetProgressResponse?> Txt2Img()
        {
            if (Client == null || State != ServiceState.Running) return null;
            try
            {
                var res = await Client.ExecuteAsync(new RestRequest("queue/status"));
                if (res.IsSuccessStatusCode && res.Content != null)
                {
                    return JsonSerializer.Deserialize<GetProgressResponse>(res.Content);
                }
            }
            catch
            {
            }
            return null;
        }
    }
}