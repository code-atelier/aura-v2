﻿using ARSoft.Tools.Net.Dns;
using ARSoft.Tools.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Net;

namespace Aura.CodeAtelier
{

    public class DnsJson
    {
        [JsonPropertyName("Status")]
        public int Status { get; set; }

        [JsonPropertyName("TC")]
        public bool TC { get; set; }

        [JsonPropertyName("RD")]
        public bool RD { get; set; }

        [JsonPropertyName("RA")]
        public bool RA { get; set; }

        [JsonPropertyName("AD")]
        public bool AD { get; set; }

        [JsonPropertyName("CD")]
        public bool CD { get; set; }

        [JsonPropertyName("Question")]
        public List<DnsJsonQuestion> Question { get; set; } = new List<DnsJsonQuestion>();

        [JsonPropertyName("Answer")]
        public List<DnsJsonAnswer> Answer { get; set; } = new List<DnsJsonAnswer>();

        [JsonPropertyName("Additional")]
        public List<DnsJsonAnswer> Additional { get; set; } = new List<DnsJsonAnswer>();

        [JsonPropertyName("Authority")]
        public List<DnsJsonAnswer> Authority { get; set; } = new List<DnsJsonAnswer>();

        [JsonPropertyName("Comment")]
        public string? Comment { get; set; }

        [JsonPropertyName("edns_client_subnet")]
        public string? EDNSClientSubnet { get; set; }

        public DnsMessage ToMessage(int? overrideTTL = null)
        {
            DnsMessage message = new DnsMessage();
            message.ReturnCode = (ReturnCode)Status;
            message.IsTruncated = TC;
            message.IsRecursionDesired = RD;
            message.IsRecursionAllowed = RA;
            message.IsAuthenticData = AD;
            message.IsCheckingDisabled = CD;
            message.Questions = Question.Select(x => x.ToQuestion()).ToList();
            message.AnswerRecords = Answer.Select(x => x.ToRecord(overrideTTL)).OfType<DnsRecordBase>().ToList();
            message.AdditionalRecords = Additional.Select(x => x.ToRecord(overrideTTL)).OfType<DnsRecordBase>().ToList();
            message.AuthorityRecords = Authority.Select(x => x.ToRecord(overrideTTL)).OfType<DnsRecordBase>().ToList();
            return message;
        }
    }

    public class DnsJsonQuestion
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("type")]
        public int Type { get; set; }

        public static DnsJsonQuestion FromQuestion(DnsQuestion question)
        {
            DnsJsonQuestion q = new DnsJsonQuestion();
            q.Name = question.Name.ToString();
            q.Type = (int)question.RecordType;
            return q;
        }

        public DnsQuestion ToQuestion()
        {
            return new DnsQuestion(DomainName.Parse(Name), (RecordType)Type, RecordClass.INet);
        }
    }

    public class DnsJsonAnswer
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("type")]
        public int Type { get; set; }

        [JsonPropertyName("TTL")]
        public int TTL { get; set; }

        [JsonPropertyName("data")]
        public string Data { get; set; } = string.Empty;

        public DnsRecordBase? ToRecord(int? overrideTTL = null)
        {
            try
            {
                var rt = (RecordType)Type;
                var domainName = DomainName.Parse(Name);
                var ttl = overrideTTL ?? TTL;
                var data = ParseQuotedStringData();
                switch (rt)
                {
                    case RecordType.A:
                        return new ARecord(domainName, ttl, IPAddress.Parse(Data));
                    case RecordType.Aaaa:
                        return new AaaaRecord(domainName, ttl, IPAddress.Parse(Data));
                    case RecordType.Ns:
                        return new NsRecord(domainName, ttl, DomainName.Parse(Data));
                    case RecordType.CName:
                        return new CNameRecord(domainName, ttl, DomainName.Parse(Data));
                    case RecordType.Soa:
                        return new SoaRecord(domainName, ttl,
                            DomainName.Parse(data[0]),
                            DomainName.Parse(data[1]),
                            uint.Parse(data[2]),
                            int.Parse(data[3]),
                            int.Parse(data[4]),
                            int.Parse(data[5]),
                            int.Parse(data[6])
                            );
                    case RecordType.Mx:
                        return new MxRecord(domainName, ttl, ushort.Parse(data[0]), DomainName.Parse(data[1]));
                    case RecordType.Txt:
                        return new TxtRecord(domainName, ttl, Data);
                    case RecordType.CAA:
                        return new CAARecord(domainName, ttl, byte.Parse(data[0]), data[1], data[2]);
                }
            }
            catch
            {
            }
            return null;
        }

        private string[] ParseQuotedStringData()
        {
            List<string> res = new List<string>();
            string buffer = "";
            bool afterEscape = false;
            bool inQuote = false;
            bool afterQuote = false;
            for (var i = 0; i < Data.Length; i++)
            {
                var chr = Data[i];
                if (afterEscape)
                {
                    buffer += chr;
                    continue;
                }

                if (afterQuote && chr == ' ')
                {
                    afterQuote = false;
                    continue;
                }

                if (!inQuote && chr == ' ')
                {
                    res.Add(buffer);
                    buffer = "";
                    continue;
                }

                if (inQuote && chr == '"')
                {
                    res.Add(buffer);
                    buffer = "";
                    afterQuote = true;
                    inQuote = false;
                    continue;
                }

                if (chr == '"')
                {
                    buffer = "";
                    inQuote = true;
                    continue;
                }

                if (inQuote && chr == '\\')
                {
                    afterEscape = true;
                    continue;
                }

                buffer += chr;
            }
            if (buffer != "")
                res.Add(buffer);
            return res.ToArray();
        }
    }
}
