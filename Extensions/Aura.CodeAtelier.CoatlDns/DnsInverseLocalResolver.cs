﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using Aura.CodeAtelier.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.CodeAtelier
{
    public class DnsInverseLocalResolver : IDnsResolver
    {
        static readonly DomainName Inv4AddrTLD = DomainName.Parse("in-addr.arpa");
        static readonly DomainName Inv6AddrTLD = DomainName.Parse("ip6.arpa");
        static List<IPAddress> LoopbackBoundAddress { get; } = new List<IPAddress>();

        CoatlDnsConfiguration Configuration { get; }

        public string Name { get; } = "Inverse Local Resolver";

        public DnsInverseLocalResolver(IEnumerable<IPEndPoint> endPoints, CoatlDnsConfiguration configuration)
        {
            Configuration = configuration;
            LoopbackBoundAddress.Clear();
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                    {
                        if (endPoints.Any(x => x.Address.Equals(ip.Address)
                            || (x.Address.Equals(IPAddress.Any) && x.Address.AddressFamily == ip.Address.AddressFamily)
                            || (x.Address.Equals(IPAddress.IPv6Any) && x.Address.AddressFamily == ip.Address.AddressFamily)
                        ))
                        {
                            LoopbackBoundAddress.Add(ip.Address);
                        }
                    }
                }
            }
        }

        public async Task<DnsMessage?> Resolve(DnsQuestion question)
        {
            return await Task.Run(() => {
                var labels = question.Name.Labels.ToArray();
                Array.Reverse(labels);
                IPAddress? address = null;
                if (question.Name.IsSubDomainOf(Inv4AddrTLD))
                {
                    address = IPAddress.Parse(string.Join(".", labels[2..]));
                }
                else if (question.Name.IsSubDomainOf(Inv6AddrTLD))
                {
                    var ipv6parts = new List<string>();
                    for (var i = 2; i < labels.Length - 3; i += 4)
                    {
                        ipv6parts.Add(labels[i] + labels[i + 1] + labels[i + 2] + labels[i + 3]);
                    }
                    address = IPAddress.Parse(string.Join(":", ipv6parts));
                }
                if (address != null)
                {
                    if (question.RecordType == RecordType.Ptr)
                    {
                        return ResolvePtr(address, question.Name);
                    }
                }
                return null;
            });
        }

        private DnsMessage? ResolvePtr(IPAddress address, DomainName questionedDomain)
        {
            if (
                address.Equals(IPAddress.Loopback)
                || address.Equals(IPAddress.IPv6Loopback)
                || LoopbackBoundAddress.Any(x => x.Equals(address))
                )
            {
                var msg = new DnsMessage()
                {
                    ReturnCode = ReturnCode.NoError
                };
                msg.AnswerRecords.Add(new PtrRecord(questionedDomain, 200, 
                    DomainName.Parse(!string.IsNullOrWhiteSpace(Configuration.Listener.ServerName) ? Configuration.Listener.ServerName : Environment.MachineName)
                ));
                return msg;
            }
            return null;
        }
    }
}
