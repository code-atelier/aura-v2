﻿using ARSoft.Tools.Net.Dns;
using Aura.CodeAtelier.Configuration;
using Aura.Models;
using Aura.Services;
using Aura.Services.Passive;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.CodeAtelier
{
    public partial class CoatlDnsService : PassiveServiceBase, IServiceWithConfiguration, IMessageReceiver, IServiceWithLog
    {
        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = "coatl.dns",
            Name = "Coatl DNS",
            Author = "CodeAtelier",
            Description = "Resolves DNS requests from your local network.",
            Version = new Version(3, 2, 11),
            Icon = Util.CreateResourceUri("res/icon.png"),
        };

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.All;

        public CoatlDnsConfiguration Configuration { get; }

        public ConfigurationService ConfigurationService { get; }

        public IServiceMessageBus ServiceMessageBus { get; }

        public CoatlDnsService(ConfigurationService configurationService, IServiceMessageBus serviceMessageBus)
        {
            ConfigurationService = configurationService;
            Configuration = ConfigurationService.LoadConfig<CoatlDnsConfiguration>();
            Cache = new DnsResponseCache(Configuration);
            ServiceMessageBus = serviceMessageBus;
            Log = new ServiceLog(this, DateTime.Now.ToString("yyyyMMdd_HHmmss_DNS") + ".log");
        }

        private Window? ConfigWindow { get; set; }

        public Window CreateConfigurationWindow()
        {
            var ac = new AutoConfigurator();
            ac.LoadConfiguration(Configuration);
            ac.Closed += Ac_Closed;
            return ac;
        }

        private void Ac_Closed(object? sender, EventArgs e)
        {
            try
            {
                if (sender is Window w && w == ConfigWindow)
                {
                    ConfigWindow = null;
                }
                ConfigurationService.SaveConfig<CoatlDnsConfiguration>();
            }
            catch { }
        }

        public DnsServer? Server { get; private set; }

        public List<IDnsResolver> Resolvers { get; } = new List<IDnsResolver>();

        public DnsResponseCache Cache { get; }

        public MessageReceiverPriority Priority { get; }

        public string Id { get => Info.Id; }

        public bool CanReceiveMessage { get; } = true;

        public ServiceLog Log { get; }

        private void InitializeResolvers(IEnumerable<IPEndPoint> endPoints)
        {
            Resolvers.Clear();
            Resolvers.Add(Cache);
            Resolvers.Add(new DnsInverseLocalResolver(endPoints, Configuration));

            foreach (var dohResolverConfig in Configuration.Resolver.DnsOverHttpConfigs)
            {
                if (dohResolverConfig.Active)
                {
                    try
                    {
                        var dohResolver = new DnsOverHttpResolver(dohResolverConfig);
                        Resolvers.Add(dohResolver);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
            }
        }

        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                ServiceMessageBus.Subscribe(this, InternalEvents.UriRequest);
            });
        }

        public override async Task OnStart()
        {
            await Task.Run(() =>
            {
                var logPath = Path.Combine(AuraLogger.DefaultLogPath, "coatl.dns");
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }
                var files = Directory.EnumerateFiles(logPath, "*.log");
                foreach (var f in files)
                {
                    try
                    {
                        var fi = new FileInfo(f);
                        var age = DateTime.Now - fi.LastWriteTime;
                        if (age.TotalDays > 7)
                            File.Delete(f);
                    }
                    catch { }
                }
                Log.Begin();
                List<IPEndPoint> endPoints = new List<IPEndPoint>();
                if (Configuration.Listener.LocalMachineOnly)
                {
                    if (Configuration.Listener.UseIPv6)
                    {
                        endPoints.Add(IPEndPoint.Parse("[::1]:53"));
                    }
                    if (Configuration.Listener.UseIPv4)
                    {
                        endPoints.Add(IPEndPoint.Parse("127.0.0.1:53"));
                    }
                }
                else
                {
                    var allEP = Configuration.Listener.EndPoints.Where(x => IPEndPoint.TryParse(x, out _)).Select(x => IPEndPoint.Parse(x));
                    if (Configuration.Listener.UseIPv6)
                    {
                        endPoints.AddRange(allEP.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6));
                    }
                    if (Configuration.Listener.UseIPv4)
                    {
                        endPoints.AddRange(allEP.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork));
                    }
                }

                List<IServerTransport> serverTransports = new List<IServerTransport>();
                Log.Information("Listening on " + string.Join(", ", endPoints.Select(x => "[" + x + "]")));
                List<string> protocols = new List<string>();
                if (Configuration.Listener.ListenUDP)
                {
                    protocols.Add("UDP");
                    serverTransports.AddRange(endPoints.Select(x => new UdpServerTransport(x)));
                }
                if (Configuration.Listener.ListenTCP)
                {
                    protocols.Add("TCP");
                    serverTransports.AddRange(endPoints.Select(x => new TcpServerTransport(x)));
                }
                Log.Information("Protocols: " + string.Join(", ", protocols));
                if (serverTransports.Count == 0)
                {
                    Log.Error("No end point to listen");
                    throw new Exception("No end point to listen");
                }
                Server = new DnsServer(serverTransports.ToArray());

                Server.ExceptionThrown += Server_ExceptionThrown;
                Server.ClientConnected += Server_ClientConnected;
                Server.QueryReceived += Server_QueryReceived;
                Server.InvalidSignedMessageReceived += Server_InvalidSignedMessageReceived;

                Log.Information("Setting up resolvers...");
                InitializeResolvers(endPoints);

                Log.Information("Starting server...");
                try
                {
                    Server.Start();
                    Log.Information("Server started");
                }
                catch (Exception e)
                {
                    Log.Error(e);
                    throw;
                }
            });
        }

        public override async Task OnShutdown()
        {
            await Task.Run(() =>
            {
                if (Server != null)
                {
                    Log.Info("Stopping server...");
                    Server.Stop();
                    Log.End("Server stopped");
                }
                Server = null;
            });
        }

        public async Task<Message?> ReceiveMessageAsync(Message message, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var uriStart = "aura:services/" + Info.Id + "/";
                if (message.EventId == InternalEvents.UriRequest && message.HasBody<UriRequest>())
                {
                    var uriReq = message.GetBody<UriRequest>()?.Uris.Select(x => x.ToString()).Where(x => x.ToLower().StartsWith(uriStart)).Select(x => x.Substring(uriStart.Length))
                    .Distinct(StringComparer.OrdinalIgnoreCase).FirstOrDefault()?.Trim().ToLower();
                    if (!string.IsNullOrWhiteSpace(uriReq))
                    {
                        if (uriReq.StartsWith("config") && ConfigWindow == null)
                        {
                            ServiceManager.MainDispatcher?.Invoke(() =>
                            {
                                var w = CreateConfigurationWindow();
                                if (w is AutoConfigurator ac)
                                {
                                    ac.LoadConfiguration(Configuration);
                                    ac.Closed += Ac_Closed;
                                    ac.Topmost = true;
                                    ac.ShowDialog();
                                }
                            });
                        }
                    }
                }
                return null as Message;
            });
        }
    }
}