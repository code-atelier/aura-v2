﻿using ARSoft.Tools.Net.Dns;
using Aura.CodeAtelier.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public class DnsOverHttpResolver : IDnsResolver
    {
        public DnsOverHttpConfig Configuration { get; }
        public RestClient Client { get; }

        public string Name { get => Configuration.Name; }

        public DnsOverHttpResolver(DnsOverHttpConfig configuration)
        {
            Configuration = configuration;
            Client = new RestClient(new RestClientOptions()
            {
                Encoding = Encoding.UTF8,
            });
            foreach (var hdr in Configuration.Headers)
            {
                try
                {
                    if (hdr.Key.Trim().ToLower() != "user-agent")
                        Client.AddDefaultHeader(hdr.Key, hdr.Value);
                }
                catch { }
            }
            Client.AddDefaultHeader("User-Agent", "CoatlDNS");
        }

        public async Task<DnsMessage?> Resolve(DnsQuestion question)
        {
            var req = new RestRequest(Configuration.Uri, Configuration.Method == RequestMethod.GET ? Method.Get : Method.Post);
            if (Configuration.Method == RequestMethod.GET)
            {
                var fieldName = "name";
                if (Configuration.QueryFields.ContainsKey(fieldName))
                    fieldName = Configuration.QueryFields[fieldName];
                req.AddQueryParameter(fieldName, question.Name.ToString());
                fieldName = "type";
                if (Configuration.QueryFields.ContainsKey(fieldName))
                    fieldName = Configuration.QueryFields[fieldName];
                req.AddQueryParameter(fieldName, question.RecordType.ToString());
                var response = await Client.ExecuteAsync(req);
                if (response.IsSuccessStatusCode)
                {
                    var json = response.Content ?? "null";
                    var result = JsonSerializer.Deserialize<DnsJson>(json);
                    if (result != null)
                    {
                        return result.ToMessage();
                    }
                }
            }
            else
            {
                throw new NotImplementedException();
            }
            return null;
        }
    }
}
