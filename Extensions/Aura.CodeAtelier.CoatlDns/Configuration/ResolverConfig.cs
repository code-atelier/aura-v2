﻿using Aura.CodeAtelier.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Configuration
{
    public class ResolverConfig
    {
        [JsonPropertyName("timeout")]
        [AutoConfigMember("Timeout",
            Description = "Resolver timeout in milliseconds",
            NumericMaxValue = 60000,
            NumericMinValue = 100,
            Icon = "res/clock.png",
            Kind = AutoConfigPropertyKind.IntegerTextbox)]
        public int Timeout { get; set; } = 1000;


        [JsonPropertyName("doh")]
        [AutoConfigMember("DNS over Http",
            Description = "Setup DNS over Http resolvers that supports dns json",
            Icon = "res/cloud-up.png",
            ConfigurationControl = typeof(DOHResolverEditorLinkLabel),
            Kind = AutoConfigPropertyKind.CustomWithLabel)]
        public List<DnsOverHttpConfig> DnsOverHttpConfigs { get; set; } = new List<DnsOverHttpConfig>();
    }
}
