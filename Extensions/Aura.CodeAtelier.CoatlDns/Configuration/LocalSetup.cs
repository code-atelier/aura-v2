﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.CodeAtelier.Configuration
{
    public class LocalSetup
    {
        [AutoConfigMember("Network interface",
            Description = "The network interface to setup",
            ItemSource = typeof(NICSource),
            Icon = "res/device-drive.png",
            Kind = AutoConfigPropertyKind.DropDown,
            UpdatePage = true)]
        public NICWrapper? NIC { get; set; } = NICSource.GetDefault();

        [AutoConfigMember("IP Address",
            Group = "Internet Protocol version 4",
            GroupIndex = 1,
            Description = "The address and gateway of this network interface",
            Icon = "res/network.png",
            Kind = AutoConfigPropertyKind.ReadOnlyLabel
        )]
        public string IP4Gateway { get => NIC?.IP4AddressGateway ?? "-"; }

        [AutoConfigMember("DNS servers",
            Group = "Internet Protocol version 4",
            GroupIndex = 1,
            Index = 1,
            Description = "DNS server IP addresses and tunnel status",
            Icon = "res/layers.png",
            Kind = AutoConfigPropertyKind.ReadOnlyLabel
        )]
        public string IP4DNS { get => ((NIC?.IP4DNS ?? "-").Length > 40 ? (NIC?.IP4DNS ?? "-").Substring(0, 37) + "..." : (NIC?.IP4DNS ?? "-")) + Environment.NewLine + NIC?.IP4DOHReady; }

        [AutoConfigMember("IP Address",
            Group = "Internet Protocol version 6",
            GroupIndex = 1,
            Description = "The address and gateway of this network interface",
            Icon = "res/network.png",
            Kind = AutoConfigPropertyKind.ReadOnlyLabel
        )]
        public string IP6Gateway { get => NIC?.IP6AddressGateway ?? "-"; }

        [AutoConfigMember("DNS servers",
            Group = "Internet Protocol version 6",
            GroupIndex = 1,
            Index = 1,
            Description = "DNS server IP addresses and tunnel status",
            Icon = "res/layers.png",
            Kind = AutoConfigPropertyKind.ReadOnlyLabel
        )]
        public string IP6DNS { get => ((NIC?.IP6DNS ?? "-").Length > 40 ? (NIC?.IP6DNS ?? "-").Substring(0, 37) + ".." : (NIC?.IP6DNS ?? "-")) + Environment.NewLine + NIC?.IP6DOHReady; }

        [AutoConfigMember("Setup",
            Group = "Internet Protocol version 4",
            GroupIndex = 0,
            Index = 2,
            Description = "Attach or detach CoatlDNS on this NIC",
            Icon = "res/shield.png",
            MethodLabel = "Run Setup",
            UpdatePage = true
        )]
        public void RunIPv4Setup()
        {
            if (NIC is NICWrapper nicw)
            {
                try
                {
                    var dns = nicw.NetworkInterface.GetIPProperties().DnsAddresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).Select(x => x.ToString()).ToList();
                    if (!dns.Contains("127.0.0.1"))
                        dns.Insert(0, "127.0.0.1");
                    if (nicw.IP4DOHReady == "Not Ready")
                        SetupDOH("ipv4", nicw.Name, dns.ToArray());
                    else
                        SetupDOH("ipv4", nicw.Name);
                }
                catch
                {
                    if (nicw.IP4DOHReady == "Not Ready")
                        MessageBox.Show("Failed to setup DNS Over Https", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    else
                        MessageBox.Show("Failed to reset interface configuration", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    NICSource.Populate(true);
                    NIC = NICSource.Get(NIC);
                }
            }
        }

        [AutoConfigMember("Setup",
            Group = "Internet Protocol version 6",
            GroupIndex = 1,
            Index = 2,
            Description = "Attach or detach CoatlDNS on this NIC",
            Icon = "res/shield.png",
            MethodLabel = "Run Setup",
            UpdatePage = true
        )]
        public void RunIPv6Setup()
        {
            if (NIC is NICWrapper nicw)
            {
                try
                {
                    var dns = nicw.NetworkInterface.GetIPProperties().DnsAddresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6).Select(x => x.ToString()).ToList();
                    if (!dns.Contains("::1"))
                        dns.Insert(0, "::1");
                    if (nicw.IP6DOHReady == "Not Ready")
                        SetupDOH("ipv6", nicw.Name, "::1");
                    else
                        SetupDOH("ipv6", nicw.Name);
                }
                catch
                {
                    if (nicw.IP6DOHReady == "Not Ready")
                        MessageBox.Show("Failed to setup DNS Over Https", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    else
                        MessageBox.Show("Failed to reset interface configuration", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    NICSource.Populate(true);
                    NIC = NICSource.Get(NIC);
                }
            }
        }

        private void SetupDOH(string ipv, string name, params string?[] address)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.FileName = "cmd";
            startInfo.Verb = "runas";
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = "/c netsh interface " + ipv + " set dnsserver name=\"" + name + "\"";
            if (address != null && address.Length > 0)
            {
                startInfo.Arguments += " source=static address=" + address[0];
                for (var i = 1; i < address.Length; i++)
                {
                    startInfo.Arguments += " &&";
                    startInfo.Arguments += " netsh interface " + ipv + " add dnsserver name=\"" + name + "\"";
                    startInfo.Arguments += " address=" + address[i];
                }
            }
            else
            {
                startInfo.Arguments += " source=dhcp";
            }
            Process? p = Process.Start(startInfo);
            if (p != null)
            {
                p.WaitForExit();
                if (p.ExitCode != 0)
                    throw new Exception();
            }
            else throw new Exception();
        }
    }

    public class NICSource : IAutoConfigItemSource
    {
        private static Dictionary<string, NICWrapper> NICWrappers { get; } = new Dictionary<string, NICWrapper>();

        public static NICWrapper? Get(NICWrapper wrapper)
        {
            return NICWrappers.Values.FirstOrDefault(x => x.NetworkInterface.Id == wrapper.NetworkInterface.Id);
        }

        public static NICWrapper? GetDefault()
        {
            Populate();
            return NICWrappers.Values.FirstOrDefault(x => x.NetworkInterface.GetIPProperties().GatewayAddresses.Any(x => x?.Address != null));
        }

        public static void Populate(bool force = false)
        {
            if (NICWrappers.Count > 0 && !force)
            {
                return;
            }
            NICWrappers.Clear();
            var nics = NetworkInterface.GetAllNetworkInterfaces()
                    .Where(ni =>
                    ni.OperationalStatus == OperationalStatus.Up && (
                        ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                        || ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211
                    )).Select(x => new NICWrapper(x)).ToList();
            foreach (var nic in nics)
            {
                NICWrappers[nic.Name] = nic;
            }
        }

        public object? this[string key]
        {
            get => NICWrappers[key];
            set
            {
                if (value is NICWrapper nic)
                    NICWrappers[key] = nic;
            }
        }

        public NICSource()
        {
            Populate();
        }

        public IEnumerator<KeyValuePair<string, object?>> GetEnumerator()
        {
            return NICWrappers.ToDictionary(x => x.Key, x => (object)x.Value).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
