﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Configuration
{
    public class CacheConfig
    {
        [JsonPropertyName("useCache")]
        [AutoConfigMember("Enable Cache",
            Description = "Enable caching to prevent repeated request to DoH server",
            Icon = "res/address-book.png",
            Index = 1,
            Kind = AutoConfigPropertyKind.Toggle)]
        public bool UseCache { get; set; } = true;

        /// <summary>
        /// Cache expiration in seconds
        /// </summary>
        [JsonPropertyName("cacheExpire")]
        [AutoConfigMember("Expiration",
            Description = "Set the expiration of cached message (in seconds)",
            Icon = "res/clock.png",
            Index = 2,
            NumericMaxValue = 999999,
            NumericMinValue = 0,
            Kind = AutoConfigPropertyKind.IntegerTextbox)]
        public int CacheExpire { get; set; } = 60;

        /// <summary>
        /// Maximum number of cached responses. Default is 3000.
        /// </summary>
        [JsonPropertyName("maxResponses")]
        [AutoConfigMember("Cache Size",
            Description = "Set the maximum number of cached messages",
            Icon = "res/dashboard.png",
            Index = 2,
            NumericMaxValue = 999999,
            NumericMinValue = 100,
            Kind = AutoConfigPropertyKind.IntegerTextbox)]
        public int MaxResponses { get; set; } = 3000;
    }
}
