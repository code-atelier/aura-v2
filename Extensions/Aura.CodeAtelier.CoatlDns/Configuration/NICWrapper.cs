﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Configuration
{

    public class NICWrapper
    {
        public NetworkInterface NetworkInterface { get; }

        public string Name { get => NetworkInterface.Name; }
        public string Description { get => NetworkInterface.Description; }
        public OperationalStatus OperationalStatus { get => NetworkInterface.OperationalStatus; }
        public NetworkInterfaceType NetworkInterfaceType { get => NetworkInterface.NetworkInterfaceType; }

        public string IP4Address { get => NetworkInterface.GetIPProperties()?.UnicastAddresses.FirstOrDefault(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)?.Address.ToString() ?? "??"; }
        public string IP6Address { get => NetworkInterface.GetIPProperties()?.UnicastAddresses.FirstOrDefault(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)?.Address.ToString() ?? "??"; }
        public string IP4Gateway { get => NetworkInterface.GetIPProperties()?.GatewayAddresses.FirstOrDefault(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)?.Address.ToString() ?? "??"; }
        public string IP6Gateway { get => NetworkInterface.GetIPProperties()?.GatewayAddresses.FirstOrDefault(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)?.Address.ToString() ?? "??"; }

        public string IP4AddressGateway { get => IP4Address + " → " + IP4Gateway; }
        public string IP6AddressGateway { get => IP6Address + " → " + IP6Gateway; }

        public string IP4DNS
        {
            get
            {
                var ips = NetworkInterface.GetIPProperties()?.DnsAddresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).Select(x => x.ToString()).ToList() ?? new List<string>();
                return ips.Count > 0 ? string.Join(", ", ips) : "-";
            }
        }
        public string IP6DNS
        {
            get
            {
                var ips = NetworkInterface.GetIPProperties()?.DnsAddresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6).Select(x => x.ToString()).ToList() ?? new List<string>();
                return ips.Count > 0 ? string.Join(", ", ips) : "-";
            }
        }

        public string IP4DOHReady { get => NetworkInterface.GetIPProperties()?.DnsAddresses.Any(x => x.ToString() == "127.0.0.1") == true ? "Ready" : "Not Ready"; }
        public string IP6DOHReady { get => NetworkInterface.GetIPProperties()?.DnsAddresses.Any(x => x.ToString() == "::1") == true ? "Ready" : "Not Ready"; }

        public bool IP4CanSetup { get => NetworkInterface.OperationalStatus == OperationalStatus.Up; }
        public bool IP6CanSetup { get => NetworkInterface.OperationalStatus == OperationalStatus.Up; }

        public string IP4SetupText { get => IP4DOHReady == "Yes" ? "Reset IPv4 Configuration" : "Setup DNS Over Https for IPv4"; }
        public string IP6SetupText { get => IP6DOHReady == "Yes" ? "Reset IPv6 Configuration" : "Setup DNS Over Https for IPv6"; }

        public NICWrapper(NetworkInterface networkInterface)
        {
            NetworkInterface = networkInterface;
        }

        public override string ToString()
        {
            return NetworkInterface.Name + " (" + NetworkInterface.Description + ")";
        }

        public override bool Equals(object? obj)
        {
            if (obj is NICWrapper other)
            {
                return other.NetworkInterface.Id == NetworkInterface.Id;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return NetworkInterface.Id.GetHashCode();
        }
    }
}
