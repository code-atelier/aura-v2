﻿using Aura.CodeAtelier.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Configuration
{
    public class ListenerConfig
    {
        [JsonPropertyName("serverName")]
        [AutoConfigMember("Server Name",
            Description = "Server name that will be shown in queries (must be a valid domain name)",
            Icon = "res/sign-info.png",
            Index = 0,
            Validator = typeof(DomainNameValidator),
            Kind = AutoConfigPropertyKind.Textbox)]
        public string ServerName { get; set; } = Environment.MachineName;

        /// <summary>
        /// Binds the server to local loopback addresses only. Overrides <see cref="EndPoints"/>.
        /// </summary>
        [JsonPropertyName("localMachineOnly")]
        [AutoConfigMember("Local Machine Only",
            Description = "Binds to local machine address only (turn off to also serve requests from local network)",
            Icon = "res/device-computer.png",
            Index = 1,
            Kind = AutoConfigPropertyKind.Toggle)]
        public bool LocalMachineOnly { get; set; } = true;

        [JsonPropertyName("useIPv4")]
        [AutoConfigMember("Use IPv4",
            Description = "Use Internet Protocol version 4 to listen",
            Icon = "res/layers.png",
            Index = 2,
            Kind = AutoConfigPropertyKind.Toggle)]
        public bool UseIPv4 { get; set; } = true;

        [JsonPropertyName("useIPv6")]
        [AutoConfigMember("Use IPv6",
            Description = "Use Internet Protocol version 6 to listen",
            Icon = "res/layers.png",
            Index = 3,
            Kind = AutoConfigPropertyKind.Toggle)]
        public bool UseIPv6 { get; set; } = false;

        /// <summary>
        /// End points that should be bound.
        /// </summary>
        [JsonPropertyName("endPoints")]
        [AutoConfigMember("End Points",
            Description = "End points which listeners are bound to (only when 'Local Machine Only' is turned off)",
            Icon = "res/network.png",
            Index = 4,
            ConfigurationControl = typeof(BoundAddressEditorLinkLabel),
            Kind = AutoConfigPropertyKind.CustomWithLabel)]
        public List<string> EndPoints { get; set; } = new List<string>() { "0.0.0.0:53", "[::]:53" };

        [JsonPropertyName("listenUDP")]
        [AutoConfigMember("Listen UDP",
            Group = "Connections",
            Icon = "res/handshake.png",
            GroupIndex = 1,
            Description = "Listen to UDP requests",
            Kind = AutoConfigPropertyKind.Toggle)]
        public bool ListenUDP { get; set; } = true;

        [JsonPropertyName("listenTCP")]
        [AutoConfigMember("Listen TCP",
            Group = "Connections",
            Icon = "res/handshake.png",
            GroupIndex = 1,
            Description = "Listen to TCP requests",
            Kind = AutoConfigPropertyKind.Toggle)]
        public bool ListenTCP { get; set; } = false;

        [JsonPropertyName("domainFilter")]
        [AutoConfigMember("Domain Filter",
            Group = "Filter",
            GroupIndex = 2,
            Description = "Filter domains with whitelist or blacklist",
            Icon = "res/globe.png",
            ConfigurationControl = typeof(HostFilteringRuleEditorLinkLabel),
            Kind = AutoConfigPropertyKind.CustomWithLabel)]
        public HostFilteringRule DomainFilter { get; set; } = new HostFilteringRule();

        [JsonPropertyName("clientFilter")]
        [AutoConfigMember("Client Filter",
            Group = "Filter",
            GroupIndex = 2,
            Description = "Filter clients with whitelist or blacklist",
            Icon = "res/profile-group.png",
            ConfigurationControl = typeof(HostFilteringRuleEditorLinkLabel),
            Kind = AutoConfigPropertyKind.CustomWithLabel)]
        public HostFilteringRule ClientFilter { get; set; } = new HostFilteringRule();
    }
}
