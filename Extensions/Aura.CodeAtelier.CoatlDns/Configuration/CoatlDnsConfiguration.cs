﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json.Serialization;
using System.Windows.Documents;

namespace Aura.CodeAtelier.Configuration
{
    [AuraConfiguration("coatldns.json")]
    [AutoConfig("Coatl DNS", Icon = "res/icon.png")]
    public class CoatlDnsConfiguration
    {
        [JsonIgnore]
        [AutoConfigMember("Local machine",
            Description = "Configure your local machine",
            Index = 1,
            Icon = "res/device-computer.png",
            Kind = AutoConfigPropertyKind.Page)]
        public LocalSetup LocalSetup { get; set; } = new LocalSetup();

        [JsonPropertyName("listener")]
        [AutoConfigMember("Listener",
            Description = "Configure listener IP binding and filter",
            Index = 1,
            Icon = "res/cloud-service.png",
            Kind = AutoConfigPropertyKind.Page)]
        public ListenerConfig Listener { get; set; } = new ListenerConfig();

        [JsonPropertyName("resolver")]
        [AutoConfigMember("Resolver",
            Description = "Configure DNS resolver",
            Index = 2,
            Icon = "res/cloud-up.png",
            Kind = AutoConfigPropertyKind.Page)]
        public ResolverConfig Resolver { get; set; } = new ResolverConfig();

        [JsonPropertyName("cache")]
        [AutoConfigMember("Cache",
            Description = "Configure DNS response cache",
            Index = 3,
            Icon = "res/address-book.png",
            Kind = AutoConfigPropertyKind.Page)]
        public CacheConfig Cache { get; set; } = new CacheConfig();

        [JsonPropertyName("authorityDomains")]
        [AutoConfigMember("Authority",
            Description = "Configure authority domains",
            Index = 4,
            Icon = "res/lock.png",
            Kind = AutoConfigPropertyKind.Page)]
        public List<AuthorityDomainConfig> AuthorityDomains { get; set; } = new List<AuthorityDomainConfig>();

        /*
        [AutoConfigMember("Logs",
            Description = "View application logs",
            MethodLabel = "Open log folder",
            Icon = "res/file-text.png",
            Index = 5
        )]
        public void ShowLog()
        {
            var logPath = Path.Combine(AuraLogger.DefaultLogPath, "coatl.dns");
            Util.OpenFile(logPath);
        }
        */
    }
}
