﻿using ARSoft.Tools.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Configuration
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum FilteringRule
    {
        AllowAll,

        Blacklist,
        Whitelist,
    }

    public class HostFilteringRule
    {
        [JsonPropertyName("rule")]
        public FilteringRule Rule { get; set; } = FilteringRule.AllowAll;

        [JsonPropertyName("whitelistedHosts")]
        public List<string> WhitelistedHosts { get; set; } = new List<string>();

        [JsonPropertyName("blacklistedHosts")]
        public List<string> BlacklistedHosts { get; set; } = new List<string>();

        public bool Allowed(IPEndPoint endPoint)
        {
            if (Rule == FilteringRule.AllowAll) return true;
            var full = endPoint.Address.ToString().ToLower() + ":" + endPoint.Port;
            var hostOnly = endPoint.Address.ToString();
            if (Rule == FilteringRule.Whitelist)
            {
                if (WhitelistedHosts.Contains(full) || WhitelistedHosts.Contains(hostOnly))
                    return true;
            }
            if (Rule == FilteringRule.Blacklist)
            {
                if (!BlacklistedHosts.Contains(full) && !BlacklistedHosts.Contains(hostOnly))
                    return true;
            }
            return false;
        }
        public bool Allowed(string domainName)
        {
            if (Rule == FilteringRule.AllowAll) return true;
            try
            {
                var domName = DomainName.Parse(domainName);
                if (Rule == FilteringRule.Whitelist)
                {
                    if (WhitelistedHosts.Any(x =>
                    {
                        try
                        {
                            return DomainName.Parse(x).IsEqualOrSubDomainOf(domName);
                        }
                        catch
                        {
                            return false;
                        }
                    }))
                        return true;
                }
                if (Rule == FilteringRule.Blacklist)
                {
                    if (!BlacklistedHosts.Any(x =>
                    {
                        try
                        {
                            return DomainName.Parse(x).IsEqualOrSubDomainOf(domName);
                        }
                        catch
                        {
                            return false;
                        }
                    }))
                        return true;
                }
            }
            catch { }
            return false;
        }
    }
}
