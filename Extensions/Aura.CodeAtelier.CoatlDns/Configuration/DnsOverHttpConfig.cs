﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Configuration
{
    public class DnsOverHttpConfig
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("uri")]
        public string Uri { get; set; } = string.Empty;

        [JsonPropertyName("method")]
        public RequestMethod Method { get; set; } = RequestMethod.GET;

        [JsonPropertyName("queryFields")]
        public Dictionary<string, string> QueryFields { get; set; } = new Dictionary<string, string>();

        [JsonPropertyName("headers")]
        public Dictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();

        [JsonPropertyName("active")]
        public bool Active { get; set; } = true;

        public override string ToString()
        {
            return Name;
        }
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum RequestMethod
    {
        GET,
        POST
    }
}
