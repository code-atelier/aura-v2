﻿using ARSoft.Tools.Net.Dns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public partial class CoatlDnsService
    {
        private async Task Server_ExceptionThrown(object sender, ExceptionEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                Log.Error(eventArgs.Exception);
            });
        }

        private async Task Server_ClientConnected(object sender, ClientConnectedEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                if (Configuration.Listener.ClientFilter.Allowed(eventArgs.RemoteEndpoint))
                {
                    Log.Debug($"Client [" + eventArgs.RemoteEndpoint + "] connected");
                }
                else
                {
                    eventArgs.RefuseConnect = true;
                    Log.Warning($"Client [" + eventArgs.RemoteEndpoint + "] tried to connect, but filtered");
                }
            });
        }

        private async Task Server_InvalidSignedMessageReceived(object sender, InvalidSignedMessageEventArgs eventArgs)
        {
            await Task.Run(() =>
            {

            });
        }

        private async Task Server_QueryReceived(object sender, QueryReceivedEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                if (eventArgs.Query is DnsMessage message)
                {
                    Log.Debug($"Client [" + eventArgs.RemoteEndpoint + "] requested " + string.Join(",", message.Questions.Select(x => "{" + x.Name + ":" + x.RecordType + "}")));

                    try
                    {
                        List<Task<ResolverResult?>> tasks = new List<Task<ResolverResult?>>();
                        foreach (var question in message.Questions)
                        {
                            tasks.Add(Resolve(question));
                        }
                        Task.WaitAll(tasks.ToArray(), Configuration.Resolver.Timeout);
                        var results = tasks.Where(x => x.IsCompletedSuccessfully).Select(x => x.Result).OfType<ResolverResult>().Where(x => x.Message.ReturnCode != ReturnCode.ServerFailure).ToList();
                        foreach (var res in results)
                        {
                            if (res.Resolver != Cache && !(res.Resolver is DnsInverseLocalResolver))
                            {
                                Log.Debug($"Client [" + eventArgs.RemoteEndpoint + "] question " + "{" + res.Question.Name + ":" + res.Question.RecordType + "} resolved by '" + res.Resolver + "'");

                                Cache.Record(res.Question, res.Message);
                            }
                        }

                        var reply = message.CreateResponseInstance();
                        reply.AnswerRecords.AddRange(results.SelectMany(x => x.Message.AnswerRecords));
                        reply.AnswerRecords = reply.AnswerRecords.Distinct().ToList();
                        reply.AdditionalRecords.AddRange(results.SelectMany(x => x.Message.AdditionalRecords));
                        reply.AdditionalRecords = reply.AdditionalRecords.Distinct().ToList();
                        reply.AuthorityRecords.AddRange(results.SelectMany(x => x.Message.AuthorityRecords));
                        reply.AuthorityRecords = reply.AuthorityRecords.Distinct().ToList();
                        reply.IsAuthenticData = !results.Any(x => x.Message.IsAuthenticData);
                        reply.IsCheckingDisabled = results.Any(x => x.Message.IsCheckingDisabled);
                        reply.IsTruncated = results.Any(x => x.Message.IsTruncated);
                        reply.IsRecursionDesired = results.Any(x => x.Message.IsRecursionDesired);
                        reply.IsRecursionAllowed = results.Any(x => x.Message.IsRecursionAllowed);

                        reply.ReturnCode = reply.AnswerRecords.Count > 0 || reply.AdditionalRecords.Count > 0  || reply.AuthorityRecords.Count > 0 ? ReturnCode.NoError : ReturnCode.NxDomain;
                        foreach (var res in results)
                        {
                            if (res.Message.ReturnCode > reply.ReturnCode)
                            {
                                reply.ReturnCode = res.Message.ReturnCode;
                            }
                        }

                        eventArgs.Response = reply;
                        Log.Debug($"Replied [{eventArgs.RemoteEndpoint}] with '{reply.ReturnCode}'");
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }
                else
                {
                    Log.Warning($"Client [" + eventArgs.RemoteEndpoint + "] requested something, but the format is unknown");
                }
            });
        }

        private async Task<ResolverResult?> Resolve(DnsQuestion question)
        {
            var retriableReturnCode = new List<ReturnCode>()
            {
                ReturnCode.NxDomain
            };
            foreach (var resolver in Resolvers)
            {
                try
                {
                    var message = await resolver.Resolve(question);
                    if (message != null && !retriableReturnCode.Contains(message.ReturnCode))
                    {
                        return new ResolverResult(question, message, resolver);
                    }
                }
                catch { }
            }
            return null;
        }
    }

    public class ResolverResult
    {
        public DnsMessage Message { get; }

        public IDnsResolver Resolver { get; }

        public DnsQuestion Question { get; }

        public ResolverResult(DnsQuestion question, DnsMessage message, IDnsResolver resolver)
        {
            Message = message;
            Resolver = resolver;
            Question = question;
        }
    }
}
