﻿using ARSoft.Tools.Net.Dns;
using Aura.CodeAtelier.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public class DnsResponseCache : IDnsResolver
    {
        public CoatlDnsConfiguration Config { get; }

        public List<DnsResponseCacheEntry> CacheEntries { get; } = new List<DnsResponseCacheEntry>();

        public string Name { get; } = "Cache";

        public DnsResponseCache(CoatlDnsConfiguration configuration)
        {
            Config = configuration;
        }

        public async Task<DnsMessage?> Resolve(DnsQuestion question)
        {
            return await Task.Run(() =>
            {
                try
                {
                    Housekeeping();
                    if (CacheEntries.FirstOrDefault(x => x?.Question != null && x.Question.Equals(question)) is DnsResponseCacheEntry entry)
                    {
                        return entry.Message;
                    }
                }
                catch { }
                return null;
            });
        }

        public void Housekeeping()
        {
            CacheEntries.RemoveAll(x => x != null && x.Expire <= DateTime.Now);
            var sorted = CacheEntries.OrderBy(x => x.Expire).ToList();
            int index = 0;
            while(CacheEntries.Count > (Config?.Cache?.MaxResponses ?? 3000) && index < sorted.Count)
            {
                if (!CacheEntries.Remove(sorted[index++]))
                    break;
            }
        }

        public void Record(DnsQuestion question, DnsMessage message)
        {
            try
            {
                CacheEntries.RemoveAll(x => x?.Question != null && x.Question.Equals(question));
                CacheEntries.Add(new DnsResponseCacheEntry(question, message, Config?.Cache?.CacheExpire ?? 60));
                Housekeeping();
            }
            catch { }
        }
    }

    public class DnsResponseCacheEntry
    {
        public DnsQuestion Question { get; }

        public DnsMessage Message { get; }

        public DateTime Expire { get; }

        public DnsResponseCacheEntry(DnsQuestion question, DnsMessage message, double expireSecond)
        {
            Question = question;
            Message = message;
            Expire = DateTime.Now.AddSeconds(expireSecond);
        }
    }
}
