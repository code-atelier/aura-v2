﻿using ARSoft.Tools.Net.Dns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public interface IDnsResolver
    {
        string Name { get; }

        Task<DnsMessage?> Resolve(DnsQuestion question);
    }
}
