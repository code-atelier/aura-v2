﻿using Org.BouncyCastle.Utilities.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.CodeAtelier.WPF
{
    /// <summary>
    /// Interaction logic for IPEndPointListEditor.xaml
    /// </summary>
    public partial class IPEndPointListEditor : Window
    {
        public IPEndPointListEditor()
        {
            InitializeComponent();
        }

        public List<string> EndPoints { get; } = new List<string>();

        public bool AddEndpoint(string endpoint)
        {
            if (IPEndPoint.TryParse(endpoint, out var ipe))
            {
                if (ipe.Port == 0)
                {
                    ipe.Port = 53; // default dns port
                }
                lbList.Items.Add(new ListBoxItem()
                {
                    Content = ipe.ToString() + (
                    ipe.AddressFamily == AddressFamily.InterNetwork ? " (IPv4)" 
                    : ipe.AddressFamily == AddressFamily.InterNetworkV6 ? " (IPv6)"
                    : ""),
                    Tag = ipe,
                });
                return true;
            }
            return false;
        }

        public void Load(IEnumerable<string> ipEndPoints)
        {
            lbList.Items.Clear();
            foreach (var ip in ipEndPoints)
            {
                AddEndpoint(ip);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            EndPoints.Clear();
            foreach(var lbi in lbList.Items.OfType<ListBoxItem>())
            {
                if (lbi.Tag is IPEndPoint ipe)
                {
                    EndPoints.Add(ipe.ToString());
                }
            }
            DialogResult = true;
            Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            ManualAdd();
        }

        private void ManualAdd()
        {
            if (AddEndpoint(txEndPoint.Text))
            {
                txEndPoint.Clear();
            }
            else
            {
                MessageBox.Show($"'{txEndPoint.Text}' is not a valid IP end point!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void lbList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnDelete.IsEnabled = lbList.SelectedItem is ListBoxItem lbi && lbi.Tag is IPEndPoint;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lbList.SelectedItem is ListBoxItem lbi)
                lbList.Items.Remove(lbi);
        }

        private void txEndPoint_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;

                ManualAdd();
            }
        }
    }
}
