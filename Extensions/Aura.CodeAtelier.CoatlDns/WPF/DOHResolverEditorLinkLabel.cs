﻿using Aura.CodeAtelier.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows;

namespace Aura.CodeAtelier.WPF
{
    internal class DOHResolverEditorLinkLabel : TextBlock, IAutoConfigUserControlWithLabel
    {
        List<DnsOverHttpConfig> Configs { get; set; } = new List<DnsOverHttpConfig>();

        public DOHResolverEditorLinkLabel()
        {
            Margin = new Thickness(50, 0, 0, 0);

            var hp = new Hyperlink();
            hp.Inlines.Add("Manage resolvers");
            hp.Click += Hp_Click;
            Inlines.Add(hp);
        }

        public event EventHandler? ValueChanged;

        private void Hp_Click(object sender, RoutedEventArgs e)
        {
            var w = new DOHListEditor();
            w.LoadConfig(Configs);
            if (w.ShowDialog() == true && w.Result != null)
            {
                Configs = w.Result;
                ValueChanged?.Invoke(this, new EventArgs());
            }
        }

        public void LoadConfigurationValue(object? value)
        {
            if (value is List<DnsOverHttpConfig> list)
            {
                Configs = list;
            }
        }

        public string GetLabel()
        {
            return $"{Configs.Count} resolver(s)";
        }

        public object? GetConfigurationValue()
        {
            return Configs;
        }
    }
}
