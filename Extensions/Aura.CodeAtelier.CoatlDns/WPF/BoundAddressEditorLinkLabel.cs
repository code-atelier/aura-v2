﻿using Aura.Models;
using Aura.WPF.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows;

namespace Aura.CodeAtelier.WPF
{
    public class BoundAddressEditorLinkLabel : TextBlock, IAutoConfigUserControlWithLabel
    {
        List<string> EndPoints { get; } = new List<string>();

        public BoundAddressEditorLinkLabel()
        {
            Margin = new Thickness(50, 0, 0, 0);

            var hp = new Hyperlink();
            hp.Inlines.Add("Manage Endpoints");
            hp.Click += Hp_Click;
            Inlines.Add(hp);
        }

        public event EventHandler? ValueChanged;

        private void Hp_Click(object sender, RoutedEventArgs e)
        {
            var compend = new IPEndPointListEditor();
            compend.Load(EndPoints);
            if (compend.ShowDialog() == true)
            {
                try
                {
                    EndPoints.Clear();
                    EndPoints.AddRange(compend.EndPoints);
                    ValueChanged?.Invoke(this, new EventArgs());
                }
                catch
                {
                }
            }
        }

        public void LoadConfigurationValue(object? value)
        {
            if (value is List<string> lst)
            {
                EndPoints.Clear();
                EndPoints.AddRange(lst);
            }
        }

        public string GetLabel()
        {
            if (EndPoints.Count == 0)
            {
                return "none";
            }
            if (EndPoints.Count <= 2)
            {
                return string.Join("; ", EndPoints);
            }
            return EndPoints.Count + " end points";
        }

        public object? GetConfigurationValue()
        {
            return EndPoints;
        }
    }
}
