﻿using Aura.CodeAtelier.Configuration;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.CodeAtelier.WPF
{
    /// <summary>
    /// Interaction logic for DOHListEditor.xaml
    /// </summary>
    public partial class DOHListEditor : Window
    {
        public DOHListEditor()
        {
            InitializeComponent();
        }

        public List<DnsOverHttpConfig> Result { get; private set; } = new List<DnsOverHttpConfig>();

        public void LoadConfig(List<DnsOverHttpConfig> configs)
        {
            Result = new List<DnsOverHttpConfig>();
            lbDoHList.Items.Clear();
            foreach (var item in configs)
            {
                var nitem = new DnsOverHttpConfig()
                {
                    Active = item.Active,
                    Headers = item.Headers.ToDictionary(x => x.Key, x => x.Value),
                    Method = item.Method,
                    Name = item.Name,
                    QueryFields = item.QueryFields.ToDictionary(x => x.Key, x => x.Value),
                    Uri = item.Uri,
                };
                lbDoHList.Items.Add(nitem);
                Result.Add(nitem);
            }
        }

        private void CommandAlways_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var item = new DnsOverHttpConfig()
            {
                Active = false,
                Name = "New DOH Resolver",
                Method = RequestMethod.GET,
            };
            Result.Add(item);
            lbDoHList.Items.Add(item);
            lbDoHList.SelectedItem = item;
        }

        bool disableLoad = false;
        private void RefreshItems()
        {
            disableLoad = true;
            try
            {
                var selItem = lbDoHList.SelectedItem;
                lbDoHList.Items.Clear();
                foreach (var item in Result)
                {
                    lbDoHList.Items.Add(item);
                }
                lbDoHList.SelectedItem = selItem;
            }
            finally
            {
                disableLoad = false;
            }
        }

        private void CommandImport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "DoH JSON|*.doh.json";
            if (ofd.ShowDialog() == true)
            {
                try
                {
                    var json = File.ReadAllText(ofd.FileName);
                    var config = JsonSerializer.Deserialize<DnsOverHttpConfig>(json);
                    if (config == null)
                    {
                        throw new Exception("Not a valid DoH JSON!");
                    }
                    lbDoHList.Items.Add(config);
                    Result.Add(config);
                    lbDoHList.SelectedItem = config;
                    MessageBox.Show("Imported successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void CommandEdit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lbDoHList.SelectedItem is DnsOverHttpConfig;
        }

        private void CommandExport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (lbDoHList.SelectedItem is DnsOverHttpConfig config)
            {
                var sfd = new SaveFileDialog();
                sfd.Filter = "DoH JSON|*.doh.json";
                sfd.FileName = config.Name + ".doh.json";
                if (sfd.ShowDialog() == true)
                {
                    try
                    {
                        var json = JsonSerializer.Serialize(config);
                        File.WriteAllText(sfd.FileName, json);
                        MessageBox.Show("Exported successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (lbDoHList.SelectedItem is DnsOverHttpConfig config)
            {
                if (MessageBox.Show($"Are you sure to delete '{config.Name}'?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
                Result.Remove(config);
                lbDoHList.Items.Remove(config);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        bool disableUpdate { get; set; } = false;
        private void lbDoHList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (disableLoad) { return; }
            gbEditor.IsEnabled = lbDoHList.SelectedItem is DnsOverHttpConfig;
            disableUpdate = true;
            try
            {
                if (lbDoHList.SelectedItem is DnsOverHttpConfig config)
                {
                    txName.Text = config.Name;
                    txUri.Text = config.Uri;
                    cxActive.IsChecked = config.Active;
                    txQuery.Text = string.Join(Environment.NewLine, config.QueryFields.Select(x => x.Key + "=" + x.Value).ToArray());
                    txHeaders.Text = string.Join(Environment.NewLine, config.Headers.Select(x => x.Key + ": " + x.Value).ToArray());
                    cbMethod.SelectedIndex = (int)config.Method;
                }
                else
                {
                    txName.Text = string.Empty;
                    txUri.Text = string.Empty;
                    cxActive.IsChecked = false;
                    txQuery.Text = string.Empty;
                    txHeaders.Text = string.Empty;
                    cbMethod.SelectedIndex = 0;
                }
            }
            catch { }
            finally
            { disableUpdate = false; }
        }

        private void UpdateValue(bool shouldRefresh = false)
        {
            if (!disableUpdate && lbDoHList.SelectedItem is DnsOverHttpConfig config)
            {
                config.Name = txName.Text.Trim();
                config.Uri = txUri.Text.Trim();
                config.Active = cxActive.IsChecked == true;
                config.Method = (RequestMethod)cbMethod.SelectedIndex;
                var lines = txQuery.Text.Split(Environment.NewLine, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                config.QueryFields.Clear();
                foreach (var line in lines)
                {
                    var spl = line.Split('=', 2, StringSplitOptions.TrimEntries);
                    if (spl.Length == 2)
                    {
                        if (!string.IsNullOrWhiteSpace(spl[0]) && !string.IsNullOrWhiteSpace(spl[1]))
                        {
                            config.QueryFields[spl[0]] = spl[1];
                        }
                    }
                }
                lines = txHeaders.Text.Split(Environment.NewLine, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                config.Headers.Clear();
                foreach (var line in lines)
                {
                    var spl = line.Split(':', 2, StringSplitOptions.TrimEntries);
                    if (spl.Length == 2)
                    {
                        if (!string.IsNullOrWhiteSpace(spl[0]) && !string.IsNullOrWhiteSpace(spl[1]))
                        {
                            config.Headers[spl[0]] = spl[1];
                        }
                    }
                }
                if (shouldRefresh)
                    RefreshItems();
            }
        }

        private void any_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            UpdateValue(sender == txName);
        }

        private void cbMethod_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateValue();
        }

        private void cxActive_Unchecked(object sender, RoutedEventArgs e)
        {
            UpdateValue();
        }

        private void cxActive_Checked(object sender, RoutedEventArgs e)
        {
            UpdateValue();
        }

        private void any_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateValue(sender == txName);
        }
    }
}
