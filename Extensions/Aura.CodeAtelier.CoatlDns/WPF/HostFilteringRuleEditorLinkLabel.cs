﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Controls;
using Aura.CodeAtelier.Configuration;

namespace Aura.CodeAtelier.WPF
{
    public class HostFilteringRuleEditorLinkLabel : TextBlock, IAutoConfigUserControlWithLabel
    {
        HostFilteringRule? FilteringRule { get; set; }

        public HostFilteringRuleEditorLinkLabel()
        {
            Margin = new Thickness(50, 0, 0, 0);

            var hp = new Hyperlink();
            hp.Inlines.Add("Manage Filter");
            hp.Click += Hp_Click;
            Inlines.Add(hp);
        }

        public event EventHandler? ValueChanged;

        private void Hp_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Coming soon");
            ValueChanged?.Invoke(this, EventArgs.Empty);
        }

        public void LoadConfigurationValue(object? value)
        {
            FilteringRule = value as HostFilteringRule;
        }

        public string GetLabel()
        {
            if (FilteringRule == null || FilteringRule.Rule == Configuration.FilteringRule.AllowAll)
            {
                return "Allow All";
            }
            return FilteringRule.Rule.ToString() + "ed: " + (FilteringRule.Rule == Configuration.FilteringRule.Whitelist ? FilteringRule.WhitelistedHosts.Count + " host(s)" : FilteringRule.BlacklistedHosts.Count + " host(s)");
        }

        public object? GetConfigurationValue()
        {
            return FilteringRule;
        }
    }
}
