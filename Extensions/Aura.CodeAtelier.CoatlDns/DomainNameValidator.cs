﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public class DomainNameValidator : IAutoConfigValidator
    {
        public object? Format(object? value)
        {
            if (value is string name)
            { 
                return name.Trim();
            }
            return null;
        }

        public bool Validate(object? value, object? configurationObject)
        {
            if (value is string name)
            {
                return DomainName.TryParse(name, out _);
            }
            return false;
        }
    }
}
