﻿using AliceMediaLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary
{
    public static class AliceMediaLibraryStatic
    {
        public static Version Version { get; } = new Version(4, 0, 7);

        public const string ThemePath = "services/Aura.AliceMediaLibrary/bin/theme";
        public const string DataPath = "data/alicemedialibrary/";

        public const string CoverPath = "covers/";
        public const string BackdropPath = "backdrops/";
        public const string ThumbnailPath = "thumbnails/";

        public static string GetCoverPath(string metaStoragePath, Content content)
        {
            var id = "c-" + content.Id;
            return System.IO.Path.Combine(metaStoragePath, CoverPath, id + ".png");
        }

        public static string GetBackdropPath(string metaStoragePath, Content content)
        {
            var id = "c-" + content.Id;
            return System.IO.Path.Combine(metaStoragePath, BackdropPath, id + ".png");
        }

        public static string GetThumbnailPath(string metaStoragePath, Media content)
        {
            var id = "m-" + content.Id;
            return System.IO.Path.Combine(metaStoragePath, ThumbnailPath, id + ".png");
        }
    }
}
