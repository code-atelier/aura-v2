﻿using AliceMediaLibrary.InfoFetcher;
using AliceMediaLibrary.Models;
using Amaterasu.Database.MySQL;
using Aura;
using Aura.Services;
using Aura.Services.Passive;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AliceMediaLibrary
{
    public partial class AliceMediaLibraryService : PassiveServiceBase, IServiceWithConfiguration
    {
        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = "coatl.alicemedialibrary",
            Name = "Alice Media Library",
            Author = "CodeAtelier",
            Description = "Service to manage your media library and sync it across devices",
            Version = AliceMediaLibraryStatic.Version,
            Icon = Util.CreateResourceUri("res/img/icon.png"),
        };

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.Configure | ServiceUserAction.Stop | ServiceUserAction.Start | ServiceUserAction.Restart;

        public AliceMediaLibraryConfiguration Configuration { get; }

        private ConfigurationService ConfigurationService { get; }

        private IServiceMessageBus ServiceMessageBus { get; }

        internal MySQLDatabaseContext DB { get; }

        public List<IMediaInfoFetcher> MediaInfoFetchers { get; } = new List<IMediaInfoFetcher>();

        public List<IMediaInfoRefiner> MediaInfoRefiners { get; } = new List<IMediaInfoRefiner>();

        public AliceMediaLibraryService(IServiceMessageBus serviceMessageBus, ConfigurationService configurationService)
        {
            Configuration = configurationService.LoadConfig<AliceMediaLibraryConfiguration>();
            if (Configuration.Theme == null)
            {
                Configuration.Theme = "default.xaml";
            }
            ServiceMessageBus = serviceMessageBus;
            ConfigurationService = configurationService;
            DB = new MySQLDatabaseContext();
        }
        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                MediaInfoFetchers.Add(new EHentaiInfoFetcher(this));
                MediaInfoFetchers.Add(new GeneralInfoFetcher());
                MediaInfoFetchers.Add(new AML3InfoFetcher());

                MediaInfoRefiners.Add(new TagRefiner());
                MediaInfoRefiners.Add(new TitleRefiner());

                if (!Directory.Exists(AliceMediaLibraryStatic.DataPath))
                {
                    try
                    {
                        Directory.CreateDirectory(AliceMediaLibraryStatic.DataPath);
                    }
                    catch { }
                }
            });
        }

        public override async Task OnStart()
        {
            await Task.Run(() =>
            {
                DB.Credential.Host = Configuration.Database.Host;
                DB.Credential.Port = Configuration.Database.Port;
                DB.Credential.User = Configuration.Database.Username;
                DB.Credential.Password = Configuration.Database.Password;
                DB.Credential.Database = Configuration.Database.Database;
                DB.Open();
            });
        }

        public override async Task OnShutdown()
        {
            await Task.Run(() =>
            {
                DB.Close();
            });
        }

        public Window CreateConfigurationWindow()
        {
            var ac = new AutoConfigurator();
            ac.LoadConfiguration(Configuration);
            ac.Closed += Ac_Closed;
            return ac;
        }

        private void Ac_Closed(object? sender, EventArgs e)
        {
            try
            {
                ConfigurationService.SaveConfig<AliceMediaLibraryConfiguration>();
            }
            catch { }
        }
    }
}
