﻿using AliceMediaLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary
{
    public interface IMediaInfoFetcher
    {
        Guid Id { get; }

        string Name { get; }

        byte Priority { get; }

        bool DefaultIsActive { get; }

        Task<MediaInfoFetchResult?> FetchInfo(string fullPath);
    }

    public interface IMediaInfoRefiner
    {
        Guid Id { get; }

        string Name { get; }

        byte Priority { get; }

        bool DefaultIsActive { get; }

        void Refine(MediaInfoFetchResult fetchResult);
    }

    public class MediaInfoFetchResult
    {
        public string Title { get; set; } = string.Empty;

        public string OriginalTitle { get; set; } = string.Empty;

        public string Summary { get; set; } = string.Empty;

        public float? OnlineRating { get; set; }

        public bool IsExplicit { get; set; }

        public ContentRating? ContentRating { get; set; }

        public List<string> Tags { get; set; } = new List<string>();

        public string? ThumbnailUri { get; set; }
    }
}
