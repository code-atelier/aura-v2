﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AliceMediaLibrary.Models;
using System.Text.RegularExpressions;
using Ubiety.Dns.Core.Records.NotUsed;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows;

namespace AliceMediaLibrary
{
    public static class AMLHelper
    {
        public static Dictionary<ContentMediaType, string[]> SupportedMediaExtensions { get; } = new Dictionary<ContentMediaType, string[]>()
        {
            { ContentMediaType.Image, new string[] { "*.jpg", "*.jpeg", "*.png", "*.bmp", "*.tga", "*.webp", "*.gif", "*.pdf", "*.zip" } },
            { ContentMediaType.EBook, new string[] { "*.jpg", "*.jpeg", "*.png", "*.bmp", "*.tga", "*.webp", "*.gif", "*.pdf", "*.docx", "*.doc", "*.zip" } },
            { ContentMediaType.Audio, new string[] { "*.mp3", "*.flac", "*.wav", "*.ogg", "*.m4a" } },
            { ContentMediaType.Video, new string[] { "*.mp4", "*.avi", "*.mpg", "*.mkv", "*.webm", "*.3gp", "*.ts" } },
            { ContentMediaType.Game, new string[] { "*.exe", "*.swf" } },
            { ContentMediaType.Application, new string[] { "*.exe" } },
            { ContentMediaType.Other, new string[] { "*.*" } },
        };

        public static string[] ImageExtensions { get; } = new string[] {
                ".png", ".jpg", ".jpeg", ".bmp", ".gif", ".tga", ".webp"
            };

        public static Color? ParseColor(string color)
        {
            if (color.Length != 6)
            {
                return null;
            }
            try
            {
                byte r = byte.Parse(color.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                byte g = byte.Parse(color.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
                byte b = byte.Parse(color.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
                return Color.FromArgb(255, r, g, b);
            }
            catch
            {
            }
            return null;
        }

        public static Color? GenerateForegroundColor(Color? backgroundColor, Color darkForeground, Color lightForeground)
        {
            if (backgroundColor == null) return Colors.White;
            double sum = backgroundColor.Value.R + backgroundColor.Value.G + backgroundColor.Value.B;
            sum /= 3.0;

            return sum < 160 ? lightForeground : darkForeground;
        }

        public static string[]? ParseTag(string tag)
        {
            try
            {
                tag = tag.ToLower().Trim();
                string pattern = @"^(?>([a-z0-9][a-z0-9_\(\)]*)\:)?([a-z0-9][a-z0-9_\(\)]*)$";
                var match = Regex.Match(tag, pattern);
                if (match.Success)
                {
                    if (match.Groups[1].Success)
                    {
                        return new string[] { match.Groups[1].Value, match.Groups[2].Value };
                    }
                    else
                    {
                        return new string[] { match.Groups[2].Value };
                    }
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static string SanitizePath(string path)
        {
            return path.Trim().TrimEnd('\\').TrimEnd('/').Replace('\\', '/');
        }

        public static bool IsImageFile(string path)
        {
            var ext = Path.GetExtension(path)?.ToLower() ?? "";
            return ImageExtensions.Contains(ext);
        }

        public static BitmapImage? GetBitmap(string path)
        {
            try
            {
                var buffer = File.ReadAllBytes(path);
                using (var ms = new MemoryStream(buffer))
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    var bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.CacheOption = BitmapCacheOption.OnLoad;
                    bmp.StreamSource = ms;
                    bmp.EndInit();
                    return bmp;
                }
            }
            catch { }
            return null;
        }

        public static BitmapImage? CropBitmap(BitmapImage srcImage, int width, int height, Rect srcRect)
        {
            try
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(srcImage));
                System.Drawing.Bitmap bmp;
                System.Drawing.Bitmap bmpOut = new System.Drawing.Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                using (var ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    bmp = new System.Drawing.Bitmap(ms);
                }
                using (bmp)
                using (bmpOut)
                {
                    using (var gOut = System.Drawing.Graphics.FromImage(bmpOut))
                    {
                        gOut.DrawImage(
                            bmp,
                            new System.Drawing.Rectangle(0, 0, bmpOut.Width, bmpOut.Height),
                            new System.Drawing.Rectangle((int)srcRect.X, (int)srcRect.Y, (int)srcRect.Width, (int)srcRect.Height),
                            System.Drawing.GraphicsUnit.Pixel
                        );
                    }
                    using (var ms = new MemoryStream())
                    {
                        bmpOut.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        ms.Seek(0, SeekOrigin.Begin);
                        var ret = new BitmapImage();
                        ret.BeginInit();
                        ret.CacheOption = BitmapCacheOption.OnLoad;
                        ret.StreamSource = ms;
                        ret.EndInit();
                        return ret;
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        internal static BitmapImage GetBitmap(BitmapSource bitmapSource)
        {
            var encoder = new PngBitmapEncoder();
            using(var ms = new MemoryStream())
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                encoder.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);

                var bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.CacheOption = BitmapCacheOption.OnLoad;
                bmp.StreamSource = ms;
                bmp.EndInit();
                return bmp;
            }
        }
    }
}
