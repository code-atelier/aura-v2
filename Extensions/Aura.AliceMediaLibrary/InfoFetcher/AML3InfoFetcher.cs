﻿using AliceMediaLibrary.Models.AML3;
using Amaterasu.Database.Access;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AliceMediaLibrary.InfoFetcher
{
    public class AML3InfoFetcher : IMediaInfoFetcher
    {
        public Guid Id { get; } = Guid.Parse("a704fd79-5f02-43d4-aeb5-c15538788324");

        public string Name { get; } = "Alice Media Library v3 Info Fetcher";

        public byte Priority { get; } = 128;

        private List<AML3Media> MediaDatabase { get; } = new List<AML3Media>();

        public bool DefaultIsActive { get; } = true;

        public AML3InfoFetcher()
        {
            try
            {
                var path = Path.Combine(AliceMediaLibraryStatic.DataPath, "aml3.xml");
                if (File.Exists(path))
                {
                    var serializer = new XmlSerializer(typeof(AML3MediaXmlWrapper));
                    using (var fi = File.OpenRead(path))
                    {
                        var o = serializer.Deserialize(fi);
                        if (o is AML3MediaXmlWrapper lib)
                        {
                            MediaDatabase = lib.Medias;
                        }
                    }
                }
            }
            catch { }
        }

        public async Task<MediaInfoFetchResult?> FetchInfo(string fullPath)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var res = new MediaInfoFetchResult();
                    var path = fullPath.TrimEnd('/').TrimEnd('\\').Replace('/', '\\').Trim().Substring(2);
                    var media = MediaDatabase.Where(x => x.Path?.ToLower().Substring(2) == path.ToLower()).FirstOrDefault();
                    if (media != null)
                    {
                        if (!string.IsNullOrWhiteSpace(media.Title))
                            res.Title = media.Title;
                        if (!string.IsNullOrWhiteSpace(media.OriginalTitle))
                            res.OriginalTitle = media.OriginalTitle;
                        if (media.Rating != null && media.Rating > 0)
                        {
                            res.OnlineRating = media.Rating.Value * 2;
                        }
                        if (!string.IsNullOrWhiteSpace(media.Tags))
                        {
                            var tags = media.Tags.Split('|', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
                            foreach (var tag in tags)
                            {
                                var t = tag.Trim().ToLower().Replace(' ', '_');
                                if (AMLHelper.ParseTag(t) != null)
                                {
                                    res.Tags.Add(t);
                                }
                            }
                        }
                        return res;
                    }
                }
                catch { }
                return null;
            });
        }
    }
}
