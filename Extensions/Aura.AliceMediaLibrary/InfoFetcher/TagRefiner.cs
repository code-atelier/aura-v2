﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AliceMediaLibrary.InfoFetcher
{
    public class TagRefiner : IMediaInfoRefiner
    {
        public Guid Id { get; } = Guid.Parse("5418cef8-1d98-4aff-bee6-22427fe3ca7f");

        public string Name { get; } = "Tag Refiner";

        public bool DefaultIsActive { get; } = true;

        public byte Priority { get; } = 128;

        public void Refine(MediaInfoFetchResult fetchResult)
        {
            var datapath = Path.Combine(AliceMediaLibraryStatic.DataPath, "tag_refiner.json");
            var keypath = Path.Combine(AliceMediaLibraryStatic.DataPath, "key_refiner.json");
            try
            {
                if (!File.Exists(datapath))
                {
                    File.WriteAllText(datapath, "{}");
                }
                if (!File.Exists(keypath))
                {
                    File.WriteAllText(keypath, "{}");
                }
                if (File.Exists(datapath) && File.Exists(keypath))
                {
                    var json = File.ReadAllText(datapath);
                    var dic = JsonSerializer.Deserialize<Dictionary<string, string>>(json);
                    json = File.ReadAllText(keypath);
                    var kdic = JsonSerializer.Deserialize<Dictionary<string, string>>(json);
                    if (dic != null)
                    {
                        var tags = fetchResult.Tags.ToList();
                        fetchResult.Tags.Clear();
                        foreach (var tag in tags)
                        {
                            var t = tag;
                            if (dic.ContainsKey(t))
                            {
                                t = dic[tag];
                            }
                            var tp = AMLHelper.ParseTag(t);
                            if (tp != null)
                            {
                                if (kdic != null)
                                {
                                    if (tp.Length == 2)
                                    {
                                        if (kdic.ContainsKey(tp[0]))
                                        {
                                            t = kdic[tp[0]] + ":" + tp[1];
                                        }
                                    }
                                }
                                fetchResult.Tags.Add(t);
                            }
                        }
                    }
                }
            }
            catch { }
        }
    }
}
