﻿using AliceMediaLibrary.WPF;
using Aura;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AliceMediaLibrary.InfoFetcher
{
    public class EHentaiInfoFetcher : IMediaInfoFetcher
    {
        public Guid Id { get; } = Guid.Parse("ca6bf65f-392c-4a23-a98e-fbead0df0a5f");

        public string Name { get; } = "E-H Info Fetcher";

        public byte Priority { get; } = 255;

        public bool DefaultIsActive { get; } = false;

        AliceMediaLibraryService Service { get; }

        public EHentaiInfoFetcher(AliceMediaLibraryService service)
        {
            Service = service;
        }

        public async Task<MediaInfoFetchResult?> FetchInfo(string fullPath)
        {
            try
            {
                if (File.Exists(fullPath))
                {
                    var fname = Path.GetFileNameWithoutExtension(fullPath);
                    fullPath = await GetGallery(fname) ?? fullPath;
                }
                else if (Directory.Exists(fullPath))
                {
                    var fname = Path.GetFileName(fullPath.TrimEnd('/').TrimEnd('\\'));
                    fullPath = await GetGallery(fname) ?? fullPath;
                }

                string pattern = @"^(https\:\/\/(?>exhentai|e-hentai)\.org)\/g\/(\w+)\/(\w+)\/?$";

                Match m = Regex.Match(fullPath, pattern);
                if (m.Success)
                {
                    var host = m.Groups[1].Value.Replace("e-hentai", "api.e-hentai").Replace("exhentai", "s.exhentai");
                    var galleryId = m.Groups[2].Value;
                    var galleryToken = m.Groups[3].Value;
                    var data = new EHentaiGalleryRequest();
                    data.AddGallery(galleryId, galleryToken);
                    var client = new RestClient(host);
                    var req = new RestRequest("api.php", Method.Post);
                    req.AddJsonBody(data);
                    var res = await client.PostAsync(req);
                    if (res.IsSuccessStatusCode && res.Content != null)
                    {
                        var metadata = JsonSerializer.Deserialize<EHentaiGalleryResponse>(res.Content);
                        if (metadata != null && metadata.Metadatas.Count > 0 && metadata.Metadatas.First() is EHentaiGalleryMetadata g)
                        {
                            return new MediaInfoFetchResult()
                            {
                                OnlineRating = float.TryParse(g.Rating, out float rating) ? rating * 2 : null,
                                OriginalTitle = WebUtility.HtmlDecode(g.OriginalTitle),
                                Title = WebUtility.HtmlDecode(g.Title),
                                Tags = g.Tags.Select(x => x.Replace(' ', '_')).ToList(),
                                ThumbnailUri = g.Thumbnail,
                                IsExplicit = true,
                                ContentRating = Models.ContentRating.AdultOnly,
                            };
                        }
                    }
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        private async Task<string?> GetGallery(string title)
        {
            try
            {
                string pattern = @"\s*(?>\([^\)]+\)|\[[^\]]+\]|\{[^\}]+\})\s*";
                title = Regex.Replace(title, pattern, "").Trim();
                pattern = @"\~|,|\.|\-|\+|\!";
                title = Regex.Replace(title, pattern, " ").Trim();
                var spl = title.ToLower().Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries).Where(x => x.Length > 2).Take(5);
                var uri = "https://e-hentai.org/?f_search=" + string.Join("+", spl);
                var links = new Dictionary<string, string>();
                using (var client = new HttpClient())
                {
                    var resp = await client.GetStringAsync(uri);
                    if (resp != null && !resp.ToLower().Contains("no hits found"))
                    {
                        pattern = @"<a[^\>]*?href=""(https:\/\/e[-x]hentai\.org\/g\/[a-zA-Z0-9]+\/[a-zA-Z0-9]+)\/?[^\>]*?>\s*<div[^\>]*?class=""glink""[^\>]*?>\s*([^\<]+)\s*<\/div";
                        Regex regex = new Regex(pattern, RegexOptions.Singleline);
                        var matches = regex.Matches(resp);
                        foreach (Match match in matches)
                        {
                            if (match.Success && match.Groups.Count > 1)
                            {
                                try
                                {
                                    var gl = match.Groups[1].Value;
                                    var gt = WebUtility.HtmlDecode(match.Groups[2].Value);
                                    links[gt] = gl;
                                }
                                catch { }
                            }
                        }
                    }
                }
                
                try
                {
                    string? res = null;
                    ServiceManager.MainDispatcher?.Invoke(() =>
                    {
                        var f = new EHLinkSelector(Service);
                        f.LoadOptions(links);
                        if (f.ShowDialog() == true && f.Result != null)
                        {
                            res = f.Result;
                        }
                    });
                    if (res != null) return res;
                }
                catch { }
                return null;
            }
            catch { }
            return null;
        }
    }

    public class EHentaiGalleryRequest
    {
        [JsonPropertyName("method")]
        public string Method { get; set; } = "gdata";

        [JsonPropertyName("namespace")]
        public int Namespace { get; set; } = 1;

        [JsonPropertyName("gidlist")]
        public List<string[]> Galleries { get; set; } = new List<string[]>();

        public void AddGallery(string id, string token)
        {
            Galleries.Add(new string[] { id, token });
        }
    }

    public class EHentaiGalleryResponse
    {
        [JsonPropertyName("gmetadata")]
        public List<EHentaiGalleryMetadata> Metadatas { get; set; } = new List<EHentaiGalleryMetadata>();
    }

    public class EHentaiGalleryMetadata
    {
        [JsonPropertyName("gid")]
        public int Id { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; } = string.Empty;

        [JsonPropertyName("title")]
        public string Title { get; set; } = string.Empty;

        [JsonPropertyName("title_jpn")]
        public string OriginalTitle { get; set; } = string.Empty;

        [JsonPropertyName("thumb")]
        public string Thumbnail { get; set; } = string.Empty;

        [JsonPropertyName("rating")]
        public string Rating { get; set; } = string.Empty;

        [JsonPropertyName("tags")]
        public List<string> Tags { get; set; } = new List<string>();
    }
}
