﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AliceMediaLibrary.InfoFetcher
{
    public class TitleRefiner : IMediaInfoRefiner
    {
        public Guid Id { get; } = Guid.Parse("2932161f-0f7d-4e05-acaf-4ca2324f8d8d");

        public string Name { get; } = "Title Refiner";

        public bool DefaultIsActive { get; } = true;

        public byte Priority { get; } = 128;

        public void Refine(MediaInfoFetchResult fetchResult)
        {
            string pattern = @"\s*(?>\([^\)]+\)|\[[^\]]+\]|\{[^\}]+\})\s*";
            try
            {
                fetchResult.Title = Regex.Replace(fetchResult.Title, pattern, "").Trim();
                fetchResult.OriginalTitle = Regex.Replace(fetchResult.OriginalTitle, pattern, "").Trim();
            }
            catch { }
        }
    }
}
