﻿using ControlzEx.Theming;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AliceMediaLibrary.InfoFetcher
{
    public class GeneralInfoFetcher : IMediaInfoFetcher
    {
        public Guid Id { get; } = Guid.Parse("09e353b2-fc97-4eaf-afb3-2ec0616de07f");

        public string Name { get; } = "General Info Fetcher";

        public byte Priority { get; } = 128;

        public bool DefaultIsActive { get; } = true;

        const string pattern = @"(?>\(.*\)\s*)?(?>[\[「](?'group'[^\]]+)[\]」])?(?>\s*(?'otitle'[^\(\[\{]+)\s*\|)?\s*(?'title'[^\(^\[^\{]+)\s*(?>\((?'parody'([^\(\[\{]+))\))?";

        public async Task<MediaInfoFetchResult?> FetchInfo(string fullPath)
        {
            return await Task.Run(() =>
            {
                try
                {

                    var res = new MediaInfoFetchResult();
                    var path = fullPath.TrimEnd('/').TrimEnd('\\');
                    var fName = Path.GetFileNameWithoutExtension(path);
                    RegexOptions options = RegexOptions.Multiline;
                    Match? m = Regex.Matches(fName, pattern, options)?.FirstOrDefault();
                    if (m != null)
                    {
                        foreach (Group g in m.Groups)
                        {
                            if (g.Success)
                            {
                                if (g.Name == "otitle")
                                    res.OriginalTitle = g.Value;
                                if (g.Name == "title")
                                    res.Title = g.Value;
                                if (g.Name == "group")
                                {
                                    var tag = "group:" + g.Value.Trim().ToLower().Replace(' ', '_');
                                    if (AMLHelper.ParseTag(tag) != null)
                                    {
                                        res.Tags.Add(tag);
                                    }
                                }
                                if (g.Name == "parody")
                                {
                                    var tag = "parody:" + g.Value.Trim().ToLower().Replace(' ', '_');
                                    if (AMLHelper.ParseTag(tag) != null)
                                    {
                                        res.Tags.Add(tag);
                                    }
                                }
                            }
                        }
                    }

                    try
                    {
                        var datapath = Path.Combine(AliceMediaLibraryStatic.DataPath, "title_tag.json");
                        if (!File.Exists(datapath))
                        {
                            File.WriteAllText(datapath, "{}");
                        }
                        var json = File.ReadAllText(datapath);
                        var tagConvert = JsonSerializer.Deserialize<Dictionary<string, string>>(json);
                        if (tagConvert != null)
                        {
                            foreach(var tc in tagConvert)
                            {
                                if (fName.ToLower().Contains(tc.Key))
                                {
                                    var tag = tc.Value.ToLower().Trim().Replace(' ', '_');
                                    var p = AMLHelper.ParseTag(tag);
                                    if (p != null)
                                        res.Tags.Add(tag);
                                }
                            }
                        }
                    }
                    catch { }

                    return res;
                }
                catch { }
                return (MediaInfoFetchResult?)null;
            });
        }
    }
}
