﻿using Aura;
using Org.BouncyCastle.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AliceMediaLibrary.Repositories
{
    public class ThemeRepository : IAutoConfigItemSource
    {
        private Dictionary<string, object?> Items { get; } = new Dictionary<string, object?>();

        public ThemeRepository() 
        {
            if (Directory.Exists(AliceMediaLibraryStatic.ThemePath))
            {
                var xamls = Directory.EnumerateFiles(AliceMediaLibraryStatic.ThemePath, "*.xaml");
                foreach (var xaml in xamls)
                {
                    Items.Add(Path.GetFileNameWithoutExtension(xaml), Path.GetFileName(xaml));
                }
            }
        }

        public object? this[string key] { get => Items[key]; set => Items[key] = value; }

        public IEnumerator<KeyValuePair<string, object?>> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }
    }
}
