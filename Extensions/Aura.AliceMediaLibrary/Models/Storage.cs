﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [Model("storages")]
    public class Storage : IPreSave
    {
        [PrimaryKey]
        public string Id { get; set; } = string.Empty;

        public string Name { get; set; } = string.Empty;

        public string BasePath { get; set; } = string.Empty;

        public StorageKind Kind { get; set; } = StorageKind.FileSystem;

        public bool IsMetaStorage { get; set; }

        public void OnPreSave(IDatabaseContext db, bool isUpdate)
        {
            if (string.IsNullOrWhiteSpace(Id))
            {
                Id = Guid.NewGuid().ToString("n");
            }
        }

        public override string ToString()
        {
            return Name + " (" + BasePath + ")";
        }
    }

    public enum StorageKind : int
    {
        FileSystem = 0,
        Uri = 1,
    }
}
