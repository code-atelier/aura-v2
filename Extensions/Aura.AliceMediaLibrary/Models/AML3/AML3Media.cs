﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AliceMediaLibrary.Models.AML3
{
    public class AML3Media
    {
        public string? Id { get; set; }

        public string Title { get; set; } = string.Empty;

        public string OriginalTitle { get; set; } = string.Empty;

        public string Summary { get; set; } = string.Empty;

        public float? Rating { get; set; }

        public string Path { get; set; } = string.Empty;

        public string Tags { get; set; } = string.Empty;
    }
}
