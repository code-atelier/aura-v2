﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AliceMediaLibrary.Models.AML3
{
    [XmlRoot("dataroot")]
    public class AML3MediaXmlWrapper
    {
        [XmlElement("Media")]
        public List<AML3Media> Medias { get; set; } = new List<AML3Media>();
    }
}
