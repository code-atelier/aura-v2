﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [Model("content_tags")]
    public class ContentTags
    {
        public string? ContentId { get; set; }

        public string Tag { get; set; } = string.Empty;
    }
}
