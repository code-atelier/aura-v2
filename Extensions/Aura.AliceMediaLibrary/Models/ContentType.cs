﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [Model("content_types")]
    public class ContentType
    {
        [PrimaryKey]
        public int? Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public string Color { get; set; } = string.Empty;

        public int Index { get; set; }

        public ContentMediaType MediaType { get; set; }

        /// <summary>
        /// When set, use a custom media editor to edit this kind of content.
        /// </summary>
        public string? MediaEditorId { get; set; }

        /// <summary>
        /// When set, use a custom media player to play this kind of content.
        /// </summary>
        public string? MediaPlayerId { get; set; }

        /// <summary>
        /// When set, use a custom media info fetcher to get information of this kind of content.
        /// </summary>
        public string? MediaInfoFetcherId { get; set; }

        /// <summary>
        /// When true, this content has multiple medias inside.
        /// </summary>
        public bool IsCollection { get; set; }

        public string? Fetchers { get; set; }

        public string? Refiners { get; set; }

        [IgnoreMember]
        public Dictionary<string, bool> FetcherSettings
        {
            get
            {
                try
                {
                    return JsonSerializer.Deserialize<Dictionary<string, bool>>(Fetchers ?? "{}") ?? new Dictionary<string, bool>();
                }
                catch { }
                return new Dictionary<string, bool>();
            }
        }

        [IgnoreMember]
        public Dictionary<string, bool> RefinerSettings
        {
            get
            {
                try
                {
                    return JsonSerializer.Deserialize<Dictionary<string, bool>>(Refiners ?? "{}") ?? new Dictionary<string, bool>();
                }
                catch { }
                return new Dictionary<string, bool>();
            }
        }
    }

    public enum ContentMediaType
    {
        /// <summary>
        /// Other file-based media
        /// </summary>
        Other = 0,

        /// <summary>
        /// Images: folder of images, archive
        /// </summary>
        Image = 1,

        /// <summary>
        /// Pdf Docs: pdf, doc, ppt, xls, folder of images, archive
        /// </summary>
        EBook = 2,

        /// <summary>
        /// Audio: mp3, wav, ogg, folder of audios, archive
        /// </summary>
        Audio = 3,

        /// <summary>
        /// Video: 3gp, mpg, mp4, mkv, webm, ts
        /// </summary>
        Video = 4,

        /// <summary>
        /// Apps: exe, archive
        /// </summary>
        Application = 5,

        /// <summary>
        /// Apps: exe, archive
        /// </summary>
        Game = 6,
    }
}
