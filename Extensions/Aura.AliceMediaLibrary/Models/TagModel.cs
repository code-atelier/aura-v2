﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [Model("tags")]
    public class TagModel : IPreSave
    {
        [PrimaryKey]
        public string Tag { get; set; } = string.Empty;

        public string? Key { get; set; }

        public string Value { get; set; } = string.Empty;

        public int Priority { get; set; } = 0;

        public int Count { get; set; }

        public bool IsExplicit { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public void OnPreSave(IDatabaseContext db, bool isUpdate)
        {
            if (!isUpdate)
                CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }
    }

    [Model("content_tags")]
    public class ContentTag
    {
        public string ContentId { get; set; } = string.Empty;

        public string Tag { get; set; } = string.Empty;
    }
}
