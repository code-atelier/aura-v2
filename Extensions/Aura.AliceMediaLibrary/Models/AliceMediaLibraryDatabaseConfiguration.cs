﻿using AliceMediaLibrary.Repositories;
using Amaterasu.Database.MySQL;
using Aura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows;

namespace AliceMediaLibrary.Models
{
    public class AliceMediaLibraryDatabaseConfiguration
    {
        [JsonPropertyName("host")]
        [AutoConfigMember("Host",
            Description = "The host computer where the database server resides",
            Kind = AutoConfigPropertyKind.Textbox,
            //Icon = "res/img/bubbles-alt.png",
            Index = 0
            )]
        public string Host { get; set; } = "127.0.0.1";

        [JsonPropertyName("port")]
        [AutoConfigMember("Port",
            Description = "The internet protocol port that the database server is listening to",
            Kind = AutoConfigPropertyKind.IntegerTextbox,
            //Icon = "res/img/bubbles-alt.png",
            Index = 1,
            NumericMinValue = 1,
            NumericMaxValue = 65535
            )]
        public int Port { get; set; } = 3306;

        [JsonPropertyName("user")]
        [AutoConfigMember("Username",
            Description = "The user name to login",
            Kind = AutoConfigPropertyKind.Textbox,
            //Icon = "res/img/bubbles-alt.png",
            Index = 2
            )]
        public string Username { get; set; } = "root";

        [JsonPropertyName("pass")]
        [AutoConfigMember("Password",
            Description = "The password to login",
            Kind = AutoConfigPropertyKind.Textbox,
            //Icon = "res/img/bubbles-alt.png",
            Index = 3
            )]
        public string Password { get; set; } = "";

        [JsonPropertyName("name")]
        [AutoConfigMember("Database",
            Description = "The database name to use",
            Kind = AutoConfigPropertyKind.Textbox,
            //Icon = "res/img/bubbles-alt.png",
            Index = 4
            )]
        public string Database { get; set; } = "alicemedialibrary";

        [AutoConfigMember("Test Connection",
            Index = 101,
            //Icon = "res/img/folder.png",
            MethodLabel = "Connect",
            Description = "Test your database configuration and try to connect"
        )]
        public void GoToRootDir()
        {
            try
            {
                var db = new MySQLDatabaseContext();
                db.Credential.Database = Database;
                db.Credential.Password = Password;
                db.Credential.User = Username;
                db.Credential.Port = Port;
                db.Credential.Host = Host;
                db.Open();
                db.Close();
                MessageBox.Show("Connection test is successful!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connection test has failed!" + Environment.NewLine + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
