﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [Model("media_collections")]
    public class MediaCollection
    {
        [PrimaryKey]
        public string? Id { get; set; }

        public string? ContentId { get; set; }

        #region Descriptor
        public string Name { get; set; } = string.Empty;

        public int Index { get; set; }
        #endregion
    }
}
