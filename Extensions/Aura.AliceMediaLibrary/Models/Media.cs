﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [Model("medias")]
    public class Media: ContentBase, IPreSave
    {
        [PrimaryKey]
        public string? Id { get; set; }

        public string? ContentId { get; set; }

        public string? MediaCollectionId { get; set; }

        public int? Episode { get; set; }

        #region Storage
        public string? StorageId { get; set; }

        public string Path { get; set; } = string.Empty;

        public bool IsDirectory { get; set; }

        public bool IsArchive { get; set; }
        #endregion

        #region Lazy Loads
        [IgnoreMember]
        public Storage? Storage { get; set; }

        [IgnoreMember]
        public Content? Content { get; set; }

        [IgnoreMember]
        public MediaCollection? MediaCollection { get; set; }

        [IgnoreMember]
        public List<string> Tags { get; } = new List<string>();

        public void OnPreSave(IDatabaseContext db, bool isInsert)
        {
            if (string.IsNullOrWhiteSpace(Id))
            {
                Id = Guid.NewGuid().ToString("n");
                CreatedAt = DateTime.UtcNow;
            }
            UpdatedAt = DateTime.UtcNow;
        }
        #endregion
    }
}
