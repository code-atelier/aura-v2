﻿using AliceMediaLibrary.Repositories;
using Aura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [AuraConfiguration("alicemedialibrary.json")]
    [AutoConfig("Alice Media Library")]
    public class AliceMediaLibraryConfiguration
    {
        [JsonPropertyName("theme")]
        [AutoConfigMember("Theme",
            Description = "The theme for AliceMediaLibrary",
            Kind = AutoConfigPropertyKind.DropDown,
            //Icon = "res/img/bubbles-alt.png",
            Index = 0,
            ItemSource = typeof(ThemeRepository)
            )]
        public string? Theme { get; set; }

        [JsonPropertyName("database")]
        [AutoConfigMember("Database Connection",
            Description = "Configure connection to MySQL database",
            //Icon = "res/img/headphone.png",
            Index = 1,
            Kind = AutoConfigPropertyKind.Page)]
        public AliceMediaLibraryDatabaseConfiguration Database { get; set; } = new AliceMediaLibraryDatabaseConfiguration();
    }
}
