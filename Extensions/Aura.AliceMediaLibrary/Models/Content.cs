﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [Model("contents")]
    public class Content : ContentBase, IPreSave, IPostSave
    {
        [PrimaryKey]
        public string? Id { get; set; }

        public int ContentTypeId { get; set; }

        #region Meta
        public DateTime? AirEndDate { get; set; }

        public bool FetchTagFromContent { get; set; } = true;

        public string SearchTerm { get; set; } = string.Empty; // max 500

        public bool FromAutoScan { get; set; }

        public DateTime? LastAccess { get; set; }
        #endregion

        #region Lazy Loads
        [IgnoreMember]
        public ContentType? ContentType { get; set; }

        [IgnoreMember]
        public List<Media> Medias { get; } = new List<Media>();

        [IgnoreMember]
        public Media? NonCollectionContentMedia { get; set; }

        [IgnoreMember]
        public List<string> Tags { get; } = new List<string>();
        #endregion

        public void BuildSearchTerm(AliceMediaLibraryService service)
        {
            var terms = new List<string>();
            if (Tags.Count == 0) LoadTags(service);
            foreach (var tag in Tags)
            {
                terms.Add("$" + tag);
            }
            SearchTerm = "|";

            foreach (var term in terms)
            {
                var append = term + "|";
                if (SearchTerm.Length + append.Length > 5000)
                {
                    break;
                }
                SearchTerm += append;
            }
        }

        public List<TagModel> LoadTags(AliceMediaLibraryService service)
        {
            try
            {
                Tags.Clear();
                var tags = service.DB.From<ContentTag>().Where(x => x.ContentId == Id).ToList();
                Tags.AddRange(tags.Select(x => x.Tag));
                if (Tags.Count > 0)
                    return service.DB.From<TagModel>().Where(x => Tags.Contains(x.Tag)).OrderBy(x => x.Priority, QuerySortDirection.Descending).OrderBy(x => x.Tag, QuerySortDirection.Descending).ToList();
            }
            catch { }
            return new List<TagModel>();
        }

        public void UpdateTags(AliceMediaLibraryService service, List<string> tags)
        {
            var loadedTagModels = LoadTags(service);
            var existing = Tags.ToList();
            var combined = new List<string>();
            combined.AddRange(tags);
            combined.AddRange(existing);
            combined = combined.Distinct().ToList();

            Tags.Clear();
            foreach (var tag in combined)
            {
                try
                {
                    if (tags.Contains(tag))
                    {
                        if (AMLHelper.ParseTag(tag) is string[] tagParts)
                        {
                            if (!existing.Contains(tag))
                            {
                                var tm = loadedTagModels.FirstOrDefault(x => x.Tag == tag) ?? service.DB.From<TagModel>().Where(x => x.Tag == tag).FirstOrDefault();
                                if (tm == null)
                                {
                                    tm = new TagModel
                                    {
                                        Count = 0,
                                        Tag = tag,
                                        Key = tagParts.Length == 1 ? null : tagParts[0],
                                        Value = tagParts.Length == 1 ? tagParts[0] : tagParts[1],
                                    };
                                }
                                var ct = new ContentTag()
                                {
                                    Tag = tag,
                                    ContentId = Id ?? ""
                                };
                                tm.Count++;
                                service.DB.Save(tm);
                                service.DB.Insert(ct);
                            }
                            Tags.Add(tag);
                        }
                    }
                    else
                    {
                        if (existing.Contains(tag))
                        {
                            service.DB.DeleteWhere<ContentTag>(x => x.ContentId == Id && x.Tag == tag);
                            var tm = loadedTagModels.FirstOrDefault(x => x.Tag == tag) ?? service.DB.From<TagModel>().Where(x => x.Tag == tag).FirstOrDefault();
                            if (tm != null)
                            {
                                tm.Count--;
                                service.DB.Save(tm);
                            }
                        }
                    }
                }
                catch { }
            }
        }

        public void OnPostSave(IDatabaseContext db, bool isUpdate, object insertId)
        {
            if (NonCollectionContentMedia != null)
            {
                try
                {
                    var media = NonCollectionContentMedia;
                    CopyValuesTo(media);
                    media.ContentId = Id;
                    media.MediaCollectionId = null;
                    db.Save(media);
                }
                catch
                {
                    if (!isUpdate)
                        db.Delete(this);
                    throw;
                }
            }
        }

        public void OnPreSave(IDatabaseContext db, bool isUpdate)
        {
            if (ContentType?.Id != null)
            {
                ContentTypeId = ContentType.Id.Value;
            }
            if (string.IsNullOrWhiteSpace(Id))
            {
                Id = Guid.NewGuid().ToString("n");
                CreatedAt = DateTime.UtcNow;
            }
            UpdatedAt = DateTime.UtcNow;
        }
    }

    public enum ContentRating
    {
        Unrated,
        Everyone,
        TenPlus,
        Teen,
        Mature,
        AdultOnly
    }
}
