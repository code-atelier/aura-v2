﻿using Amaterasu.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    [Model("media_tags")]
    public class MediaTags
    {
        public string? MediaId { get; set; }

        public string Tag { get; set; } = string.Empty;
    }
}
