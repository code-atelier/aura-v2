﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary.Models
{
    public class ContentBase
    {
        #region Descriptor
        public string Title { get; set; } = string.Empty;

        public string OriginalTitle { get; set; } = string.Empty;

        public string Summary { get; set; } = string.Empty;
        #endregion

        #region Meta
        public float? Rating
        {
            get
            {
                var cnt = 0;
                var sum = 0f;
                if (VisualRating != null)
                {
                    sum += VisualRating.Value;
                    cnt++;
                }
                if (SoundRating != null)
                {
                    sum += SoundRating.Value;
                    cnt++;
                }
                if (StoryRating != null)
                {
                    sum += StoryRating.Value;
                    cnt++;
                }
                if (UserRating != null)
                {
                    sum += UserRating.Value;
                    cnt++;
                }
                if (OnlineRating != null)
                {
                    sum += OnlineRating.Value;
                    cnt++;
                }
                return cnt > 0 ? (float)Math.Round(sum / cnt, 2) : null;
            }
            set { }
        }

        public float? VisualRating { get; set; }

        public float? SoundRating { get; set; }

        public float? StoryRating { get; set; }

        public float? UserRating { get; set; }

        public float? OnlineRating { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public ContentRating ContentRating { get; set; }

        public bool ExplicitContent { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
        #endregion

        public void CopyValuesTo(ContentBase content)
        {
            content.Title = Title;
            content.OriginalTitle = OriginalTitle;
            content.Summary = Summary;
            content.VisualRating = VisualRating;
            content.SoundRating = SoundRating;
            content.StoryRating = StoryRating;
            content.UserRating = UserRating;
            content.OnlineRating = OnlineRating;
            content.ReleaseDate = ReleaseDate;
            content.ContentRating = ContentRating;
            content.ExplicitContent = ExplicitContent;
            content.CreatedAt = CreatedAt;
            content.UpdatedAt = UpdatedAt;
        }
    }
}
