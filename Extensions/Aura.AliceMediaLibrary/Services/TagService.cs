﻿using AliceMediaLibrary.Models;
using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary
{
    public partial class AliceMediaLibraryService
    {
        public List<TagModel> GetContentTags(Content content)
        {
            var res = new List<TagModel>();

            try
            {
                var tags = DB.From<ContentTag>().Where(x => x.ContentId == content.Id).ToList();
                var tagIds = tags.Select(x => x.Tag).ToList();
                return DB.From<TagModel>().Where(x => tagIds.Contains(x.Tag)).ToList();
            }
            catch { }

            return res;
        }

        public bool SaveTag(TagModel data)
        {
            try
            {
                DB.Save(data);
                return true;
            }
            catch { }
            return false;
        }

        public bool DeleteTag(TagModel data)
        {
            try
            {
                var used = DB.From<ContentTag>().Where(x => x.Tag == data.Tag).ToList();
                DB.DeleteWhere<ContentTag>(x => x.Tag == data.Tag);
                DB.Delete(data);
                UpdateContents(used.Select(x => x.ContentId).ToList());
                return true;
            }
            catch { }
            return false;
        }

        private async void UpdateContents(List<string> contentIds)
        {
            await Task.Run(() =>
            {
                // update contents
                foreach (var contentId in contentIds)
                {
                    try
                    {
                        var content = DB.From<Content>().Where(x => x.Id == contentId).FirstOrDefault();
                        if (content != null)
                        {
                            content.BuildSearchTerm(this);
                            DB.Save(content);
                        }
                    }
                    catch { }
                }
            });
        }

        public int CountTagUsage(TagModel data)
        {
            return DB.From<ContentTag>().Where(x => x.Tag == data.Tag).Count();
        }
    }
}
