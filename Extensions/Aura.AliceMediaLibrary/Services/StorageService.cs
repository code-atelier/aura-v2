﻿using AliceMediaLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary
{
    public partial class AliceMediaLibraryService
    {
        public Storage? GetMetaStorage()
        {
            try
            {
                return DB.From<Storage>().Where(x => x.IsMetaStorage).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public bool StorageExists(string name, string path, string? excludeId = null)
        {
            try
            {
                if (excludeId == null)
                {
                    return DB.From<Storage>().Where(x => x.Name == name || x.BasePath == path).Count() > 0;
                }
                return DB.From<Storage>().Where(x => (x.Name == name || x.BasePath == path) && x.Id != excludeId).Count() > 0;
            }
            catch
            {
                return false;
            }
        }

        public Storage? StorageFrom(string path)
        {
            try
            {
                path = AMLHelper.SanitizePath(path) + "/";
                return DB.From<Storage>().Where(x => path.StartsWith(x.BasePath)).FirstOrDefault();
            }
            catch
            {

            }
            return null;
        }

        public bool SaveStorage(Storage storage)
        {
            try
            {
                DB.Save(storage);
                return true;
            }
            catch { }
            return false;
        }

        public bool DeleteStorage(Storage storage)
        {
            try
            {
                DB.Delete(storage);
                return true;
            }
            catch { }
            return false;
        }
    }
}
