﻿using AliceMediaLibrary.WPF;
using Aura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AliceMediaLibrary.Tool
{
    public class AliceMediaLibraryTool : ToolBase
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Alice Media Library",
            Description = "Open and compile Code Mate solutions",
            Author = "Code Atelier",
            Id = "coatl.codemate.editor",
            Version = AliceMediaLibraryStatic.Version,
        };

        public AliceMediaLibraryService Service { get; }

        public AliceMediaLibraryTool(AliceMediaLibraryService service)
        {
            Service = service;
        }

        public override string Icon { get; } = Util.CreateResourceUri("res/img/icon.png");

        protected override Window CreateToolWindow()
        {
            return new MediaLibrary(Service);
        }
    }
}
