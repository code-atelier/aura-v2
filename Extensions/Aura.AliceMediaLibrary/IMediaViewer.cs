﻿using AliceMediaLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliceMediaLibrary
{
    public interface IMediaViewer
    {
        Guid Id { get; }

        string Name { get; }

        bool Accepts(ContentMediaType mediaType);
        
        void Run(Content content, Media media);
    }
}
