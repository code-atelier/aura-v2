﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Amaterasu;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for MediaItem.xaml
    /// </summary>
    public partial class MediaItem : UserControl
    {
        AliceMediaLibraryService Service { get; }

        public MediaItem(AliceMediaLibraryService service)
        {
            InitializeComponent();
            Service = service;
            LoadTheme();

            tbTitle.Text = "";
            tbLastUpdate.Text = "";
            spRating.Text = "-";
            lbContentType.Content = "";
            ContentTypeColor = Colors.Transparent;
            brdNew.Visibility = Visibility.Collapsed;
            brdAuto.Visibility = Visibility.Collapsed;
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(System.IO.Path.GetFullPath(System.IO.Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        public string Title { get => tbTitle.Text; set => tbTitle.Text = value; }

        Color _contentTypeColor = Colors.Red;
        public Color ContentTypeColor
        {
            get => _contentTypeColor;
            set
            {
                if (_contentTypeColor != value)
                {
                    _contentTypeColor = value;
                    lbContentType.Background = new SolidColorBrush(value);
                    lbContentType.Foreground = new SolidColorBrush(AMLHelper.GenerateForegroundColor(value, Color.FromArgb(0xff, 0x33, 0x33, 0x33), Color.FromArgb(0xff, 0xff, 0xff, 0xff)) ?? Color.FromArgb(0xff, 0x33, 0x33, 0x33));
                }
            }
        }

        public string ContentType { get => lbContentType.Content?.ToString() ?? ""; set => lbContentType.Content = value; }

        public ImageSource? Cover { get => imgCover.Source; set => imgCover.Source = value; }

        DateTime? _lastUpdate = null;
        public DateTime? LastUpdate
        {
            get => _lastUpdate;
            set
            {
                if (_lastUpdate != value)
                {
                    _lastUpdate = value;
                    tbLastUpdate.Text = _lastUpdate?.AsUTC().ToLocalTime().ToString("yyyy-MM-dd HH:mm") ?? "";
                }
            }
        }

        float? _rating = null;
        public float? Rating
        {
            get => _rating;
            set
            {
                if (_rating != value)
                {
                    _rating = value;
                    if (_rating.HasValue)
                    {
                        spRating.Text = Math.Round(_rating.Value, 2).ToString();

                        if (_rating <= 2 && Resources["AML.Brushes.Muted"] is SolidColorBrush bfg)
                            spRating.Foreground = bfg;
                        else if (_rating > 2 && _rating <= 4 && Resources["AML.Brushes.Danger"] is SolidColorBrush bfg2)
                            spRating.Foreground = bfg2;
                        else if (_rating > 4 && _rating <= 6 && Resources["AML.Brushes.Warning"] is SolidColorBrush bfg3)
                            spRating.Foreground = bfg3;
                        else if (_rating > 6 && _rating <= 8 && Resources["AML.Brushes.Success"] is SolidColorBrush bfg4)
                            spRating.Foreground = bfg4;
                        else if (_rating > 8 && Resources["AML.Brushes.Primary"] is SolidColorBrush bfg5)
                            spRating.Foreground = bfg5;
                    }
                    else
                    {
                        spRating.Text = "-";
                        if (Resources["AML.Brushes.Muted"] is SolidColorBrush bfg)
                            spRating.Foreground = bfg;
                    }
                }
            }
        }

        bool _selected = false;
        public bool IsSelected
        {
            get
            {
                return _selected;
            }
            set
            {
                if (_selected != value)
                {
                    _selected = value;
                    if (value)
                    {
                        if (Parent is Panel panel)
                        {
                            foreach (var mi in panel.Children.OfType<MediaItem>())
                            {
                                if (mi != this)
                                    mi.IsSelected = false;
                            }
                        }
                        if (Resources["AML.Brushes.BackgroundSelected"] is SolidColorBrush brBackground)
                            brdMain.Background = brBackground;
                        if (Resources["AML.Brushes.MutedSelected"] is SolidColorBrush brMuted)
                        {
                            brdMain.BorderBrush = brMuted;
                            tbLastUpdate.Foreground = brMuted;
                        }
                        OnSelected?.Invoke(this, new EventArgs());
                    }
                    else
                    {
                        if (Resources["AML.Brushes.Background"] is SolidColorBrush brBackground)
                            brdMain.Background = brBackground;
                        if (Resources["AML.Brushes.Muted"] is SolidColorBrush brMuted)
                        {
                            brdMain.BorderBrush = brMuted;
                            tbLastUpdate.Foreground = brMuted;
                        }
                    }
                }
            }
        }

        bool _isNew = false;
        public bool IsNew
        {
            get => _isNew;
            set
            {
                if (_isNew != value)
                {
                    _isNew = value;
                    brdNew.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        bool _isAuto = false;
        public bool IsAuto
        {
            get => _isAuto;
            set
            {
                if (_isAuto != value)
                {
                    _isAuto = value;
                    brdAuto.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        public void SetTags(IEnumerable<string> tags)
        {
            wpTags.Children.Clear();
            foreach (var tag in tags)
            {
                var tb = new TagButton();
                tb.Text = tag;
                tb.Mode = TagButtonMode.Addable;
                tb.Margin = new Thickness(0, 4, 8, 4);
                tb.TagClicked += Tb_TagClicked;
                tb.AddClicked += Tb_AddClicked;
                wpTags.Children.Add(tb);
            }
        }

        private void Tb_AddClicked(object? sender, EventArgs e)
        {
            if (sender is TagButton btn)
            {
                OnTagClick?.Invoke(this, btn.Text, true);
            }
        }

        private void Tb_TagClicked(object? sender, EventArgs e)
        {
            if (sender is TagButton btn)
            {
                OnTagClick?.Invoke(this, btn.Text, false);
            }
        }

        private void UserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            IsSelected = true;
        }

        public event EventHandler? OnSelected;

        private void UserControl_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            IsSelected = true;
        }

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OnDoubleClick?.Invoke(this, e);
        }

        public event EventHandler? OnDoubleClick;
        public event TagClickedEventHandler? OnTagClick;
    }

    public delegate void TagClickedEventHandler(object sender, string tag, bool isAdding);
}
