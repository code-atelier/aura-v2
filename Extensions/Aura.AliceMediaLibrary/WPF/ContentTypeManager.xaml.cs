﻿using AliceMediaLibrary.Models;
using Amaterasu.Database.MySQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for ContentTypeManager.xaml
    /// </summary>
    public partial class ContentTypeManager : Window
    {
        MySQLDatabaseContext DB { get; }
        AliceMediaLibraryService Service { get; }

        ContentType? CurrentData { get; set; }

        public ContentTypeManager(AliceMediaLibraryService service)
        {
            InitializeComponent();
            Service = service;
            LoadTheme();
            spEditor.IsEnabled = gbEditor.IsEnabled = false;
            DB = service.DB;

            var enumvals = Enum.GetValues<ContentMediaType>();
            foreach (var enumval in enumvals)
            {
                cbMediaType.Items.Add(enumval);
            }

            RefreshDataGrid();
            LoadFetcherAndRefiners();
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(System.IO.Path.GetFullPath(System.IO.Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        private void LoadFetcherAndRefiners()
        {
            lbFetchers.Items.Clear();
            lbRefiners.Items.Clear();
            try
            {
                var brush = Resources.Contains("AML.Brushes.SecondaryForeground") && Resources["AML.Brushes.SecondaryForeground"] is SolidColorBrush scb ? scb : new SolidColorBrush(Colors.White);
                foreach (var fetcher in Service.MediaInfoFetchers.OrderBy(x => x.Priority))
                {
                    var lbi = new ListBoxItem();
                    var cx = new CheckBox();
                    cx.Foreground = brush;
                    cx.Content = fetcher.Name;
                    cx.Tag = fetcher;
                    lbi.Content = cx;
                    lbFetchers.Items.Add(lbi);
                }
                foreach (var refiner in Service.MediaInfoRefiners.OrderBy(x => x.Priority))
                {
                    var lbi = new ListBoxItem();
                    var cx = new CheckBox();
                    cx.Foreground = brush;
                    cx.Content = refiner.Name;
                    cx.Tag = refiner;
                    lbi.Content = cx;
                    lbRefiners.Items.Add(lbi);
                }
            }
            catch { }
        }

        public void RefreshDataGrid()
        {
            try
            {
                var term = txSearch.Text.Trim();
                var contentTypes = DB.From<ContentType>().Where(x => x.Name.Contains(term)).OrderBy(x => x.Index, Amaterasu.Database.QuerySortDirection.Ascending).ToList();
                dgData.ItemsSource = contentTypes;
            }
            catch
            {

            }
        }

        public void ReadData(ContentType? data = null)
        {
            txName.Clear();
            txDescription.Clear();
            txColor.Clear();
            txIndex.Clear();
            cbMediaType.SelectedIndex = 0;
            swCollection.Value = 0;
            cbMediaType.IsEnabled = swCollection.IsEnabled = true;
            foreach(var lbi in lbFetchers.Items.OfType<ListBoxItem>())
            {
                if (lbi.Content is CheckBox cb && cb.Tag is IMediaInfoFetcher fetcher)
                {
                    if (data == null)
                    {
                        cb.IsChecked = fetcher.DefaultIsActive;
                    }
                    else
                    {
                        var setting = data.FetcherSettings;
                        if (setting.ContainsKey(fetcher.Id.ToString("n")))
                        {
                            cb.IsChecked = setting[fetcher.Id.ToString("n")];
                        }
                        else
                        {
                            cb.IsChecked = fetcher.DefaultIsActive;
                        }
                    }
                }
            }
            foreach (var lbi in lbRefiners.Items.OfType<ListBoxItem>())
            {
                if (lbi.Content is CheckBox cb && cb.Tag is IMediaInfoRefiner refiner)
                {
                    if (data == null)
                    {
                        cb.IsChecked = refiner.DefaultIsActive;
                    }
                    else
                    {
                        var setting = data.RefinerSettings;
                        if (setting.ContainsKey(refiner.Id.ToString("n")))
                        {
                            cb.IsChecked = setting[refiner.Id.ToString("n")];
                        }
                        else
                        {
                            cb.IsChecked = refiner.DefaultIsActive;
                        }
                    }
                }
            }
            if (data == null) return;
            txName.Text = data.Name;
            txDescription.Text = data.Description;
            txColor.Text = data.Color;
            txIndex.Text = data.Index.ToString();
            cbMediaType.SelectedItem = data.MediaType;
            swCollection.Value = data.IsCollection ? 1 : 0;
        }

        public bool WriteData()
        {
            if (CurrentData == null) return false;

            try
            {
                var name = txName.Text.Trim();
                if (string.IsNullOrWhiteSpace(name))
                    throw new Exception("Name cannot be empty");

                var color = txColor.Text;
                if (!string.IsNullOrWhiteSpace(color) && color.Length != 6)
                {
                    throw new Exception("Invalid color code");
                }

                int index;
                if (!int.TryParse(txIndex.Text, out index))
                {
                    throw new Exception("Invalid index value");
                }

                ContentMediaType mediaType = ContentMediaType.Image;
                if (cbMediaType.SelectedItem is ContentMediaType cmt)
                {
                    mediaType = cmt;
                }
                else
                {
                    throw new Exception("Please select media type");
                }

                CurrentData.Name = name;
                CurrentData.Description = txDescription.Text.Trim();
                CurrentData.Index = index;
                CurrentData.Color = color;
                CurrentData.MediaType = mediaType;
                CurrentData.IsCollection = swCollection.Value > 0;
                var fetSetting = new Dictionary<string, bool>();
                var refSetting = new Dictionary<string, bool>();
                foreach (var lbi in lbFetchers.Items.OfType<ListBoxItem>())
                {
                    if (lbi.Content is CheckBox cb && cb.Tag is IMediaInfoFetcher fetcher)
                    {
                        fetSetting[fetcher.Id.ToString("n")] = cb.IsChecked == true;
                    }
                }
                foreach (var lbi in lbRefiners.Items.OfType<ListBoxItem>())
                {
                    if (lbi.Content is CheckBox cb && cb.Tag is IMediaInfoRefiner refiner)
                    {
                        refSetting[refiner.Id.ToString("n")] = cb.IsChecked == true;
                    }
                }
                CurrentData.Fetchers = JsonSerializer.Serialize(fetSetting);
                CurrentData.Refiners = JsonSerializer.Serialize(refSetting);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        public void EditData(ContentType data)
        {
            ReadData(data);
            CurrentData = data;
            cbMediaType.IsEnabled = swCollection.IsEnabled = false;
            spEditor.IsEnabled = gbEditor.IsEnabled = true;
            spData.IsEnabled = gbData.IsEnabled = false;
        }

        public bool DeleteData(ContentType data)
        {
            try
            {
                DB.Delete(data);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveCurrentData()
        {
            try
            {
                if (CurrentData == null) return false;
                DB.Save(CurrentData);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            EditData(new ContentType());
            cbMediaType.IsEnabled = swCollection.IsEnabled = true;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (dgData.SelectedItem is ContentType ct)
            {
                EditData(ct);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(CurrentData?.Id == null ? "Cancel creating this data?" : "Cancel editing this data?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                return;
            }
            spEditor.IsEnabled = gbEditor.IsEnabled = false;
            spData.IsEnabled = gbData.IsEnabled = true;
            ReadData(null);
            RefreshDataGrid();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!WriteData())
            {
                return;
            }
            if (MessageBox.Show("Save this data?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                return;
            }
            if (SaveCurrentData())
            {
                MessageBox.Show("Data saved successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                spEditor.IsEnabled = gbEditor.IsEnabled = false;
                spData.IsEnabled = gbData.IsEnabled = true;
                ReadData(null);
                RefreshDataGrid();
            }
            else
            {
                MessageBox.Show("Failed to save data!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dgData.SelectedItem is ContentType ct)
            {
                if (MessageBox.Show("Are you sure to delete this data?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                {
                    return;
                }
                if (DeleteData(ct))
                {
                    MessageBox.Show("Data deleted successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReadData(null);
                    RefreshDataGrid();
                }
                else
                {
                    MessageBox.Show("Failed to delete data!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void swCollection_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            swCollection.Background = swCollection.Value > 0 ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Crimson);
        }

        private void dgData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgData.SelectedItem is ContentType ct)
            {
                ReadData(ct);
            }
            else
            {
                ReadData(null);
            }
        }

        private void txSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                RefreshDataGrid();
            }
        }

        private void btnColorPicker_Click(object sender, RoutedEventArgs e)
        {
            using (var colorPicker = new System.Windows.Forms.ColorDialog())
            {
                if (colorPicker.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var c = colorPicker.Color;
                    var bArr = new byte[3];
                    bArr[0] = c.R;
                    bArr[1] = c.G;
                    bArr[2] = c.B;
                    txColor.Text = BitConverter.ToString(bArr).Replace("-", "").ToUpper();
                }
            }
        }
    }
}
