﻿using AliceMediaLibrary.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for CreateNewContent.xaml
    /// </summary>
    public partial class SelectContentType : Window
    {
        ObservableCollection<ContentTypeSelectorModel> Items { get; } = new ObservableCollection<ContentTypeSelectorModel>();
        AliceMediaLibraryService Service { get; }

        public ContentType? Result { get; private set; }

        private bool WithCollection { get; }

        public SelectContentType(AliceMediaLibraryService service, bool withCollection)
        {
            InitializeComponent();
            icContentTypes.ItemsSource = Items;
            Service = service;
            LoadTheme();
            WithCollection = withCollection;
            LoadContentTypes();
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(System.IO.Path.GetFullPath(System.IO.Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        private void LoadContentTypes()
        {
            Items.Clear();
            try
            {
                var q = Service.DB.From<ContentType>();
                if (!WithCollection)
                    q = q.Where(x => x.IsCollection == false);
                var cts = q.OrderBy(x => x.Index).ToList().Select(x => new ContentTypeSelectorModel(x));
                foreach (ContentTypeSelectorModel model in cts)
                {
                    Items.Add(model);
                }
            }
            catch
            { }
        }

        private void borderItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Border brd && brd.Tag is ContentTypeSelectorModel model)
            {
                foreach (var item in Items)
                {
                    if (item != model)
                        item.IsSelected = false;
                }
                model.IsSelected = true;
                btnSelect.IsEnabled = true;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            var model = Items.Where(x => x.IsSelected).FirstOrDefault();
            if (model != null)
            {
                Result = model.ContentType;
                DialogResult = true;
                Close();
            }
        }
    }

    class ContentTypeSelectorModel : INotifyPropertyChanged
    {
        public ContentType ContentType { get; }

        public ContentTypeSelectorModel(ContentType contentType)
        {
            ContentType = contentType;
        }

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    NotifyPropertyChanged(nameof(IsSelected));
                    NotifyPropertyChanged(nameof(Background));
                    NotifyPropertyChanged(nameof(Foreground));
                    NotifyPropertyChanged(nameof(MutedForeground));
                }
            }
        }
        bool _isSelected = false;

        public SolidColorBrush Background
        {
            get => new SolidColorBrush((IsSelected ? AMLHelper.ParseColor(ContentType.Color) : null) ?? Color.FromArgb(255, 204, 204, 204));
        }

        public SolidColorBrush Foreground
        {
            get => new SolidColorBrush((IsSelected ? 
                AMLHelper.GenerateForegroundColor(AMLHelper.ParseColor(ContentType.Color), Color.FromArgb(0xff, 0x33, 0x33, 0x33), Color.FromArgb(0xff, 0xff, 0xff, 0xff)) : null)
                ?? Color.FromArgb(0xff, 0x33, 0x33, 0x33));
        }

        public SolidColorBrush MutedForeground
        {
            get => new SolidColorBrush((IsSelected ?
                AMLHelper.GenerateForegroundColor(AMLHelper.ParseColor(ContentType.Color), Color.FromArgb(0xff, 0x66, 0x66, 0x66), Color.FromArgb(0xff, 0xdd, 0xdd, 0xdd)) : null)
                ?? Color.FromArgb(0xff, 0x66, 0x66, 0x66));
        }

        public string Name { get => ContentType.Name; }

        public string Description { get => ContentType.Description; }

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler? PropertyChanged;
    }
}
