﻿using AliceMediaLibrary.Models;
using Amaterasu.Database.MySQL;
using Org.BouncyCastle.Crypto.Prng;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for StorageManager.xaml
    /// </summary>
    public partial class StorageManager : Window
    {
        AliceMediaLibraryService Service { get; }

        Storage? CurrentData { get; set; }

        public StorageManager(AliceMediaLibraryService service)
        {
            InitializeComponent();
            spEditor.IsEnabled = gbEditor.IsEnabled = false;
            Service = service;
            LoadTheme();

            var enumvals = Enum.GetValues<StorageKind>();
            foreach (var enumval in enumvals)
            {
                cbKind.Items.Add(enumval);
            }

            RefreshDataGrid();
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(System.IO.Path.GetFullPath(System.IO.Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        public void RefreshDataGrid()
        {
            try
            {
                var term = txSearch.Text.Trim();
                var contentTypes = Service.DB.From<Storage>().Where(x => (x.Name.Contains(term) || x.BasePath.Contains(term)) && x.IsMetaStorage == false).OrderBy(x => x.Name, Amaterasu.Database.QuerySortDirection.Ascending).ToList();
                dgData.ItemsSource = contentTypes;
            }
            catch
            {

            }
        }

        public void ReadData(Storage? data = null)
        {
            txName.Clear();
            txBasePath.Clear();
            cbKind.SelectedIndex = 0;
            cbKind.IsEnabled = true;
            if (data == null) return;
            txName.Text = data.Name;
            txBasePath.Text = data.BasePath;
            cbKind.SelectedItem = data.Kind;
        }

        public bool WriteData()
        {
            if (CurrentData == null) return false;

            try
            {
                var name = txName.Text.Trim();
                if (string.IsNullOrWhiteSpace(name))
                    throw new Exception("Name cannot be empty");

                var basePath = AMLHelper.SanitizePath(txBasePath.Text);
                if (string.IsNullOrWhiteSpace(basePath))
                {
                    throw new Exception("Base path cannot be empty");
                }
                basePath = basePath.TrimEnd('/') + "/";

                if (Service.StorageExists(name, basePath, CurrentData.Id))
                {
                    throw new Exception("Storage with the same name or path already existed");
                }

                StorageKind storageKind = StorageKind.FileSystem;
                if (cbKind.SelectedItem is StorageKind cmt)
                {
                    storageKind = cmt;
                }
                else
                {
                    throw new Exception("Please select storage kind");
                }

                if (storageKind == StorageKind.FileSystem && !Directory.Exists(basePath))
                {
                    throw new Exception("Directory not found: " + basePath);
                }
                if (storageKind == StorageKind.Uri)
                {
                    try
                    {
                        new Uri(basePath);
                    }
                    catch
                    {
                        throw new Exception("Base path is not a valid uri: " + basePath);
                    }
                }

                CurrentData.Name = name;
                CurrentData.BasePath = basePath;
                CurrentData.Kind = storageKind;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        public void EditData(Storage data)
        {
            ReadData(data);
            CurrentData = data;
            cbKind.IsEnabled = false;
            spEditor.IsEnabled = gbEditor.IsEnabled = true;
            spData.IsEnabled = gbData.IsEnabled = false;
        }

        public bool DeleteData(Storage data)
        {
            try
            {
                return Service.DeleteStorage(data);
            }
            catch
            {
                return false;
            }
        }

        public bool SaveCurrentData()
        {
            try
            {
                if (CurrentData == null) return false;
                return Service.SaveStorage(CurrentData);
            }
            catch
            {
                return false;
            }
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            EditData(new Storage()
            {
                Id = Guid.NewGuid().ToString("n"),
                IsMetaStorage = false,
            });
            cbKind.IsEnabled = true;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (dgData.SelectedItem is Storage data)
            {
                EditData(data);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(CurrentData?.Id == null ? "Cancel creating this data?" : "Cancel editing this data?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                return;
            }
            spEditor.IsEnabled = gbEditor.IsEnabled = false;
            spData.IsEnabled = gbData.IsEnabled = true;
            ReadData(null);
            RefreshDataGrid();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!WriteData())
            {
                return;
            }
            if (MessageBox.Show("Save this data?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                return;
            }
            if (SaveCurrentData())
            {
                MessageBox.Show("Data saved successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                spEditor.IsEnabled = gbEditor.IsEnabled = false;
                spData.IsEnabled = gbData.IsEnabled = true;
                ReadData(null);
                RefreshDataGrid();
            }
            else
            {
                MessageBox.Show("Failed to save data!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dgData.SelectedItem is Storage data)
            {
                if (MessageBox.Show("Are you sure to delete this data?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                {
                    return;
                }
                if (DeleteData(data))
                {
                    MessageBox.Show("Data deleted successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReadData(null);
                    RefreshDataGrid();
                }
                else
                {
                    MessageBox.Show("Failed to delete data!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void dgData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgData.SelectedItem is Storage data)
            {
                ReadData(data);
            }
            else
            {
                ReadData(null);
            }
        }

        private void txSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                RefreshDataGrid();
            }
        }

        private void cbKind_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnBrowse.IsEnabled = false;
            if (cbKind.SelectedItem is StorageKind storageKind)
            {
                btnBrowse.IsEnabled = storageKind == StorageKind.FileSystem;
            }
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            var fbd = new System.Windows.Forms.FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txBasePath.Text = AMLHelper.SanitizePath(fbd.SelectedPath) + "/";
            }
        }
    }
}
