﻿using Aura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for EHLinkSelector.xaml
    /// </summary>
    public partial class EHLinkSelector : Window
    {
        AliceMediaLibraryService Service { get; }

        public EHLinkSelector(AliceMediaLibraryService service)
        {
            Service = service;
            InitializeComponent();
            LoadTheme();
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(System.IO.Path.GetFullPath(System.IO.Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        List<KeyValuePair<string, string>> previousOption = new List<KeyValuePair<string, string>>();

        public void LoadOptions(IEnumerable<KeyValuePair<string, string>> options)
        {
            spSelection.Children.Clear();
            previousOption.Clear();
            var first = true;
            foreach (var kv in options)
            {
                var rb = new RadioButton();
                var tb = new TextBlock();
                rb.Margin = new Thickness(0, 4, 0, 4);
                var hl = new Hyperlink();
                tb.Inlines.Add(hl);
                hl.Inlines.Add(new Run(kv.Key));
                hl.Click += Hl_Click;
                if (Resources.Contains("AML.Brushes.Foreground") && Resources["AML.Brushes.Foreground"] is SolidColorBrush scb)
                {
                    hl.Foreground = scb;
                }
                rb.Tag = kv.Value;
                hl.Tag = kv.Value;
                rb.Content = tb;
                if (first)
                {
                    first = false;
                    rb.IsChecked = true;
                }
                spSelection.Children.Add(rb);
                previousOption.Add(new KeyValuePair<string, string>(kv.Key, kv.Value));
            }
        }

        private void Hl_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Hyperlink hl && hl.Tag is string uri)
            {
                try
                {
                    Util.OpenFile(uri);
                }
                catch { }
            }
        }

        public string? Result { get; set; }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            var rb = spSelection.Children.OfType<RadioButton>().FirstOrDefault(x => x.IsChecked == true);
            if (rb != null)
            {
                Result = rb.Tag?.ToString();
                DialogResult = true;
                Close();
            }
        }

        private void btnAddCustomLink_Click(object sender, RoutedEventArgs e)
        {
            var f = new EHCustomLink();
            if (f.ShowDialog() == true && !string.IsNullOrWhiteSpace(f.Result))
            {
                if (!previousOption.Any(x => x.Value == f.Result))
                {
                    previousOption.Insert(0, new KeyValuePair<string, string>(f.ResultName ?? "Custom: " + f.Result, f.Result));
                }
                LoadOptions(previousOption.ToList());
            }
        }
    }
}
