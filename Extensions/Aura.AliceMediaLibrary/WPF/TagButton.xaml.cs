﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for TagButton.xaml
    /// </summary>
    public partial class TagButton : UserControl
    {
        public TagButton()
        {
            InitializeComponent();
            btnAdd.Visibility = Visibility.Collapsed;
        }

        public string Text { get => lbText.Text; set => lbText.Text = value; }

        TagButtonMode _mode = TagButtonMode.Deleteable;
        public TagButtonMode Mode
        {
            get => _mode;
            set
            {
                if (_mode != value)
                {
                    _mode = value;
                    btnAdd.Visibility = _mode == TagButtonMode.Addable ? Visibility.Visible : Visibility.Collapsed;
                    btnDelete.Visibility = _mode == TagButtonMode.Deleteable ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        private void btnTag_Click(object sender, RoutedEventArgs e)
        {
            TagClicked?.Invoke(this, e);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddClicked?.Invoke(this, e);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteClicked?.Invoke(this, e);
        }

        Color _tagButtonBackground = Color.FromArgb(0xff, 0xee, 0xee, 0xee);
        public Color TagButtonBackground
        {
            get => _tagButtonBackground;
            set
            {
                if (_tagButtonBackground != value)
                {
                    _tagButtonBackground = value;
                    btnTag.Background = new SolidColorBrush(value);
                }
            }
        }

        Color _tagButtonForeground = Color.FromArgb(0xff, 0, 0, 0);
        public Color TagButtonForeground
        {
            get => _tagButtonForeground;
            set
            {
                if (_tagButtonForeground != value)
                {
                    _tagButtonForeground = value;
                    lbText.Foreground = new SolidColorBrush(value);
                }
            }
        }

        public event EventHandler? TagClicked;
        public event EventHandler? AddClicked;
        public event EventHandler? DeleteClicked;
    }

    public enum TagButtonMode
    {
        Deleteable,
        Addable,
    }
}
