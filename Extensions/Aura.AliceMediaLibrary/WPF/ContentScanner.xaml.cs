﻿using AliceMediaLibrary.Models;
using Aura;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for ContentScanner.xaml
    /// </summary>
    public partial class ContentScanner : Window
    {
        public AliceMediaLibraryService Service { get; }

        public ContentType ContentType { get; }

        private bool AnyContentAdded { get; set; }

        private bool WantStopScanning { get; set; }

        private DispatcherTimer QueueScheduler { get; }

        public ContentScanner(AliceMediaLibraryService service, ContentType contentType)
        {
            Service = service;
            ContentType = contentType;
            InitializeComponent();
            LoadTheme();
            LoadFileSystemStorage();

            lbContentType.Content = contentType.Name;
            lbContentType.Foreground = new SolidColorBrush(AMLHelper.ParseColor(contentType.Color) ?? Colors.White);

            QueueScheduler = new DispatcherTimer();
            QueueScheduler.Interval = new TimeSpan(0, 0, 1);
            QueueScheduler.Tick += QueueScheduler_Tick;
            QueueScheduler.Start();
        }

        bool schedulerRunning = false;
        private void QueueScheduler_Tick(object? sender, EventArgs e)
        {
            if (schedulerRunning) return;
            try
            {
                schedulerRunning = true;
                ScheduleQueue();
                if (ScannedContents.Count > 0)
                {
                    var li = new List<ScannedContent>();
                    while (ScannedContents.Count > 0)
                    {
                        li.Add(ScannedContents.Dequeue());
                    }
                    Dispatcher.Invoke(() =>
                    {
                        foreach (var c in li)
                        {
                            try
                            {
                                dataGrid.Items.Add(c);
                            }
                            catch { }
                        }
                        lbContentCount.Content = dataGrid.Items.Count + " content" + (dataGrid.Items.Count > 1 ? "s" : "") + " found";
                    });
                }
            }
            catch { }
            finally
            {
                schedulerRunning = false;
            }
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(System.IO.Path.GetFullPath(System.IO.Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        private void LoadFileSystemStorage()
        {
            cbStorage.Items.Clear();
            try
            {
                var storages = Service.DB.From<Storage>().Where(x => x.IsMetaStorage == false && x.Kind == StorageKind.FileSystem).OrderBy(x => x.Name).ToList();
                foreach (var storage in storages)
                {
                    cbStorage.Items.Add(storage);
                }
                if (cbStorage.Items.Count > 0)
                {
                    cbStorage.SelectedIndex = 0;
                    btnScan.IsEnabled = true;
                }
            }
            catch { }
        }

        private void DataGridHyperlinkColumn_Click(object sender, RoutedEventArgs e)
        {
            if (sender is TextBlock tb && tb.Tag is ScannedContent c)
            {
                try
                {
                    var content = new Content()
                    {
                        ContentType = ContentType,
                        ContentTypeId = ContentType.Id ?? 0,
                        FetchTagFromContent = false,
                        Title = System.IO.Path.GetFileNameWithoutExtension(c.Path.TrimEnd('/')),
                    };
                    content.NonCollectionContentMedia = new Media()
                    {
                        Storage = c.Storage,
                        StorageId = c.Storage.Id,
                        Path = c.Path,
                    };
                    var ced = new ContentEditor(Service, content, true);
                    if (ced.ShowDialog() == true)
                    {
                        AnyContentAdded = true;
                        dataGrid.Items.Remove(c);
                        lbContentCount.Content = dataGrid.Items.Count + " content" + (dataGrid.Items.Count > 1 ? "s" : "") + " found";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void DataGridHyperlinkColumnBrowse_Click(object sender, RoutedEventArgs e)
        {
            if (sender is TextBlock tb && tb.Tag is ScannedContent c)
            {
                try
                {
                    if (File.Exists(c.Path))
                    {
                        Util.ShowInExplorer(c.Path);
                    }
                    else if (Directory.Exists(c.Path))
                    {
                        Util.OpenFile(c.Path);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Close content scanner?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
            else
            {
                WantStopScanning = true;
                DialogResult = AnyContentAdded;
                try
                {
                    QueueScheduler.Stop();
                }
                catch { }
            }
        }

        Thread? ScanThread { get; set; }
        private void btnScan_Click(object sender, RoutedEventArgs e)
        {
            if (cbStorage.SelectedItem is Storage storage)
            {
                btnScan.Visibility = Visibility.Collapsed;
                btnStopScan.Visibility = Visibility.Visible;
                cbStorage.IsEnabled = false;
                dataGrid.IsEnabled = true;
                dataGrid.Items.Clear();
                WantStopScanning = false;
                ScannedContents.Clear();
                ScanThread = new Thread(new ParameterizedThreadStart(ScanAction));
                ScanThread.Start(storage);
                lbContentCount.Content = "0 content found";
            }
        }

        private void ScanAction(object? storageObject)
        {
            try
            {
                if (storageObject is Storage storage)
                {
                    ScanStorage(storage);
                }
            }
            catch
            {
            }
            finally
            {
                WantStopScanning = true;
                Dispatcher.Invoke(() =>
                {
                    btnStopScan.Visibility = Visibility.Collapsed;
                    btnScan.Visibility = Visibility.Visible;
                    cbStorage.IsEnabled = true;
                });
            }
        }

        private void ScanStorage(Storage storage)
        {
            if (!Directory.Exists(storage.BasePath))
                return;
            ScanDirectory(storage, storage.BasePath);
        }

        private void ScanDirectory(Storage storage, string path)
        {
            if (WantStopScanning) return;
            if (Directory.Exists(path))
            {
                try
                {
                    bool scanSubdir = true;
                    var dirs = Directory.EnumerateDirectories(path).Select(x => new DirectoryInfo(x)).OrderBy(x => x.CreationTime).Reverse();
                    var files = Directory.EnumerateFiles(path).Where(x => AMLHelper.SupportedMediaExtensions[ContentType.MediaType].Contains("*" + System.IO.Path.GetExtension(x).ToLower())).ToList();
                    if (ContentType.MediaType == ContentMediaType.Image || ContentType.MediaType == ContentMediaType.EBook)
                    {
                        var imageFilesCount = files.RemoveAll(AMLHelper.IsImageFile);
                        // if image, then get the dir
                        if (path != storage.BasePath && imageFilesCount > 0)
                        {
                            scanSubdir = false;
                            EnqueuePath(storage, path + "/");
                        }
                    }
                    foreach (var file in files.Select(x => new FileInfo(x)).OrderBy(x => x.CreationTime).Reverse())
                    {
                        // push files
                        if (WantStopScanning) return;
                        EnqueuePath(storage, file.FullName);
                    }
                    if (scanSubdir)
                    {
                        foreach (var dir in dirs)
                        {
                            if (WantStopScanning) return;
                            ScanDirectory(storage, dir.FullName);
                        }
                    }
                }
                catch { }
            }
        }

        private void EnqueuePath(Storage storage, string path)
        {
            try
            {
                if (WantStopScanning) return;
                path = AMLHelper.SanitizePath(path);
                if (!path.StartsWith(storage.BasePath)) return;
                var relpath = path.Substring(storage.BasePath.Length);
                if (File.Exists(path))
                {
                    var fi = new FileInfo(path);
                    TempQueue.Enqueue(new ScannedContent(storage)
                    {
                        RelativePath = relpath,
                        Path = AMLHelper.SanitizePath(fi.FullName),
                        CreatedAt = fi.CreationTime.ToString("dd MMM yyyy HH:mm:ss"),
                        UpdatedAt = fi.CreationTime.ToString("dd MMM yyyy HH:mm:ss"),
                    });
                }
                else if (Directory.Exists(path))
                {
                    var di = new DirectoryInfo(path);
                    TempQueue.Enqueue(new ScannedContent(storage)
                    {
                        RelativePath = relpath + "/",
                        Path = AMLHelper.SanitizePath(di.FullName) + "/",
                        CreatedAt = di.CreationTime.ToString("dd MMM yyyy HH:mm:ss"),
                        UpdatedAt = di.CreationTime.ToString("dd MMM yyyy HH:mm:ss"),
                    });
                }
            }
            catch { }
        }

        private void ScheduleQueue()
        {
            if (TempQueue.Count > 0)
            {
                try
                {
                    var li = new List<ScannedContent>();
                    while (TempQueue.Count > 0)
                    {
                        li.Add(TempQueue.Dequeue());
                    }
                    var storId = li.First().Storage.Id;
                    var relpaths = li.Select(x => x.RelativePath).ToList();
                    var existing = Service.DB.From<Media>().Where(x => x.StorageId == storId && relpaths.Contains(x.Path) && x.ContentId != null).ToList();
                    foreach (var i in li)
                    {
                        if (!existing.Any(x => x.Path == i.RelativePath))
                        {
                            ScannedContents.Enqueue(i);
                        }
                    }
                }
                catch { }
            }
        }

        private Queue<ScannedContent> TempQueue { get; } = new Queue<ScannedContent>();
        private Queue<ScannedContent> ScannedContents { get; } = new Queue<ScannedContent>();

        private void btnStopScan_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure to stop scanning?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
            WantStopScanning = true;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                WantStopScanning = true;
            }
            catch { }
        }
    }

    public class ScannedContent
    {
        public string Path { get; set; } = string.Empty;

        public string RelativePath { get; set; } = string.Empty;

        public string CreatedAt { get; set; } = string.Empty;

        public string UpdatedAt { get; set; } = string.Empty;

        public string Action { get; set; } = "Add";

        public string Browse { get; set; } = "Browse";

        public Storage Storage { get; set; }

        public ScannedContent(Storage storage)
        {
            Storage = storage;
        }
    }
}
