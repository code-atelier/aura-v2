﻿using AliceMediaLibrary.Models;
using Aura;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for ContentEditor.xaml
    /// </summary>
    public partial class ContentEditor : Window
    {
        Content EditedContent { get; }

        AliceMediaLibraryService Service { get; }

        DispatcherTimer SuggestedTagsDelayer { get; }

        bool DoAutoFill { get; set; }
        public ContentEditor(AliceMediaLibraryService service, Content content, bool doAutoFill = false)
        {
            EditedContent = content;
            Service = service;
            DoAutoFill = doAutoFill;
            InitializeComponent();
            LoadTheme();
            SuggestedTagsDelayer = new DispatcherTimer();
            SuggestedTagsDelayer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            SuggestedTagsDelayer.Tick += SuggestedTagsDelayer_Tick;

            var enumvals = Enum.GetValues<ContentRating>();
            foreach (var enumval in enumvals)
            {
                cbContentRating.Items.Add(enumval);
            }

            wpSuggestedTags.Children.Clear();
            wpTags.Children.Clear();
            ReadContent();
        }

        private void SuggestedTagsDelayer_Tick(object? sender, EventArgs e)
        {
            GenerateSuggestedTags();
        }

        private List<string> GetAddedTags()
        {
            var tags = new List<string>();
            foreach (var tb in wpTags.Children.OfType<TagButton>())
            {
                tags.Add(tb.Text);
            }
            return tags;
        }

        bool generatingSuggestedTag = false;
        private async void GenerateSuggestedTags()
        {
            SuggestedTagsDelayer.Stop();
            if (generatingSuggestedTag) return;
            generatingSuggestedTag = true;
            await Task.Run(() =>
            {
                try
                {
                    string[]? spl = null;
                    List<string> excl = new List<string>();
                    Dispatcher.Invoke(() =>
                    {
                        wpSuggestedTags.Children.Clear();
                        spl = txAddTag.Text.Split(' ', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                        excl = GetAddedTags();
                    });
                    var term = spl == null || spl.Length == 0 ? "" : spl.Last();
                    var q = Service.DB.From<TagModel>();
                    q = q.Where(x => x.Tag.Contains(term)).OrderBy(x => x.Priority, Amaterasu.Database.QuerySortDirection.Descending).OrderBy(x => x.Count, Amaterasu.Database.QuerySortDirection.Descending).OrderBy(x => x.UpdatedAt, Amaterasu.Database.QuerySortDirection.Descending).Take(20);
                    if (excl.Count > 0)
                        q = q.Where(x => !excl.Contains(x.Tag));
                    var res = q.ToList();
                    Dispatcher.Invoke(() =>
                    {
                        foreach (var tag in res)
                        {
                            var tb = new TagButton();
                            tb.Text = tag.Tag;
                            tb.Mode = TagButtonMode.Addable;
                            tb.Margin = new Thickness(0, 8, 6, 0);
                            tb.AddClicked += TbSuggested_AddClicked;
                            tb.TagClicked += TbSuggested_TagClicked;
                            wpSuggestedTags.Children.Add(tb);
                        }
                    });
                }
                catch
                {
                }
                finally
                {
                    generatingSuggestedTag = false;
                }
            });
        }

        private void TbSuggested_TagClicked(object? sender, EventArgs e)
        {
            if (sender is TagButton btn)
            {
                txAddTag.Clear();
                AddTag(btn.Text, true);
            }
        }

        private void TbSuggested_AddClicked(object? sender, EventArgs e)
        {
            if (sender is TagButton btn)
            {
                AddTag(btn.Text, true);
            }
        }

        private void AddTag(string tag, bool generateSuggestedTagsAfter = false)
        {
            var existing = GetAddedTags();
            if (!existing.Contains(tag))
            {
                var tb = new TagButton();
                tb.Text = tag;
                tb.Mode = TagButtonMode.Deleteable;
                tb.Margin = new Thickness(0, 8, 6, 0);
                tb.DeleteClicked += Tb_DeleteClicked;
                wpTags.Children.Add(tb);
                CheckExplicitTag(tag);
                if (generateSuggestedTagsAfter)
                {
                    SuggestedTagsDelayer.Stop();
                    SuggestedTagsDelayer.Start();
                }
            }
        }

        bool _doNotCheckExplicitTag = false;
        private async void CheckExplicitTag(string tag)
        {
            if (_doNotCheckExplicitTag) return;
            await Task.Run(() =>
            {
                try
                {
                    if (Service.DB.From<TagModel>().Where(x => x.Tag == tag && x.IsExplicit).Count() > 0)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            cxExplicitContent.IsChecked = true;
                        });
                    }
                }
                catch { }
            });
        }

        private void Tb_DeleteClicked(object? sender, EventArgs e)
        {
            if (sender is TagButton btn)
            {
                wpTags.Children.Remove(btn);
                SuggestedTagsDelayer.Stop();
                SuggestedTagsDelayer.Start();
            }
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(System.IO.Path.GetFullPath(System.IO.Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        private void PopulateStorage()
        {
            try
            {
                var prevId = cbStorage.SelectedItem is Storage str ? str.Id : null;

                Storage? selected = null;
                var storages = Service.DB.From<Storage>().Where(x => x.IsMetaStorage == false).OrderBy(x => x.Kind).OrderBy(x => x.Name).ToList();
                foreach (var storage in storages)
                {
                    if (prevId != null && storage.Id == prevId)
                        selected = storage;
                    cbStorage.Items.Add(storage);
                }
                if (EditedContent.NonCollectionContentMedia?.Id != null)
                {
                    cbStorage.SelectedItem = selected ?? cbStorage.Items.OfType<Storage>().Where(x => x.Id == EditedContent.NonCollectionContentMedia.StorageId).FirstOrDefault();
                }
                else if (cbStorage.Items.Count > 0)
                {
                    if (selected == null)
                        cbStorage.SelectedIndex = 0;
                    else
                        cbStorage.SelectedItem = selected;
                }
            }
            catch { }
        }

        private void ReadContent()
        {
            Title = "Content Editor - Editing '" + EditedContent.Title + "'";
            if (EditedContent.ContentType != null)
            {
                lbContentType.Content = EditedContent.ContentType.Name;
                lbContentType.Background = new SolidColorBrush(AMLHelper.ParseColor(EditedContent.ContentType.Color) ?? Color.FromArgb(0xff, 0x33, 0x33, 0x33));
                lbContentType.Foreground = new SolidColorBrush(
                    AMLHelper.GenerateForegroundColor(AMLHelper.ParseColor(EditedContent.ContentType.Color), Color.FromArgb(0xff, 0x33, 0x33, 0x33), Color.FromArgb(0xff, 0xff, 0xff, 0xff))
                    ?? Color.FromArgb(0xff, 0xff, 0xff, 0xff)
                    );
            }

            txTitle.Text = EditedContent.Title;
            txOriginalTitle.Text = EditedContent.OriginalTitle;
            txSummary.Text = EditedContent.Summary;

            lbStorage.IsEnabled =
            cbStorage.IsEnabled =
            lbPath.IsEnabled =
            txPath.IsEnabled =
            btnBrowsePath.IsEnabled = !(EditedContent.ContentType?.IsCollection ?? false);
            cxFetchTagFromContent.IsEnabled = EditedContent.ContentType?.IsCollection ?? false;
            if (!(EditedContent.ContentType?.IsCollection ?? false))
            {
                // populate storages
                PopulateStorage();
                tiMedia.Visibility = Visibility.Collapsed;
                rdTagging.Height = new GridLength(0);
                if (EditedContent.NonCollectionContentMedia is Media m && m.Storage != null)
                {
                    txPath.Text = System.IO.Path.Combine(m.Storage.BasePath, m.Path);
                }
            }
            else
            {
                rdStorage.Height = rdPath.Height = new GridLength(0);
                btnAuto.Visibility = Visibility.Collapsed;
            }

            if (EditedContent.Tags.Count > 0)
            {
                _doNotCheckExplicitTag = true;
                try
                {
                    foreach (var tag in EditedContent.Tags)
                    {
                        AddTag(tag);
                    }
                }
                finally
                {
                    _doNotCheckExplicitTag = false;
                }
            }

            slOnlineRating.Value = EditedContent.OnlineRating ?? 0;
            slSoundRating.Value = EditedContent.SoundRating ?? 0;
            slStoryRating.Value = EditedContent.StoryRating ?? 0;
            slUserRating.Value = EditedContent.UserRating ?? 0;
            slVisualRating.Value = EditedContent.VisualRating ?? 0;

            dpReleaseDate.SelectedDate = EditedContent.ReleaseDate;
            dpAirEndDate.SelectedDate = EditedContent.AirEndDate;
            cbContentRating.SelectedItem = EditedContent.ContentRating;
            cxExplicitContent.IsChecked = EditedContent.ExplicitContent;
            cxFetchTagFromContent.IsChecked = EditedContent.FetchTagFromContent;
            if (EditedContent.Id != null)
            {
                try
                {
                    var metaStorage = Service.DB.From<Storage>().Where(x => x.IsMetaStorage).FirstOrDefault();
                    if (metaStorage != null)
                    {
                        try
                        {
                            var path = AliceMediaLibraryStatic.GetCoverPath(metaStorage.BasePath, EditedContent);
                            using (var fi = File.OpenRead(path))
                            {
                                var bmp = new BitmapImage();
                                bmp.BeginInit();
                                bmp.StreamSource = fi;
                                bmp.CacheOption = BitmapCacheOption.OnLoad;
                                bmp.EndInit();
                                imgCover.Source = bmp;
                            }
                        }
                        catch { }
                        try
                        {
                            var path = AliceMediaLibraryStatic.GetBackdropPath(metaStorage.BasePath, EditedContent);
                            using (var fi = File.OpenRead(path))
                            {
                                var bmp = new BitmapImage();
                                bmp.BeginInit();
                                bmp.StreamSource = fi;
                                bmp.CacheOption = BitmapCacheOption.OnLoad;
                                bmp.EndInit();
                                imgBackdrop.Source = bmp;
                            }
                        }
                        catch { }
                    }
                }
                catch { }
            }

            SuggestedTagsDelayer.Stop();
            SuggestedTagsDelayer.Start();
        }

        private bool WriteContent()
        {
            // Path must match storage
            string path = txPath.Text.Trim();
            string fullPath = path;
            if (EditedContent.ContentType != null && !EditedContent.ContentType.IsCollection && cbStorage.SelectedItem is Storage storage)
            {
                if (path.ToLower().StartsWith(storage.BasePath.Trim().ToLower()))
                    path = path.Substring(storage.BasePath.Trim().Length);
                else
                {
                    MessageBox.Show("Path must start with storage's base path!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
                if (EditedContent.NonCollectionContentMedia != null)
                {
                    if (Directory.Exists(fullPath))
                    {
                        fullPath = fullPath.TrimEnd('/') + "/";
                    }
                    var media = EditedContent.NonCollectionContentMedia;
                    media.Path = path;
                    media.StorageId = storage.Id;
                    media.Storage = storage;
                    media.IsDirectory = storage.Kind == StorageKind.FileSystem ? Directory.Exists(fullPath) : false;
                    media.IsArchive = storage.Kind == StorageKind.FileSystem ? System.IO.Path.GetExtension(path)?.ToLower() == ".zip" : false;
                    media.Episode = null;
                }
            }

            // Title must not be empty
            string title = txTitle.Text.Trim();
            if (string.IsNullOrWhiteSpace(title))
            {
                MessageBox.Show("Title must not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            EditedContent.Title = title;
            EditedContent.ExplicitContent = cxExplicitContent.IsChecked ?? false;
            EditedContent.OriginalTitle = txOriginalTitle.Text.Trim();
            EditedContent.Summary = txSummary.Text;
            if (cbContentRating.SelectedItem is ContentRating rating)
                EditedContent.ContentRating = rating;
            EditedContent.ReleaseDate = dpReleaseDate.SelectedDate;
            EditedContent.AirEndDate = dpAirEndDate.SelectedDate;
            EditedContent.OnlineRating = slOnlineRating.Value == 0 ? null : (float)slOnlineRating.Value;
            EditedContent.UserRating = slUserRating.Value == 0 ? null : (float)slUserRating.Value;
            EditedContent.VisualRating = slVisualRating.Value == 0 ? null : (float)slVisualRating.Value;
            EditedContent.SoundRating = slSoundRating.Value == 0 ? null : (float)slSoundRating.Value;
            EditedContent.StoryRating = slStoryRating.Value == 0 ? null : (float)slStoryRating.Value;
            EditedContent.FetchTagFromContent = cxFetchTagFromContent.IsChecked ?? false;

            return true;
        }

        private void slUserRating_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lbUserRating.Content = slUserRating.Value <= 0 ? "Not Rated" : slUserRating.Value;
            UpdateOverallRating();
        }

        private void slOnlineRating_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lbOnlineRating.Content = slOnlineRating.Value <= 0 ? "Not Rated" : slOnlineRating.Value;
            UpdateOverallRating();
        }

        private void slVisualRating_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lbVisualRating.Content = slVisualRating.Value <= 0 ? "Not Rated" : slVisualRating.Value;
            UpdateOverallRating();
        }

        private void slSoundRating_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lbSoundRating.Content = slSoundRating.Value <= 0 ? "Not Rated" : slSoundRating.Value;
            UpdateOverallRating();
        }

        private void slStoryRating_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lbStoryRating.Content = slStoryRating.Value <= 0 ? "Not Rated" : slStoryRating.Value;
            UpdateOverallRating();
        }

        private void UpdateOverallRating()
        {
            var sum = 0f;
            var cnt = 0;
            if (slUserRating.Value > 0)
            {
                cnt++;
                sum += (float)slUserRating.Value;
            }
            if (slOnlineRating.Value > 0)
            {
                cnt++;
                sum += (float)slOnlineRating.Value;
            }
            if (slVisualRating.Value > 0)
            {
                cnt++;
                sum += (float)slVisualRating.Value;
            }
            if (slSoundRating.Value > 0)
            {
                cnt++;
                sum += (float)slSoundRating.Value;
            }
            if (slStoryRating.Value > 0)
            {
                cnt++;
                sum += (float)slStoryRating.Value;
            }
            lbOverallRating.Content = cnt > 0 ? Math.Round(sum / cnt, 2) : "Not Rated";
        }

        private void cbStorage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbStorage?.IsEnabled == true && cbStorage?.SelectedItem is Storage storage)
            {
                //btnBrowsePath.IsEnabled = txPath.IsReadOnly = storage.Kind == StorageKind.FileSystem;
            }
        }

        private void btnBrowsePath_Click(object sender, RoutedEventArgs e)
        {
            if (cbStorage?.IsEnabled == true && cbStorage?.SelectedItem is Storage storage && EditedContent.ContentType is ContentType contentType)
            {
                var ofd = new OpenFileDialog();
                ofd.CheckPathExists = true;
                ofd.InitialDirectory = storage.BasePath.Replace('/', '\\');
                ofd.Filter = contentType.MediaType + " Files|" + string.Join(";", AMLHelper.SupportedMediaExtensions[contentType.MediaType]);
                if (ofd.ShowDialog() == true)
                {
                    var ext = System.IO.Path.GetExtension(ofd.FileName)?.ToLower();
                    if (contentType.MediaType == ContentMediaType.EBook || contentType.MediaType == ContentMediaType.Image)
                    {
                        if (ext == ".pdf" || ext == ".zip" || ext == ".doc" || ext == ".docx")
                            txPath.Text = AMLHelper.SanitizePath(ofd.FileName);
                        else
                            txPath.Text = AMLHelper.SanitizePath(System.IO.Path.GetDirectoryName(ofd.FileName) ?? "") + "/";
                    }
                    else
                    {
                        txPath.Text = AMLHelper.SanitizePath(ofd.FileName);
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        bool forceClose = false;
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (forceClose) return;
            if (MessageBox.Show("Are you sure to cancel?\r\nUnsaved data will be lost!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (WriteContent() && MessageBox.Show("Save this content?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    if (EditedContent.NonCollectionContentMedia?.Storage != null)
                    {
                        var storid = EditedContent.NonCollectionContentMedia.Storage.Id;
                        var path = EditedContent.NonCollectionContentMedia.Path;
                        // find existing path
                        var existingPath = Service.DB.From<Media>().Where(x => x.ContentId != null && x.ContentId != EditedContent.Id && x.StorageId == storid && x.Path == path).FirstOrDefault();
                        if (existingPath != null)
                        {
                            MessageBox.Show("Content with the same path already existed:\r\n"
                                + "Title: " + existingPath.Title + "\r\n"
                                + "Original Title: " + existingPath.OriginalTitle + "\r\n"
                                + "Path: " + existingPath.Path
                                , "Error", MessageBoxButton.OK, MessageBoxImage.Error
                                );
                            return;
                        }

                        // find similar title
                        var orgTitle = EditedContent.OriginalTitle.Trim();
                        existingPath = Service.DB.From<Media>().Where(x => x.ContentId != null && x.ContentId != EditedContent.Id && (x.Title == EditedContent.Title || (orgTitle != "" && x.OriginalTitle == orgTitle))).FirstOrDefault();
                        if (existingPath != null)
                        {
                            if (MessageBox.Show("Content with the same title already existed. Do you want to proceed?\r\n"
                                + "Title: " + existingPath.Title + "\r\n"
                                + "Original Title: " + existingPath.OriginalTitle + "\r\n"
                                + "Path: " + existingPath.Path
                                , "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning
                                ) != MessageBoxResult.Yes)
                                return;
                        }
                    }


                    EditedContent.FromAutoScan = false;
                    Service.DB.Save(EditedContent);
                    if (!WriteCoverAndBackdrop(EditedContent)) return;
                    var addedTags = GetAddedTags();
                    EditedContent.UpdateTags(Service, addedTags);
                    EditedContent.BuildSearchTerm(Service);
                    Service.DB.Save(EditedContent);

                    MessageBox.Show("Content saved successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    forceClose = true;
                    DialogResult = true;
                    Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private bool WriteCoverAndBackdrop(Content content)
        {
            if (content.Id == null) return false;
            try
            {
                var metaStorage = Service.DB.From<Storage>().Where(x => x.IsMetaStorage).FirstOrDefault();
                if (metaStorage == null)
                {
                    throw new Exception("Meta storage is not set!");
                }
                if (imgCover.Source is BitmapImage bitmapImage)
                {
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    var path = AliceMediaLibraryStatic.GetCoverPath(metaStorage.BasePath, content);
                    using (var fo = File.Create(path))
                    {
                        encoder.Save(fo);
                    }
                }
                if (imgBackdrop.Source is BitmapImage bmp)
                {
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bmp));
                    var path = AliceMediaLibraryStatic.GetBackdropPath(metaStorage.BasePath, content);
                    using (var fo = File.Create(path))
                    {
                        encoder.Save(fo);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to save cover and backrop.\r\n" + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        private void txAddTag_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var spl = txAddTag.Text.Split(' ', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                foreach (var tag in spl)
                {
                    var p = AMLHelper.ParseTag(tag.ToLower());
                    if (p != null)
                    {
                        AddTag(tag);
                    }
                }
                txAddTag.Clear();
            }
            SuggestedTagsDelayer.Stop();
            SuggestedTagsDelayer.Start();
        }

        private void btnAutoFill_Click(object sender, RoutedEventArgs e)
        {
            AutoFill();
        }

        private async void AutoFill(bool showMessage = true)
        {
            string fullPath = string.Empty;
            Dispatcher.Invoke(() =>
            {
                btnAutoFill.IsEnabled = false;
                fullPath = txPath.Text.Trim();

                if (!showMessage || MessageBox.Show("Clear current data before auto filling?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    txTitle.Clear();
                    txOriginalTitle.Clear();
                    txSummary.Clear();
                    wpTags.Children.Clear();
                    slOnlineRating.Value = 0;
                }
            });
            try
            {
                var mifs = Service.MediaInfoFetchers.OrderBy(x => x.Priority).ToList();
                MediaInfoFetchResult result = new MediaInfoFetchResult();
                var ct = EditedContent.ContentType ?? new ContentType();
                var fetcherSettings = ct.FetcherSettings;
                var refinerSettings = ct.RefinerSettings;
                foreach (var mif in mifs)
                {
                    try
                    {
                        if (fetcherSettings.ContainsKey(mif.Id.ToString("n")) && !fetcherSettings[mif.Id.ToString("n")])
                            continue;
                        var res = await mif.FetchInfo(fullPath);
                        if (res != null)
                        {
                            if (!string.IsNullOrWhiteSpace(res.Title))
                                result.Title = res.Title;
                            if (!string.IsNullOrWhiteSpace(res.OriginalTitle))
                                result.OriginalTitle = res.OriginalTitle;
                            if (!string.IsNullOrWhiteSpace(res.Summary))
                                result.Summary = res.Summary;
                            if (res.OnlineRating != null)
                                result.OnlineRating = (float)Math.Round(Math.Min(10, Math.Max(0, res.OnlineRating.Value)), 2);
                            if (res.IsExplicit)
                                result.IsExplicit = true;
                            if (res.ContentRating != null)
                                result.ContentRating = res.ContentRating.Value;
                            if (res.Tags?.Count > 0)
                            {
                                foreach (var tag in res.Tags)
                                {
                                    var taglc = tag.ToLower();
                                    if (AMLHelper.ParseTag(taglc) != null && !result.Tags.Contains(taglc))
                                    {
                                        result.Tags.Add(taglc);
                                    }
                                }
                            }
                            if (res.ThumbnailUri != null)
                                result.ThumbnailUri = res.ThumbnailUri;
                        }
                    }
                    catch { }
                }
                foreach (var re in Service.MediaInfoRefiners)
                {
                    if (refinerSettings.ContainsKey(re.Id.ToString("n")) && !refinerSettings[re.Id.ToString("n")])
                        continue;
                    re.Refine(result);
                }

                string? tempPath = Path.Combine(Path.GetTempPath(), "aml4", Guid.NewGuid().ToString("n") + ".tmp");
                try
                {
                    var dir = Path.GetDirectoryName(tempPath);
                    if (dir != null && !Directory.Exists(dir))
                        Directory.CreateDirectory(dir);
                }
                catch { }
                if (!string.IsNullOrWhiteSpace(result.ThumbnailUri))
                {
                    try
                    {
                        if (File.Exists(result.ThumbnailUri))
                        {
                            File.Copy(result.ThumbnailUri, tempPath);
                        }
                        else
                        {
                            using (var wc = new HttpClient())
                            {
                                using (var res = await wc.GetAsync(result.ThumbnailUri, HttpCompletionOption.ResponseContentRead))
                                {
                                    if (res.IsSuccessStatusCode)
                                    {
                                        var stream = res.Content.ReadAsStream();
                                        using (var fo = File.Create(tempPath))
                                        {
                                            stream.CopyTo(fo);
                                        }
                                    }
                                    else
                                    {
                                        tempPath = null;
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        tempPath = null;
                    }
                }
                else tempPath = null;
                Dispatcher.Invoke(() =>
                {
                    if (!string.IsNullOrWhiteSpace(result.Title))
                        txTitle.Text = result.Title;
                    if (!string.IsNullOrWhiteSpace(result.OriginalTitle))
                        txOriginalTitle.Text = result.OriginalTitle;
                    if (!string.IsNullOrWhiteSpace(result.Summary))
                        txSummary.Text = result.Summary;
                    if (result.OnlineRating != null)
                        slOnlineRating.Value = Math.Round(Math.Min(10, Math.Max(0, result.OnlineRating.Value)), 2);
                    if (result.ContentRating != null)
                        cbContentRating.SelectedItem = result.ContentRating.Value;
                    if (result.IsExplicit)
                        cxExplicitContent.IsChecked = true;
                    if (result.Tags?.Count > 0)
                    {
                        foreach (var tag in result.Tags)
                        {
                            AddTag(tag);
                        }
                    }
                    if (tempPath != null)
                    {
                        SetCover(tempPath);
                    }
                    else
                    {
                        btnAuto_Click(this, new RoutedEventArgs());
                    }
                });
                GenerateSuggestedTags();
                if (showMessage)
                    MessageBox.Show("Autofill success!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                if (showMessage)
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    btnAutoFill.IsEnabled = true;
                });
            }
        }

        private void btnManageStorage_Click(object sender, RoutedEventArgs e)
        {
            var f = new StorageManager(Service);
            f.ShowDialog();
            PopulateStorage();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (DoAutoFill)
            {
                DoAutoFill = false;
                if (!string.IsNullOrWhiteSpace(txPath.Text))
                {
                    AutoFill(false);
                }
            }
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Util.OpenFile(txPath.Text.Trim());
            }
            catch { }
        }
    }
}
