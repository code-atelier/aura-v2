﻿using AliceMediaLibrary.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace AliceMediaLibrary.WPF
{
    public partial class ContentEditor
    {
        private void GridCoverBackdropDragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
            e.Effects = DragDropEffects.None;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files?.Length > 0)
                {
                    var file = files.First();
                    if (AMLHelper.IsImageFile(file))
                    {
                        e.Effects = DragDropEffects.Copy;
                    }
                }
            }
        }

        private void GridCoverBackdropDrop(object sender, DragEventArgs e)
        {
            e.Handled = true;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files?.Length > 0)
                {
                    var file = files.First();
                    if (AMLHelper.IsImageFile(file))
                    {
                        if (sender == grCover)
                        {
                            SetCover(file);
                        }
                        else if (sender == grBackdrop)
                        {
                            SetBackdrop(file);
                        }
                    }
                }
            }
        }
        
        private async void SetCover(string path)
        {
            await Task.Run(() =>
            {
                Dispatcher.Invoke(() =>
                {
                    try
                    {
                        var bmp = AMLHelper.GetBitmap(path);
                        if (bmp != null)
                        {
                            var f = new ImageCropper();
                            f.LoadImage(bmp, 180, 240);
                            if (f.ShowDialog() == true && f.Result.HasValue)
                            {
                                try
                                {
                                    var bitmap = AMLHelper.CropBitmap(bmp, 180, 240, f.Result.Value);
                                    imgCover.Source = bitmap;
                                    lbCoverDragDropNotice.Visibility = Visibility.Collapsed;
                                }
                                catch { }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                });
            });
        }

        private async void SetBackdrop(string path)
        {
            await Task.Run(() =>
            {
                Dispatcher.Invoke(() =>
                {
                    try
                    {
                        var bmp = AMLHelper.GetBitmap(path);
                        if (bmp != null)
                        {
                            var f = new ImageCropper();
                            f.LoadImage(bmp, 960, 540);
                            if (f.ShowDialog() == true && f.Result.HasValue)
                            {
                                try
                                {
                                    var bitmap = AMLHelper.CropBitmap(bmp, 960, 540, f.Result.Value);
                                    imgBackdrop.Source = bitmap;
                                    lbBackdropDragDropNotice.Visibility = Visibility.Collapsed;
                                }
                                catch { }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                });
            });
        }

        private void txPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnAutoFill.IsEnabled = !string.IsNullOrWhiteSpace(txPath.Text);
            btnBrowse.IsEnabled = false;

            // auto find storage
            try
            {
                var path = AMLHelper.SanitizePath(txPath.Text);
                if (Directory.Exists(path))
                    path += "/";
                Storage? selected = null;
                foreach (var storage in cbStorage.Items.OfType<Storage>())
                {
                    if (path.StartsWith(storage.BasePath))
                    {
                        selected = storage;
                        break;
                    }
                }
                if (selected != null)
                {
                    cbStorage.SelectedItem = selected;
                    if (selected.Kind == StorageKind.FileSystem)
                    {
                        btnBrowse.IsEnabled = File.Exists(txPath.Text.Trim()) || Directory.Exists(txPath.Text.Trim());
                    }
                    else
                    {
                        btnBrowse.IsEnabled = true;
                    }
                }
            }
            catch { }
        }

        private void btnClearCover_Click(object sender, RoutedEventArgs e)
        {
            imgCover.Source = null;
            lbCoverDragDropNotice.Visibility = Visibility.Visible;
        }

        private void btnClearBackdrop_Click(object sender, RoutedEventArgs e)
        {
            imgBackdrop.Source = null;
            lbBackdropDragDropNotice.Visibility = Visibility.Visible;
        }

        private OpenFileDialog CreateImageOpenDialog()
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "Image Files|" + string.Join(";", AMLHelper.ImageExtensions.Select(x => "*" + x));
            if (cbStorage.IsEnabled && cbStorage.SelectedItem is Storage storage && storage.Kind == StorageKind.FileSystem)
            {
                if (!string.IsNullOrWhiteSpace(txPath.Text))
                {
                    if (Directory.Exists(txPath.Text))
                    {
                        ofd.InitialDirectory = txPath.Text.Replace("/", "\\");
                    }
                    else if (File.Exists(txPath.Text))
                    {
                        ofd.InitialDirectory = Path.GetDirectoryName(txPath.Text)?.Replace("/", "\\") ?? "";
                    }
                }
                else if (Directory.Exists(storage.BasePath))
                {
                    ofd.InitialDirectory = storage.BasePath.Replace("/", "\\");
                }
            }
            return ofd;
        }

        private void btnSetCover_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var ofd = CreateImageOpenDialog();
                if (ofd.ShowDialog() == true)
                {
                    SetCover(ofd.FileName);
                }
            }
            catch { }
        }

        private void btnSetBackdrop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var ofd = CreateImageOpenDialog();
                if (ofd.ShowDialog() == true)
                {
                    SetBackdrop(ofd.FileName);
                }
            }
            catch { }
        }

        private void btnCoverClipboard_Click(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsFileDropList())
            {
                var file = Clipboard.GetFileDropList().OfType<string>().FirstOrDefault(AMLHelper.IsImageFile);
                if (file != null)
                {
                    SetCover(file);
                }
            }
            else if (Clipboard.ContainsImage())
            {
                try
                {
                    var bmp = AMLHelper.GetBitmap(Clipboard.GetImage());
                    var f = new ImageCropper();
                    f.LoadImage(bmp, 180, 240);
                    if (f.ShowDialog() == true && f.Result.HasValue)
                    {
                        try
                        {
                            var bitmap = AMLHelper.CropBitmap(bmp, 180, 240, f.Result.Value);
                            imgCover.Source = bitmap;
                            lbCoverDragDropNotice.Visibility = Visibility.Collapsed;
                        }
                        catch { }
                    }
                }
                catch { }
            }
        }

        private void btnBackdropClipboard_Click(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsFileDropList())
            {
                var file = Clipboard.GetFileDropList().OfType<string>().FirstOrDefault(AMLHelper.IsImageFile);
                if (file != null)
                {
                    SetBackdrop(file);
                }
            }
            else if (Clipboard.ContainsImage())
            {
                try
                {
                    var bmp = AMLHelper.GetBitmap(Clipboard.GetImage());
                    var f = new ImageCropper();
                    f.LoadImage(bmp, 960, 540);
                    if (f.ShowDialog() == true && f.Result.HasValue)
                    {
                        try
                        {
                            var bitmap = AMLHelper.CropBitmap(bmp, 960, 540, f.Result.Value);
                            imgBackdrop.Source = bitmap;
                            lbBackdropDragDropNotice.Visibility = Visibility.Collapsed;
                        }
                        catch { }
                    }
                }
                catch { }
            }
        }
        private void btnAuto_Click(object sender, RoutedEventArgs e)
        {
            if (cbStorage.IsEnabled && cbStorage.SelectedItem is Storage storage && storage.Kind == StorageKind.FileSystem)
            {
                if (Directory.Exists(txPath.Text))
                {
                    var findOrder = new List<string>()
                    {
                        "*cover*.*",
                        "*title*.*",
                        "*jacket*.*",
                        "*logo*.*",
                        "*.*",
                    };
                    string? coverFile = null;
                    var patternIndex = 0;
                    while (coverFile == null && patternIndex < findOrder.Count)
                    {
                        coverFile = Directory.EnumerateFiles(txPath.Text, findOrder[patternIndex], SearchOption.AllDirectories).OrderBy(x => Path.GetFileNameWithoutExtension(x)).FirstOrDefault(AMLHelper.IsImageFile);
                        patternIndex++;
                    }
                    if (coverFile != null)
                    {
                        SetCover(coverFile);
                    }
                }
                else if (File.Exists(txPath.Text) && AMLHelper.IsImageFile(txPath.Text))
                {
                    SetCover(txPath.Text);
                }
            }
        }
    }
}
