﻿using AliceMediaLibrary.Models;
using Aura;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;

namespace AliceMediaLibrary.WPF
{
    public partial class MediaLibrary
    {
        public static RoutedCommand ManageContentType { get; set; } = new RoutedCommand();
        public static RoutedCommand MetaStorage { get; set; } = new RoutedCommand();
        public static RoutedCommand ManageStorage { get; set; } = new RoutedCommand();
        public static RoutedCommand AddContent { get; set; } = new RoutedCommand();
        public static RoutedCommand ScanContent { get; set; } = new RoutedCommand();
        public static RoutedCommand ManageTags { get; set; } = new RoutedCommand();
        public static RoutedCommand ViewContent { get; set; } = new RoutedCommand();
        public static RoutedCommand OpenContentFolder { get; set; } = new RoutedCommand();
        public static RoutedCommand EditContent { get; set; } = new RoutedCommand();
        public static RoutedCommand DeleteContent { get; set; } = new RoutedCommand();


        public static RoutedCommand NextPage { get; set; } = new RoutedCommand();
        public static RoutedCommand Next10Page { get; set; } = new RoutedCommand();
        public static RoutedCommand PrevPage { get; set; } = new RoutedCommand();
        public static RoutedCommand Prev10Page { get; set; } = new RoutedCommand();

        private void UpdateTitle()
        {
            Title = AppName;
        }

        private void UpdateContentTypes(bool checkAll = true)
        {
            try
            {
                var checkedList = rgbContentTypes.Items.OfType<Fluent.CheckBox>().Where(x => x.Tag is int).ToDictionary(x => (int)x.Tag, x => x.IsChecked == true);
                rgbContentTypes.Items.Clear();
                var cts = Service.DB.From<ContentType>().OrderBy(x => x.Index, Amaterasu.Database.QuerySortDirection.Ascending).ToList();
                foreach (var ct in cts)
                {
                    var cbi = new CheckBox()
                    {
                        Content = new TextBlock() { Text = ct.Name },
                        IsChecked = checkAll || !checkedList.ContainsKey(ct.Id ?? -1) || checkedList[ct.Id ?? -1],
                        Tag = ct.Id,
                        FontWeight = FontWeights.DemiBold,
                        Margin = new Thickness(8, 4, 8, 4)
                    };
                    if (AMLHelper.ParseColor(ct.Color) is Color color)
                    {
                        cbi.Foreground = new SolidColorBrush(color);
                    }
                    rgbContentTypes.Items.Add(cbi);
                }
                rgbContentTypes.Visibility = cts.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
            }
            catch
            {
            }
        }

        private void RefreshSearchFilters(bool firstTime = false)
        {
            UpdateContentTypes(firstTime);
            RefreshData();
        }

        DateTime? _lastPopularTagsUpdate = null;
        private async void RefreshData(int? page = 1)
        {
            bool shouldUpdatePopularTags = _lastPopularTagsUpdate == null || (DateTime.Now - _lastPopularTagsUpdate.Value).TotalMinutes > 5;
            int? selPage = page ?? null;
            Dispatcher.Invoke(() =>
            {
                sbiStatus.Content = "Loading data...";
                sbiTime.Content = "";
                sbiOfTotalPages.Content = "";
                wpList.Children.Clear();
                _skipPageSelection = true;
                selPage = selPage ?? (cbPage.SelectedItem is int v ? v : null);
                cbPage.Items.Clear();
                _skipPageSelection = false;
                cbPage.IsEnabled = false;
                btnFind.IsEnabled = false;
                lbLoading.Visibility = Visibility.Visible;
                lbNoResult.Visibility = Visibility.Collapsed;
                ftxSearch.IsEnabled = false;
                if (shouldUpdatePopularTags)
                    rgbPopularTags.Items.Clear();
            });
            var initTime = DateTime.Now;
            try
            {
                var res = await PerformSearch(selPage);
                var delta = DateTime.Now - initTime;
                var mst = Service.DB.From<Storage>().Where(x => x.IsMetaStorage).FirstOrDefault();
                var loadList = new Dictionary<MediaItem, string>();
                Dispatcher.Invoke(() =>
                {
                    foreach (var i in res.Contents)
                    {
                        try
                        {
                            var tagModels = i.LoadTags(Service);

                            var mi = new MediaItem(Service);
                            mi.Margin = new Thickness(8);
                            mi.Tag = i;
                            mi.Title = i.Title;
                            mi.Rating = i.Rating;
                            mi.LastUpdate = i.UpdatedAt;
                            mi.IsAuto = i.FromAutoScan;
                            mi.IsNew = i.CreatedAt >= DateTime.UtcNow.AddDays(-7);
                            if (i.ContentType != null)
                            {
                                mi.ContentType = i.ContentType.Name;
                                mi.ContentTypeColor = AMLHelper.ParseColor(i.ContentType.Color) ?? Colors.Black;
                            }
                            mi.SetTags(tagModels.Take(5).Select(x => x.Tag));
                            mi.OnSelected += Mi_OnSelected;
                            mi.OnDoubleClick += Mi_OnDoubleClick;
                            mi.OnTagClick += Mi_OnTagClick;
                            wpList.Children.Add(mi);
                            if (mst != null)
                            {
                                var cover = AliceMediaLibraryStatic.GetCoverPath(mst.BasePath, i);
                                loadList.Add(mi, cover);
                            }
                        }
                        catch { }
                    }
                    var loadTime = Math.Round(delta.TotalMilliseconds, 1);
                    var loadTimeUnit = "ms";
                    if (loadTime >= 1000)
                    {
                        loadTime = Math.Round(loadTime / 1000, 1);
                        loadTimeUnit = "s";
                    }

                    sbiTime.Content = "(" + loadTime + loadTimeUnit + ")";
                    sbiStatus.Content = $"Showing {res.TotalInPage} of {res.TotalCount} results";
                    sbiOfTotalPages.Content = "of " + res.PageCount;
                    rbgSelectedContent.Visibility = Visibility.Collapsed;
                    cbPage.IsEnabled = true;
                    btnFind.IsEnabled = true;
                    ftxSearch.IsEnabled = true;
                    lbLoading.Visibility = Visibility.Collapsed;
                    lbNoResult.Visibility = res.TotalInPage <= 0 ? Visibility.Visible : Visibility.Collapsed;
                    for (var i = 1; i <= res.PageCount; i++)
                    {
                        cbPage.Items.Add(i);
                    }
                    _skipPageSelection = true;
                    try
                    {
                        if (cbPage.Items.Contains(res.CurrentPage))
                        {
                            cbPage.SelectedItem = res.CurrentPage;
                        }
                        else if (cbPage.Items.Count > 0)
                        {
                            cbPage.SelectedIndex = 0;
                        }
                    }
                    finally
                    {
                        _skipPageSelection = false;
                    }
                    CommandManager.InvalidateRequerySuggested();
                });

                foreach (var ll in loadList)
                {
                    try
                    {
                        LoadImageFor(ll.Key, ll.Value);
                    }
                    catch { }
                }

                if (shouldUpdatePopularTags)
                {
                    var popTags = Service.DB.From<TagModel>().Where(x => x.Count > 0).OrderBy(x => x.Count, Amaterasu.Database.QuerySortDirection.Descending).OrderBy(x => x.Priority, Amaterasu.Database.QuerySortDirection.Descending).OrderBy(x => x.Tag, Amaterasu.Database.QuerySortDirection.Ascending).Take(9).ToList();
                    Dispatcher.Invoke(() =>
                    {
                        foreach (var tag in popTags)
                        {
                            var tb = new TagButton();
                            tb.Text = tag.Tag;
                            tb.Mode = TagButtonMode.Addable;
                            tb.Margin = new Thickness(4, 1, 4, 1);
                            tb.AddClicked += Tb_AddClicked;
                            tb.TagClicked += Tb_TagClicked;
                            rgbPopularTags.Items.Add(tb);
                        }
                    });
                    rgbPopularTags.Visibility = popTags.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
                    _lastPopularTagsUpdate = DateTime.Now;
                }
            }
            catch { }
            finally
            {
            }
        }

        private void Tb_TagClicked(object? sender, EventArgs e)
        {
            if (sender is TagButton tb)
            {
                Mi_OnTagClick(tb, tb.Text, false);
            }
        }

        private void Tb_AddClicked(object? sender, EventArgs e)
        {
            if (sender is TagButton tb)
            {
                Mi_OnTagClick(tb, tb.Text, true);
            }
        }

        private void Mi_OnTagClick(object sender, string tag, bool isAdding)
        {
            if (isAdding)
            {
                ftxSearch.Text += (ftxSearch.Text.Length > 0 ? " " : "") + "$" + tag;
            }
            else
            {
                ftxSearch.Text = "$" + tag;
            }
            rtiLibrary.IsSelected = true;
            RefreshData();
        }

        private void Mi_OnDoubleClick(object? sender, EventArgs e)
        {
            if (sender is MediaItem mi && mi.Tag is Content content)
            {
                OpenContentViewer(content);
            }
        }

        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            RefreshData();
        }

        private void UpdateViewMargin()
        {
            var rem = svMain.ActualWidth % (234 + 16);
            wpList.Margin = new Thickness(rem / 2, 0, 0, 0);
        }

        private void check_ServiceConnected(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Service.State == Aura.Services.ServiceState.Running || Service.State == Aura.Services.ServiceState.Busy;
        }

        private void command_ManageContentType(object sender, ExecutedRoutedEventArgs e)
        {
            var f = new ContentTypeManager(Service);
            f.ShowDialog();
            RefreshSearchFilters();
        }

        private void command_AddContent(object sender, ExecutedRoutedEventArgs e)
        {
            var f = new SelectContentType(Service, true);
            if (f.ShowDialog() == true && f.Result?.Id != null)
            {
                var newContent = new Content()
                {
                    Title = "New Content",
                    ContentType = f.Result,
                    ContentTypeId = f.Result.Id.Value,
                    FetchTagFromContent = false,
                };
                if (!f.Result.IsCollection)
                {
                    newContent.NonCollectionContentMedia = new Media();
                }
                var ced = new ContentEditor(Service, newContent);
                if (ced.ShowDialog() == true)
                {
                    RefreshData(null);
                }
            }
        }

        private void command_ScanContent(object sender, ExecutedRoutedEventArgs e)
        {
            var f = new SelectContentType(Service, false);
            if (f.ShowDialog() == true && f.Result?.Id != null)
            {
                var cs = new ContentScanner(Service, f.Result);
                if (cs.ShowDialog() == true)
                {
                    RefreshData();
                }
            }
        }

        private void command_ManageTags(object sender, ExecutedRoutedEventArgs e)
        {
            var f = new TagManager(Service);
            f.ShowDialog();
        }

        private void command_ManageStorage(object sender, ExecutedRoutedEventArgs e)
        {
            var f = new StorageManager(Service);
            f.ShowDialog();
        }

        private void command_MetaStorage(object sender, ExecutedRoutedEventArgs e)
        {
            var metaStorage = Service.GetMetaStorage();
            var fbd = new System.Windows.Forms.FolderBrowserDialog();
            if (metaStorage != null)
            {
                fbd.InitialDirectory = metaStorage.BasePath.Replace("/", "\\");
            }
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var basePath = AMLHelper.SanitizePath(Path.GetFullPath(fbd.SelectedPath)) + "/";
                if (metaStorage == null)
                {
                    metaStorage = new Storage()
                    {
                        BasePath = basePath,
                        Id = "meta",
                        IsMetaStorage = true,
                        Kind = StorageKind.FileSystem,
                        Name = "Meta Storage"
                    };
                }
                else
                {
                    metaStorage.BasePath = basePath;
                }

                try
                {
                    var coverDir = Path.Combine(metaStorage.BasePath, AliceMediaLibraryStatic.CoverPath);
                    var backdropDir = Path.Combine(metaStorage.BasePath, AliceMediaLibraryStatic.BackdropPath);
                    var thumbnailDir = Path.Combine(metaStorage.BasePath, AliceMediaLibraryStatic.ThumbnailPath);
                    if (!Directory.Exists(coverDir))
                        Directory.CreateDirectory(coverDir);
                    if (!Directory.Exists(backdropDir))
                        Directory.CreateDirectory(backdropDir);
                    if (!Directory.Exists(thumbnailDir))
                        Directory.CreateDirectory(thumbnailDir);
                    var testFile = Path.Combine(metaStorage.BasePath, "test.file");
                    File.WriteAllText(testFile, "This is a test file");
                    File.Delete(testFile);
                    var anchorFile = Path.Combine(metaStorage.BasePath, "aml4.metastorage");
                    File.WriteAllText(anchorFile, "What? This is just a marker file.");
                }
                catch
                {
                    MessageBox.Show("Failed to write to meta storage!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                if (Service.SaveStorage(metaStorage))
                {
                    MessageBox.Show("Meta storage successfully set!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Failed to set meta storage!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }

        private void RibbonWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateViewMargin();
        }

        private void check_ContentSelected(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = wpList.Children.OfType<MediaItem>().Any(x => x.IsSelected);
        }

        private void command_ViewContent(object sender, ExecutedRoutedEventArgs e)
        {
            var selected = wpList.Children.OfType<MediaItem>().FirstOrDefault(x => x.IsSelected);
            if (selected != null && selected.Tag is Content content)
            {
                OpenContentViewer(content);
            }
        }

        private void OpenContentViewer(Content content)
        {
            if (content.NonCollectionContentMedia?.Storage?.Kind == StorageKind.Uri)
            {
                try
                {
                    var fullUri = Path.Combine(content.NonCollectionContentMedia.Storage.BasePath, content.NonCollectionContentMedia.Path);
                    Util.OpenFile(fullUri);
                }
                catch { }
            }
            else if (content.NonCollectionContentMedia?.Storage?.Kind == StorageKind.FileSystem)
            {
                try
                {
                    var fullPath = Path.Combine(content.NonCollectionContentMedia.Storage.BasePath, content.NonCollectionContentMedia.Path);
                    Util.OpenFile(fullPath);
                }
                catch { }
            }
        }

        private void check_ContentSelectedAndNotCollection(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = wpList.Children.OfType<MediaItem>().Any(x => x.IsSelected && x.Tag is Content content && !(content.ContentType?.IsCollection ?? false)
                && content.NonCollectionContentMedia != null && content.NonCollectionContentMedia.Storage != null && content.NonCollectionContentMedia.Storage.Kind == StorageKind.FileSystem);
        }

        private void command_OpenContentFolder(object sender, ExecutedRoutedEventArgs e)
        {
            var selected = wpList.Children.OfType<MediaItem>().FirstOrDefault(x => x.IsSelected);
            if (selected != null && selected.Tag is Content content && content.NonCollectionContentMedia != null && content.NonCollectionContentMedia.Storage != null)
            {
                try
                {
                    var folderPath = Path.Combine(content.NonCollectionContentMedia.Storage.BasePath, content.NonCollectionContentMedia.Path);
                    if (!content.NonCollectionContentMedia.IsDirectory)
                    {
                        Util.ShowInExplorer(folderPath);
                    }
                    else
                    {
                        Util.OpenFile(folderPath);
                    }
                }
                catch { }
            }
        }

        private void command_EditContent(object sender, ExecutedRoutedEventArgs e)
        {
            var selected = wpList.Children.OfType<MediaItem>().FirstOrDefault(x => x.IsSelected);
            if (selected != null && selected.Tag is Content content)
            {
                var ced = new ContentEditor(Service, content);
                if (ced.ShowDialog() == true)
                {
                    RefreshData(null);
                }
            }
        }

        private void command_DeleteContent(object sender, ExecutedRoutedEventArgs e)
        {
            var selected = wpList.Children.OfType<MediaItem>().FirstOrDefault(x => x.IsSelected);
            if (selected != null && selected.Tag is Content content
                && MessageBox.Show("Are you sure to delete this content?\r\n" + content.Title, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                try
                {
                    Service.DB.Delete(content);
                    var metaStorage = Service.DB.From<Storage>().Where(x => x.IsMetaStorage).FirstOrDefault();
                    if (content != null && metaStorage != null)
                    {
                        var cover = AliceMediaLibraryStatic.GetCoverPath(metaStorage.BasePath, content);
                        if (File.Exists(cover))
                        {
                            try
                            {
                                File.Delete(cover);
                            }
                            catch { }
                        }
                        var backdrop = AliceMediaLibraryStatic.GetBackdropPath(metaStorage.BasePath, content);
                        if (File.Exists(backdrop))
                        {
                            try
                            {
                                File.Delete(backdrop);
                            }
                            catch { }
                        }
                    }
                    MessageBox.Show("Content deleted successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to delete content!\r\n" + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    RefreshData();
                }
            }
        }


        private void check_CanNextPage(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = cbPage?.Items.Count > 0 && cbPage.SelectedIndex < cbPage.Items.Count - 1;
        }

        private void check_CanPrevPage(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = cbPage?.Items.Count > 0 && cbPage.SelectedIndex > 0;
        }

        private void command_NextPage(object sender, ExecutedRoutedEventArgs e)
        {
            if (cbPage?.Items.Count > 0 && cbPage.SelectedIndex < cbPage.Items.Count - 1)
            {
                cbPage.SelectedIndex++;
            }
        }

        private void command_Next10Page(object sender, ExecutedRoutedEventArgs e)
        {
            if (cbPage?.Items.Count > 0 && cbPage.SelectedIndex < cbPage.Items.Count - 1)
            {
                cbPage.SelectedIndex = Math.Min(cbPage.Items.Count - 1, cbPage.SelectedIndex + 10);
            }
        }

        private void command_PrevPage(object sender, ExecutedRoutedEventArgs e)
        {
            if (cbPage?.Items.Count > 0 && cbPage.SelectedIndex > 0)
            {
                cbPage.SelectedIndex--;
            }
        }

        private void command_Prev10Page(object sender, ExecutedRoutedEventArgs e)
        {
            if (cbPage?.Items.Count > 0 && cbPage.SelectedIndex > 0)
            {
                cbPage.SelectedIndex = Math.Max(0, cbPage.SelectedIndex - 10);
            }
        }
    }
}
