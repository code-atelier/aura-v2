﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for ImageCropper.xaml
    /// </summary>
    public partial class ImageCropper : Window
    {
        public ImageCropper()
        {
            InitializeComponent();
        }

        double ImageWidth { get; set; }
        double ImageHeight { get; set; }
        double BoxWidth { get; set; }
        double BoxHeight { get; set; }

        public void LoadImage(BitmapImage imageSource, int width, int height)
        {
            BoxWidth = width;
            BoxHeight = height;
            imgToCut.Source = imageSource;
            ImageWidth = imageSource.PixelWidth;
            ImageHeight = imageSource.PixelHeight;
            rdWidth.Width = new GridLength(BoxWidth);
            rdHeight.Height = new GridLength(BoxHeight);
            Width = 200 + BoxWidth;
            Height = 200 + BoxHeight;

            // get max of min scales
            var scale = Math.Max(BoxWidth / ImageWidth, BoxHeight / ImageHeight);
            imgToCut.Width = ImageWidth * scale;
            imgToCut.Height = ImageHeight * scale;
            imgToCut.Margin = new Thickness((Width - imgToCut.Width) / 2, (Height - imgToCut.Height) / 2, 0, 0);
        }

        private void imgToCut_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var mloc = e.GetPosition(imgToCut);
            var initX = imgToCut.Margin.Left;
            var initY = imgToCut.Margin.Top;
            var rx = mloc.X / imgToCut.Width;
            var ry = mloc.Y / imgToCut.Height;

            var delta = e.Delta;
            double cWidth = imgToCut.Width;
            double cHeight = imgToCut.Height;
            double rate = 1;
            if (delta > 0)
            {
                // zoom in
                rate = delta / 100d;
            }
            else if (delta < 0)
            {
                // zoom out
                rate = 1 / (delta * -1 / 100d);
            }
            // adjust rate
            double minWRate = BoxWidth / cWidth;
            double maxWRate = ImageWidth / cWidth;
            double minHRate = BoxHeight / cHeight;
            double maxHRate = ImageHeight / cHeight;
            rate = Math.Max(Math.Max(minWRate, minHRate), Math.Min(Math.Min(maxWRate, maxHRate), rate));
            imgToCut.Width = cWidth * rate;
            imgToCut.Height = cHeight * rate;

            var crx = mloc.X / imgToCut.Width;
            var cry = mloc.Y / imgToCut.Height;
            var drx = crx - rx;
            var dry = cry - ry;

            var dx = Math.Min(100, Math.Max(initX + drx * imgToCut.Width, ActualWidth - 100 - imgToCut.Width));
            var dy = Math.Min(100, Math.Max(initY + dry * imgToCut.Height, ActualHeight - 100 - imgToCut.Height));

            imgToCut.Margin = new Thickness(dx, dy, 0, 0);
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            imgToCut.Margin = new Thickness((ActualWidth - imgToCut.Width) / 2, (ActualHeight - imgToCut.Height) / 2, 0, 0);
        }

        bool _mouseDown = false;
        Point _initialMousePosition = new Point(0, 0);
        private void imgToCut_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _mouseDown = imgToCut.CaptureMouse();
            _initialMousePosition = e.GetPosition(imgToCut);
        }

        private void imgToCut_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_mouseDown)
            {
                imgToCut.ReleaseMouseCapture();
                _mouseDown = false;
            }
        }

        private void imgToCut_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseDown)
            {
                var pos = e.GetPosition(imgToCut) - _initialMousePosition;
                var x = imgToCut.Margin.Left;
                var y = imgToCut.Margin.Top;

                var dx = Math.Min(100, Math.Max(x + pos.X, ActualWidth - 100 - imgToCut.Width));
                var dy = Math.Min(100, Math.Max(y + pos.Y, ActualHeight - 100 - imgToCut.Height));

                imgToCut.Margin = new Thickness(dx, dy, 0, 0);
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public Rect? Result { get; private set; }
        private void btnCrop_Click(object sender, RoutedEventArgs e)
        {
            Result = new Rect((imgToCut.Margin.Left - 100) * -1 / imgToCut.Width * ImageWidth, (imgToCut.Margin.Top - 100) * -1 / imgToCut.Height * ImageHeight, BoxWidth / imgToCut.Width * ImageWidth, BoxHeight / imgToCut.Height * ImageHeight);
            DialogResult = true;
            Close();
        }
    }
}
