﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using AliceMediaLibrary.Models;
using AliceMediaLibrary.Repositories;
using Aura;
using Fluent; // https://fluentribbon.github.io/documentation/styles_since_8
using Path = System.IO.Path;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for MediaLibrary.xaml
    /// </summary>
    public partial class MediaLibrary : RibbonWindow
    {
        public string AppName { get => "Alice Media Library v" + AliceMediaLibraryStatic.Version; }

        public AliceMediaLibraryService Service { get; }

        private DispatcherTimer StateTimer { get; }

        public MediaLibrary(AliceMediaLibraryService service)
        {
            Service = service;
            _skipPageSelection = true;
            InitializeComponent();
            _skipPageSelection = false;
            LoadTheme();
            StateTimer = new DispatcherTimer();
            StateTimer.Interval = new TimeSpan(0, 0, 1);
            StateTimer.Tick += StateTimer_Tick;
            StateTimer.Start();
        }

        private void StateTimer_Tick(object? sender, EventArgs e)
        {
            var connected = Service.State == Aura.Services.ServiceState.Running || Service.State == Aura.Services.ServiceState.Busy;
            if (connected)
            {
                sbMiOnline.Text = "Online";
                sbMiOffline.Text = "";
            }
            else
            {
                sbMiOnline.Text = "";
                sbMiOffline.Text = "Offline";
            }
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(Path.GetFullPath(Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            UpdateTitle();
            RefreshSearchFilters(true);
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (Service.State != Aura.Services.ServiceState.Running && Service.State != Aura.Services.ServiceState.Busy)
            {
                if (MessageBox.Show("Alice Media Library Service is not running.\r\nDo you want to start it now?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.Yes) != MessageBoxResult.Yes)
                {
                    Close();
                    return;
                }
                else
                {
                    try
                    {
                        Task.Run(Service.Start).Wait();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        Close();
                        return;
                    }
                }
            }
            UpdateViewMargin();
        }

        private void RibbonWindow_Closed(object sender, EventArgs e)
        {
            StateTimer.Stop();
        }

        private async Task<SearchResult> PerformSearch(int? page = 1)
        {
            return await Task.Run(() =>
            {
                var ret = new SearchResult();
                string sort = "CD_DESC";
                int safeSearch = 0;
                List<int> ctIds = new List<int>();
                List<string> searchTerms = new List<string>();
                Dispatcher.Invoke(() =>
                {
                    ret.CurrentPage = Math.Max(1, page ?? (cbPage.SelectedItem is int pg ? pg : 1));
                    ret.ItemsPerPage = cbItemCount.SelectedItem is ComboBoxItem cbi && cbi.Tag is string count && int.TryParse(count, out int itemPerPage) ? itemPerPage : 10;
                    if (cbSort.SelectedItem is ComboBoxItem cbi2 && cbi2.Tag is string st)
                    {
                        sort = st;
                    }
                    safeSearch = cbSafeSearch.SelectedIndex;
                    ctIds.AddRange(rgbContentTypes.Items.OfType<System.Windows.Controls.CheckBox>().Where(x => x.IsChecked == true && x.Tag is int).Select(x => (int)x.Tag));
                    searchTerms = ftxSearch.Text.Split(new char[] { ' ' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries).ToList();
                });
                try
                {
                    var metaStorage = Service.DB.From<Storage>().Where(x => x.IsMetaStorage).FirstOrDefault();
                    var storages = Service.DB.From<Storage>().Where(x => !x.IsMetaStorage).ToList();
                    var contentTypes = Service.DB.From<ContentType>().ToList();
                    var query = Service.DB.From<Content>();

                    if (safeSearch == 0)
                    {
                        // safe
                        query = query.Where(x => x.ExplicitContent == false && x.ContentRating < ContentRating.Mature);
                    }
                    else if (safeSearch == 1)
                    {
                        // moderate
                        query = query.Where(x => x.ExplicitContent == false);
                    }

                    // filter content type
                    if (ctIds.Count > 0)
                    {
                        query = query.Where(x => ctIds.Contains(x.ContentTypeId));
                    }

                    // filter by term
                    foreach (var t in searchTerms)
                    {
                        var term = t.ToLower();
                        var negative = false;
                        if (term.StartsWith("-"))
                        {
                            if (term.Length == 1) continue;
                            negative = true;
                            term = term.Substring(1);
                        }

                        // tag
                        if (term.StartsWith("$"))
                        {
                            if (term.Length == 1) continue;
                            var tag = "|" + term.ToLower() + "|";
                            if (!negative)
                            {
                                query = query.Where(x => x.SearchTerm.Contains(tag));
                            }
                            else
                            {
                                query = query.Where(x => !x.SearchTerm.Contains(tag));
                            }
                        }
                        // flag
                        else if (term.StartsWith("#"))
                        {
                            if (term.Length == 1) continue;
                            term = term.Substring(1).ToLowerInvariant();
                            if (term == "explicit")
                            {
                                if (!negative)
                                {
                                    query = query.Where(x => x.ExplicitContent);
                                }
                                else
                                {
                                    query = query.Where(x => x.ExplicitContent == false);
                                }
                            }
                            if (term == "auto")
                            {
                                if (!negative)
                                {
                                    query = query.Where(x => x.FromAutoScan);
                                }
                                else
                                {
                                    query = query.Where(x => x.FromAutoScan == false);
                                }
                            }
                        }
                        // textual
                        else
                        {
                            if (!negative)
                            {
                                query = query.Where(x => x.Title.Contains(term) || x.OriginalTitle.Contains(term) || x.SearchTerm.Contains(term));
                            }
                            else
                            {
                                query = query.Where(x => !(x.Title.Contains(term) || x.OriginalTitle.Contains(term) || x.SearchTerm.Contains(term)));
                            }
                        }
                    }

                    ret.TotalCount = query.Count(false);
                    query = query.Take(ret.ItemsPerPage).Skip(ret.RowNumberStart - 1);

                    var spl = sort.Split('_');
                    if (spl.Length == 2)
                    {
                        var sortBy = spl[0];
                        var sortDir = spl[1];
                        if (sortBy == "MD")
                            query = query.OrderBy(x => x.UpdatedAt, sortDir == "DESC" ? Amaterasu.Database.QuerySortDirection.Descending : Amaterasu.Database.QuerySortDirection.Ascending);
                        if (sortBy == "CD")
                            query = query.OrderBy(x => x.CreatedAt, sortDir == "DESC" ? Amaterasu.Database.QuerySortDirection.Descending : Amaterasu.Database.QuerySortDirection.Ascending);
                        if (sortBy == "TT")
                            query = query.OrderBy(x => x.Title, sortDir == "DESC" ? Amaterasu.Database.QuerySortDirection.Descending : Amaterasu.Database.QuerySortDirection.Ascending);
                        if (sortBy == "RT")
#pragma warning disable CS8603 // Possible null reference return.
                            query = query.OrderBy(x => x.Rating, sortDir == "DESC" ? Amaterasu.Database.QuerySortDirection.Descending : Amaterasu.Database.QuerySortDirection.Ascending);
#pragma warning restore CS8603 // Possible null reference return.
                    }

                    var result = query.ToList();
                    ret.Contents = result;
                    foreach (var content in result)
                    {
                        try
                        {
                            var ct = contentTypes.Where(x => x.Id == content.ContentTypeId).FirstOrDefault();
                            if (ct != null)
                            {
                                content.ContentType = ct;
                                if (!ct.IsCollection)
                                {
                                    var media = Service.DB.From<Media>().Where(x => x.ContentId == content.Id).FirstOrDefault();
                                    if (media != null)
                                    {
                                        content.NonCollectionContentMedia = media;
                                        media.Storage = storages.FirstOrDefault(x => x.Id == media.StorageId);
                                    }
                                }
                                else
                                {
                                    throw new NotImplementedException();
                                }
                            }
                        }
                        catch { }
                    }
                }
                catch { }
                return ret;
            });
        }

        public class SearchResult
        {
            public List<Content> Contents { get; set; } = new List<Content>();

            public int TotalCount { get; set; }

            public int TotalInPage { get => Contents.Count; }

            public int PageCount { get => (int)Math.Ceiling(TotalCount / (double)ItemsPerPage); }

            public int CurrentPage { get; set; } = 1;

            public int RowNumberStart { get => (CurrentPage - 1) * ItemsPerPage + 1; }

            public int RowNumberEnd { get => (CurrentPage - 1) * ItemsPerPage + TotalInPage; }

            public int ItemsPerPage { get; set; } = 10;
        }

        private void Mi_OnSelected(object? sender, EventArgs e)
        {
            if (sender is MediaItem mi && mi.Tag is Content content)
            {
                tiContent.IsSelected = true;
                rbgSelectedContent.Visibility = Visibility.Visible;
            }
        }

        Dictionary<string, ImageSource> ImageCache { get; } = new Dictionary<string, ImageSource>();
        private async void LoadImageFor(MediaItem mi, string path)
        {
            await Task.Run(() =>
            {
                try
                {
                    var lcPath = path.Trim().ToLower();
                    if (ImageCache.ContainsKey(lcPath))
                    {
                        Dispatcher.Invoke(() =>
                        {
                            mi.Cover = ImageCache[lcPath];
                        });
                    }
                    else
                    {
                        var buffer = File.ReadAllBytes(path);
                        Dispatcher.Invoke(() =>
                        {
                            var localBuffer = new byte[buffer.Length];
                            Array.Copy(buffer, localBuffer, buffer.Length);
                            using (var ms = new MemoryStream(localBuffer))
                            {
                                var bmp = new BitmapImage();
                                bmp.BeginInit();
                                bmp.StreamSource = ms;
                                bmp.CacheOption = BitmapCacheOption.OnLoad;
                                bmp.EndInit();
                                ImageCache[lcPath] = bmp;
                                mi.Cover = bmp;
                            }
                        });
                    }
                }
                catch { }
            });
        }

        bool _skipPageSelection = false;
        private void cbPage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_skipPageSelection) return;
            RefreshData(null);
        }

        private void ftxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                RefreshData();
            }
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Alice Media Library v" + AliceMediaLibraryStatic.Version + "\r\nCopyright©2024 Code Atelier - All Rights Reserved", "About", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnCoatl_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Open Code Atelier website?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    Util.OpenFile("https://codeatelier.id");
                }
                catch { }
            }
        }
    }
}
