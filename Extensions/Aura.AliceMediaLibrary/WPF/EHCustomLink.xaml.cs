﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for EHCustomLink.xaml
    /// </summary>
    public partial class EHCustomLink : Window
    {
        public EHCustomLink()
        {
            InitializeComponent();
            if (Clipboard.ContainsText())
            {
                txLink.Text = Clipboard.GetText();
            }
        }

        public string? Result { get; private set; }
        public string? ResultName { get; private set; }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            btnAdd.IsEnabled = false;
            string url = txLink.Text.Trim();
            Add(url);
        }

        private async void Add(string url)
        {
            try
            {
                string pattern = @"^(https\:\/\/(?>exhentai|e-hentai)\.org)\/g\/(\w+)\/(\w+)\/?$";

                Match m = Regex.Match(url, pattern);
                if (m.Success)
                {
                    try
                    {
                        var client = new HttpClient();
                        var html = await client.GetStringAsync(url);
                        if (!string.IsNullOrWhiteSpace(html))
                        {
                            pattern = @"\<\s*title\s*\>([^\<]+)\<\/\s*title\s*\>";
                            Match mt = Regex.Match(html, pattern);
                            if (mt.Success)
                            {
                                ResultName = mt.Groups[1].Value;
                            }
                        }
                    }
                    catch { }
                    Dispatcher.Invoke(() =>
                    {
                        Result = url;
                        DialogResult = true;
                        Close();
                    });
                }
                else
                {
                    MessageBox.Show("Please enter EH or EXH link", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    btnAdd.IsEnabled = true;
                });
            }
        }
    }
}
