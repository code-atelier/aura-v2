﻿using AliceMediaLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Google.Protobuf.WellKnownTypes.Field.Types;
using System.Xml.Linq;

namespace AliceMediaLibrary.WPF
{
    /// <summary>
    /// Interaction logic for TagManager.xaml
    /// </summary>
    public partial class TagManager : Window
    {
        AliceMediaLibraryService Service { get; }

        TagModel? CurrentData { get; set; }

        public TagManager(AliceMediaLibraryService service)
        {
            InitializeComponent();
            spEditor.IsEnabled = gbEditor.IsEnabled = false;
            Service = service;
            LoadTheme();
            RefreshDataGrid();
        }

        private void LoadTheme()
        {
            try
            {
                var themeRes = new ResourceDictionary();
                themeRes.Source = new Uri(System.IO.Path.GetFullPath(System.IO.Path.Combine(AliceMediaLibraryStatic.ThemePath, Service.Configuration.Theme ?? "default.xaml")));
                Resources.MergedDictionaries.Add(themeRes);
            }
            catch
            {
            }
        }

        public void RefreshDataGrid()
        {
            try
            {
                var term = txSearch.Text.Trim();
                var contentTypes = Service.DB.From<TagModel>().Where(x => x.Tag.Contains(term)).OrderBy(x => x.Tag, Amaterasu.Database.QuerySortDirection.Ascending).ToList();
                dgData.ItemsSource = contentTypes;
            }
            catch
            {
            }
        }

        public void ReadData(TagModel? data = null)
        {
            txTag.Clear();
            txPriority.Clear();
            cxExplicit.IsChecked = false;
            if (data == null) return;
            txTag.Text = data.Tag;
            txPriority.Text = data.Priority.ToString();
            cxExplicit.IsChecked = data.IsExplicit;
        }

        public bool WriteData()
        {
            if (CurrentData == null) return false;

            try
            {
                var tag = txTag.Text.ToLower().Trim();
                if (string.IsNullOrWhiteSpace(tag))
                    throw new Exception("Tag cannot be empty");
                var parsed = AMLHelper.ParseTag(tag);
                if (parsed == null)
                    throw new Exception("Invalid tag format");
                int priority;
                if (!int.TryParse(txPriority.Text, out priority))
                    throw new Exception("Priority must be an integer");

                CurrentData.Tag = tag;
                if (parsed.Length == 2)
                {
                    CurrentData.Key = parsed[0];
                    CurrentData.Value = parsed[1];
                }
                else
                {
                    CurrentData.Key = null;
                    CurrentData.Value = parsed[0];
                }
                CurrentData.Priority = priority;
                CurrentData.IsExplicit = cxExplicit.IsChecked == true;
                CurrentData.UpdatedAt = DateTime.UtcNow;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        public void EditData(TagModel data)
        {
            ReadData(data);
            CurrentData = data;
            spEditor.IsEnabled = gbEditor.IsEnabled = true;
            spData.IsEnabled = gbData.IsEnabled = false;
        }

        public bool DeleteData(TagModel data)
        {
            try
            {
                return Service.DeleteTag(data);
            }
            catch
            {
                return false;
            }
        }

        public bool SaveCurrentData()
        {
            try
            {
                if (CurrentData == null) return false;
                return Service.SaveTag(CurrentData);
            }
            catch
            {
                return false;
            }
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            EditData(new TagModel()
            {
                Tag = "",
                CreatedAt = DateTime.UtcNow
            });
            txTag.IsEnabled = true;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (dgData.SelectedItem is TagModel data)
            {
                EditData(data);
            }
            txTag.IsEnabled = false;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(string.IsNullOrWhiteSpace(CurrentData?.Tag) ? "Cancel creating this data?" : "Cancel editing this data?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                return;
            }
            spEditor.IsEnabled = gbEditor.IsEnabled = false;
            spData.IsEnabled = gbData.IsEnabled = true;
            txTag.IsEnabled = true;
            ReadData(null);
            RefreshDataGrid();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!WriteData())
            {
                return;
            }
            if (MessageBox.Show("Save this data?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                return;
            }
            if (SaveCurrentData())
            {
                MessageBox.Show("Data saved successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                spEditor.IsEnabled = gbEditor.IsEnabled = false;
                spData.IsEnabled = gbData.IsEnabled = true;
                ReadData(null);
                RefreshDataGrid();
            }
            else
            {
                MessageBox.Show("Failed to save data!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dgData.SelectedItem is TagModel data)
            {
                var count = Service.CountTagUsage(data);
                if (MessageBox.Show("Are you sure to delete this data?" + Environment.NewLine + $"There are {count} data currently using this tag.", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                {
                    return;
                }
                if (DeleteData(data))
                {
                    MessageBox.Show("Data deleted successfully!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReadData(null);
                    RefreshDataGrid();
                }
                else
                {
                    MessageBox.Show("Failed to delete data!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void dgData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgData.SelectedItem is TagModel data)
            {
                ReadData(data);
            }
            else
            {
                ReadData(null);
            }
        }

        private void txSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                RefreshDataGrid();
            }
        }
    }
}
