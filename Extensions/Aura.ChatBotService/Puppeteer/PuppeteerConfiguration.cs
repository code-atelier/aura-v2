﻿using PuppeteerSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.ChatBotService.Puppeteer
{
    public class PuppeteerConfiguration
    {
        /// <summary>
        /// Where puppeteer will run. When null, default directory is used.
        /// </summary>
        public string? WorkingDirectory { get; set; }

        /// <summary>
        /// User data directory
        /// </summary>
        public string? UserDataDirectory { get; set; }

        /// <summary>
        /// Browser type to run
        /// </summary>
        public SupportedBrowser BrowserType { get; set; } = SupportedBrowser.Chrome;

        public bool Headless { get; set; } = false;

        public Dictionary<string, string> DefaultHeaders { get; } = new Dictionary<string, string>();
    }
}
