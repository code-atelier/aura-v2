﻿using PuppeteerSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Aura.ChatBotService.Puppeteer
{
    public class PuppeteerResponse
    {
        public string Content { get; private set; } = string.Empty;

        public HttpStatusCode StatusCode { get; private set; }

        private PuppeteerResponse()
        {
        }

        public static async Task<PuppeteerResponse> FromAsync(IResponse response)
        {
            var res = new PuppeteerResponse();
            res.Content = await response.TextAsync();
            res.StatusCode = response.Status;
            return res;
        }
    }
}
