﻿using PuppeteerExtraSharp;
using PuppeteerExtraSharp.Plugins.ExtraStealth;
using PuppeteerExtraSharp.Plugins.ExtraStealth.Evasions;
using PuppeteerSharp;
using System.Diagnostics;
using System.IO;
using System.Net.Http;

namespace Aura.ChatBotService.Puppeteer
{
    public class PuppeteerController
    {
        /// <summary>
        /// Working directory of puppeteer
        /// </summary>
        public string WorkingDirectory { get; private set; }

        public SupportedBrowser BrowserType { get; private set; }

        public string? BrowserExecutablePath { get; private set; }

        public bool IsReady { get; private set; }

        public bool IsBusy { get; private set; }

        public IBrowser? Browser { get; private set; }

        public PuppeteerConfiguration Configuration { get; private set; }

        public PuppeteerController(PuppeteerConfiguration? configuration = null)
        {
            BrowserType = configuration?.BrowserType ?? SupportedBrowser.Chrome;
            WorkingDirectory = configuration?.WorkingDirectory ?? Path.Combine(Directory.GetCurrentDirectory(), "temp", "puppeteer");
            Configuration = configuration ?? new PuppeteerConfiguration();
        }

        bool initialized = false;

        public async Task Initialize()
        {
            if (initialized) return;
            IsBusy = true;
            try
            {
                BrowserExecutablePath = await DownloadBrowserAsync(WorkingDirectory, BrowserType);
                if (BrowserExecutablePath == null)
                {
                    throw new Exception("Failed to download browser");
                }
                initialized = true;
                IsReady = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public static async Task<string?> DownloadBrowserAsync(string path, SupportedBrowser browserType)
        {
            try
            {
                using (var browserFetcher = new BrowserFetcher(new BrowserFetcherOptions() { Path = path, Browser = browserType }))
                {
                    var browsers = browserFetcher.GetInstalledBrowsers();
                    var browser = browsers.FirstOrDefault(x => x.Browser == browserType);
                    if (browser != null)
                    {
                        return browser.GetExecutablePath();
                    }

                    browser = await browserFetcher.DownloadAsync();
                    return browser.GetExecutablePath();
                }
            }
            catch { }
            return null;
        }

        public async Task StartBrowserAsync()
        {
            if (IsBusy || !File.Exists(BrowserExecutablePath)) return;
            await StopBrowserAsync();
            IsBusy = true;
            try
            {
                var pupExtra = new PuppeteerExtra();
                var stealthPlugin = new StealthPlugin(new StealthHardwareConcurrencyOptions(1));

                var chromeArgs = new[]
                {
                    "--no-default-browser-check", 
                    "--no-sandbox", 
                    "--disable-setuid-sandbox", 
                    "--no-first-run", 
                    "--single-process",
                    "--disable-default-apps", 
                    "--disable-features=Translate", 
                    "--disable-infobars", 
                    "--disable-dev-shm-usage",
                    "--mute-audio", 
                    "--ignore-certificate-errors", 
                    "--use-gl=egl",
                    "--user-agent=\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36\""
                };

                Browser = await pupExtra.Use(stealthPlugin).LaunchAsync(new()
                {
                    Headless = Configuration.Headless,
                    ExecutablePath = BrowserExecutablePath,
                    IgnoredDefaultArgs = new[] { "--disable-extensions" },
                    Args = BrowserType == SupportedBrowser.Chrome ? chromeArgs : null,
                    Timeout = 1_200_000,
                    UserDataDir = Configuration.UserDataDirectory,
                });
                IsReady = true;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task StopBrowserAsync()
        {
            if (IsBusy) return;
            IsBusy = true;
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        IsReady = false;
                        var runningProcesses = Process.GetProcesses();
                        var browserProcess = runningProcesses.FirstOrDefault(p =>
                            p.ProcessName.ToLower() == BrowserType.ToString().ToLower()
                            && p.MainModule != null
                            && p.MainModule.FileName.ToLower() == BrowserExecutablePath?.ToLower()
                        );
                        if (browserProcess != null)
                        {
                            browserProcess.Kill();
                        }
                    }
                    catch { }
                });
            }
            catch { }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task<PuppeteerResponse?> GetRequest(string url, Dictionary<string,string>? headers = null)
        {
            var page = await OpenNewPageWithIntercept();
            if (page == null) return null;

            page.Request += (s, e) => MutateRequest(e, null, HttpMethod.Get, "application/json", headers);

            var response = await page.GoToAsync(url);
            var content = await response.TextAsync();
            _ = page.CloseAsync();
            return await PuppeteerResponse.FromAsync(response);
        }

        private async void MutateRequest(RequestEventArgs args, dynamic? data, HttpMethod method, string contentType, Dictionary<string, string>? headers = null)
        {
            var r = args.Request;

            var cHeader = new Dictionary<string, string>(); 
            foreach(var hdr in Configuration.DefaultHeaders)
            {
                cHeader[hdr.Key] = hdr.Value;
            }
            if (headers != null)
            {
                foreach (var hdr in headers)
                {
                    cHeader[hdr.Key] = hdr.Value;
                }
            }

            string? postData = null;
            var payload = new Payload() { Method = method, Headers = cHeader, PostData = postData };

            await r.ContinueAsync(payload);
        }

        public async Task<IPage?> OpenNewPageAsync()
        {
            if (!IsReady || Browser == null) return null;
            return await Browser.NewPageAsync();
        }

        protected async Task<IPage?> OpenNewPageWithIntercept()
        {
            if (!IsReady || Browser == null) return null;
            var page = await Browser.NewPageAsync();
            await page.SetRequestInterceptionAsync(true);
            return page;
        }

        public async Task<bool> HookWebSocket(IPage page, string script)
        {
            var wsPrototypeHandle = await page.EvaluateExpressionHandleAsync("WebSocket.prototype");
            var wsInstances = await page.QueryObjectsAsync(wsPrototypeHandle);
            await page.EvaluateFunctionAsync(@"(instances) => {
                let ws = instances.find(x => x.readyState == 1);
                if (ws) {
                " + script + @"
                }
            }", wsInstances);
            return true;
        }
    }
}
