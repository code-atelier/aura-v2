﻿using Aura.ChatBotService.CAI;
using Aura.ChatBotService.Puppeteer;
using Aura.Models;
using Aura.Services;
using Aura.Services.Passive;
using PuppeteerSharp;
using System.Text.Json;
using System.IO;
using System.Windows;
using Aura.WPF;
using System.Configuration;

namespace Aura.ChatBotService
{
    public class CharacterAIService : PassiveServiceBase, IDisposable, IChatbotService, IRetryableChatbotService, IServiceWithConfiguration, IMessageReceiver
    {
        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = "coatl.characterai",
            Name = "Character.AI",
            Author = "CodeAtelier",
            Description = "Allows you to chat with Character.ai bot.",
            Version = new Version(1, 0, 0),
            Icon = Util.CreateResourceUri("res/cai.png"),
        };

        public override ServiceUserAction UserActions => ServiceUserAction.All;

        public PuppeteerController Puppeteer { get; }

        public IPage? Page { get; private set; }

        protected ConfigurationService ConfigurationService { get; private set; }

        public CharacterAIConfig Config { get; }

        private Queue<GetTurnsResponse.TurnModel> ReceivedMessages { get; } = new Queue<GetTurnsResponse.TurnModel>();

        public Queue<ChatbotServiceMessage> ChatHistory { get; } = new Queue<ChatbotServiceMessage>();

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public string Id { get => Info.Id; }

        private IServiceMessageBus ServiceMessageBus { get; }

        public bool CanReceiveMessage { get => true; }

        public CharacterAIService(ConfigurationService configurationService, IServiceMessageBus serviceMessageBus)
        {
            ConfigurationService = configurationService;
            Config = ConfigurationService.LoadConfig<CharacterAIConfig>() ?? new CharacterAIConfig();
            var config = new PuppeteerConfiguration()
            {
                BrowserType = SupportedBrowser.Chrome,
                WorkingDirectory = Path.GetFullPath(Path.Combine("puppeteer", "p-cai")),
                UserDataDirectory = Path.GetFullPath(Path.Combine("puppeteer", "p-cai-userdata")),
                Headless = true,
            };
            if (Config.UseCaiPlus && Config.CaiPlusToken != null)
                config.DefaultHeaders.Add("authorization", "Token " + Config.CaiPlusToken);
            config.DefaultHeaders["user-agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36";
            Puppeteer = new PuppeteerController(config);
            ServiceMessageBus = serviceMessageBus;
        }

        #region IService
        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                ServiceMessageBus.Subscribe(this, InternalEvents.UriRequest);
            });
        }

        public override async Task OnStart()
        {
            try
            {
                await Puppeteer.Initialize();
                await Puppeteer.StartBrowserAsync();

                if (!(await CheckSignedIn()))
                {
                    await PromptSignIn();
                }

                Page = await Puppeteer.OpenNewPageAsync();
                if (Page == null)
                {
                    throw new Exception("Failed to create chat page");
                }
                var response = await Page.GoToAsync(CreateCaiLink("chat2?char=" + Config.CharacterId));
                if (response.Status != System.Net.HttpStatusCode.OK)
                {
                    throw new Exception("Failed to access character page");
                }
                try
                {
                    await Page.WaitForRequestAsync(
                        x => x.Url.EndsWith("/resurrect"),
                        new WaitForOptions()
                        {
                            Timeout = Config.NetworkDelay * 1000,
                        }
                    );
                    await Page.WaitForSelectorAsync("textarea#user-input", new WaitForSelectorOptions()
                    {
                        Timeout = Config.NetworkDelay * 1000,
                    });
                }
                catch { }
                await RehookWebSocket();
                Page.Console += Page_Console;
                ReceivedMessages.Clear();
            }
            catch
            {
                throw;
            }
        }

        public override async Task OnShutdown()
        {
            await Puppeteer.StopBrowserAsync();
        }
        #endregion

        #region Puppeteer Control
        private async Task RehookWebSocket()
        {
            if (Page != null)
            {
                await Puppeteer.HookWebSocket(Page,
                    @"window.caiWebSocket = ws;/* 
if (!window.caiWebSocketSendFunc) {
    window.caiWebSocketSendFunc = ws.send;
    ws.send = function(data) { 
        console.log('ws:sent',data); 
        window.caiWebSocketSendFunc.apply(this, [data]); 
    }; 
}*/
ws.addEventListener(""message"", (e) => console.log('ws:receive', e));"
                );
            }
        }

        private async Task SendMessage(string msg)
        {
            if (Page != null)
            {
                var script = "document.getElementById(\"user-input\").focus();";
                await Page.EvaluateExpressionAsync(script);
                await Page.Keyboard.SendCharacterAsync(msg);
                script = "document.querySelectorAll(\"button\").forEach((x,e) => { if (x.title == \"Submit Message\") x.click(); })";
                await Page.EvaluateExpressionAsync(script);
            }
        }

        protected async Task<GetRecentChatResponse?> GetRecentChat(string characterId)
        {
            var link = Config.UseNeo ? CreateCaiLink("chats/recent/" + characterId, true) : throw new NotImplementedException();
            if (link == null) return null;
            var headers = new Dictionary<string, string>()
            {
                { "accept", "application/json, text/plain, */*" },
                { "accept-encoding", "gzip, deflate, br" },
                { "content-type", "application/json" },
            };
            var response = await Puppeteer.GetRequest(link, headers);
            if (response == null) return null;
            return JsonSerializer.Deserialize<GetRecentChatResponse>(response.Content);
        }

        protected async Task<GetTurnsResponse?> GetTurns(string chatId)
        {
            var link = Config.UseNeo ? CreateCaiLink("turns/" + chatId, true) : throw new NotImplementedException();
            if (link == null) return null;
            var headers = new Dictionary<string, string>()
            {
                { "accept", "application/json, text/plain, */*" },
                { "accept-encoding", "gzip, deflate, br" },
                { "content-type", "application/json" },
            };
            var response = await Puppeteer.GetRequest(link, headers);
            if (response == null) return null;
            return JsonSerializer.Deserialize<GetTurnsResponse>(response.Content);
        }

        private async Task<bool> CheckSignedIn()
        {
            if (Puppeteer.Browser == null) return false;
            Page = (await Puppeteer.Browser.PagesAsync()).First();
            var charaPage = CreateCaiLink("search?");
            await Page.GoToAsync(charaPage);
            var isNotLoggedIn = false;
            try
            {
                try
                {
                    await Page.WaitForNavigationAsync(new NavigationOptions()
                    {
                        Timeout = Config.NetworkDelay * 1000,
                    });
                }
                catch { }
                await Page.WaitForSelectorAsync(".subscription-modal", new WaitForSelectorOptions()
                {
                    Timeout = Config.NetworkDelay * 1000,
                });
                isNotLoggedIn = true;
            }
            catch
            {
            }
            return !isNotLoggedIn;
        }

        private async Task PromptSignIn()
        {
            Puppeteer.Configuration.Headless = false;
            await Puppeteer.StartBrowserAsync();
            if (Puppeteer.Browser != null)
            {
                var page = (await Puppeteer.Browser.PagesAsync()).First();
                var maxWait = Config.SigninTimer;

                await page.SetContentAsync("Please sign in to character.ai => <a href=\"https://character.ai\" target=\"_blank\">click here</a><br />After logged in, close this browser.<br />You have " + maxWait + " seconds to do this.");

                var cWait = 0;
                while (!Puppeteer.Browser.IsClosed)
                {
                    await Task.Delay(1000);
                    cWait++;
                    if (cWait >= maxWait)
                    {
                        throw new Exception("User not signing in");
                    }
                }
                await Puppeteer.StartBrowserAsync();
                if (await CheckSignedIn())
                {
                    return;
                }
            }

            throw new Exception("Not signed in");
        }

        private async void Page_Console(object? sender, ConsoleEventArgs e)
        {
            if (e.Message.Args?.Count == 2 && Page != null)
            {
                try
                {
                    var farg = (await e.Message.Args[0].JsonValueAsync()).ToString();
                    if (farg == "ws:receive")
                    {
                        var sarg = await Page.EvaluateFunctionAsync<string>("(e) => e.data", e.Message.Args[1]);
                        var turn = JsonSerializer.Deserialize<WebSocketTurnMessage>(sarg);
                        if (turn != null && turn.Turn.Author.AuthorId == Config.CharacterId && turn.Turn.GetPrimaryCandidate() != null)
                        {
                            var final = turn.Turn.GetPrimaryCandidate();
                            if (final != null && !ReceivedMessages.Any(x => x.GetPrimaryCandidate()?.CandidateId == final.CandidateId))
                            {
                                ReceivedMessages.Enqueue(turn.Turn);
                            }
                        }
                    }
                }
                catch
                {

                }
            }
        }
        #endregion

        #region IChatbotService
        public async Task<bool> LoadChatHistory()
        {
            if (State != ServiceState.Running) return false;
            try
            {
                State = ServiceState.Busy;
                if (Config.CharacterId != null)
                {
                    var recent = await GetRecentChat(Config.CharacterId);
                    if (recent != null && recent.Chats.Count > 0)
                    {
                        var turns = await GetTurns(recent.Chats.First().ChatId);
                        if (turns != null && turns.Turns.Count > 0)
                        {
                            ChatHistory.Clear();
                            foreach (var turn in turns.Turns.OrderBy(x => x.CreateTime))
                            {
                                var candidate = turn.GetPrimaryCandidate();
                                if (candidate != null)
                                {
                                    if (turn.Author.AuthorId == Config.CharacterId)
                                    {
                                        var msg = new ChatbotServiceBotMessage()
                                        {
                                            Message = candidate.RawContent,
                                            MessageId = candidate.CandidateId,
                                            Timestamp = candidate.CreateTime ?? DateTime.Now,
                                            AuthorName = turn.Author.Name,
                                            AuthorId = turn.Author.AuthorId,
                                        };
                                        ChatHistory.Enqueue(msg);
                                    }
                                    else
                                    {
                                        var msg = new ChatbotServiceUserMessage()
                                        {
                                            Message = candidate.RawContent,
                                            MessageId = candidate.CandidateId,
                                            Timestamp = candidate.CreateTime ?? DateTime.Now,
                                            AuthorName = turn.Author.Name,
                                            AuthorId = turn.Author.AuthorId,
                                        };
                                        ChatHistory.Enqueue(msg);
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch { }
            finally
            {
                State = ServiceState.Running;
            }
            return false;
        }

        public async Task<ChatbotServiceBotMessage?> GetGreeting()
        {
            return await Task.Run(() =>
            {
                return null as ChatbotServiceBotMessage;
            });
        }

        public async Task<ChatbotServiceBotMessage> GetNewResponseFor(ChatbotServiceBotMessage message)
        {
            return await Task.Run(() =>
            {
                return new ChatbotServiceBotMessage()
                {
                    IsSuccessful = false,
                };
            });
        }

        public async Task<IEnumerable<ChatbotServiceBotMessage>?> SendMessage(ChatbotServiceUserMessage message)
        {
            if (State != ServiceState.Running)
                throw new Exception($"'{Info.Id}' is " + State.ToString().ToLower());
            if (!string.IsNullOrWhiteSpace(message.Message))
            {
                await SendMessage(message.Message);
                int timeout = 0;
                while (timeout < Config.NetworkDelay * 10)
                {
                    if (ReceivedMessages.Count > 0)
                    {
                        var res = new List<ChatbotServiceBotMessage>();
                        GetTurnsResponse.TurnModel? msg;
                        while (ReceivedMessages.TryDequeue(out msg))
                        {
                            var candidate = msg.GetPrimaryCandidate();
                            if (candidate != null)
                            {
                                var sbm = new ChatbotServiceBotMessage()
                                {
                                    IsSuccessful = true,
                                    Message = candidate.RawContent,
                                    AuthorName = msg.Author.Name,
                                    MessageId = candidate.CandidateId,
                                    Timestamp = msg.CreateTime ?? DateTime.Now,
                                };
                                ChatHistory.Enqueue(sbm);
                                res.Add(sbm);
                            }
                        }
                        return res;
                    }
                    await Task.Delay(1000);
                    timeout++;
                }
                throw new TimeoutException();
            }
            return null;
        }
        #endregion

        #region IDisposable
        public async void Dispose()
        {
            try
            {
                await Puppeteer.StopBrowserAsync();
            }
            catch { }
        }
        #endregion

        #region Utilities
        protected string CreateCaiLink(string resource, bool neo = false)
        {
            if (neo)
            {
                return CreateNeoCaiLink(resource);
            }
            return $"https://{(Config.UseCaiPlus ? "plus" : "beta")}.character.ai/" + resource;
        }

        protected string CreateNeoCaiLink(string resource)
        {
            return "https://neo.character.ai/" + resource;
        }

        private CookieParam[] ParseCookie(string cookie)
        {
            var res = new List<CookieParam>();
            var spl = cookie.Split(';', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            foreach (var kv in spl)
            {
                var sp2 = kv.Split('=', 2);
                if (sp2.Length == 2)
                {
                    res.Add(new CookieParam()
                    {
                        Domain = ".character.ai",
                        Path = "/",
                        Name = sp2[0],
                        Value = sp2[1],
                        SameSite = SameSite.Strict,
                        HttpOnly = true,
                    });
                }
            }
            return res.ToArray();
        }

        CharacterAIConfigurationWindow? ConfigWindow;
        public Window CreateConfigurationWindow()
        {
            var ac = new AutoConfigurator();
            ac.LoadConfiguration(Config);
            ac.Closed += ConfigWindow_Closed;
            return ac;
            /*
            ConfigWindow = new CharacterAIConfigurationWindow(ConfigurationService, Config);
            ConfigWindow.Closed += ConfigWindow_Closed;
            return ConfigWindow;*/
        }

        private void ConfigWindow_Closed(object? sender, EventArgs e)
        {
            ConfigurationService.SaveConfig<CharacterAIConfig>();
            ConfigWindow = null;
        }

        public async Task<Message?> ReceiveMessageAsync(Message message, CancellationToken cancellationToken)
        {
            var uriStart = "aura:services/" + Info.Id + "/";
            return await Task.Run(() =>
            {
                if (message.EventId == InternalEvents.UriRequest && message.HasBody<UriRequest>())
                {
                    var uriReq = message.GetBody<UriRequest>()?.Uris.Select(x => x.ToString()).Where(x => x.ToLower().StartsWith(uriStart)).Select(x => x.Substring(uriStart.Length))
                    .Distinct(StringComparer.OrdinalIgnoreCase).FirstOrDefault()?.Trim().ToLower();
                    if (!string.IsNullOrWhiteSpace(uriReq))
                    {
                        if (uriReq.StartsWith("config?") && ConfigWindow == null)
                        {
                            ServiceManager.MainDispatcher?.Invoke(() =>
                            {
                                var w = CreateConfigurationWindow();
                                if (w is CharacterAIConfigurationWindow wcai)
                                {
                                    wcai.LoadConfiguration(uriReq.Substring(7));
                                    wcai.Closed += ConfigWindow_Closed;
                                    wcai.ShowDialog();
                                }
                                if (w is AutoConfigurator ac)
                                {
                                    ac.LoadConfiguration(Config);
                                    ac.Closed += ConfigWindow_Closed;
                                    ac.ShowDialog();
                                }
                            });
                        }
                    }
                }
                return null as Message;
            });
        }
        #endregion
    }
}