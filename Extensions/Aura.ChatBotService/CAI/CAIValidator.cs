﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.ChatBotService.CAI
{
    public class CAIValidator : IAutoConfigValidator
    {
        public object? Format(object? value)
        {
            if (value is string charaid)
            {
                try
                {
                    var uri = new Uri(charaid);
                    if (uri.Query != null)
                    {
                        var queries = uri.Query.TrimStart('?').Split('&').Select(x => x.Split('=', 2)).ToDictionary(x => x[0], x => x.Length == 1 ? "" : x[1]);
                        if (queries.ContainsKey("char"))
                        {
                            charaid = queries["char"];
                        }
                    }
                    return charaid;
                }
                catch { }
            }
            return null;
        }

        public bool Validate(object? value, object? configurationObject)
        {
            return !string.IsNullOrWhiteSpace(value?.ToString());
        }
    }
}
