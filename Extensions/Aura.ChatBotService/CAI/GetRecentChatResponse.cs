﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.ChatBotService.CAI
{
    public class GetRecentChatResponse
    {
        public class Chat
        {
            [JsonPropertyName("chat_id")]
            public string ChatId { get; set; } = string.Empty;

            [JsonPropertyName("character_id")]
            public string CharacterId { get; set; } = string.Empty;

            [JsonPropertyName("character_name")]
            public string CharacterName { get; set; } = string.Empty;

            [JsonPropertyName("character_avatar_uri")]
            public string CharacterAvatarUri { get; set; } = string.Empty;
        }

        [JsonPropertyName("chats")]
        public List<Chat> Chats { get; set; } = new List<Chat>();
    }
}
