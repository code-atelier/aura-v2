﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.ChatBotService.CAI
{
    public class GetTurnsResponse
    {
        [JsonPropertyName("turns")]
        public List<TurnModel> Turns { get; set; } = new List<TurnModel>();

        [JsonPropertyName("meta")]
        public MetaModel Meta { get; set; } = new MetaModel();


        public class TurnModel
        {
            [JsonPropertyName("turn_key")]
            public TurnKeyModel TurnKey { get; set; } = new TurnKeyModel();

            [JsonPropertyName("create_time")]
            public DateTime? CreateTime { get; set; }

            [JsonPropertyName("last_update_time")]
            public DateTime? LastUpdateTime { get; set; }

            [JsonPropertyName("state")]
            public string State { get; set; } = string.Empty;

            [JsonPropertyName("author")]
            public AuthorModel Author { get; set; } = new AuthorModel();

            [JsonPropertyName("candidates")]
            public List<CandidateModel> Candidates { get; set; } = new List<CandidateModel>();

            [JsonPropertyName("primary_candidate_id")]
            public string? PrimaryCandidateId { get; set; }

            public CandidateModel? GetPrimaryCandidate(bool final = true)
            {
                return Candidates.FirstOrDefault(x => x.CandidateId == PrimaryCandidateId && (!final || x.IsFinal));
            }
        }

        public class TurnKeyModel
        {
            [JsonPropertyName("chat_id")]
            public string ChatId { get; set; } = string.Empty;
        }

        public class AuthorModel
        {
            [JsonPropertyName("author_id")]
            public string AuthorId { get; set; } = string.Empty;

            [JsonPropertyName("name")]
            public string Name { get; set; } = string.Empty;
        }

        public class CandidateModel
        {
            [JsonPropertyName("candidate_id")]
            public string CandidateId { get; set; } = string.Empty;

            [JsonPropertyName("create_time")]
            public DateTime? CreateTime { get; set; }

            [JsonPropertyName("raw_content")]
            public string RawContent { get; set; } = string.Empty;

            [JsonPropertyName("is_final")]
            public bool IsFinal { get; set; }
        }

        public class MetaModel
        {
            [JsonPropertyName("next_token")]
            public string? NextToken { get; set; }
        }
    }
}
