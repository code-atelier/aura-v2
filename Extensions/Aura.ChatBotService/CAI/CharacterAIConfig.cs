﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.ChatBotService.CAI
{
    [AuraConfiguration("cai.json")]
    [AutoConfig("Character AI",
        Icon = "res/cai.png")]
    public class CharacterAIConfig
    {
        [JsonPropertyName("useNeo")]
        [AutoConfigMember("Use CAI Neo API",
            Description = "Use CAI Neo API (currently must be on)",
            Kind = AutoConfigPropertyKind.Toggle,
            ReadOnly = true
            )]
        public bool UseNeo { get; set; } = true;

        [JsonPropertyName("caiPlus")]
        [AutoConfigMember("Use CAI+",
            Description = "Use CAI+ API (currently must be on)",
            Kind = AutoConfigPropertyKind.Toggle,
            ReadOnly = true
            )]
        public bool UseCaiPlus { get; set; } = true;

        [JsonPropertyName("caiPlusToken")]
        [AutoConfigMember("CAI+ user token",
            Description = "User token for CAI+ (currently must be filled)",
            Kind = AutoConfigPropertyKind.Textbox
            )]
        public string? CaiPlusToken { get; set; }

        [JsonPropertyName("characterId")]
        [AutoConfigMember("CAI character id",
            Description = "The character id to chat with (you can paste character url here)",
            Kind = AutoConfigPropertyKind.Textbox,
            Validator = typeof(CAIValidator)
            )]
        public string CharacterId { get; set; } = "-";

        [JsonPropertyName("signinTimerSecond")]
        [AutoConfigMember("Sign in timer",
            Description = "Sign in time for you to login (in seconds)",
            Kind = AutoConfigPropertyKind.IntegerTextbox,
            NumericMinValue = 60,
            NumericMaxValue = 10000
            )]
        public int SigninTimer { get; set; } = 300;

        [JsonPropertyName("networkDelaySecond")]
        [AutoConfigMember("Network delay",
            Description = "Delay for the emulator to wait for network activities (in seconds)",
            Kind = AutoConfigPropertyKind.IntegerTextbox,
            NumericMinValue = 5,
            NumericMaxValue = 20
            )]
        public int NetworkDelay { get; set; } = 5;
    }
}
