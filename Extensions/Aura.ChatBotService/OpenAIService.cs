﻿using Aura.ChatBotService.CAI;
using Aura.ChatBotService.OpenAI;
using Aura.ChatBotService.OpenAI.Models;
using Aura.Models;
using Aura.Services;
using Aura.Services.Passive;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace Aura.ChatBotService
{
    public class OpenAIService : PassiveServiceBase, IChatbotService, IRetryableChatbotService, IServiceWithConfiguration, IMessageReceiver
    {
        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = "coatl.openai",
            Name = "Open AI",
            Author = "CodeAtelier",
            Description = "Allows you to chat with Open AI bot.",
            Version = new Version(1, 0, 0),
            Icon = Util.CreateResourceUri("res/cai.png"),
        };

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public string Id { get => Info.Id; }

        public bool CanReceiveMessage { get => State == ServiceState.Running; }

        public Queue<ChatbotServiceMessage> ChatHistory { get; } = new Queue<ChatbotServiceMessage>();

        private OpenAIConfiguration Config { get; set; }

        private MessageRepository MessageRepository { get; set; }

        private ConfigurationService ConfigurationService { get; }

        private OpenAIClient? Client { get; set; }

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.Start | ServiceUserAction.Restart | ServiceUserAction.Stop | ServiceUserAction.Configure;

        public OpenAIService(ConfigurationService configurationService)
        {
            ConfigurationService = configurationService;
            Config = ConfigurationService.LoadConfig<OpenAIConfiguration>(true);
            MessageRepository = ConfigurationService.LoadConfig<MessageRepository>(true);
        }

        public Window CreateConfigurationWindow()
        {
            var ac = new AutoConfigurator();
            ac.LoadConfiguration(Config);
            ac.Closed += ConfigWindow_Closed;
            return ac;
        }

        public override async Task OnStart()
        {
            await Task.Run(() =>
            {
                Config = ConfigurationService.LoadConfig<OpenAIConfiguration>(true);
                MessageRepository = ConfigurationService.LoadConfig<MessageRepository>(true);
                Client = new OpenAIClient(Config.BaseUri);
                Client.SetApiKey(Config.APIKey);
            });
        }

        private void ConfigWindow_Closed(object? sender, EventArgs e)
        {
            ConfigurationService.SaveConfig<OpenAIConfiguration>();
        }

        public async Task<ChatbotServiceBotMessage?> GetGreeting()
        {
            return await Task.Run(() => new ChatbotServiceBotMessage()
            {
                Message = "Hello!",
            });

        }

        public Task<ChatbotServiceBotMessage> GetNewResponseFor(ChatbotServiceBotMessage message)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> LoadChatHistory()
        {
            return await Task.Run(() => true);
        }

        public async Task<Services.Message?> ReceiveMessageAsync(Services.Message message, CancellationToken cancellationToken)
        {
            return await Task.Run(() => (Services.Message?)null);
        }

        public async Task<IEnumerable<ChatbotServiceBotMessage>?> SendMessage(ChatbotServiceUserMessage message)
        {
            if (Client == null) return null;
            var userId = "user";
            var chatLog = MessageRepository.FindUser(userId);
            var req = new CompletionsRequest()
            {
                Model = Config.Model,
            };
            req.Messages.Add(new OpenAI.Models.Message()
            {
                Role = Roles.System,
                Content = "You are Alice, a 13 years old girl. Despite your young age, you are a mafia lord that controls the world in shadow. Yet, you are a sweet and kind girl. You have a long blue hair and a beautiful face. You don't like to play games, but love challenges."
            });
            if (chatLog != null)
            {
                req.Messages.AddRange(chatLog.Take(100));
            }
            req.Messages.Add(
                new OpenAI.Models.Message()
                {
                    Content = message.Message,
                    Role = Roles.User,
                }
            );
            MessageRepository.Record(userId, new OpenAI.Models.Message()
            {
                Content = message.Message,
                Role = Roles.User,
            });

            var res = await Client.Chat.Completions(req);
            if (res == null || res.Choices.Count == 0) return null;
            MessageRepository.Record(userId, res.Choices[0].Message);

            var bm = res.ToBotMessage();
            if (bm == null) return null;
            bm.AuthorName = "Chatbot";
            return new List<ChatbotServiceBotMessage>()
            {
                bm
            };
        }
    }
}
