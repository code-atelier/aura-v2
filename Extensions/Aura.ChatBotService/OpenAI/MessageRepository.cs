﻿using Aura.ChatBotService.OpenAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.ChatBotService.OpenAI
{
    [AuraConfiguration("openai_msgrepo.db.json")]
    public class MessageRepository
    {
        [JsonPropertyName("chatlogs")]
        public List<UserChatlog> Chatlogs { get; set; } = new List<UserChatlog>();

        public UserChatlog? FindUser(string userId)
        {
            return Chatlogs.FirstOrDefault(x => x.UserId == userId);
        }

        public void Record(string userId, Message message)
        {
            var chatLog = FindUser(userId);
            if (chatLog == null)
            {
                chatLog = new UserChatlog() { UserId = userId };
                Chatlogs.Add(chatLog);
            }
            chatLog.Messages.Add(message);
        }
    }
}
