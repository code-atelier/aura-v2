﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.ChatBotService.OpenAI
{
    [AuraConfiguration("openai.json")]
    [AutoConfig("Open AI",
        Icon = "res/cai.png")]
    public class OpenAIConfiguration
    {
        [JsonPropertyName("base_uri")]
        [AutoConfigMember("Base Uri",
            Description = "Base Uri of the Open AI service (host only).",
            Kind = AutoConfigPropertyKind.Textbox,
            Index = 0
            )]
        public string BaseUri { get; set; } = string.Empty;

        [JsonPropertyName("api_key")]
        [AutoConfigMember("API Key",
            Description = "API Key that you got from Open AI provider.",
            Kind = AutoConfigPropertyKind.Textbox,
            Index = 1
            )]
        public string APIKey { get; set; } = string.Empty;

        [JsonPropertyName("model")]
        [AutoConfigMember("Model",
            Description = "Model used to generate messages.",
            Kind = AutoConfigPropertyKind.Textbox,
            Index = 2
            )]
        public string Model { get; set; } = string.Empty;
    }
}
