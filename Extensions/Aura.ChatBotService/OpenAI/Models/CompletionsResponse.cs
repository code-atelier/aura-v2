﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.ChatBotService.OpenAI.Models
{
    public class CompletionsResponse
    {
        [JsonPropertyName("id")]
        public string Id { get; set; } = string.Empty;
        
        [JsonPropertyName("choices")]
        public List<Choice> Choices { get; set; } = new List<Choice>();

        [JsonPropertyName("created")]
        public int Created { get; set; }

        [JsonIgnore]
        public DateTime CreatedAt { get => DateTime.UnixEpoch.AddSeconds(Created); }

        [JsonPropertyName("model")]
        public string Model { get; set; } = string.Empty;

        public ChatbotServiceBotMessage? ToBotMessage(int choiceIndex = 0)
        {
            if (Choices.Count <= choiceIndex || choiceIndex < 0) return null;
            return new ChatbotServiceBotMessage()
            {
                IsSuccessful = true,
                Message = Choices[choiceIndex].Message.Content,
                Timestamp = CreatedAt,
            };
        }
    }
}
