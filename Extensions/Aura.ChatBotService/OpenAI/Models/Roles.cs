﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.ChatBotService.OpenAI.Models
{
    public static class Roles
    {
        public const string System = "system";

        public const string User = "user";

        public const string Assistant = "assistant";
    }
}
