﻿using Aura.ChatBotService.OpenAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.ChatBotService.OpenAI
{
    public class UserChatlog
    {
        [JsonPropertyName("user_id")]
        public string UserId { get; set; } = string.Empty;

        [JsonPropertyName("messages")]
        public List<Message> Messages { get; set; } = new List<Message>();

        public List<Message> Take(int count)
        {
            if (Messages.Count <= count) return Messages;
            return Messages.Skip(Messages.Count - count).Take(count).ToList();
        }
    }
}
