﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Aura.ChatBotService.OpenAI
{
    public class OpenAIClient
    {
        internal RestClient RestClient { get; }

        public OpenAIChat Chat { get; }

        public OpenAIClient(string baseUrl) { 
            RestClient = new RestClient(baseUrl);
            Chat = new OpenAIChat(this);
        }

        public void SetApiKey(string apiKey)
        {
            RestClient.AddDefaultHeader("Authorization", "Bearer " + apiKey);
        }
    }
}
