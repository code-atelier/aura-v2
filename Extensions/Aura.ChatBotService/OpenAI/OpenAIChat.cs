﻿using Aura.ChatBotService.OpenAI.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Aura.ChatBotService.OpenAI
{
    public class OpenAIChat
    {
        private OpenAIClient Client { get; }

        internal OpenAIChat(OpenAIClient client)
        {
            Client = client;
        }

        public async Task<CompletionsResponse?> Completions(CompletionsRequest request)
        {
            try
            {
                var restRequest = new RestRequest("v1/chat/completions", Method.Post);
                restRequest.AddJsonBody(request);
                var response = await Client.RestClient.PostAsync(restRequest);
                if (response.IsSuccessStatusCode && response.Content != null)
                {
                    return JsonSerializer.Deserialize<CompletionsResponse>(response.Content);
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
    }
}
