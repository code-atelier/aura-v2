﻿using Aura.ChatBotService.CAI;
using Aura.Services.Passive;
using System.Windows;

namespace Aura.ChatBotService
{
    /// <summary>
    /// Interaction logic for CharacterAIConfigurationWindow.xaml
    /// </summary>
    public partial class CharacterAIConfigurationWindow : Window
    {
        private ConfigurationService ConfigurationService { get; }
        private CharacterAIConfig Config { get; }

        public CharacterAIConfigurationWindow(ConfigurationService configurationService, CharacterAIConfig currentConfig)
        {
            InitializeComponent();
            ConfigurationService = configurationService;
            Config = currentConfig;

            cxUseCaiPlus.IsChecked = Config.UseCaiPlus || true;
            cxUseNeo.IsChecked = Config.UseNeo || true;
            txToken.Text = Config.CaiPlusToken ?? "";
            txSigninTime.Text = Config.SigninTimer.ToString();
            txNetworkTimeout.Text = Config.NetworkDelay.ToString();
            txCharacterID.Text = Config.CharacterId ?? "";
        }

        public void LoadConfiguration(string config)
        {
            var spl = config.Split('&', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            foreach (var kv in spl)
            {
                var sp = kv.Split('=', 2);
                var k = sp[0];
                var v = sp.Length > 1 ? Uri.UnescapeDataString(sp[1]) : "";
                if (k == "charid")
                {
                    txCharacterID.Text = v;
                }
                if (k == "token")
                {
                    txToken.Text = v;
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var charaId = txCharacterID.Text?.Trim() ?? "";
            if (string.IsNullOrWhiteSpace(txCharacterID.Text))
            {
                MessageBox.Show("Character id is required!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                var uri = new Uri(charaId);
                if (uri.Query != null)
                {
                    var queries = uri.Query.TrimStart('?').Split('&').Select(x => x.Split('=', 2)).ToDictionary(x => x[0], x => x.Length == 1 ? "" : x[1]);
                    if (queries.ContainsKey("char"))
                    {
                        charaId = queries["char"];
                    }
                }
            }
            catch { }

            var token = txToken.Text?.Trim() ?? "";
            if (cxUseCaiPlus.IsChecked == true && string.IsNullOrWhiteSpace(token))
            {
                MessageBox.Show("Authentication token is required if you use character.ai+!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int signinTime, networkTimeout;
            if (!int.TryParse(txSigninTime.Text, out signinTime) || signinTime < 0)
            {
                MessageBox.Show("Please enter a valid user sign-in time!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (signinTime < 60)
            {
                MessageBox.Show("Please enter a larger value for user sign-in time!\r\n(Minimum is 60 seconds)", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txNetworkTimeout.Text, out networkTimeout) || networkTimeout < 0)
            {
                MessageBox.Show("Please enter a valid network wait time!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (networkTimeout < 3)
            {
                MessageBox.Show("Please enter a larger value for network timeout!\r\n(Minimum is 3 seconds)", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Config.UseCaiPlus = cxUseCaiPlus.IsChecked == true || true;
            Config.UseNeo = cxUseNeo.IsChecked == true || true;
            Config.CaiPlusToken = token;
            Config.SigninTimer = signinTime;
            Config.NetworkDelay = networkTimeout;
            Config.CharacterId = charaId;
            ConfigurationService.SaveConfig<CharacterAIConfig>();
            if (MessageBox.Show("Configuration saved!\r\nYou must restart the service for the new configuration to take effect.\r\nDo you want to restart the service now?", "Question",
                MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                RestartService();
            }
            Close();
        }

        async void RestartService()
        {
            try
            {
                var svc = ServiceManager.GetInstance<CharacterAIService>();
                await svc.Shutdown();
                await Task.Delay(100);
                await svc.Start();
            }
            catch { }
        }
    }
}
