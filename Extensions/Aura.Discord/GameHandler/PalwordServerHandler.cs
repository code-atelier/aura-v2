﻿using Aura.CodeAtelier.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Interop;

namespace Aura.CodeAtelier.GameHandler
{
    public class PalwordServerHandler : IGameHandler
    {
        public string Id { get; } = "PalworldHandler";

        public GameHandlerConfig Config { get; private set; } = new GameHandlerConfig();

        public int ServerShutdownWaitTime = 300;

        private string LastLoadedConfig = string.Empty;

        private Process? PalServerProcess { get; set; }

        public async Task<GameHandlerResponse> ProcessMessage(GameHandlerMessage message)
        {
            if (!message.HasCommand) throw new Exception();
            try
            {
                if (message.FirstCommand?.ToLower() == "stat")
                {
                    return await Stat(message);
                }
                if (message.FirstCommand?.ToLower() == "restart")
                {
                    return await Restart(message);
                }
            }
            catch
            {

            }

            return new GameHandlerResponse()
            {
                Message = "Unknown command '" + message.FirstCommand + "'."
            };
        }

        public async Task ResetState()
        {
            await FindProcess();
        }

        private async Task FindProcess()
        {
            await Task.Run(() =>
            {
                // try to find running process
                PalServerProcess = Process.GetProcesses().Where(x => x.ProcessName?.Contains("PalServer") == true && x.MainModule?.ModuleName == "PalServer.exe").FirstOrDefault();
            });
        }

        public async Task UpdateConfig(string config)
        {
            Config = GameHandlerConfig.Parse(config);
            if (Config.ToString() != LastLoadedConfig)
            {
                await ResetState();
                LastLoadedConfig = Config.ToString();
            }
        }

        private RestClient? CreateClient()
        {
            if (!Config.ContainsKey("host")) return null;
            var username = Config.ContainsKey("user") ? Config["user"] : "";
            var password = Config.ContainsKey("password") ? Config["password"] : "";
            var authString = $"{username}:{password}";
            var b64Auth = Convert.ToBase64String(Encoding.ASCII.GetBytes(authString));
            var client = new RestClient(Config.ContainsKey("host") ? Config["host"] : "");
            client.AddDefaultHeader("Authorization", $"Basic {b64Auth}");
            return client;
        }

        private async Task<GameHandlerResponse> Stat(GameHandlerMessage message)
        {
            await FindProcess();
            var active = PalServerProcess != null && !PalServerProcess.HasExited;
            var msg = new List<string>
            {
                "◄◄◄ " + message.Game.GetProperServerName() + " ►►►",
            };
            var client = CreateClient();
            if (client == null)
            {
                msg.Add("Status: " + (active ? "Online" : "Offline"));
            }
            else
            {
                try
                {
                    var resp = await client.GetAsync(new RestRequest("v1/api/metrics", Method.Get));
                    if (resp.IsSuccessStatusCode)
                    {
                        var jso = JsonSerializer.Deserialize<PalServerMetricsResponse>(resp.Content ?? "null");
                        if (jso != null)
                        {
                            msg.Add("Status: Online");
                            msg.Add($"Player: {jso.CurrentPlayerCount}/{jso.MaxPlayerCount}");
                        }
                    }
                    try
                    {
                        resp = await client.GetAsync(new RestRequest("v1/api/settings", Method.Get));
                        if (resp.IsSuccessStatusCode)
                        {
                            var jso = JsonSerializer.Deserialize<PalServerSettingsResponse>(resp.Content ?? "null");
                            if (jso != null)
                            {
                                msg.Add("");
                                msg.Add("--= Server Rates =--");
                                msg.Add($"Experience Rate     : {Math.Round((jso.ExpRate ?? 1) * 100)}%");
                                msg.Add($"Pal Capture Rate    : {Math.Round((jso.PalCaptureRate ?? 1) * 100)}%");
                                msg.Add($"Pal Spawn Rate      : {Math.Round((jso.PalSpawnNumRate ?? 1) * 100)}%");
                                msg.Add($"Egg Incubation Rate : {jso.IncubationRate}%");
                            }
                        }
                    }
                    catch { }
                }
                catch
                {
                    msg.Add("Status: Offline");
                }
            }
            return new GameHandlerResponse()
            {
                Message = "```" + string.Join("\r\n", msg) + "```",
            };
        }

        private Thread? StartPendingThread { get; set; }
        bool isRestarting = false;
        int waitTime = 0;

        private async Task<GameHandlerResponse> Restart(GameHandlerMessage message)
        {
            if (!message.IsAdmin)
            {
                return new GameHandlerResponse()
                {
                    Message = "You must be an admin to run this command."
                };
            }
            if (isRestarting)
            {
                return new GameHandlerResponse()
                {
                    Message = "Server already queued for a restart."
                };
            }
            var startserver = Config.ContainsKey("exe") ? Config["exe"] : null;
            if (string.IsNullOrEmpty(startserver))
                return new GameHandlerResponse()
                {
                    Message = "This server is not configured for restart."
                };
            await FindProcess();
            var active = PalServerProcess != null && !PalServerProcess.HasExited;
            if (active)
            {
                var client = CreateClient();
                if (client == null)
                {
                    return new GameHandlerResponse()
                    {
                        Message = "Server API is not configured.",
                    };
                }
                var req = new RestRequest("v1/api/shutdown", Method.Post);
                req.AddBody(new PalServerShutdownRequest() { WaitTime = ServerShutdownWaitTime }, ContentType.Json);
                var resp = await client.PostAsync(req);
                if (resp.IsSuccessStatusCode)
                {
                    waitTime = ServerShutdownWaitTime + 10;
                    StartPendingThread = new Thread(RunPalworldServer);
                    StartPendingThread.Start();

                    var s = ServerShutdownWaitTime % 60;
                    var m = ServerShutdownWaitTime / 60;
                    return new GameHandlerResponse()
                    {
                        Message = $"Server will restart in {m}m and {s}s."
                    };
                }
                throw new Exception("Server not returning correct code");
            }
            else
            {
                waitTime = 0;
                StartPendingThread = new Thread(RunPalworldServer);
                StartPendingThread.Start();
                return new GameHandlerResponse()
                {
                    Message = "Server will start soon."
                };
            }
        }

        private void RunPalworldServer()
        {
            isRestarting = true;
            try
            {
                var startTime = DateTime.Now;
                while (true)
                {
                    var delta = DateTime.Now - startTime;
                    if (delta.TotalSeconds >= waitTime)
                        break;
                    Thread.Sleep(1000);
                }
                var startserver = Config.ContainsKey("exe") ? Config["exe"] : null;
                var args = Config.ContainsKey("args") ? Config["args"] : null;
                var psi = new ProcessStartInfo()
                {
                    FileName = startserver,
                    Arguments = args ?? "",
                    UseShellExecute = false,
                };
                startTime = DateTime.Now;
                Process.Start(psi);
                while ((DateTime.Now - startTime).TotalSeconds <= 60)
                {
                    var task = FindProcess();
                    task.Wait();
                    if (PalServerProcess != null)
                    {
                        break;
                    }
                }
            }
            catch { }
            finally
            {
                StartPendingThread = null;
                isRestarting = false;
            }
        }
    }
}
