﻿using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.GameHandler
{
    public interface IGameHandler
    {
        string Id { get; }

        GameHandlerConfig Config { get; }

        Task UpdateConfig(string config);

        Task ResetState();

        Task<GameHandlerResponse> ProcessMessage(GameHandlerMessage message);
    }

    public class GameHandlerConfig: Dictionary<string, string>
    {
        public static GameHandlerConfig Parse(string config)
        {
            var parts = config.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            var res = new GameHandlerConfig();
            foreach (var part in parts)
            {
                var value = part.Split('=', 2, StringSplitOptions.TrimEntries);
                if (!string.IsNullOrWhiteSpace(value[0]))
                {
                    if (value.Length > 1)
                    {
                        res[value[0]] = value[1];
                    }
                    else
                    {
                        res[value[0]] = "true";
                    }
                }
            }
            return res;
        }

        public override string ToString()
        {
            return string.Join(";", this.Select(x => x.Key + "=" + x.Value));
        }
    }

    public class GameHandlerMessage
    {
        public GameHandlerMessage(ManagedGameServer game)
        {
            Game = game;
        }

        public ManagedGameServer Game { get; }

        public string[] Commands { get; set; } = new string[0];

        public bool IsAdmin { get; set; }

        public bool HasCommand { get => Commands?.Length > 0; }

        public string? FirstCommand { get => Commands?.Length > 0 ? Commands[0] : null; }
    }

    public static class GameHandler
    {
        public static Dictionary<string, IGameHandler> GameHandlers { get; } = new Dictionary<string, IGameHandler>();

        public static void Register(string name, IGameHandler handler)
        {
            GameHandlers.Add(name, handler);
        }

        public static void Register(IGameHandler handler)
        {
            GameHandlers.Add(handler.Id, handler);
        }

        public static IGameHandler? Get(string name)
        {
            if (GameHandlers.ContainsKey(name)) return GameHandlers[name];
            return null;
        }
    }

    public class GameHandlerResponse
    {
        public string Message { get; set; } = string.Empty;

        public List<GameHandlerResponseAttachment> Attachments { get; } = new List<GameHandlerResponseAttachment>();
    }

    public class GameHandlerResponseAttachment
    {
        public GameHandlerResponseAttachmentType Type { get; set; }

        public Stream? Stream { get; set; }
    }

    public enum GameHandlerResponseAttachmentType
    {
        Image,
    }
}
