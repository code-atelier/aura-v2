﻿using Aura.CodeAtelier.Config;
using Aura.Services.Passive;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Commands
{
    [Group("game")]
    public class GameServerList : ModuleBase<SocketCommandContext>
    {
        public GameServerList()
        {
        }

        [Command("list")]
        [Summary("Shows a list of game servers")]
        public async Task GameList(
            [Summary("Filter result by game name (optional)")]
            string? gameName = null
            )
        {
            var cfg = AuraDiscordService.ConfigurationService?.LoadConfig<AuraDiscordServiceManagedGameServerConfiguration>(true);
            if (cfg == null) return;
            var msgRef = new Discord.MessageReference(Context.Message.Id, Context.Message.Channel?.Id, null, false);
            ulong? guildId = Context.Guild?.Id;
            var validGames = cfg.Games?.Where(x => x.CheckGuildAccess(guildId) && !string.IsNullOrWhiteSpace(x.Id) && !string.IsNullOrEmpty(x.GameName) && !string.IsNullOrWhiteSpace(x.ServerName) && x.Hostnames?.Count > 0).ToList() ?? new List<Models.ManagedGameServer>();            if (cfg.AllowQuery && validGames.Count > 0)
            {
                var matched = validGames.Where(x => !string.IsNullOrWhiteSpace(x.GameName) && x.Hostnames?.Count > 0 && x.CheckGuildAccess(guildId) &&

                (
                    string.IsNullOrWhiteSpace(gameName)
                    || x.GameName?.ToLower().Contains(gameName.ToLower().Trim()) == true
                    || x.ServerName?.ToLower().Contains(gameName.ToLower().Trim()) == true
                )

                ).OrderBy(x => x.GameName + x.GameVersion).ToList();
                if (matched.Count > 0)
                {
                    var retList = new List<string>();
                    foreach (var game in matched)
                    {
                        retList.Add("  ► " + game.GetProperGameName() + " - " + game.GetProperServerName() + " (ID: `" + game.Id + "`)");
                    }
                    await ReplyAsync("Here is a list of games that I have:\r\n" + string.Join("\r\n", retList), messageReference: msgRef);
                    return;
                }
            }
            await ReplyAsync(validGames.Count > 0 ? "I don't think I have that game." : "Hmm, I don't have any games right now.", messageReference: msgRef);
        }

        [Command("info")]
        [Summary("Shows info of a specific game server")]
        public async Task GameInfo(
            [Summary("Game server id")]
            string gameId)
        {
            var msgRef = new Discord.MessageReference(Context.Message.Id, Context.Message.Channel?.Id, null, false);
            if (string.IsNullOrWhiteSpace(gameId))
            {
                await ReplyAsync("You need to specify the game server id!", messageReference: msgRef);
                return;
            }
            var cfg = AuraDiscordService.ConfigurationService?.LoadConfig<AuraDiscordServiceManagedGameServerConfiguration>(true);
            if (cfg == null) return;
            ulong? guildId = Context.Guild?.Id;
            var validGames = cfg.Games?.Where(x => x.CheckGuildAccess(guildId) && !string.IsNullOrWhiteSpace(x.Id) && !string.IsNullOrEmpty(x.GameName) && !string.IsNullOrWhiteSpace(x.ServerName) && x.Hostnames?.Count > 0).ToList() ?? new List<Models.ManagedGameServer>();
            if (cfg.AllowQuery && validGames.Count > 0 && !string.IsNullOrWhiteSpace(gameId))
            {
                var game = validGames.Where(x => !string.IsNullOrWhiteSpace(x.GameName) && x.Hostnames?.Count > 0 && x.CheckGuildAccess(guildId) &&

                (
                    x.Id == gameId
                )

                ).FirstOrDefault();
                if (game != null)
                {
                    var li = new List<string>()
                    {
                        "◄◄◄ " + game.GetProperServerName() + " ►►►",
                        "Game: " + game.GetProperGameName()
                    };
                    if (game.Hostnames.Count == 1)
                    {
                        li.Add("Host: " + game.Hostnames[0]);
                    }
                    else
                    {
                        li.Add("Hosts (pick one):");
                        foreach (var host in game.Hostnames)
                        {
                            li.Add("  ► " + host);
                        }
                    }
                    if (game.ModList?.Count > 0)
                    {
                        li.Add("Mod List:");
                        foreach (var item in game.ModList)
                        {
                            li.Add("  - " + item);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(game.Note))
                    {
                        li.Add("");
                        li.Add(game.Note);
                    }
                    await ReplyAsync("```" + string.Join("\r\n", li) + "```", messageReference: msgRef);
                    return;
                }
            }
            await ReplyAsync("Hmm... I cannot find that game server. You can get a list of game servers using the `game list` command.", messageReference: msgRef);
        }
    }
}
