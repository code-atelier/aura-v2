﻿using Aura.CodeAtelier.Config;
using Aura.CodeAtelier.GameHandler;
using Aura.CodeAtelier.Models;
using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Commands
{
    [Group("server")]
    public class GameHandlerCommand : ModuleBase<SocketCommandContext>
    {
        private AuraDiscordServiceManagedGameServerConfiguration? GetConfig()
        {
            return AuraDiscordService.ConfigurationService?.LoadConfig<AuraDiscordServiceManagedGameServerConfiguration>(true);
        }

        private MessageReference GetMessageReference()
        {
            return new MessageReference(Context.Message.Id, Context.Message.Channel?.Id, null, false);
        }

        private async Task SendCannotFind()
        {
            await ReplyAsync("Hmm, I can't find the game server id. Try using `game list` command to see the correct id.", messageReference: GetMessageReference());
        }

        private async Task SendCannotFindHandler()
        {
            await ReplyAsync("This game server is not supported.", messageReference: GetMessageReference());
        }

        private ManagedGameServer? GetGame(string id)
        {
            var cfg = GetConfig();
            if (cfg == null || string.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            var game = cfg.Games?.Where(x => x.Id == id).FirstOrDefault();
            if (game == null || game.Handler == null || string.IsNullOrWhiteSpace(game.Handler.Id))
            {
                return null;
            }
            return game;
        }

        private async Task<GameHandlerResponse?> QuickProcess(string gameId, params string[] commands)
        {
            var game = GetGame(gameId);
            if (game == null)
            {
                await SendCannotFind();
                return null;
            }
            var handler = GameHandler.GameHandler.Get(game.Handler.Id);
            if (handler == null)
            {
                await SendCannotFindHandler();
                return null;
            }

            await handler.UpdateConfig(game.Handler.Config);

            try
            {
                var message = new GameHandler.GameHandlerMessage(game)
                {
                    Commands = commands,
                    IsAdmin = game.Handler.CheckAccess(Context.Message.Author.Id),
                };
                return await handler.ProcessMessage(message);
            }
            catch
            {
                return null;
            }
        }

        [Command("stats")]
        [Summary("Shows stats of a game server")]
        public async Task Stats(
            [Summary("Game server id")]
            string gameId
            )
        {
            var res = await QuickProcess(gameId, "stat");
            if (res == null) return;

            await ReplyAsync(res.Message, messageReference: GetMessageReference());
        }

        [Command("restart")]
        [Summary("Restart a game server (admin only)")]
        public async Task Restart(
            [Summary("Game server id")]
            string gameId,
            string? message = null
            )
        {
            var res = await QuickProcess(gameId, "restart", message ?? "");
            if (res == null) return;

            await ReplyAsync(res.Message, messageReference: GetMessageReference());
        }
    }
}
