﻿using Aura.CodeAtelier.Config;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Commands
{
    public class InfoCommand : ModuleBase<SocketCommandContext>
    {
        [Command("userinfo")]
        [Alias("whois", "user")]
        [Summary("Shows the information of a user")]
        public async Task UserInfo(
            [Summary("Target user (optional)")]
            SocketUser? user = null
            )
        {
            var msgRef = new Discord.MessageReference(Context.Message.Id, Context.Message.Channel?.Id, null, false);
            var userInfo = user ?? Context.Message.Author;
            if (userInfo.Id == Context.Client.CurrentUser.Id)
            {
                await ReplyAsync($"No doxing, okay?", messageReference: msgRef);
                return;
            }
            var userName = userInfo.Username;
            if (!string.IsNullOrEmpty(userInfo.Discriminator) && userInfo.Discriminator != "0000")
            {
                userName += "#" + userInfo.Discriminator;
            }
            await ReplyAsync($"{userName} (UID: `{userInfo.Id})`", messageReference: msgRef);
        }

        [Command("guildinfo")]
        [Alias("where", "guild")]
        [Summary("Shows the information of current guild")]
        public async Task GuildInfo()
        {
            var msgRef = new Discord.MessageReference(Context.Message.Id, Context.Message.Channel?.Id, null, false);
            var guildInfo = Context.Guild;
            if (guildInfo != null)
            {
                await ReplyAsync($"{guildInfo.Name} (UID: `{guildInfo.Id}`)", messageReference: msgRef);
            }
        }
    }
}
