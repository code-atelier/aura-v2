﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class PalServerMetricsResponse
    {
        [JsonPropertyName("currentplayernum")]
        public int CurrentPlayerCount { get; set; } = 0;

        [JsonPropertyName("maxplayernum")]
        public int MaxPlayerCount { get; set; } = 0;
    }
}
