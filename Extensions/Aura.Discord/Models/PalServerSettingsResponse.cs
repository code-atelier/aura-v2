﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class PalServerSettingsResponse
    {
        public double? ExpRate { get; set; }

        public double? PalCaptureRate { get; set; }

        public double? PalSpawnNumRate { get; set; }

        public double? PalEggDefaultHatchingTime { get; set; }

        [JsonIgnore]
        public double IncubationRate
        {
            get
            {
                if (PalEggDefaultHatchingTime == null) return 100;
                var rate = PalEggDefaultHatchingTime.Value != 0 ? 72d / PalEggDefaultHatchingTime.Value * 100 : 100;
                return Math.Round(rate);
            }
        }
    }
}
