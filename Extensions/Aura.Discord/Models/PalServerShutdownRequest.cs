﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class PalServerShutdownRequest
    {
        [JsonPropertyName("waittime")]
        public int WaitTime { get; set; } = 300;

        [JsonPropertyName("message")]
        public string Message { get; set; } = "Server will be restarted in 5 minutes.";
    }
}
