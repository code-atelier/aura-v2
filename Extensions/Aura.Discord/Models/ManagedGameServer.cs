﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class ManagedGameServer
    {
        [JsonPropertyName("server_name")]
        public string ServerName { get; set; } = "";

        [JsonPropertyName("game_name")]
        public string GameName { get; set; } = "";

        [JsonPropertyName("game_version")]
        public string GameVersion { get; set; } = "";

        [JsonPropertyName("hosts")]
        public List<string> Hostnames { get; set; } = new List<string>();

        [JsonPropertyName("mods")]
        public List<string> ModList { get; set; } = new List<string>();

        [JsonPropertyName("note")]
        public string? Note { get; set; }

        [JsonPropertyName("id")]
        public string Id { get; set; } = "";

        [JsonPropertyName("guilds")]
        public List<ulong> Guilds { get; set; } = new List<ulong>();

        [JsonPropertyName("handler")]
        public ManagedGameServerHandler Handler { get; set; } = new ManagedGameServerHandler();

        public string GetProperServerName()
        {
            return ServerName;
        }

        public string GetProperGameName()
        {
            return GameName + (!string.IsNullOrWhiteSpace(GameVersion) ? " v" + GameVersion : "");
        }

        public bool CheckGuildAccess(ulong? guildid)
        {
            return !guildid.HasValue || Guilds == null || Guilds.Count == 0 || Guilds.Contains(guildid.Value);
        }
    }

    public class ManagedGameServerHandler
    {
        [JsonPropertyName("id")]
        public string Id { get; set; } = "";

        [JsonPropertyName("config")]
        public string Config { get; set; } = "";

        [JsonPropertyName("admins")]
        public List<ulong> AdminIds { get; set; } = new List<ulong>();

        public bool CheckAccess(ulong id)
        {
            return AdminIds.Contains(id);
        }
    }
}
