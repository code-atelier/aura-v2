﻿using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Config
{
    [AuraConfiguration("auradiscord.games.json")]
    public class AuraDiscordServiceManagedGameServerConfiguration
    {
        [JsonPropertyName("allow_query")]
        public bool AllowQuery { get; set; } = true;

        [JsonPropertyName("allow_command")]
        public bool AllowCommand { get; set; } = true;

        [JsonPropertyName("games")]
        public List<ManagedGameServer> Games { get; set; } = new List<ManagedGameServer>();
    }
}
