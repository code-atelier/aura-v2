﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Config
{
    [AuraConfiguration("auradiscord.json")]
    [AutoConfig("Aura Discord", Icon = "res/icon.png")]
    public class AuraDiscordServiceConfiguration
    {
        [JsonPropertyName("bot_token")]
        [AutoConfigMember("Token",
            Description = "Discord Bot Token",
            Icon = "res/img/key.png",
            Index = 0,
            Kind = AutoConfigPropertyKind.Textbox)]
        public string? BotToken { get; set; }

        [JsonPropertyName("bot_name")]
        [AutoConfigMember("Name",
            Description = "The name of the Discord bot",
            Icon = "res/img/user.png",
            Index = 0,
            Kind = AutoConfigPropertyKind.Textbox)]
        public string? BotName { get; set; }
    }
}
