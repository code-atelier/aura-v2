﻿using Aura.CodeAtelier.Config;
using Aura.Services;
using Aura.Services.Passive;
using Aura.WPF;
using Aura;
using Discord.WebSocket;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Discord.Commands;
using Discord;
using Aura.CodeAtelier.Commands;
using System.Collections.Generic;
using Aura.CodeAtelier.GameHandler;

namespace Aura.CodeAtelier
{
    public class AuraDiscordService : PassiveServiceBase, IMessageReceiver, IServiceWithConfiguration, IServiceWithLog
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo
        {
            Id = "coatl.aura.discord",
            Name = "Aura Discord Service",
            Version = new Version(1, 0, 0),
            Description = "Discord bot service",
            Author = "CodeAtelier",
            Icon = Util.CreateResourceUri("res/icon.png"),
        };

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public string Id { get => Info.Id; }

        public bool CanReceiveMessage { get => BotReady; }

        private bool BotReady { get; set; } = false;

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.Start | ServiceUserAction.Stop | ServiceUserAction.Restart | ServiceUserAction.Configure | ServiceUserAction.ViewLog;

        public ServiceLog Log { get; }

        public IServiceMessageBus ServiceMessageBus { get; }

        private AuraDiscordServiceConfiguration Configuration { get; }

        public static ConfigurationService? ConfigurationService { get; private set; }

        public AuraDiscordService(ConfigurationService configurationService, IServiceMessageBus serviceMessageBus)
        {
            ServiceMessageBus = serviceMessageBus;
            ConfigurationService = configurationService;
            Configuration = configurationService.LoadConfig<AuraDiscordServiceConfiguration>();
            Log = new ServiceLog(this, DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".log");
            DiscordClient = new DiscordSocketClient();
            CommandService = new CommandService(new CommandServiceConfig());
        }

        private async Task DiscordClient_Log(global::Discord.LogMessage arg)
        {
            await Task.Run(() =>
            {
                try
                {
                    switch (arg.Severity)
                    {
                        case Discord.LogSeverity.Verbose:
                        case Discord.LogSeverity.Debug:
                            if (arg.Message != null)
                                Log.Debug(arg.Message);
                            break;
                        default:
                        case Discord.LogSeverity.Info:
                            if (arg.Message != null)
                                Log.Info(arg.Message);
                            break;
                        case Discord.LogSeverity.Warning:
                            if (arg.Message != null)
                                Log.Warning(arg.Message);
                            if (arg.Exception != null)
                                Log.Warning(arg.Exception.Message);
                            break;
                        case Discord.LogSeverity.Error:
                            if (arg.Message != null)
                                Log.Error(arg.Message);
                            if (arg.Exception != null)
                                Log.Error(arg.Exception);
                            break;
                    }
                }
                catch { }
            });
        }

        public Window CreateConfigurationWindow()
        {
            var ac = new AutoConfigurator();
            ac.LoadConfiguration(Configuration);
            ac.Closed += ConfiguratorClosed;
            return ac;
        }

        private void ConfiguratorClosed(object? sender, EventArgs e)
        {
            try
            {
                ConfigurationService?.SaveConfig<AuraDiscordServiceConfiguration>();
            }
            catch { }
        }

        DiscordSocketClient DiscordClient { get; }

        CommandService CommandService { get; }

        public override async Task Initialize()
        {
            ServiceMessageBus.Subscribe(this);
            DiscordClient.Log += DiscordClient_Log;
            DiscordClient.Ready += DiscordClient_Ready;
            DiscordClient.Connected += DiscordClient_Connected;
            DiscordClient.Disconnected += DiscordClient_Disconnected;
            DiscordClient.MessageReceived += DiscordClient_MessageReceived;
            DiscordClient.MessageUpdated += DiscordClient_MessageUpdated;

            await CommandService.AddModuleAsync<GameServerList>(null);
            await CommandService.AddModuleAsync<InfoCommand>(null);
            await CommandService.AddModuleAsync<GameHandlerCommand>(null);

            try
            {
                var palServer = new PalwordServerHandler();
                await palServer.ResetState();
                GameHandler.GameHandler.Register(palServer);
            }
            catch { }
        }

        private async Task DiscordClient_Disconnected(Exception arg)
        {
            await Task.Run(() =>
            {
                Log.Info(BotName + " is disconnected");
            });
        }

        private async Task DiscordClient_Connected()
        {
            await Task.Run(() =>
            {
                Log.Info(BotName + " is connected");
            });
        }

        private async Task DiscordClient_Ready()
        {
            await Task.Run(() =>
            {
                BotReady = true;
                Log.Info(BotName + " is ready");
            });
        }

        private async Task DiscordClient_MessageReceived(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            if (message == null) return;
            if (message.Author.IsBot) return;

            try
            {
                var repliedTo = message.ReferencedMessage as SocketUserMessage;
                if (repliedTo?.Author?.Id != DiscordClient.CurrentUser.Id)
                {
                    repliedTo = null;
                }
                var channelType = message.Channel.GetChannelType();

                var msgPos = 0;
                if (channelType == ChannelType.DM || message.HasMentionPrefix(DiscordClient.CurrentUser, ref msgPos))
                {
                    // handle commands first
                    var context = new SocketCommandContext(DiscordClient, message);
                    var commandResult = await CommandService.ExecuteAsync(context, msgPos, null);
                    if (commandResult.IsSuccess) 
                        return;

                    // else treat as convo
                    var convo = message.Content.Substring(msgPos);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private async Task DiscordClient_MessageUpdated(Discord.Cacheable<Discord.IMessage, ulong> arg1, SocketMessage arg2, ISocketMessageChannel arg3)
        {
        }

        private string BotName { get; set; } = "Bot";

        public override async Task OnStart()
        {
            BotReady = false;
            Log.Begin();
            if (string.IsNullOrWhiteSpace(Configuration.BotToken))
            {
                Log.Error("Bot Token is not configured");
                Log.End();
                throw new Exception("Bot Token is not configured.");
            }
            BotName = string.IsNullOrWhiteSpace(Configuration.BotName) ? "Bot" : Configuration.BotName.Trim();
            await DiscordClient.LoginAsync(Discord.TokenType.Bot, Configuration.BotToken);
            await DiscordClient.StartAsync();
        }

        public override async Task OnShutdown()
        {
            try
            {
                await DiscordClient.LogoutAsync();
                await DiscordClient.StopAsync();
            }
            finally
            {
                BotReady = false;
                Log.End();
            }
        }

        public async Task<Message?> ReceiveMessageAsync(Message message, CancellationToken cancellationToken)
        {
            return null;
        }

    }
}
