﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class ProjectItem
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("kind")]
        public ProjectItemKind Kind { get; set; } = ProjectItemKind.File;

        [JsonPropertyName("buildAction")]
        public ProjectItemBuildAction BuildAction { get; set; } = ProjectItemBuildAction.Compile;

        [JsonPropertyName("children")]
        public List<ProjectItem> Children { get; set; } = new List<ProjectItem>();

        [JsonIgnore]
        public ProjectItem? Parent { get; set; }

        public string GetFullPath(Project project)
        {
            var parentPath = Parent != null ? GetFullPath(project) : project.Path;
            return Path.Combine(parentPath, Name);
        }

        public void Reparent(ProjectItem? parent)
        {
            Parent = parent;
            foreach (var child in Children)
            {
                child.Reparent(this);
            }
        }
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum ProjectItemKind
    {
        File,

        Folder,

        /// <summary>
        /// File inside file, usually put in the same folder with main file
        /// </summary>
        AttachedFile,
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum ProjectItemBuildAction
    {
        Compile,
        Content,
        Resource
    }
}
