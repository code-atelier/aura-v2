﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class Project
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("version")]
        public Version Version { get; set; } = new Version(1, 0, 0);

        [JsonPropertyName("outputType")]
        public Guid OutputType { get; set; }

        [JsonPropertyName("outputPath")]
        public string OutputPath { get; set; } = "bin\\{Environment}";

        [JsonPropertyName("targetFramework")]
        public Guid TargetFramework { get; set; }

        [JsonPropertyName("assemblyName")]
        public string AssemblyName { get; set; } = "{ProjectName}";

        [JsonPropertyName("item")]
        public List<ProjectItem> Items { get; set; } = new List<ProjectItem>();

        public IEnumerable<ProjectItem> FindItems(Func<ProjectItem, bool> predicate)
        {
            return FindItems(Items, predicate);
        }

        private IEnumerable<ProjectItem> FindItems(IEnumerable<ProjectItem> projectItems, Func<ProjectItem, bool> predicate)
        {
            var sol = new List<ProjectItem>();
            sol.AddRange(projectItems.Where(predicate));
            foreach (var item in projectItems)
            {
                sol.AddRange(FindItems(item.Children, predicate));
            }
            return sol;
        }

        public void ReparentItems()
        {
            foreach(var item in Items)
            {
                item.Reparent(null);
            }
        }

        #region Internal
        [JsonIgnore]
        public string Path { get; set; } = string.Empty;

        public IEnumerable<ProjectItem>? FindPath(ProjectItem item)
        {
            var col = Items.ToList();
            var paths = new List<ProjectItem>();
            return FindPath(item, paths, col);
        }

        private IEnumerable<ProjectItem>? FindPath(ProjectItem item, List<ProjectItem> passedPaths, List<ProjectItem> itemCollection)
        {
            if (itemCollection.Contains(item)) return passedPaths;
            foreach(var p in itemCollection)
            {
                var pp = passedPaths.ToList();
                pp.Add(p);
                var re = FindPath(item, pp, p.Children);
                if (re != null) return re;
            }
            return null;
        }

        public string? GetFullPath(ProjectItem? item)
        {
            if (item == null) return Path;
            var paths = FindPath(item);
            if (paths == null) return null;
            var pList = new List<ProjectItem>();
            foreach(var p in paths)
            {
                if (p.Kind == ProjectItemKind.Folder)
                {
                    pList.Add(p);
                }
                else
                {
                    break;
                }
            }
            var fpath = System.IO.Path.Combine(string.Join('\\', pList.Select(x => x.Name)), item.Name);
            return System.IO.Path.Combine(Path, fpath);
        }

        public void Save(string path)
        {
            var json = JsonSerializer.Serialize(this, new JsonSerializerOptions()
            {
                WriteIndented = true,
            });
            File.WriteAllText(path, json);
        }

        public static Project Load(string path)
        {
            var json = File.ReadAllText(path);
            var proj = JsonSerializer.Deserialize<Project>(json); 
            if (proj == null)
            {
                throw new Exception("Invalid project");
            }
            proj.Path = System.IO.Path.GetDirectoryName(path) ?? "";
            return proj;
        }

        public string BuildOutputPath(string? fileName = null)
        {
            fileName = fileName ?? Name + ".js";
            return CodeMateService.ParseVariables(System.IO.Path.Combine(Path, OutputPath, fileName), this);
        }
        #endregion
    }
}
