﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class Solution
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("items")]
        public List<SolutionItem> Items { get; set; } = new List<SolutionItem>();

        [JsonIgnore]
        public List<Project> Projects { get; set; } = new List<Project>();

        #region Internals
        [JsonIgnore]
        public string Path { get; set; } = string.Empty;
        #endregion

        public void Save(string path)
        {
            var json = JsonSerializer.Serialize(this, new JsonSerializerOptions()
            {
                WriteIndented = true,
            });
            File.WriteAllText(path, json);

            foreach(var proj in Projects)
            {
                var itm = FindItems(x => x.Kind == SolutionItemKind.Project && x.ProjectId == proj.Id).FirstOrDefault();
                if (itm?.ProjectPath != null)
                    proj.Save(System.IO.Path.Combine(Path, itm.ProjectPath));
            }
        }

        public static Solution Load(string path)
        {
            var json = File.ReadAllText(path);
            var sol = JsonSerializer.Deserialize<Solution>(json);
            if (sol == null)
            {
                throw new Exception("Solution file is not valid");
            }

            sol.Path = System.IO.Path.GetDirectoryName(path) ?? "";
            sol.LoadProjects();

            return sol;
        }

        private void LoadProjects()
        {
            var projectItems = FindItems(x => x.Kind == SolutionItemKind.Project && !Projects.Any(w => w.Id == x.ProjectId)).ToList();
            foreach (var proj in projectItems)
            {
                proj.Loaded = false;
                if (proj.ProjectId != null && proj.ProjectPath != null)
                {
                    var projPath = System.IO.Path.Combine(Path, proj.ProjectPath);
                    if (File.Exists(projPath))
                    {
                        try
                        {
                            var p = Project.Load(projPath);
                            Projects.Add(p);
                            proj.Loaded = true;
                        }
                        catch { }
                    }
                }
            }
        }

        public IEnumerable<SolutionItem> FindItems(Func<SolutionItem, bool> predicate)
        {
            return FindItems(Items, predicate);
        }

        private IEnumerable<SolutionItem> FindItems(IEnumerable<SolutionItem> solutionItems, Func<SolutionItem, bool> predicate)
        {
            var sol = new List<SolutionItem>();
            sol.AddRange(solutionItems.Where(predicate));
            foreach (var item in solutionItems)
            {
                sol.AddRange(FindItems(item.Children, predicate));
            }
            return sol;
        }
    }
}
