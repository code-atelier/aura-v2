﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Models
{
    public class SolutionItem
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("kind")]
        public SolutionItemKind Kind { get; set; }

        [JsonPropertyName("children")]
        public List<SolutionItem> Children { get; set; } = new List<SolutionItem>();

        [JsonPropertyName("projectId")]
        public Guid? ProjectId { get; set; }

        [JsonPropertyName("projectPath")]
        public string? ProjectPath { get; set; }

        #region Internal
        [JsonIgnore]
        public bool Loaded { get; set; } = true;
        #endregion
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum SolutionItemKind
    {
        Project,
        Folder
    }
}
