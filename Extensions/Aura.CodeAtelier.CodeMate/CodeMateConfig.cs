﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    [AuraConfiguration("codemate.json")]
    [AutoConfig("Code Mate", Icon = "res/icon.png")]
    public class CodeMateConfig
    {
        [JsonPropertyName("allowNotification")]
        [AutoConfigMember("Allow Notification",
            Description = "Allow Code Mate to pop toast notification on compile events.",
            Icon = "res/file-code.png",
            Index = 0,
            Kind = AutoConfigPropertyKind.Toggle)]
        public bool AllowNotification { get; set; } = true;
    }
}
