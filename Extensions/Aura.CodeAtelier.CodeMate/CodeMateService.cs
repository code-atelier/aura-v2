﻿using Aura.CodeAtelier.Interfaces;
using Aura.CodeAtelier.Models;
using Aura.Models;
using Aura.Services;
using Aura.Services.Passive;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.CodeAtelier
{
    public class CodeMateService : PassiveServiceBase, IMessageReceiver, IServiceWithConfiguration
    {
        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = "coatl.codemate",
            Name = "Code Mate",
            Author = "CodeAtelier",
            Description = "Process auto compilation for Code Mate projects.",
            Version = CodeMateStatic.Version,
            Icon = Util.CreateResourceUri("res/icon.png"),
        };

        public override ServiceUserAction UserActions { get; } = ServiceUserAction.Configure;

        public List<ICompiler> CompilerList { get; } = new List<ICompiler>();

        private IServiceMessageBus MessageBus { get; }
        public ConfigurationService ConfigurationService { get; }

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public string Id => Info.Id;

        public bool CanReceiveMessage { get => State == ServiceState.Running; }

        [SupportedOSPlatform("Windows")]
        public CodeMateService(IServiceMessageBus messageBus, ConfigurationService configurationService)
        {
            MessageBus = messageBus;
            ConfigurationService = configurationService;
            Configuration = ConfigurationService.LoadConfig<CodeMateConfig>();
        }

        public CodeMateConfig Configuration { get; }

        private Window? ConfigWindow { get; set; }

        [SupportedOSPlatform("Windows")]
        public Window CreateConfigurationWindow()
        {
            var ac = new AutoConfigurator();
            ac.LoadConfiguration(Configuration);
            ac.Closed += Ac_Closed;
            return ac;
        }

        private void Ac_Closed(object? sender, EventArgs e)
        {
            try
            {
                if (sender is Window w && w == ConfigWindow)
                {
                    ConfigWindow = null;
                }
                ConfigurationService.SaveConfig<CodeMateConfig>();
            }
            catch { }
        }

        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                MessageBus.Subscribe(this, InternalEvents.UriRequest);
            });
        }

        public override async Task OnStart()
        {
            await Task.Run(() =>
            {
                try
                {
                    CompilerList.Clear();
                    var compilerType = typeof(ICompiler);
                    var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    var types = assemblies.SelectMany(x => x.GetTypes().Where(t => t.IsClass && !t.IsAbstract && t.IsAssignableTo(compilerType)));
                    foreach (var type in types)
                    {
                        try
                        {
                            var comp = ServiceManager.CreateInstance(type) as ICompiler;
                            if (comp != null)
                                CompilerList.Add(comp);
                        }
                        catch { }
                    }
                }
                catch { }
            });
        }

        public static string ParseVariables(string text, Project project)
        {
            return text
                .Replace("{Environment}", "debug")
                .Replace("{ProjectName}", project.Name)
                ;
        }

        public async Task<Services.Message?> ReceiveMessageAsync(Services.Message message, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var uriStart = "aura:services/" + Info.Id + "/";
                if (message.EventId == InternalEvents.UriRequest && message.HasBody<UriRequest>())
                {
                    var requets = message.GetBody<UriRequest>()?.Uris
                        .Select(x => x.ToString()).Where(x => x.ToLower().StartsWith(uriStart))
                        .Select(x => new Uri(x)).ToList();
                    if (requets != null)
                    {
                        foreach (var req in requets)
                        {
                            if (req.Segments.Length > 2)
                            {
                                var command = req.Segments[2];
                                var query = Uri.UnescapeDataString(req.Query);
                                var queries = new Dictionary<string, string>();
                                if (!string.IsNullOrWhiteSpace(query) && query.Length > 1)
                                {
                                    query = query.Substring(1);
                                    var spl = query.Split('&');
                                    foreach (var s in spl)
                                    {
                                        var kv = s.Split('=');
                                        queries[kv[0]] = kv.Length > 1 ? kv[1] : "";
                                    }
                                }
                                RunCommand(command, queries);
                            }
                        }
                    }
                }
                return null as Services.Message;
            });
        }

        private async void RunCommand(string command, Dictionary<string, string> parameters)
        {
            try
            {
                var spl = parameters["p"].Split(';');
                var completed = new List<string>();
                foreach (var s in spl)
                {
                    try
                    {
                        var project = Project.Load(s);
                        if (project == null) return;
                        var compiler = CompilerList.FirstOrDefault(x => x.OutputType.Id == project.OutputType);
                        if (compiler == null) return;
                        compiler.Compile(project);
                        completed.Add(project.Name);
                    }
                    catch { }
                }

                if (completed.Count > 0 && Configuration.AllowNotification)
                {
                    // make toast notif
                    var toast = new ToastNotification();
                    toast.Title = "Code Mate Compiler";
                    toast.Contents.Add(new ToastNotificationContent()
                    {
                        Kind = ToastNotificationContentKind.Text,
                        Text = $"{(completed.Count == 1 ? completed[0] : completed.Count + " projects")} has successfully compiled."
                    });
                    var tNotif = new Message<ToastNotification>(Id, InternalEvents.ShowToastNotificationRequest, toast);
                    await MessageBus.SendMessageAsync(tNotif);
                }
            }
            catch { }
        }
    }
}