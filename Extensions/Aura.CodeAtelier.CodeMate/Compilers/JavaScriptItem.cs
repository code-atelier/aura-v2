﻿using Aura.CodeAtelier.Interfaces;
using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Compilers
{
    public class JavaScriptItem : IProjectItemFactory
    {
        public bool AllowAddManually { get; } = true;

        public string Name { get; } = "Java Script";

        public string Category { get; } = "JavaScript";

        public string FileExtension => ".js";

        public Guid Id { get; } = Guid.Parse("1b2988ac-b1cb-454b-a93f-0598098c4917");

        public ProjectItem Create(string name, Project project, ProjectItem? parent = null)
        {
            var dir = project.GetFullPath(parent);
            if (dir == null)
            {
                throw new Exception("Invalid project directory");
            }
            name = name.Trim();
            if (!name.ToLower().EndsWith(FileExtension))
                name += FileExtension;
            var pi = new ProjectItem()
            {
                BuildAction = ProjectItemBuildAction.Compile,
                Kind = ProjectItemKind.File,
                Name = name,
            };
            if (parent == null)
            {
                project.Items.Add(pi);
            }
            else
            {
                parent.Children.Add(pi);
            }
            var fpath = Path.Combine(dir, name);
            if (fpath != null)
            {
                File.WriteAllText(fpath, "/* " + name + " */");
            }
            return pi;
        }
    }
}
