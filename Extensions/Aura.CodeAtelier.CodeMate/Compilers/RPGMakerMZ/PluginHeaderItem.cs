﻿using Aura.CodeAtelier.Interfaces;
using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Compilers.RPGMakerMZ
{
    public class PluginHeaderItem : IProjectItemFactory
    {
        public bool AllowAddManually { get; } = true;

        public string Name { get; } = "Plugin Header";

        public string Category { get; } = "RPG Maker MZ Plugin";

        public string FileExtension { get; } = ".rmmzph.js";

        public Guid Id { get; } = Guid.Parse("43759cc8-7794-43bb-932a-ea8a494e201c");

        public ProjectItem Create(string name, Project project, ProjectItem? parent = null)
        {
            var dir = project.GetFullPath(parent);
            if (dir == null)
            {
                throw new Exception("Invalid project directory");
            }
            var originalname = name = name.Trim();
            if (!name.ToLower().EndsWith(FileExtension))
                name += FileExtension;

            #region header
            var pi = new ProjectItem()
            {
                BuildAction = ProjectItemBuildAction.Compile,
                Kind = ProjectItemKind.File,
                Name = name,
            };
            if (parent == null)
            {
                project.Items.Add(pi);
            }
            else
            {
                parent.Children.Add(pi);
            }
            var fpath = Path.Combine(dir, name);
            if (fpath != null)
            {
                File.WriteAllText(fpath,
@"//=============================================================================
// RPG Maker MZ Plugin
//=============================================================================

/*:
 * @target MZ
 * @plugindesc <description>
 * @author <author>
 *
 * @help <helptext>
 */

#include main.js
"
                    );
            }
            #endregion
            #region main js
            var jspi = new ProjectItem()
            {
                BuildAction = ProjectItemBuildAction.Compile,
                Kind = ProjectItemKind.File,
                Name = "main.js",
            };
            if (parent == null)
            {
                project.Items.Add(jspi);
            }
            else
            {
                parent.Children.Add(jspi);
            }
            fpath = Path.Combine(dir, "main.js");
            if (fpath != null)
            {
                File.WriteAllText(fpath,
                    $"/* Main logic for {originalname}*/"
                    );
            }
            #endregion
            return pi;
        }
    }
}
