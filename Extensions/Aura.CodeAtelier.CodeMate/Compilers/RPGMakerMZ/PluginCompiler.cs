﻿using Aura.CodeAtelier.Interfaces;
using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Compilers.RPGMakerMZ
{
    public class PluginCompiler : ICompiler
    {
        public string Id { get; } = "coatl.rpgmakermz.plugin";

        public string Name { get; } = "RPG Maker MZ Plugin";

        public string? Icon { get; } = Util.CreateResourceUri("res/box-mz.png");

        public Version Version { get; } = new Version(1, 0, 0);

        public OutputType OutputType { get; } = new OutputType()
        {
            Id = Guid.Parse("1e920b35-5eff-4518-a55d-a08c58924881"),
            Name = "RPG Maker MZ Plugin JSON",
        };

        public IReadOnlyList<FrameworkInfo> Frameworks { get; } = new List<FrameworkInfo>()
        {
            new FrameworkInfo()
            {
                Id = Guid.Parse("b6e544f2-72e6-446e-8f92-808ca8806b70"),
                Name = "RPG Maker MZ",
                Version = new Version(1, 7, 0)
            }
        };

        public CompilerFlag Flags { get; } = CompilerFlag.None;

        public IReadOnlyList<Guid> SupportedProjectItemFactories { get; } = new List<Guid>()
        {
            Guid.Parse("1b2988ac-b1cb-454b-a93f-0598098c4917"),
            Guid.Parse("43759cc8-7794-43bb-932a-ea8a494e201c")
        };

        public void Compile(Project project)
        {
            // find header
            var headers = project.FindItems(x => x.Kind == ProjectItemKind.File && x.Name.ToLower().EndsWith(".rmmzph.js")).ToList();
            foreach (var header in headers)
            {
                var fpath = project.GetFullPath(header);
                var compiled = "";
                if (File.Exists(fpath))
                {
                    var curPath = Path.GetDirectoryName(fpath) ?? "";
                    var file = File.ReadAllLines(fpath);
                    foreach (var line in file)
                    {
                        var cline = line;
                        var tline = cline.Trim().ToLower();
                        if (tline.StartsWith("#include ") && tline.Length > 9)
                        {
                            var fname = tline.Substring(9).Trim();
                            var incf = Path.Combine(curPath, fname);
                            cline = File.ReadAllText(incf);
                        }
                        compiled += cline + Environment.NewLine;
                    }
                }

                var assemblyPath = project.BuildOutputPath(null);
                var assemblyDir = Path.GetDirectoryName(assemblyPath);
                if (!Directory.Exists(assemblyDir) && assemblyDir != null)
                {
                    Directory.CreateDirectory(assemblyDir);
                }
                File.WriteAllText(assemblyPath, compiled);
            }
        }

        public Project CreateProject(string name, string path, FrameworkInfo framework)
        {
            var fw = Frameworks.FirstOrDefault(x => x.Id == framework.Id);
            if (fw != null)
            {
                Project? p = null;
                if (fw.Version == new Version(1, 7, 0))
                {
                    p = new Project()
                    {
                        Id = Guid.NewGuid(),
                        Name = name,
                        TargetFramework = fw.Id,
                        OutputType = OutputType.Id,
                        AssemblyName = "{ProjectName}",
                        Version = new Version(1, 0, 0),
                        Path = path,
                    };
                }

                if (p != null)
                {
                    var factory = new PluginHeaderItem();
                    factory.Create(name, p);
                    return p;
                }
            }

            throw new Exception("Unsupported framework " + framework);
        }
    }
}
