﻿using Aura.CodeAtelier.WPF;
using System;
using System.Text;
using System.Windows;

namespace Aura.CodeAtelier
{
    public class StringEncodingTool : ToolBase
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Encoding Tool",
            Description = "Convert between string encodings",
            Author = "Code Atelier",
            Id = "coatl.codemate.encodingtool",
            Version = new Version(1, 0, 0),
        };

        public override string Icon { get; } = Util.CreateResourceUri("res/file-text.png");

        bool RegisteredEncodingProvider { get; set; } = false;
        public StringEncodingTool()
        {
            if (!RegisteredEncodingProvider)
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                RegisteredEncodingProvider = true;
            }
        }

        protected override Window CreateToolWindow()
        {
            return new StringEncodingToolWindow();
        }
    }
}
