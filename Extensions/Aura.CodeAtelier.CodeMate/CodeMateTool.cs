﻿using Aura.CodeAtelier.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.CodeAtelier
{
    public class CodeMateTool : ToolBase
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Code Mate",
            Description = "Open and compile Code Mate solutions",
            Author = "Code Atelier",
            Id = "coatl.codemate.editor",
            Version = CodeMateStatic.Version,
        };

        public override string Icon { get; } = Util.CreateResourceUri("res/icon.png");

        protected override Window CreateToolWindow()
        {
            return new SolutionView();
        }
    }
}
