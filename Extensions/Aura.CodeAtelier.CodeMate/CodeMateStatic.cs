﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier
{
    public static class CodeMateStatic
    {
        public static Version Version { get; } = new Version(2, 1, 5);
    }
}
