﻿using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Interfaces
{
    public interface IProjectItemFactory
    {
        Guid Id { get; }

        bool AllowAddManually { get; }

        string Name { get; }

        string Category { get; }

        string FileExtension { get; }

        ProjectItem Create(string name, Project project, ProjectItem? parent = null);
    }
}
