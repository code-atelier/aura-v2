﻿using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.CodeAtelier.Interfaces
{
    public interface ICompiler
    {
        string Id {get;}

        Version Version { get; }

        OutputType OutputType { get; }

        IReadOnlyList<FrameworkInfo> Frameworks { get; }

        CompilerFlag Flags { get; }

        string Name { get; }

        string? Icon { get; }

        public IReadOnlyList<Guid> SupportedProjectItemFactories { get; }

        void Compile(Project project);

        Project CreateProject(string name, string path, FrameworkInfo framework);
    }

    public enum CompilerFlag
    {
        None = 0,
        Deprecated = 1,
    }
}
