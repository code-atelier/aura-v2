﻿using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using DataFormats = System.Windows.DataFormats;
using Image = System.Windows.Controls.Image;
using MessageBox = System.Windows.MessageBox;
using Orientation = System.Windows.Controls.Orientation;
using Path = System.IO.Path;
using TextBox = System.Windows.Controls.TextBox;

namespace Aura.CodeAtelier.WPF
{
    /// <summary>
    /// Interaction logic for SolutionView.xaml
    /// </summary>
    public partial class SolutionView : Window
    {
        public Solution? CurrentSolution { get; private set; }
        public string? SolutionPath { get; private set; }
        public CodeMateService CodeMateService { get; }
        static string[] ImageExtensions { get; } = new string[]
        {
            "bmp", "png", "jpg", "jpeg", "tiff", "tga", "gif", "webp"
        };
        static string[] CodeExtensions { get; } = new string[]
        {
            "js", "cs", "vb", "php", "css", "ts", "xaml", "xml", "json", "asp", "aspx", "asax"
        };

        bool hasChanges = false;
        public bool HasChanges { get => hasChanges; set { hasChanges = value; UpdateRender(); } }
        DispatcherTimer DTStatus = new DispatcherTimer(DispatcherPriority.Normal)
        {
            Interval = new TimeSpan(0, 0, 10)
        };

        public SolutionView()
        {
            InitializeComponent();
            CodeMateService = ServiceManager.GetInstance<CodeMateService>();
            DTStatus.Tick += DTStatus_Tick;
        }

        private void DTStatus_Tick(object? sender, EventArgs e)
        {
            DTStatus.Stop();
            Dispatcher.Invoke(() =>
            {
                sbiCurrentAction.Content = "Ready";
            });
        }

        public void LoadSolution(string? path, Solution? solution)
        {
            if (solution != null)
            {
                foreach (var proj in solution.Projects)
                {
                    proj.ReparentItems();
                }
            }
            CurrentSolution = solution;
            SolutionPath = path;
            grEditor.Items.Clear();
            UpdateSolutionExplorer();
            UpdateRender();
        }

        private StackPanel CreateTreeViewItemHeader(string? iconUri, string header)
        {
            var image = new Image();
            image.Height = 20;
            image.Width = 20;
            image.Stretch = Stretch.Fill;
            if (iconUri != null)
            {
                try
                {
                    var img = new BitmapImage();
                    img.BeginInit();
                    img.UriSource = new Uri(iconUri);
                    img.EndInit();
                    image.Source = img;
                }
                catch { }
            }

            var textblock = new TextBlock();
            textblock.Text = header;
            textblock.VerticalAlignment = VerticalAlignment.Center;
            textblock.Margin = new Thickness(5, 0, 0, 0);

            var sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;
            sp.Children.Add(image);
            sp.Children.Add(textblock);

            return sp;
        }

        public void UpdateSolutionExplorer()
        {
            tvSolution.Items.Clear();
            if (CurrentSolution == null) { return; }

            // build solution tvi
            var tviSolution = new TreeViewItem()
            {
                Header = CreateTreeViewItemHeader(Util.CreateResourceUri("res/box-codemate.png"), "Solution: " + CurrentSolution.Name),
                Tag = CurrentSolution,
                ContextMenu = Resources["cmSolution"] as ContextMenu,
                IsExpanded = true,
            };
            tviSolution.Style = Resources["treeViewItemStyle"] as Style;
            tviSolution.MouseRightButtonDown += TviSolution_MouseRightButtonDown;
            tviSolution.Selected += TviSolution_Selected;
            tviSolution.Unselected += TviSolution_Unselected;

            // build solution items
            BuildSolutionItems(CurrentSolution.Items, tviSolution);
            tvSolution.Items.Add(tviSolution);
        }

        private void TviSolution_Unselected(object sender, RoutedEventArgs e)
        {
            if (sender is TreeViewItem item)
            {
                item.Foreground = new SolidColorBrush(Colors.White);
                e.Handled = true;
            }
        }

        private void TviSolution_Selected(object sender, RoutedEventArgs e)
        {
            if (sender is TreeViewItem item)
            {
                item.Foreground = new SolidColorBrush(Colors.Black);
                e.Handled = true;
            }
        }

        private void TviSolution_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is TreeViewItem item)
            {
                item.IsSelected = true;
                e.Handled = true;
            }
        }

        private void BuildSolutionItems(IEnumerable<SolutionItem> items, TreeViewItem tviParent)
        {
            if (CurrentSolution == null) { return; }
            foreach (var itm in items)
            {
                string? iconPath = null;
                var project = CurrentSolution.Projects.FirstOrDefault(x => x.Id == itm.ProjectId);
                if (itm.Kind == SolutionItemKind.Project && project != null)
                {
                    var compiler = CodeMateService.CompilerList.FirstOrDefault(x => x.OutputType.Id == project.OutputType);
                    if (compiler != null)
                    {
                        iconPath = compiler.Icon;
                    }
                }
                if (itm.Kind == SolutionItemKind.Folder)
                {
                    iconPath = Util.CreateResourceUri("res/folder.png");
                }
                var tviSolutionItem = new TreeViewItem()
                {
                    Header = CreateTreeViewItemHeader(iconPath, itm.Name),
                    Tag = itm,
                };
                tviSolutionItem.Style = Resources["treeViewItemStyle"] as Style;
                tviSolutionItem.MouseRightButtonDown += TviSolution_MouseRightButtonDown;
                tviSolutionItem.Selected += TviSolution_Selected;
                tviSolutionItem.Unselected += TviSolution_Unselected;

                if (itm.Kind == SolutionItemKind.Project && project != null)
                {
                    tviSolutionItem.AllowDrop = true;
                    tviSolutionItem.DragOver += tviSoliProject_DragOver;
                    tviSolutionItem.Drop += tviSoliProject_Drop;
                    tviSolutionItem.ContextMenu = Resources["cmProject"] as ContextMenu;
                    tviSolutionItem.IsExpanded = true;
                    BuildProjectItems(project.Items, tviSolutionItem);
                }
                else
                {
                    BuildSolutionItems(itm.Children, tviSolutionItem);
                }

                if (tviParent != null)
                {
                    tviParent.Items.Add(tviSolutionItem);
                }
                else
                {
                    tviSolutionItem.IsExpanded = true;
                    tvSolution.Items.Add(tviSolutionItem);
                }
            }
        }

        private void tviSoliProject_Drop(object sender, System.Windows.DragEventArgs e)
        {
            e.Handled = true;
            if (sender is TreeViewItem tvi && tvi.Tag is SolutionItem soli && CurrentSolution != null)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    var proj = CurrentSolution.Projects.FirstOrDefault(x => x.Id == soli.ProjectId);
                    if (proj != null)
                    {
                        e.Effects = System.Windows.DragDropEffects.Copy;
                        var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                        if (files?.Length > 0)
                        {
                            hasChanges = true;
                            try
                            {
                                foreach (var file in files)
                                {
                                    var fname = Path.GetFileName(file);
                                    var pi = new ProjectItem()
                                    {
                                        BuildAction = ProjectItemBuildAction.Compile,
                                        Kind = ProjectItemKind.File,
                                        Name = fname,
                                    };
                                    proj.Items.Add(pi);

                                    var fpath = proj.GetFullPath(pi);
                                    if (fpath != null)
                                    {
                                        var fdir = Path.GetDirectoryName(fpath);
                                        if (!Directory.Exists(fdir) && fdir != null)
                                        {
                                            Directory.CreateDirectory(fdir);
                                        }
                                        File.Copy(file, fpath, true);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            finally
                            {
                                UpdateSolutionExplorer();
                            }
                        }
                    }
                }
            }
        }
        private void tviSoliProject_DragOver(object sender, System.Windows.DragEventArgs e)
        {
            e.Handled = true;
            if (sender is TreeViewItem tvi && tvi.Tag is SolutionItem soli && CurrentSolution != null)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    var proj = CurrentSolution.Projects.FirstOrDefault(x => x.Id == soli.ProjectId);
                    if (proj != null)
                    {
                        e.Effects = System.Windows.DragDropEffects.Copy;
                    }
                    else
                    {
                        e.Effects = System.Windows.DragDropEffects.None;
                    }
                }
            }
        }

        private void BuildProjectItems(IEnumerable<ProjectItem> items, TreeViewItem tviParent)
        {
            if (CurrentSolution == null) { return; }
            foreach (var item in items)
            {
                string? iconPath = null;
                if (item.Kind == ProjectItemKind.Folder)
                {
                    iconPath = Util.CreateResourceUri("res/folder.png");
                }
                else
                {
                    var ext = System.IO.Path.GetExtension(item.Name).ToLower().Trim('.');
                    if (CodeExtensions.Contains(ext))
                    {
                        iconPath = Util.CreateResourceUri("res/file-code.png");
                    }
                    else if (ImageExtensions.Contains(ext))
                    {
                        iconPath = Util.CreateResourceUri("res/file-picture.png");
                    }
                    else
                    {
                        iconPath = Util.CreateResourceUri("res/file-empty.png");
                    }
                }
                var tviProjectItem = new TreeViewItem()
                {
                    Header = CreateTreeViewItemHeader(iconPath, item.Name),
                    Tag = item,
                    Foreground = new SolidColorBrush(Colors.White),
                };
                tviProjectItem.Style = Resources["treeViewItemStyle"] as Style;
                if (item.Kind != ProjectItemKind.Folder)
                {
                    tviProjectItem.MouseDoubleClick += TviProi_MouseDoubleClick;
                }
                tviProjectItem.MouseRightButtonDown += TviSolution_MouseRightButtonDown;
                tviProjectItem.Selected += TviSolution_Selected;
                tviProjectItem.Unselected += TviSolution_Unselected;
                tviProjectItem.DragOver += TviProi_DragOver;
                tviProjectItem.Drop += TviProi_Drop;

                BuildProjectItems(item.Children, tviProjectItem);

                tviParent.Items.Add(tviProjectItem);
            }
        }

        private void TviProi_Drop(object sender, System.Windows.DragEventArgs e)
        {
            e.Handled = true;
            e.Effects = System.Windows.DragDropEffects.None;
        }

        private void TviProi_DragOver(object sender, System.Windows.DragEventArgs e)
        {
            e.Handled = true;
            e.Effects = System.Windows.DragDropEffects.None;
        }

        private void TviProi_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender is TreeViewItem tvi && tvi.Tag is ProjectItem item)
            {
                var project = GetProject(tvi);
                if (project != null)
                {
                    var fpath = project.GetFullPath(item);
                    if ((Keyboard.Modifiers & ModifierKeys.Shift) > 0)
                    {
                        try
                        {
                            if (File.Exists(fpath))
                            {
                                Util.OpenFile(fpath);
                            }
                        }
                        catch { }
                        return;
                    }
                    try
                    {
                        foreach (var itm in grEditor.Items)
                        {
                            if (itm is TabItem ti)
                            {
                                if (ti.Tag == item)
                                {
                                    grEditor.SelectedItem = ti;
                                    return;
                                }
                            }
                        }
                        if (File.Exists(fpath))
                        {
                            PushOpenFile(item, fpath);
                        }
                    }
                    catch { }
                }
            }
        }

        private void PushOpenFile(ProjectItem item, string path)
        {
            var content = File.ReadAllText(path);
            var sp = new StackPanel() { Orientation = Orientation.Horizontal };
            sp.Children.Add(new Label() { Content = item.Name, Padding = new Thickness(8, 2, 8, 2) });
            var btnTab = new Button() { Content = "x", VerticalAlignment = VerticalAlignment.Center, Padding = new Thickness(4, 0, 4, 2), BorderThickness = new Thickness(0) };
            sp.Children.Add(btnTab);
            var tp = new TabItem()
            {
                Header = sp,
                Tag = item
            };
            btnTab.Tag = tp;
            btnTab.Click += BtnTab_Click;
            var tx = new TextBox()
            {
                Background = new SolidColorBrush(Colors.Transparent),
                FontFamily = new System.Windows.Media.FontFamily("Consolas"),
                Text = content,
                TextWrapping = TextWrapping.NoWrap,
                AcceptsReturn = true,
                Foreground = new SolidColorBrush(Colors.White),
                HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                Tag = item
            };
            tp.Content = tx;
            grEditor.Items.Add(tp);
            grEditor.SelectedItem = tp;
        }

        private void BtnTab_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && b.Tag is TabItem ti)
            {
                grEditor.Items.Remove(ti);
            }
        }

        private Project? GetProject(TreeViewItem tvi)
        {
            if (tvi.Tag is SolutionItem si && si.Kind == SolutionItemKind.Project)
            {
                return CurrentSolution?.Projects.FirstOrDefault(x => x.Id == si.ProjectId);
            }
            var parent = tvi.Parent;
            if (parent is TreeViewItem ptvi)
            {
                return GetProject(ptvi);
            }
            return null;
        }

        public void UpdateRender()
        {
            if (CurrentSolution != null)
            {
                Title = "Code Mate v" + CodeMateStatic.Version + " - " + CurrentSolution.Name + (hasChanges ? "*" : "");
            }
            else
            {
                Title = "Code Mate v" + CodeMateStatic.Version;
            }
        }

        private void miClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            StopAutoBuild();
        }
    }
}
