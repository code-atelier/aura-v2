﻿using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using MessageBox = System.Windows.MessageBox;
using Path = System.IO.Path;

namespace Aura.CodeAtelier.WPF
{
    /// <summary>
    /// Interaction logic for CreateSolutionWindow.xaml
    /// </summary>
    public partial class CreateSolutionWindow : Window
    {
        public CreateSolutionWindow()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        public Solution? Solution { get; private set; }
        public string? SolutionPath { get; private set; }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            var dir = Path.GetFullPath(txSolutionDir.Text.Trim());
            var name = txSolutionName.Text.Trim().Replace(' ', '.');
            if (string.IsNullOrWhiteSpace(name))
            {
                MessageBox.Show("Solution name cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var solutionDir = Path.Combine(dir, name);
            try
            {
                if (!Directory.Exists(solutionDir))
                {
                    Directory.CreateDirectory(solutionDir);
                }
            }
            catch
            {
                MessageBox.Show("Failed to create directory for new solution!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                SolutionPath = Path.Combine(solutionDir, name + ".cmsln");

                var sol = new Solution();
                sol.Id = Guid.NewGuid();
                sol.Name = name;
                sol.Save(SolutionPath);
                Solution = sol;

                DialogResult = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnPickDirectory_Click(object sender, RoutedEventArgs e)
        {
            var fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txSolutionDir.Text = fbd.SelectedPath;
            }
        }

        private void txSolutionName_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void txSolutionName_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            txSolutionName.Text = Util.SanitizeStrictId(txSolutionName.Text);
        }
    }
}
