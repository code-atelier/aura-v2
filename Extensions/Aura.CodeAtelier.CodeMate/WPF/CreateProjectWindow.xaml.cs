﻿using Aura.CodeAtelier.Interfaces;
using Aura.CodeAtelier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MessageBox = System.Windows.MessageBox;

namespace Aura.CodeAtelier.WPF
{
    /// <summary>
    /// Interaction logic for CreateProjectWindow.xaml
    /// </summary>
    public partial class CreateProjectWindow : Window
    {
        CodeMateService CodeMateService { get; }

        public CreateProjectWindow()
        {
            InitializeComponent();
            CodeMateService = ServiceManager.GetInstance<CodeMateService>();

            foreach (var compiler in CodeMateService.CompilerList)
            {
                var cbi = new ComboBoxItem()
                {
                    Content = compiler.Name,
                    Tag = compiler,
                    Foreground = new SolidColorBrush(Colors.Black),
                };
                cbProjectType.Items.Add(cbi);
            }

            if (cbProjectType.Items.Count > 0)
            {
                cbProjectType.SelectedIndex = 0;
            }
        }

        public ICompiler? Compiler { get; private set; }

        public FrameworkInfo? TargetFramework { get; private set; }

        public string? ProjectName { get; private set; }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            var name = txProjectName.Text.Trim().Replace(' ', '.');
            if (string.IsNullOrWhiteSpace(name))
            {
                MessageBox.Show("Solution name cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Guid? outputTypeId = null;
            ICompiler? compiler = null;
            if (cbProjectType.SelectedItem is ComboBoxItem cbi)
            {
                if (cbi.Tag is ICompiler comp)
                {
                    compiler = comp;
                    outputTypeId = compiler.OutputType.Id;
                }
                else
                {
                    MessageBox.Show("Please select a project type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            if (outputTypeId == null || compiler == null)
            {
                MessageBox.Show("Please select a project type!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Guid? targetFramework = null;
            FrameworkInfo? framework = null;
            if (cbTargetFramework.SelectedItem is ComboBoxItem cfw)
            {
                if (cfw.Tag is FrameworkInfo fwi)
                {
                    framework = fwi;
                    targetFramework = fwi.Id;
                }
                else
                {
                    MessageBox.Show("Please select a target framework!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            if (targetFramework == null || framework == null)
            {
                MessageBox.Show("Please select a target framework!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            ProjectName = name;
            TargetFramework = framework;
            Compiler = compiler;

            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txProjectName_TextChanged(object sender, TextChangedEventArgs e)
        {
            txProjectName.Text = Util.SanitizeStrictId(txProjectName.Text);
        }

        private void cbProjectType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTargetFramework != null)
            {
                cbTargetFramework.Items.Clear();
                if (cbProjectType.SelectedItem is ComboBoxItem cbi)
                {
                    if (cbi.Tag is ICompiler compiler)
                    {
                        foreach (var fw in compiler.Frameworks)
                        {
                            var ncb = new ComboBoxItem()
                            {
                                Content = fw.Name + " (v" + fw.Version + ")",
                                Tag = fw,
                                Foreground = new SolidColorBrush(Colors.Black),
                            };
                            cbTargetFramework.Items.Add(ncb);
                        }
                        if (cbTargetFramework.Items.Count > 0)
                        {
                            cbTargetFramework.SelectedIndex = 0;
                        }
                    }
                }
            }
        }
    }
}
