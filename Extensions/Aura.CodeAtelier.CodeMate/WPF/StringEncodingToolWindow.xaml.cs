﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.IO;
using System.Text;
using System.Windows;
using DataObject = System.Windows.DataObject;

namespace Aura.CodeAtelier.WPF
{
    /// <summary>
    /// Interaction logic for StringEncodingToolWindow.xaml
    /// </summary>
    public partial class StringEncodingToolWindow : Window
    {
        Dictionary<string, Encoding> Encodings { get; } = new Dictionary<string, Encoding>()
        {
            { "UTF-8", Encoding.UTF8 },
            { "ASCII", Encoding.ASCII },
            { "Unicode", Encoding.Unicode },
            { "Latin1", Encoding.Latin1 },
            { "CP37", Encoding.GetEncoding(37) },
            { "Shift-JIS", Encoding.GetEncoding(932) },
            { "CP1252", Encoding.GetEncoding(1252) },
            { "EUC-1", Encoding.GetEncoding(20932) },
            { "EUC-2", Encoding.GetEncoding(51932) },
            { "ISO-2022-JP-1", Encoding.GetEncoding(50220) },
            { "ISO-2022-JP-2", Encoding.GetEncoding(50221) },
            { "ISO-2022-JP-3", Encoding.GetEncoding(50222) },
            { "UTF-32", Encoding.UTF32 },
            { "UTF-32BE", Encoding.GetEncoding(12001) },
#pragma warning disable SYSLIB0001 // Type or member is obsolete
            { "UTF-7", Encoding.UTF7 },
#pragma warning restore SYSLIB0001 // Type or member is obsolete
        };

        byte[]? RawData { get; set; }
        string? RawDataEncoding { get; set; }
        bool IsFromClipboard { get; set; } 
        string? FileName { get; set; }

        public StringEncodingToolWindow()
        {
            InitializeComponent();
            cbEncoding.Items.Clear();
            cbInputEncoding.Items.Clear();
            foreach (var encoding in Encodings)
            {
                cbEncoding.Items.Add(encoding.Key);
                cbInputEncoding.Items.Add(encoding.Key);
            }
            cbEncoding.SelectedIndex = 0;
            cbInputEncoding.SelectedIndex = 0;
            txInput.FontFamily = new System.Windows.Media.FontFamily("Consolas");

            DataObject.AddPastingHandler(txInput, OnPasteInput);
        }

        private void OnPasteInput(object sender, DataObjectPastingEventArgs e)
        {
            try
            {
                e.Handled = true;
                e.CancelCommand();
                var encoding = Encodings[cbInputEncoding.SelectedItem.ToString() ?? "UTF-8"];
                if (e.SourceDataObject.GetDataPresent(DataFormats.Text, false))
                {
                    var str = e.SourceDataObject.GetData(DataFormats.Text) as string;
                    RawDataEncoding = cbInputEncoding.SelectedItem.ToString() ?? "UTF-8";
                    IsFromClipboard = true;
                    RawData = str != null ? encoding.GetBytes(str) : null;
                    FileName = null;
                    ReloadRawData();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnLoadFile_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Text Files|*.txt;*.log;readme;*.md|Any Files|*.*";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    IsFromClipboard = false;
                    RawDataEncoding = cbInputEncoding.SelectedItem.ToString() ?? "UTF-8";
                    RawData = File.ReadAllBytes(ofd.FileName);
                    FileName = Path.GetFileName(ofd.FileName);
                    ReloadRawData();
                }
                catch { }
            }
        }

        private void ReloadRawData()
        {
            skipTextChangeEvent = true;
            try
            {
                if (RawData != null)
                {
                    var encoding = Encodings[cbInputEncoding.SelectedItem.ToString() ?? "UTF-8"];
                    txInput.Text = encoding.GetString(RawData);
                    lbRawBytes.Content = "Raw bytes: " + RawData.Length + (IsFromClipboard ? " (Read from Clipboard as " + RawDataEncoding + ")" : "");
                }
                else
                {
                    txInput.Text = "";
                    lbRawBytes.Content = "Raw bytes:";
                }
            }
            finally {
                skipTextChangeEvent = false;
            }
        }

        private void txInput_GotFocus(object sender, RoutedEventArgs e)
        {
            txInput.SelectAll();
        }

        private void cbInputEncoding_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ReloadRawData();
        }

        bool skipTextChangeEvent = false;
        private void txInput_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (skipTextChangeEvent) return;
            var encoding = Encodings[cbInputEncoding.SelectedItem.ToString() ?? "UTF-8"];
            RawData = encoding.GetBytes(txInput.Text);
            ReloadRawData();
        }

        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {
            var sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.Filter = "Any Files|*.*";
            sfd.FileName = FileName ?? "";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    var encoding = Encodings[cbEncoding.SelectedItem.ToString() ?? "UTF-8"];
                    var bytes = RawData != null ? encoding.GetBytes(txInput.Text) : new byte[0];

                    File.WriteAllBytes(sfd.FileName, bytes);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnPasteClipboard_Click(object sender, RoutedEventArgs e)
        {
            if (Clipboard.ContainsText(TextDataFormat.Text))
            {
                try
                {
                    var encoding = Encodings[cbInputEncoding.SelectedItem.ToString() ?? "UTF-8"];
                    var str = Clipboard.GetText(TextDataFormat.Text);
                    RawData = str != null ? encoding.GetBytes(str) : null;
                    RawDataEncoding = cbInputEncoding.SelectedItem.ToString() ?? "UTF-8";
                    IsFromClipboard = true;
                    FileName = null;
                    ReloadRawData();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnConvert_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var inpEncoding = Encodings[cbInputEncoding.SelectedItem.ToString() ?? "UTF-8"];
                var outEncoding = Encodings[cbEncoding.SelectedItem.ToString() ?? "UTF-8"];
                var bytes = RawData != null ? inpEncoding.GetBytes(txInput.Text) : new byte[0];
                var convertedBytes = Encoding.Convert(inpEncoding, outEncoding, bytes);
                var text = outEncoding.GetString(convertedBytes);

                var sByte = Encoding.Default.GetBytes("1æfæ¦(òPéáéF)");
                var li = new List<string>();
                foreach (var enc in Encodings.Values)
                {
                    var decoded = enc.GetString(sByte);
                    li.Add(decoded);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
