﻿using Aura.CodeAtelier.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace Aura.CodeAtelier.WPF
{
    public partial class SolutionView
    {
        public static RoutedCommand BuildSolution { get; } = new RoutedCommand();
        public static RoutedCommand NewProject { get; } = new RoutedCommand();
        public static RoutedCommand BuildProject { get; } = new RoutedCommand();
        public static RoutedCommand AddProjectFile { get; } = new RoutedCommand();
        public static RoutedCommand NewProjectFile { get; } = new RoutedCommand();

        public static RoutedCommand EnableAutoBuild { get; } = new RoutedCommand();
        public static RoutedCommand DisableAutoBuild { get; } = new RoutedCommand();

        private void CommandAlwaysOn_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (HasChanges &&
                MessageBox.Show("Unsaved changes will be lost if you create a new solution.\r\nDo you want to continue?", "Warning",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
            {
                return;
            }

            var csol = new CreateSolutionWindow();
            if (csol.ShowDialog() == true && csol.Solution != null && csol.SolutionPath != null)
            {
                LoadSolution(csol.SolutionPath, csol.Solution);
            }
        }

        private void CommandOpen_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (HasChanges &&
                MessageBox.Show("Unsaved changes will be lost if you open another solution.\r\nDo you want to continue?", "Warning",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
            {
                return;
            }

            var ofd = new OpenFileDialog();
            ofd.Filter = "Code Mate Solution|*.cmsln";
            if (ofd.ShowDialog() == true)
            {
                try
                {
                    var sol = Solution.Load(ofd.FileName);
                    LoadSolution(ofd.FileName, sol);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void CommandNewProject_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tvSolution.SelectedItem is TreeViewItem tvi && tvi.Tag is Solution && CurrentSolution != null;
        }

        private void CommandNewProject_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (tvSolution.SelectedItem is TreeViewItem tvi && tvi.Tag is Solution && CurrentSolution != null)
            {
                var np = new CreateProjectWindow();
                if (np.ShowDialog() == true && np.Compiler != null && np.TargetFramework != null && np.ProjectName != null)
                {
                    var dir = Path.GetDirectoryName(SolutionPath);
                    if (Directory.Exists(dir))
                    {
                        var subd = Path.Combine(dir, np.ProjectName);
                        if (!Directory.Exists(subd))
                        {
                            Directory.CreateDirectory(subd);
                        }
                        var proj = np.Compiler.CreateProject(np.ProjectName, subd, np.TargetFramework);
                        CurrentSolution.Items.Add(new SolutionItem()
                        {
                            Name = np.ProjectName,
                            Kind = SolutionItemKind.Project,
                            ProjectId = proj.Id,
                            ProjectPath = proj.Path.Replace(CurrentSolution.Path, "").Trim('\\') + "\\" + np.ProjectName + ".cmproj",
                        });
                        CurrentSolution.Projects.Add(proj);
                        HasChanges = true;
                        UpdateSolutionExplorer();
                    }
                }
            }
        }

        private void CommandMustHaveOpenSolution_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CurrentSolution != null && SolutionPath != null;
        }

        private void CommandSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (CurrentSolution != null && SolutionPath != null)
            {
                try
                {
                    CurrentSolution.Save(SolutionPath);
                    HasChanges = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void CommandAddNewProjectFile_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (tvSolution.SelectedItem is TreeViewItem tvi && ((tvi.Tag is SolutionItem si && si.Kind == SolutionItemKind.Project && si.ProjectId != null) || tvi.Tag is ProjectItem))
            {
                e.CanExecute = true;
            }
        }

        private void CommandNewProjectFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (tvSolution.SelectedItem is TreeViewItem tvi && CurrentSolution is Solution solution)
            {
                if (tvi.Tag is SolutionItem si && si.Kind == SolutionItemKind.Project && si.ProjectId != null)
                {
                    var proj = solution.Projects.Where(x => x.Id == si.ProjectId).FirstOrDefault();
                    if (proj != null)
                    {

                    }
                }
                else if (tvi.Tag is ProjectItem projectItem)
                {

                }
            }
        }

        private void CommandAddProjectFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        volatile bool isBuilding = false;
        private void CommandBuildProject_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tvSolution.SelectedItem is TreeViewItem tvi && CurrentSolution is Solution && tvi.Tag is SolutionItem soli && soli.Loaded && soli.Kind == SolutionItemKind.Project;
        }

        private async void CommandBuildProject_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (tvSolution.SelectedItem is TreeViewItem tvi && CurrentSolution is Solution solution
                && tvi.Tag is SolutionItem soli && soli.Loaded && soli.Kind == SolutionItemKind.Project)
            {
                var project = solution.Projects.FirstOrDefault(x => x.Id == soli.ProjectId);
                if (project == null) { return; }
                try
                {
                    await CompileProject(project);
                    //MessageBox.Show("Compiled successfully", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch
                {
                    //MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private async Task CompileProject(Project project)
        {
            if (isBuilding) return;
            var compiler = CodeMateService.CompilerList.FirstOrDefault(x => x.OutputType.Id == project.OutputType);
            if (compiler == null) { return; }

            try
            {
                var timeStart = DateTime.Now;
                isBuilding = true;
                Dispatcher.Invoke(() =>
                {
                    sbiCurrentAction.Content = "Compiling project '" + project.Name + "'...";
                });
                await Task.Run(() =>
                {
                    compiler.Compile(project);
                });
                Dispatcher.Invoke(() =>
                {
                    var delta = DateTime.Now - timeStart;
                    sbiCurrentAction.Content = $"Project '{project.Name}' compiled successfully ({Math.Round(delta.TotalSeconds, 2)}s)";
                });
                DTStatus.Stop();
                DTStatus.Start();
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(() =>
                {
                    sbiCurrentAction.Content = "Failed to compile project '" + project.Name + "': " + ex.Message;
                });
                throw;
            }
            finally
            {
                isBuilding = false;
            }
        }

        private async void CommandBuildSolution_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (isBuilding) return;
            await CompileSolution();
        }

        bool isCompilingSolution = false;
        private async Task CompileSolution()
        {
            if (isCompilingSolution) return;
            try
            {
                isCompilingSolution = true;
                if (CurrentSolution is Solution solution)
                {
                    var timeStart = DateTime.Now;
                    foreach (var project in solution.Projects)
                    {
                        await CompileProject(project);
                    }
                    Dispatcher.Invoke(() =>
                    {
                        var delta = DateTime.Now - timeStart;
                        sbiCurrentAction.Content = $"Solution '{solution.Name}' compiled successfully ({Math.Round(delta.TotalSeconds, 2)}s)";
                    });
                }
            }
            finally
            {
                isCompilingSolution = false;
            }
        }

        private void CanExecute_WhenHasSolution(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CurrentSolution != null;
        }

        #region Auto Build
        bool AutoBuildRunning = false;
        const int AutoBuildMinSeconds = 3;
        DateTime? LastAutoBuild { get; set; } = null;
        DispatcherTimer AutoBuildTimer { get; } = new DispatcherTimer()
        {
            Interval = new TimeSpan(0, 0, 1),
        };

        private void EnableAutoBuild_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (CurrentSolution == null) return;
            AutoBuildTimer.Tick += AutoBuildTimer_Tick;
            AutoBuildTimer.Start();
            miDisableAutoBuild.IsChecked = false;
            miEnableAutoBuild.IsChecked = true;
            FileSystemWatcher.Path = CurrentSolution.Path;
            FileSystemWatcher.EnableRaisingEvents = true;
            FileSystemWatcher.Changed += FileSystemWatcher_Changed;
            FileSystemWatcher.Created += FileSystemWatcher_Changed;
        }

        private void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (isBuilding || isCompilingSolution)
            {
                return;
            }

            if (CurrentSolution is Solution solution)
            {
                if (solution.Projects.Any(p => Path.Combine(p.Path, p.Name + ".cmproj").Trim('\\').ToLower() == e.FullPath.Trim('\\').ToLower()) || solution.Projects.Any(p => p.Items.Any(i => i.GetFullPath(p).Trim('\\').ToLower() == e.FullPath.Trim('\\').ToLower())))
                    LastAutoBuild = DateTime.Now;
            }
        }

        private void StopAutoBuild()
        {
            AutoBuildTimer.Stop();
            AutoBuildTimer.Tick -= AutoBuildTimer_Tick;
            miDisableAutoBuild.IsChecked = true;
            miEnableAutoBuild.IsChecked = false;
            FileSystemWatcher.EnableRaisingEvents = false;
            FileSystemWatcher.Changed -= FileSystemWatcher_Changed;
            FileSystemWatcher.Created -= FileSystemWatcher_Changed;
        }

        private void DisableAutoBuild_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            StopAutoBuild();
        }

        private async void AutoBuildTimer_Tick(object? sender, EventArgs e)
        {
            if (AutoBuildRunning || LastAutoBuild == null) return;
            var delta = DateTime.Now - LastAutoBuild.Value;
            if (delta.TotalSeconds >= AutoBuildMinSeconds)
            {
                try
                {
                    AutoBuildRunning = true;
                    await CompileSolution();
                    LastAutoBuild = null;
                }
                catch { }
                finally
                {
                    AutoBuildRunning = false;
                }
            }
        }

        FileSystemWatcher FileSystemWatcher { get; } = new FileSystemWatcher()
        {
            IncludeSubdirectories = true,
            EnableRaisingEvents = false,
        };
        #endregion
    }
}