﻿using Aura.Models;
using Aura.Script;
using Aura.Script.Expressions;
using Aura.Services.Active;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services.Passive
{
    public class ScriptingService : PassiveServiceBase, IMessageReceiver
    {
        #region Info
        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = InternalServices.ScriptingService,
            Name = "Scripting Service",
            Author = "CodeAtelier",
            Description = "Allows processing of Aura Script. This is a core service and should not be stopped.",
            Version = new Version(1, 0, 0),
        };

        public override ServiceUserAction UserActions => ServiceUserAction.None;

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Low;

        public string Id { get => Info.Id; }

        public bool CanReceiveMessage { get; set; } = true;
        #endregion

        IServiceMessageBus MessageBus { get; }

        TagService TagService { get; }

        public ScriptingService(IServiceMessageBus bus, TagService tagService)
        {
            MessageBus = bus;
            TagService = tagService;
        }

        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                MessageBus.Subscribe(this);
            });
        }

        private Dictionary<string, AuraScriptExpression> Events { get; } = new Dictionary<string, AuraScriptExpression>();

        private Persona? Persona { get; set; }

        public void LoadPersona(Persona? persona)
        {
            Persona = persona;
            TagService.RemoveTagsBySource("scriptingservice");
            LoadEvents();
        }

        private void LoadEvents()
        {
            try
            {
                CanReceiveMessage = false;
                var parser = new AuraScriptParser();
                Events.Clear();
                if (Persona == null) return;
                foreach (var evt in Persona.Events)
                {
                    if (!evt.Active) continue;
                    try
                    {
                        var res = parser.Parse(evt.Script);
                        if (res is AuraScriptExpression exp)
                        {
                            Events[evt.Name] = exp;
                        }
                    }
                    catch { }
                }
            }
            catch { }
            finally
            {
                CanReceiveMessage = true;
            }
        }

        Dictionary<string, Action<Message, AuraScriptContext>> ContextFactory { get; } = new Dictionary<string, Action<Message, AuraScriptContext>>()
        {
            { InternalEvents.FileDrop, 
                (m, c) => {
                    c.Trigger = "e:file_dropped";
                    c.TriggeringEntity = string.Join(";", m.GetBody<FileDropEventBody>()?.Files ?? new List<string>()).ToLower();
                } 
            },
            { InternalEvents.StartMenuOpened,
                (m, c) => {
                    c.Trigger = "e:startmenu_opened";
                    c.TriggeringEntity = "";
                }
            },
        };

        private AuraScriptContext? CreateContext(Message message)
        {
            if (Persona == null) return null;
            var ret = new AuraScriptContext(Persona);
            ret.ActiveTags = TagService.ActiveTags;
            if (!ContextFactory.ContainsKey(message.EventId))
            {
                ret.Trigger = "e:" + message.EventId;
                return ret;
            }
            ContextFactory[message.EventId].Invoke(message, ret);
            return ret;
        }

        public async Task<Message?> ReceiveMessageAsync(Message message, CancellationToken cancellationToken)
        {
            var context = CreateContext(message);
            if (context == null)
                return null;
            await Task.Run(() =>
            {
                try
                {
                    CanReceiveMessage = false;
                    foreach (var ev in Events.Values)
                    {
                        try
                        {
                            ev.Eval(context);
                        }
                        catch
                        {
                        }
                    }
                    foreach (var tag in context.SetTag)
                    {
                        TagService.Add("scriptingservice", tag.Tag, tag.ExpiresAt, tag.Weight);
                    }
                }
                catch
                {
                }
                finally
                {
                    CanReceiveMessage = true;
                }
            });
            return null;
        }
    }
}
