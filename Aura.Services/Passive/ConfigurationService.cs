﻿using Aura.Models.Configs;
using System.IO;
using System.Text.Json;

namespace Aura.Services.Passive
{
    public class ConfigurationService : PassiveServiceBase
    {
        const string BaseConfigurationPath = "data\\configs\\";
        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = InternalServices.ConfigurationService,
            Name = "Configuration Service",
            Author = "CodeAtelier",
            Description = "Facilitates loading and saving configurations for other services. This is a core service and should not be stopped.",
            Version = new Version(1, 0, 0),
        };
        public override ServiceUserAction UserActions => ServiceUserAction.None;

        public ConfigurationService()
        {
        }

        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                if (!Directory.Exists(BaseConfigurationPath))
                {
                    Directory.CreateDirectory(BaseConfigurationPath);
                }
            });
        }

        protected Dictionary<Type, object> LiveConfigurations { get; } = new Dictionary<Type, object>();

        public bool CanReceiveMessage { get; } = true;

        /// <summary>
        /// Loads a configuration file. If it does not exists, a new one will be created. Loaded configuration will become live configuration.
        /// </summary>
        /// <returns>New instance of configuration as <typeparamref name="T"/></returns>
        public T LoadConfig<T>(bool forceFromFile = false) where T : class, new()
        {
            var type = typeof(T);
            if (!forceFromFile && LiveConfigurations.ContainsKey(type) && LiveConfigurations[type] is T config)
            {
                return config;
            }
            var cattrb = type.GetCustomAttributes(false).OfType<AuraConfigurationAttribute>().FirstOrDefault();
            var fName = cattrb?.FileName;
            if (string.IsNullOrWhiteSpace(fName))
            {
                fName = type.Name + ".json";
            }
            var cfgPath = Path.Combine(BaseConfigurationPath, fName);
            if (!File.Exists(cfgPath))
            {
                var inst = new T();
                LiveConfigurations[type] = inst;
                return inst;
            }
            var json = File.ReadAllText(cfgPath);
            try
            {
                var cfg = JsonSerializer.Deserialize<T>(json);
                if (cfg != null)
                {
                    LiveConfigurations[type] = cfg; 
                    try
                    {
                        SaveConfig<T>();
                    }
                    catch { }
                    return cfg;
                }
            }
            catch { }
            var instance = new T();
            LiveConfigurations[type] = instance;
            try
            {
                SaveConfig<T>();
            }
            catch { }
            return instance;
        }

        /// <summary>
        /// Loads a configuration file.
        /// </summary>
        /// <returns>New instance of configuration as <typeparamref name="T"/></returns>
        public async Task<T?> LoadConfigAsync<T>() where T : class, new()
        {
            return await Task.Run(() =>
            {
                return LoadConfig<T>();
            });
        }

        /// <summary>
        /// Writes a configuration file.
        /// </summary>
        /// <param name="value">The content of the configuration file.</param>
        private void SaveConfig<T>(T value) where T : class, new()
        {
            var type = typeof(T);
            var cattrb = type.GetCustomAttributes(false).OfType<AuraConfigurationAttribute>().FirstOrDefault();
            var fName = cattrb?.FileName;
            if (string.IsNullOrWhiteSpace(fName))
            {
                fName = type.Name + ".json";
            }
            var cfgPath = Path.Combine(BaseConfigurationPath, fName);
            var cfgDir = Path.GetDirectoryName(cfgPath);
            if (!string.IsNullOrWhiteSpace(cfgDir) && !Directory.Exists(cfgDir))
            {
                Directory.CreateDirectory(cfgDir);
            }
            var json = JsonSerializer.Serialize(value, new JsonSerializerOptions() { WriteIndented = true });
            File.WriteAllText(cfgPath, json);
        }

        /// <summary>
        /// Save live configuration to config dir.
        /// </summary>
        public void SaveConfig<T>() where T : class, new()
        {
            var type = typeof(T);
            if (LiveConfigurations.ContainsKey(type) && LiveConfigurations[type] is T config)
            {
                SaveConfig(config);
            }
        }

        /// <summary>
        /// Save live configuration to config dir.
        /// </summary>
        public async Task SaveConfigAsync<T>() where T : class, new()
        {
            await Task.Run(() =>
            {
                SaveConfig<T>();
            });
        }

        public override async Task OnStart()
        {
            var auraConfig = LoadConfig<AuraConfig>() ?? new AuraConfig();
            await SaveConfigAsync<AuraConfig>();
        }

        public override async Task OnShutdown()
        {
            await SaveConfigAsync<AuraConfig>();
        }
    }
}
