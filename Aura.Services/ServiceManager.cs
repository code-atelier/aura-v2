﻿using Aura.IO;
using Aura.Models;
using Aura.Models.Configs;
using Aura.Services;
using Aura.Services.Active;
using Aura.Services.Passive;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Aura
{
    /// <summary>
    /// A static class that manages services.
    /// </summary>
    public static class ServiceManager
    {
        public static List<Type> FileDropMenus { get; } = new List<Type>() {
            typeof(FileDropMenu)
        };

        public static Dispatcher? MainDispatcher { get; set; }

        public static Persona? Persona { get; set; }

        public static IChatbotService? GetChatbotService()
        {
            return Services.FirstOrDefault(x => x is IChatbotService && x.State == ServiceState.Running) as IChatbotService;
        }

        #region Initializer
        /// <summary>
        /// Loads core services and extensions
        /// </summary>
        public static void LoadComponents()
        {
            AddSingleton(new AuraLogger());
            try
            {
                var files = Directory.EnumerateFiles(AuraLogger.DefaultLogPath, "*.aura.log");
                foreach (var f in files)
                {
                    try
                    {
                        var fi = new FileInfo(f);
                        var age = DateTime.Now - fi.LastWriteTime;
                        if (age.TotalDays > 7)
                            File.Delete(f);
                    }
                    catch { }
                }
            }
            catch { }
            var logger = GetInstance<AuraLogger>();
            logger.Information("Loading services...");

            AddSingleton<VirtualFileSystem, VirtualFileSystem>();
            AddSingleton<IServiceMessageBus, ServiceMessageBus>();

            if (!ServiceUtil.SafeMode)
            {
                // load services from assembly
                if (!Directory.Exists(AuraPaths.Services))
                {
                    Directory.CreateDirectory(AuraPaths.Services);
                }
                var assemblies = Directory.EnumerateFiles(AuraPaths.Services, "*.dll");
                foreach (var asmf in assemblies)
                {
                    try
                    {
                        logger.Debug("Found assembly '" + Path.GetFileName(asmf) + "'");
                        var fpath = Path.GetFullPath(asmf);
                        var asm = Assembly.LoadFile(fpath);
                        var isvc = typeof(IService);
                        var services = asm.GetTypes().Where(x => x.IsAssignableTo(isvc) && x.IsClass && !x.IsAbstract);
                        foreach (var service in services)
                        {
                            logger.Debug($"Found service '{service.Name}' in '" + Path.GetFileName(asmf) + "'");
                            var obsolete = service.GetCustomAttribute<ObsoleteAttribute>();
                            if (obsolete == null)
                                RegisterService(service);
                        }

                        // Get Drop Menus
                        var idropmenu = typeof(IFileDropMenu);
                        var menus = asm.GetTypes().Where(x => x.IsAssignableTo(idropmenu) && x.IsClass && !x.IsAbstract);
                        FileDropMenus.AddRange(menus);

                        // Get Tools
                        var itool = typeof(ITool);
                        var tools = asm.GetTypes().Where(x => x.IsAssignableTo(itool) && x.IsClass && !x.IsAbstract);
                        foreach (var tool in tools)
                        {
                            logger.Debug($"Found tool '{tool.Name}' in '" + Path.GetFileName(asmf) + "'");
                            var obsolete = tool.GetCustomAttribute<ObsoleteAttribute>();
                            if (obsolete == null)
                                RegisterTool(tool);
                        }

                        // Get ConsoleExt
                        var icex = typeof(IConsoleExtension);
                        var cexs = asm.GetTypes().Where(x => x.IsAssignableTo(icex) && x.IsClass && !x.IsAbstract);
                        foreach (var cex in cexs)
                        {
                            logger.Debug($"Found console extension '{cex.Name}' in '" + Path.GetFileName(asmf) + "'");
                            var obsolete = cex.GetCustomAttribute<ObsoleteAttribute>();
                            if (obsolete == null)
                                RegisterConsoleExtension(cex);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Failed to load assembly '" + Path.GetFileName(asmf) + "': " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Creates all services and initialize them.
        /// </summary>
        public static async Task InitializeServices()
        {
            var loadList = ServiceCollection.Where(x => x.Value == null).ToList();
            loadList.Sort((a, b) => ServiceCompare(a.Key, b.Key));

            foreach (var service in loadList)
            {
                var svc = ActivateService(service.Key);
                ServiceCollection[service.Key] = svc;
                await svc.Initialize();
            }

            foreach (var tool in ToolCollection)
            {
                try
                {
                    var instance = CreateInstance(tool) as ITool;
                    if (instance != null)
                    {
                        Tools.Add(instance);
                    }
                }
                catch { }
            }

            foreach (var cex in ConsoleExtensionCollection)
            {
                try
                {
                    var instance = CreateInstance(cex) as IConsoleExtension;
                    if (instance != null && !ConsoleExtensions.Any(x => x.Command.ToLower() == instance.Command.ToLower()))
                    {
                        ConsoleExtensions.Add(instance);
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Create an instance of a service.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static IService ActivateService(Type type)
        {
            var tIService = typeof(IService);
            var ctors = type.GetConstructors();
            if (ctors.Length == 1)
            {
                var ctorParams = ctors[0].GetParameters();

                // build parameter for ctor
                var ctorArgs = new List<object?>();
                foreach (var param in ctorParams)
                {
                    try
                    {
                        var argValue = GetInstance(param.ParameterType);
                        ctorArgs.Add(argValue);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Failed to activate '{type.FullName}'", ex);
                    }
                }

                // create instance of the service
                var inst = Activator.CreateInstance(type, ctorArgs.ToArray());
                if (inst is IService instIService)
                {
                    if (inst is IChatbotService)
                    {
                        ChatbotEnumerator.Items.Add(instIService.Info.Name, instIService.Info.Id);
                    }
                    return instIService;
                }
                else
                {
                    throw new Exception($"Failed to activate service '{type.FullName}'");
                }
            }
            throw new Exception($"Service '{type.FullName}' must have exactly one constructor.");
        }
        #endregion

        #region Dependency Injection

        /// <summary>
        /// Contains all dependencies.
        /// </summary>
        private static Dictionary<Type, DependencyContext> Dependencies { get; } = new Dictionary<Type, DependencyContext>();

        public static T GetInstance<T>()
        {
            var inst = GetInstance(typeof(T));
            if (inst is T Tinst)
            {
                return Tinst;
            }
            throw new Exception($"Failed to get an instance of '{typeof(T).FullName}'");
        }

        /// <summary>
        /// Gets an instance of a type from dependency injector.
        /// </summary>
        /// <exception cref="Exception"></exception>
        /// <returns>Same instance for singletons, new instance for transients.</returns>
        public static object GetInstance(Type type)
        {
            if (ServiceCollection.ContainsKey(type))
            {
                return ServiceCollection[type] ?? throw new Exception($"Service '{type.FullName}' does not have an instance.");
            }

            if (Dependencies.ContainsKey(type))
            {
                var dep = Dependencies[type];
                if (dep.Type == DependencyContextType.Singleton)
                {
                    if (dep.Instance == null)
                    {
                        throw new Exception($"Instance is not generated for singleton of '{type.FullName}'");
                    }
                    return dep.Instance;
                }
                else
                {
                    return CreateInstance(dep.TargetType);
                }
            }
            throw new Exception($"Cannot find dependency for '{type.FullName}'");
        }

        /// <summary>
        /// Instantiate a type.
        /// </summary>
        /// <returns>New instance of the type.</returns>
        public static object CreateInstance(Type type)
        {
            var ctor = type.GetConstructors();
            ConstructorInfo ct;
            if (ctor.Length == 1)
            {
                ct = ctor[0];
            }
            else if (ctor.Length > 1)
            {
                ct = ctor.FirstOrDefault(x => x.GetParameters().Length > 0) ?? ctor[0];
            }
            else
            {
                throw new Exception($"'{type.FullName}' does not have a public constructor");
            }

            var ctorParams = ct.GetParameters();

            // build parameter for ctor
            var ctorArgs = new List<object>();
            foreach (var param in ctorParams)
            {
                try
                {
                    var argValue = GetInstance(param.ParameterType);
                    ctorArgs.Add(argValue);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Failed to activate '{type.FullName}'", ex);
                }
            }

            // create instance of the type
            var inst = Activator.CreateInstance(type, ctorArgs.ToArray());
            if (inst != null)
            {
                return inst;
            }
            else
            {
                throw new Exception($"Failed to instantiate '{type.FullName}'");
            }

        }

        public static void AddSingleton<TType, TInstance>()
        {
            var tType = typeof(TType);
            var tInstance = typeof(TInstance);

            if (Dependencies.ContainsKey(tType) || ServiceCollection.ContainsKey(tType))
            {
                throw new Exception($"'{tType.FullName}' already registered");
            }
            var inst = CreateInstance(tInstance);
            Dependencies.Add(tType, new DependencyContext(tType, tInstance, inst));
        }

        public static void AddSingleton<TType>(TType instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }
            var tType = typeof(TType);

            if (Dependencies.ContainsKey(tType) || ServiceCollection.ContainsKey(tType))
            {
                throw new Exception($"'{tType.FullName}' already registered");
            }
            Dependencies.Add(tType, new DependencyContext(tType, instance.GetType(), instance));
        }

        public static void AddTransient<TType, TInstance>()
        {
            var tType = typeof(TType);
            var tInstance = typeof(TInstance);

            if (Dependencies.ContainsKey(tType) || ServiceCollection.ContainsKey(tType))
            {
                throw new Exception($"'{tType.FullName}' already registered");
            }

            Dependencies.Add(tType, new DependencyContext(tType, tInstance));
        }
        #endregion

        #region Service Controls
        /// <summary>
        /// Starts core services and extensions based on auto-start configuration
        /// </summary>
        public static async Task AutoStartServices()
        {
            var autoStartConfig = new Dictionary<string, bool>();
            var configService = Services.OfType<ConfigurationService>().FirstOrDefault();
            var logger = GetInstance<AuraLogger>();
            if (configService != null)
            {
                var auraConfig = await configService.LoadConfigAsync<AuraConfig>();
                if (auraConfig != null)
                {
                    autoStartConfig = auraConfig.AutoStartService;
                }
            }
            var loadedServices = Services;
            loadedServices.Sort((a, b) => ServiceCompare(a.GetType(), b.GetType()));
            foreach (var s in loadedServices)
            {
                var autoStart = !autoStartConfig.ContainsKey(s.Info.Id) || autoStartConfig[s.Info.Id];
                if ((autoStart && !ServiceUtil.SafeMode) || ServiceUtil.IsSystemService(s))
                {
                    try
                    {
                        logger.Information($"Starting service [{s.Info}]...");
                        _ = s.Start();
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Failed to start service [{s.Info}]!", ex);
                    }
                }
            }
        }

        /// <summary>
        /// Stops all service
        /// </summary>
        /// <returns></returns>
        public static async Task ShutdownServices()
        {
            var svcs = Services;
            foreach (var svc in svcs)
            {
                await svc.Shutdown();
            }

            foreach (var tool in Tools)
            {
                try
                {
                    tool.Close();
                }
                catch { }
            }
        }
        #endregion

        #region Tool-Related
        public static List<Type> ToolCollection { get; } = new List<Type>();

        public static List<ITool> Tools { get; } = new List<ITool>();

        public static void RegisterTool(Type toolType)
        {
            var type = typeof(ITool);
            if (!type.IsAssignableFrom(toolType) || toolType.IsAbstract)
            {
                throw new Exception($"Type '{toolType.FullName}' is not a valid tool");
            }
            if (ServiceCollection.ContainsKey(toolType))
            {
                throw new Exception($"Service '{toolType.FullName}' already registered");
            }
            ToolCollection.Add(toolType);
        }

        public static void RegisterTool<TTool>() where TTool : class, ITool
        {
            RegisterTool(typeof(TTool));
        }
        #endregion

        #region Console-Related
        public static List<Type> ConsoleExtensionCollection { get; } = new List<Type>();

        public static List<IConsoleExtension> ConsoleExtensions { get; } = new List<IConsoleExtension>();

        public static void RegisterConsoleExtension(Type consoleExtType)
        {
            var type = typeof(IConsoleExtension);
            if (!type.IsAssignableFrom(consoleExtType) || consoleExtType.IsAbstract)
            {
                throw new Exception($"Type '{consoleExtType.FullName}' is not a valid tool");
            }
            if (ServiceCollection.ContainsKey(consoleExtType))
            {
                throw new Exception($"Service '{consoleExtType.FullName}' already registered");
            }
            ConsoleExtensionCollection.Add(consoleExtType);
        }

        public static void RegisterConsoleExtension<TConsoleExtension>() where TConsoleExtension : class, IConsoleExtension
        {
            RegisterConsoleExtension(typeof(TConsoleExtension));
        }

        public static async Task<string> RunConsoleCommand(ConsoleCommand command)
        {
            var ext = ConsoleExtensions.FirstOrDefault(x => x.Command.ToLower() == command.Command.ToLower());
            if (ext != null)
            {
                return await ext.RunCommand(command);
            }
            throw new Exception($"'{command.Command}' is not recognized as a command.");
        }
        #endregion

        #region Service-Related
        /// <summary>
        /// Contains all registered services.
        /// </summary>
        private static Dictionary<Type, IService?> ServiceCollection { get; } = new Dictionary<Type, IService?>();

        /// <summary>
        /// Gets services registered in service manager.
        /// </summary>
        public static List<IService> Services
        {
            get => ServiceCollection.Values.OfType<IService>().ToList();
        }

        /// <summary>
        /// Registers a service.
        /// </summary>
        /// <exception cref="Exception"></exception>
        public static void RegisterService(Type serviceType)
        {
            var type = typeof(IService);
            if (!type.IsAssignableFrom(serviceType) || serviceType.IsAbstract)
            {
                throw new Exception($"Type '{serviceType.FullName}' is not a valid service");
            }
            if (ServiceCollection.ContainsKey(serviceType))
            {
                throw new Exception($"Service '{serviceType.FullName}' already registered");
            }
            ServiceCollection.Add(serviceType, null);
        }

        /// <summary>
        /// Registers a service.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <exception cref="Exception"></exception>
        public static void RegisterService<TService>() where TService : class, IService
        {
            RegisterService(typeof(TService));
        }

        /// <summary>
        /// Registers an instance of a service.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <param name="service">Service instance.</param>
        /// <exception cref="Exception"></exception>
        public static void RegisterService<TService>(TService service) where TService : class, IService
        {
            var type = typeof(TService);
            if (ServiceCollection.ContainsKey(type))
            {
                throw new Exception($"Service '{type.FullName}' already registered");
            }
            ServiceCollection.Add(type, service);
        }

        /// <summary>
        /// Compares the order of service.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static int ServiceCompare(Type a, Type b)
        {
            var ctora = a.GetConstructors();
            if (ctora.Length != 1)
            {
                throw new Exception($"Service '{a.FullName}' must have exactly one constructor.");
            }
            var ctorb = b.GetConstructors();
            if (ctorb.Length != 1)
            {
                throw new Exception($"Service '{b.FullName}' must have exactly one constructor.");
            }

            var ca = ctora[0];
            var cb = ctorb[0];

            var pa = ca.GetParameters().Select(x => x.ParameterType).ToList();
            var pb = cb.GetParameters().Select(x => x.ParameterType).ToList();

            if (pa.Contains(b) && pb.Contains(a))
            {
                throw new Exception($"Circular reference between '{a.FullName}' and '{b.FullName}'");
            }
            if (pa.Contains(b))
            {
                return 1;
            }
            if (pb.Contains(a))
            {
                return -1;
            }
            return 0;
        }
        #endregion

    }
}
