﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Xml.Linq;

namespace Aura.Services.Active
{
    public class TagService : ServiceBase
    {
        const string DataPath = "data\\memory\\tags";
        const string TagSaveFile = "main.json";

        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = InternalServices.TagService,
            Name = "Tag Service",
            Author = "CodeAtelier",
            Description = "Processes tag updates for other services. This is a core service and should not be stopped.",
            Version = new Version(1, 1, 5),
        };

        private MemoryTagCollection TagRecords { get; } = new MemoryTagCollection();

        public List<MemoryTag> Tags { get => TagRecords.ToList(); }

        private Persona? Persona { get; set; }
        public string? ActiveSchedule { get; set; }

        public WeightedTagCollection ActiveTags
        {
            get
            {
                RemoveExpiredTags();
                var res = new WeightedTagCollection();
                foreach (var wt in TagRecords)
                {
                    var val = Math.Max(wt.Weight, res[wt.Tag]);
                    res[wt.Tag] = val;
                }
                return res;
            }
        }

        private bool TagsHadChanged { get; set; } = false;

        public override ServiceUserAction UserActions => ServiceUserAction.None;

        IServiceMessageBus ServiceMessageBus { get; }

        public MessageReceiverPriority Priority => MessageReceiverPriority.Critical;

        public TagService(IServiceMessageBus smb)
        {
            ServiceMessageBus = smb;
        }

        public override async Task Initialize()
        {
            await Task.Run(() =>
            {
                if (!Directory.Exists(DataPath))
                {
                    Directory.CreateDirectory(DataPath);
                }
                var path = Path.Combine(DataPath, TagSaveFile);
                TagRecords.Clear();
                if (File.Exists(path))
                {
                    try
                    {
                        var json = File.ReadAllText(path);
                        var dtos = JsonSerializer.Deserialize<List<MemoryTagDto>>(json);
                        if (dtos != null)
                        {
                            TagRecords.AddRange(dtos.Select(x => new MemoryTag(x)));
                        }
                    }
                    catch { }
                }
            });
        }

        public override async Task Serve()
        {
            UpdatePersonaScheduleTags();
            RemoveExpiredTags();
            if (TagsHadChanged)
            {
                await HandleTagNotification();
            }
            else
            {
                Thread.Sleep(16);
            }
        }

        /// <summary>
        /// Removes all tags originated from specified source.
        /// </summary>
        /// <param name="source">The id of the source of tag.</param>
        /// <returns>The number of tags removed.</returns>
        public int RemoveTagsBySource(string source)
        {
            var removedCount = TagRecords.RemoveTagsBySource(source);
            if (removedCount > 0)
            {
                TagsHadChanged = true;
            }
            return removedCount;
        }

        /// <summary>
        /// Removes all tags originated from specified source.
        /// </summary>
        /// <param name="source">The id of the source of tag.</param>
        /// <returns>The number of tags removed.</returns>
        public WeightedTagCollection GetTagsBySource(string source)
        {
            RemoveExpiredTags();
            var res = new WeightedTagCollection();
            foreach (var wt in TagRecords)
            {
                if (wt.SourceId == source)
                {
                    var val = Math.Max(wt.Weight, res[wt.Tag]);
                    res[wt.Tag] = val;
                }
            }
            return res;
        }

        /// <summary>
        /// Removes all expired tags.
        /// </summary>
        /// <returns>The number of tags removed.</returns>
        public int RemoveExpiredTags()
        {
            var removedCount = TagRecords.RemoveExpiredTags();
            if (removedCount > 0)
            {
                TagsHadChanged = true;
            }
            return removedCount;
        }

        /// <summary>
        /// Removes all tags matching specified tag. Optionally, when source is specified, only remove tags originated from that source.
        /// </summary>
        /// <param name="tag">The tag to remove.</param>
        /// <param name="source">The id of the source of tag.</param>
        /// <returns>The number of tags removed.</returns>
        public int RemoveAll(string tag, string? source = null)
        {
            tag = tag.Trim().ToLower();
            var removedCount = TagRecords.RemoveAll(x => x.Tag == tag && (source == null || x.SourceId == source));
            if (removedCount > 0)
            {
                TagsHadChanged = true;
            }
            return removedCount;
        }

        public void ClearTags(bool procChange = false)
        {
            TagRecords.Clear();
            if (procChange)
                TagsHadChanged = true;
        }

        /// <summary>
        /// Adds a new tag.
        /// </summary>
        /// <param name="source">The source id of the tag.</param>
        /// <param name="tag">The tag to add.</param>
        /// <param name="expireAt">Expire time of the tag.</param>
        public void Add(string source, string tag, DateTime? expireAt = null, double weight = 1)
        {
            var tr = !string.IsNullOrWhiteSpace(source) ? new MemoryTag(tag, source, expireAt, weight) : expireAt.HasValue ? new MemoryTag(tag, expireAt.Value, weight) : new MemoryTag(tag, weight);
            TagRecords.Add(tr);
            TagsHadChanged = true;
            RemoveExpiredTags();
        }

        /// <summary>
        /// Adds a new tag.
        /// </summary>
        /// <param name="tag">The tag to add.</param>
        /// <param name="expireAt">Expire time of the tag.</param>
        public void Add(string tag, DateTime? expireAt = null, double weight = 1)
        {
            var tr = expireAt.HasValue ? new MemoryTag(tag, expireAt.Value, weight) : new MemoryTag(tag, weight);
            TagRecords.Add(tr);
            TagsHadChanged = true;
            RemoveExpiredTags();
        }

        /// <summary>
        /// Adds a new tag that expires after n seconds.
        /// </summary>
        /// <param name="tag">The tag to add.</param>
        /// <param name="expireAfterSeconds">How many seconds should passed before the tag removed.</param>
        public void Add(string tag, double expireAfterSeconds, double weight = 1)
        {
            var expireAt = DateTime.Now.AddSeconds(expireAfterSeconds);
            TagsHadChanged = true;
            Add(tag, expireAt, weight);
        }

        public override async Task OnStart()
        {
            await Task.Run(() =>
            {
            });
        }

        public override async Task OnShutdown()
        {
            await Task.Run(() =>
            {
            });
        }

        private void UpdatePersonaScheduleTags()
        {
            if (Persona != null)
            {
                var activeSchedules = Persona.Schedule.Where(x => x.DateTimeRange.ActiveNow).ToList();
                var actSchedule = string.Join(",", activeSchedules.Select(x => x.Name));
                if (actSchedule == ActiveSchedule) return;
                RemoveTagsBySource("avatarschedule");
                foreach (var schedule in activeSchedules)
                {
                    foreach (var tag in schedule.Tags)
                    {
                        Add("avatarschedule", tag.Tag, null, tag.Weight);
                    }
                }
                ActiveSchedule = actSchedule;
                TagsHadChanged = true;
            }
            else
            {
                ActiveSchedule = null;
            }
        }

        private async Task HandleTagNotification()
        {
            try
            {
                var msg = new Message(InternalServices.TagService, InternalEvents.PersonaTagsUpdated);
                await ServiceMessageBus.SendMessageAsync(msg);
                TagsHadChanged = false;
            }
            catch
            {

            }
        }

        public void LoadPersona(Persona? persona)
        {
            ClearTags();
            ActiveSchedule = null;
            Persona = persona;
            if (Persona != null)
            {
                // NYI: should load memory here
            }
            UpdatePersonaScheduleTags();
            TagsHadChanged = true;
        }
    }
}
