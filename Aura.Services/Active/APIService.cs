﻿using Aura.API;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;

namespace Aura.Services.Active
{
    public class APIService : ServiceBase
    {
        public override ExtensionInfo Info => new ExtensionInfo()
        {
            Id = InternalServices.APIService,
            Name = "API Service",
            Author = "CodeAtelier",
            Description = "Reveals API to other application. If stopped, other applications won't be able to connect to Aura API.",
            Version = new Version(1, 0, 0),
        };

        public override ServiceUserAction UserActions => ServiceUserAction.All;

        public TcpListener Listener { get; } = new TcpListener(IPAddress.Loopback, 2769);

        public AuraLogger Logger { get; }

        public IServiceMessageBus Bus { get; }

        private int MessageId { get; set; }

        public APIService(IServiceMessageBus bus)
        {
            Logger = new AuraLogger(Path.Combine(AuraLogger.DefaultLogPath, DateTime.Now.ToString("yyyyMMdd") + ".aura.apiservice.log"));
            Bus = bus;
            if (Directory.Exists(AuraLogger.DefaultLogPath))
            {
                try
                {
                    var files = Directory.EnumerateFiles(AuraLogger.DefaultLogPath, "*.aura.apiservice.log");
                    foreach (var f in files)
                    {
                        try
                        {
                            var fi = new FileInfo(f);
                            var age = DateTime.Now - fi.LastWriteTime;
                            if (age.TotalDays > 7)
                                File.Delete(f);
                        }
                        catch { }
                    }
                }
                catch { }
            }
        }

        public override async Task Initialize()
        {
            await Task.Run(() => { });
        }

        public override async Task OnStart()
        {
            await Task.Run(() =>
            {
                Listener.Start();
            });
        }

        public override async Task OnShutdown()
        {
            await Task.Run(() =>
            {
                Listener.Stop();
            });
        }

        public override async Task Serve()
        {
            if (!Listener.Pending())
            {
                Thread.Sleep(100);
                return;
            }
            try
            {
                var client = await Listener.AcceptTcpClientAsync();
                ProcessRequest(client);
            }
            catch { }
        }

        private void SetBody(APIMessage message, string provider, IMessageWithBody data)
        {
            if (data.GetBody() is byte[] raw)
            {
                message.Contents = new APIMessageContent[] { new APIMessageContent(provider, data.Type, raw) };
            }
            else if (data.GetBody() is string str)
            {
                var rawString = Encoding.UTF8.GetBytes(str);
                message.Contents = new APIMessageContent[] { new APIMessageContent(provider, data.Type, rawString) };
            }
            else
            {
                var json = JsonSerializer.Serialize(data);
                var rawJson = Encoding.UTF8.GetBytes(json);
                message.Contents = new APIMessageContent[] { new APIMessageContent(provider, data.Type, rawJson) };
            }
        }

        private void AppendBody(APIMessage message, string provider, IMessageWithBody data)
        {
            if (data.GetBody() is byte[] raw)
            {
                message.Contents = message.Contents.Concat(new APIMessageContent[] { new APIMessageContent(provider, data.Type, raw) }).ToArray();
            }
            else if (data.GetBody() is string str)
            {
                var rawString = Encoding.UTF8.GetBytes(str);
                message.Contents = message.Contents.Concat(new APIMessageContent[] { new APIMessageContent(provider, data.Type, rawString) }).ToArray();
            }
            else
            {
                var json = JsonSerializer.Serialize(data);
                var rawJson = Encoding.UTF8.GetBytes(json);
                message.Contents = message.Contents.Concat(new APIMessageContent[] { new APIMessageContent(provider, data.Type, rawJson) }).ToArray();
            }
        }

        private async void ProcessRequest(TcpClient client)
        {
            var msgId = ++MessageId;
            try
            {
                var reply = new APIMessage();
                reply.Headers["api-version"] = Info.Version.ToString();
                Logger.Debug($"#{msgId} >> Accepted connection from [" + client.Client.RemoteEndPoint?.ToString() + "]");
                // parse message
                var msg = await APIMessage.FromClient(client);
                if (!msg.Headers.ContainsKey("event"))
                {
                    throw new InvalidDataException("No Event");
                }
                if (!msg.Headers.ContainsKey("sender"))
                {
                    throw new InvalidDataException("No Sender");
                }
                if (msg.Contents.Length > 1)
                {
                    throw new InvalidDataException("Multiple Request Content");
                }
                var busMsg = new Message<byte[]>(msg.Headers["sender"], "api/" + msg.Headers["event"], msg.Contents.FirstOrDefault()?.Content);
                foreach (var header in msg.Headers)
                {
                    busMsg.Headers[header.Key] = header.Value;
                }
                busMsg.Type = msg.Headers["content-type"];
                var replies = await Bus.SendMessageAsync(busMsg);
                if (replies.Count() > 0)
                {
                    foreach (var rep in replies)
                    {
                        if (rep is IMessageWithBody iwb)
                        {
                            AppendBody(reply, rep.SenderId, iwb);
                        }
                    }
                }
                else if (replies.Count() == 1)
                {
                    if (replies.First() is IMessageWithBody iwb)
                    {
                        SetBody(reply, replies.First().SenderId, iwb);
                    }
                }
                reply.Headers["status"] = "200";
                reply.Headers["status-message"] = "OK";
                await reply.ToClient(client);
                Logger.Information($"#{msgId} >> Response Sent");
            }
            catch (InvalidDataException ide)
            {
                var reply = new APIMessage();
                reply.Headers["api-version"] = Info.Version.ToString();
                reply.Headers["status"] = "400";
                reply.Headers["status-message"] = "Bad Request";
                reply.Contents = new APIMessageContent[]
                {
                    new APIMessageContent("aura", "text/plain", Encoding.UTF8.GetBytes(ide.Message))
                };
                await reply.ToClient(client);
                Logger.Error($"#{msgId} >> Bad Request", ide);
            }
            catch (Exception ex)
            {
                var reply = new APIMessage();
                reply.Headers["api-version"] = Info.Version.ToString();
                reply.Headers["status"] = "500";
                reply.Headers["status-message"] = "Internal Error";
                reply.Contents = new APIMessageContent[]
                {
                    new APIMessageContent("aura", "text/plain", Encoding.UTF8.GetBytes(ex.Message))
                };
                await reply.ToClient(client);
                Logger.Error($"#{msgId} >> Internal Error", ex);
            }
            finally
            {
                client.Close();
            }
        }
    }
}
