﻿using Aura.Services.Active;
using Aura.Services.Passive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public static class ServiceUtil
    {
        public static bool SafeMode { get; set; } = false;
        public static bool IsSystemService(IService service)
        {
            return service is ConfigurationService || service is TagService || service is ScriptingService;
        }
    }
}
