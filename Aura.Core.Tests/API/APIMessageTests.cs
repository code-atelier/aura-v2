﻿using Aura.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Core.Tests.API
{
    [TestClass]
    public class APIMessageTests
    {
        private APIMessage CreateMessage()
        {
            var msg = new APIMessage();
            msg.Headers["sender"] = "meow";
            msg.Headers["event"] = "meowing";
            msg.Contents = new APIMessageContent[]
            {
                new APIMessageContent("nyan", "text/plain", Encoding.UTF8.GetBytes("BAKALOLI")),
                new APIMessageContent("nyanko", "text/plain", Encoding.UTF8.GetBytes("BAKANEKO")),
            };
            return msg;
        }

        [TestMethod]
        public void WriteMessage_WithAnyMessage_SuccessNoException()
        {
            using (var ms = new MemoryStream())
            {
                var msg = CreateMessage();
                msg.WriteMessage(ms);
                ms.Position = 0;
                Assert.IsTrue(ms.Length > 0);
            }
        }

        [TestMethod]
        public void ReadMessage_WithAnyMessage_ReturnTheExactSameMessage()
        {
            using (var ms = new MemoryStream())
            {
                var msg = CreateMessage();
                msg.WriteMessage(ms);
                ms.Position = 0;
                Assert.IsTrue(ms.Length > 0);

                var readMsg = APIMessage.ReadMessage(ms);
                Assert.IsNotNull(readMsg);
                Assert.AreEqual(msg.ProtocolVersion, readMsg.ProtocolVersion);
                Assert.AreEqual(msg.Contents.Length, readMsg.Contents.Length);
                foreach(var header in msg.Headers)
                {
                    Assert.IsTrue(readMsg.Headers.ContainsKey(header.Key));
                    Assert.AreEqual(header.Value, readMsg.Headers[header.Key]);
                }
            }
        }
    }
}
