﻿using Aura.Script;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Core.Tests.Script
{
    [TestClass]
    public class AuraScriptParserTest
    {
        [TestMethod]
        public void Parse_SimpleScript()
        {
            var lexer = new AuraScriptLexer();
            var grammarDefinition = File.ReadAllText("data/AuraScriptGrammar.txt");
            var script = File.ReadAllText("data/AuraScriptEvent.ase");
            var grammar = new Grammar();
            grammar.LoadGrammarDefinition(grammarDefinition);
            var tokens = lexer.Tokenize(script);
            var invalid = tokens.Where(x => !x.IsValid).ToList();
            var tokenStream = TokenStream.Create(tokens.Where(x => x.Type != AuraScriptTokenType.Comment));

            var parser = new Parser<Token<AuraScriptTokenType>>();
            var result = parser.Parse(tokenStream, grammar);
            foreach (var err in result.Entries.Where(x => x.Type == ParseResultEntryType.Error))
            {
                Console.WriteLine(err.Message);
            }

            var compiled = parser.GetCompiled();

            Assert.IsTrue(result.IsSuccess, $"Parsing failed with {result.Errors} errors");
        }

        [TestMethod]
        public void Parse_SimpleScriptAndOutputExpression()
        {
            var parser = new AuraScriptParser();
            var script = File.ReadAllText("data/AuraScriptEvent.ase");
            var compiled = parser.Parse(script);
            Assert.IsNotNull(compiled);
        }
    }
}
