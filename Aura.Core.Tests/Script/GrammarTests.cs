﻿using Aura.Script;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Core.Tests.Script
{
    [TestClass]
    public class GrammarTests
    {
        [TestMethod]
        public void LoadGrammarDefinition_AuraScript()
        {
            var grammar = new Grammar();
            var grammarDefinitions = File.ReadAllText("data/AuraScriptGrammar.txt");
            var result = grammar.LoadGrammarDefinition<AuraScriptTokenType>(grammarDefinitions);
            var baked = grammar.BakeRuleset();
            Assert.IsTrue(result.IsSuccess, "Grammar definition loading failed: " + result.Errors + " errors found");
        }
    }
}
