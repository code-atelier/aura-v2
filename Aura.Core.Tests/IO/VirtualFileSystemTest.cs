using Aura.IO;
using Aura.Script;
using System.Text;

namespace Aura.Core.Tests.IO
{
    [TestClass]
    public class VirtualFileSystemTest
    {
        const string TestText = "TEST";
        const string TempFileName = "test.txt";
        const string TempFileNameWithTrailingSpaces = "  test.txt ";
        const string TempArchiveName = "test.zip";
        readonly byte[] TestData = new byte[] { 1, 2, 3, 4, 5 };
        readonly byte[] TestData2 = new byte[] { 6, 7, 8, 9, 10 };


        private VirtualFileSystem CreateVirtualFileSystem()
        {
            return new VirtualFileSystem();
        }

        [TestMethod]
        public void Tokenize_SimpleScript()
        {
            var lexer = new AuraScriptLexer();
            var tokens = lexer.Tokenize(@"
                on !tagchanged;
                on !regionclick ""sleeping:button_1"";
                when @sleep >= 1;
                do
                nothing
            ").ToList();
        }

        [TestMethod]
        public void Tokenize_ComplexScript()
        {
            var lexer = new AuraScriptLexer();
            var tokens = lexer.Tokenize(@"
                on !tagchanged;
                on !regionclick ""sleeping:button_1"";
                when @sleep >= 1;
                do
                if @sleep <= 0 then
                    set @button_1 = 1 for 3s and 3m;
                end
            ").ToList();
        }

        [TestMethod]
        public void Add_WithValidStream_ShouldRetainData()
        {
            var vfs = CreateVirtualFileSystem();
            vfs.Add(TempFileName, new MemoryStream(TestData));
            var file = vfs[TempFileName];
            file.Seek(0, SeekOrigin.Begin);
            var buffer = new byte[file.Length];
            file.Read(buffer);

            Assert.AreEqual(1, vfs.Count);
            Assert.AreEqual(TestData.Length, buffer.Length);
            for(var i = 0; i < buffer.Length; i++)
            {
                Assert.AreEqual(TestData[i], buffer[i], $"Data at position {i} is different");
            }
        }

        [TestMethod]
        public void Add_WithSamePath_ShouldUpdateData()
        {
            var vfs = CreateVirtualFileSystem();
            vfs.Add(TempFileName, new MemoryStream(TestData));
            vfs.Add(TempFileName, new MemoryStream(TestData2));
            var file = vfs[TempFileName];
            file.Seek(0, SeekOrigin.Begin);
            var buffer = new byte[file.Length];
            file.Read(buffer);

            Assert.AreEqual(1, vfs.Count);
            Assert.AreEqual(TestData2.Length, buffer.Length);
            for (var i = 0; i < buffer.Length; i++)
            {
                Assert.AreEqual(TestData2[i], buffer[i], $"Data at position {i} is different");
            }
        }

        [TestMethod]
        public void Add_WithTrailingSpacesButSamePath_ShouldUpdateData()
        {
            var vfs = CreateVirtualFileSystem();
            vfs.Add(TempFileName, new MemoryStream(TestData));
            vfs.Add(TempFileNameWithTrailingSpaces, new MemoryStream(TestData2));
            var file = vfs[TempFileName];
            file.Seek(0, SeekOrigin.Begin);
            var buffer = new byte[file.Length];
            file.Read(buffer);

            Assert.AreEqual(1, vfs.Count);
            Assert.AreEqual(TestData2.Length, buffer.Length);
            for (var i = 0; i < buffer.Length; i++)
            {
                Assert.AreEqual(TestData2[i], buffer[i], $"Data at position {i} is different");
            }
        }

        [TestMethod]
        public void WriteAllText_WithValidString_ShouldWriteCorrectly()
        {
            var vfs = CreateVirtualFileSystem();
            vfs.WriteAllText(TempFileName, TestText);

            var ms = vfs[TempFileName];
            ms.Seek(0, SeekOrigin.Begin);
            var buf = new byte[ms.Length];
            ms.Read(buf);

            var text = Encoding.UTF8.GetString(buf);
            Assert.AreEqual(TestText, text);
        }

        [TestMethod]
        public void ReadAllText_WithValidString_ShouldReadCorrectly()
        {
            var vfs = CreateVirtualFileSystem();
            vfs.WriteAllText(TempFileName, TestText);

            var text = vfs.ReadAllText(TempFileName);
            Assert.AreEqual(TestText, text);
        }

        [TestMethod]
        public void SaveAsArchive_WithValidData_ShouldWriteCorrectly()
        {
            var vfs = CreateVirtualFileSystem();
            vfs.WriteAllText(TempFileName, TestText);
            vfs.SaveAsArchive(TempArchiveName);

            Assert.IsTrue(File.Exists(TempArchiveName));
        }

        [TestMethod]
        public void ReadFromArchive_WithValidData_ShouldReadCorrectly()
        {
            var vfs = CreateVirtualFileSystem();
            vfs.WriteAllText(TempFileName, TestText);
            vfs.SaveAsArchive(TempArchiveName);
            vfs.LoadFromArchive(TempArchiveName);
            var text = vfs.ReadAllText(TempFileName);

            Assert.IsTrue(File.Exists(TempArchiveName));
            Assert.AreEqual(TestText, text);
        }
    }
}