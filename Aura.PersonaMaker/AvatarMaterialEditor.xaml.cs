﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarMaterialEditor.xaml
    /// </summary>
    public partial class AvatarMaterialEditor : Window
    {
        Material? Material { get; set; }
        Avatar? Avatar { get; set; }
        SpriteManager? Sprites { get; set; }
        SolidColorBrush BorderBackgroundColor = new SolidColorBrush(Color.FromRgb(0x66, 0x66, 0x66));
        SolidColorBrush BorderColor = new SolidColorBrush(Color.FromRgb(0x44, 0x44, 0x44));
        SolidColorBrush BorderSelectedColor = new SolidColorBrush(Color.FromRgb(0x44, 0x88, 0xcc));

        List<Guid> SpriteIds { get; } = new List<Guid>();

        public AvatarMaterialEditor()
        {
            InitializeComponent();
        }

        public void LoadMaterial(Material material, Avatar avatar, SpriteManager sprites)
        {
            Material = material;
            Avatar = avatar;
            Sprites = sprites;
            txId.Text = material.Id.ToString("n");
            txRef.Text = material.Reference ?? "";
            txWidth.Text = material.Width.ToString();
            txHeight.Text = material.Height.ToString();
            txFPS.Text = material.FramesPerSecond.ToString();
            cbLoop.SelectedIndex = (int)material.LoopKind;
            SpriteIds.Clear();
            SpriteIds.AddRange(Material.Frames);
            LoadFrames();
        }

        private void LoadFrames(int? selIndex = null)
        {
            var lastSelected = selIndex ?? spFrames.Children.OfType<Border>().Select(x => x.Tag as FrameInfo).FirstOrDefault(x => x != null && x.Selected == true)?.Index;
            spFrames.Children.Clear();
            if (Sprites == null)
            {
                return;
            }
            int index = 0;
            foreach (var frame in SpriteIds)
            {
                var border = new Border();
                border.BorderBrush = BorderColor;
                border.BorderThickness = new Thickness(2);
                border.Margin = new Thickness(4, 0, 4, 0);
                border.Background = BorderBackgroundColor;
                border.MouseLeftButtonDown += Border_MouseLeftButtonDown;
                var image = new Image();
                image.Width = 100;
                image.Height = 100;
                image.Stretch = Stretch.Uniform;
                image.Source = Sprites.GetSpriteAsImageSource(frame);
                border.Child = image;
                var frameInfo = new FrameInfo()
                {
                    Selected = false,
                    Index = index++,
                    ImageSource = image.Source,
                    SpriteId = frame,
                };
                border.Tag = frameInfo;
                spFrames.Children.Add(border);
            }
            if (lastSelected != null)
            {
                if (lastSelected >= spFrames.Children.Count)
                {
                    lastSelected = spFrames.Children.Count - 1;
                }
                if (spFrames.Children.Count > 0)
                {
                    if (spFrames.Children[lastSelected.Value] is Border border && border.Tag is FrameInfo fi)
                    {
                        fi.Selected = true;
                    }
                }
            }
            UpdateSelectionUI();
        }

        private void Border_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is Border border && border.Tag is FrameInfo fi)
            {
                foreach (var b in spFrames.Children.OfType<Border>())
                {
                    if (b.Tag is FrameInfo f)
                    {
                        f.Selected = false;
                    }
                }
                fi.Selected = true;
                UpdateSelectionUI();
                e.Handled = true;
            }
        }

        private void UpdateSelectionUI()
        {
            var lastSelected = spFrames.Children.OfType<Border>().Select(x => x.Tag as FrameInfo).FirstOrDefault(x => x != null && x.Selected == true)?.Index;
            if (lastSelected == null)
            {
                imgPreview.Source = null;
            }
            var index = 0;
            var selIndex = -1;
            Border? selborder = null;
            foreach (var border in spFrames.Children.OfType<Border>())
            {
                index++;
                if (border.Tag is FrameInfo fi)
                {
                    border.BorderBrush = fi.Selected ? BorderSelectedColor : BorderColor;
                    if (fi.Selected)
                    {
                        selborder = border;
                        selIndex = index;
                        imgPreview.Source = fi.ImageSource;
                        var sprite = Avatar?.Sprites[fi.SpriteId];
                        if (sprite != null)
                        {
                            grPreview.Width = imgPreview.Width = sprite.Width;
                            grPreview.Height = imgPreview.Height = sprite.Height;
                        }
                    }
                }
            }
            lbFrameCount.Content = "Frame: " + (selIndex < 0 ? "?" : selIndex.ToString()) + " of " + SpriteIds.Count;
            btnDupeMat.IsEnabled =
            btnDelete.IsEnabled = selIndex > 0;

            btnMatLeft.IsEnabled = selIndex > 0;
            btnMatRight.IsEnabled = (selIndex - 1) < SpriteIds.Count - 1 && selIndex > 0;
            if (selborder != null)
            {
                selborder.BringIntoView();
            }
        }

        private void txRef_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            txRef.Text = Util.SanitizeId(txRef.Text);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var insertAfter = sender is Button btn && btn.Tag is string tg ? tg == "after" : false;
            if (Avatar == null) return;
            var lastSelected = spFrames.Children.OfType<Border>().Select(x => x.Tag as FrameInfo).FirstOrDefault(x => x != null && x.Selected == true)?.Index;

            var w = new AvatarSpriteManager();
            w.SelectionMode = true;
            w.LoadAvatar(Avatar);
            if (w.ShowDialog() == true)
            {
                var sprite = w.SelectionResult;
                if (sprite != null)
                {
                    if (SpriteIds.Count == 0)
                    {
                        grPreview.Width = imgPreview.Width = sprite.Width;
                        grPreview.Height = imgPreview.Height = sprite.Height;
                    }
                    int? selIndex = null;
                    if (lastSelected == null)
                    {
                        if (insertAfter)
                        {
                            selIndex = SpriteIds.Count;
                            SpriteIds.Add(sprite.Id);
                        }
                        else
                        {
                            selIndex = 0;
                            SpriteIds.Insert(0, sprite.Id);
                        }
                    }
                    else
                    {
                        selIndex = lastSelected.Value + (insertAfter ? 1 : 0);
                        SpriteIds.Insert(selIndex.Value, sprite.Id);
                    }
                    if (txRef.Text == "newmaterial")
                    {
                        var sprRef = sprite.Reference ?? txRef.Text;
                        var uIdx = sprRef.IndexOf('_');
                        if (uIdx == -1) uIdx = sprRef.IndexOf(' ');
                        if (uIdx >= 0)
                        {
                            sprRef = sprRef.Substring(0, uIdx) + ":" + sprRef.Substring(uIdx + 1);
                        }
                        txRef.Text = Util.SanitizeId(sprRef);
                    }
                    LoadFrames(selIndex);
                }
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var selected = spFrames.Children.OfType<Border>().Select(x => x.Tag as FrameInfo).FirstOrDefault(x => x != null && x.Selected == true)?.Index;
            if (selected == null) return;
            if (selected < SpriteIds.Count)
            {
                SpriteIds.RemoveAt(selected.Value);
                if (SpriteIds.Count == 0)
                {
                    grPreview.Width = imgPreview.Width = 0;
                    grPreview.Height = imgPreview.Height = 0;
                }
                LoadFrames(SpriteIds.Count > 0 ? Math.Min(SpriteIds.Count - 1, Math.Max(0, selected.Value - 1)) : null);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Avatar == null || Material == null) return;

            Guid id;
            int matW, matH, fps;
            if (!Guid.TryParse(txId.Text, out id))
            {
                MessageBox.Show("Invalid Id!\r\nId must be a valid guid.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(txRef.Text))
            {
                MessageBox.Show("Reference cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (SpriteIds.Count == 0)
            {
                MessageBox.Show("Material contains no frame!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txWidth.Text, out matW))
            {
                MessageBox.Show("Invalid material width!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txHeight.Text, out matH))
            {
                MessageBox.Show("Invalid material height!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txFPS.Text, out fps) || fps <= 0)
            {
                MessageBox.Show("Invalid material frame per second!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Avatar.Materials.Any(x => x.Id == id && x != Material))
            {
                MessageBox.Show("Material with the same id already existed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Avatar.Materials.Any(x => x.Reference == txRef.Text && x != Material))
            {
                MessageBox.Show("Material with the same reference already existed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Material.Id = id;
            Material.Reference = txRef.Text;
            Material.Width = matW;
            Material.Height = matH;
            Material.FramesPerSecond = fps;
            if (cbLoop.SelectedIndex >= 0)
            {
                Material.LoopKind = (LoopKind)cbLoop.SelectedIndex;
            }
            Material.Frames.Clear();
            Material.Frames.AddRange(SpriteIds);

            DialogResult = true;
            Close();
        }

        private void btnDupeMat_Click(object sender, RoutedEventArgs e)
        {
            var selected = spFrames.Children.OfType<Border>().Select(x => x.Tag as FrameInfo).FirstOrDefault(x => x != null && x.Selected == true)?.Index;
            if (selected == null) return;
            if (selected < SpriteIds.Count)
            {
                var dupe = SpriteIds[selected.Value];
                SpriteIds.Insert(selected.Value + 1, dupe);
                LoadFrames(selected.Value + 1);
            }
        }

        private void btnMatLeft_Click(object sender, RoutedEventArgs e)
        {
            var selected = spFrames.Children.OfType<Border>().Select(x => x.Tag as FrameInfo).FirstOrDefault(x => x != null && x.Selected == true)?.Index;
            if (selected == null) return;
            if (selected < SpriteIds.Count && selected > 0)
            {
                var mat = SpriteIds[selected.Value];
                SpriteIds.RemoveAt(selected.Value);
                SpriteIds.Insert(selected.Value - 1, mat);
                LoadFrames(selected.Value - 1);
            }
        }

        private void btnMatRight_Click(object sender, RoutedEventArgs e)
        {
            var selected = spFrames.Children.OfType<Border>().Select(x => x.Tag as FrameInfo).FirstOrDefault(x => x != null && x.Selected == true)?.Index;
            if (selected == null) return;
            if (selected < SpriteIds.Count && selected < SpriteIds.Count - 1 && selected >= 0)
            {
                var mat = SpriteIds[selected.Value];
                SpriteIds.RemoveAt(selected.Value);
                SpriteIds.Insert(selected.Value + 1, mat);
                LoadFrames(selected.Value + 1);
            }
        }
    }

    class FrameInfo
    {
        public bool Selected { get; set; }

        public int Index { get; set; }

        public ImageSource? ImageSource { get; set; }

        public Guid SpriteId { get; set; }
    }
}
