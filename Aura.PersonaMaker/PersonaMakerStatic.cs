﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.PersonaMaker
{
    public static class PersonaMakerStatic
    {
        public static Version Version { get; } = new Version(2, 1, 36);
    }
}
