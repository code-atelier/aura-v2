﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for TagWeightEditor.xaml
    /// </summary>
    public partial class TagWeightEditor : Window
    {
        public TagWeightEditor()
        {
            InitializeComponent();

            var typ = typeof(InternalTags.Emotion);
            var values = typ.GetFields().Where(x => x.IsStatic && x.FieldType == typeof(string) && x.IsLiteral).ToList();
            foreach (var v in values)
            {
                var val = v.GetValue(null) as string;
                tagName.Items.Add(val);
            }

            typ = typeof(InternalTags.CompositeEmotion);
            values = typ.GetFields().Where(x => x.IsStatic && x.FieldType == typeof(string) && x.IsLiteral).ToList();
            foreach (var v in values)
            {
                var val = v.GetValue(null) as string;
                tagName.Items.Add(val);
            }

            typ = typeof(InternalTags.Urges);
            values = typ.GetFields().Where(x => x.IsStatic && x.FieldType == typeof(string) && x.IsLiteral).ToList();
            foreach (var v in values)
            {
                var val = v.GetValue(null) as string;
                tagName.Items.Add(val);
            }
        }

        WeightedTag? EditedTag { get; set; }
        IEnumerable<WeightedTag>? TagList { get; set; }

        public void Load(WeightedTag tag, IEnumerable<WeightedTag> list)
        {
            EditedTag = tag;
            TagList = list;
            tagName.Text = tag.Tag;
            tagWeight.Text = tag.Weight.ToString();
        }

        private void tagName_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            tagName.Text = Util.SanitizeId(tagName.Text);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (EditedTag == null) { return; }
            if (string.IsNullOrWhiteSpace(tagName.Text))
            {
                MessageBox.Show("Invalid tag name!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (TagList != null && TagList.Any(x=> x.Tag == tagName.Text && x != EditedTag))
            {
                MessageBox.Show("Tag with the same name already existed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double weight;
            if (!double.TryParse(tagWeight.Text, out weight))
            {
                MessageBox.Show("Invalid tag weight!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            EditedTag.Tag = tagName.Text;
            EditedTag.Weight = weight;
            DialogResult = true;
            Close();
        }
    }
}
