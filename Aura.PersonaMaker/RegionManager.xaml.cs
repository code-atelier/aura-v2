﻿using Aura.Models;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for RegionManager.xaml
    /// </summary>
    public partial class RegionManager : Window
    {
        private Avatar? Avatar { get; set; }
        private SpriteManager SpriteManager { get; } = new SpriteManager();
        private AvatarCanvas AvatarCanvas { get; }
        private Ellipse PointEllipse { get; }

        const int PointEllipseSize = 6;

        public RegionManager()
        {
            InitializeComponent();
            AvatarCanvas = new AvatarCanvas();
            AvatarCanvas.IsRegionVisible = true;
            grPreview.Children.Add(AvatarCanvas);
            PointEllipse = new Ellipse();
            PointEllipse.Width = PointEllipse.Height = PointEllipseSize;
            PointEllipse.Fill = new SolidColorBrush(Color.FromArgb(255, 255, 0, 255));
            PointEllipse.Visibility = Visibility.Collapsed;
            PointEllipse.VerticalAlignment = VerticalAlignment.Top;
            PointEllipse.HorizontalAlignment = HorizontalAlignment.Left;
            grPreview.Children.Add(PointEllipse);
        }

        public void LoadAvatar(Avatar? avatar)
        {
            Avatar = avatar;
            SpriteManager.Clear();
            if (Avatar != null)
            {
                foreach (var spr in Avatar.Sprites)
                {
                    SpriteManager.Add(spr);
                }
                grPreview.Width = AvatarCanvas.Width = Avatar.Display.CanvasWidth;
                grPreview.Height = AvatarCanvas.Height = Avatar.Display.CanvasHeight;
            }
            else
            {
                grPreview.Width = AvatarCanvas.Width = 0;
                grPreview.Height = AvatarCanvas.Height = 0;
            }
            AvatarCanvas.LoadAvatar(Avatar, SpriteManager);
            LoadRegions();
        }

        private void LoadRegions(Region? select = null)
        {
            lvRegions.Items.Clear();
            if (Avatar == null) return;
            foreach (var region in Avatar.Regions)
            {
                lvRegions.Items.Add(region);
            }
            lvRegions.SelectedItem = select;
            RefreshPreview();
        }

        private void lvRegions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshPreview();
        }

        private void RefreshPreview()
        {
            PointEllipse.Visibility = Visibility.Collapsed;
            if (Avatar != null && lvRegions.SelectedItem is Region region)
            {
                AvatarCanvas.ReloadRegions(null, x => x.Id == region.Id);
                var point = region.Shapes.FirstOrDefault(x => x.Type == ShapeType.Point);
                if (point != null)
                {
                    PointEllipse.Visibility = Visibility.Visible;
                    PointEllipse.Margin = new Thickness(point.X - PointEllipseSize / 2, point.Y - PointEllipseSize / 2, 0, 0);
                }
            }
            else
            {
                AvatarCanvas.ReloadRegions(null, x => false);
            }
        }

        private void CommandNew_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null)
            {
                var r = new Region()
                {
                    Reference = "newregion"
                };
                var w = new RegionEditor();
                w.LoadRegion(Avatar, r);
                if (w.ShowDialog() == true)
                {
                    Avatar.Regions.Add(r);
                    LoadRegions(r);
                }
            }
        }

        private void CommandModify_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Avatar != null && lvRegions.SelectedItem is Region;
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {

            if (Avatar != null && lvRegions.SelectedItem is Region region)
            {
                if (MessageBox.Show("Are you sure to delete this region?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    lvRegions.Items.Remove(region);
                    RefreshPreview();
                }
            }
        }

        private void PerformEdit()
        {
            if (Avatar != null && lvRegions.SelectedItem is Region region)
            {
                var w = new RegionEditor();
                w.LoadRegion(Avatar, region);
                if (w.ShowDialog() == true)
                {
                    LoadRegions(region);
                }
            }
        }

        private void lvRegions_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                PerformEdit();
            }
        }

        private void CommandMoveUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvRegions.SelectedItem is Region && lvRegions.SelectedIndex > 0;
        }

        private void CommandMoveUp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && lvRegions.SelectedItem is Region region)
            {
                var lastIndex = Avatar.Regions.IndexOf(region);
                if (lastIndex > 0)
                {
                    Avatar.Regions.Remove(region);
                    Avatar.Regions.Insert(lastIndex - 1, region);
                    LoadRegions(region);
                }
            }
        }

        private void CommandMoveDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvRegions.SelectedItem is Region && lvRegions.SelectedIndex < lvRegions.Items.Count - 1;
        }

        private void CommandMoveDown_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && lvRegions.SelectedItem is Region region)
            {
                var lastIndex = Avatar.Regions.IndexOf(region);
                if (lastIndex < Avatar.Regions.Count - 1)
                {
                    Avatar.Regions.Remove(region);
                    Avatar.Regions.Insert(lastIndex + 1, region);
                    LoadRegions(region);
                }
            }
        }
    }
}
