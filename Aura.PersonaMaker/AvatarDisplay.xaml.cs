﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarDisplay.xaml
    /// </summary>
    public partial class AvatarDisplay : Window
    {
        public Avatar? Avatar { get; set; }

        public AvatarDisplay()
        {
            InitializeComponent();
        }

        public void LoadAvatar(Avatar avatar)
        {
            Avatar = avatar;
            canvasHeight.Text = avatar.Display.CanvasHeight.ToString();
            canvasWidth.Text = avatar.Display.CanvasWidth.ToString();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (Avatar == null) return;
            int w, h;
            if (!int.TryParse(canvasWidth.Text, out w) || w < 1)
            {
                MessageBox.Show("Invalid canvas width", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(canvasHeight.Text, out h) || h < 1)
            {
                MessageBox.Show("Invalid canvas height", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Avatar.Display.CanvasWidth = w;
            Avatar.Display.CanvasHeight = h;
            DialogResult = true;
            Close();
        }

        private void Window_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files?.Length == 1)
                {
                    var ext = System.IO.Path.GetExtension(files[0]).ToLower();
                    if (ext == ".png")
                    {
                        e.Effects = DragDropEffects.Link;
                        e.Handled = true;
                    }
                }
            }
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files?.Length == 1)
                {
                    var ext = System.IO.Path.GetExtension(files[0]).ToLower();
                    if (ext == ".png")
                    {
                        e.Effects = DragDropEffects.Link;
                        e.Handled = true;

                        var file = files[0];
                        try
                        {
                            var bmp = System.Drawing.Bitmap.FromFile(file);
                            canvasWidth.Text = bmp.Width.ToString();
                            canvasHeight.Text = bmp.Height.ToString();
                        }
                        catch { }
                    }
                }
            }
        }
    }
}
