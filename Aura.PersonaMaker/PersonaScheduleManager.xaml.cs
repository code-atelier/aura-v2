﻿using Aura.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for PersonaScheduleManager.xaml
    /// </summary>
    public partial class PersonaScheduleManager : Window
    {
        Persona? Persona { get; set; }

        ScheduleEntry? CurrentData { get; set; }

        public PersonaScheduleManager()
        {
            InitializeComponent();
            cbDateRangeKind.Items.Add(ScheduleDateRangeKind.AnyDate);
            cbDateRangeKind.Items.Add(ScheduleDateRangeKind.DateOfYear);
            cbDateRangeKind.Items.Add(ScheduleDateRangeKind.FixedDate);
            grData.IsEnabled = false;
            grList.IsEnabled = true;
            ClearForm();
        }

        public void LoadPersona(Persona persona)
        {
            Persona = persona;
            ReloadList();
        }

        void ReloadList()
        {
            lvData.Items.Clear();
            if (Persona == null) return;
            foreach (var item in Persona.Schedule)
            {
                lvData.Items.Add(item);
            }
            LoadValues();
        }

        private void ClearForm()
        {
            cbDateRangeKind.SelectedIndex = 0;
            txName.Clear();
            dtpStart.SelectedDate = null;
            dtpEnd.SelectedDate = null;
            cxUseTime.IsChecked = false;
            txStart.Text = "00:00:00.000";
            txEnd.Text = "23:59:59.999";
            txStart.IsEnabled = txEnd.IsEnabled = false;
            cxMonday.IsChecked =
            cxTuesday.IsChecked =
            cxWednesday.IsChecked =
            cxThursday.IsChecked =
            cxFriday.IsChecked =
            cxSaturday.IsChecked =
            cxSunday.IsChecked = true;
            txTags.Clear();
        }

        private void CommandNew_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClearForm();
            CurrentData = null;
            grData.IsEnabled = true;
            grList.IsEnabled = false;
            txName.Focus();
        }

        private void cbDateRangeKind_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dtpEnd.IsEnabled = dtpStart.IsEnabled = cbDateRangeKind.SelectedIndex > 0;
        }

        private void lvData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadValues();
        }

        private void LoadValues()
        {
            ClearForm();
            if (lvData.SelectedValue is ScheduleEntry schedule)
            {
                CurrentData = schedule;
                txName.Text = schedule.Name;
                cbDateRangeKind.SelectedItem = schedule.DateTimeRange.Kind;
                dtpStart.SelectedDate = schedule.DateTimeRange.StartDate;
                dtpEnd.SelectedDate = schedule.DateTimeRange.EndDate;
                cxUseTime.IsChecked = schedule.DateTimeRange.UseTimeRange;
                txStart.Text = schedule.DateTimeRange.StartTime?.ToString("hh\\:mm\\:ss\\.fff") ?? "";
                txEnd.Text = schedule.DateTimeRange.EndTime?.ToString("hh\\:mm\\:ss\\.fff") ?? "";
                cxSunday.IsChecked = schedule.DateTimeRange.DayOfWeeks.Contains(DayOfWeek.Sunday);
                cxMonday.IsChecked = schedule.DateTimeRange.DayOfWeeks.Contains(DayOfWeek.Monday);
                cxTuesday.IsChecked = schedule.DateTimeRange.DayOfWeeks.Contains(DayOfWeek.Tuesday);
                cxWednesday.IsChecked = schedule.DateTimeRange.DayOfWeeks.Contains(DayOfWeek.Wednesday);
                cxThursday.IsChecked = schedule.DateTimeRange.DayOfWeeks.Contains(DayOfWeek.Thursday);
                cxFriday.IsChecked = schedule.DateTimeRange.DayOfWeeks.Contains(DayOfWeek.Friday);
                cxSaturday.IsChecked = schedule.DateTimeRange.DayOfWeeks.Contains(DayOfWeek.Saturday);
                txTags.Text = string.Join("\r\n", schedule.Tags.Select(x => x.Tag + ":" + x.Weight));
            }
        }

        private void lvData_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PerformEdit();
        }

        private void CommandSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var data = CurrentData ?? new ScheduleEntry();
            var name = txName.Text.Trim();
            if (string.IsNullOrWhiteSpace(name))
            {
                MessageBox.Show("Name cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Persona.Schedule.Any(x => x.Name.ToLower() == name.ToLower() && x != data))
            {
                MessageBox.Show($"Schedule named '{name}' is already exists", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            TimeSpan? tStart, tEnd;
            if (string.IsNullOrWhiteSpace(txStart.Text))
            {
                tStart = null;
            }
            else if (TimeSpan.TryParseExact(txStart.Text, "hh\\:mm\\:ss\\.fff", null, out TimeSpan res))
            {
                tStart = res;
            }
            else
            {
                MessageBox.Show($"Invalid start time format!\r\nFormat is hh:mm:ss.fff", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(txEnd.Text))
            {
                tEnd = null;
            }
            else if (TimeSpan.TryParseExact(txEnd.Text, "hh\\:mm\\:ss\\.fff", null, out TimeSpan res))
            {
                tEnd = res;
            }
            else
            {
                MessageBox.Show($"Invalid end time format!\r\nFormat is hh:mm:ss.fff", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var wtag = new List<WeightedTag>();
            var lines = txTags.Text.Split("\r\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            foreach(var line in lines) { 
                var spl = line.Split(':');
                var tag = spl[0];
                double weight = 1;
                if (spl.Length == 2 && double.TryParse(spl[1], out double val))
                {
                    weight = val;
                }
                if (weight != 0)
                {
                    wtag.Add(new WeightedTag()
                    {
                        Tag = tag,
                        Weight = weight,
                    });
                }
            }

            // when all validated
            data.Name = name;
            data.DateTimeRange.Kind = (ScheduleDateRangeKind)cbDateRangeKind.SelectedItem;
            data.DateTimeRange.StartDate = dtpStart.SelectedDate;
            data.DateTimeRange.EndDate = dtpEnd.SelectedDate;
            data.DateTimeRange.UseTimeRange = cxUseTime.IsChecked ?? false;
            data.DateTimeRange.StartTime = tStart;
            data.DateTimeRange.EndTime = tEnd;
            data.DateTimeRange.DayOfWeeks.Clear();
            if (cxSunday.IsChecked == true) data.DateTimeRange.DayOfWeeks.Add(DayOfWeek.Sunday);
            if (cxMonday.IsChecked == true) data.DateTimeRange.DayOfWeeks.Add(DayOfWeek.Monday);
            if (cxTuesday.IsChecked == true) data.DateTimeRange.DayOfWeeks.Add(DayOfWeek.Tuesday);
            if (cxWednesday.IsChecked == true) data.DateTimeRange.DayOfWeeks.Add(DayOfWeek.Wednesday);
            if (cxThursday.IsChecked == true) data.DateTimeRange.DayOfWeeks.Add(DayOfWeek.Thursday);
            if (cxFriday.IsChecked == true) data.DateTimeRange.DayOfWeeks.Add(DayOfWeek.Friday);
            if (cxSaturday.IsChecked == true) data.DateTimeRange.DayOfWeeks.Add(DayOfWeek.Saturday);
            data.Tags.Clear();
            data.Tags.AddRange(wtag);

            if (!Persona.Schedule.Contains(data))
                Persona.Schedule.Add(data);
            ClearForm();
            CurrentData = null;
            grData.IsEnabled = false;
            grList.IsEnabled = true;
            ReloadList();
        }

        private void PerformEdit()
        {
            if (lvData.SelectedValue is ScheduleEntry)
            {
                LoadValues();
                grData.IsEnabled = true;
                grList.IsEnabled = false;
            }
        }

        private void CommandClose_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClearForm();
            CurrentData = null;
            grData.IsEnabled = false;
            grList.IsEnabled = true;
            LoadValues();
        }

        private void cxUseTime_Checked(object sender, RoutedEventArgs e)
        {
            txStart.IsEnabled = txEnd.IsEnabled = true;
        }

        private void cxUseTime_Unchecked(object sender, RoutedEventArgs e)
        {
            txStart.IsEnabled = txEnd.IsEnabled = false;
        }

        private void CommandEdit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvData.SelectedValue is ScheduleEntry;
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (lvData.SelectedValue is ScheduleEntry schedule && Persona != null)
            {
                if (MessageBox.Show("Are you sure to delete this schedule?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                {
                    return;
                }
                Persona.Schedule.Remove(schedule);
                ReloadList();
            }
        }
    }
}
