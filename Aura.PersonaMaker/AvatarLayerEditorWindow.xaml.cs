﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarLayerEditorWindow.xaml
    /// </summary>
    public partial class AvatarLayerEditorWindow : Window
    {
        public AvatarLayerEditorWindow()
        {
            InitializeComponent();
        }

        Layer? Layer { get; set; }
        Avatar? Avatar { get; set; }

        public void LoadLayer(Layer layer, Avatar avatar)
        {
            Layer = layer;
            txLayerId.Text = layer.Id;
            txZIndex.Text = layer.ZIndex.ToString();
            cxFadeIn.IsChecked = layer.FadeIn;
            cxFadeOut.IsChecked = layer.FadeOut;
            cxFadeInTransition.IsChecked = layer.TransitionFadeIn;
            Avatar = avatar;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (Layer == null || Avatar == null) { return; }
            int zIndex;
            if (string.IsNullOrWhiteSpace(txLayerId.Text))
            {
                MessageBox.Show("Layer id cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Avatar.Layers.Any(x => x.Id == txLayerId.Text && x != Layer))
            {
                MessageBox.Show("Layer id already existed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txZIndex.Text, out zIndex))
            {
                MessageBox.Show("Z Index is invalid!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Layer.Id = txLayerId.Text;
            Layer.ZIndex = zIndex;
            Layer.FadeIn = cxFadeIn.IsChecked == true;
            Layer.FadeOut = cxFadeOut.IsChecked == true;
            Layer.TransitionFadeIn = cxFadeInTransition.IsChecked == true;
            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void canvasWidth_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            txLayerId.Text = Util.SanitizeId(txLayerId.Text);
        }
    }
}
