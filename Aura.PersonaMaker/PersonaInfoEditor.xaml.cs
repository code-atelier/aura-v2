﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for PersonaInfoEditor.xaml
    /// </summary>
    public partial class PersonaInfoEditor : Window
    {
        public PersonaInfoEditor()
        {
            InitializeComponent();
        }

        public Persona? Persona { get; private set; }

        public void LoadPersona(Persona persona)
        {
            Persona = persona;
            txAuthor.Text = persona.Author;
            txFamilyName.Text = persona.FamilyName;
            txGivenName.Text = persona.GivenName;
            txTags.Text = string.Join(", ", persona.Tags);
            dtpBirthday.SelectedDate = persona.Birthday;
            rbNSFWNo.IsChecked = !persona.NSFW;
            rbNSFWYes.IsChecked = persona.NSFW;
            txVersion.Text = persona.PersonaVersion ?? "1.0.0";
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (Version.TryParse(txVersion.Text, out var version))
            {
                if (Persona != null)
                {
                    Persona.Author = txAuthor.Text.Trim();
                    Persona.FamilyName = txFamilyName.Text.Trim();
                    Persona.GivenName = txGivenName.Text;
                    Persona.Tags = new List<string>();
                    Persona.Tags.AddRange(txTags.Text.Split(',').Select(x => x.Trim().ToLower()).Where(x => !string.IsNullOrWhiteSpace(x)));
                    Persona.NSFW = rbNSFWYes.IsChecked == true;
                    Persona.Birthday = dtpBirthday.SelectedDate;
                    Persona.PersonaVersion = version.ToString();
                    DialogResult = true;
                    Close();
                }
            }
            else
            {
                MessageBox.Show("Version string is not valid.\r\nIt must follow semantic versioning: {major}.{minor}[.{revision}]", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dtpBirthday_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpBirthday.SelectedDate != null && dtpBirthday.SelectedDate < DateTime.Today)
            {
                var bd = dtpBirthday.SelectedDate.Value.Date;
                var dif = DateTime.Today.Year - bd.Year;
                var ty = new DateTime(DateTime.Today.Year, bd.Month, bd.Day);
                if (ty > DateTime.Today)
                {
                    dif--;
                }
                lbAge.Content = dif + " yo";
            }
            else
            {
                lbAge.Content = "0 yo";
            }
        }
    }
}
