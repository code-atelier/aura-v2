﻿using Aura.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection.Emit;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    public partial class MainWindow
    {
        public Config Config { get; set; }

        private string? LastPersonaPath = null;

        public Persona? Persona { get; private set; }

        public bool HasChanges { get; set; }

        private string? SavePath { get; set; }

        private void NewPersona()
        {
            Persona = CreateEmptyPersona();
            SavePath = null;
            HasChanges = false;
            grControlAvatar.IsEnabled = true;
            LoadPersona();
            SetTitle("New project");
        }

        private void ClosePersona()
        {
            if (HasChanges && !ConfirmClose("close persona")) return;
            Persona = null;
            grControlAvatar.IsEnabled = false;
            LoadPersona();
            SetTitle("");
        }

        private void OpenPersona(string path)
        {
            try
            {
                var persona = Persona.Load(path);
                if (persona == null) throw new Exception("Failed to load persona!");
                if (persona.MakerVersion != null && Version.TryParse(persona.MakerVersion, out var version) && version > PersonaMakerStatic.Version)
                {
                    if (MessageBox.Show("This persona is created with a newer version of Persona Maker (v" + version + ").\r\nSome content might not shown and might be lost if saved.\r\nDo you want to continue opening it?", "Information", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                        return;
                }
                Persona = persona;
                LoadPersona();
                LastPersonaPath = path;
                SavePath = path;
                SaveConfig();
                HasChanges = false;
                SetTitle(null);
                MessageBox.Show("Persona loaded!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool ConfirmClose(string action)
        {
            return MessageBox.Show($"Are you sure to {action}?\r\nUnsaved changes will be lost.", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes;
        }

        private void LoadPersona()
        {
            lbAuthor.Content = "";
            lbName.Content = "";
            lbNSFW.Content = "";
            lbTags.Content = "";
            lbID.Content = "";
            lbVersion.Content = "";
            tvLayers.Items.Clear();
            SpriteManager.Clear();
            cbSchedule.Items.Clear();
            txSimTags.Clear();
            grControlAvatar.IsEnabled = Persona != null;

            if (Persona != null)
            {
                foreach (var sprite in Persona.Avatar.Sprites)
                {
                    SpriteManager.Add(sprite);
                }
                lbAuthor.Content = Persona.Author;
                lbName.Content = Persona.GivenName + " " + Persona.FamilyName;
                lbNSFW.Content = Persona.NSFW ? "YES" : "NO";
                lbNSFW.Foreground = new SolidColorBrush(Persona.NSFW ? Colors.Red : Colors.Green);
                lbID.Content = Persona.Id;
                lbVersion.Content = Persona.PersonaVersion?.ToString();
                lbTags.Content = string.Join(", ", Persona.Tags);
                foreach (var schedule in Persona.Schedule)
                {
                    cbSchedule.Items.Add(schedule);
                }
            }

            avaCanvas.LoadAvatar(Persona?.Avatar, SpriteManager);

            if (Persona == null) return;
            LoadPersonaLayers();
        }

        private void LoadPersonaLayers()
        {
            if (Persona == null) return;
            var layers = Persona.Avatar.Layers.ToList();
            layers.Sort((a, b) => a.ZIndex.CompareTo(b.ZIndex));
            foreach (var layer in layers)
            {
                var tvi = new TreeViewItem();
                tvi.Header = layer.Id;
                tvi.Tag = layer;
                tvi.FontWeight = FontWeights.Bold;
                tvi.IsExpanded = true;
                tvLayers.Items.Add(tvi);
                foreach (var mat in layer.Materials)
                {
                    var match = avaCanvas.MaterialOverride.ContainsKey(layer.Id) ? avaCanvas.MaterialOverride[layer.Id] ?? layer.Default : layer.Default;
                    var mat2 = Persona.Avatar.Materials[mat.Material];
                    if (mat2 != null)
                    {
                        var tvm = new TreeViewItem();
                        tvm.Header = mat2.Reference ?? $"mat<{mat2.Id.ToString("n")}>";
                        tvm.Tag = new Tuple<string, Guid?>(layer.Id, mat.Material);
                        tvm.FontWeight = FontWeights.Regular;
                        if (mat.Material == layer.Default)
                        {
                            tvm.FontStyle = FontStyles.Italic;
                        }
                        if (mat.Material == match)
                        {
                            tvm.Foreground = new SolidColorBrush(Colors.Blue);
                        }
                        tvm.MouseDoubleClick += MaterialDoubleClick;
                        tvi.Items.Add(tvm);
                    }
                }
            }
        }

        private void UpdatePersonaLayerState()
        {
            if (Persona == null) return;
            foreach (var state in avaCanvas.MaterialOverride)
            {
                var layerId = state.Key;
                var layer = Persona.Avatar.Layers.FirstOrDefault(x => x.Id == layerId);
                Guid? materialId = null;
                if (layer != null)
                {
                    materialId = state.Value ?? layer.Default;
                }
                var tvp = tvLayers.Items.OfType<TreeViewItem>().Where(x => x.Tag is Layer l && l.Id == layerId).FirstOrDefault();
                if (tvp != null)
                {
                    foreach (var child in tvp.Items.OfType<TreeViewItem>())
                    {
                        if (child.Tag is Tuple<string, Guid?> meta)
                        {
                            if (meta.Item2 == materialId)
                            {
                                child.Foreground = new SolidColorBrush(Colors.Blue);
                            }
                            else
                            {
                                child.Foreground = new SolidColorBrush(Colors.Black);
                            }
                        }
                    }
                }
            }
        }

        private void MaterialDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is TreeViewItem tvi && tvi.Tag is Tuple<string, Guid?> tag)
            {
                if (tvi.Parent is TreeViewItem tvp)
                {
                    foreach (var child in tvp.Items.OfType<TreeViewItem>())
                    {
                        child.Foreground = new SolidColorBrush(Colors.Black);
                    }
                }

                var lastItem = avaCanvas.MaterialOverride[tag.Item1];
                var sameItem = tag.Item2 == lastItem;
                if (!sameItem)
                {
                    avaCanvas.MaterialOverride[tag.Item1] = tag.Item2;
                    avaCanvas.UpdateLayers();

                    tvi.Foreground = new SolidColorBrush(Colors.Blue);
                }
                else
                {
                    avaCanvas.MaterialOverride[tag.Item1] = null;
                    avaCanvas.UpdateLayers();

                    tvi.Foreground = new SolidColorBrush(Colors.Black);
                    if (tvi.Parent is TreeViewItem tvx && tvx.Tag is Layer layer)
                    {
                        foreach (var child in tvx.Items.OfType<TreeViewItem>())
                        {
                            if (child.Tag is Tuple<string, Guid?> tg && tg.Item2 == layer.Default)
                            {
                                child.Foreground = new SolidColorBrush(Colors.Blue);
                            }
                        }
                    }
                }
            }
        }

        private void UpdateMaterialTreeView()
        {
            foreach (var tvp in tvLayers.Items.OfType<TreeViewItem>())
            {
                foreach (var child in tvp.Items.OfType<TreeViewItem>())
                {
                    if (tvp.Tag is Layer layer)
                    {
                        if (child.Tag is Tuple<string, Guid?> tg && tg.Item2 == layer.Default)
                        {
                            child.Foreground = new SolidColorBrush(Colors.Blue);
                        }
                        else
                        {
                            child.Foreground = new SolidColorBrush(Colors.Black);
                        }
                    }
                }
            }
        }

        private Persona CreateEmptyPersona()
        {
            var guids = new List<Guid>();
            for (var i = 0; i < 6; i++) guids.Add(Guid.NewGuid());

            return new Persona()
            {
                Avatar = new Avatar()
                {
                    Display = new DisplayConfiguration()
                    {
                        CanvasHeight = 500,
                        CanvasWidth = 200,
                    }
                }
            };
        }

        public bool SaveConfig()
        {
            Config.LastEditedPersona = LastPersonaPath;
            var json = JsonSerializer.Serialize(Config);
            try
            {
                File.WriteAllText(ConfigPath, json);
                return true;
            }
            catch { }
            return false;
        }

        private void SavePersona(bool saveAs = false)
        {
            if (Persona == null) return;
            if (Persona.MakerVersion != null && Version.TryParse(Persona.MakerVersion, out var version) && version > PersonaMakerStatic.Version)
            {
                if (MessageBox.Show("This persona is created with a newer version of Persona Maker (v\" + version + \").\r\nSome content might be lost if saved.\r\nDo you want to continue opening it?", "Information", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                    return;
            }
            
            Persona.MakerVersion = PersonaMakerStatic.Version.ToString();
            Persona.TargetVersion = AuraInfo.Version.ToString();

            var savePath = SavePath;
            if (saveAs || string.IsNullOrWhiteSpace(SavePath))
            {
                var sfd = new SaveFileDialog();
                if (!string.IsNullOrWhiteSpace(Persona.GivenName + Persona.FamilyName))
                    sfd.FileName = Persona.GivenName + "_" + Persona.FamilyName + ".a2p";
                sfd.Filter = "Aura v2 Persona File|*.a2p";
                sfd.OverwritePrompt = true;
                if (sfd.ShowDialog() == true)
                {
                    savePath = sfd.FileName;
                }
                else
                {
                    return;
                }
            }
            if (savePath == null)
            {
                return;
            }

            try
            {
                Persona.Save(savePath);
                LastPersonaPath = SavePath = savePath;
                SaveConfig();
                HasChanges = false;
                SetTitle(null);
                MessageBox.Show("Persona saved!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
