﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for ShapeEditor.xaml
    /// </summary>
    public partial class ShapeEditor : Window
    {
        public ShapeEditor()
        {
            InitializeComponent();
        }

        Models.Shape? Shape { get; set; }
        Region? Region {get;set;}

        public void LoadRegion(Region region, Models.Shape shape)
        {
            Region = region;
            Shape = shape;
            txHeight.Text = shape.Height.ToString();
            txWidth.Text = shape.Width.ToString();
            txX.Text = shape.X.ToString();
            txY.Text = shape.Y.ToString();
            cbType.SelectedIndex = (int)shape.Type;
        }

        private void cbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (txWidth != null && txHeight != null && lbWidth != null && lbHeight != null)
            {
                var visibility = cbType.SelectedIndex < 2 ? Visibility.Visible : Visibility.Collapsed;
                txWidth.Visibility = txHeight.Visibility = lbWidth.Visibility = lbHeight.Visibility = visibility;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Shape == null || Region == null) return;
            int width, height, x, y;
            var shpType = (ShapeType)cbType.SelectedIndex; 
            if (!int.TryParse(txWidth.Text, out width) || (shpType != ShapeType.Point && width <= 0))
            {
                MessageBox.Show("Invalid width!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txHeight.Text, out height) || (shpType != ShapeType.Point && height <= 0))
            {
                MessageBox.Show("Invalid height!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txX.Text, out x))
            {
                MessageBox.Show("Invalid x position!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txY.Text, out y))
            {
                MessageBox.Show("Invalid y position!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Region.Shapes.Any(x => x.Type == ShapeType.Point && x != Shape))
            {
                MessageBox.Show("You can only add one point to a region!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Shape.X = x;
            Shape.Y = y;
            Shape.Width = width;
            Shape.Height = height;
            Shape.Type = shpType;
            DialogResult = true;
            Close();
        }
    }
}
