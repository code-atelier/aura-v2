﻿using Aura.Models;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarSetManager.xaml
    /// </summary>
    public partial class AvatarSetManager : Window
    {
        public AvatarSetManager()
        {
            InitializeComponent();
        }

        public Avatar? Avatar { get; private set; }

        public SpriteManager SpriteManager { get; private set; } = new SpriteManager();

        public void LoadAvatar(Avatar avatar)
        {
            Avatar = avatar;
            SpriteManager.Clear();
            foreach (var spr in Avatar.Sprites)
            {
                SpriteManager.Add(spr);
            }
            canvasAvatar.LoadAvatar(avatar, SpriteManager);
            grPreview.Width = canvasAvatar.Width = Avatar.Display.CanvasWidth;
            grPreview.Height = canvasAvatar.Height = Avatar.Display.CanvasHeight;
            LoadSets();
        }

        private void LoadSets()
        {
            Set? lastSelected = lvRegions.SelectedItem as Set;
            lvRegions.Items.Clear();
            if (Avatar != null)
            {
                foreach (var set in Avatar.Sets.GroupBy(x => x.Category + "#" + x.Priority.ToString().PadLeft(9, '0')).OrderByDescending(x => x.Key).SelectMany(x => x.OrderBy(w => w.Reference)))
                {
                    lvRegions.Items.Add(set);
                }
                if (lvRegions.Items.Contains(lastSelected))
                    lvRegions.SelectedItem = lastSelected;
            }
        }
        private void UpdatePreview()
        {
            if (Avatar != null && lvRegions.SelectedItem is Set set)
            {
                canvasAvatar.ForceSet.Clear();
                canvasAvatar.ForceSet.Add(set.Id);
                canvasAvatar.UpdateLayers();
            }
        }

        private void lvRegions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdatePreview();
        }

        private void lvRegions_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                if (Avatar != null && lvRegions.SelectedItem is Set set)
                {
                    var cloned = set.TagCondition.Clone();
                    var w = new TagEditor();
                    w.LoadTagCondition(cloned, set.Reference);
                    if (w.ShowDialog() == true)
                    {
                        set.TagCondition = cloned;
                    }
                }
            }
            else
                PerformEdit();
        }

        private void PerformEdit()
        {
            if (Avatar != null && lvRegions.SelectedItem is Set set)
            {
                var w = new AvatarSetEditor();
                w.LoadSet(set, Avatar);
                if (w.ShowDialog() == true)
                {
                    LoadSets();
                }
            }
        }

        private void CommandNew_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandModify_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvRegions.SelectedItem is Set;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null)
            {
                var set = new Set();
                set.TagCondition.Bias = 0;
                var w = new AvatarSetEditor();
                w.LoadSet(set, Avatar);
                if (w.ShowDialog() == true)
                {
                    Avatar.Sets.Add(set);
                    LoadSets();
                }
            }
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && lvRegions.SelectedItem is Set set)
            {

                if (MessageBox.Show("Are you sure to delete this set?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
                Avatar.Sets.Remove(set);
                LoadSets();
            }
        }

        private void CommandConditions_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && lvRegions.SelectedItem is Set set)
            {
                var cloned = set.TagCondition.Clone();
                var w = new TagEditor();
                w.LoadTagCondition(cloned, set.Reference);
                if (w.ShowDialog() == true)
                {
                    set.TagCondition = cloned;
                }
            }
        }
    }
}
