﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarSpriteEditor.xaml
    /// </summary>
    public partial class AvatarSpriteEditor : Window
    {
        public Sprite Sprite { get; private set; }
        public ObjectWithReferenceCollection<Sprite> Sprites { get; private set; } = new ObjectWithReferenceCollection<Sprite>();


        public AvatarSpriteEditor()
        {
            InitializeComponent();
            Sprite = new Sprite();
        }

        public void LoadSprite(Sprite sprite, ObjectWithReferenceCollection<Sprite> collection)
        {
            Sprite = sprite;
            Sprites = collection;
            FillInput();
        }

        private void FillInput()
        {
            txId.Text = Sprite.Id.ToString("n");
            txRef.Text = Sprite.Reference;
            txHeight.Text = txWidth.Text = "?";
            try
            {
                var buf = new byte[Sprite.BinaryData.Length];
                Array.Copy(Sprite.BinaryData, buf, buf.Length);
                var ms = new MemoryStream(buf);
                var bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = ms;
                bmp.EndInit();
                imgPreview.Source = bmp;
                txHeight.Text = bmp.PixelHeight.ToString();
                txWidth.Text = bmp.PixelWidth.ToString();
            }
            catch
            {
                MessageBox.Show("Invalid image data.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                DialogResult = false;
                Close();
            }
        }

        private void txRef_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            txRef.Text = Util.SanitizeId(txRef.Text);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Guid id;
            if (!Guid.TryParse(txId.Text, out id))
            {
                MessageBox.Show("Invalid Id!\r\nId must be a valid guid.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(txRef.Text))
            {
                MessageBox.Show("Reference cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Sprites.Any(x => x.Id == id && x != Sprite))
            {
                MessageBox.Show("Sprite with the same id already existed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Sprites.Any(x => x.Reference == txRef.Text && x != Sprite))
            {
                MessageBox.Show("Sprite with the same reference already existed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (imgPreview.Source is BitmapImage bmp)
            {
                Sprite.Height = bmp.PixelHeight;
                Sprite.Width = bmp.PixelWidth;
                if (bmp.StreamSource is MemoryStream ms)
                {
                    Sprite.BinaryData = new byte[ms.Length];
                    ms.Seek(0, SeekOrigin.Begin);
                    ms.Read(Sprite.BinaryData, 0, Sprite.BinaryData.Length);
                    ms.Seek(0, SeekOrigin.Begin);
                }
            }
            Sprite.Id = id;
            Sprite.Reference = txRef.Text;
            DialogResult = true;
            Close();
        }

        private void Grid_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files != null && files.Length == 1 && files[0].ToLower().EndsWith(".png"))
                {
                    e.Effects = DragDropEffects.Copy;
                }
            }
            e.Handled = true;
        }

        private void Grid_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files != null && files.Length == 1 && files[0].ToLower().EndsWith(".png"))
                {
                    e.Handled = true;
                    try
                    {
                        byte[] buf;
                        using (var fi = File.OpenRead(files[0]))
                        {
                            buf = new byte[fi.Length];
                            fi.Read(buf, 0, buf.Length);
                        }
                        var ms = new MemoryStream(buf);
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.StreamSource = ms;
                        bmp.EndInit();
                        if (bmp.PixelWidth != Sprite.Width || bmp.PixelHeight != Sprite.Height)
                        {
                            if (MessageBox.Show($"The new image have different size, continue?\r\nOld: {Sprite.Width}x{Sprite.Height}\r\nNew: {bmp.PixelWidth}x{bmp.PixelHeight}",
                                "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                            {
                                return;
                            }
                        }
                        imgPreview.Source = bmp;
                        txHeight.Text = bmp.PixelHeight.ToString();
                        txWidth.Text = bmp.PixelWidth.ToString();
                    }
                    catch
                    {
                        MessageBox.Show("Invalid image data.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        DialogResult = false;
                        Close();
                    }
                }
            }
        }
    }
}
