﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarSetEditor.xaml
    /// </summary>
    public partial class AvatarSetEditor : Window
    {
        public Set? EditedSet { get; private set; }

        public TagCondition? EditedCondition { get; private set; }

        public Avatar? Avatar { get; private set; }

        public AvatarSetEditor()
        {
            InitializeComponent();
        }

        public void LoadSet(Set set, Avatar avatar)
        {
            EditedSet = set;
            EditedCondition = set.TagCondition.Clone();
            Avatar = avatar;

            txId.Text = set.Id.ToString("n");
            txCategory.Text = set.Category;
            txReference.Text = set.Reference;
            txPriority.Text = set.Priority.ToString();
            referenceInitiallyEmpty = string.IsNullOrWhiteSpace(set.Reference);
            LoadLayerConfigurations(set.Layers);
        }

        private void LoadLayerConfigurations(Dictionary<string, Guid?> config)
        {
            if (Avatar != null && EditedSet != null)
            {
                spLayers.Children.Clear();
                foreach (var layer in Avatar.Layers.OrderBy(x => x.ZIndex))
                {
                    Guid? selectedMaterial = null;
                    if (EditedSet.Layers.ContainsKey(layer.Id))
                    {
                        selectedMaterial = EditedSet.Layers[layer.Id];
                    }
                    var sp = new StackPanel();
                    sp.Orientation = Orientation.Horizontal;
                    sp.Margin = new Thickness(0, 6, 0, 0);
                    var lb = new Label();
                    lb.Width = 150;
                    lb.Content = layer.Id;
                    sp.Children.Add(lb);
                    var cb = new ComboBox();
                    cb.Width = 200;
                    cb.Tag = layer;
                    sp.Tag = cb;

                    var ecbi = new ComboBoxItem();
                    ecbi.Content = "-- Inherit --";
                    ecbi.Tag = null;
                    cb.Items.Add(ecbi);
                    var selectedItem = ecbi;
                    ecbi = new ComboBoxItem();
                    ecbi.Content = "-- Unset --";
                    ecbi.Tag = Guid.Empty;
                    cb.Items.Add(ecbi);
                    if (selectedMaterial == Guid.Empty)
                        selectedItem = ecbi;

                    foreach (var mat in layer.Materials)
                    {
                        var tmat = Avatar.Materials[mat.Material];
                        if (tmat != null)
                        {
                            var cbi = new ComboBoxItem();
                            cbi.Content = tmat.ToString();
                            cbi.Tag = tmat;
                            cb.Items.Add(cbi);
                            if (tmat.Id == selectedMaterial)
                            {
                                selectedItem = cbi;
                            }
                        }
                    }
                    sp.Children.Add(cb);
                    cb.SelectedItem = selectedItem;
                    cb.SelectionChanged += Cb_SelectionChanged;
                    spLayers.Children.Add(sp);
                }
            }
        }

        private void Cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ComboBox cb && cb.SelectedItem is ComboBoxItem cbi && cbi.Tag is Material mat)
            {
                if (referenceInitiallyEmpty)
                {
                    var spl = mat.Reference?.Split(new char[] { ':', '/', '\\', '.' }, StringSplitOptions.RemoveEmptyEntries);
                    txReference.Text = spl?.Length > 1 ? spl[1] : mat.Reference;
                    txCategory.Text = spl?.Length > 1 ? spl[0] : txCategory.Text;
                    referenceInitiallyEmpty = true;
                }
            }
        }

        private void txCategory_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            txCategory.Text = Util.SanitizeId(txCategory.Text);
        }

        private void txReference_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            txReference.Text = Util.SanitizeId(txReference.Text);
        }

        bool referenceInitiallyEmpty = false;
        private void txReference_TextChanged(object sender, TextChangedEventArgs e)
        {
            referenceInitiallyEmpty = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Avatar == null || EditedSet == null) return;

            Guid id;
            int prio;
            if (!Guid.TryParse(txId.Text, out id))
            {
                MessageBox.Show("Invalid Id!\r\nId must be a valid guid.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(txReference.Text))
            {
                MessageBox.Show("Reference cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!int.TryParse(txPriority.Text, out prio))
            {
                MessageBox.Show("Invalid set priority!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Avatar.Sets.Any(x => x.Id == id && x != EditedSet))
            {
                MessageBox.Show("Set with the same id already existed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Avatar.Sets.Any(x => x.Reference == txReference.Text && x != EditedSet))
            {
                MessageBox.Show("Set with the same reference already existed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            EditedSet.Id = id;
            EditedSet.Reference = txReference.Text;
            EditedSet.Priority = prio;
            EditedSet.Category = txCategory.Text.Trim();
            EditedSet.Layers.Clear();
            foreach (var spLayer in spLayers.Children.OfType<StackPanel>())
            {
                if (spLayer.Tag is ComboBox cbLayer && cbLayer.Tag is Layer layer)
                {
                    if (cbLayer.SelectedItem is ComboBoxItem cbi && cbi.Tag is Material mat)
                    {
                        EditedSet.Layers[layer.Id] = mat.Id;
                    }
                    else if (cbLayer.SelectedItem is ComboBoxItem cbi2 && cbi2.Tag is Guid guid)
                    {
                        EditedSet.Layers[layer.Id] = guid;
                    }
                    else
                    {
                        EditedSet.Layers[layer.Id] = null;
                    }
                }
            }
            if (EditedCondition != null)
            {
                EditedSet.TagCondition = EditedCondition;
            }
            DialogResult = true;
            Close();
        }

        private void btnConditions_Click(object sender, RoutedEventArgs e)
        {
            if (EditedSet == null || EditedCondition == null) return;
            var cloned = EditedCondition.Clone();
            var w = new TagEditor();
            w.LoadTagCondition(cloned, txReference.Text);
            if (w.ShowDialog() == true)
            {
                EditedCondition = cloned;
            }
        }
    }
}
