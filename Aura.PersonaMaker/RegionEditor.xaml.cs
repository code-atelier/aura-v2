﻿using Aura.Models;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for RegionEditor.xaml
    /// </summary>
    public partial class RegionEditor : Window
    {
        private Avatar? Avatar { get; set; }
        private SpriteManager SpriteManager { get; } = new SpriteManager();
        AvatarCanvas AvatarCanvas { get; }
        Region? EditedRegion { get; set; }
        TagCondition? EditedTagCondition { get; set; }
        SolidColorBrush FillColor { get; } = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        public RegionEditor()
        {
            InitializeComponent();
            AvatarCanvas = new AvatarCanvas();
            grPreview.Children.Insert(0, AvatarCanvas);

            txReference.Items.Clear();

            var typ = typeof(InternalTags.Anchor);
            var values = typ.GetFields().Where(x => x.IsStatic && x.FieldType == typeof(string) && x.IsLiteral).ToList();
            foreach (var v in values)
            {
                var val = v.GetValue(null) as string;
                txReference.Items.Add(val);
            }

            typ = typeof(InternalTags.Region);
            values = typ.GetFields().Where(x => x.IsStatic && x.FieldType == typeof(string) && x.IsLiteral).ToList();
            foreach (var v in values)
            {
                var val = v.GetValue(null) as string;
                txReference.Items.Add(val);
            }
        }

        public void LoadRegion(Avatar avatar, Region regions)
        {
            Avatar = avatar;
            SpriteManager.Clear();
            foreach (var spr in Avatar.Sprites)
            {
                SpriteManager.Add(spr);
            }
            grShapes.Width = grPreview.Width = AvatarCanvas.Width = Avatar.Display.CanvasWidth;
            grShapes.Height = grPreview.Height = AvatarCanvas.Height = Avatar.Display.CanvasHeight;
            AvatarCanvas.LoadAvatar(Avatar, SpriteManager);
            AvatarCanvas.ReloadRegions(null, x => false);
            EditedRegion = regions;
            EditedTagCondition = regions.TagCondition.Clone();

            txId.Text = regions.Id.ToString("n");
            txReference.Text = regions.Reference;
            lvShapes.Items.Clear();
            foreach (var r in regions.Shapes)
            {
                lvShapes.Items.Add(r);
            }
            DrawShapes();
        }

        public void ReloadRegions()
        {
            var col = lvShapes.Items.OfType<Models.Shape>().ToList();
            var sel = lvShapes.SelectedItem as Models.Shape;
            lvShapes.Items.Clear();
            foreach (var c in col)
            {
                lvShapes.Items.Add(c);
            }
            lvShapes.SelectedItem = sel;
        }

        public void DrawShapes()
        {
            grShapes.Children.Clear();
            foreach (var r in lvShapes.Items.OfType<Models.Shape>())
            {
                var selected = lvShapes.SelectedItem == r;
                System.Windows.Shapes.Shape? shape = null;
                if (r.Type == ShapeType.Rectangle)
                {
                    shape = new Rectangle();
                    shape.Width = r.Width;
                    shape.Height = r.Height;
                    shape.Margin = new Thickness(r.X, r.Y, 0, 0);
                }
                else if (r.Type == ShapeType.Ellipse)
                {
                    shape = new Ellipse();
                    shape.Width = r.Width;
                    shape.Height = r.Height;
                    shape.Margin = new Thickness(r.X, r.Y, 0, 0);
                }
                else if (r.Type == ShapeType.Point)
                {
                    shape = new Ellipse();
                    shape.Width = 6;
                    shape.Height = 6;
                    shape.Margin = new Thickness(r.X - 3, r.Y - 3, 0, 0);
                }

                if (shape != null)
                {
                    shape.HorizontalAlignment = HorizontalAlignment.Left;
                    shape.VerticalAlignment = VerticalAlignment.Top;
                    shape.Fill = r.Type == ShapeType.Point ? new SolidColorBrush(Color.FromArgb(255, 255, 0, 255)) : FillColor;
                    shape.Opacity = selected ? 1 : 0.3;
                    grShapes.Children.Add(shape);
                }
            }
        }

        private void CommandNew_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (EditedRegion == null) return;
            var shp = new Models.Shape()
            {
                Height = 50,
                Width = 50,
            };
            var w = new ShapeEditor();
            w.LoadRegion(EditedRegion, shp);
            if (w.ShowDialog() == true)
            {
                lvShapes.Items.Add(shp);
                lvShapes.SelectedItem = shp;
            }
        }

        private void CommandModify_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvShapes.SelectedItem is Models.Shape;
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (lvShapes.SelectedItem is Models.Shape shp)
            {
                if (MessageBox.Show("Are you sure to delete this shape?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    lvShapes.Items.Remove(shp);
                }
            }
        }

        private void lvShapes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DrawShapes();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (EditedRegion == null || Avatar == null) return;

            Guid id;
            if (!Guid.TryParse(txId.Text, out id))
            {
                MessageBox.Show("Invalid Id!\r\nId must be a valid guid.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(txReference.Text))
            {
                MessageBox.Show("Reference cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Avatar.Regions.Any(x => x.Id == id && x != EditedRegion))
            {
                MessageBox.Show("Region with the same id already existed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Avatar.Regions.Any(x => x.Reference == txReference.Text && x != EditedRegion))
            {
                MessageBox.Show("Region with the same reference already existed.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            EditedRegion.Id = id;
            EditedRegion.Reference = txReference.Text.Trim();
            EditedRegion.Shapes.Clear();
            foreach (var shp in lvShapes.Items.OfType<Models.Shape>())
            {
                EditedRegion.Shapes.Add(shp);
            }
            if (EditedTagCondition != null)
                EditedRegion.TagCondition = EditedTagCondition;
            DialogResult = true;
            Close();
        }

        private void txReference_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            txReference.Text = Util.SanitizeId(txReference.Text);
        }

        private void lvShapes_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                PerformEdit();
            }
        }

        private void PerformEdit()
        {
            if (EditedRegion == null) { return; }
            if (lvShapes.SelectedItem is Models.Shape shp)
            {
                var w = new ShapeEditor();
                w.LoadRegion(EditedRegion, shp);
                if (w.ShowDialog() == true)
                {
                    ReloadRegions();
                }
            }
        }

        private void btnConditions_Click(object sender, RoutedEventArgs e)
        {
            if (EditedRegion == null || EditedTagCondition == null) return;
            var cloned = EditedTagCondition.Clone();
            var w = new TagEditor();
            w.LoadTagCondition(cloned, txReference.Text);
            if (w.ShowDialog() == true)
            {
                EditedTagCondition = cloned;
            }
        }
    }
}
