﻿using Aura.Models;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.WebSockets;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarLayerManager.xaml
    /// </summary>
    public partial class AvatarLayerManager : Window
    {
        public static RoutedCommand AddMaterial { get; } = new RoutedCommand();
        public static RoutedCommand DeleteMaterial { get; } = new RoutedCommand();
        public static RoutedCommand SetDefaultMaterial { get; } = new RoutedCommand();
        public static RoutedCommand UnsetDefaultMaterial { get; } = new RoutedCommand();
        public static RoutedCommand ApplyMaterialPosition { get; } = new RoutedCommand();
        public static RoutedCommand AddLayer { get; } = new RoutedCommand();
        public static RoutedCommand EditLayer { get; } = new RoutedCommand();
        public static RoutedCommand DeleteLayer { get; } = new RoutedCommand();

        private Avatar? Avatar { get; set; }

        private SpriteManager SpriteManager { get; } = new SpriteManager();

        public AvatarLayerManager()
        {
            InitializeComponent();
        }

        public void LoadAvatar(Avatar avatar)
        {
            Avatar = avatar;
            SpriteManager.Clear();
            foreach (var spr in Avatar.Sprites)
            {
                SpriteManager.Add(spr);
            }
            LoadLayers();
        }

        private void LoadLayers()
        {
            tvLayers.Items.Clear();
            if (Avatar == null) { return; }
            foreach (var layer in Avatar.Layers.OrderBy(x => x.ZIndex))
            {
                var tvi = new TreeViewItem();
                tvi.Header = layer.ToString();
                tvi.Tag = layer;
                tvi.FontWeight = FontWeights.Bold;
                tvi.IsExpanded = true;

                layer.Materials.Sort((a, b) => Avatar.Materials.FirstOrDefault(x => x.Id == a.Material)?.Reference?.CompareTo(Avatar.Materials.FirstOrDefault(x => x.Id == b.Material)?.Reference) ?? 0);
                foreach (var mat in layer.Materials)
                {
                    var material = Avatar.Materials.FirstOrDefault(x => x.Id == mat.Material);
                    var tvm = new TreeViewItem();
                    tvm.Header = material != null ? material.Reference ?? $"<mat:{material.Id.ToString("n")}>" : "Missing: " + mat.Material.ToString("n");
                    tvm.Tag = mat;
                    tvm.FontWeight = FontWeights.Regular;
                    if (material != null && layer.Default == material.Id)
                    {
                        tvm.FontStyle = FontStyles.Italic;
                    }
                    if (material == null)
                    {
                        tvm.Foreground = new SolidColorBrush(Colors.Red);
                    }
                    tvi.Items.Add(tvm);
                }

                tvLayers.Items.Add(tvi);
            }
        }

        private void CommandAddMat_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar == null) { return; }
            if (tvLayers.SelectedItem is TreeViewItem tvi)
            {
                Layer? layer = null;
                if (tvi.Tag is Layer)
                {
                    layer = tvi.Tag as Layer;
                }
                else if (tvi.Tag is LayerMaterial)
                {
                    var tvp = tvi.Parent;
                    if (tvp is TreeViewItem tvpp && tvpp.Tag is Layer pl)
                    {
                        layer = pl;
                    }
                }
                if (layer == null) return;

                var w = new AvatarMaterialManager();
                w.LoadAvatar(Avatar);
                if (w.ShowSelectionDialog() == true && w.SelectedMaterials.Count > 0)
                {
                    foreach (var mat in w.SelectedMaterials)
                    {
                        layer.Materials.Add(new LayerMaterial()
                        {
                            Material = mat.Id
                        });
                    }
                    LoadLayers();
                }
            }
        }

        private void CommandAddMat_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Avatar != null && tvLayers.SelectedItem != null;
        }

        private void CommandAddLayer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar == null) { return; }
            var layer = new Layer()
            {
                Id = "newlayer",
                ZIndex = Avatar.Layers.Count == 0 ? 0 : Avatar.Layers.Max(x => x.ZIndex) + 1,
            };
            var w = new AvatarLayerEditorWindow();
            w.LoadLayer(layer, Avatar);
            if (w.ShowDialog() == true)
            {
                Avatar.Layers.Add(layer);
                LoadLayers();
            }
        }

        private void CommandAddLayer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Avatar != null;
        }

        private void CommandEditLayer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Tag is Layer layer)
            {
                var w = new AvatarLayerEditorWindow();
                w.LoadLayer(layer, Avatar);
                if (w.ShowDialog() == true)
                {
                    LoadLayers();
                }
            }
        }

        private void CommandEditLayer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Tag is Layer layer;
        }

        private void CommandDeleteLayer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Tag is Layer layer)
            {
                if (MessageBox.Show("Are you sure to delete this layer?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
                Avatar.Layers.Remove(layer);
                LoadLayers();
            }
        }

        private void CommandSetDefaultMat_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Parent is TreeViewItem tvp
                && tvi.Tag is LayerMaterial mat && tvp.Tag is Layer layer
                && layer.Default != mat.Material)
            {
                tvi.FontStyle = FontStyles.Italic;
                layer.Default = mat.Material;
                UpdateDefMatButton();
            }
        }

        private void CommandSetDefaultMat_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Parent is TreeViewItem tvp
                && tvi.Tag is LayerMaterial mat && tvp.Tag is Layer layer
                && layer.Default != mat.Material;
        }

        private void tvLayers_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            UpdateDefMatButton();
            UpdatePreview();
        }

        void UpdateDefMatButton()
        {
            btnUnDefaultMaterial.Visibility = Visibility.Collapsed;
            btnDefaultMaterial.Visibility = Visibility.Visible;
            if (Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Parent is TreeViewItem tvp
                && tvi.Tag is LayerMaterial mat && tvp.Tag is Layer layer)
            {
                btnDefaultMaterial.Visibility = layer.Default == mat.Material ? Visibility.Collapsed : Visibility.Visible;
                btnUnDefaultMaterial.Visibility = layer.Default == mat.Material ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void CommandUnsetDefaultMat_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Parent is TreeViewItem tvp
                && tvi.Tag is LayerMaterial mat && tvp.Tag is Layer layer
                && layer.Default == mat.Material)
            {
                tvi.FontStyle = FontStyles.Normal;
                layer.Default = null;
                UpdateDefMatButton();
            }
        }

        private void CommandUnsetDefaultMat_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Parent is TreeViewItem tvp
                && tvi.Tag is LayerMaterial mat && tvp.Tag is Layer layer
                && layer.Default == mat.Material;
        }

        private void CommandDelMat_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Parent is TreeViewItem tvp
                && tvi.Tag is LayerMaterial mat && tvp.Tag is Layer layer)
            {
                if (MessageBox.Show("Are you sure to delete this material from this layer?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
                layer.Materials.Remove(mat);
                LoadLayers();
            }
        }

        private void CommandDelMat_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Parent is TreeViewItem tvp
                && tvi.Tag is LayerMaterial && tvp.Tag is Layer;
        }

        private void CommandSetMatPos_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Tag is LayerMaterial mat)
            {
                int x, y;
                if (!int.TryParse(txMatPosX.Text, out x))
                {
                    MessageBox.Show("X value is not valid!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!int.TryParse(txMatPosY.Text, out y))
                {
                    MessageBox.Show("Y value is not valid!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                mat.X = x; mat.Y = y;
                UpdatePreview(true);
            }
        }

        private void UpdatePreview(bool force = false)
        {
            txMatPosX.Text = txMatPosY.Text = "0";
            grPreview.Children.Clear();
            if (Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Tag is LayerMaterial mat && tvi.Parent is TreeViewItem tvp && tvp.Tag is Layer layer)
            {
                var avaLayer = new AvatarLayer(layer, Avatar, SpriteManager);
                grPreview.Width = avaLayer.Width = Avatar.Display.CanvasWidth;
                grPreview.Height = avaLayer.Height = Avatar.Display.CanvasHeight;
                avaLayer.SetActiveMaterial(mat.Material);
                grPreview.Children.Add(avaLayer);
                txMatPosX.Text = mat.X.ToString();
                txMatPosY.Text = mat.Y.ToString();
            }
        }

        private void CommandSetMatPos_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Avatar != null && tvLayers.SelectedItem is TreeViewItem tvi && tvi.Tag is LayerMaterial;
        }
    }
}
