﻿using Aura.Models;
using Aura.Script;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Linq;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for EventManager.xaml
    /// </summary>
    public partial class PersonaEventManager : Window
    {
        Persona? Persona { get; set; }

        PersonaEvent? CurrentData { get; set; }

        AuraScriptParser Parser { get; } = new AuraScriptParser();

        public PersonaEventManager()
        {
            InitializeComponent();
            rtxScript.SpellCheck.IsEnabled = false;
            timerSpellcheck.Tick += TimerSpellcheck_Tick;
            timerSpellcheck.Start();
        }


        public void LoadPersona(Persona persona)
        {
            Persona = persona;
            ReloadList();
        }

        void ReloadList()
        {
            lvData.Items.Clear();
            if (Persona == null) return;
            Persona.Events.Sort((a, b) => a.Name.CompareTo(b.Name));
            foreach (var item in Persona.Events)
            {
                lvData.Items.Add(item);
            }
            LoadValues();
        }

        private void LoadValues()
        {
            ClearForm();
            if (lvData.SelectedValue is PersonaEvent data)
            {
                CurrentData = data;
                txName.Text = data.Name;
                cxActive.IsChecked = data.Active;
                var doc = MakeDocument();
                var spl = data.Script.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                foreach (var s in spl)
                {
                    var par = new Paragraph();
                    var run = new Run(s);
                    par.Inlines.Add(run);
                    doc.Blocks.Add(par);
                }
                rtxScript.Document = doc;
            }
        }

        private void ClearForm()
        {
            txName.Clear();
            cxActive.IsChecked = true;
            rtxScript.Document = MakeDocument();
        }

        private FlowDocument MakeDocument()
        {
            var doc = new FlowDocument();
            doc.FontFamily = new FontFamily("Consolas");
            doc.FontSize = 14;
            return doc;
        }

        private void PerformEdit()
        {
            if (lvData.SelectedValue is PersonaEvent)
            {
                LoadValues();
                grData.IsEnabled = true;
                grList.IsEnabled = false;
            }
        }

        private void lvData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadValues();
        }
        private void CommandAlways_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClearForm();
            CurrentData = null;
            grData.IsEnabled = true;
            grList.IsEnabled = false;
            txName.Focus();
        }

        private void lvData_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PerformEdit();
        }

        private void CommandClose_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClearForm();
            CurrentData = null;
            grData.IsEnabled = false;
            grList.IsEnabled = true;
            LoadValues();
        }

        private void CommandEdit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvData.SelectedValue is PersonaEvent;
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (lvData.SelectedValue is PersonaEvent ev && Persona != null)
            {
                if (MessageBox.Show("Are you sure to delete this event?\r\n" + ev.Name, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                {
                    return;
                }
                Persona.Events.Remove(ev);
                ReloadList();
            }
        }

        private void CommandCommit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = grData.IsEnabled;
        }

        private void CommandSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var data = CurrentData ?? new PersonaEvent();
            var name = txName.Text.Trim();
            if (string.IsNullOrWhiteSpace(name))
            {
                MessageBox.Show("Name cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Persona.Events.Any(x => x.Name.ToLower() == name.ToLower() && x != data))
            {
                MessageBox.Show($"Event named '{name}' is already exists", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            bool parseSuccess = false;
            string script = "";
            try
            {
                var tr = new TextRange(rtxScript.Document.ContentStart, rtxScript.Document.ContentEnd);
                script = tr.Text;
                var result = Parser.Check(script);
                if (result == null || !result.IsSuccess)
                {
                    if (MessageBox.Show($"The script contains error, event will be disabled.\r\nContinue?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                        return;
                }
                else
                {
                    parseSuccess = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error when parsing the script: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            data.Name = name;
            data.Script = script;
            data.Active = (cxActive.IsChecked ?? false) && parseSuccess;

            if (!Persona.Events.Contains(data))
                Persona.Events.Add(data);
            ClearForm();
            CurrentData = null;
            grData.IsEnabled = false;
            grList.IsEnabled = true;
            ReloadList();
        }

        DispatcherTimer timerSpellcheck = new DispatcherTimer()
        {
            Interval = new TimeSpan(0, 0, 0, 1)
        };
        DateTime? LastUpdateRequested { get; set; }
        private void rtxScript_TextChanged(object sender, TextChangedEventArgs e)
        {
            LastUpdateRequested = DateTime.Now;
        }

        private void TimerSpellcheck_Tick(object? sender, EventArgs e)
        {
            if (LastUpdateRequested == null) return;
            if (!grData.IsEnabled)
            {
                txParserOutput.Clear();
                return;
            }
            var delta = DateTime.Now - LastUpdateRequested.Value;
            if (delta.TotalSeconds < 1) return;
            LastUpdateRequested = null;
            try
            {
                var tr = new TextRange(rtxScript.Document.ContentStart, rtxScript.Document.ContentEnd);
                var script = tr.Text;
                tr.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.White));
                var tokens = Parser.Lexer.Tokenize(script);
                var result = Parser.Check(script);

                if (result != null)
                {
                    txParserOutput.Text = string.Join(Environment.NewLine, result.Entries.Select(x => x.ToString()));
                }
                else
                {
                    txParserOutput.Clear();
                }

                var colorCodes = new Dictionary<List<AuraScriptTokenType>, SolidColorBrush>()
            {
                {
                    new List<AuraScriptTokenType>()
                    {
                        AuraScriptTokenType.ConditionClause,
                        AuraScriptTokenType.TriggerClause,
                        AuraScriptTokenType.BeginActions,
                        AuraScriptTokenType.For,
                        AuraScriptTokenType.After,
                        AuraScriptTokenType.Play,
                        AuraScriptTokenType.In,
                        AuraScriptTokenType.Random,
                        AuraScriptTokenType.If,
                        AuraScriptTokenType.Then,
                        AuraScriptTokenType.Else,
                        AuraScriptTokenType.ElseIf,
                        AuraScriptTokenType.End,
                    },
                    new SolidColorBrush(Colors.LightSkyBlue)
                },
                {
                    new List<AuraScriptTokenType>()
                    {
                        AuraScriptTokenType.TagIdentifier,
                        AuraScriptTokenType.RegionIdentifier,
                        AuraScriptTokenType.EventIdentifier,
                        AuraScriptTokenType.AudioIdentifier,
                    },
                    new SolidColorBrush(Colors.Cyan)
                },
                {
                    new List<AuraScriptTokenType>()
                    {
                        AuraScriptTokenType.BooleanLiteral,
                        AuraScriptTokenType.NumberLiteral,
                        AuraScriptTokenType.TimeInterval,
                    },
                    new SolidColorBrush(Colors.Honeydew)
                },
                {
                    new List<AuraScriptTokenType>()
                    {
                        AuraScriptTokenType.StringLiteral,
                    },
                    new SolidColorBrush(Colors.Orange)
                },
                {
                    new List<AuraScriptTokenType>()
                    {
                        AuraScriptTokenType.Comment,
                    },
                    new SolidColorBrush(Colors.Lime)
                },
            };

                foreach (var token in tokens)
                {
                    var key = colorCodes.Keys.Where(x => x.Contains(token.Type)).FirstOrDefault();
                    if (key != null)
                    {
                        var cs = rtxScript.Document.ContentStart;
                        var selStart = token.Index - (token.Line - 1) * 2;
                        tr = new TextRange(GetPoint(cs, selStart), GetPoint(cs, selStart + token.Length));
                        var str = tr.Text;
                        tr.ApplyPropertyValue(TextElement.ForegroundProperty, colorCodes[key]);
                    }
                }
            }
            catch { }
        }
        private static TextPointer GetPoint(TextPointer start, int x)
        {
            var ret = start;
            var i = 0;
            while (i < x && ret != null)
            {
                if (ret.GetPointerContext(LogicalDirection.Backward) == TextPointerContext.Text ||
                    ret.GetPointerContext(LogicalDirection.Backward) == TextPointerContext.None)
                    i++;
                if (ret.GetPositionAtOffset(1, LogicalDirection.Forward) == null)
                    return ret;
                ret = ret.GetPositionAtOffset(1, LogicalDirection.Forward);
            }
            return ret ?? start;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            timerSpellcheck.Stop();
        }
    }
}
