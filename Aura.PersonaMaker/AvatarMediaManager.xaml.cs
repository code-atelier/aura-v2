﻿using Aura.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarMediaManager.xaml
    /// </summary>
    public partial class AvatarMediaManager : Window
    {
        TreeViewItem? tviAudio;
        TreeViewItem? tviImage;
        TreeViewItem? tviVideo;
        Persona? Persona { get; set; }
        MediaFile? CurrentData { get; set; }

        string? TempMediaFilePath { get; set; }

        public AvatarMediaManager()
        {
            InitializeComponent();
            GenerateTreeView();
        }

        public void LoadPersona(Persona persona)
        {
            Persona = persona;
            GenerateTreeView();
        }

        private void GenerateTreeView()
        {
            string? lastScrRef = GetSelected()?.ToString();
            tvData.Items.Clear();
            tviImage = new TreeViewItem()
            {
                Header = "Image Files",
                FontWeight = FontWeights.Bold,
                Foreground = new SolidColorBrush(Colors.DarkRed),
                IsExpanded = true,
            };
            tvData.Items.Add(tviImage);
            tviAudio = new TreeViewItem()
            {
                Header = "Audio Files",
                FontWeight = FontWeights.Bold,
                Foreground = new SolidColorBrush(Colors.DarkGreen),
                IsExpanded = true,
            };
            tvData.Items.Add(tviAudio);
            tviVideo = new TreeViewItem()
            {
                Header = "Video Files",
                FontWeight = FontWeights.Bold,
                Foreground = new SolidColorBrush(Colors.DarkBlue),
                IsExpanded = true,
            };
            tvData.Items.Add(tviVideo);
            if (Persona != null)
            {
                foreach (var audio in Persona.AudioFiles.OrderBy(x => x.ToString()))
                {
                    var selected = lastScrRef != null && audio.ToString() == lastScrRef;
                    var tvi = new TreeViewItem()
                    {
                        Header = audio.ToString(),
                        FontWeight = FontWeights.Normal,
                        Tag = audio,
                        IsSelected = selected,
                    };
                    tviAudio.Items.Add(tvi);
                }
            }
        }

        private void tvData_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PerformEdit();
        }

        private MediaFile? GetSelected()
        {
            if (tvData?.SelectedItem is TreeViewItem tvi && tvi.Tag is MediaFile mf)
                return mf;
            return null;
        }

        private void CommandAlways_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandEdit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = GetSelected() != null;
        }

        private void CommandCommit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CurrentData != null;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Dictionary<string, string[]> mediaExts = new Dictionary<string, string[]>()
            {
                {
                    "Image",
                    new string[] { ".png"}
                },
                {
                    "Audio",
                    new string[] { ".ogg", ".wav", ".mp3"}
                },
                {
                    "Video",
                    new string[] { ".mp4"}
                },
            };
            var ofd = new OpenFileDialog();
            ofd.Filter = "Media Files|" + string.Join(";", mediaExts.Values.SelectMany(x => x.Select(w => "*" + w))) + "|" + string.Join("|", mediaExts.Select(x => x.Key + " Files|" + string.Join(";", x.Value.Select(w => "*" + w))));
            if (ofd.ShowDialog() == true)
            {
                var ext = System.IO.Path.GetExtension(ofd.FileName)?.ToLower() ?? "";
                if (!mediaExts.Values.Any(x => x.Contains(ext)))
                {
                    MessageBox.Show("Invalid media extension", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                var type = mediaExts.FirstOrDefault(x => x.Value.Contains(ext)).Key;
                try
                {
                    switch (type)
                    {
                        case "Audio":
                            CurrentData =
                                new AudioFile()
                                {
                                    Id = Guid.NewGuid(),
                                    Extension = ext,
                                    BinaryData = File.ReadAllBytes(ofd.FileName),
                                    Category = AudioCategory.Voice,
                                    Reference = Util.SanitizeStrictId(System.IO.Path.GetFileNameWithoutExtension(ofd.FileName)) ?? "newaudio"
                                };
                            break;
                        case "Video":
                        case "Image":
                        default:
                            MessageBox.Show("Invalid media extension", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                LoadData(CurrentData, true);
                grList.IsEnabled = false;
                gbInfo.IsEnabled = true;
                grControl.IsEnabled = true;
            }
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void PerformEdit()
        {
            if (Persona != null && GetSelected() is MediaFile mf)
            {
                CurrentData = mf;
                LoadData(mf);
                grList.IsEnabled = false;
                gbInfo.IsEnabled = true;
                grControl.IsEnabled = true;
            }
        }

        private void CommandClose_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CurrentData = null;
            grList.IsEnabled = true;
            gbInfo.IsEnabled = false;
            grControl.IsEnabled = false;
            DeletePreview();
        }

        private void DeletePreview()
        {
            mePreview.Stop();
            btnPlay.Content = "4";
            isPaused = true;
            mePreview.Source = null;
            if (File.Exists(TempMediaFilePath))
            {
                try
                {
                    File.Delete(TempMediaFilePath);
                    TempMediaFilePath = null;
                }
                catch { }
            }
        }

        private void LoadPreview(MediaFile mf, bool autoPlay = false)
        {
            mePreview.Source = null;
            grControlPreview.IsEnabled = false;
            btnPlay.Content = "4";
            isPaused = true;
            DeletePreview();
            if (mf.BinaryData == null)
            {
                return;
            }
            var tempDir = "temp";
            try
            {
                if (!Directory.Exists(tempDir))
                {
                    Directory.CreateDirectory(tempDir);
                    var di = new DirectoryInfo(tempDir);
                    di.Attributes |= FileAttributes.Hidden;
                }
                TempMediaFilePath = System.IO.Path.Combine(tempDir, mf.FileName);
                File.WriteAllBytes(TempMediaFilePath, mf.BinaryData);
                try
                {
                    mePreview.Stop();
                    mePreview.Source = new Uri(System.IO.Path.GetFullPath(TempMediaFilePath));
                    grControlPreview.IsEnabled = true;
                    if (autoPlay)
                    {
                        mePreview.Play();
                        btnPlay.Content = ";";
                        isPaused = false;
                    }
                    else
                    {
                        btnPlay.Content = "4";
                        isPaused = true;
                    }
                }
                catch
                {
                    DeletePreview();
                }
            }
            catch
            {

            }
        }

        private void tvData_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            LoadData(GetSelected());
        }

        private void CommandSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null || CurrentData == null) return;
            var data = CurrentData;
            var reference = Util.SanitizeStrictId(txReference.Text);
            if (string.IsNullOrWhiteSpace(reference))
            {
                MessageBox.Show("Reference cannot be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            data.Reference = reference;
            if (CurrentData is AudioFile af)
            {
                if (Persona.AudioFiles.Any(x => x.Reference.ToLower() == reference.ToLower() && x != data))
                {
                    MessageBox.Show($"Audio file '{reference}' is already exists", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (cbCategory.SelectedItem is AudioCategory ac)
                {
                    af.Category = ac;
                }
                else
                {
                    MessageBox.Show($"Audio category must be specified", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!Persona.AudioFiles.Contains(af))
                {
                    Persona.AudioFiles.Add(af);
                    Persona.AudioFiles.Sort((a, b) => a.Reference.CompareTo(b.Reference));
                }
            }

            CurrentData = null;
            grList.IsEnabled = true;
            gbInfo.IsEnabled = false;
            grControl.IsEnabled = false;
            GenerateTreeView();
        }

        private void LoadData(MediaFile? data, bool autoPlay = false)
        {
            txReference.Clear();
            txScriptReference.Clear();
            cbCategory.Items.Clear();
            lbFileSize.Content = "Size: -";
            DeletePreview();
            if (data == null)
            {
                return;
            }
            txReference.Text = data.Reference;
            txScriptReference.Text = data.ToString();
            lbFileSize.Content = "Size: " + Util.FormatByteSize(data.BinaryData?.Length);
            if (data != null)
            {
                if (data is AudioFile af)
                {
                    var enumVals = Enum.GetValues(typeof(AudioCategory));
                    foreach (var enumVal in enumVals)
                    {
                        cbCategory.Items.Add(enumVal);
                    }
                    try
                    {
                        cbCategory.SelectedValue = af.Category;
                    }
                    catch { }
                }
                LoadPreview(data, autoPlay);
            }
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona != null && GetSelected() is MediaFile mf)
            {
                if (MessageBox.Show("Are you sure to delete this data?\r\n" + mf.ToString() + "?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes) return;
                if (mf is AudioFile af)
                {
                    Persona.AudioFiles.Remove(af);
                    GenerateTreeView();
                }
            }
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mePreview.Stop();
                btnPlay.Content = "4";
                isPaused = true;
            }
            catch { }
        }

        bool isPaused = true;
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (isPaused)
                {
                    mePreview.Play();
                    btnPlay.Content = ";";
                    isPaused = false;
                }
                else
                {
                    mePreview.Pause();
                    btnPlay.Content = "4";
                    isPaused = true;
                }
            }
            catch { }
        }

        private void mePreview_MediaEnded(object sender, RoutedEventArgs e)
        {
            try
            {
                mePreview.Stop();
                btnPlay.Content = "4";
                isPaused = true;
            }
            catch { }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            DeletePreview();
        }

        private void txReference_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (gbInfo.IsEnabled && CurrentData != null)
            {
                txScriptReference.Text = CurrentData.BuildScriptReference(Util.SanitizeStrictId(txReference.Text));
            }
        }
    }
}
