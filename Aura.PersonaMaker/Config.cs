﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.PersonaMaker
{
    public class Config
    {
        [JsonPropertyName("lastEditedPersona")]
        public string? LastEditedPersona { get; set; } 
    }
}
