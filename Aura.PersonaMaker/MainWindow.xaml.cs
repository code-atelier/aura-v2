﻿using Aura.Models;
using Aura.WPF;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Path = System.IO.Path;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string ConfigPath = "config.json";

        private SpriteManager SpriteManager { get; } = new SpriteManager();

        public static RoutedCommand ReopenPersona { get; } = new RoutedCommand();
        public static RoutedCommand DisplayWindow { get; } = new RoutedCommand();
        public static RoutedCommand SpriteManagerWindow { get; } = new RoutedCommand();
        public static RoutedCommand MaterialManagerWindow { get; } = new RoutedCommand();
        public static RoutedCommand LayerManagerWindow { get; } = new RoutedCommand();
        public static RoutedCommand RegionManagerWindow { get; } = new RoutedCommand();
        public static RoutedCommand SetManagerWindow { get; } = new RoutedCommand();
        public static RoutedCommand ScheduleManagerWindow { get; } = new RoutedCommand();
        public static RoutedCommand EventManagerWindow { get; } = new RoutedCommand();
        public static RoutedCommand MediaManagerWindow { get; } = new RoutedCommand();

        public MainWindow()
        {
            InitializeComponent();
            SetTitle("");
            Config = new Config();
            try
            {
                if (File.Exists(ConfigPath))
                {
                    var json = File.ReadAllText(ConfigPath);
                    Config = JsonSerializer.Deserialize<Config>(json) ?? new Config();
                }
            }
            catch { }
            LastPersonaPath = Config.LastEditedPersona;
            TagChangeDelayer = new DispatcherTimer();
            TagChangeDelayer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            TagChangeDelayer.Tick += TagChangeDelayer_Tick;
            TagChangeDelayer.Start();
        }

        private void SetTitle(string? title)
        {
            title = title ?? SavePath;
            Title = "Persona Maker v" + PersonaMakerStatic.Version + (!string.IsNullOrWhiteSpace(title) ? " - " + title + (HasChanges ? "*" : "") : "");
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (HasChanges && !ConfirmClose("create new persona")) return;
            NewPersona();
        }

        private void CommandNew_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandStop_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void CommandStop_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandOpen_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (HasChanges && !ConfirmClose("open persona")) return;
            var ofd = new OpenFileDialog();
            ofd.Filter = "Aura v2 Persona File|*.a2p";
            if (ofd.ShowDialog() == true)
            {
                OpenPersona(ofd.FileName);
            }
        }

        private void CommandOpen_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SavePersona();
        }

        private void CommandSaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SavePersona(true);
        }

        private void CommandClose_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClosePersona();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (HasChanges && !ConfirmClose("exit application")) e.Cancel = true;
        }

        private void CommandDisplayWindow_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new AvatarDisplay();
            w.LoadAvatar(Persona.Avatar);
            if (w.ShowDialog() == true)
            {
                HasChanges = true;
                SetTitle(null);
                LoadPersona();
            }
        }

        private void CanExecuteWhenPersonaIsLoaded(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Persona != null;
        }

        private void CommandSpriteManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new AvatarSpriteManager();
            w.LoadAvatar(Persona.Avatar);
            w.ShowDialog();
            HasChanges = true;
            SetTitle(null);
            LoadPersona();
        }

        private void CommandMaterialManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new AvatarMaterialManager();
            w.LoadAvatar(Persona.Avatar);
            w.ShowDialog();
            HasChanges = true;
            SetTitle(null);
            LoadPersona();
        }

        private void CommandReopenLastPersona_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (LastPersonaPath != null)
            {
                if (HasChanges && !ConfirmClose("open last edited persona")) return;
                OpenPersona(LastPersonaPath);
            }
        }

        private void CommandReopenLastPersona_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = LastPersonaPath != null && File.Exists(LastPersonaPath);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SaveConfig();
        }

        private void CommandLayerManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new AvatarLayerManager();
            w.LoadAvatar(Persona.Avatar);
            w.ShowDialog();
            HasChanges = true;
            SetTitle(null);
            LoadPersona();
        }

        private void btnEditPersonaInfo_Click(object sender, RoutedEventArgs e)
        {
            if (Persona != null)
            {
                var w = new PersonaInfoEditor();
                w.LoadPersona(Persona);
                if (w.ShowDialog() == true)
                {
                    HasChanges = true;
                    SetTitle(null);
                    LoadPersona();
                }
            }
        }

        private void CommandRegionManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new RegionManager();
            w.LoadAvatar(Persona.Avatar);
            w.ShowDialog();
            HasChanges = true;
            SetTitle(null);
            LoadPersona();
        }

        private void CommandSetManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new AvatarSetManager();
            w.LoadAvatar(Persona.Avatar);
            w.ShowDialog();
            HasChanges = true;
            SetTitle(null);
            LoadPersona();
        }

        private void avaCanvas_RegionMouseEnter(object sender, MouseEventArgs e)
        {
            if (sender is Region reg)
            {
                lbRegion.Content = "Region: " + reg.Reference;
            }
            else
            {
                lbRegion.Content = "Region: -";
            }
        }

        private void avaCanvas_RegionMouseLeave(object sender, MouseEventArgs e)
        {
            lbRegion.Content = "Region: -";
        }

        private void avaCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {

                try
                {
                    var tempDir = Util.GetTempDirectory("personamaker.clipboard");
                    var files = Directory.EnumerateFiles(tempDir, "*", SearchOption.AllDirectories);
                    foreach (var file in files)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch
                        {
                        }
                    }
                    var tempFile = Path.Combine(tempDir, Guid.NewGuid().ToString("n") + ".png");
                    bool success;
                    using (var fo = File.Create(tempFile))
                    {
                        success = avaCanvas.WriteAvatar(fo);
                    }
                    if (success)
                    {
                        var dataObject = new DataObject();
                        dataObject.SetData(DataFormats.FileDrop, new string[] { tempFile });
                        DragDrop.DoDragDrop(avaCanvas, dataObject, DragDropEffects.All);
                    }
                }
                catch { }
            }
        }

        private void CommandScheduleManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new PersonaScheduleManager();
            w.LoadPersona(Persona);
            w.ShowDialog();
            HasChanges = true;
            SetTitle(null);
            LoadPersona();
        }

        private void btnLoadTags_Click(object sender, RoutedEventArgs e)
        {
            if (cbSchedule.SelectedItem is ScheduleEntry schedule)
            {
                txSimTags.Text = string.Join("\r\n", schedule.Tags.Select(x => x.Tag + ":" + x.Weight)) + "\r\n";
            }
        }

        DateTime? LastChange { get; set; }
        DispatcherTimer TagChangeDelayer { get; }
        private void txSimTags_TextChanged(object sender, TextChangedEventArgs e)
        {
            LastChange = DateTime.Now;
            txSimTags.Foreground = new SolidColorBrush(Colors.Crimson);
        }

        private void TagChangeDelayer_Tick(object? sender, EventArgs e)
        {
            if (LastChange == null) return;
            var delta = DateTime.Now - LastChange.Value;
            if (delta.TotalSeconds >= 0.5)
            {
                LastChange = null;
                avaCanvas.ActiveTags.BeginUpdate();
                avaCanvas.ActiveTags.Clear();
                txSimTags.Foreground = new SolidColorBrush(Colors.Blue);
                foreach (var kv in avaCanvas.MaterialOverride)
                {
                    avaCanvas.MaterialOverride[kv.Key] = null;
                }
                UpdateMaterialTreeView();
                var lines = txSimTags.Text.Split('\n', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in lines)
                {
                    var spl = line.Split(':');
                    var tag = spl[0];
                    double weight = 1;
                    if (spl.Length > 1 && double.TryParse(spl[1], out var v))
                    {
                        weight = v;
                    }
                    avaCanvas.ActiveTags.Add(tag, weight);
                }
                avaCanvas.ActiveTags.EndUpdate();
            }
        }

        private void CommandEventManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new PersonaEventManager();
            w.LoadPersona(Persona);
            w.ShowDialog();
            HasChanges = true;
            SetTitle(null);
            LoadPersona();
        }

        private void CommandMediaManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Persona == null) return;
            var w = new AvatarMediaManager();
            w.LoadPersona(Persona);
            w.ShowDialog();
            HasChanges = true;
            SetTitle(null);
            LoadPersona();
        }
    }
}
