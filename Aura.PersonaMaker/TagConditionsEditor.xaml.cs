﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for TagEditor.xaml
    /// </summary>
    public partial class TagEditor : Window
    {
        public TagEditor()
        {
            InitializeComponent();
        }

        protected TagCondition? TagCondition { get; private set; }

        public void LoadTagCondition(TagCondition tagCondition, string? title = null)
        {
            if (!string.IsNullOrEmpty(title))
            {
                Title = "Tag Conditions - " + title;
            }
            else
            {
                Title = "Tag Conditions";
            }
            TagCondition = tagCondition;
            txBias.Text = TagCondition.Bias.ToString();
            ReloadTags();
        }

        private void ReloadTags()
        {
            lbTags.Items.Clear();
            if (TagCondition != null)
            {
                foreach (var wt in TagCondition.Tags)
                {
                    lbTags.Items.Add(wt);
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void CommandNew_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandModify_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lbTags.SelectedItem is WeightedTag;
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var wt = new WeightedTag();
            var w = new TagWeightEditor();
            w.Load(wt, lbTags.Items.OfType<WeightedTag>());
            if (w.ShowDialog() == true)
            {
                lbTags.Items.Add(wt);
            }
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (lbTags.SelectedItem is WeightedTag wt && MessageBox.Show("Are you sure to delete this tag?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                lbTags.Items.Remove(wt);
            }
        }

        private void PerformEdit()
        {
            if (lbTags.SelectedItem is WeightedTag wt)
            {
                var w = new TagWeightEditor();
                w.Load(wt, lbTags.Items.OfType<WeightedTag>());
                if (w.ShowDialog() == true)
                {
                    var list = lbTags.Items.OfType<WeightedTag>().ToList();
                    lbTags.Items.Clear();
                    foreach(var l in list)
                    {
                        lbTags.Items.Add(l);
                    }
                }
            }
        }

        private void lbTags_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                PerformEdit();
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (TagCondition == null) return;
            double bias;
            if (!double.TryParse(txBias.Text, out bias) || bias > 10 || bias < -10)
            {
                MessageBox.Show("Invalid bias value!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            TagCondition.Tags.Clear();
            foreach(var wt in lbTags.Items.OfType<WeightedTag>())
            {
                TagCondition.Tags.Add(wt);
            }
            TagCondition.Bias = bias;
            DialogResult = true;
            Close();
        }
    }
}
