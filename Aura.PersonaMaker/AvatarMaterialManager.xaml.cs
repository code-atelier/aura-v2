﻿using Aura.Models;
using Aura.WPF;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarMaterialManager.xaml
    /// </summary>
    public partial class AvatarMaterialManager : Window
    {
        bool IsSelectionMode = false;

        Avatar? Avatar { get; set; }
        SpriteManager? SpriteManager { get; set; }
        AvatarLayerMaterial? AvatarLayerMaterial { get; set; }

        public static RoutedCommand Export { get; set; } = new RoutedCommand();

        public static RoutedCommand Import { get; set; } = new RoutedCommand();

        public AvatarMaterialManager()
        {
            InitializeComponent();
        }

        public void LoadAvatar(Avatar avatar)
        {
            Avatar = avatar;
            SpriteManager = new SpriteManager();
            foreach (var spr in Avatar.Sprites)
            {
                SpriteManager.Add(spr);
            }
            AvatarLayerMaterial = new AvatarLayerMaterial(avatar, SpriteManager);
            AvatarLayerMaterial.SizeChanged += AvatarLayerMaterial_SizeChanged;
            grPreview.Children.Add(AvatarLayerMaterial);
            LoadMaterials();
        }

        private void AvatarLayerMaterial_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (AvatarLayerMaterial != null)
            {
                grPreview.Width = AvatarLayerMaterial.Width;
                grPreview.Height = AvatarLayerMaterial.Height;
            }
        }

        public bool? ShowSelectionDialog()
        {
            IsSelectionMode = true;
            spExport.Visibility = spManager.Visibility = Visibility.Collapsed;
            spSelector.Visibility = Visibility.Visible;
            return ShowDialog();
        }

        void LoadMaterials()
        {
            lvMaterials.Items.Clear();
            if (Avatar == null) return;
            var lastSelected = lvMaterials.SelectedItem as Material;
            foreach (var mat in Avatar.Materials.Where
                (x => x.Reference?.ToLower().Contains(txSearch.Text.ToLower()) == true
                || x.Id.ToString("n").Contains(txSearch.Text.ToLower()))
                .OrderBy(x => x.Reference))
            {
                lvMaterials.Items.Add(mat);
            }
            if (lastSelected != null)
            {
                lvMaterials.SelectedItem = lastSelected;
            }
        }

        private void CommandNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar == null || SpriteManager == null) return;
            var w = new AvatarMaterialEditor();
            var mat = new Material()
            {
                Id = Guid.NewGuid(),
                Reference = "newmaterial",
                Width = Avatar.Display.CanvasWidth,
                Height = Avatar.Display.CanvasHeight,
            };
            w.LoadMaterial(mat, Avatar, SpriteManager);
            if (w.ShowDialog() == true)
            {
                Avatar.Materials.Add(mat);
                LoadMaterials();
            }
        }

        private void CommandNew_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !IsSelectionMode;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (AvatarLayerMaterial != null)
            {
                try
                {
                    AvatarLayerMaterial.Dispose();
                }
                catch { }
            }
        }

        private void lvMaterials_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lbReso.Content = "Size: ?";
            if (AvatarLayerMaterial == null) return;
            var sel = lvMaterials.SelectedItem as Material;
            AvatarLayerMaterial.SetMaterial(sel?.Id);
            if (sel != null)
            {
                lbReso.Content = $"Size: {sel.Width} x {sel.Height}";
            }
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void PerformEdit()
        {
            if (Avatar == null || SpriteManager == null) return;
            if (lvMaterials.SelectedItem is Material mat)
            {
                var w = new AvatarMaterialEditor();
                w.LoadMaterial(mat, Avatar, SpriteManager);
                if (w.ShowDialog() == true)
                {
                    LoadMaterials();
                }
            }
        }

        private void CommandAnySelected_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !IsSelectionMode && lvMaterials.SelectedItem is Material;
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (lvMaterials.SelectedItem is Material mat && Avatar != null)
            {
                if (MessageBox.Show("Are you sure to delete selected material?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.Yes)
                {
                    Avatar.Materials.Remove(mat);
                    LoadMaterials();
                }
            }
        }

        private void lvMaterials_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (IsSelectionMode)
                {
                    PerformSelect();
                }
                else
                {
                    PerformEdit();
                }
            }
        }

        public List<Material> SelectedMaterials { get; } = new List<Material>();

        private void CommandSelect_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformSelect();
        }

        private void PerformSelect()
        {
            if (IsSelectionMode && lvMaterials.SelectedItem is Material)
            {
                SelectedMaterials.Clear();
                SelectedMaterials.AddRange(lvMaterials.SelectedItems.OfType<Material>());
                DialogResult = true;
                Close();
            }
        }

        private void CommandSelect_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsSelectionMode && lvMaterials.SelectedItem is Material;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (IsSelectionMode)
            {
                DialogResult = false;
                Close();
            }
        }

        private void CommandExport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && lvMaterials.SelectedItem is Material mat)
            {
                var sfd = new SaveFileDialog();
                sfd.FileName = Util.SanitizeFileName(mat.Reference + ".pmat");
                sfd.Filter = "Persona Maker Material|*.pmat";
                sfd.OverwritePrompt = true;
                if (sfd.ShowDialog() == true)
                {
                    try
                    {
                        mat.Save(sfd.FileName ?? "", Avatar);
                        MessageBox.Show("Material exported successfully!", "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void CommandExport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !IsSelectionMode && lvMaterials.SelectedItem is Material;
        }

        private void CommandImport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar != null && SpriteManager != null)
            {
                var ofd = new OpenFileDialog();
                ofd.Filter = "Persona Maker Material|*.pmat";
                if (ofd.ShowDialog() == true)
                {
                    try
                    {
                        var mat = Material.Load(ofd.FileName, Avatar);
                        LoadMaterials();
                        foreach(var sprite in Avatar.Sprites)
                        {
                            if (!SpriteManager.Contains(sprite))
                                SpriteManager.Add(sprite);
                        }
                        MessageBox.Show("Material imported successfully!", "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void CommandImport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !IsSelectionMode;
        }
    }
}
