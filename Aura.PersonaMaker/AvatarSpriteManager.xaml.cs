﻿using Aura.Models;
using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Aura.PersonaMaker
{
    /// <summary>
    /// Interaction logic for AvatarSpriteManager.xaml
    /// </summary>
    public partial class AvatarSpriteManager : Window
    {
        bool _selectionMode = false;
        public bool SelectionMode
        {
            get => _selectionMode; set
            {
                _selectionMode = value;
                spManager.Visibility = value ? Visibility.Collapsed : Visibility.Visible;
                spSelector.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                lvSprites.SelectionMode = value ? System.Windows.Controls.SelectionMode.Extended : System.Windows.Controls.SelectionMode.Single;
            }
        }

        public Size? ImageSizeFilter { get; set; }

        private Avatar? Avatar { get; set; }
        public AvatarSpriteManager()
        {
            InitializeComponent();
        }

        public void LoadAvatar(Avatar avatar)
        {
            Avatar = avatar;
            LoadSprites();
        }

        private void LoadSprites()
        {
            var selectedSprite = lvSprites.SelectedItem as Sprite;
            lvSprites.Items.Clear();
            if (Avatar != null)
            {
                foreach (var sprite in Avatar.Sprites.
                    Where(x => 
                    (x.Reference?.ToLower().Contains(txSearch.Text) == true 
                    || x.Id.ToString("n").Contains(txSearch.Text))
                    && (ImageSizeFilter == null || (
                        x.Width == (int)ImageSizeFilter.Value.Width && x.Height == (int)ImageSizeFilter.Value.Height
                    ))
                    ).OrderBy(x => x.Reference))
                {
                    var lvi = new ListViewItem();
                    lvi.Content = sprite.ToString();
                    lvi.Tag = sprite;
                    lvSprites.Items.Add(sprite);
                }
            }
            if (lvSprites.Items.Contains(selectedSprite))
            {
                lvSprites.SelectedItem = selectedSprite;
            }
        }

        private void CommandNew_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            ofd.Filter = "PNG Images|*.png";
            if (ofd.ShowDialog() == true)
            {
                foreach (var file in ofd.FileNames)
                {
                    if (!AddNewSprite(file))
                    {
                        break;
                    }
                }
                LoadSprites();
            }
        }

        private void CommandNew_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !SelectionMode;
        }

        private void CommandEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PerformEdit();
        }

        private void PerformEdit()
        {
            if (Avatar == null || SelectionMode) return;
            if (lvSprites?.SelectedItems?.Count == 1)
            {
                var item = lvSprites.SelectedItems[0];
                if (item is Sprite sprite)
                {
                    var w = new AvatarSpriteEditor();
                    w.LoadSprite(sprite, Avatar.Sprites);
                    w.ShowDialog();
                    LoadSprites();
                    lvSprites.SelectedItem = sprite;
                }
            }
        }

        private void CommandEdit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvSprites.SelectedItems.Count == 1 && !SelectionMode;
        }

        private void CommandDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Avatar == null) return;
            if (lvSprites?.SelectedItems?.Count > 0)
            {
                if (MessageBox.Show("Are you sure to delete selected sprites?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                    return;
                var sprites = lvSprites.SelectedItems.OfType<Sprite>();
                foreach (var sprite in sprites)
                {
                    Avatar.Sprites.Remove(sprite);
                }
                LoadSprites();
            }
        }

        private void CommandDelete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvSprites.SelectedItems.Count > 0 && !SelectionMode;
        }

        private bool AddNewSprite(string path)
        {
            if (Avatar == null) return false;
            var sprite = new Sprite
            {
                Id = Guid.NewGuid(),
                Reference = Util.SanitizeId(System.IO.Path.GetFileNameWithoutExtension(path)),
            };
            try
            {
                using (var fi = File.OpenRead(path))
                {
                    var buf = new byte[fi.Length];
                    fi.Read(buf, 0, buf.Length);
                    sprite.BinaryData = buf;
                }
                var w = new AvatarSpriteEditor();
                w.LoadSprite(sprite, Avatar.Sprites);
                if (w.ShowDialog() == true)
                {
                    Avatar.Sprites.Add(sprite);
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        private void lvSprites_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            grPreview.Width = grPreview.Height = 0;
            imgPreview.Source = null;
            lbReso.Content = "Size: ?";
            if (lvSprites?.SelectedItems?.Count > 0)
            {
                var item = lvSprites.SelectedItems[lvSprites.SelectedItems.Count - 1];
                if (item is Sprite spr)
                {
                    grPreview.Width = spr.Width;
                    grPreview.Height = spr.Height;
                    try
                    {
                        var buf = new byte[spr.BinaryData.Length];
                        Array.Copy(spr.BinaryData, buf, buf.Length);
                        var ms = new MemoryStream(buf);
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.StreamSource = ms;
                        bmp.EndInit();
                        imgPreview.Source = bmp;
                        lbReso.Content = $"Size: {bmp.PixelWidth} x {bmp.PixelHeight}";
                    }
                    catch { }
                }
            }
        }

        private void lvSprites_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (SelectionMode)
                {
                    DoSelection();
                }
                else
                {
                    PerformEdit();
                }
            }
        }

        private void lvSprites_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;
            if (SelectionMode) return;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files?.Length > 0 && files.All(x => x.ToLower().EndsWith(".png")))
                {
                    e.Effects = DragDropEffects.Copy;
                }
            }
        }

        private void lvSprites_Drop(object sender, DragEventArgs e)
        {
            e.Handled = true;
            if (SelectionMode) return;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files?.Length > 0 && files.All(x => x.ToLower().EndsWith(".png")))
                {
                    foreach (var file in files)
                    {
                        if (!AddNewSprite(file))
                        {
                            break;
                        }
                    }
                    LoadSprites();
                }
            }
        }

        private void txSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            LoadSprites();
        }

        private void CommandSelect_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DoSelection();
        }

        private void DoSelection()
        {
            if (lvSprites.SelectedItem is Sprite)
            {
                SelectionResult = lvSprites.SelectedItem as Sprite;
                DialogResult = true;
                Close();
            }
        }

        private void CommandSelect_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = lvSprites?.SelectedIndex >= 0 && SelectionMode;
        }

        public Sprite? SelectionResult { get; private set; }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
