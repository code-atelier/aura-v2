﻿using System.Diagnostics;
using System.IO.Compression;

namespace ServicePacker
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\..\\..\\..\\..");
            Console.WriteLine("Packing released services...");
            var dir = Path.Combine(Directory.GetCurrentDirectory(), "dist", "services", "Release");
            var packDir = Path.Combine(Directory.GetCurrentDirectory(), "dist", "services", "Packs");
            var releaseDir = Path.Combine(Directory.GetCurrentDirectory(), "dist", "release");
            var releaseServiceDir = Path.Combine(Directory.GetCurrentDirectory(), "dist", "release", "services");
            if (Directory.Exists(releaseDir))
            {
                Directory.Delete(releaseDir, true);
                Directory.CreateDirectory(releaseServiceDir);
            }
            if (!Directory.Exists(packDir))
            {
                Directory.CreateDirectory(packDir);
            }
            var files = Directory.EnumerateFiles(packDir);
            foreach (var file in files)
            {
                try
                {
                    File.Delete(file);
                }
                catch { }
            }
            var serviceDirs = Directory.EnumerateDirectories(dir);
            bool hasError = false;
            foreach (var serviceDir in serviceDirs)
            {
                var versionDirs = Directory.EnumerateDirectories(serviceDir);
                var versions = new List<Tuple<Version, string>>();
                foreach (var versionDir in versionDirs)
                {
                    try
                    {
                        var name = Path.GetFileName(versionDir).Substring(1);
                        versions.Add(new Tuple<Version, string>(Version.Parse(name), versionDir));
                    }
                    catch { }
                }
                var maxVer = versions.MaxBy(x => x.Item1);
                if (maxVer != null)
                {
                    var serviceName = Path.GetFileName(serviceDir);
                    var versionName = Path.GetFileName(maxVer.Item2);
                    Console.Write($"Packing {serviceName} {versionName}...");
                    try
                    {
                        var destFileName = Path.Combine(packDir, $"{serviceName}_{versionName}.asvc");

                        if (File.Exists(destFileName))
                        {
                            File.Delete(destFileName);
                        }
                        ZipFile.CreateFromDirectory(maxVer.Item2, destFileName);
                        var releaseFile = Path.Combine(releaseServiceDir, Path.GetFileName(destFileName));
                        File.Copy(destFileName, releaseFile, true);

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("OK");
                        Console.ResetColor();
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(ex.ToString());
                        Console.ResetColor();
                        hasError = true;
                    }
                }
            }

            if (args.Any(x => x.StartsWith("-inno=")) && File.Exists("inno.template.iss"))
            {
                try
                {
                    var setupDir = Path.Combine(Directory.GetCurrentDirectory(), "dist", "setup");
                    if (!Directory.Exists(setupDir)) Directory.CreateDirectory(setupDir);
                    if (Directory.Exists(setupDir))
                    {
                        var oldSetupFiles = Directory.GetFiles(setupDir, "*_setup.exe");
                        foreach (var f in oldSetupFiles)
                        {
                            try
                            {
                                File.Delete(f);
                            }
                            catch { }
                        }
                    }
                    var innoParam = args.First(x => x.StartsWith("-inno="));
                    var ver = innoParam.Length > 6 ? innoParam.Substring(6).Trim() : "";
                    if (ver == "latest" || string.IsNullOrWhiteSpace(ver))
                    {
                        var mainDir = Path.Combine(Directory.GetCurrentDirectory(), "dist", "main", "Release");
                        var dirs = Directory.GetDirectories(mainDir, "v*");
                        Version? maxVer = null;
                        foreach (var d in dirs)
                        {
                            var vstring = Path.GetFileName(d).Substring(1);
                            if (Version.TryParse(vstring, out var v))
                            {
                                if (maxVer == null || maxVer < v) maxVer = v;
                            }
                        }
                        ver = maxVer?.ToString();
                    }
                    if (!Version.TryParse(ver, out _))
                    {
                        throw new Exception("Invalid main app version: " + ver);
                    }
                    Console.ResetColor();
                    Console.Write("Building main app setup (v");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(ver);
                    Console.ResetColor();
                    Console.WriteLine(")...");
                    var content = File.ReadAllText("inno.template.iss");
                    content = content.Replace("{{APP_VERSION}}", ver);
                    File.WriteAllText("inno.iss", content);
                    var p = Process.Start(new ProcessStartInfo()
                    {
                        FileName = "cmd.exe",
                        Arguments = "/c iscc \"" + Path.GetFullPath("inno.iss") + "\"",
                        WindowStyle = ProcessWindowStyle.Hidden,
                    });
                    if (p != null)
                    {
                        p.WaitForExit();
                        if (p.ExitCode != 0)
                        {
                            throw new Exception("Inno setup compilation failed");
                        }
                    }
                    if (Directory.Exists(setupDir))
                    {
                        var oldSetupFiles = Directory.GetFiles(setupDir, "*_setup.exe");
                        if (oldSetupFiles.Length > 0)
                        {
                            var destFileName = oldSetupFiles[0];
                            var releaseFile = Path.Combine(releaseDir, Path.GetFileName(destFileName));
                            File.Copy(destFileName, releaseFile, true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.ResetColor();
                    Console.Write("An error has occured when compiling setup: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    Console.ResetColor();
                    hasError = true;
                }
            }

            if (hasError)
            {
                Console.WriteLine("Error occured during packing. Press any key to exit.");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Completed successfully.");
                Environment.Exit(0);
            }
        }
    }
}