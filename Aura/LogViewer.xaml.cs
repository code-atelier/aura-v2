﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura
{
    /// <summary>
    /// Interaction logic for LogViewer.xaml
    /// </summary>
    public partial class LogViewer : Window
    {
        DispatcherTimer timer;
        ObservableCollection<ServiceLogLine> ServiceLogLines { get; } = new ObservableCollection<ServiceLogLine>();
        public LogViewer()
        {
            InitializeComponent();
            timer = new DispatcherTimer(DispatcherPriority.Normal);
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += Timer_Tick;
            dataGrid.ItemsSource = ServiceLogLines;
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            if (Log == null)
            {
                timer.Stop();
                return;
            }
            if (cxAutoReload.IsChecked != true)
            {
                return;
            }
            RefreshLog();
        }

        ServiceLog? Log { get; set; }

        public void LoadLog(ServiceLog log)
        {
            Log = log;
            timer.Start();
            RefreshLog();
        }

        private async void RefreshLog()
        {
            if (Log == null) return;
            timer.Stop();

            try
            {
                ServiceLogLevel minLevel = ServiceLogLevel.Info;
                DateTime? startTime = DateTime.Now.AddMinutes(-5);

                await Dispatcher.InvokeAsync(() =>
                {
                    minLevel = (ServiceLogLevel)cbLevel.SelectedIndex;
                    startTime =
                        cbTime.SelectedIndex == 0 ? DateTime.Now.AddMinutes(-5)
                        : cbTime.SelectedIndex == 1 ? DateTime.Now.AddMinutes(-15)
                        : cbTime.SelectedIndex == 2 ? DateTime.Now.AddMinutes(-30)
                        : cbTime.SelectedIndex == 3 ? DateTime.Now.AddHours(-1)
                        : cbTime.SelectedIndex == 4 ? DateTime.Now.AddHours(-12)
                        : cbTime.SelectedIndex == 5 ? DateTime.Now.AddHours(-24)
                        : null;
                });

                List<ServiceLogLine> lines = new List<ServiceLogLine>();
                await Task.Run(() =>
                {
                    lines = Log.GetLines(minLevel, startTime);
                });

                Dispatcher.Invoke(() =>
                {
                    var changed = false;
                    foreach (var item in lines)
                    {
                        if (!ServiceLogLines.Contains(item))
                        {
                            ServiceLogLines.Add(item);
                            changed = true;
                        }
                    }
                    var toRemove = new List<ServiceLogLine>();
                    foreach (var item in ServiceLogLines)
                    {
                        if (item.Level < minLevel || (startTime != null && item.Timestamp < startTime.Value))
                        {
                            toRemove.Add(item);
                        }
                    }
                    foreach (var item in toRemove)
                    {
                        ServiceLogLines.Remove(item);
                        changed = true;
                    }
                    var sorted = dataGrid.Columns.FirstOrDefault(x => x.SortDirection != null);
                    if (sorted != null)
                    {
                        var performSortMethod = typeof(DataGrid)
                                    .GetMethod("PerformSort",
                                               BindingFlags.Instance | BindingFlags.NonPublic);
                        performSortMethod?.Invoke(dataGrid, new[] { sorted });
                        performSortMethod?.Invoke(dataGrid, new[] { sorted });
                    }
                    if (changed && dataGrid.Items.Count > 0)
                    {
                        try
                        {
                            dataGrid.ScrollIntoView(dataGrid.Items[dataGrid.Items.Count - 1]);
                        }
                        catch { }
                    }
                });
            }
            finally
            {
                timer.Start();
            }
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            timer.Stop();
        }
    }
}
