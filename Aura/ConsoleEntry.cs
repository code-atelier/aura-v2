﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Sources;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Aura
{
    public class ConsoleEntry
    {
        public static Color AuraBackgroundColor { get; } = Colors.White;
        public static Color UserBackgroundColor { get; } = Colors.Honeydew;
        public static Color ConsoleBackgroundColor { get; } = Colors.Black;
        public static Color AuraBorderColor { get; } = Color.FromRgb(0x99, 0x99, 0x99);
        public static Color UserBorderColor { get; } = Color.FromRgb(0x99, 0xbb, 0x99);
        public static Color ConsoleBorderColor { get; } = Colors.Gray;

        public string Text { get; set; } = string.Empty;

        public bool IsErrorMessage { get; set; }
        public bool IsConsoleCommand { get; set; }
        public bool SentByAura { get; set; }
        public bool Thinking { get; set; }
        public bool CanFocus { get => SentByAura || !IsConsoleCommand; }

        public ChatbotServiceMessage? ChatbotServiceMessage { get; set; }
        public string? Footer { get => ChatbotServiceMessage?.Timestamp.ToLocalTime().ToString("dd MMM yyy, hh:mm tt") + (ChatbotServiceMessage?.AuthorName != null ? " - " + ChatbotServiceMessage?.AuthorName : "") + (ChatbotServiceMessage?.MessageId != null ? " (ID: " + ChatbotServiceMessage?.MessageId + ")" : ""); }
        public double FooterHeight { get => ChatbotServiceMessage != null ? 22 : 0; }

        public SolidColorBrush ForegroundColor { get => IsErrorMessage ? new SolidColorBrush(Colors.Crimson) : IsConsoleCommand ? new SolidColorBrush(Colors.White) : !Thinking ? new SolidColorBrush(Color.FromArgb(0xff, 0x33, 0x33, 0x33)) : new SolidColorBrush(Colors.Gray); }
        public SolidColorBrush BackgroundColor { get => new SolidColorBrush(IsConsoleCommand ? ConsoleBackgroundColor : SentByAura ? AuraBackgroundColor : UserBackgroundColor); }
        public SolidColorBrush BorderColor { get => new SolidColorBrush(IsConsoleCommand ? ConsoleBorderColor : SentByAura ? AuraBorderColor : UserBorderColor); }
        public FontFamily TextFontFamily { get => IsConsoleCommand ? new FontFamily("Consolas") : new FontFamily(); }
        public double FontSize { get => IsConsoleCommand ? 12 : 14; }

        public HorizontalAlignment HorizontalAlignment { get => SentByAura ? HorizontalAlignment.Left : HorizontalAlignment.Right; }
        public Thickness Margin { get => SentByAura ? new Thickness(5, 5, 100, 5) : new Thickness(100, 5, 5, 5); }
    }

    public static class RichTextDocumentParser
    {
        public static FlowDocument Parse(string text)
        {
            ResetVars();
            var lines = text.Split(new[] { "\r\n", "\n", "\r" }, StringSplitOptions.None);
            // Create a FlowDocument to contain content for the RichTextBox.
            FlowDocument doc = new FlowDocument();

            foreach (var line in lines)
            {
                var block = ParseLine(line);
                if (block != null)
                    doc.Blocks.AddRange(block);
            }

            var finalBlock = Cleanup();
            if (finalBlock != null)
                doc.Blocks.AddRange(finalBlock);

            return doc;
        }

        private static void ResetVars()
        {
            _list.Clear();
            codeStarted = false;
            CodeBlocks.Clear();
        }

        static Stack<List> _list { get; } = new Stack<List>();

        private static IEnumerable<Block>? Cleanup()
        {
            var res = new List<Block>();
            List? finalList = null;
            while (_list.Count > 0)
            {
                finalList = PopList();
            }
            if (finalList != null)
            {
                res.Add(finalList);
            }

            ResetVars();
            return res;
        }

        private static List? PopList()
        {
            if (_list.Count < 1) return null;
            var popped = _list.Pop();
            if (_list.Count < 1) return popped;
            var parentList = _list.Peek();
            if (popped != null)
            {
                parentList.ListItems.LastListItem.Blocks.Add(popped);
            }
            return popped;
        }

        static bool codeStarted = false;
        static List<Paragraph> CodeBlocks { get; } = new List<Paragraph>();

        private static IEnumerable<Block>? ParseLine(string line)
        {
            line = line.Replace("\t", "  ");
            if (codeStarted)
            {
                if (line.Trim().StartsWith("```"))
                {
                    codeStarted = false;
                    var blocks = CodeBlocks.ToArray();
                    CodeBlocks.Clear();
                    return blocks;
                }
                else
                {
                    var pCode = new Paragraph()
                    {
                        Padding = new Thickness(4, 2, 4, 2),
                        Margin = new Thickness(0),
                    };
                    pCode.FontFamily = new FontFamily("Consolas");
                    pCode.FontSize = 11;
                    pCode.Inlines.Add(new Run(line));
                    pCode.Background = new SolidColorBrush(Colors.AliceBlue);
                    CodeBlocks.Add(pCode);
                    return null;
                }
            }
            else
            {
                if (line.Trim().StartsWith("```"))
                {
                    codeStarted = true;
                    CodeBlocks.Clear();
                    return null;
                }
            }
            if (line.TrimStart().StartsWith("- "))
            {
                var nestLevel = line.IndexOf('-') / 2 + 1;
                while (_list.Count < nestLevel)
                {
                    _list.Push(new List()
                    {
                        Margin = new Thickness(0, 0, 0, 4),
                        Padding = new Thickness(8, 0, 0, 0)
                    });
                }
                while (_list.Count > nestLevel)
                {
                    PopList();
                }
                var list = _list.Peek();
                list.MarkerStyle = TextMarkerStyle.Disc;
                list.ListItems.Add(new ListItem(ParseInline(line.TrimStart().Substring(2))));
                return null;
            }

            var res = new List<Block>();

            var popped = PopList();
            while (popped != null)
            {
                var lastPopped = popped;
                popped = PopList();
                if (popped == null)
                {
                    res.Add(lastPopped);
                }
            }

            var p = ParseInline(line);
            p.Margin = new Thickness(0, 0, 0, 4);
            res.Add(p);
            return res;
        }

        private static Paragraph ParseInline(string line)
        {
            var parser = new MarkdownInlineParser();
            return parser.Parse(line);
        }
    }

    public class MarkdownInlineParser
    {
        bool afterEscape = false;
        Queue<char> chars = new Queue<char>();
        List<MarkdownInlineParserSection> Sections { get; } = new List<MarkdownInlineParserSection>();
        Stack<MarkdownInlineParserDecorationType> DecorationStack { get; } = new Stack<MarkdownInlineParserDecorationType>();
        Dictionary<string, MarkdownInlineParserDecorationType> DecorationKeys { get; } = new Dictionary<string, MarkdownInlineParserDecorationType>()
        {
            { "*", MarkdownInlineParserDecorationType.Italic },
            { "_", MarkdownInlineParserDecorationType.Italic_ },
            { "**", MarkdownInlineParserDecorationType.Bold },
            { "__", MarkdownInlineParserDecorationType.Underline },
            { "~~", MarkdownInlineParserDecorationType.Strikethrough },
        };

        public Paragraph Parse(string line)
        {
            chars = new Queue<char>();
            foreach (var c in line) { chars.Enqueue(c); }
            afterEscape = false;
            Sections.Clear();
            DecorationStack.Clear();
            ParseSections();

            var para = new Paragraph();
            var decos = new List<MarkdownInlineParserDecorationType>();
            foreach (var sec in Sections)
            {
                if (sec.Type == MarkdownInlineParserSectionType.Text)
                {
                    if (!string.IsNullOrEmpty(sec.TextContent))
                    {
                        para.Inlines.Add(MakeRun(sec.TextContent, decos));
                    }
                }
                else
                {
                    if (sec.IsOpeningTag && !decos.Contains(sec.Decoration) && sec.Decoration != MarkdownInlineParserDecorationType.None)
                    {
                        decos.Add(sec.Decoration);
                    }
                    else if (sec.IsClosingTag && decos.Contains(sec.Decoration))
                    {
                        decos.Remove(sec.Decoration);
                    }
                }
            }
            return para;
        }

        private Run MakeRun(string text, IEnumerable<MarkdownInlineParserDecorationType> decorations)
        {
            var run = new Run(text);
            if (decorations.Contains(MarkdownInlineParserDecorationType.Italic) || decorations.Contains(MarkdownInlineParserDecorationType.Italic_))
            {
                run.FontStyle = FontStyles.Italic;
            }
            if (decorations.Contains(MarkdownInlineParserDecorationType.Bold))
            {
                run.FontWeight = FontWeights.Bold;
            }
            if (decorations.Contains(MarkdownInlineParserDecorationType.Strikethrough))
            {
                run.TextDecorations.Add(TextDecorations.Strikethrough);
            }
            if (decorations.Contains(MarkdownInlineParserDecorationType.Underline))
            {
                run.TextDecorations.Add(TextDecorations.Underline);
            }
            return run;
        }

        private void ParseSections()
        {
            char c;
            NewSection(MarkdownInlineParserSectionType.Text);
            while (chars.TryDequeue(out c))
            {
                Process(c);
            }
        }

        private void Process(char c)
        {
            char nextC;
            if (!chars.TryPeek(out nextC))
            {
                nextC = '\0';
            }
            string unigram = "" + c;
            string bigram = "" + c + nextC;

            // handle escape
            if (afterEscape)
            {
                var section = GetTextSection();
                section.TextContent += c;
                return;
            }
            if (c == '\\')
            {
                afterEscape = true;
                return;
            }

            if (DecorationKeys.ContainsKey(bigram))
            {
                var deco = DecorationKeys[bigram];
                // if this is the closing tag
                if (DecorationStack.Count > 0 && DecorationStack.Peek() == deco)
                {
                    // pop it
                    DecorationStack.Pop();
                    var section = NewSection(MarkdownInlineParserSectionType.Decoration);
                    section.TextContent = bigram;
                    section.Decoration = deco;
                    section.IsClosingTag = true;
                    chars.Dequeue();
                    return;
                }
                // if not the closing tag of a unigram
                else if (!DecorationKeys.ContainsKey(unigram) || DecorationStack.Count == 0 || DecorationKeys[unigram] != DecorationStack.Peek())
                {
                    DecorationStack.Push(deco);
                    var section = NewSection(MarkdownInlineParserSectionType.Decoration);
                    section.TextContent = bigram;
                    section.Decoration = deco;
                    section.IsOpeningTag = true;
                    chars.Dequeue();
                    return;
                }
            }
            if (DecorationKeys.ContainsKey(unigram))
            {
                var deco = DecorationKeys[unigram];
                // if this is the closing tag
                if (DecorationStack.Count > 0 && DecorationStack.Peek() == deco)
                {
                    // pop it
                    DecorationStack.Pop();
                    var section = NewSection(MarkdownInlineParserSectionType.Decoration);
                    section.TextContent = unigram;
                    section.Decoration = deco;
                    section.IsClosingTag = true;
                    return;
                }
                else
                {
                    DecorationStack.Push(deco);
                    var section = NewSection(MarkdownInlineParserSectionType.Decoration);
                    section.TextContent = unigram;
                    section.Decoration = deco;
                    section.IsOpeningTag = true;
                    return;
                }
            }

            var textSection = GetTextSection();
            textSection.TextContent += c;
        }

        private MarkdownInlineParserSection NewSection(MarkdownInlineParserSectionType type)
        {
            var sec = new MarkdownInlineParserSection(type);
            Sections.Add(sec);
            return sec;
        }

        private MarkdownInlineParserSection GetTextSection()
        {
            if (Sections.LastOrDefault()?.Type == MarkdownInlineParserSectionType.Text)
            {
                return Sections.Last();
            }
            else
            {
                return NewSection(MarkdownInlineParserSectionType.Text);
            }
        }
    }

    public class MarkdownInlineParserSection
    {
        public MarkdownInlineParserSectionType Type { get; set; }

        public MarkdownInlineParserDecorationType Decoration { get; set; }

        public bool IsClosingTag { get; set; }

        public bool IsOpeningTag { get; set; }

        public string TextContent { get; set; } = string.Empty;

        public MarkdownInlineParserSection(MarkdownInlineParserSectionType type)
        {
            Type = type;
        }
    }

    public enum MarkdownInlineParserSectionType
    {
        Text,
        Decoration
    }

    public enum MarkdownInlineParserDecorationType
    {
        None,
        Italic,
        Italic_,
        Bold,
        Underline,
        Strikethrough,
    }
}
