﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura
{
    /// <summary>
    /// Interaction logic for ShutdownWindow.xaml
    /// </summary>
    public partial class ShutdownWindow : Window
    {
        public ShutdownWindow()
        {
            InitializeComponent();
        }

        public ShutdownWindowResult ShutdownResult { get; private set; } = ShutdownWindowResult.Cancel;

        public ShutdownWindowResult ShowShutdownDialog()
        {
            ShowDialog();
            return ShutdownResult;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnRestart_Click(object sender, RoutedEventArgs e)
        {
            ShutdownResult = ShutdownWindowResult.Restart;
            Close();
        }

        private void btnShutdown_Click(object sender, RoutedEventArgs e)
        {
            ShutdownResult = ShutdownWindowResult.Shutdown;
            Close();
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            try
            {
                Close();
            }
            catch { }
        }
    }

    public enum ShutdownWindowResult
    {
        Cancel = 0,
        Shutdown = 1,
        Restart = 2,
    }
}
