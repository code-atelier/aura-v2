﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Aura
{
    /// <summary>
    /// Interaction logic for LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow : Window
    {
        public bool FastClose { get; set; } = false;

        public LoadingWindow()
        {
            InitializeComponent();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            await ServiceManager.InitializeServices();
            var logger = ServiceManager.GetInstance<AuraLogger>();
            logger.Information($"Services loaded ({ServiceManager.Services.Count}): " + Environment.NewLine + string.Join(Environment.NewLine, ServiceManager.Services.Select(x => "- " + x.Info.ToString())));
            logger.Information("Auto-Starting services...");
            await ServiceManager.AutoStartServices();
            allowClose = true;
            if (FastClose)
            {
                Close();
            }
            else
            {
                StartClosing();
            }
        }

        private void StartClosing()
        {
            var fadeOutAnim = new DoubleAnimation()
            {
                From = 1.0,
                To = 0.0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 1)),
                BeginTime = new TimeSpan(0, 0, 0, 1, 500),
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(fadeOutAnim);
            Storyboard.SetTargetProperty(fadeOutAnim, new PropertyPath(Grid.OpacityProperty));

            storyboard.Completed += SplashFadedOut;
            storyboard.Begin(this);
        }

        private void SplashFadedOut(object? sender, EventArgs e)
        {
            Close();
        }

        bool allowClose = false;
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!allowClose)
            {
                e.Cancel = true;
            }
        }
    }
}
