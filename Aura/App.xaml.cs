﻿using Aura.Models;
using System.Windows;

namespace Aura
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public bool FastLoading { get; set; }

        public UriRequest? UriArguments { get; set; }

        public App()
        {
            var loadingWindow = new LoadingWindow();
            loadingWindow.FastClose = FastLoading;
            MainWindow = loadingWindow;
            ShutdownMode = ShutdownMode.OnExplicitShutdown;
            Startup += App_Startup;
        }

        private void App_Startup(object sender, StartupEventArgs e)
        {
            MainWindow.ShowDialog();
            MainWindow = new MainWindow()
            {
                UriArguments = UriArguments,
            };
            MainWindow.Show();
            try
            {
                ShutdownMode = ShutdownMode.OnMainWindowClose;
            }
            catch { }
        }
    }
}
