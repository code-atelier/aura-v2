﻿using Aura.Models.Configs;
using Aura.Services;
using Aura.Services.Passive;
using Aura.WPF.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Message = Aura.Services.Message;
using MessageBox = System.Windows.MessageBox;

namespace Aura.WPF
{
    /// <summary>
    /// Interaction logic for StartMenu.xaml
    /// </summary>
    public partial class StartMenu : Window, ICommandWindow
    {
        ConfigurationService ConfigurationService { get; }
        AuraConfig Config { get; }
        IServiceMessageBus ServiceMessageBus { get; }

        public System.Windows.Controls.Button ShutdownButton { get => btnShutdown; }

        public StartMenu()
        {
            InitializeComponent();
            Opacity = 0;
            lbAuraVersion.Content = "Aura v" + AuraInfo.Version;
            ConfigurationService = ServiceManager.GetInstance<ConfigurationService>();
            ServiceMessageBus = ServiceManager.GetInstance<IServiceMessageBus>();
            Config = ConfigurationService.LoadConfig<AuraConfig>();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var da = new DoubleAnimation()
            {
                From = 0,
                To = 1,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 200))
            };
            var sb = new Storyboard();
            sb.Children.Add(da);
            Storyboard.SetTargetProperty(da, new PropertyPath(OpacityProperty));
            sb.Begin(this);

            var tools = ServiceManager.Tools.Where(x => x.Kind == ToolKind.Tool).OrderBy(x => x.Info.Name);
            var widgets = ServiceManager.Tools.Where(x => x.Kind == ToolKind.Widget).OrderBy(x => x.Info.Name);

            foreach (var tool in tools)
            {
                var scbi = new ScrollableCommandButtonItem()
                {
                    Label = tool.Info.Name,
                    Tag = tool,
                    Tooltip = tool.Info.Description,
                };
                var bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.UriSource = new Uri(tool.Icon);
                bmp.EndInit();
                scbi.Image = bmp;
                scbi.OpenRequested += Scbi_Click;
                scbTools.Items.Add(scbi);
            }

            foreach (var widget in widgets)
            {
                var scbi = new ScrollableCommandButtonItem()
                {
                    Label = widget.Info.Name,
                    Tag = widget,
                    Tooltip = widget.Info.Description,
                };
                var bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.UriSource = new Uri(widget.Icon);
                bmp.EndInit();
                scbi.Image = bmp;
                scbi.OpenRequested += Scbi_Click;
                scbWidgets.Items.Add(scbi);
            }

            LoadShortcuts();
        }

        private void Scbi_Click(object? sender, EventArgs e)
        {
            if (sender is ScrollableCommandButtonItem scbi && scbi.Tag is ITool tool)
            {
                try
                {
                    tool.Show();
                    BeginClose();
                }
                catch { }
            }
        }

        public void BeginClose()
        {
            var da = new DoubleAnimation()
            {
                From = 1,
                To = 0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 200))
            };
            var sb = new Storyboard();
            sb.Children.Add(da);
            Storyboard.SetTargetProperty(da, new PropertyPath(OpacityProperty));
            sb.Completed += Sb_Completed;
            sb.Begin(this);
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            if (preventCloseOnDeactivate) return;
            BeginClose();
        }

        bool allowClose = false;
        private void Sb_Completed(object? sender, EventArgs e)
        {
            ForceClose();
        }

        private void ForceClose()
        {
            allowClose = true;
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!allowClose)
            {
                e.Cancel = true;
            }
        }

        bool preventCloseOnDeactivate = false;
        private void btnShutdown_Click(object sender, RoutedEventArgs e)
        {
            preventCloseOnDeactivate = true;
            try
            {
                ShutdownClicked?.Invoke(this, EventArgs.Empty);
            }
            finally
            {
                preventCloseOnDeactivate = false;
            }
        }

        public void Show(string anchor, double anchorX, double anchorY)
        {
            const int margin = 10;
            if (anchor == InternalTags.Anchor.CommandRight)
            {
                Left = anchorX + margin;
                Top = anchorY - Height / 2;
            }
            else if (anchor == InternalTags.Anchor.CommandTray)
            {
                Left = anchorX - Width;
                Top = anchorY - Height;
                btnMinimize.Content = new Image()
                {
                    Source = new BitmapImage(new Uri(Util.CreateResourceUri("res/img/sign-up.png")))
                };
                btnMinimize.ToolTip = "Restore avatar view";
            }
            else
            {
                // left / origin
                Left = anchorX - Width - margin;
                Top = anchorY - Height / 2;
            }
            Show();
        }

        public event EventHandler? ShutdownClicked;
        public event EventHandler? MinimizeClicked;

        private void Window_Activated(object sender, EventArgs e)
        {
            txSearch.Focus();
            txSearch.SelectAll();
        }

        private void txSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            lbSearch.Visibility = txSearch.Text == "" ? Visibility.Visible : Visibility.Collapsed;

            var lcase = txSearch.Text.Trim().ToLower();
            if (!string.IsNullOrWhiteSpace(lcase))
            {
                scbShortcuts.Items.Filter =
                scbTools.Items.Filter =
                scbWidgets.Items.Filter = (x) => x.Label.ToLower().Contains(lcase);
            }
            else
            {
                scbShortcuts.Items.Filter =
                scbTools.Items.Filter =
                scbWidgets.Items.Filter = null;
            }
        }

        private void btnAddShortcut_Click(object sender, RoutedEventArgs e)
        {
            preventCloseOnDeactivate = true;
            try
            {
                if (sender == btnAddFolderShortcut)
                {
                    // select folder instead
                    var okd = new FolderBrowserDialog();
                    if (okd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        if (!Config.Shortcuts.Any(x => x.Path.ToLower() == okd.SelectedPath.ToLower()))
                        {
                            Config.Shortcuts.Add(new ShortcutRecord(okd.SelectedPath));
                            ConfigurationService.SaveConfig<AuraConfig>();
                        }
                    }
                }
                else
                {
                    var ofd = new OpenFileDialog();
                    ofd.Filter = "All Files|*.*";
                    if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        if (!Config.Shortcuts.Any(x => x.Path.ToLower() == ofd.FileName.ToLower()))
                        {
                            Config.Shortcuts.Add(new ShortcutRecord(ofd.FileName));
                            ConfigurationService.SaveConfig<AuraConfig>();
                        }
                    }
                }
            }
            catch { }
            finally
            {
                Activate();
                LoadShortcuts();
                preventCloseOnDeactivate = false;
            }
        }

        private void LoadShortcuts()
        {
            scbShortcuts.Items.Clear();
            Config.Shortcuts.RemoveAll(x => !x.Exists);
            try
            {
                ConfigurationService.SaveConfig<AuraConfig>();
            }
            catch { }

            var allShortcuts = Config.Shortcuts.ToList();

            var scTopRecent = allShortcuts.Where(x => x.LastAccess != null).OrderBy(x => x.LastAccess).ToList();
            scTopRecent.Reverse();
            while (scTopRecent.Count > 5)
            {
                scTopRecent.RemoveAt(5);
            }
            var scRemaining = allShortcuts.Where(x => !scTopRecent.Contains(x)).ToList();
            scRemaining.Sort();

            var shortcuts = new List<ShortcutRecord>();
            shortcuts.AddRange(scTopRecent);
            shortcuts.AddRange(scRemaining);
            foreach (var sc in shortcuts)
            {
                if (string.IsNullOrWhiteSpace(sc.Name))
                {
                    sc.Name = System.IO.Path.GetFileNameWithoutExtension(sc.Path);
                }
                var scbiShortcut = new ScrollableCommandButtonItem()
                {
                    Label = sc.Name,
                    Tag = sc,
                    Tooltip = sc.Path,
                };
                scbiShortcut.OpenRequested += ScbiShortcut_Open;
                scbiShortcut.OpenFolderRequested += ScbiShortcut_OpenFolder;
                scbiShortcut.DeleteRequested += ScbiShortcut_DeleteRequested;
                scbiShortcut.RenameRequested += ScbiShortcut_RenameRequested;
                try
                {
                    if (sc.IsFile)
                    {
                        var icon = System.Drawing.Icon.ExtractAssociatedIcon(sc.Path);
                        if (icon == null)
                        {
                            throw new Exception("Failed to extract icon");
                        }
                        var bitmap = icon.ToBitmap();
                        var ms = new MemoryStream();
                        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.StreamSource = ms;
                        bmp.EndInit();
                        scbiShortcut.Image = bmp;
                    }
                    else if (sc.IsDirectory)
                    {
                        try
                        {
                            var bmp = new BitmapImage();
                            bmp.BeginInit();
                            bmp.UriSource = new Uri(Util.CreateResourceUri("res/img/folder.png"));
                            bmp.EndInit();
                            scbiShortcut.Image = bmp;
                        }
                        catch { }
                    }
                }
                catch
                {
                    try
                    {
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.UriSource = new Uri(Util.CreateResourceUri("res/img/file-empty.png"));
                        bmp.EndInit();
                        scbiShortcut.Image = bmp;
                    }
                    catch { }
                }
                scbShortcuts.Items.Add(scbiShortcut);
            }
        }

        private void ScbiShortcut_RenameRequested(object? sender, EventArgs e)
        {
            if (sender is ScrollableCommandButtonItem scbi && scbi.Tag is ShortcutRecord sc)
            {
                preventCloseOnDeactivate = true;
                try
                {
                    var ren = Prompt.ShowDialog("Enter new name for the shortcut", "Rename Shortcut", sc.Name);
                    if (string.IsNullOrWhiteSpace(ren))
                    {
                        return;
                    }
                    sc.Name = ren;
                    scbi.Label = sc.Name;
                    LoadShortcuts();
                    Activate();
                    try
                    {
                        ConfigurationService.SaveConfig<AuraConfig>();
                    }
                    catch { }
                }
                finally
                {
                    preventCloseOnDeactivate = false;
                }
            }
        }

        private bool ScbiShortcut_DeleteRequested(object sender, EventArgs e)
        {
            if (sender is ScrollableCommandButtonItem scbi && scbi.Tag is ShortcutRecord sc)
            {
                // ask delete shortcut
                preventCloseOnDeactivate = true;
                try
                {
                    if (MessageBox.Show("Do you want to delete this shortcut?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) != MessageBoxResult.Yes)
                    {
                        return false;
                    }
                    Config.Shortcuts.Remove(sc);
                    LoadShortcuts();
                    Activate();
                    try
                    {
                        ConfigurationService.SaveConfig<AuraConfig>();
                    }
                    catch { }
                    return true;
                }
                finally
                {
                    preventCloseOnDeactivate = false;
                }
            }
            return false;
        }

        private void ScbiShortcut_OpenFolder(object? sender, EventArgs e)
        {
            if (sender is ScrollableCommandButtonItem scbi && scbi.Tag is ShortcutRecord sc)
            {
                sc.LastAccess = DateTime.Now;
                Util.ShowInExplorer(sc.Path);
                try
                {
                    ConfigurationService.SaveConfig<AuraConfig>();
                }
                catch { }
            }
        }

        private void ScbiShortcut_Open(object? sender, EventArgs e)
        {
            if (sender is ScrollableCommandButtonItem scbi && scbi.Tag is ShortcutRecord sc)
            {
                sc.LastAccess = DateTime.Now;
                Util.OpenFile(sc.Path);
                try
                {
                    ConfigurationService.SaveConfig<AuraConfig>();
                }
                catch { }
            }
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            var autoConfig = new AutoConfigurator();
            autoConfig.Topmost = Config.Avatar.TopMost;
            autoConfig.LoadConfiguration(Config);
            autoConfig.ShowDialog();
            try
            {
                ConfigurationService.SaveConfig<AuraConfig>();
                var msg = new Message(InternalServices.MainUI, InternalEvents.PersonaConfigurationUpdated);
                ServiceMessageBus.SendMessageAsync(msg, true);
            }
            catch { }
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            MinimizeClicked?.Invoke(this, EventArgs.Empty);
        }
    }
}
