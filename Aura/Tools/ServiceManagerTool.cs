﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.Tools
{
    public class ServiceManagerTool : ToolBase
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Service Manager",
            Description = "Manage and control services used by Aura",
            Author = "Code Atelier",
            Id = "coatl.aura.servicemanager",
            Version = AuraInfo.Version,
        };

        public override string Icon { get; } = Util.CreateResourceUri("res/img/window-system.png");

        protected override Window CreateToolWindow()
        {
            return new ServiceManagerWindow();
        }
    }
}
