﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.Tools
{
    public class DebugTool : ToolBase
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Debug",
            Description = "View or edit internal stats used by Aura",
            Author = "Code Atelier",
            Id = "coatl.aura.debug",
            Version = AuraInfo.Version,
        };

        public override string Icon { get; } = Util.CreateResourceUri("res/img/monitor.png");

        protected override Window CreateToolWindow()
        {
            return new DebugWindow();
        }
    }
}
