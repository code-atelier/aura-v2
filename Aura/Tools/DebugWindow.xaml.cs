﻿using Aura.Tools.DebugTabs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura
{
    /// <summary>
    /// Interaction logic for DebugWindow.xaml
    /// </summary>
    public partial class DebugWindow : Window
    {
        public DebugWindow()
        {
            InitializeComponent();
            LoadPage("tags");
        }

        private void PageButtonClicked(object sender, RoutedEventArgs e)
        {
            if (sender is Button button && button.Tag is string tag)
            {
                LoadPage(tag);
            }
        }

        private void LoadPage(string tag)
        {
            bool valid = false;
            foreach (var item in spPager.Children.OfType<Button>())
            {
                if (item.Tag is string btag && btag == tag)
                {
                    item.Background = new SolidColorBrush(Colors.CornflowerBlue);
                    item.Foreground = new SolidColorBrush(Colors.White);
                    item.BorderBrush = new SolidColorBrush(Colors.CornflowerBlue);
                    valid = true;
                }
                else
                {
                    item.Background = new SolidColorBrush(Colors.Transparent);
                    item.BorderBrush = item.Foreground = new SolidColorBrush(Colors.Black);
                }
            }
            if (!valid) { return; }
            switch(tag)
            {
                case "tags":
                    brMain.Child = new DebugTags();
                    break;
                default:
                    brMain.Child = null;
                    break;
            }
        }
    }
}
