﻿using Aura.Services.Active;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WinRT;

namespace Aura.Tools.DebugTabs
{
    /// <summary>
    /// Interaction logic for DebugTags.xaml
    /// </summary>
    public partial class DebugTags : UserControl
    {
        DispatcherTimer timer = new DispatcherTimer();
        TagService TagService { get; }

        Dictionary<string, string> TagDescriptions { get; } = new Dictionary<string, string>();
        string SearchTerm { get; set; } = string.Empty;
        string SetSearchTerm { get; set; } = string.Empty;

        public DebugTags()
        {
            TagService = ServiceManager.GetInstance<TagService>();
            InitializeComponent();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += Timer_Tick;

            var typ = typeof(InternalTags.Emotion);
            var values = typ.GetFields().Where(x => x.IsStatic && x.FieldType == typeof(string) && x.IsLiteral).ToList();
            foreach (var v in values)
            {
                var val = v.GetValue(null) as string;
                if (val != null)
                    TagDescriptions.Add(val, v.Name);
            }

            typ = typeof(InternalTags.CompositeEmotion);
            values = typ.GetFields().Where(x => x.IsStatic && x.FieldType == typeof(string) && x.IsLiteral).ToList();
            foreach (var v in values)
            {
                var val = v.GetValue(null) as string;
                if (val != null)
                    TagDescriptions.Add(val, v.Name);
            }

            typ = typeof(InternalTags.Urges);
            values = typ.GetFields().Where(x => x.IsStatic && x.FieldType == typeof(string) && x.IsLiteral).ToList();
            foreach (var v in values)
            {
                var val = v.GetValue(null) as string;
                if (val != null)
                    TagDescriptions.Add(val, v.Name);
            }
            dgvTags.Items.Clear();
            dgvTags.Columns[1].SortDirection = System.ComponentModel.ListSortDirection.Descending;

            TagChangeDelayer = new DispatcherTimer();
            TagChangeDelayer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            TagChangeDelayer.Tick += TagChangeDelayer_Tick;
            TagChangeDelayer.Start();

            txCustomTags.Text = string.Join(Environment.NewLine, TagService.GetTagsBySource("debug").Select(x => x.Key + ":" + x.Value));
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            UpdateTags();
        }

        private void UpdateTags()
        {
            try
            {
                var tags = TagService.ActiveTags;
                var tlist = tags.ToList();
                var searchTerm = SearchTerm?.Trim().ToLower() ?? "";
                foreach (var tag in tlist)
                {
                    var existing = dgvTags.Items.OfType<TagDisplayObject>().Where(x => x.Tag == tag.Key).FirstOrDefault();
                    if (existing == null)
                    {
                        existing = new TagDisplayObject()
                        {
                            Tag = tag.Key,
                            Weight = tag.Value,
                            Description = TagDescriptions.ContainsKey(tag.Key) ? TagDescriptions[tag.Key] : "",
                        };
                        dgvTags.Items.Add(existing);
                    }
                    else
                    {
                        existing.Weight = tag.Value;
                    }
                }
                var toRemove = dgvTags.Items.OfType<TagDisplayObject>().Where(x => !tlist.Any(w => w.Key == x.Tag)).ToList();
                foreach (var tag in toRemove)
                    dgvTags.Items.Remove(tag);
                foreach (var tdo in dgvTags.Items.OfType<TagDisplayObject>())
                {
                    tdo.Visible = string.IsNullOrWhiteSpace(searchTerm) || tdo.Tag.Contains(searchTerm) || tdo.Description.Contains(searchTerm);
                }
            }
            catch { }
            UpdateSets();
        }

        private void UpdateSets()
        {
            try
            {
                var selTag = (dgvTags.SelectedItem as TagDisplayObject)?.Tag;
                var persona = ServiceManager.Persona;
                if (persona == null)
                {
                    dgvSets.Items.Clear();
                    return;
                }
                var tags = TagService.ActiveTags;
                var sets = persona.Avatar.Sets.OrderBy(x => x.ToString());
                var slist = new List<string>();
                var searchTerm = SetSearchTerm?.Trim().ToLower() ?? "";
                foreach (var set in sets)
                {
                    var id = (set.Category ?? "") + "#" + (set.Reference ?? "ref?");
                    if (!slist.Contains(id))
                        slist.Add(id);
                    var existing = dgvSets.Items.OfType<SetDisplayObject>().Where(x => x.Set == id).FirstOrDefault();
                    var currentWeight = set.TagCondition.CalculateWeight(tags);
                    var tagWeight = selTag != null ? set.TagCondition.Tags.FirstOrDefault(x => x.Tag == selTag)?.Weight ?? 0 : 0;
                    if (existing == null)
                    {
                        existing = new SetDisplayObject()
                        {
                            Set = id,
                            CurrentWeight = currentWeight,
                            TagWeight = tagWeight,
                        };
                        dgvSets.Items.Add(existing);
                    }
                    else
                    {
                        existing.CurrentWeight = currentWeight;
                        existing.TagWeight = tagWeight;
                    }
                }
                var toRemove = dgvSets.Items.OfType<SetDisplayObject>().Where(x => !slist.Any(w => w == x.Set)).ToList();
                foreach (var tag in toRemove)
                    dgvSets.Items.Remove(tag);
                foreach (var sdo in dgvSets.Items.OfType<SetDisplayObject>())
                {
                    sdo.Visible = string.IsNullOrWhiteSpace(searchTerm) || sdo.Set.Contains(searchTerm);
                }

                var sorted = dgvSets.Columns.FirstOrDefault(x => x.SortDirection != null);
                if (sorted != null)
                {
                    var performSortMethod = typeof(DataGrid)
                                .GetMethod("PerformSort",
                                           BindingFlags.Instance | BindingFlags.NonPublic);
                    performSortMethod?.Invoke(dgvSets, new[] { sorted });
                    performSortMethod?.Invoke(dgvSets, new[] { sorted });
                }
            }
            catch { }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            TagChangeDelayer.Stop();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateTags();
            timer.Start();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            SearchTerm = txSearch.Text;
            UpdateTags();
        }

        private void txSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                SearchTerm = txSearch.Text;
                UpdateTags();
            }
        }

        private class SetDisplayObject : INotifyPropertyChanged
        {
            bool _visible = true;
            public bool Visible
            {
                get => _visible;
                set
                {
                    if (_visible != value)
                    {
                        _visible = value;
                        OnPropertyChanged(nameof(Visible));
                    }
                }
            }

            public string Set { get; set; } = string.Empty;

            double _tagWeight = 1;
            public double TagWeight
            {
                get => _tagWeight;
                set
                {
                    if (_tagWeight != value)
                    {
                        _tagWeight = value;
                        OnPropertyChanged(nameof(TagWeight));
                    }
                }
            }

            double _currentWeight = 1;
            public double CurrentWeight
            {
                get => _currentWeight;
                set
                {
                    if (_currentWeight != value)
                    {
                        _currentWeight = value;
                        OnPropertyChanged(nameof(CurrentWeight));
                    }
                }
            }

            public event PropertyChangedEventHandler? PropertyChanged;

            void OnPropertyChanged(string propertyName)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private class TagDisplayObject : INotifyPropertyChanged
        {
            bool _visible = true;
            public bool Visible
            {
                get => _visible;
                set
                {
                    if (_visible != value)
                    {
                        _visible = value;
                        OnPropertyChanged(nameof(Visible));
                    }
                }
            }

            public string Tag { get; set; } = string.Empty;

            double _weight = 1;
            public double Weight
            {
                get => _weight;
                set
                {
                    if (_weight != value)
                    {
                        _weight = value;
                        OnPropertyChanged(nameof(Weight));
                    }
                }
            }

            public string Description { get; set; } = string.Empty;

            public event PropertyChangedEventHandler? PropertyChanged;

            void OnPropertyChanged(string propertyName)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void dgvTags_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateSets();
        }

        private void txSearchSet_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                SetSearchTerm = txSearchSet.Text;
                UpdateSets();
            }
        }

        private void btnSearchSet_Click(object sender, RoutedEventArgs e)
        {
            SetSearchTerm = txSearchSet.Text;
            UpdateSets();
        }

        DateTime? LastChange { get; set; }
        DispatcherTimer TagChangeDelayer { get; }
        private void txCustomTags_TextChanged(object sender, TextChangedEventArgs e)
        {
            LastChange = DateTime.Now;
            txCustomTags.Foreground = new SolidColorBrush(Colors.Crimson);
        }
        private void TagChangeDelayer_Tick(object? sender, EventArgs e)
        {
            if (LastChange == null) return;
            var delta = DateTime.Now - LastChange.Value;
            if (delta.TotalSeconds >= 0.5)
            {
                LastChange = null;
                txCustomTags.Foreground = new SolidColorBrush(Colors.Blue);
                var lines = txCustomTags.Text.Split('\n', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                TagService.RemoveTagsBySource("debug");
                foreach (var line in lines)
                {
                    var spl = line.Split(':');
                    var tag = spl[0];
                    double weight = 1;
                    if (spl.Length > 1 && double.TryParse(spl[1], out var v))
                    {
                        weight = v;
                    }
                    TagService.Add("debug", tag, null, weight);
                }
            }
        }

    }
}
