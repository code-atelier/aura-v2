﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.Tools
{
    public class ConsoleTool : ToolBase
    {
        public override ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Console",
            Description = "Run commands or chat with chatbot",
            Author = "Code Atelier",
            Id = "coatl.aura.console",
            Version = AuraInfo.Version,
        };

        public override string Icon { get; } = Util.CreateResourceUri("res/img/terminal.png");

        protected override Window CreateToolWindow()
        {
            return new ConsoleWindow();
        }
    }
}
