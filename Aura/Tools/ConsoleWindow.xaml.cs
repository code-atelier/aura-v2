﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura
{
    /// <summary>
    /// Interaction logic for ConsoleWindow.xaml
    /// </summary>
    public partial class ConsoleWindow : Window
    {
        public ObservableCollection<ConsoleEntry> Entries { get; } = new ObservableCollection<ConsoleEntry>();

        ConsoleEntry? thinkingEntry = null;
        bool _isBusy = false;
        private bool IsBusy
        {
            get => _isBusy; set
            {
                if (value != _isBusy)
                {
                    Dispatcher.Invoke(() =>
                    {
                        if (value)
                        {
                            thinkingEntry = new ConsoleEntry()
                            {
                                Text = "Thinking...",
                                SentByAura = true,
                                Thinking = true,
                            };
                            Entries.Add(thinkingEntry);
                            svChat.ScrollToEnd();
                        }
                        else if (thinkingEntry != null)
                        {
                            Entries.Remove(thinkingEntry);
                            thinkingEntry = null;
                        }
                    });
                    _isBusy = value;
                }
            }
        }
        private static bool _initialized = false;

        public ConsoleWindow()
        {
            Initialize();
            InitializeComponent();
            itemChat.ItemsSource = Entries;
            Entries.Add(new ConsoleEntry()
            {
                Text = string.Join(Environment.NewLine,
                    "Aura v" + AuraInfo.Version + " - Copyright © 2023 Code Atelier",
                    "Hi! Welcome to Aura Console.",
                    "**To chat with chatbot**",
                    "- Type your message and press enter to send",
                    "- Do not prefix it with forward-slash character (/)",
                    "- You can use shift+enter to create a multiline message",
                    "- Press escape to exit multiline mode",
                    "- Use '/history' to show chat history if supported",
                    "**To execute console commands**",
                    "- Start your message with forward-slash character (/)",
                    "- Then type the console commands and press enter to send",
                    "- For Example: /help"
                ),
                SentByAura = true,
            });
            RichTextBoxHelper.OwnChatClicked += RichTextBoxHelper_OwnChatClicked;
            txMessage.Focus();
        }

        private void RichTextBoxHelper_OwnChatClicked(object? sender, string? e)
        {
            if (e != null)
            {
                txMessage.Text = e;
                txMessage.Select(e.Length, 0);
                txMessage.Focus();
            }
        }

        public static void Initialize()
        {
            if (_initialized) return;

            var processorType = typeof(IConsoleExtension);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            Processors.Clear();
            foreach (var assembly in assemblies)
            {
                try
                {
                    var classes = assembly.GetExportedTypes().Where(x => processorType.IsAssignableFrom(x) && !x.IsAbstract && x.IsClass);
                    foreach (var type in classes)
                    {
                        try
                        {
                            var processor = Activator.CreateInstance(type) as IConsoleExtension;
                            if (processor != null)
                            {
                                var id = processor.Command;
                                if (!Processors.ContainsKey(id))
                                    Processors.Add(id, processor);
                            }
                        }
                        catch { }
                    }
                }
                catch { }
            }
            _initialized = true;
        }

        public static Dictionary<string, IConsoleExtension> Processors { get; } = new Dictionary<string, IConsoleExtension>();


        private void txMessage_TextChanged(object sender, TextChangedEventArgs e)
        {
            lbTypeMessage.Visibility = string.IsNullOrEmpty(txMessage.Text) ? Visibility.Visible : Visibility.Collapsed;
        }

        void EnterChatMode()
        {
            rwChat.Height = new GridLength(90);
            txMessage.Height = 90;
            txMessage.AcceptsReturn = true;
            txMessage.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            chatMode = true;
            txMessage.Text += Environment.NewLine;
            txMessage.Select(txMessage.Text.Length, 0);
        }
        void LeaveChatMode()
        {
            txMessage.Height = 30;
            rwChat.Height = new GridLength(30);
            txMessage.AcceptsReturn = false;
            txMessage.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            chatMode = false;
            txMessage.Text = txMessage.Text.Split(new char[] { '\r', '\n' })[0];
            txMessage.Select(txMessage.Text.Length, 0);
        }

        bool chatMode = false;
        private void txMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (chatMode && e.Key == Key.Escape)
            {
                e.Handled = true;
                LeaveChatMode();
                return;
            }
            if (e.Key == Key.Enter && !chatMode)
            {
                if (e.KeyboardDevice.IsKeyDown(Key.LeftShift) || e.KeyboardDevice.IsKeyDown(Key.RightShift))
                {
                    EnterChatMode();
                }
                else
                {
                    e.Handled = true;
                    if (IsBusy) return;

                    var message = txMessage.Text.Trim();
                    txMessage.Clear();
                    if (string.IsNullOrWhiteSpace(message)) return;


                    if (message.StartsWith("/"))
                    {
                        Entries.Add(new ConsoleEntry()
                        {
                            Text = message,
                            SentByAura = false,
                            IsConsoleCommand = true,
                        });
                        svChat.ScrollToEnd();
                        HandleConsoleCommand(message.Substring(1).Trim());
                    }
                    else
                    {
                        Entries.Add(new ConsoleEntry()
                        {
                            Text = message,
                            SentByAura = false,
                            IsConsoleCommand = false,
                            ChatbotServiceMessage = new ChatbotServiceUserMessage()
                            {
                                AuthorName = "You"
                            }
                        });
                        svChat.ScrollToEnd();
                        HandleChat(message);
                    }
                }
            }
        }

        private void txMessage_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && chatMode)
            {
                if (!e.KeyboardDevice.IsKeyDown(Key.LeftShift) && !e.KeyboardDevice.IsKeyDown(Key.RightShift))
                {
                    e.Handled = true;
                    if (IsBusy) return;
                    var message = txMessage.Text.Trim();
                    LeaveChatMode();
                    txMessage.Clear();
                    if (string.IsNullOrWhiteSpace(message)) return;
                    Entries.Add(new ConsoleEntry()
                    {
                        Text = message,
                        SentByAura = false,
                        IsConsoleCommand = false,
                    });
                    svChat.ScrollToEnd();
                    HandleChat(message);
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            RichTextBoxHelper.OwnChatClicked -= RichTextBoxHelper_OwnChatClicked;
        }

        private async void HandleChat(string message)
        {
            IsBusy = true;
            try
            {
                var cs = ServiceManager.GetChatbotService();
                if (cs == null)
                    throw new Exception("Failed to send chat message, no chatbot is currently active.");
                var cbm = new ChatbotServiceUserMessage()
                {
                    Message = message,
                    Retries = 0,
                };
                var replies = await cs.SendMessage(cbm);
                if (replies != null)
                {
                    foreach (var reply in replies)
                    {
                        if (reply.IsSuccessful && !string.IsNullOrWhiteSpace(reply.Message))
                        {
                            Dispatcher.Invoke(() =>
                            {
                                Entries.Add(new ConsoleEntry()
                                {
                                    Text = reply.Message,
                                    SentByAura = true,
                                    ChatbotServiceMessage = reply,
                                });
                                svChat.ScrollToEnd();
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(() =>
                {
                    Entries.Add(new ConsoleEntry()
                    {
                        Text = "Error: " + ex.Message,
                        SentByAura = true,
                        IsErrorMessage = true,
                    });
                    svChat.ScrollToEnd();
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async void HandleConsoleCommand(string text)
        {
            IsBusy = true;
            try
            {
                var command = ConsoleCommand.Parse(text);
                if (command.Command.ToLower() == "cls")
                {
                    Entries.Clear();
                    return;
                }
                if (command.Command.ToLower() == "history")
                {
                    var count = 10;
                    var cntPart = command.Parts.FirstOrDefault(x => x.ParameterName == null);
                    if (cntPart != null && cntPart.Value != null)
                    {
                        if (!int.TryParse(cntPart.Value, out count))
                        {
                            count = 10;
                        }
                    }
                    LoadLastChat(count, true);
                    return;
                }
                var res = await ServiceManager.RunConsoleCommand(command);
                Dispatcher.Invoke(() =>
                {
                    Entries.Add(new ConsoleEntry()
                    {
                        Text = res,
                        IsConsoleCommand = true,
                        SentByAura = true,
                    });
                    svChat.ScrollToEnd();
                });
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(() =>
                {
                    Entries.Add(new ConsoleEntry()
                    {
                        Text = ex.Message,
                        IsConsoleCommand = true,
                        IsErrorMessage = true,
                        SentByAura = true,
                    });
                    svChat.ScrollToEnd();
                });
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private async void LoadLastChat(int count, bool clearEntries = false)
        {
            var cb = ServiceManager.GetChatbotService();
            if (cb != null)
            {
                if (cb is IService svc && svc.State != ServiceState.Running)
                {
                    Dispatcher.Invoke(() =>
                    {
                        Entries.Add(new ConsoleEntry()
                        {
                            Text = "Failed to load chat history, '" + svc.Info.Name + "' is currently " + svc.State.ToString().ToLower() + ".",
                            IsErrorMessage = true,
                            SentByAura = true,
                        });
                        svChat.ScrollToEnd();
                    });
                    return;
                }
                if (await cb.LoadChatHistory())
                {
                    Dispatcher.Invoke(() =>
                    {
                        if (clearEntries)
                        {
                            Entries.Clear();
                        }
                        var history = cb.ChatHistory.ToList();
                        if (history.Count > count)
                        {
                            history = history.GetRange(history.Count - count, count);
                        }
                        foreach (var chat in history)
                        {
                            Entries.Add(new ConsoleEntry()
                            {
                                Text = chat.Message,
                                SentByAura = !chat.IsHuman,
                                ChatbotServiceMessage = chat,
                            });
                        }
                        svChat.ScrollToEnd();
                    });
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        Entries.Add(new ConsoleEntry()
                        {
                            Text = "Failed to load chat history",
                            IsErrorMessage = true,
                            SentByAura = true,
                        });
                        svChat.ScrollToEnd();
                    });
                }
            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    Entries.Add(new ConsoleEntry()
                    {
                        Text = "Cannot load chat history, no chatbot is currently active.",
                        IsErrorMessage = true,
                        SentByAura = true,
                    });
                    svChat.ScrollToEnd();
                });
            }
        }
    }
    public class RichTextBoxHelper : DependencyObject
    {
        public static string GetDocumentMarkdown(DependencyObject obj)
        {
            return (string)obj.GetValue(DocumentMarkdownProperty);
        }

        public static void SetDocumentMarkdown(DependencyObject obj, string value)
        {
            obj.SetValue(DocumentMarkdownProperty, value);
        }

        public static readonly DependencyProperty DocumentMarkdownProperty =
            DependencyProperty.RegisterAttached(
                "DocumentMarkdown",
                typeof(string),
                typeof(RichTextBoxHelper),
                new FrameworkPropertyMetadata
                {
                    BindsTwoWayByDefault = false,
                    PropertyChangedCallback = (obj, e) =>
                    {
                        var richTextBox = (RichTextBox)obj;
                        var entry = (ConsoleEntry)richTextBox.Tag;
                        var md = GetDocumentMarkdown(richTextBox);
                        if (entry.IsConsoleCommand)
                        {
                            var doc = new FlowDocument();
                            var p = new Paragraph();
                            if (!entry.SentByAura)
                            {
                                p.MouseLeftButtonDown += OwnChatTrigger;
                                p.Cursor = Cursors.Hand;
                                p.Tag = md;
                            }
                            p.Inlines.Add(new Run(md));
                            doc.Blocks.Add(p);
                            richTextBox.Document = doc;
                        }
                        else
                        {
                            var doc = RichTextDocumentParser.Parse(md);
                            richTextBox.Document = doc;
                        }
                    }
                });

        private static void OwnChatTrigger(object sender, MouseButtonEventArgs e)
        {
            var para = (Paragraph)sender;
            OwnChatClicked?.Invoke(sender, para.Tag as string);
        }

        public static event EventHandler<string?>? OwnChatClicked;
    }
}
