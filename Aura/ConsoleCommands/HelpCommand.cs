﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace Aura.ConsoleCommands
{
    public class HelpCommand : IConsoleExtension
    {
        public ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Help",
            Description = "Show information about available commands",
            Author = "Code Atelier",
            Id = "aura.console.commands.help",
            Version = AuraInfo.Version,
        };

        public string Command { get; } = "help";

        public async Task<string> RunCommand(ConsoleCommand command)
        {
            return await Task.Run(() =>
            {
                var lines = new List<string>();
                if (command.Parts.Count > 0 && !string.IsNullOrWhiteSpace(command.Parts[0].Value))
                {
                    var qry = command.Parts[0].Value.Trim().ToLower();
                    var scex = ServiceManager.ConsoleExtensions.FirstOrDefault(x => x.Command.ToLower() == qry);
                    if (scex != null)
                    {
                        if (scex is IConsoleExtensionWithHelp wh)
                        {
                            return scex.Command.ToLower().Trim() + ": " + scex.Info.Description + Environment.NewLine + Environment.NewLine + wh.Help;
                        }
                        else
                        {
                            return scex.Command.ToLower().Trim().PadRight(10) + " - " + scex.Info.Description
                             + Environment.NewLine + "(this command has no extended help)";
                        }
                    }
                    var cexes = ServiceManager.ConsoleExtensions
                    .Where(x => x.Command.ToLower().Contains(qry) || x.Info.Description.ToLower().Contains(qry))
                    .OrderBy(x => x.Command.ToLower());
                    foreach (var cex in cexes)
                    {
                        lines.Add(cex.Command.ToLower().Trim().PadRight(10) + " - " + cex.Info.Description);
                    }
                }
                else
                {
                    foreach (var cex in ServiceManager.ConsoleExtensions.OrderBy(x => x.Command.ToLower()))
                    {
                        lines.Add(cex.Command.ToLower().Trim().PadRight(10) + " - " + cex.Info.Description);
                    }
                }
                lines.Add("");
                lines.Add("Use '/help <command>' to see extended help about that command.");
                return string.Join(Environment.NewLine, lines.ToArray());
            });
        }
    }
}
