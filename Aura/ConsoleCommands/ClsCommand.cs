﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.ConsoleCommands
{
    public class ClsCommand : IConsoleExtension
    {
        public ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Cls",
            Description = "Clear console screen",
            Author = "Code Atelier",
            Id = "aura.console.commands.cls",
            Version = AuraInfo.Version,
        };

        public string Command { get; } = "cls";

        public async Task<string> RunCommand(ConsoleCommand command)
        {
            return await Task.Run(() =>
            {
                return "";
            });
        }
    }
    public class HistoryCommand : IConsoleExtensionWithHelp
    {
        public ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "History",
            Description = "Clear console screen and show chat history",
            Author = "Code Atelier",
            Id = "aura.console.commands.history",
            Version = AuraInfo.Version,
        };

        public string Command { get; } = "history";

        public string Help { get; } = string.Join(Environment.NewLine,
            "/history [count]",
            "count: Number of chat messages to show, default is 10"
        );

        public async Task<string> RunCommand(ConsoleCommand command)
        {
            return await Task.Run(() =>
            {
                return "";
            });
        }
    }
}
