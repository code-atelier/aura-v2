﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.ConsoleCommands
{
    public class SvcCommand : IConsoleExtensionWithHelp
    {
        public ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Svc",
            Description = "Show information about running services",
            Author = "Code Atelier",
            Id = "aura.console.commands.svc",
            Version = AuraInfo.Version,
        };

        public string Command { get; } = "svc";

        public string Help { get; } = string.Join(Environment.NewLine,
            "/svc [name] [-core|-noncore]",
            "name: Substring of the name of the service",
            "-core: Show only core services (or -c)",
            "-noncore: Show only non-core services (or -nc)"
            );

        public async Task<string> RunCommand(ConsoleCommand command)
        {
            var coreOnly = command.Switch("core") != null || command.Switch("c") != null;
            var nonCoreOnly = command.Switch("noncore") != null || command.Switch("nc") != null;

            return await Task.Run(() =>
            {
                var lines = new List<string>();
                
                string initLine = string.Empty;
                initLine += "Service ID".PadRight(35);
                initLine += " | " + "Service Name".PadRight(30);
                initLine += " | " + "State".PadRight(10);
                lines.Add(initLine);
                lines.Add(new string('=', 81));

                string filter = command.Value(0)?.ToLower().Trim() ?? "";

                foreach (var svc in ServiceManager.Services.Where(
                    x =>
                        (coreOnly ? ServiceUtil.IsSystemService(x) :
                        nonCoreOnly ? !ServiceUtil.IsSystemService(x) : true)
                        &&
                        (x.Info.Name.ToLower().Contains(filter) || x.Info.Id.ToLower().Contains(filter))
                    ))
                {
                    var name = svc.Info.Id;
                    if (ServiceUtil.IsSystemService(svc))
                    {
                        name = "<<" + name + ">>";
                    }
                    string line = string.Empty;
                    line += name.PadRight(35);
                    line += " | " + svc.Info.Name.PadRight(30);
                    line += " | " + svc.State.ToString().PadRight(10);
                    lines.Add(line);
                }
                return string.Join(Environment.NewLine, lines);
            });
        }
    }
}
