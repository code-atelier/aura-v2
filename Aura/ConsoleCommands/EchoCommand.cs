﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.ConsoleCommands
{
    public class EchoCommand : IConsoleExtension
    {
        public ExtensionInfo Info { get; } = new ExtensionInfo()
        {
            Name = "Echo",
            Description = "Send back your message as is",
            Author = "Code Atelier",
            Id = "aura.console.commands.echo",
            Version = AuraInfo.Version,
        };

        public string Command { get; } = "echo";

        public async Task<string> RunCommand(ConsoleCommand command)
        {
            return await Task.Run(() =>
            {
                var bare = command.RawCommand.Trim();
                if (bare.StartsWith("echo", StringComparison.InvariantCultureIgnoreCase))
                {
                    bare = bare.Substring(4).Trim();
                }
                return bare;
            });
        }
    }
}
