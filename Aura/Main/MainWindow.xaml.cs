﻿using Aura.Models;
using Aura.Models.Configs;
using Aura.Services;
using Aura.Services.Active;
using Aura.Services.Passive;
using Aura.WPF;
using Aura.WPF.Windows;
using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Media;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using static System.Windows.Forms.AxHost;

namespace Aura
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IMessageReceiver
    {
        System.Windows.Forms.NotifyIcon NotifyIcon { get; } = new System.Windows.Forms.NotifyIcon()
        {
            BalloonTipTitle = "Aura",
            BalloonTipText = "Double click to show Aura",
        };

        ConfigurationService ConfigService { get; }

        SpriteManager Sprites { get; } = new SpriteManager();

        IServiceMessageBus MessageBus { get; }

        public UriRequest? UriArguments { get; set; }

        public bool CanReceiveMessage { get => true; }

        public Persona? Persona { get; private set; }

        static List<string> AllowedDropFormats { get; } = new List<string>()
        {
            DataFormats.FileDrop,
            DataFormats.Text,
            DataFormats.Bitmap,
        };

        public MessageReceiverPriority Priority { get; } = MessageReceiverPriority.Critical;

        public string Id { get; } = InternalServices.MainUI;

        public MainWindow()
        {
            InitializeComponent();
            ConfigService = ServiceManager.GetInstance<ConfigurationService>();
            MessageBus = ServiceManager.GetInstance<IServiceMessageBus>();
            ServiceManager.MainDispatcher = Dispatcher;
            SubscribeMessages();

            try
            {
                var sri = App.GetResourceStream(new Uri(Util.CreateResourceUri("res/icon.ico")));
                using (var ms = new MemoryStream())
                {
                    sri.Stream.Seek(0, SeekOrigin.Begin);
                    sri.Stream.CopyTo(ms);
                    var bmp = new System.Drawing.Bitmap(ms);
                    NotifyIcon.Icon = System.Drawing.Icon.FromHandle(bmp.GetHicon());
                }
            }
            catch { }
            
            NotifyIcon.MouseDoubleClick += NotifyIcon_MouseDoubleClick;
            NotifyIcon.MouseClick += NotifyIcon_MouseClick;
            InitAudioManager();
        }

        private void NotifyIcon_MouseClick(object? sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ShowStartMenu(true);
            }
        }

        private void LoadPersona()
        {
            var config = ConfigService.LoadConfig<AuraConfig>() ?? new AuraConfig();
            var personaPath = Path.Combine(AuraPaths.Compendium, config.Avatar.PersonaId ?? "");
            var p = Persona.Load(personaPath);
            Sprites.Clear();
            foreach (var spr in p.Avatar.Sprites)
            {
                Sprites.Add(spr);
            }
            avatarCanvas.Visibility = Visibility.Collapsed;
            avatarCanvas.LoadAvatar(p.Avatar, Sprites);
            Width = p.Avatar.Display.CanvasWidth;
            Height = p.Avatar.Display.CanvasHeight;
            Persona = p;
            ServiceManager.Persona = p;
            UpdateScale();

            var currentScreen = GetCurrentScreenRect();
            if (currentScreen == null || config.Avatar.Position == null)
            {
                SetPosition(null, null);
            }
            else
            {
                SetPosition(config.Avatar.Position.X, config.Avatar.Position.Y);
            }
            Topmost = config.Avatar.TopMost;
            ShowInTaskbar = config.ShowInTaskbar;
            avatarCanvas.Visibility = Visibility.Visible;
            var tagService = ServiceManager.GetInstance<TagService>();
            if (tagService != null)
            {
                tagService.LoadPersona(Persona);
            }
            var scrService = ServiceManager.GetInstance<ScriptingService>();
            if (scrService != null)
            {
                scrService.LoadPersona(Persona);
            }
        }

        private void UpdateScale()
        {
            if (Persona != null)
            {
                var scale = (ConfigService.LoadConfig<AuraConfig>()?.Avatar.Scale ?? 100) / 100.0;
                //Width = Persona.Avatar.Display.CanvasWidth * scale;
                //Height = Persona.Avatar.Display.CanvasHeight * scale;
                gridRoot.RenderTransform = new ScaleTransform(scale, scale);
            }
        }

        async void HandleUriArgs()
        {
            if (UriArguments != null)
            {
                var msg = new Message<UriRequest>(Id, InternalEvents.UriRequest, UriArguments);
                await MessageBus.SendMessageAsync(msg);
            }
        }

        public void SubscribeMessages()
        {
            MessageBus.Subscribe(this, InternalEvents.ShowToastNotificationRequest);
            MessageBus.Subscribe(this, InternalEvents.FileDrop);
            MessageBus.Subscribe(this, InternalEvents.ExecuteFileDrop);
            MessageBus.Subscribe(this, InternalEvents.PersonaConfigurationUpdated);
            MessageBus.Subscribe(this, InternalEvents.PersonaTagsUpdated);
        }
    }
}
