﻿using Aura.Models;
using Aura.Models.Configs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    [SupportedOSPlatform("Windows")]
    public partial class MainWindow
    {
        private string? SaveToTemp(AudioFile audioFile)
        {
            if (audioFile.BinaryData == null || Persona == null) return null;
            try
            {
                var tempdir = "temp/audio/" + Persona.Id + "/";
                if (!Directory.Exists(tempdir))
                {
                    Directory.CreateDirectory(tempdir);
                }
                var tempfile = Path.Combine(tempdir, Guid.NewGuid().ToString("n") + audioFile.Extension);
                if (!File.Exists(tempfile))
                    File.WriteAllBytes(tempfile, audioFile.BinaryData);
                return Path.GetFullPath(tempfile);
            }
            catch { }
            return null;
        }

        public void InitAudioManager()
        {
            SoundManager.PlayAudio = PlayAudio;
            try
            {
                var tempdir = "temp/audio";
                if (Directory.Exists(tempdir))
                {
                    Directory.Delete(tempdir, true);
                }
                Directory.CreateDirectory(tempdir);
                var di = new DirectoryInfo(tempdir);
                di.Attributes |= FileAttributes.Hidden;
            }
            catch { }
        }

        public void PlayAudio(AudioFile audioFile)
        {
            Dispatcher.Invoke(() =>
            {
                if (audioFile.Category == AudioCategory.Voice)
                {
                    PlayVoice(audioFile);
                }
            });
        }

        private void PlayVoice(AudioFile audioFile)
        {
            try
            {
                meVoice.Stop();
            }
            catch
            { }
            try
            {
                var config = ConfigService.LoadConfig<AuraConfig>();
                var vol = Math.Min(100, Math.Max(0, config?.Audio?.VoiceVolume ?? 100));
                vol = vol / 100;
                var tempFile = SaveToTemp(audioFile);
                if (tempFile != null)
                {
                    meVoice.Volume = vol;
                    if (meVoice.Tag is string prevFile && prevFile == tempFile)
                    {
                        meVoice.Play();
                    }
                    else
                    {
                        meVoice.Source = new Uri(tempFile);
                        meVoice.Tag = tempFile;
                        meVoice.Play();
                    }
                }
            }
            catch { }
        }
    }
}
