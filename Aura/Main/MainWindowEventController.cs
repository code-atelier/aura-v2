﻿using Aura.Models.Configs;
using System.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Input;

namespace Aura
{
    public partial class MainWindow
    {
        bool forceClose = false;

        private void NotifyIcon_MouseDoubleClick(object? sender, System.Windows.Forms.MouseEventArgs e)
        {
            Show();
            WindowState = WindowState.Normal;
            NotifyIcon.Visible = false;
            ShowInTaskbar = ConfigService.LoadConfig<AuraConfig>().ShowInTaskbar;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CheckPersonaSelected();
            var config = ConfigService.LoadConfig<AuraConfig>() ?? new AuraConfig();
            try
            {
                LoadPersona();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                AppEntryPoint.DoShutdown();
                return;
            }
            HandleUriArgs();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var personaConfig = ConfigService.LoadConfig<AuraConfig>();
            if (personaConfig?.Avatar.LockPosition != true)
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    Cursor = Cursors.SizeAll;
                    DragMove();
                    Cursor = Cursors.Arrow;
                    SavePosition();
                }
            }
            else
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    try
                    {
                        var tempDir = Util.GetTempDirectory("aura.clipboard");
                        var files = Directory.EnumerateFiles(tempDir, "*", SearchOption.AllDirectories);
                        foreach (var file in files)
                        {
                            try
                            {
                                File.Delete(file);
                            }
                            catch
                            {

                            }
                        }
                        var tempFile = Path.Combine(tempDir, Guid.NewGuid().ToString("n") + ".png");
                        bool success;
                        using (var fo = File.Create(tempFile))
                        {
                            success = avatarCanvas.WriteAvatar(fo);
                        }
                        if (success)
                        {
                            var dataObject = new DataObject();
                            dataObject.SetData(DataFormats.FileDrop, new string[] { tempFile });
                            allowFileDrop = false;
                            try
                            {
                                DragDrop.DoDragDrop(avatarCanvas, dataObject, DragDropEffects.All);
                            }
                            finally { allowFileDrop = true; }
                        }
                    }
                    catch { }
                }
            }
        }

        private void avatarCanvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ShowStartMenu();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!forceClose)
            {
                if (MessageBox.Show("Are you sure to shut down Aura?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) != MessageBoxResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }

        private void Window_LocationChanged(object sender, EventArgs e)
        {
            var config = ConfigService.LoadConfig<AuraConfig>() ?? new AuraConfig();
            if (config.Avatar.SnapToScreen)
            {
                var currentScreen = GetCurrentScreenRect();
                var allScreens = System.Windows.Forms.Screen.AllScreens;
                var scale = config.Avatar.Scale / 100;
                var wRect = new Rect(Left, Top, gridRoot.ActualWidth * scale, gridRoot.ActualHeight * scale);
                var rPoints = new List<Point>() { wRect.TopLeft, wRect.TopRight, wRect.BottomLeft, wRect.BottomRight };
                var pointsInScreens = rPoints.Where(x => allScreens.Any(s => s.Bounds.Contains((int)x.X, (int)x.Y))).ToList();
                if (pointsInScreens.Count < 3)
                {
                    // should be 2 items or more
                    var pointsNotInScreen = rPoints.Where(x => !pointsInScreens.Contains(x)).ToList();
                    rPoints.Add(new Point(wRect.Left + wRect.Width / 2, wRect.Top + wRect.Height / 2));

                    // get screen with the most points
                    var screens = allScreens.Select(s =>
                        new Tuple<System.Windows.Forms.Screen, int>(s, rPoints.Count(p => s.Bounds.Contains((int)p.X, (int)p.Y)))
                    ).ToList();
                    var maxScreen = screens.Count > 0 ? screens.MaxBy(x => x.Item2)?.Item1 : null;
                    if (maxScreen != null && pointsNotInScreen.Count >= 2)
                    {
                        var shouldMoveX = pointsNotInScreen.Count(x => x.X == wRect.Left) >= 2 || pointsNotInScreen.Count(x => x.X == wRect.Right) >= 2;
                        var shouldMoveY = pointsNotInScreen.Count(x => x.Y == wRect.Top) >= 2 || pointsNotInScreen.Count(x => x.Y == wRect.Bottom) >= 2;

                        if (shouldMoveX)
                        {
                            Left = Math.Min(maxScreen.Bounds.Right - wRect.Width, Math.Max(maxScreen.Bounds.Left, wRect.Left));
                        }
                        if (shouldMoveY)
                        {
                            Top = Math.Min(maxScreen.Bounds.Bottom - wRect.Height, Math.Max(maxScreen.Bounds.Top, wRect.Top));
                        }
                    }
                }
            }
        }

    }
}
