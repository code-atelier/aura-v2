﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Aura.WPF;
using System.Windows;
using Aura.Models.Configs;
using System.Security.Cryptography;
using Aura.Services.Active;
using System.Runtime.Versioning;

namespace Aura
{
    public partial class MainWindow
    {
        Window? FileDropWindow = null;

        private void avatarCanvas_DragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
            if (!allowFileDrop)
            {
                e.Effects = DragDropEffects.None;
                return;
            }
            if (AllowedDropFormats.Any(x => e.Data.GetDataPresent(x)))
            {
                e.Effects = DragDropEffects.Link;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private async void avatarCanvas_Drop(object sender, DragEventArgs e)
        {
            e.Handled = true;
            e.Effects = DragDropEffects.None;
            if (!allowFileDrop)
            {
                e.Effects = DragDropEffects.None;
                return;
            }
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && e.Data.GetData(DataFormats.FileDrop) is string[] files)
            {
                try
                {
                    allowFileDrop = false;
                    e.Effects = DragDropEffects.Link;
                    var fde = new FileDropEventBody(files);
                    var msg = new Message<FileDropEventBody>(InternalServices.MainUI, InternalEvents.FileDrop, fde);
                    var replies = (await MessageBus.SendMessageAsync(msg)).Where(x => x.HasBody<FileDropHandler>() || x.HasBody<IEnumerable<FileDropHandler>>()).ToList();

                    if (replies.Count > 0)
                    {
                        var handlers = replies.Where(x => x.HasBody<FileDropHandler>()).Select(x => x.GetBody<FileDropHandler>()).OfType<FileDropHandler>().ToList();
                        var handlersList = replies.Where(x => x.HasBody<IEnumerable<FileDropHandler>>()).Select(x => x.GetBody<IEnumerable<FileDropHandler>>()).OfType<IEnumerable<FileDropHandler>>().ToList().SelectMany(x => x);
                        handlers.AddRange(handlersList);
                        ShowFileDropMenu(handlers, files);
                    }
                }
                catch { }
                finally
                {
                    allowFileDrop = true;
                }
            }
        }

        void ShowFileDropMenu(IEnumerable<FileDropHandler> handlers, IEnumerable<string> files)
        {
            try
            {
                if (FileDropWindow != null)
                {
                    try
                    {
                        FileDropWindow.Close();
                    }
                    catch { }
                }
                var menuType = ServiceManager.FileDropMenus.First();
                var instance = Activator.CreateInstance(menuType) as IFileDropMenu;
                if (instance != null)
                {
                    instance.OnFileDropExecute += Instance_OnFileDropExecute;
                    if (instance is Window fileDropWindow)
                    {
                        FileDropWindow = fileDropWindow;
                        fileDropWindow.Owner = this;
                        fileDropWindow.Closed += FileDropWindow_Closed;
                    }
                    instance.LoadFiles(files);
                    instance.LoadHandlers(handlers);
                    var cmdAnchor = CalculateCommandAnchor();
                    instance.Show(cmdAnchor.Item1, cmdAnchor.Item2.X, cmdAnchor.Item2.Y);
                }
            }
            catch
            {

            }
        }

        private void FileDropWindow_Closed(object? sender, EventArgs e)
        {
            try
            {
                if (sender is Window w && w == FileDropWindow)
                {
                    FileDropWindow = null;
                }
            }
            catch { }
        }

        private async void Instance_OnFileDropExecute(object sender, FileDropHandler handler, IEnumerable<string> files)
        {
            await ExecuteFileDrop(handler, files);
        }

        async Task ExecuteFileDrop(FileDropHandler handler, IEnumerable<string> files)
        {
            if (!string.IsNullOrWhiteSpace(handler.Id))
            {
                var msg = new Message<FileDropExecuteEventBody>(InternalServices.MainUI, InternalEvents.ExecuteFileDrop, new FileDropExecuteEventBody(files, handler));
                await MessageBus.SendMessageToAsync(msg, handler.Id);
            }
        }

        private async Task<string?> GetFileHash(string path)
        {
            return await Task.Run(() =>
            {
                if (!File.Exists(path)) return null;
                try
                {
                    using (var hmd5 = MD5.Create())
                    {
                        var buffer = File.ReadAllBytes(path);
                        var hbytes = hmd5.ComputeHash(buffer);
                        return BitConverter.ToString(hbytes).Replace("-", "").ToLower();
                    }
                }
                catch { }
                return null;
            });
        }

        [SupportedOSPlatform("Windows")]
        public async Task<Message?> ReceiveMessageAsync(Message message, CancellationToken cancellationToken)
        {
            if (message.EventId == InternalEvents.ShowToastNotificationRequest && message.HasBody<ToastNotification>() && message.GetBody<ToastNotification>() is ToastNotification toast)
            {
                Notifications.ShowToast(toast);
                return message.CreateReply(Id);
            }
            if (message.EventId == InternalEvents.FileDrop)
            {
                if (message.GetBody<FileDropEventBody>() is FileDropEventBody body)
                {
                    if (body.Files.Count == 1 && Path.GetExtension(body.Files[0])?.ToLower().TrimStart('.') == "asvc")
                    {
                        var b = new FileDropHandler()
                        {
                            Id = Id,
                            Name = "Service Installer",
                            Icon = Util.CreateResourceUri("res/img/brick.png"),
                            Action = "service-install"
                        };

                        return message.CreateReply(Id, b);
                    }
                }
            }
            if (message.EventId == InternalEvents.ExecuteFileDrop)
            {
                if (message.GetBody<FileDropExecuteEventBody>() is FileDropExecuteEventBody body)
                {
                    if (body.Files.Count == 1 && body.Handler.Action == "service-install" && Path.GetExtension(body.Files[0])?.ToLower().TrimStart('.') == "asvc")
                    {
                        var tempDir = Path.Combine(Directory.GetCurrentDirectory(), "svc-install");
                        var pendingUpdateDir = Path.Combine(Directory.GetCurrentDirectory(), "pending-updates");
                        try
                        {
                            if (Directory.Exists(tempDir))
                            {
                                Directory.Delete(tempDir, true);
                            }
                            Directory.CreateDirectory(tempDir);

                            using (var zipFile = ZipFile.OpenRead(body.Files[0]))
                            {
                                zipFile.ExtractToDirectory(tempDir);
                            }

                            var tempFiles = Directory.EnumerateFiles(tempDir, "*", SearchOption.AllDirectories);
                            var overwriteableFiles = new string[] { ".deps.json", ".pdb" };
                            foreach (var tempFile in tempFiles)
                            {
                                var destFile = Path.Combine(Directory.GetCurrentDirectory(), tempFile.Substring(tempDir.Length).Trim('\\'));
                                var toInstallAfterRestart = Path.Combine(pendingUpdateDir, tempFile.Substring(tempDir.Length).Trim('\\'));
                                if (File.Exists(destFile) && !overwriteableFiles.Any(x => destFile.ToLower().EndsWith(x)))
                                {
                                    var hd = await GetFileHash(destFile);
                                    var ht = await GetFileHash(tempFile);
                                    if (hd != ht)
                                    {
                                        if (!Directory.Exists(pendingUpdateDir))
                                            Directory.CreateDirectory(pendingUpdateDir);
                                        var destDir = Path.GetDirectoryName(toInstallAfterRestart);
                                        if (!Directory.Exists(destDir) && destDir != null)
                                            Directory.CreateDirectory(destDir);
                                        File.Copy(tempFile, toInstallAfterRestart, true);
                                    }
                                }
                                else
                                {
                                    var destDir = Path.GetDirectoryName(destFile);
                                    if (!Directory.Exists(destDir) && destDir != null)
                                        Directory.CreateDirectory(destDir);
                                    File.Copy(tempFile, destFile, true);
                                }
                            }

                            var t = new ToastNotification();
                            t.Title = "Service Installation Success";
                            t.Contents.Add(new ToastNotificationContent()
                            {
                                Kind = ToastNotificationContentKind.Text,
                                Text = $"You have to restart aura to start using the service"
                            });
                            var tNotif = new Message<ToastNotification>(Id, InternalEvents.ShowToastNotificationRequest, t);
                            await MessageBus.SendMessageAsync(tNotif);
                        }
                        catch (Exception ex)
                        {
                            // make toast notif
                            var t = new ToastNotification();
                            t.Title = $"'{Path.GetFileNameWithoutExtension(body.Files[0])}' Installation Failed";
                            t.Contents.Add(new ToastNotificationContent()
                            {
                                Kind = ToastNotificationContentKind.Text,
                                Text = ex.Message.Length < 120 ? ex.Message : ex.GetType().Name + " was thrown",
                            });
                            var tNotif = new Message<ToastNotification>(Id, InternalEvents.ShowToastNotificationRequest, t);
                            await MessageBus.SendMessageAsync(tNotif);
                        }
                        finally
                        {
                            try
                            {
                                Directory.Delete(tempDir, true);
                            }
                            catch { }
                        }
                    }
                }
            }
            if (message.EventId == InternalEvents.PersonaConfigurationUpdated)
            {
                var config = ConfigService.LoadConfig<AuraConfig>();
                Topmost = config.Avatar.TopMost;
                ShowInTaskbar = config.ShowInTaskbar;
                if ((Persona?.Id ?? "").ToLower() + ".a2p" != config.Avatar.PersonaId?.ToLower() && config.Avatar.PersonaId != null)
                {
                    config.Avatar.Position = null;
                }
                LoadPersona();
            }
            if (message.EventId == InternalEvents.PersonaTagsUpdated)
            {
                HandleTagsUpdated();
            }
            return null;
        }

        private void HandleTagsUpdated()
        {
            var tagService = ServiceManager.GetInstance<TagService>();
            if (tagService != null)
            {
                avatarCanvas.ActiveTags.BeginUpdate();
                try
                {
                    avatarCanvas.ActiveTags.Clear();
                    var activeTags = tagService.ActiveTags;
                    foreach(var tag in activeTags)
                    {
                        avatarCanvas.ActiveTags.Add(tag.Key, tag.Value);
                    }
                }
                catch {
                }
                finally
                {
                    Dispatcher.Invoke(() =>
                    {
                        avatarCanvas.ActiveTags.EndUpdate();
                    });
                }
            }
        }
    }
}
