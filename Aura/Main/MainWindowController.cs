﻿using Aura.Services;
using System.Windows;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using System;
using Aura.Models.Configs;
using Aura.Models;
using Aura.WPF.Windows;
using System.IO;
using Aura.WPF;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Diagnostics;
using System.Reflection;

namespace Aura
{
    public partial class MainWindow
    {
        bool allowFileDrop = true;

        private void SetPosition(int? x, int? y)
        {
            if (x.HasValue && y.HasValue)
            {
                Left = x.Value;
                Top = y.Value;
                return;
            }

            var screenRect = GetCurrentScreenRect();

            if (screenRect != null)
            {
                Left = screenRect.Value.Right - Width;
                Top = screenRect.Value.Bottom - Height;
            }
            else
            {
                var primScreenRect = System.Windows.Forms.Screen.PrimaryScreen?.WorkingArea;
                if (primScreenRect != null)
                {
                    Left = primScreenRect.Value.Right - Width;
                    Top = primScreenRect.Value.Bottom - Height;
                }
            }
        }

        private Rect? GetCurrentScreenRect()
        {
            var rect = new Rect(Left, Top, Width, Height);
            var screenRects = new List<Rect>();
            foreach (var scrn in System.Windows.Forms.Screen.AllScreens)
            {
                screenRects.Add(new Rect(scrn.WorkingArea.Left, scrn.WorkingArea.Top, scrn.WorkingArea.Width, scrn.WorkingArea.Height));
            }
            var windowRectsInScreens = screenRects.Select(s =>
            {
                var clone = new Rect(s.Left, s.Top, s.Width, s.Height);
                clone.Intersect(rect);
                return clone;
            }).ToList();

            Rect maxRect;
            double area = 0;
            int index = 0;
            int selIndex = -1;
            foreach (var re in windowRectsInScreens)
            {
                if (re.IsEmpty)
                {
                    index++;
                    continue;
                }
                var curArea = re.Height * re.Width;
                if (curArea > area)
                {
                    selIndex = index;
                    maxRect = re;
                    area = curArea;
                }
                index++;
            }

            if (selIndex >= 0)
            {
                return screenRects[selIndex];
            }
            return null;
        }

        private Rect? GetScreenThatContains(Point point)
        {
            var screenRects = new List<Rect>();
            foreach (var scrn in System.Windows.Forms.Screen.AllScreens.Where(s => new Rect(s.Bounds.Left, s.Bounds.Top, s.Bounds.Width, s.Bounds.Height).Contains(point)))
            {
                screenRects.Add(new Rect(scrn.WorkingArea.Left, scrn.WorkingArea.Top, scrn.WorkingArea.Width, scrn.WorkingArea.Height));
            }
            if (screenRects.Count > 0)
            {
                return screenRects[0];
            }
            return null;
        }

        private Rect? GetScreenThatContains(double x, double y)
        {
            return GetScreenThatContains(new Point(x, y));
        }

        private void SavePosition()
        {
            var config = ConfigService.LoadConfig<AuraConfig>() ?? new AuraConfig();
            if (config != null)
            {
                config.Avatar.Position = new AuraAvatarPosition()
                {
                    X = (int)Left,
                    Y = (int)Top,
                };
            }
        }

        private void EnsureSnap()
        {
            var config = ConfigService.LoadConfig<AuraConfig>();
            if (config == null || !config.Avatar.SnapToScreen) return;
            var screenRect = GetCurrentScreenRect();
            if (screenRect == null) return;
            Rect currentScreen = screenRect.Value;
            Left = Math.Max(currentScreen.Left, Math.Min(currentScreen.Right - Width, Left));
            Top = Math.Max(currentScreen.Top, Math.Min(currentScreen.Bottom - Height, Top));
        }

        void CheckPersonaSelected()
        {
            var personaConfig = ConfigService.LoadConfig<AuraConfig>();
            var currentPersonaPath = Path.Combine(AuraPaths.Compendium, personaConfig?.Avatar?.PersonaId ?? "");
            if (!Directory.Exists(AuraPaths.Compendium))
            {
                Directory.CreateDirectory(AuraPaths.Compendium);
            }
            if (!File.Exists(currentPersonaPath))
            {
                if (MessageBox.Show("No persona is loaded, do you want to open compendium?\r\nApplication will exit if you choose no.", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                {
                    AppEntryPoint.DoShutdown();
                    return;
                }

                if (!PromptUserToSelectPersonaFromCompendium())
                {
                    AppEntryPoint.DoShutdown();
                    return;
                }
            }
        }

        bool PromptUserToSelectPersonaFromCompendium()
        {
            var personaConfig = ConfigService.LoadConfig<AuraConfig>();
            var compend = new PersonaCompendium();
            compend.SetWorkingDirectory(AuraPaths.Compendium);
            if (personaConfig != null && compend.ShowDialog() == true && File.Exists(compend.SelectedPersonaPath))
            {
                try
                {
                    var p = Persona.Load(compend.SelectedPersonaPath, true);
                    personaConfig.Avatar.PersonaId = p.Id + ".a2p";
                    personaConfig.Avatar.Position = null;
                    ConfigService.SaveConfig<AuraConfig>();
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            return false;
        }

        Point? GetPositionOfAnchor(string anchor)
        {
            var cScreen = GetCurrentScreenRect();
            var config = ConfigService.LoadConfig<AuraConfig>();
            var scale = config.Avatar.Scale / 100.0;
            if (cScreen != null && avatarCanvas.Avatar != null)
            {
                var regPoint = avatarCanvas.Avatar.Regions.FirstOrDefault(x => x.Reference == anchor);
                if (regPoint != null && regPoint.Shapes.Any(x => x.Type == ShapeType.Point))
                {
                    var fShape = regPoint.Shapes.First(x => x.Type == ShapeType.Point);
                    var x = Left + fShape.X * scale;
                    var y = Top + fShape.Y * scale;
                    return new Point(x, y);
                }
            }
            return null;
        }

        Tuple<string, Point> CalculateCommandAnchor()
        {
            var config = ConfigService.LoadConfig<AuraConfig>();
            var scale = config.Avatar.Scale / 100.0;
            var cScreen = GetCurrentScreenRect();
            if (cScreen != null)
            {
                var mid = cScreen.Value.Left + (cScreen.Value.Width / 2);
                var anchorTag = Left < mid ? InternalTags.Anchor.CommandRight : InternalTags.Anchor.CommandLeft;
                var anchor = GetPositionOfAnchor(anchorTag);

                if (anchor.HasValue)
                {
                    return new Tuple<string, Point>(anchorTag, anchor.Value);
                }
            }
            return new Tuple<string, Point>(InternalTags.Anchor.CommandOrigin, new Point(Left + Width / 2, Top + Height / 2));
        }
    }
}
