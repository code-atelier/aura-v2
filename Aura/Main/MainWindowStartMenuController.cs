﻿using Aura.Models;
using Aura.Models.Configs;
using Aura.Services;
using Aura.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Aura
{
    public partial class MainWindow
    {
        Window? StartMenuWindow = null;
        void ShowStartMenu(bool fromNotificationArea = false)
        {
            if (StartMenuWindow != null && StartMenuWindow is StartMenu sm)
            {
                try
                {
                    sm.BeginClose();
                }
                catch { }
            }

            StartMenuWindow = new StartMenu();
            StartMenuWindow.Owner = this;
            StartMenuWindow.Closed += StartMenuWindow_Closed;

            if (StartMenuWindow is StartMenu startMenu)
            {
                startMenu.ShutdownClicked += StartMenu_ShutdownClicked;
                startMenu.MinimizeClicked += StartMenu_MinimizeClicked;
                if (!fromNotificationArea)
                {
                    var cmdAnchor = CalculateCommandAnchor();
                    startMenu.Show(cmdAnchor.Item1, cmdAnchor.Item2.X, cmdAnchor.Item2.Y);

                    // send notification
                    var msg = new Message(InternalServices.MainUI, InternalEvents.StartMenuOpened);
                    MessageBus.SendMessageAsync(msg);
                }
                else
                {
                    var mPos = System.Windows.Forms.Control.MousePosition;
                    var cScreen = GetScreenThatContains(mPos.X, mPos.Y);
                    if (cScreen != null)
                    {
                        startMenu.Show(InternalTags.Anchor.CommandTray, cScreen.Value.Right, cScreen.Value.Bottom);
                        startMenu.Activate();
                    }
                }
            }
        }

        private void StartMenu_MinimizeClicked(object? sender, EventArgs e)
        {
            if (NotifyIcon.Visible == true)
            {
                Show();
                WindowState = WindowState.Normal;
                NotifyIcon.Visible = false;
                ShowInTaskbar = ConfigService.LoadConfig<AuraConfig>().ShowInTaskbar;
            }
            else
            {
                WindowState = WindowState.Minimized;
                ShowInTaskbar = false;
                Hide();
                NotifyIcon.Visible = true;
            }
        }

        private void StartMenu_ShutdownClicked(object? sender, EventArgs e)
        {
            var f = new ShutdownWindow();
            f.Owner = sender as Window;
            var mpos = System.Windows.Forms.Control.MousePosition;
            f.Left = mpos.X - f.Width;
            f.Top = mpos.Y;
            var r = f.ShowShutdownDialog();
            if (r == ShutdownWindowResult.Shutdown)
            {
                forceClose = true;
                Close();
            }
            else if (r == ShutdownWindowResult.Restart)
            {
                forceClose = true;
                AppEntryPoint.RestartAfterClose = true;
                Close();
            }
        }

        private void StartMenuWindow_Closed(object? sender, EventArgs e)
        {
            if (sender is Window w && w == StartMenuWindow)
            {
                StartMenuWindow = null;
            }
        }
    }
}
