﻿using Aura.ConsoleCommands;
using Aura.Models;
using Aura.Services;
using Aura.Services.Active;
using Aura.Services.Passive;
using Aura.Tools;
using Microsoft.Toolkit.Uwp.Notifications;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Threading;
using System.Windows;
using System.Windows.Markup;

namespace Aura
{
    public static class AppEntryPoint
    {
        const string IPCPipeName = "auraipc";
        public static bool RestartAfterClose { get; set; } = false;
        public static IPCServer? IPCServer { get; private set; }

        [STAThread]
        public static void Main(string[] args)
        {
            // wait until previous instance is closed
            while (args.Any(x => x?.ToLower() == "-restart") && AlreadyRunning())
            {
                Thread.Sleep(100);
            }

            HandlePendingUpdates();

            var curdir = Path.GetDirectoryName(typeof(AppEntryPoint).Assembly.Location);
            if (curdir != null && Directory.Exists(curdir))
            {
                Directory.SetCurrentDirectory(curdir);
            }
            HandleCommands(args);
            var uriRequests = HandleUri(args);
            HandleToastActivation();

            // exit if this is uri request and Aura already running
            if (!uriRequests.Item1) return;

            // just exit if this was a callback for toast
            if (ToastNotificationManagerCompat.WasCurrentProcessToastActivated())
            {
                return;
            }

            // prevent duplicate instance except when ran with -dupe switch
            if (!args.Any(x => x?.ToLower() == "-dupe") && AlreadyRunning())
            {
                return;
            }

            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            args = args ?? new string[] { };

            // do not load or start any non-core service when in safe mode
            ServiceUtil.SafeMode = args.Any(x => x?.ToLower() == "-safe");

            try
            {
                RegisterComponents();
                ServiceManager.LoadComponents();

                StartIPC();

                var app = new App();
                app.UriArguments = uriRequests.Item2;
                app.FastLoading = args.Any(x => x?.ToLower() == "-quick");
                app.Run();
            }
            catch
            {
                throw;
            }
            finally
            {
                try
                {
                    var logger = ServiceManager.GetInstance<AuraLogger>();
                    logger.Information("Stopping services...");
                    BeforeShutdown();
                    logger.Information("Aura stopped.");

                    if (RestartAfterClose)
                    {
                        // start a new instance
                        var path = Assembly.GetExecutingAssembly().Location;
                        path = Path.GetFileNameWithoutExtension(path) + ".exe";
                        var psi = new ProcessStartInfo()
                        {
                            Arguments = "-quick -restart",
                            FileName = path,
                            UseShellExecute = false,
                            WorkingDirectory = Environment.CurrentDirectory,
                        };
                        Process.Start(psi);
                    }
                }
                catch { }
            }
        }

        private static void HandlePendingUpdates()
        {
            var pendingUpdateDir = Path.Combine(Directory.GetCurrentDirectory(), "pending-updates");
            if (Directory.Exists(pendingUpdateDir))
            {
                try
                {
                    var files = Directory.EnumerateFiles(pendingUpdateDir, "*", SearchOption.AllDirectories);
                    foreach (var file in files)
                    {
                        var destFile = Path.Combine(Directory.GetCurrentDirectory(), file.Substring(pendingUpdateDir.Length).Trim('\\'));
                        var destDir = Path.GetDirectoryName(destFile);
                        if (!Directory.Exists(destDir) && destDir != null)
                            Directory.CreateDirectory(destDir);
                        File.Copy(file, destFile, true);
                    }
                }
                catch { }
                finally
                {
                    try
                    {
                        Directory.Delete(pendingUpdateDir, true);
                    }
                    catch { }
                }
            }
        }

        private static void HandleToastActivation()
        {
            ToastNotificationManagerCompat.OnActivated += ToastNotificationManagerCompat_OnActivated;
        }

        private static void ToastNotificationManagerCompat_OnActivated(ToastNotificationActivatedEventArgsCompat e)
        {
            var args = ToastArguments.Parse(e.Argument);

            if (args.Contains("event") && args.Contains("body"))
            {
                var evt = args["event"];
                var body = args["body"];
                if (evt == InternalEvents.ToastShowInExplorerRequest && File.Exists(body))
                {
                    Util.ShowInExplorer(body);
                    return;
                }
                if (evt == InternalEvents.ToastOpenFileRequest && File.Exists(body))
                {
                    Util.OpenFile(body);
                    return;
                }
            }

            try
            {
                var dic = new Dictionary<string, string>();
                foreach (var kv in args)
                {
                    dic[kv.Key] = kv.Value;
                }
                var messageBus = ServiceManager.GetInstance<IServiceMessageBus>();
                messageBus.SendMessage(new Message<Dictionary<string, string>>(InternalServices.ToastNotification, InternalEvents.ToastActivation, dic));
            }
            catch { }
        }

        private static void RegisterComponents()
        {
            ServiceManager.RegisterService<ConfigurationService>();
            ServiceManager.RegisterService<TagService>();
            ServiceManager.RegisterService<APIService>();
            ServiceManager.RegisterService<ScriptingService>();

            ServiceManager.RegisterConsoleExtension<EchoCommand>();
            ServiceManager.RegisterConsoleExtension<SvcCommand>();
            ServiceManager.RegisterConsoleExtension<HelpCommand>();
            ServiceManager.RegisterConsoleExtension<ClsCommand>();
            ServiceManager.RegisterConsoleExtension<HistoryCommand>();

            ServiceManager.RegisterTool<ConsoleTool>();
            ServiceManager.RegisterTool<DebugTool>();
            ServiceManager.RegisterTool<ServiceManagerTool>();
        }

        private static async void BeforeShutdown()
        {
            await ServiceManager.ShutdownServices();
            ToastNotificationManagerCompat.Uninstall();
            if (IPCServer != null)
            {
                IPCServer.Stop();
            }
        }

        public static bool IsShuttingDown { get; private set; } = false;

        public static async void DoShutdown()
        {
            IsShuttingDown = true;
            Application.Current?.Shutdown();
            await ServiceManager.ShutdownServices();
        }

        private static Assembly? CurrentDomain_AssemblyResolve(object? sender, ResolveEventArgs args)
        {
            try
            {
                string folderPath = Path.GetFullPath(AuraPaths.Assemblies.Replace("{assembly}", args.RequestingAssembly?.GetName()?.Name));
                if (!Directory.Exists(folderPath)) return null;

                string assemblyFileName = new AssemblyName(args.Name).Name + ".dll";
                var assemblyFiles = Directory.EnumerateFiles(folderPath, assemblyFileName, SearchOption.AllDirectories).ToList();
                assemblyFiles.AddRange(Directory.EnumerateFiles(AuraPaths.Services, assemblyFileName, SearchOption.AllDirectories).Where(x => !assemblyFiles.Contains(x)));
                if (assemblyFiles.Count == 0)
                    return null;
                foreach (var assemblyPath in assemblyFiles)
                {
                    try
                    {
                        Assembly assembly = Assembly.LoadFrom(assemblyPath);
                        if (args.Name == assembly.FullName)
                        {
                            return assembly;
                        }
                    }
                    catch { }
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static bool HandleCommands(string[] args)
        {
            try
            {
                if (args.Contains("-registerurischeme"))
                {
                    using (var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Classes\\aura"))
                    {
                        // Replace typeof(App) by the class that contains the Main method or any class located in the project that produces the exe.
                        // or replace typeof(App).Assembly.Location by anything that gives the full path to the exe
                        string applicationLocation = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? Directory.GetCurrentDirectory(), "Aura.exe");

                        key.SetValue("", "URL:Aura Protocol");
                        key.SetValue("URL Protocol", "");

                        using (var defaultIcon = key.CreateSubKey("DefaultIcon"))
                        {
                            defaultIcon.SetValue("", applicationLocation + ",1");
                        }

                        using (var commandKey = key.CreateSubKey(@"shell\open\command"))
                        {
                            commandKey.SetValue("", "\"" + applicationLocation + "\" \"%1\"");
                        }
                    }
                    return true;
                }
                if (args.Contains("-unregisterurischeme"))
                {
                    Registry.CurrentUser.DeleteSubKey("SOFTWARE\\Classes\\aura");
                    return true;
                }
            }
            catch { }
            return false;
        }

        public static Tuple<bool, UriRequest?> HandleUri(string[] args)
        {
            var uris = args.Select(x => Uri.TryCreate(x.Trim('"'), UriKind.Absolute, out var uri) ? uri : null).OfType<Uri>().Where(x => string.Equals(x.Scheme, "aura", StringComparison.OrdinalIgnoreCase)).ToList();
            var uriRequest = new UriRequest()
            {
                Uris = uris.ToList(),
            };
            if (AlreadyRunning())
            {
                // send ipc
                IPCClient.SendMessageToPipe(IPCPipeName, new IPCMessage()
                {
                    Message = JsonSerializer.Serialize(uriRequest),
                    Type = "uri"
                }.ToJSON());
                return new Tuple<bool, UriRequest?>(false, uriRequest);
            }
            return new Tuple<bool, UriRequest?>(true, uriRequest);
        }

        public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private static bool AlreadyRunning()
        {
            Process[] processes = Process.GetProcesses();
            Process currentProc = Process.GetCurrentProcess();
            if (processes.Any(p =>
                {
                    try
                    {
                        return (currentProc.ProcessName == p.ProcessName || currentProc.MainModule?.FileName == p.MainModule?.FileName) && currentProc.Id != p.Id;
                    }
                    catch { }
                    return false;
                }
                )
            )
            {
                return true;
            }
            return false;
        }

        private static void StartIPC()
        {
            try
            {
                var smb = ServiceManager.GetInstance<IServiceMessageBus>();
                IPCServer = new IPCServer(IPCPipeName, smb);
                IPCServer.Start();
            }
            catch { }
        }
    }
}
