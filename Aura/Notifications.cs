﻿using Aura.Models;
using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public static class Notifications
    {
        [SupportedOSPlatform("windows10.0.17763.0")]
        // https://learn.microsoft.com/en-us/windows/apps/design/shell/tiles-and-notifications/send-local-toast?tabs=desktop
        public static void ShowToast(ToastNotification notification)
        {
            var toast = new ToastContentBuilder();
            toast.AddText(notification.Title);
            foreach (var content in notification.Contents)
            {
                if (content.Kind == ToastNotificationContentKind.Text && !string.IsNullOrWhiteSpace(content.Text))
                {
                    toast.AddText($"{content.Text.Trim()}");
                }
                if (content.Kind == ToastNotificationContentKind.InlineImage && content.ImageUri != null)
                {
                    toast.AddInlineImage(content.ImageUri);
                }
                if (content.Kind == ToastNotificationContentKind.HeroImage && content.ImageUri != null)
                {
                    toast.AddHeroImage(content.ImageUri);
                }
                if (content.Kind == ToastNotificationContentKind.Button && !string.IsNullOrWhiteSpace(content.Text))
                {
                    var button = new ToastButton()
                        .SetContent(content.Text);
                    foreach (var arg in content.Arguments)
                    {
                        button = button.AddArgument(arg.Key, arg.Value);
                    }
                    switch (content.ButtonActivation)
                    {
                        default:
                        case ToastNotificationButtonActivation.BackgroundActivation:
                            button = button.SetBackgroundActivation();
                            break;
                        case ToastNotificationButtonActivation.ProtocolActivation:
                            button = button.SetProtocolActivation(content.ActivationUri);
                            break;
                        case ToastNotificationButtonActivation.Snooze:
                            button = button.SetSnoozeActivation();
                            break;
                        case ToastNotificationButtonActivation.Dismiss:
                            button = button.SetDismissActivation();
                            break;
                    }
                    toast.AddButton(button);
                }
            }

            foreach (var arg in notification.Arguments)
            {
                toast.AddArgument(arg.Key, arg.Value);
            }
            toast.SetToastScenario((ToastScenario)(int)notification.Scenario);
            toast.SetToastDuration((ToastDuration)(int)notification.Duration);
            toast.Show(toastInstance =>
            {
                toastInstance.ExpirationTime = notification.ExpirationTime;
                toastInstance.Group = notification.Group ?? "aura";
                if (notification.Tag != null)
                {
                    toastInstance.Tag = notification.Tag;
                }
            });
        }
    }
}
