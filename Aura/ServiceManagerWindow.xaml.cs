﻿using Aura.Models.Configs;
using Aura.Services;
using Aura.Services.Passive;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura
{
    /// <summary>
    /// Interaction logic for ServiceManagerWindow.xaml
    /// </summary>
    public partial class ServiceManagerWindow : Window
    {
        DispatcherTimer Timer { get; } = new DispatcherTimer(DispatcherPriority.Render);
        ObservableCollection<ServiceGridItem> GridItems { get; } = new ObservableCollection<ServiceGridItem>();
        ConfigurationService ConfigurationService { get; }

        public ServiceManagerWindow()
        {
            InitializeComponent();
            LoadServices();
            Timer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            Timer.Start();
            Timer.Tick += Timer_Tick;
            dgvServices.ItemsSource = GridItems;
            ConfigurationService = ServiceManager.GetInstance<ConfigurationService>();
        }

        private void LoadServices()
        {
            var serviceItems = ServiceManager.Services.Select(x => new ServiceGridItem(x)).ToList();
            serviceItems.Sort((a, b) =>
            {
                if (ServiceUtil.IsSystemService(a.Service) && !ServiceUtil.IsSystemService(b.Service))
                    return -1;
                if (!ServiceUtil.IsSystemService(a.Service) && ServiceUtil.IsSystemService(b.Service))
                    return 1;
                return a.Name.CompareTo(b.Name);
            });
            int selIndex = dgvServices.SelectedIndex;
            GridItems.Clear();
            foreach (var service in serviceItems)
            {
                GridItems.Add(service);
            }
            if (GridItems.Count > 0)
            {
                dgvServices.SelectedIndex = selIndex >= 0 ? selIndex : 0;
            }
        }

        private void RefreshGrid()
        {
            foreach (var item in GridItems)
            {
                item.RaisePropertyChanged();
            }
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            RefreshGrid();
            LoadDataFromSelection(false);
        }

        private void dgvServices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadDataFromSelection(true);
        }

        private void LoadDataFromSelection(bool reloadImage)
        {
            string? logoSource = null;

            if (dgvServices.SelectedItem is ServiceGridItem item)
            {
                logoSource = item.Service.Info.Icon;
                lbAuthor.Content = item.Author;
                lbId.Content = "v" + item.Service.Info.Version;
                lbName.Content = item.Name;
                lbDescription.Text = "Description:" + Environment.NewLine + item.Service.Info.Description;
                lbConfigureService.Visibility = (item.Service.UserActions & ServiceUserAction.Configure) > 0 && item.Service is IServiceWithConfiguration ? Visibility.Visible : Visibility.Collapsed;
                lbShowLog.Visibility = (item.Service.UserActions & ServiceUserAction.ViewLog) > 0 && item.Service is IServiceWithLog ? Visibility.Visible : Visibility.Collapsed;
                lbDisableService.Visibility = ServiceUtil.IsSystemService(item.Service) ? Visibility.Collapsed : ReadAutostartFor(item.Service) ? Visibility.Visible : Visibility.Collapsed;
                lbEnableService.Visibility = ServiceUtil.IsSystemService(item.Service) ? Visibility.Collapsed : ReadAutostartFor(item.Service) ? Visibility.Collapsed : Visibility.Visible;
                lbRestartService.Visibility = (item.Service.UserActions & ServiceUserAction.Restart) > 0 && item.Service.State == ServiceState.Running ? Visibility.Visible : Visibility.Collapsed;
                lbStartService.Visibility = (item.Service.UserActions & ServiceUserAction.Start) > 0 && item.Service.State == ServiceState.Stopped ? Visibility.Visible : Visibility.Collapsed;
                lbStopService.Visibility = (item.Service.UserActions & ServiceUserAction.Stop) > 0 && item.Service.State == ServiceState.Running ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                lbAuthor.Content = "";
                lbId.Content = "";
                lbName.Content = "";
                lbDescription.Text = "Description:" + Environment.NewLine;
                lbShowLog.Visibility =
                lbConfigureService.Visibility =
                lbDisableService.Visibility =
                lbEnableService.Visibility =
                lbRestartService.Visibility =
                lbStartService.Visibility =
                lbStopService.Visibility = Visibility.Collapsed;
            }
            if (reloadImage)
            {
                if (logoSource != null)
                {
                    try
                    {
                        var logo = new BitmapImage();
                        logo.BeginInit();
                        logo.UriSource = new Uri(logoSource);
                        logo.EndInit();
                        imgLogo.Source = logo;
                        return;
                    }
                    catch { }
                }
                try
                {
                    var logo = new BitmapImage();
                    logo.BeginInit();
                    logo.UriSource = new Uri(Util.CreateResourceUri("res/img/cogs.png"));
                    logo.EndInit();
                    imgLogo.Source = logo;
                }
                catch { }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Timer.Stop();
        }

        private async void hlRestartService_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem sgi)
            {
                try
                {
                    await sgi.Service.Shutdown();
                    await Task.Delay(100);
                    await sgi.Service.Start();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Service failed to restart.\r\nError: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private async void hlStartService_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem sgi)
            {
                try
                {
                    await sgi.Service.Start();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Service failed to start.\r\nError: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private async void hlStopService_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem sgi)
            {
                try
                {
                    await sgi.Service.Shutdown();
                }
                catch
                {

                }
            }
        }

        private void hlConfigureService_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem sgi)
            {
                try
                {
                    if ((sgi.Service.UserActions & ServiceUserAction.Configure) > 0 && sgi.Service is IServiceWithConfiguration swc)
                    {
                        var w = swc.CreateConfigurationWindow();
                        w.Owner = this;
                        w.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("This service does not have a configuration tool.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch
                {

                }
            }
        }

        private bool ReadAutostartFor(IService service)
        {
            var cfg = ConfigurationService.LoadConfig<AuraConfig>();
            if (cfg == null) return true;
            return cfg.AutoStartService.ContainsKey(service.Info.Id) ? cfg.AutoStartService[service.Info.Id] : cfg.DefaultServiceAutoStart;
        }

        private void WriteAutostartFor(IService service, bool autoStart)
        {
            var cfg = ConfigurationService.LoadConfig<AuraConfig>();
            if (cfg != null)
            {
                cfg.AutoStartService[service.Info.Id] = autoStart;
                ConfigurationService.SaveConfig<AuraConfig>();
            }
        }

        private void hlDisableService_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem sgi)
            {
                try
                {
                    WriteAutostartFor(sgi.Service, false);
                }
                catch
                {

                }
            }
        }

        private void hlEnableService_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem sgi)
            {
                try
                {
                    WriteAutostartFor(sgi.Service, true);
                }
                catch
                {

                }
            }
        }

        private void hlShowLog_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem sgi)
            {
                try
                {
                    if ((sgi.Service.UserActions & ServiceUserAction.ViewLog) > 0 && sgi.Service is IServiceWithLog swc)
                    {
                        var log = swc.Log;
                        var w = new LogViewer();
                        w.LoadLog(log);
                        w.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("This service does not have a log.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch
                {

                }
            }
        }
    }

    public class ServiceGridItem : INotifyPropertyChanged
    {
        public IService Service { get; }

        public ServiceGridItem(IService service)
        {
            Service = service;
        }

        public string Name { get => Service.Info.Name; }
        public string Author { get => Service.Info.Author; }
        public string Id { get => Service.Info.Id; }
        public string Status { get => Service.State.ToString(); }
        public string StartupType { get => ReadAutostartFor(Service) ? "Automatic" : "Manual"; }

        public void RaisePropertyChanged()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Status)));
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(StartupType)));
            }
        }
        public event PropertyChangedEventHandler? PropertyChanged;

        private bool ReadAutostartFor(IService service)
        {
            var cfgS = ServiceManager.GetInstance<ConfigurationService>();
            var cfg = cfgS.LoadConfig<AuraConfig>();
            if (cfg == null) return true;
            return cfg.AutoStartService.ContainsKey(service.Info.Id) ? cfg.AutoStartService[service.Info.Id] : cfg.DefaultServiceAutoStart;
        }
    }
}
